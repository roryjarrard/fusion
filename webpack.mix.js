const {mix} = require('laravel-mix');
const {uglify} = require('uglifyjs-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .setPublicPath('public')
    .js('resources/assets/js/app.js', './public/js')
    .options({
        uglify: {
            uglifyOptions: {
                compress: {
                    drop_console: true
                }
            }
        }
    })
    .extract(['vue'])
    .sourceMaps()
    .sass('resources/assets/sass/app.scss', './public/css');
