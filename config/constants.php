<?php

defined("CUT_OFF_DATE") or define("CUT_OFF_DATE", "2019-01-01"); // date after correcting parts to fix existing bugs will be activated. Not to impact previous years/months
// we can change this date.

defined("YELLOW_NEW_DRIVER") or define("YELLOW_NEW_DRIVER", 1);
defined("GREEN_REIMBURSEMENT") or define("GREEN_REIMBURSEMENT", 2);
defined("RED_VEHICLE_TOO_OLD") or define("RED_VEHICLE_TOO_OLD", 4);
defined("RED_INSUFFICIENT_INSURANCE") or define("RED_INSUFFICIENT_INSURANCE", 8);
defined("RED_AWAITING_INSURANCE") or define("RED_AWAITING_INSURANCE", 16);
defined("RED_NO_REIMBURSEMENT") or define("RED_NO_REIMBURSEMENT", 32); // DELIBERATE, AND SPECIFICALLY APPLIED ON EDIT USER, NOT AUTO CALCULATED
defined("RED_INSURANCE_EXPIRED") or define("RED_INSURANCE_EXPIRED", 64);
defined("RED_ON_LEAVE") or define("RED_ON_LEAVE", 128);
defined("RED_DISABILITY") or define("RED_DISABILITY", 256);
defined("RED_TERMINATED") or define("RED_TERMINATED", 512);
defined("RED_DECLARATION_EXPIRED") or define("RED_DECLARATION_EXPIRED", 1024); // DEPRECATED
defined("RED_NO_BUSINESS_USE") or define("RED_NO_BUSINESS_USE", 2048); // DEPRECATED
defined("RED_DRIVER_LICENCE_REQUIRED") or define("RED_DRIVER_LICENCE_REQUIRED", 4096); //note, this is for when a license is 'expired', we require a new license
defined("GREEN_NO_INSURANCE_COMPLIANCE") or define("GREEN_NO_INSURANCE_COMPLIANCE", 8192);      // FAVR
defined("GREEN_NO_VEHICLE_COST_COMPLIANCE") or define("GREEN_NO_VEHICLE_COST_COMPLIANCE", 16384);  // FAVR
defined("GREEN_NO_VEHICLE_AGE_COMPLIANCE") or define("GREEN_NO_VEHICLE_AGE_COMPLIANCE", 32768);   // FAVR
defined("GREEN_NO_ODOMETER_COMPLIANCE") or define("GREEN_NO_ODOMETER_COMPLIANCE", 65536);      // FAVR    // DEPRECATED
defined("GREEN_NO_DEPRECIATION_COMPLIANCE") or define("GREEN_NO_DEPRECIATION_COMPLIANCE", 131072); // FAVR
defined("GREEN_NO_INSURANCE_IN_GRACE_PERIOD") or define("GREEN_NO_INSURANCE_IN_GRACE_PERIOD", 262144); // FAVR // TODO GRACE COVERED BY REIMBURSEMENT, BUT THIS WAS USED IN THE PAST. NEEDS TO BE LOOKED AT AGAIN LATER
defined("GREEN_NO_ANNUAL_DECLARATION") or define("GREEN_NO_ANNUAL_DECLARATION", 524288);      // FAVR
defined("RED_AWAITING_LICENCE") or define("RED_AWAITING_LICENCE", 1048576);
defined("GREEN_NO_5000_MILES_COMPLIANCE") or define("GREEN_NO_5000_MILES_COMPLIANCE", 2097152);  // FAVR // TODO IN PROGRESS switched annual compliances tables
defined("GREEN_NO_BANKING_DATA") or define("GREEN_NO_BANKING_DATA", 4194304); // USED NOW
defined("RED_LICENSE_AWAITING_PROCESSING") or define("RED_LICENSE_AWAITING_PROCESSING", 8388608);
defined("GREEN_LICENSE_AWAITING_PROCESSING") or define("GREEN_LICENSE_AWAITING_PROCESSING", 16777216); // FAVR NEW STATUS
defined("RED_INSURANCE_AWAITING_PROCESSING") or define("RED_INSURANCE_AWAITING_PROCESSING", 33554432); // NEW STATUS
defined("GREEN_INSURANCE_AWAITING_PROCESSING") or define("GREEN_INSURANCE_AWAITING_PROCESSING", 67108864); // FAVR NEW STATUS

// countries
defined("USA") or define("USA",1);
defined("CANADA") or define("CANADA",2);

// territory types
defined("HOME_CITY") or define("HOME_CITY", 1);
defined("HOME_STATE") or define("HOME_STATE", 2);
defined("MULTI_CITIES") or define("MULTI_CITIES", 3);
defined("MULTI_STATES") or define("MULTI_STATES", 4);

defined("WHOLESALE") or define("WHOLESALE", 4);

defined("PLAN_463") or define("PLAN_463", '463');
defined("PLAN_FAVR") or define("PLAN_FAVR", 'FAVR');
defined("PLAN_CANADA") or define("PLAN_CANADA", 'Canada');
defined("PLAN_CONSULTING") or define("PLAN_CONSULTING", 'Consulting');

// specail cases for these companies
defined("WASTE_MANAGEMENT_CANADA") or define("WASTE_MANAGEMENT_CANADA", 56);
defined("WASTE_MANAGEMENT_USA") or define("WASTE_MANAGEMENT_USA", 54);
defined("PULTE_HOMES") or define("PULTE_HOMES", 57);
defined("WHIRLPOOL") or define("WHIRLPOOL", 48);

defined("ALERTS_EMAIL_ADDRESS") or define("ALERTS_EMAIL_ADDRESS", 'alerts@cardataconsultants.com');

defined("CARDATA_BLUE_HEX") or define("CARDATA_BLUE_HEX", '#052460');
// TODO: this cant happen, constants must be scalar values
//defined("CARDATA_BLUE_RGB") or define("CARDATA_BLUE_RGB", [5,36,96]);
defined("CARDATA_BLUE_ARGB") or define("CARDATA_BLUE_ARGB", 'FF052460');

defined("CARDATA_RED_HEX") or define("CARDATA_RED_HEX", '#910811');
// TODO: this cant happen, constants must be scalar values
//defined("CARDATA_RED_RGB") or define("CARDATA_RED_RGB", [145,8,17]);
defined("CARDATA_RED_ARGB") or define("CARDATA_RED_ARGB", 'FF910811');



// use $yellow = YELLOW_NEW_DRIVER;

return [
    'yellow-new-driver', 1,
    'green-reimbursement', 2,
    'red-vehicle-too-old', 4,
    'red-insufficient-insurance', 8,
    'red-awaiting-insurance', 16,
    'red-no-reimbursement', 32,
    'red-insurance-expired', 64,
    'red-on-leave', 128,
    'red-disability', 256,
    'red-terminated', 512,
    'red-declaration-expired', 1024,
    'red-no-business-use', 2048,
    'red-driver-licence-required', 4096,
    'green-no-insurance-compliance', 8192,
    'green-no-vehicle-cost-compliance', 16384,
    'green-no-vehicle-age-complaince', 32768,
    'green-no-odometer-compliance', 65536,
    'green-no-depreciation-compliance', 131072,
    'green-no-insurance-in-grace-period', 262144,
    'green-no-annual-declaration', 524288,
    'red-awaiting-licence', 1048576,
    'green-no-5000-miles-compliance', 2097152,
    'green-no-banking-data', 4194304,
    'red-inactive', 16777216,

    'USA',1,
    'Canada',2,

    'home-city',1,
    'home-state',2,
    'multi-cities',3,
    'multi-states',4,

];

// use axios.get(‘/getConfigValue/constants.yellow-new-driver’).then…
