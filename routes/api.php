<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Setup CORS */
//header('Access-Control-Allow-Origin: *');
//header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
//header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

Route::post('oauth/token', 'ApiController@issueToken');
Route::middleware('auth:api')->post('logout', 'ApiController@logout');
Route::middleware('auth:api')->post('/getUserInfo', 'ApiController@getUserInfo');

Route::middleware('auth:api')->post('/endDay', 'ApiController@endDay');
Route::middleware('auth:api')->post('/endPreviousDay', 'ApiController@endPreviousDay');
Route::middleware('auth:api')->post('/getNearbyStops', 'ApiController@getNearbyStops');
Route::middleware('auth:api')->post('/submitNewStop', 'ApiController@submitNewStop');
Route::middleware('auth:api')->post('/submitStartingOdometer', 'ApiController@submitStartingOdometer');
Route::middleware('auth:api')->post('/updateDailyMileageLastState', 'ApiController@updateDailyMileageLastState');
Route::middleware('auth:api')->post('/atStop', 'ApiController@atStop');
Route::middleware('auth:api')->post('/resumeDriving', 'ApiController@resumeDriving');
Route::middleware('auth:api')->post('/apiLog', 'ApiController@apiLog');
Route::middleware('auth:api')->post('/setMirouteDriverOptions', 'ApiController@setMirouteDriverOptions');
Route::middleware('auth:api')->post('/updateDailyTrip', 'ApiController@updateDailyTrip');
Route::middleware('auth:api')->post('/deleteDailyTrip', 'ApiController@deleteDailyTrip');
Route::middleware('auth:api')->get('/getLastEndingOdometer', 'ApiController@getLastEndingOdometer');
Route::middleware('auth:api')->get('/getDriverStatusItems', 'ApiController@getDriverStatusItems');

Route::post('/validateEmailForResetCode', 'ApiController@validateEmailForResetCode');
Route::post('/validateResetCode', 'ApiController@validateResetCode');
Route::post('/submitNewPassword', 'ApiController@submitNewPassword');

Route::middleware('auth:api')->get('/validate-token', 'ApiController@validateToken');
Route::get('/requestSecret', 'ApiController@requestSecret');



