<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', ['as' => 'redirectToPortal', 'middleware' => 'auth', 'uses' => 'PortalController@redirectToPortal']);

Route::get('/profile', ['as' => 'view.profile', 'uses' => 'PortalController@viewProfile']);

Route::resource('users', 'UserController');
Route::resource('companies', 'CompanyController');

// all users
Route::group(['middleware' => 'role:driver|manager|administrator|super'], function () {
    Route::get('/getCompanyDates/{company_id}', 'CompanyController@getCompanyDates');
    Route::get('/switchRole', 'UserController@switchRole')->name('switchRole');
    Route::post('/linkToUser', 'PortalController@linkToUser');
    Route::get('/findUserByEmail/{email}', 'UserController@findUserByEmail');
    Route::get('/getCompanyLogoExists/{company_slug}', 'CompanyController@getCompanyLogoExists');
    Route::get('/insertDriverInsurancesRecord/{user_id}', 'DriverController@insertDriverInsurancesRecord');
    Route::post('/notifyApproverOfNewInsuranceDocument/{company_id}', 'CompanyController@notifyApproverOfNewInsuranceDocument');
    Route::post('/setView', 'UtilityController@setView');
    Route::get('/getMileageByMonthForDriver/{user_id}/{year?}/{mileage}', 'DriverController@getMileageByMonthForDriver');
    Route::get('/getUserAddress/{user_id}/{user_type}', 'UserController@getUserAddress');
    Route::post('/spoofStreetlightUpdate', 'DriverController@spoofStreetlightUpdate')->name('spoof.streetlight.update');
    Route::get('/getStateProvince/{state_province_id}', 'UtilityController@getStateProvince');

    // todo remove this test function
    Route::post('/sendCredentialsTest', 'UserController@sendCredentialsTest');
    /** ---------------------------------------------------------------------- */

    Route::get('/getContractsForSavedStop/{saved_stop_id}', 'CompanyController@getContractsForSavedStop');
    Route::get('/getDriverDivision/{driver_id}', 'DriverController@getDriverDivision')->name('get.driver.division');
    Route::get('/getDriverTimeZone/{driver_id}', 'DriverController@getDriverTimeZone')->name('get.driver.time_zone');
    Route::get('/getHighestAuthenticatedUserType', 'UserController@getHighestAuthenticatedUserType')->name('get.highest.authenticated.user.type');
    Route::get('/getEmail', 'UserController@getEmail')->name('get.email');
    Route::get('/getCompanySelections/{company}', 'CompanyController@getCompanySelections')->name('company.selections');
    Route::post('/updateEmail', ['as'=>'update.email', 'uses'=>'UserController@updateEmail']);
    Route::post('/updatePersonalEmail', ['as'=>'update.personal.email', 'uses'=>'UserController@updatePersonalEmail']);
    Route::post('/updatePassword', 'UserController@updatePassword')->name('update.password');
    Route::get('/getConfigValue/{field}', ['as'=>'get.config.value', 'uses'=>'UtilityController@getConfigValue']);

    Route::get('/checkUserPermission/{user_id}/{user_type}/{permission}', 'UserController@checkUserPermission')->name('check.user.permission');
    Route::get('/checkIfCommuterDriver/{user_id}', 'DriverController@checkIfCommuterDriver');

    Route::get('/getNotifications', ['as'=>'get.notifications', 'uses'=>'UtilityController@getNotifications']);
    Route::post('/markNotificationsRead', ['as'=>'mark.notifications.read', 'uses'=>'UtilityController@markNotificationsRead']);
    Route::post('/getHolidays', ['as'=>'get.holidays', 'uses'=>'UtilityController@getHolidays']);
    Route::get('/getHolidaysStateProvinceYear/{state_province_id}/{year}', ['as'=>'get.holidays.state.province.year', 'uses'=>'UtilityController@getHolidaysStateProvinceYear']);
    Route::post('/addHolidays', ['as'=>'add.holidays', 'uses'=>'UtilityController@addHolidays']);
    Route::post('/updateHoliday', ['as'=>'update.holiday', 'uses'=>'UtilityController@updateHoliday']);
    Route::get('/isHoliday/{date}/{country_id?}/{state_province_id?}', ['as'=>'is.holiday', 'uses'=>'UtilityController@isHoliday']);
    Route::get('/getHolidayOn/{date}', ['as'=>'get.holiday.on', 'uses'=>'UtilityController@getHolidayOn']);
    Route::get('/getGeoDataFromZipPostal/{country_id}/{zip_postal_code}/{get_state_province?}', ['as'=>'get.geo.data.from.zip.postal', 'uses'=>'UtilityController@getGeoDataFromZipPostal']);
    Route::post('/parseGoogleAddressData', ['as'=>'parse.google.address.data', 'uses'=>'UtilityController@parseGoogleAddressData']);
    Route::get('/getFuelYears', 'UtilityController@getFuelYears')->name('get.fuel.years');

    Route::get('/getDriverNotes/{driver_id}', ['as'=>'get.driver.notes', 'uses'=>'DriverController@getDriverNotes']);
    Route::post('/addDestinationAddress', ['as'=>'add.address', 'uses'=>'DriverController@addDestinationAddress']);
    Route::get('/getInsuranceMinimums/{company_id}', ['as'=>'get.insurance.minimums', 'uses'=>'CompanyController@getInsuranceMinimums']);
    Route::post('/uploadDriverLicenseDocument', ['as'=>'upload.driver.license', 'uses'=>'DriverController@uploadDriverLicenseDocument']);
    Route::post('/uploadDriverInsuranceDocument', ['as'=>'upload.driver.insurance', 'uses'=>'DriverController@uploadDriverInsuranceDocument']);
    Route::get('/getDriverDocuments/{doc_type}/{user_id}', ['as'=>'get.driver.documents', 'uses'=>'DriverController@getDriverDocuments']);
    Route::get('/resolveUserId/{id}/{user_type}', ['as'=>'resolve.user.id', 'uses'=>'UserController@resolveUserId']);
    Route::post('/uploadFile', ['as'=>'upload.file', 'uses'=>'UtilityController@uploadFile']);
    Route::get('/getVehicleProfile/{vehicle_profile_id}/{inactive?}/{get_drivers?}/{get_creator?}', ['as'=>'get.vehicle.profile', 'uses'=>'CompanyController@getVehicleProfile']);
    Route::get('/getCompanyName/{company_id}', ['as' => 'company.name', 'uses' => 'CompanyController@getCompanyName']);
    Route::get('/getCompanyCountry/{company_id}', ['as' => 'get.company_country', 'uses' => 'CompanyController@getCompanyCountry']);
    Route::get('/getCompanyBank/{company_id}/{restricted?}', 'CompanyController@getCompanyBank')->name('get.company.bank');
    Route::get('/getCompanyCountry/{company_id}', ['as' => 'get.company_country', 'uses' => 'CompanyController@getCompanyCountry']);
    Route::get('/getCompanyModuleOptions/{company_id}', ['as' => 'get.company.options', 'uses' => 'CompanyController@getCompanyModuleOptions']);
    Route::get('/getCompanyInsuranceRequirements/{company_id}', ['as' => 'get.company.insurance.requirements', 'uses' => 'CompanyController@getCompanyInsuranceRequirements']);
    Route::get('/getCompanyCoordinators/{company_id}', 'CompanyController@getCompanyCoordinators');

    Route::post('/getReimbursement', ['as' => 'driver.reimbursement', 'uses' => 'DriverController@getReimbursementForYear']);
    Route::post('getFavrReimbursementStatus', ['as' => 'get.favr.reimbursement.status', 'uses' => 'DriverController@getFavrReimbursementStatus']);
    Route::post('calculateReimbursement', ['as' => 'calculate.fixed.reimbursement', 'uses' => 'ReimbursementController@calculateReimbursement']);
    Route::get('getGenerateReimbursement/{year?}/{month?}/{company_id?}/{driver_profile_id?}', ['as'=>'get.generate.reimbursement', 'uses'=>'ReimbursementController@generateReimbursement']);
    Route::get('addReimbursementDetails/{driver_id}', ['as'=>'add.reimbursement.details', 'uses'=>'ReimbursementController@addReimbursementDetails']);

    Route::get('/getCarPolicyDocuments/{company_id?}', ['as' => 'get.car_policy_documents', 'uses' => 'CompanyController@getCarPolicyDocuments']);
    Route::get('/getCompanyMileageDates/{company_id?}', ['as' => 'get.company_mileage_dates', 'uses' => 'CompanyController@getCompanyMileageDates']);
    Route::get('/getCompanyPaymentDates/{company_id?}/{with_holidays?}/{year?}/{month?}', ['as' => 'get.company_payment_dates', 'uses' => 'CompanyController@getCompanyPaymentDates']);

    Route::get('/users/{id}/impersonate', ['as' => 'start.impersonating', 'uses' => 'PortalController@impersonate']);
    Route::get('/stopImpersonating', ['as' => 'stop.impersonating', 'uses' => 'PortalController@stopImpersonate']);
    Route::get('/getDriver/{driver_id}', ['as' => 'get.driver', 'uses' => 'UserController@getDriver']);
    Route::post('/getDaysForMonthAndYear', ['as' => 'get.days', 'uses' => 'UtilityController@getDaysForMonthAndYear']);
    Route::get('/getMonths/{disabled_future_months?}/{disabled_present_month?}', ['as' => 'get.months', 'uses' => 'UtilityController@getMonths']);
    Route::post('/setSession', ['as' => 'set.session', 'uses' => 'UtilityController@setSession']);
    Route::post('/setMultipleSessionValues', ['as' => 'set.multi.session', 'uses' => 'UtilityController@setMultipleSessionValues']);
    Route::post('/getSessionValues', ['as' => 'get.session', 'uses' => 'UtilityController@getSessionValues']);
    Route::post('/findCities/{input}/{country_id?}', ['as' => 'post.find.cities', 'uses' => 'UtilityController@findCities']);

    Route::get('/getVehicleMakes', ['as' => 'get.makes', 'uses' => 'VehicleController@getVehicleMakes']);
    Route::post('/getVehicleModels', ['as' => 'get.models', 'uses' => 'VehicleController@getVehicleModels']);
    Route::post('/getVehicleTrims', ['as' => 'get.trims', 'uses' => 'VehicleController@getVehicleTrims']);
    Route::get('/getVehicleCategory/{category_id}', 'VehicleController@getVehicleCategory');
    Route::get('/getStatesProvinces/{country_id}', ['as' => 'get.states', 'uses' => 'UtilityController@getStatesProvinces']);
    Route::get('/getCounties/{state_id}', ['as' => 'get.counties', 'uses' => 'UtilityController@getCounties']);
    Route::get('/getZipCodes/{state_id}', ['as' => 'get.zip.codes', 'uses' => 'UtilityController@getZipCodes']);
    Route::get('/getCounty/{zip_code}', 'UtilityController@getCounty');
    Route::get('/getPostalCodes/{province_id}/{postal_code?}', ['as' => 'get.postal.codes', 'uses' => 'UtilityController@getPostalCodes']);
    Route::get('/getUser/{user_id?}/{user_type_id?}', 'UserController@getUser');
    Route::get('/getMe/{user_id?}/{user_type_id?}', ['as' => 'get.me', 'uses' => 'UserController@getMe']);

    Route::get('/getResaleConditions', ['as' => 'get.resale.conditions', 'uses' => 'UtilityController@getResaleConditions']);
    Route::get('/getVehiclesYearRange', ['as' => 'get.vehicles.year.range', 'uses' => 'VehicleController@getVehiclesYearRange']);
    Route::get('/getVehiclesFromYearAndPlan/{year}/{plan}', ['as' => 'get.vehicles.from.year.and.plan', 'uses' => 'ReimbursementController@getVehiclesFromYearAndPlan']);
    Route::get('/getVehicle/{id}', ['as' => 'get.vehicle', 'uses' => 'UtilityController@getVehicle']);

    Route::get('/getCompanyQuarterHistory/{company_id}', 'TaxController@getCompanyQuarterHistory');
    Route::post('/getQuarterDefinition', ['uses' =>         'TaxController@getQuarterDefinition']);
    Route::post('/getDriverAllQuartersTax463', ['uses' =>   'TaxController@getDriverAllQuartersTax463']);
    Route::post('/getDriverAllQuartersTaxFAVR', ['uses' =>  'TaxController@getDriverAllQuartersTaxFAVR']);
    Route::post('/getDriverAllQuartersTaxWM', ['uses' =>    'TaxController@getDriverAllQuartersTaxWM']);
    Route::post('/getDriverAllQuartersTaxCanada', ['uses' =>'TaxController@getDriverAllQuartersTaxCanada']);
    Route::post('/createNewQuarterDefinition', 'TaxController@createNewQuarterDefinition');
    Route::post('/deleteCompanyQuarterDefinition', 'TaxController@deleteCompanyQuarterDefinition');

    Route::get('/getDriverStops', ['as' => 'get.driver.stops', 'uses' => 'DriverController@getDriverStops']);
    Route::post('/insertNewTrip', ['as' => 'post.insert.new.trip', 'uses' => 'DriverController@insertNewTrip']);
    Route::post('/updateTrip', ['as' => 'update.trip', 'uses' => 'DriverController@updateTrip']);
    Route::post('/deleteTrip', ['as' => 'update.trip', 'uses' => 'DriverController@deleteTrip']);
    Route::post('/recalculateTrips', ['as' => 'recalculate.trip', 'uses' => 'DriverController@recalculateTrips']); // after changing order

    Route::get('/getDriverStreetLightData/{user_id}', ['as' => 'get-driver-street-light-data', 'uses' => 'DriverController@getDriverStreetLightData']);

    Route::get('/getCitiesFromSearchTerm/{search_term?}', 'UtilityController@getCitiesFromSearchTerm')->name('get.cities.from.search.term');

    Route::post('/getLoginHistory', ['uses' => 'DriverController@getLoginHistory']); // after changing order

    Route::post('/getCompanyStartStopYears', ['uses' => 'CompanyController@getCompanyStartStopYears']); // for report years range

    ///////////////////////////////////////
    Route::get('testRun', ['uses'=>'DailyMasterTaskController@testRun']);
    ///////////////////////////////////////

    Route::post('/generateExcel', 'FileController@generateExcel');
    Route::get('/getReportLabel/{key}/{company_id}', 'ReportController@getReportLabel');
    Route::get('/getCompanyReportLabels/{company_id}', 'CompanyController@getCompanyReportLabels');

});

// driver routes
Route::group(['middleware' => ['role:driver|manager|administrator|super', 'impersonate']], function () {
    Route::post('/endPreviousDay', 'DriverController@endPreviousDay');
    Route::get('/getHistoricalFuelData/{country_id}/{state_province_id}/{fuel_city_id?}', 'UtilityController@getHistoricalFuelData');
    Route::get('/getCompany/{id?}', ['as' => 'get.company', 'uses' => 'CompanyController@getCompany']);
    Route::get('/getTimeZone', 'UserController@getTimeZone');
    Route::get('/getMileageBand/{id}', 'VehicleController@getMileageBand');
    Route::post('/getDriverAttributes', 'DriverController@getDriverAttributes')->name('get.driver.attributes');
    Route::post('/getDailyMileageData', ['as'=>'get.daily.mileage.data', 'uses' => 'DriverController@getDailyMileageData']);
    Route::post('/getDailyTrips', ['as' => 'daily.trips', 'uses' => 'DriverController@getDailyTrips']);
    Route::post('/getMileage', ['as' => 'driver.mileage', 'uses' => 'DriverController@getMileage']);


    Route::get('/getPersonalVehicles/{user_id}', ['as' => 'get.vehicles', 'uses' => 'DriverController@getPersonalVehicles']);
    Route::get('/getPersonalVehicle/{id}', ['as' => 'get.personal.vehicle', 'uses' => 'DriverController@getPersonalVehicle']);
    Route::get('/getUserPersonalVehicle/{user_id}', 'DriverController@getUserPersonalVehicle')->name('get.user.personal_vehicle');
    Route::get('/getVehicleFavrParameters/{user_id}', 'DriverController@getVehicleFavrParameters')->name('get.vehicle.favr.parameters');

    Route::get('/countPersonalVehicles', ['as' => 'get.count.personal.vehicle', 'uses' => 'DriverController@countPersonalVehicles']);
    Route::post('/insertPersonalVehicle/{registration?}', ['as' => 'post.insert.personal.vehicle', 'uses' => 'DriverController@insertPersonalVehicle']);
    Route::post('/updatePersonalVehicle/{registration?}', ['as' => 'post.update.personal.vehicle', 'uses' => 'DriverController@updatePersonalVehicle']);
    Route::get('/getPreviousPersonalVehicle', ['as' => 'get.previous.vehicle', 'uses' => 'DriverController@getPreviousPersonalVehicle']);

    Route::get('/driverProfile', ['as' => 'driver.profile', 'uses' => 'DriverController@viewProfile']);
    Route::post('/getChangesInDriverProfile', ['as' => 'changes.in.driver.profile', 'uses' => 'DriverController@getChangesInDriverProfile']);
    Route::get('/getDriverContent', ['as' => 'driver.content', 'uses' => 'DriverController@getDriverContent']);
    Route::get('/trips', ['as' => 'driver.trips', 'uses' => 'DriverController@trips']);
    Route::get('/getAddress', ['as' => 'get.driverAddress', 'uses' => 'DriverController@getAddress']);
    Route::get('/getDivisionName', ['as' => 'get.divisionName', 'uses' => 'DriverController@getDivisionName']);
    Route::get('/getDriverVehicleProfile', ['as' => 'get.VehicleProfile', 'uses' => 'DriverController@getDriverVehicleProfile']);
    Route::get('/getDriverTerritory', ['as' => 'get.DriverTerritory', 'uses' => 'DriverController@getDriverTerritory']);
    Route::get('/getFuelCitiesAndPrices/{average?}', ['as' => 'get.fuel.cities.and.prices', 'uses' => 'DriverController@getFuelCitiesAndPrices']);

    Route::post('/getDestinationAddresses', ['as' => 'get.DestinationAddresses', 'uses' => 'DriverController@getDestinationAddresses']);
    Route::post('/renameDestinationAddress', ['as' => 'post.RenameDestinationAddress', 'uses' => 'DriverController@renameDestinationAddress']);
    Route::post('/insertDestinationAddress', ['as' => 'post.InsertDestinationAddress', 'uses' => 'DriverController@insertDestinationAddress']);

    Route::get('/getDriverData', ['as' => 'get.DriverData', 'uses' => 'DriverController@getAllDriverProfileData']);
    Route::post('/acceptCarPolicy', 'DriverController@acceptCarPolicy')->name('accept.car.policy');
    Route::get('/getAnnualIrsOdometerDeclarationForCurrentYear', 'DriverController@getAnnualIrsOdometerDeclarationForCurrentYear')->name('get.annual.irs.odometer.declaration.for.current.year');
    Route::get('/getAllAnnualIrsOdometerDeclarations', 'DriverController@getAllAnnualIrsOdometerDeclarations')->name('get.all.annual.irs.odometer.declarations');
    Route::post('/addAnnualIrsOdometerDeclaration/{odometer}', 'DriverController@addAnnualIrsOdometerDeclaration')->name('add.annual.irs.odometer.declaration');
    Route::get('/getDriverBanking/{country_id?}/{masked?}', ['as' => 'get.DriverBanking', 'uses' => 'DriverController@getDriverBanking']);
    Route::post('/addDriverBanking', ['as' => 'add.DriverBanking', 'uses' => 'DriverController@addDriverBanking']);
    Route::put('/updateDriverBanking/', ['as' => 'update.DriverBanking', 'uses' => 'DriverController@updateDriverBanking']);
    Route::post('getMileagePerVehicle', ['as' => 'get.Mileage.per.Vehicle', 'uses' => 'DriverController@getMileagePerVehicle']);
    Route::post('/updateDriverAddress/{registration?}', ['as' => 'update.driver.address', 'uses' => 'DriverController@updateDriverAddress']);
    Route::post('/updateDriverNameAndEmail', ['as' => 'update.driver.name.and.email', 'uses' => 'DriverController@updateDriverNameAndEmail']);
    Route::post('/getDriverReimbursementYearSpan', ['as' => 'get.driver.reimbursement.years', 'uses' => 'DriverController@getReimbursementYears']);
    Route::post('/getCompanyReimbursementYearSpan', ['as' => 'get.company.reimbursement.years', 'uses' => 'CompanyController@getReimbursementYears']);
    Route::post('/getDepreciationValues', ['as' => 'get.depreciation.values', 'uses' => 'DriverController@getDepreciationValues']);
    Route::post('/updateDailyMileage', ['as' => 'update.driver.daily.mileage', 'uses' => 'DriverController@updateDailyMileage']);
    Route::post('/updateDailyMileageWithText', ['as' => 'update.driver.daily.mileage.destination', 'uses' => 'DriverController@updateDailyMileageWithText']);
    Route::post('/updateMiRouteOdometer', ['as' => 'update.miroute.odometer', 'uses' => 'DriverController@updateMiRouteOdometer']);

    Route::get('/getMyFixedReimbursementData','ReimbursementController@getMyFixedReimbursementData')->name('get.my.fixed.reimbursement.data');
    Route::get('/getMyVariableReimbursementData','ReimbursementController@getMyVariableReimbursementData')->name('get.my.variable.reimbursement.data');

    Route::get('/monthly', ['as' => 'driver.monthly', 'uses' => 'DriverController@monthlyReimbursement']);
    Route::get('/schedule', ['as' => 'driver.schedule', 'uses' => 'DriverController@schedule']);
    Route::get('/policy', ['as' => 'driver.policy', 'uses' => 'DriverController@policy']);
    Route::get('/vehicle', ['as' => 'driver.vehicle', 'uses' => 'DriverController@vehicle']);
    Route::get('/license', ['as' => 'driver.license', 'uses' => 'DriverController@license']);
    Route::get('/insurance', ['as' => 'driver.insurance', 'uses' => 'DriverController@insurance']);
    Route::get('/address', ['as' => 'driver.address', 'uses' => 'DriverController@address']);
    Route::get('/direct_pay', ['as' => 'driver.direct_pay', 'uses' => 'DriverController@directPay']);
    Route::get('/password', ['as' => 'driver.password', 'uses' => 'DriverController@password']);
    Route::get('/email', ['as' => 'driver.email', 'uses' => 'DriverController@email']);


    Route::get('/getDriverRegistrationCompletion/{user_id}', 'DriverController@getDriverRegistrationCompletion');
    Route::post('/completeDriverRegistrationModule', 'DriverController@completeDriverRegistrationModule');
    Route::post('/completeDriverRegistration', 'DriverController@CompleteDriverRegistration');

});

// manager routes
Route::group(['middleware' => 'role:manager'], function () {

    Route::get('/mileage_approval', ['as' => 'manager.mileage_approval', 'uses' => 'ManagerController@mileageApproval']);
    Route::get('/link_to_driver', ['as' => 'manager.link_to_driver', 'uses' => 'ManagerController@linkToDriver']);
    Route::get('/viewed_tutorials', ['as' => 'manager.viewed_tutorials', 'uses' => 'ManagerController@viewedTutorials']);
    Route::get('/car_policy', ['as' => 'manager.car_policy', 'uses' => 'ManagerController@carPolicy']);
    Route::get('/reimbursement_totals', ['as' => 'manager.reimbursement_totals', 'uses' => 'ManagerController@reimbursementTotals']);
    Route::get('/fuel_prices', ['as' => 'manager.fuel_prices', 'uses' => 'ManagerController@fuelPrices']);
    Route::get('/vehicle_report', ['as' => 'manager.vehicle_report', 'uses' => 'ManagerController@vehicleReport']);
    Route::get('/traffic_light', ['as' => 'manager.traffic_light', 'uses' => 'ManagerController@trafficLightReport']);

});

// administrator routes
Route::group(['middleware' => 'role:administrator'], function () {
    Route::post('/postManagerApprovalStatus', ['as'=>'post.manager.approval.by.admin', 'uses'=>'AdministratorController@postManagerApprovalStatus']);
    Route::post('/postAdministratorApproval', ['as'=>'post.administrator.approval', 'uses'=>'AdministratorController@postAdministratorApproval']);
    Route::post('/getMasterPaymentData', 'ReimbursementController@getMasterPaymentData');

});

// combined routes
Route::group(['middleware' => 'role:manager|administrator|super'], function () {
    Route::get('/getAdministratorProfile/{active_administrator_id}', 'AdministratorController@getAdministratorProfile');
    Route::get('/getNotesForDriver/{driver_id}', 'UserController@getNotesForDriver');
    Route::get('/getCompaniesWithPreDefinedStops', ['as'=>'companies.with.predefined.stops', 'uses'=>'CompanyController@getCompaniesWithPreDefinedStops']);
    Route::post('/getCompanyPredefinedStops', ['as'=>'get.predefined.stops', 'uses'=>'CompanyController@getCompanyPredefinedStops']);
    Route::get('/getCompanySavedStopDivisions/{company_id}', ['as'=>'saved.stop.divisions', 'uses'=>'CompanyController@getCompanySavedStopDivisions']);
    Route::get('/getCompanyPredefinedStop/{id}/{use_stop_contract?}', ['as'=>'get.predefined.stop', 'uses'=>'CompanyController@getCompanyPredefinedStop']);
    Route::post('/updatePredefinedStop', ['as'=>'update.predefined.stop', 'uses'=>'CompanyController@updatePredefinedStop']);
    Route::post('/deletePredefinedStop', ['as'=>'delete.predefined.stop', 'uses'=>'CompanyController@deletePredefinedStop']);
    Route::post('/addNewNote', ['as'=>'add.note', 'uses'=>'UtilityController@addNewNote']);
    Route::get('/getUserTypes', ['as'=>'get.user.types', 'uses'=>'UserController@getUserTypes']);
    Route::get('/getVehicleProfiles/{company_id}', ['as'=>'get.vehicle.profiles', 'uses'=>'CompanyController@getVehicleProfiles']);
    Route::get('/getProposedVehicleProfiles/{company_id}', 'CompanyController@getProposedVehicleProfiles');
    Route::get('/getMyDrivers/{manager_id}', ['as' => 'my.drivers', 'uses' => 'ManagerController@getMyDrivers']);
    Route::get('/getDriverManagers/{driver_id}/{company_id?}', ['as' => 'get.driver.managers', 'uses' => 'DriverController@getDriverManagers']);
    Route::post('/getDrivers', ['as' => 'get.drivers', 'uses' => 'UserController@getDrivers']);
    Route::post('/getDriversWhoHaveBanking','UserController@getDriversWhoHaveBanking')->name('get.drivers.who.have.banking');
    Route::get('/getDriverInfo/{driver_id}', ['as' => 'get.driver.info', 'uses' => 'ManagerController@getDriverInfo']);
    Route::get('/getManagers/{company_id}/{from_session?}', ['as' => 'get.managers', 'uses' => 'UserController@getManagers']);
    Route::get('/getManager/{manager_id}', ['as' => 'get.manager', 'uses' => 'UserController@getManager']);
    Route::get('/getManagerAddress/{manager_id}', ['as' => 'get.manager.address', 'uses' => 'ManagerController@getManagerAddress']);
    Route::post('/addManagerAddress', ['as' => 'get.manager.address', 'uses' => 'ManagerController@addManagerAddress']);
    Route::post('/updateManagerAddress', ['as' => 'update.manager.address', 'uses' => 'ManagerController@updateManagerAddress']);
    Route::get('/getAdministrators/{company_id}/{from_session?}', ['as' => 'get.administrators', 'uses' => 'UserController@getAdministrators']);
    Route::get('/getAdministrator/{administrator_id}', ['as' => 'get.administrator', 'uses' => 'UserController@getAdministrator']);
    Route::get('/getCompanies/{level?}/{session_format?}/{from_session?}', ['as' => 'get.companies', 'uses' => 'CompanyController@getCompanies']);
    Route::get('/getCompaniesWherePaymentResponsibilityCarData/{country_id?}', 'CompanyController@getCompaniesWherePaymentResponsibilityCarData')->name('get.companies.where.payment.responsibility.car.data');

    Route::get('/getMyDivisions/{administrator_id}', ['as' => 'my.divisions', 'uses' => 'AdministratorController@getMyDivisions']);
    Route::get('/getDivisions/{company_id}/{from_session?}', ['as' => 'get.divisions', 'uses' => 'CompanyController@getDivisions']);
    Route::get('/getDivision/{id?}', ['as' => 'get.division', 'uses' => 'CompanyController@getDivision']);
    Route::post('/saveDivision', ['as' => 'save.division', 'uses' => 'CompanyController@saveDivision']);
    Route::post('/updateDivision/{division_id}/{division_name}', ['as' => 'update.division', 'uses' => 'CompanyController@updateDivision']);
    Route::post('/getReimbursementForTimePeriod', ['as' => 'reimbursement.time.period', 'uses' => 'ManagerController@getReimbursementForTimePeriod']);
    Route::post('/getReimbursementForDrivers', ['as' => 'reimbursement.for.drivers', 'uses' => 'ReimbursementController@getReimbursementForDrivers']);
    Route::get('/getCompanyPlanValues', ['as' => 'get.plans', 'uses' => 'UtilityController@getCompanyPlanValues']);
    Route::get('/getCompanyMileageEntryValues', ['as' => 'get.mileage.entries', 'uses' => 'UtilityController@getCompanyMileageEntryValues']);
    Route::get('/getCountries', ['as' => 'get.countries', 'uses' => 'UtilityController@getCountries']);
    Route::get('/getVehicles', ['as' => 'get.vehicles', 'uses' => 'UtilityController@getVehicles']);
    Route::post('/getVehiclesByCategories','VehicleController@getVehiclesByCategories');
    Route::post('/getVehiclesBySuperCategories', 'VehicleController@getVehiclesBySuperCategories')->name('get.vehicles.by.super.categories');
    Route::post('/getAllDataForMileageApproval', ['as' => 'get.all.dataFor.mileage.approval', 'uses' => 'ManagerController@getAllDataForMileageApproval']);
    Route::post('/addZipPostalCode', ['as' => 'add.zip.postal.code', 'uses' => 'UtilityController@addZipPostalCode']);
    Route::post('/getLngLatFromGoogle', ['as' => 'get.lng.lat.from.google', 'uses' => 'UtilityController@getLngLatFromGoogle']);
    Route::get('/getFuelCities/{country_id?}','UtilityController@getFuelCities')->name('get.fuel.cities');
    Route::get('/getApprovalYears/{manager_id','ManagerController@getApprovalYears');

    Route::post('/getCompanyOneQuarterTax', ['uses' => 'TaxController@getCompanyOneQuarterTax']);
    Route::post('/getCompanyAnnualTax', ['uses' => 'TaxController@getCompanyAnnualTax']);

    // testing how to generate approval and approver records
    Route::get('/getApprovalsRecords', ['as' => 'get.approvals.records', 'uses' => 'ManagerController@getApprovalRecords']);

    Route::post('/getApprovedStatus', ['as' => 'get.approved.status', 'uses' => 'ManagerController@getApprovedStatus']);
    Route::post('postManagerApproval', ['as' => 'post.manager.approval', 'uses' => 'ManagerController@postManagerApproval']);
    Route::post('/getManagerApprovalStatus', ['as' => 'get.manager.approval.status', 'uses' => 'ManagerController@getManagerApprovalStatus']);

    Route::post('/getProrateReimbursementData', 'ReimbursementController@getProrateReimbursementData');
    Route::post('/updateProrateReimbursement', 'ReimbursementController@updateProrateReimbursement');
    Route::post('/getProrateAdjustments', 'ReimbursementController@getProrateAdjustments');


    // used in displaying company information
    Route::get('/getCompanyIndustries', ['as'=>'company.industries', 'uses'=>'CompanyController@getCompanyIndustries']);
    Route::post('/getPotentialCoordinators', ['as' => 'get.potential.coordinators', 'uses' => 'CompanyController@getPotentialCoordinators'] );
    Route::get('/getTerritoryTypes', ['as' => 'territory.types', 'uses' => 'DriverController@getTerritoryTypes']);
    Route::get('/getMileageBands', ['as' => 'mileage.bands', 'uses' => 'DriverController@getMileageBands']);
    Route::post('calculateReimbursementForAdministrator', 'AdministratorController@calculateReimbursementForAdministrator');
    Route::post('generateExcelForReimbursementCalculatorAdministrator', 'AdministratorController@generateExcelForReimbursementCalculatorAdministrator');

    // used in admin add/edit user
    Route::get('/getUsersAdministrator/{company_id}', 'AdministratorController@getUsersAdministrator');
    Route::post('/createUser', 'UserController@createUser');
    Route::post('/updateUser', ['as' => 'update.user', 'uses' => 'UserController@updateUser']);

    // DOCUMENTS
    Route::get('/getTemplates/{slug}', 'UtilityController@getTemplates');
    Route::get('/getAnnualReviews/{company_id}', 'CompanyController@getAnnualReviews');

    // INSURANCE PROCESSING
    Route::get('/getMostRecentInsuranceReview/{user_id}', 'DriverController@getMostRecentInsuranceReview');


    // LICENSE PROCESSING
    Route::get('/getMostRecentLicenseReview/{user_id}', 'DriverController@getMostRecentLicenseReview');

    // ADD MANY DRIVERS ADMIN
    Route::get('/getDataForAdministratorAddManyDrivers/{company_id}', 'AdministratorController@getDataForAdministratorAddManyDrivers');

    Route::post('/generateLoginHistoryExcel', 'UserController@generateLoginHistoryExcel');
    Route::post('/generatePredefinedStopsExcel', 'CompanyController@generateLoginHistoryExcel');

    // REPORTS
    Route::post('/generatePayFile', 'CompanyController@generatePayFile');
    Route::post('/getReportSessionData', 'ReportController@getReportSessionData');
    Route::get('/getDirectDepositPaymentScheduleExcel/{company_id}/{year}', 'FileController@getDirectDepositPaymentScheduleExcel');


});


// super routes
Route::group(['middleware' => 'role:super'], function () {
    Route::get('/getCompanyAudits', 'SuperController@getCompanyAudits');
    Route::get('getCompanyNotes/{company_id}/{user_id?}', 'CompanyController@getCompanyNotes');
    Route::post('runManualTask/{task}', 'MasterTaskController@runManualTask');
    Route::post('getMasterTask/{task}', 'MasterTaskController@getMasterTask');
    Route::get('getMasterTaskStatus/{task_id}', 'MasterTaskController@getMasterTaskStatus');
    Route::post('updateMasterTaskStatus/{task_id}/{status}', 'MasterTaskController@updateMasterTaskStatus');

    // TEST
    Route::post('/processPulteFile', 'DailyMasterTaskController@callingPulteFileFromTestPage');

    // INSURANCE PROCESSING
    Route::post('/updateSingleInsuranceDocument', 'DriverController@updateSingleInsuranceDocument');
    Route::post('/updateMultipleInsuranceDocuments', 'DriverController@updateMultipleInsuranceDocuments');
    Route::post('/processInsurance', 'DriverController@processInsurance');
    Route::post('/getDriverInsuranceApprovalRecord', 'DriverController@getDriverInsuranceApprovalRecord');
    Route::get('/getDriverInsuranceProcessingDocuments/{user_id}', 'DriverController@getDriverInsuranceProcessingDocuments');
    Route::get('/getDriverInsurancesRecords', 'DriverController@getDriverInsurancesRecords');
    Route::post('/rejectSingleInsuranceDocument', 'DriverController@rejectSingleInsuranceDocument');
    Route::get('/getDriverInsuranceDocument/{document_id}', 'DriverController@getDriverInsuranceDocument');
    Route::get('/getAllInsuranceReviewLinks/{user_id}', 'DriverController@getAllInsuranceReviewLinks');

     // todo make this like insurance above

    Route::get('/inactivateDivision/{division_id}', 'CompanyController@inactivateDivision');
    Route::get('/getAvailableReportColumns/{report_type}', 'SuperController@getAvailableReportColumns');
    Route::post('/dynamicReportReimbursement', 'SuperController@dynamicReportReimbursement');
    Route::post('/getMasterPaymentData', 'ReimbursementController@getMasterPaymentData');


    Route::get('/getMasterTasks', 'SuperController@getMasterTasks')->name('get.master.tasks');

    // CRUD USERS
    Route::post('/addManyDrivers', 'CompanyController@addManyDrivers');
    Route::post('/findUser', ['as'=>'find.user', 'uses'=>'UserController@findUser']);
    Route::get('/getSuperUsers', ['as'=>'get.super.users', 'uses'=>'UserController@getSuperUsers']);

    Route::get('/getTimeZones', ['as'=>'get.timezones', 'uses'=>'UtilityController@getTimeZones']);

    Route::get('/getPageListByCategory', 'UtilityController@getPageListByCategory')->name('get.page.list.by.category');

    Route::post('/getUserByType', ['as' => 'user.by.type', 'uses' => 'UserController@getUserByType']);
    Route::get('/resetSession', ['as' => 'reset.session', 'uses' => 'SuperController@resetSession']);


    Route::post('/generateDefaultCompanyPaymentDates/{company_id}/{year}', ['as' => 'generate.default.company.payment.dates', 'uses' => 'CompanyController@generateDefaultCompanyPaymentDates']);
    Route::post('/updateCompanyPaymentDates', 'CompanyController@updateCompanyPaymentDates')->name('update.company.payment.dates');

    Route::get('/viewUsers', ['as' => 'view.users', 'uses' => 'SuperController@viewUsers']);
    //Route::get('/getUsers/{type?}', ['as' => 'get.users', 'uses' => 'SuperController@getUsers']); // this is not getting called anywhere
    Route::get('/getUsers/{company_id}/{from_session?}', 'UserController@getUsers'); // this is not getting called anywhere
    Route::get('/driverDivision/{id}', 'DriverController@driverDivision');
    Route::get('/driverAddress/{id}', 'DriverController@driverAddress');

    Route::get('/createUser', ['as' => 'create.user', 'uses' => 'UserController@createUser']);
    Route::get('/getUserDataForDeactivation/{user_id}', 'UserController@getUserDataForDeactivation')->name('get.user.data.for.deactivation');
    Route::post('/deactivateUser', 'UserController@deactivateUser')->name('deactivate.user');
    Route::get('/getUserDataForReactivation/{user_id}', 'UserController@getUserDataForReactivation')->name('get.user.data.for.reactivation');
    Route::post('/reactivateUser', 'UserController@reactivateUser')->name('reactivate.user');
    Route::post('/updateProfileStatus', 'UserController@updateProfileStatus');

    Route::get('/getNoteCategories', ['as' => 'note.categories', 'uses' => 'UtilityController@getNoteCategories']);
    Route::get('/getNoteCategory/{id}', ['as' => 'note.category', 'uses' => 'UtilityController@getNoteCategory']);
    Route::post('/updateNoteCategory', ['as' => 'update.note.category', 'uses' => 'UtilityController@updateNoteCategory']);
    Route::post('/addNoteCategory/{title}', ['as' => 'add.note.category', 'uses' => 'UtilityController@addNoteCategory']);

    // CRUD COMPANY
    Route::post('/updateCompanyProfile/{company_id}', ['as' => 'update.company.profile', 'uses' => 'CompanyController@updateCompanyProfile']);
    Route::post('/updateCompanyOptions/{company_id}', ['as' => 'update.company.options', 'uses' => 'CompanyController@updateCompanyOptions']);
    Route::post('/updateCompanyModuleOptions/{company_id}', 'CompanyController@updateCompanyOptions');

    Route::post('/updateCompanyInsurance/{company_id}', ['as' => 'update.company.insurance', 'uses' => 'CompanyController@updateCompanyInsurance']);
    Route::post('/updateCompanyLicense/{company_id}', ['as' => 'update.company.license', 'uses' => 'CompanyController@updateCompanyLicense']);
    Route::post('/updateCompanyAdministration/{company_id}', 'CompanyController@updateCompanyAdministration');
    Route::post('/updateCompanyMileage/{company_id}', ['as' => 'update.company.mileage', 'uses' => 'CompanyController@updateCompanyMileage']);
    Route::post('/updateCompanyContacts/{company_id}', ['as' => 'update.company.contacts', 'uses' => 'CompanyController@updateCompanyContacts']);
    Route::post('/updateCompanyTaxes/{company_id}', ['as' => 'update.company.taxes', 'uses' => 'CompanyController@updateCompanyTaxes']);
    Route::get('/getCompanyCarDataCoordinator/{company_id}', 'CompanyController@getCompanyCarDataCoordinator')->name('get.company.car.data.coordinator');

    Route::post('/inactivateCompany', 'CompanyController@inactivateCompany');
    Route::post('/reactivateCompany/{company_id}', ['as' => 'reactivate.company', 'uses' => 'CompanyController@reactivateCompany']);
    Route::get('/getCompanyCreatedAndDeletedAt/{company_id}', 'CompanyController@getCompanyCreatedAndDeletedAt')->name('get.company.created.and.deleted.at');
    Route::get('/getProcessingBank/{company_id}', 'CompanyController@getProcessingBank');
    Route::get('/getCompanyFeeHistory/{company_id}/{fee?}', 'CompanyController@getCompanyFeeHistory');
    Route::post('/deleteCompanyFee', 'CompanyController@deleteCompanyFee');
    Route::get('/getAllCompanyFees/{company_id}', 'CompanyController@getAllCompanyFees');

    // POWER TO THE PEOPLE
    Route::get('/getPowerToThePeopleData/{user_id}', 'ReimbursementController@getPowerToThePeopleData');
    Route::post('/thePeopleExerciseTheirPower', 'ReimbursementController@thePeopleExerciseTheirPower');


    // VEHICLE PROFILES
    Route::post('/approveVehicleProfile', 'SuperController@approveVehicleProfile')->name('approve.vehicle.profile');
    Route::post('/rejectVehicleProfile', 'SuperController@rejectVehicleProfile')->name('reject.vehicle.profile');
    Route::post('/addVehicleProfile', ['as' => 'add.vehicle.profile', 'uses' => 'CompanyController@addVehicleProfile']);
    Route::post('/updateVehicleProfile', ['as' => 'update.vehicle.profile', 'uses' => 'CompanyController@updateVehicleProfile']);
    Route::post('/reactivateVehicleProfile/{vehicle_profile_id}', ['as' => 'reactivate.vehicle.profile', 'uses' => 'CompanyController@reactivateVehicleProfile']);
    Route::post('/inactivateVehicleProfile/{vehicle_profile_id}', ['as' => 'inactivate.vehicle.profile', 'uses' => 'CompanyController@inactivateVehicleProfile']);
    Route::post('/deleteProposedVehicleProfile', 'CompanyController@deleteProposedVehicleProfile')->name('delete.proposed.vehicle.profile');
    Route::get('/getVehicleProfileMappings/{company_id}', 'CompanyController@getVehicleProfileMappings')->name('get.vehicle.profile.mappings');
    Route::get('/getVehicleProfilesForRateComparison/{company_id}', 'CompanyController@getVehicleProfilesForRateComparison')->name('get.vehicle.profiles.for.rate.comparison');
    Route::get('/getCompaniesWithVehicleProfilesAwaitingApproval','CompanyController@getCompaniesWithVehicleProfilesAwaitingApproval');
    Route::get('/getVehicleProfilesAwaitingApproval/{company_id}', 'CompanyController@getVehicleProfilesAwaitingApproval');

    // RATE COMPARISON SPECIFIC
    Route::post('/calculateImpactOfProposedVehicleProfiles', 'ReimbursementController@calculateImpactOfProposedVehicleProfiles')->name('calculate.impact.of.proposed.vehicle.profiles');
    Route::post('/submitProposedVehicleProfileMappingsForApproval', 'CompanyController@submitProposedVehicleProfileMappingsForApproval')->name('submit.proposed.vehicle.profile.mappings.for.approval');
    Route::post('/submitProposedVehicleProfilesForApproval', 'CompanyController@submitProposedVehicleProfilesForApproval')->name('submit.proposed.vehicle.profiles.for.approval');
    Route::post('/saveProposedVehicleProfileMappings', 'CompanyController@saveProposedVehicleProfileMappings')->name('save.proposed.vehicle.profile.mappings');
    Route::post('/approveVehicleProfileMappings', 'CompanyController@approveVehicleProfileMappings')->name('approve.vehicle.profile.mappings');
    Route::post('/rejectVehicleProfileMappings', 'CompanyController@rejectVehicleProfileMappings')->name('reject.vehicle.profile.mappings');
    Route::post('/applyVehicleProfileMappings', 'CompanyController@applyVehicleProfileMappings')->name('apply.vehicle.profile.mappings');

    // REPORTS
    Route::post('/getReimbursementData', 'ReimbursementController@getReimbursementData');
    Route::post('/getReimbursementTotals', 'ReimbursementController@getReimbursementTotals');
    Route::get('/getReimbursementDiscrepancyDetails/{new_driver_profile_id}/{old_driver_profile_id}/{year}/{month}/{service_plan}', 'CompanyController@getReimbursementDiscrepancyDetails');
    Route::post('generateFixedAndVariableDiscrepanciesExcel', 'CompanyController@generateFixedAndVariableDiscrepanciesExcel');
    Route::get('/getScheduledTaskNames','SuperController@getScheduledTaskNames');
    Route::post('/getScheduledTasks', 'SuperController@getScheduledTasks');
    Route::get('/getStandardMileageRate/{country_id}','ReimbursementController@getStandardMileageRate');


    // PAYMENTS
    Route::post('/generateMasterPaymentNoticeExcel', 'ReimbursementController@generateMasterPaymentNoticeExcel');
    Route::post('generateAchEftFromMasterPaymentNotice', 'ReimbursementController@generateAchEftFromMasterPaymentNotice');
    Route::post('/updateMonthlyApproval', 'ReimbursementController@updateMonthlyApproval');
    Route::post('/generateAch', 'ReimbursementController@generateAch')->name('generate.ach');
    Route::post('/generateEft', 'ReimbursementController@generateEft')->name('generate.eft');
    Route::get('/getAllBanks', 'SuperController@getAllBanks')->name('get.all.banks');
    Route::post('/addBank', 'SuperController@addBank')->name('add.bank');
    Route::post('/updateBank', 'SuperController@updateBank')->name('update.bank');
    Route::get('/getFixedAndVariableDiscrepancies/{company_id}/{division_id}/{year}/{month}', 'CompanyController@getFixedAndVariableDiscrepancies')->name('get.fixed.and.variable.discrepancies');

    // TESTING REIMBURSEMENT
    Route::get('/getDriversWithYearProfile/{year}', ['as'=>'get.drivers.with.year.profile', 'uses'=>'ReimbursementController@getDriversWithYearProfile']);
    Route::get('/calculateFixedReimbursementForDriver/{user_id}', ['uses'=>'reimbursementController@calculateFixedReimbursementForDriver']);

    // NOTIFICATIONS
    Route::get('/getAllNotificationTemplates', 'UtilityController@getAllNotificationTemplates')->name('get.all.notification.templates');
    Route::post('/sendNotificationFromNotificationsView', 'UtilityController@sendNotificationFromNotificationsView')->name('send.notification.from.notifications.view');
    Route::post('/addNotificationTemplate', 'UtilityController@addNotificationTemplate')->name('add.notification.template');
    Route::post('/updateNotificationTemplate', 'UtilityController@updateNotificationTemplate')->name('update.notification.template');
    Route::post('/deleteNotificationTemplate', 'UtilityController@deleteNotificationTemplate')->name('delete.notification.template');

    // RATE SELECTOR / REIMBURSEMENT CALCULATOR SPECIFIC
    Route::get('/getRateSelectorDefaultParameters', 'RateSelectorController@getRateSelectorDefaultParameters')->name('get.rate.selector.default.parameters');
    Route::post('/calculateReimbursementForRateSelector', 'RateSelectorController@calculateReimbursementForRateSelector')->name('calculate.reimbursement.for.rate.selector');
    Route::post('/generateRateSelectorPdf', 'RateSelectorController@generateRateSelectorPdf');
    Route::post('/generateRateSelectorExcel', 'RateSelectorController@generateRateSelectorExcel');
    Route::get('/getReimbursementCalculatorSession/{user_id}', 'ReimbursementController@getReimbursementCalculatorSession');

    // DOCUMENTS
    Route::post('/uploadAddManyDriversExcelDocument/{company_id}', 'CompanyController@uploadAddManyDriversExcelDocument');
    Route::post('/uploadPreDefinedStops/{company_id}/{use_stop_contract}', 'CompanyController@uploadPreDefinedStops');
    Route::post('/addPreDefinedStopsFromFile', 'CompanyController@addPreDefinedStopsFromFile');
    Route::get('/getAddManyDriversFiles/{company_id}', 'CompanyController@getAddManyDriversFiles');
    Route::post('/uploadAnnualReview/{company_id}/{year}',  'CompanyController@uploadAnnualReview');
    Route::post('/deleteAnnualReview', 'CompanyController@deleteAnnualReview');
    Route::post('/deleteTemporaryFile', 'UtilityController@deleteTemporaryFile');

    // MI-ROUTE
    Route::post('/getDemoDrivers/', 'CompanyController@getDemoDrivers');
    Route::post('/updateCompanyInvoicing', 'CompanyController@updateCompanyInvoicing');
    Route::post('/updateCompanyPayments', 'CompanyController@updateCompanyPayments');
    Route::post('/updateCompanyDates', 'CompanyController@updateCompanyDates');
    Route::post('/updateCompanyVehicles', 'CompanyController@updateCompanyVehicles');


});
