<?php

use Illuminate\Database\Seeder;

class ResaleConditionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\ResaleCondition::create(['id'=>1, 'name'=>'Trade-In Excellent']);
        App\ResaleCondition::create(['id'=>2, 'name'=>'Trade-In Good']);
        App\ResaleCondition::create(['id'=>3, 'name'=>'Trade-In Fair']);
        App\ResaleCondition::create(['id'=>4, 'name'=>'Wholesale']);
        App\ResaleCondition::create(['id'=>5, 'name'=>'Retail']);
        App\ResaleCondition::create(['id'=>6, 'name'=>'Private Party Excellent']);
        App\ResaleCondition::create(['id'=>7, 'name'=>'Private Party Good']);
        App\ResaleCondition::create(['id'=>8, 'name'=>'Private Party Fair']);
    }
}
