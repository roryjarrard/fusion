<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserRolesSeeder::class);
        $this->call(DriverStreetLightStatusesTableSeeder::class);
        $this->call(RegionsSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(TerritoryTypesSeeder::class);
        $this->call(ResaleConditionsSeeder::class);
        $this->call(NoteCategoriesSeeder::class);
        $this->call(CompanyIndustrySeeder::class);
        $this->call(MonthsSeeder::class);
        $this->call(VariableAdjustmentsSeeder::class);
        $this->call(NotificationTemplatesSeeder::class);
        $this->call(RateSelectorDefaultsSeeder::class);
        $this->call(VehicleSuperCategoriesSeeder::class);
        $this->call(ReimbursementCalculatorSessionsSeeder::class);
        $this->call(DriverTaxRowSeeder::class);
        $this->call(DriverTaxColumnSeeder::class);
        //$this->call(CompanyReportLabelsSeeder::class);
    }
}
