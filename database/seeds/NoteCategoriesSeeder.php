<?php

use Illuminate\Database\Seeder;

class NoteCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\NoteCategory::create(['title' => 'General']);
        App\NoteCategory::create(['title' => 'Company Note']);
        App\NoteCategory::create(['title' => 'Driver Note']);
        App\NoteCategory::create(['title' => 'Vehicle Profile Note']);
    }
}
