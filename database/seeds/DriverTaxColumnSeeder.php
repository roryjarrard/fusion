<?php

use Illuminate\Database\Seeder;

class DriverTaxColumnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\DriverTaxColumn::create(['name' => 'quarter']);
        App\DriverTaxColumn::create(['name' => 'mileage']);
        App\DriverTaxColumn::create(['name' => 'fixed']);
        App\DriverTaxColumn::create(['name' => 'variable']);
        App\DriverTaxColumn::create(['name' => 'reimbursement']);
        App\DriverTaxColumn::create(['name' => 'taxable']);
        App\DriverTaxColumn::create(['name' => '500_adjustment']);
        App\DriverTaxColumn::create(['name' => 'annual_adjustment']);
    }
}
