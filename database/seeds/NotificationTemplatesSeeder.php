<?php

use Illuminate\Database\Seeder;

class NotificationTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\NotificationTemplate::create([
            'creator' => 'Frank',
            'area' => 'Tests',
            'severity' => 'low',
            'title' => 'Test 1',
            'content' => 'This is a test notification',
            'icon' => null,
            'link' => null,
            'dismissible' => true,
        ]);

        App\NotificationTemplate::create([
            'creator' => 'Frank',
            'area' => 'Vehicle Profiles',
            'severity' => 'high',
            'title' => 'New Vehicle Profile Approval',
            'content' => 'A new vehicle profile has been created.',
            'icon' => null,
            'link' => json_encode([
                'button_title' => 'Vehicle Approvals',
                'page' => 'vehicleProfileApprovals',
                'subpage' => null,
            ]),
            'dismissible' => true,
        ]);

        App\NotificationTemplate::create([
            'creator' => 'Frank',
            'area' => 'Driver Documents',
            'severity' => 'high',
            'title' => 'New Driver Insurance',
            'content' => 'Driver documents are awaiting processing.',
            'icon' => null,
            'link' => json_encode([
                'button_title' => 'Insurance Approvals',
                'page' => 'insuranceApproval',
                'subpage' => null,
            ]),
            'dismissible' => true,
        ]);

        App\NotificationTemplate::create([
            'creator' => 'Frank',
            'area' => 'Driver Documents',
            'severity' => 'high',
            'title' => 'New Driver License',
            'content' => 'Driver documents are awaiting processing.',
            'icon' => null,
            'link' => json_encode([
                'button_title' => 'License Approvals',
                'page' => 'licenseApproval',
                'subpage' => null,
            ]),
            'dismissible' => true,
        ]);

        App\NotificationTemplate::create([
            'creator' => 'Frank',
            'area' => 'Rate Comparison',
            'severity' => 'high',
            'title' => 'New Vehicle Profile Mapping',
            'content' => 'Proposed vehicle profile mapping is awaiting approval.',
            'icon' => null,
            'link' => json_encode([
                'button_title' => 'Rate Comparison',
                'page' => 'rateComparison',
                'subpage' => null,
            ]),
            'dismissible' => true,
        ]);

        App\NotificationTemplate::create([
            'creator' => 'Frank',
            'area' => 'Rate Comparison',
            'severity' => 'high',
            'title' => 'Proposed Vehicle Profiles Awaiting Approval',
            'content' => 'There are proposed vehicle profiles from the Rate Comparison tool awaiting approval. The company is: ',
            'icon' => null,
            'link' => json_encode([
                'button_title' => 'Vehicle Profile Approvals',
                'page' => 'vehicleProfileApprovals',
                'subpage' => null,
            ]),
            'dismissible' => true,
        ]);

    }
}
