<?php

use Illuminate\Database\Seeder;
use App\DriverStreetLightStatus;

class DriverStreetLightStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DriverStreetLightStatus::create([
            'id' => pow(2, 0),
            'color' => 'Yellow',
            'title' => 'New Driver',
            'description' => '',
            'service_plan' => 'ALL',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,1),
            'color' => 'Green',
            'title' => 'Reimbursement',
            'description' => 'You qualify for reimbursement.',
            'service_plan' => 'ALL',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,2),
            'color' => 'Red',
            'title' => 'Vehicle Too Old',
            'description' => 'Your vehicle is too old.',
            'service_plan' => '463',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,3),
            'color' => 'Red',
            'title' => 'Insufficient Insurance',
            'description' => 'Your insurance policy does not meet the required minimums. For detailed instructions, please go to the Insurance section Driver Profile Tab. You will not receive reimbursement while in Red Light Status.',
            'service_plan' => '463',
            'insurance_related' => true,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,4),
            'color' => 'Red',
            'title' => 'Awaiting Insurance',
            'description' => 'Insurance documents required.  To upload, and for detailed instructions, please go to the Insurance section Driver Profile Tab.  You will not receive reimbursement while in Red Light Status.',
            'service_plan' => '463',
            'insurance_related' => true,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,5),
            'color' => 'Red',
            'title' => 'No Reimbursement',
            'description' => 'You do not qualify for reimbursement',
            'service_plan' => '463',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,6),
            'color' => 'Red',
            'title' => 'Insurance Expired',
            'description' => 'Your insurance expired. You will not receive reimbursement while in Red Light Status.',
            'service_plan' => '463',
            'insurance_related' => true,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,7),
            'color' => 'Red',
            'title' => 'On Leave',
            'description' => 'Your record was marked "On Leave"',
            'service_plan' => 'ALL',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => true,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,8),
            'color' => 'Red',
            'title' => 'Disability',
            'description' => 'Your record was marked "Disability"',
            'service_plan' => 'ALL',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => true,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,9),
            'color' => 'Red',
            'title' => 'Terminated',
            'description' => 'Your record has been terminated.',
            'service_plan' => 'ALL',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => true,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,10),
            'color' => 'Red',
            'title' => 'Declaration Expired',
            'description' => 'Your insurance declaration is missing or expired.',
            'service_plan' => '463',
            'insurance_related' => true,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,11),
            'color' => 'Red',
            'title' => 'No Business Use',
            'description' => 'Your insurance has no business use clause. Please upload the required documentation through Insurance section of the Driver Profile Tab.',
            'service_plan' => '463',
            'insurance_related' => true,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,12),
            'color' => 'Red',
            'title' => 'Driver License Required',
            'description' => 'Driver License Required. Please upload your valid driver license (both front and back) through the License section of the Driver Profile Tab.',
            'service_plan' => '463',
            'insurance_related' => false,
            'license_related' => true,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,13),
            'color' => 'Green',
            'title' => 'No Insurance Compliance',
            'description' => 'No Insurance Compliance, for further instructions please go to the Insurance section of the Driver Profile Tab.',
            'service_plan' => 'FAVR',
            'insurance_related' => true,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,14),
            'color' => 'Green',
            'title' => 'No Vehicle Cost Compliance',
            'description' => 'No Vehicle Cost Compliance. To update and achieve compliance, please go to the Personal Vehicles section of the Driver Profile Tab.',
            'service_plan' => 'FAVR',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,15),
            'color' => 'Green',
            'title' => 'No Vehicle Age Compliance',
            'description' => 'No Vehicle Age Compliance. To update and achieve compliance, please go to the Personal Vehicles section of the Driver Profile Tab.',
            'service_plan' => 'FAVR',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,16),
            'color' => 'Green',
            'title' => 'No Odometer Compliance',
            'description' => 'No Vehicle Age Compliance. To update and achieve compliance, please go to the Personal Vehicles section of the Driver Profile Tab.',
            'service_plan' => 'FAVR',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,17),
            'color' => 'Green',
            'title' => 'No Depreciation Compliance',
            'description' => 'No Depreciation Compliance. To update and achieve compliance, please go to the Personal Vehicles section of the Driver Profile Tab.',
            'service_plan' => 'FAVR',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,18),
            'color' => 'Green',
            'title' => 'No Insurance In Grace Period',
            'description' => 'No Insurance Compliance, for further instructions please go to the Insurance section of the Driver Profile Tab.',
            'service_plan' => 'FAVR',
            'insurance_related' => true,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,19),
            'color' => 'Green',
            'title' => 'No Annual Declaration',
            'description' => 'No Annual Declaration. To update and achieve compliance, please go to the Annual IRS Odometer Declarations section of the Driver Profile Tab.',
            'service_plan' => 'FAVR',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,20),
            'color' => 'Red',
            'title' => 'Awaiting License',
            'description' => 'Driver\'s License documents required.  To upload, and for detailed instructions, please go to the License section Driver Profile Tab.  You will not receive reimbursement while in Red Light Status.',
            'service_plan' => '463',
            'insurance_related' => false,
            'license_related' => true,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,21),
            'color' => 'Red',
            'title' => 'Awaiting License',
            'description' => 'Driver\'s License documents required.  To upload, and for detailed instructions, please go to the License section Driver Profile Tab.  You will not receive reimbursement while in Red Light Status.',
            'service_plan' => '463',
            'insurance_related' => false,
            'license_related' => true,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,22),
            'color' => 'Green',
            'title' => 'No 5,000 Businesss Miles Limit',
            'description' => 'Due to the FAVR 5,000 miles limit, your reimbursement is taxable.',
            'service_plan' => 'FAVR',
            'insurance_related' => false,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,23),
            'color' => 'Red',
            'title' => 'License awaiting processing',
            'description' => 'Your uploaded license documents are awaiting processing.',
            'service_plan' => '463',
            'insurance_related' => false,
            'license_related' => true,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,24),
            'color' => 'Green',
            'title' => 'License awaiting processing',
            'description' => 'Your uploaded license documents are awaiting processing.',
            'service_plan' => 'FAVR',
            'insurance_related' => false,
            'license_related' => true,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,25),
            'color' => 'Red',
            'title' => 'Insurance awaiting processing',
            'description' => 'Your uploaded insurance documents are awaiting processing.',
            'service_plan' => '463',
            'insurance_related' => true,
            'license_related' => false,
            'active_related' => false,
        ]);

        DriverStreetLightStatus::create([
            'id' => pow(2,26),
            'color' => 'GREEN',
            'title' => 'Insurance awaiting processing',
            'description' => 'Your uploaded insurance documents are awaiting processing.',
            'service_plan' => 'FAVR',
            'insurance_related' => true,
            'license_related' => false,
            'active_related' => false,
        ]);

    }
}
