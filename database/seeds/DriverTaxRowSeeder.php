<?php

use Illuminate\Database\Seeder;

class DriverTaxRowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\DriverTaxRow::create(['name' => 'Q1', 'type' => 'D', 'order' => 1]);
        App\DriverTaxRow::create(['name' => 'Q1 old', 'type' => 'D', 'order' => 2]);
        App\DriverTaxRow::create(['name' => 'Q1 new', 'type' => 'D', 'order' => 3]);
        App\DriverTaxRow::create(['name' => 'Q2', 'type' => 'D', 'order' => 4]);
        App\DriverTaxRow::create(['name' => 'Q2 old', 'type' => 'D', 'order' => 5]);
        App\DriverTaxRow::create(['name' => 'Q2 new', 'type' => 'D', 'order' => 6]);
        App\DriverTaxRow::create(['name' => 'Q3', 'type' => 'D', 'order' => 7]);
        App\DriverTaxRow::create(['name' => 'Q3 old', 'type' => 'D', 'order' => 8]);
        App\DriverTaxRow::create(['name' => 'Q3 new', 'type' => 'D', 'order' => 9]);
        App\DriverTaxRow::create(['name' => 'Q4', 'type' => 'D', 'order' => 10]);
        App\DriverTaxRow::create(['name' => 'QUARTERLY TOTALS', 'type' => 'D', 'order' => 11]);
        App\DriverTaxRow::create(['name' => 'FAVR ADJUSTMENT (No 5,000 Miles Limit Reached)', 'type' => 'D', 'order' => 12]);
        App\DriverTaxRow::create(['name' => 'TOTAL', 'type' => 'S', 'order' => 13]);
    }
}
