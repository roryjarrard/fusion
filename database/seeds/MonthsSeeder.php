<?php

use Illuminate\Database\Seeder;

class MonthsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Month::create(['name' => 'January', 'short_name' => 'JAN']);
        App\Month::create(['name' => 'February', 'short_name' => 'FEB']);
        App\Month::create(['name' => 'March', 'short_name' => 'MAR']);
        App\Month::create(['name' => 'April', 'short_name' => 'APR']);
        App\Month::create(['name' => 'May', 'short_name' => 'MAY']);
        App\Month::create(['name' => 'June', 'short_name' => 'JUN']);
        App\Month::create(['name' => 'July', 'short_name' => 'JUL']);
        App\Month::create(['name' => 'August', 'short_name' => 'AUG']);
        App\Month::create(['name' => 'September', 'short_name' => 'SEP']);
        App\Month::create(['name' => 'October', 'short_name' => 'OCT']);
        App\Month::create(['name' => 'November', 'short_name' => 'NOV']);
        App\Month::create(['name' => 'December', 'short_name' => 'DEC']);
    }
}
