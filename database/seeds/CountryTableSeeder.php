<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Country::create(['name'=>'United States', 'short_name'=>'US']);
        App\Country::create(['name'=>'Canada', 'short_name'=>'CA']);
    }
}
