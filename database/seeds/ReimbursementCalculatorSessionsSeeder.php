<?php

use Illuminate\Database\Seeder;
use App\ReimbursementCalculatorSession;

class ReimbursementCalculatorSessionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * This seeder creates the single default record for reimbursement_calculator_sessions
     * When a user first goes to the calculator, a parameters object is constructed based on
     * the record in the reimbursement_calculator_sessions table with an id of 1.
     *
     * @return void
     */
    public function run()
    {
        App\ReimbursementCalculatorSession::create([
            'vehicle_id' => -1
        ]);
    }
}
