<?php

use Illuminate\Database\Seeder;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Driver',
            'slug' => 'driver',
            'description' => 'Company drivers generating mileage'
        ]);

        \HttpOz\Roles\Models\Role::create([
            'name' => 'Manager',
            'slug' => 'manager',
            'description' => 'Company division managers'
        ]);

        \HttpOz\Roles\Models\Role::create([
            'name' => 'Administrator',
            'slug' => 'administrator',
            'description' => 'Company custodians of the system'
        ]);

        \HttpOz\Roles\Models\Role::create([
            'name' => 'Super',
            'slug' => 'super',
            'description' => 'Just super, man'
        ]);
    }
}
