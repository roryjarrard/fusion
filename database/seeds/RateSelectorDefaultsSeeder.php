<?php

use Illuminate\Database\Seeder;

class RateSelectorDefaultsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * basic defaults. BUP is taken care of on the front end
         * as the default is plan dependant
         */
        App\RateSelectorDefaults::create([
            'user_id' => 1234,
            'vehicle_year' => 2018,
            'retention' => 4,
            'mileage_band_id' => 5,
            'territory_type_id' => 1,
            'capital_cost' => 'invoice',
            'resale_condition_id' => 3, // Trade-In Fair
            'country_id' => -1,
        ]);

    }
}
