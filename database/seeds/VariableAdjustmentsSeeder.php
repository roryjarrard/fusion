<?php

use Illuminate\Database\Seeder;

class VariableAdjustmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\VariableAdjustment::create([
            'name' => 'Snow Tires',
            'description' => '20% to maintenance',
            'displayed' => false,
        ]);

        App\VariableAdjustment::create([
            'name' => 'Rough Road',
            'description' => '30% to maintenance',
            'displayed' => false,
        ]);

        App\VariableAdjustment::create([
            'name' => 'Cold Weather Repairs',
            'description' => '25% to maintenance',
            'displayed' => false,
        ]);

        App\VariableAdjustment::create([
            'name' => 'Premium Fuel',
            'description' => '15% to fuel price',
            'displayed' => true,
        ]);

        App\VariableAdjustment::create([
            'name' => 'Cold Weather Fuel',
            'description' => '10% to fuel price',
            'displayed' => true,
        ]);

        App\VariableAdjustment::create([
            'name' => 'Cargo Weight',
            'description' => '10% to fuel price',
            'displayed' => true,
        ]);

        App\VariableAdjustment::create([
            'name' => 'Remote Area',
            'description' => '5% to fuel price',
            'displayed' => true,
        ]);
    }
}
