<?php

use Illuminate\Database\Seeder;

class CompanyReportLabelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\CompanyReportLabel::create([
            'key' => 'Business Unit',
            'company_id' => 45,
            'value' => 'Cost Center',
        ]);

        \App\CompanyReportLabel::create([
            'key' => 'Business Unit',
            'company_id' => 46,
            'value' => 'Cost Center',
        ]);

        \App\CompanyReportLabel::create([
            'key' => 'Business Unit',
            'company_id' => 83,
            'value' => 'Cost Center',
        ]);

        \App\CompanyReportLabel::create([
            'key' => 'Business Unit',
            'company_id' => 57,
            'value' => 'Area',
        ]);

        \App\CompanyReportLabel::create([
            'key' => 'Division',
            'company_id' => 57,
            'value' => 'Market',
        ]);
    }
}
