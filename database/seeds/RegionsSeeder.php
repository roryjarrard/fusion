<?php

use Illuminate\Database\Seeder;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Region::create(['name' => 'Western and Northwest']);
        App\Region::create(['name' => 'Central and Southeast']);
        App\Region::create(['name' => 'Northeast']);
    }
}
