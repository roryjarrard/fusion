<?php

use Illuminate\Database\Seeder;

class CompanyIndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\CompanyIndustry::create(['name'=>'Agriculture, Forestry & Fisheries', 'comment'=>'']);
        App\CompanyIndustry::create(['name'=>'Mining', 'comment'=>'']);
        App\CompanyIndustry::create(['name'=>'Construction', 'comment'=>'']);
        App\CompanyIndustry::create(['name'=>'Manufacturing', 'comment'=>'']);
        App\CompanyIndustry::create(['name'=>'Transportation, Communication & Public Utility', 'comment'=>'']);
        App\CompanyIndustry::create(['name'=>'Wholesale Trade', 'comment'=>'']);
        App\CompanyIndustry::create(['name'=>'Retail Trade', 'comment'=>'']);
        App\CompanyIndustry::create(['name'=>'Finance, Insurance & Real Estate', 'comment'=>'']);
        App\CompanyIndustry::create(['name'=>'Service Industries', 'comment'=>'']);
        App\CompanyIndustry::create(['name'=>'Public Administration', 'comment'=>'']);
    }
}
