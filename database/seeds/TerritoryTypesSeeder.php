<?php

use Illuminate\Database\Seeder;

class TerritoryTypesSeeder extends Seeder
{
    public function run()
    {
        App\TerritoryType::create(['id'=>1, 'name'=>'Home City']);
        App\TerritoryType::create(['id'=>2, 'name'=>'Home State']);
        App\TerritoryType::create(['id'=>3, 'name'=>'Multi City']);
        App\TerritoryType::create(['id'=>4, 'name'=>'Multi State']);
    }
}