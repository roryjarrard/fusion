<?php

use Illuminate\Database\Seeder;

class VehicleSuperCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\VehicleSuperCategory::create([
            'id' => 1,
            'name' => 'Small Cars',
        ]);

        App\VehicleSuperCategory::create([
            'id' => 2,
            'name' => 'Standard Cars',
        ]);

        App\VehicleSuperCategory::create([
            'id' => 3,
            'name' => 'Crossovers',
        ]);

        App\VehicleSuperCategory::create([
            'id' => 4,
            'name' => 'Pickups',
        ]);

        App\VehicleSuperCategory::create([
            'id' => 5,
            'name' => 'Executive',
        ]);

        App\VehicleSuperCategory::create([
            'id' => 6,
            'name' => 'SUVs',
        ]);

        App\VehicleSuperCategory::create([
            'id' => 7,
            'name' => 'Hybrids',
        ]);
    }
}
