<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTaxRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('driver_tax_rows')) { return; }
        Schema::create('driver_tax_rows', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->enum('type',['D','S'])->default('D');
            $table->smallInteger('order');
            $table->unique(['name']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_tax_rows', function (Blueprint $table) {
            $table->dropUnique('driver_tax_rows_name_unique');
        });
        Schema::dropIfExists('driver_tax_rows');
    }
}
