<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_banks')) { return; }
        Schema::create('company_banks', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->integer('bank_id')->unsigned();
            $table->foreign('bank_id')->references('id')->on('banks');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_banks', function (Blueprint $table) {
            $table->dropForeign('company_banks_company_id_foreign');
            $table->dropForeign('company_banks_bank_id_foreign');
        });
        Schema::dropIfExists('company_banks');
    }
}
