<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('manager_driver')) {
            return;
        }
        Schema::create('manager_driver', function (Blueprint $table) {
            $table->integer('manager_id')->unsigned();
            //$table->foreign('manager_id')->references('manager_id')->on('manager_profiles');

            $table->integer('driver_id')->unsigned();
            $table->foreign('driver_id')->references('user_id')->on('driver_profiles')->comment('the driver_profiles id is NOT the driver_id, just a primary key. user_id = driver_id!!!');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manager_driver', function (Blueprint $table) {
            //$table->dropForeign('manager_driver_manager_id_foreign');
            $table->dropForeign('manager_driver_driver_id_foreign');
        });
        Schema::dropIfExists('manager_driver');
    }
}
