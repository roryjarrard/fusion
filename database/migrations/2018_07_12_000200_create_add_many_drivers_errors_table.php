<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddManyDriversErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('add_many_drivers_errors')) { return; }
        Schema::create('add_many_drivers_errors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id')->unsigned();
            $table->foreign('file_id')->references('id')->on('add_many_drivers_files');
            $table->text('error')->nullable();
            $table->integer('row_number')->unsigned()->nullable();
            $table->text('row_content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('add_many_drivers_errors', function (Blueprint $table) {
            $table->dropForeign('add_many_drivers_errors_add_many_drivers_file_id_foreign');
        });

        Schema::dropIfExists('add_many_drivers_errors');
    }
}
