<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_options')) { return; }
        Schema::create('company_options', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->boolean('use_company_mileage_module')->default(true);
            $table->boolean('use_company_payments_module')->default(true);
            $table->boolean('use_company_license_module')->default(false);
            $table->boolean('use_company_insurance_module')->default(false);
            $table->boolean('use_company_invoicing_module')->default(false);
            $table->boolean('use_company_vehicles_module')->default(false);

            $table->date('launch_date')->nullable();
            $table->date('driver_passwords_date')->nullable();

            $table->enum( 'service_plan', ['463', 'FAVR', 'Canada', 'Consulting'])->nullable();
            $table->enum('account_management_responsibility', ['CarData', 'Client'])->nullable();

            $table->boolean('driver_can_edit_profile')->default(false);
            $table->enum('can_view_reimbursement_profile', ['All Users', 'Administrators', 'Super'])->default('Super');
            $table->enum('can_view_reimbursement_details', ['All Users', 'Administrators', 'Super'])->default('Super');

            $table->boolean('reports_display_manager_id')->default(false);
            $table->boolean('reports_display_job_title')->default(false);

            $table->boolean('use_cardata_online')->default(false);
            $table->boolean('use_start_stop_dates')->default(false);
            $table->boolean('use_insurance_dates')->default(false);
            $table->boolean('use_drivers_license_dates')->default(false);

            $table->boolean('use_stop_contract')->default(false);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_options', function (Blueprint $table) {
            $table->dropForeign('company_options_company_id_foreign');
        });
        Schema::dropIfExists('company_options');
    }
}
