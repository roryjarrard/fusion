<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_bank_accounts')) {
            Schema::create('user_bank_accounts', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->integer('country_id')->unsigned();
                $table->foreign('country_id')->references('id')->on('countries');
                $table->string('account_number', 500);
                $table->string('routing_number', 500)->nullable()->comment('US Only');
                $table->string('account_type')->nullable()->comment('US Only');
                $table->string('institution_number')->nullable()->comment('CAN Only');
                $table->string('branch_number')->nullable()->comment('CAN Only, also called transit number');
                $table->softDeletes();
                $table->timestamps();
                $table->unique(['user_id', 'country_id', 'deleted_at'], 'user_bank_accounts_user_id_country_id_deleted_at_unique');
                $table->index('user_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_bank_accounts', function (Blueprint $table) {
            $table->dropUnique('user_bank_accounts_user_id_country_id_deleted_at_unique');
        });
        Schema::dropIfExists('user_bank_accounts');
    }
}
