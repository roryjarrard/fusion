<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DriverInsuranceDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('driver_insurance_documents')) { return; }
        Schema::create('driver_insurance_documents', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('insurance_review_id')->unsigned();
            $table->foreign('insurance_review_id', 'fk_insurance_review_id')->references('id')->on('driver_insurance_reviews');

            /**
             * Note this is only nullable until we have started in on the system.
             * This is nullable now because we will convert "driver insurance reviews"
             * and then have a series of insurance reviews with ids, and I can then attach
             * the files to the single id and give the give the file a name of whatever I please
             */
            $table->string('filename')->nullable();

            $table->boolean('bodily_liability_per_person')->default(false);
            $table->boolean('bodily_liability_per_accident')->default(false);
            $table->boolean('property_damage')->default(false);
            $table->boolean('deductible')->default(false);
            $table->boolean('comprehensive_deductible')->default(false);
            $table->boolean('collision_deductible')->default(false);
            $table->boolean('medical')->default(false);
            $table->boolean('uninsured_per_person')->default(false);
            $table->boolean('uninsured_per_accident')->default(false);
            $table->boolean('public_liability')->default(false);
            $table->boolean('business_use')->default(false);

            $table->boolean('company_requirements')->default(false);
            $table->boolean('no_policy_term_dates')->default(false);
            $table->boolean('vehicle_match')->default(false);
            $table->boolean('document_illegible')->default(false);
            $table->boolean('document_current')->default(false);
            $table->boolean('name_missing')->default(false);
            $table->boolean('other')->default(false);

            $table->boolean('approved')->nullable()->default(null)->comment('null = not reviewed (just uploaded), 0 = rejected, 1 = accepted, 2 = partial');

            $table->integer('processor_id')->unsigned()->nullable()->default(null);
            $table->foreign('processor_id', 'fk_processor_id')->references('id')->on('users');

            $table->timestamp('created_at')->nullable();
            $table->timestamp('processing_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_insurance_documents', function (Blueprint $table) {
            $table->dropForeign('fk_insurance_review_id');
            $table->dropForeign('fk_processor_id');
        });
        Schema::dropIfExists('driver_insurance_documents');
    }
}
