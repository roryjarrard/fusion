<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxesFeesCATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('taxes_fees_ca')) { return; }
        Schema::create('taxes_fees_ca', function (Blueprint $table) {
            $table->smallInteger('year')->unsigned();

            $table->integer('state_province_id')->unsigned();
            $table->foreign('state_province_id')->references('id')->on('state_provinces');

            $table->decimal('sales_tax', 8, 3);
            $table->decimal('initial_registration', 8, 3);
            $table->decimal('annual_renewal', 8, 3);
            $table->decimal('annual_safety', 8, 3);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taxes_fees_ca', function (Blueprint $table) {
            $table->dropForeign('taxes_fees_ca_state_province_id_foreign');
        });
        Schema::dropIfExists('taxes_fees_ca');
    }
}
