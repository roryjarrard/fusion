<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_dates')) { return; }
        Schema::create('company_dates', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->smallInteger('mileage_reminder_day')->default(0);
            $table->smallInteger('second_mileage_reminder_day')->default(0);
            $table->smallInteger('mileage_lock_driver_day')->default(0);
            $table->smallInteger('mileage_lock_all_day')->default(0);
            $table->smallInteger('mileage_approval_end_day')->default(0);
            $table->smallInteger('direct_pay_day')->default(0);
            $table->smallInteger('pay_file_day')->default(0);
            $table->smallInteger('no_reimbursement_notification_day')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_dates', function (Blueprint $table) {
            $table->dropForeign('company_dates_company_id_foreign');
        });
        Schema::dropIfExists('company_dates');
    }
}
