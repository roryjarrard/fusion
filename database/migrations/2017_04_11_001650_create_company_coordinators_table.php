<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCoordinatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_coordinators')) { return; }
        Schema::create('company_coordinators', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            // these reference administrator_id on administrator_profiles, not foreign key because not primary or unique
            $table->integer('company_coordinator_id')->unsigned()->nullable();
            $table->string('company_coordinator_phone')->nullable();

            $table->integer('cardata_coordinator_id')->unsigned()->nullable();
            $table->string('cardata_coordinator_phone')->nullable();

            $table->integer('insurance_coordinator_id')->unsigned()->nullable();
            $table->string('insurance_coordinator_phone')->nullable();
            $table->string('insurance_fax_number')->nullable();

            $table->integer('license_coordinator_id')->unsigned()->nullable();
            $table->string('license_coordinator_phone')->nullable();
            $table->string('license_fax_number')->nullable();

            $table->integer('miroute_coordinator_id')->unsigned()->nullable();
            $table->string('miroute_coordinator_phone')->nullable();

            $table->integer('service_associate_id')->unsigned()->nullable();
            $table->integer('account_executive_id')->unsigned()->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_coordinators', function (Blueprint $table) {
            $table->dropForeign('company_coordinators_company_id_foreign');
        });
        Schema::dropIfExists('company_coordinators');
    }
}
