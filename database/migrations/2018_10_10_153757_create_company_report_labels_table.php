<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyReportLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_report_labels')) { return; }
        Schema::create('company_report_labels', function (Blueprint $table) {
            $table->string('key');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_report_labels', function (Blueprint $table) {
            $table->dropForeign('company_report_labels_company_id_foreign');
        });

        Schema::dropIfExists('company_report_labels');
    }
}
