<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZipCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('zip_codes')) { return; }
        Schema::create('zip_codes', function (Blueprint $table) {
            $table->string('zip_code', 5);

            $table->decimal('latitude',11,8);
            $table->decimal('longitude',11,8);

            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('state_provinces');
            $table->integer('county_id')->unsigned();
            $table->foreign('county_id')->references('id')->on('counties');
            $table->string('city_name',45);
            $table->integer('time_zone_id')->unsigned();
            $table->foreign('time_zone_id')->references('id')->on('time_zones');
            $table->timestamps();
            $table->primary('zip_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zip_codes', function (Blueprint $table) {
            $table->dropPrimary('zip_codes_primary');
            $table->dropForeign('zip_codes_state_id_foreign');
            $table->dropForeign('zip_codes_time_zone_id_foreign');
            $table->dropForeign('zip_codes_county_id_foreign');
        });
        Schema::dropIfExists('zip_codes');
    }
}
