<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleMileageAdjustment2009Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_mileage_adjustment_2009')) { return; }
        Schema::create('vehicle_mileage_adjustment_2009', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('mileage_range_id');
            $table->mediumInteger('value_range_id');
            $table->smallInteger('model_year');
            $table->smallInteger('resale_year');
            $table->integer('mileage_range_min');
            $table->integer('mileage_range_max');
            $table->integer('value_range_min');
            $table->integer('value_range_max');
            $table->enum('adjustment_type',['value','percent']);
            $table->decimal('adjustment', 7, 3);
            $table->index(['mileage_range_id','value_range_id','model_year','resale_year'], 'mileage_adjustment_index');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_mileage_adjustment_2009', function (Blueprint $table) {
            $table->dropindex('mileage_adjustment_index');
        });
        Schema::dropIfExists('vehicle_mileage_adjustment_2009');
    }
}
