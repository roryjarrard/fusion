<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('driver_taxes')) { return; }
        Schema::create('driver_taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->smallInteger('year')->unsigned();
            $table->unique(['user_id','year']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_taxes', function (Blueprint $table) {
            $table->dropForeign('driver_taxes_user_id_foreign');
            $table->dropUnique('driver_taxes_user_id_year_unique');
        });
        Schema::dropIfExists('driver_taxes');
    }
}
