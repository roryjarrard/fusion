<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('monthly_approvals')) { return; }
        Schema::create('monthly_approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->smallInteger('year')->unsigned();
            $table->tinyInteger('month');
            $table->decimal('file_fee',6,2)->default(0.00);
            $table->decimal('driver_fee', 5, 3)->default(0.00);
            $table->dateTime('approval_date')->nullable();
            $table->unique(['company_id','year','month']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_approvals');
    }
}
