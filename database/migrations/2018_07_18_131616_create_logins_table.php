<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('logins')) { return; }
        Schema::create('logins', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('username',255);
            $table->string('ip',15);
            $table->string('address',255)->nullable();;
            $table->string('platform',255)->nullable();;
            $table->enum('login_type',['cardata','mobile','fusion','miroute']);
            $table->string('access',80)->nullable();
            $table->dateTime('date_time')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->index('date_time');
            $table->index(['username','date_time']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logins', function (Blueprint $table) {
            $table->dropIndex('logins_date_time_index');
            $table->dropIndex('logins_username_date_time_index');
        });

        Schema::dropIfExists('logins');

    }
}
