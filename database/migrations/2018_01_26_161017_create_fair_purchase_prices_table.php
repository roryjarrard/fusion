<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFairPurchasePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('fair_purchase_prices')) { return; }
        Schema::create('fair_purchase_prices', function (Blueprint $table) {
            $table->integer('vehicle_id')->unsigned();
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
            $table->string('zip_postal'); // string for postal codes
            $table->double('adjustment_amount');
            $table->unique(['vehicle_id', 'zip_postal']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fair_purchase_prices', function (Blueprint $table) {
            $table->dropForeign('fair_purchase_prices_vehicle_id_foreign');
            $table->dropUnique('fair_purchase_prices_vehicle_id_zip_postal_unique');
        });
        Schema::dropIfExists('fair_purchase_prices');
    }
}
