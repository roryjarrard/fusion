<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyMileagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('monthly_mileages')) { return; }
        Schema::create('monthly_mileages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->smallInteger('year')->unsigned();
            $table->tinyInteger('month');
            $table->integer('business_mileage')->default(0);
            $table->integer('ending_odometer')->default(0);
            $table->integer('personal_mileage')->default(0);
            $table->timestamps();
            $table->unique(['user_id','year','month']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monthly_mileages', function (Blueprint $table) {
            $table->dropIndex('monthly_mileages_user_id_year_month_unique');
        });
        Schema::dropIfExists('monthly_mileages');
    }
}
