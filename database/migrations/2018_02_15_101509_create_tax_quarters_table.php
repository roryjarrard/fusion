<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxQuartersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tax_quarters')) { return; }
        Schema::create('tax_quarters', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('q1')->unsigned();
            $table->smallInteger('q2')->unsigned();
            $table->smallInteger('q3')->unsigned();
            $table->smallInteger('q4')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_quarters');
    }
}
