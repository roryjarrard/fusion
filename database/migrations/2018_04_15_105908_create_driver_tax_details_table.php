<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTaxDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('driver_tax_details')) { return; }
        Schema::create('driver_tax_details', function (Blueprint $table) {
            $table->integer('driver_tax_id')->unsigned();
            $table->foreign('driver_tax_id')->references('id')->on('driver_taxes');
            $table->integer('driver_tax_column_id')->unsigned();
            $table->foreign('driver_tax_column_id')->references('id')->on('driver_tax_columns');
            $table->integer('driver_tax_row_id')->unsigned();
            $table->foreign('driver_tax_row_id')->references('id')->on('driver_tax_rows');
            $table->decimal('amount',8,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_tax_details', function (Blueprint $table) {
            $table->dropForeign('driver_tax_details_driver_tax_id_foreign');
            $table->dropUnique('driver_tax_details_user_id_year_unique');
        });
        Schema::dropIfExists('driver_tax_details');
    }
}
