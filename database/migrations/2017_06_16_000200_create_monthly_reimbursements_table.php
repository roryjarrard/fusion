<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyReimbursementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('monthly_reimbursements')) { return; }
        Schema::create('monthly_reimbursements', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('driver_profile_id')->unsigned();
            $table->foreign('driver_profile_id')->references('id')->on('driver_profiles');

            $table->integer('fixed_status')->unsigned()->default(0);
            $table->smallInteger('year')->unsigned();
            $table->tinyInteger('month');
            $table->decimal('fixed_reimbursement',7,2)->default(0.0);
            $table->decimal('variable_reimbursement', 8, 4)->default(0.0);
            $table->decimal('adjustment_amount',7,2)->default(0.0);
            $table->decimal('vehicle_depreciation',7,2)->default(0.0);
            $table->decimal('taxable_amount', 7,2)->default(0.0);
            $table->boolean('within_grace_period')->default(false);
            $table->timestamps();

            $table->unique(['user_id', 'year', 'month']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monthly_reimbursements', function (Blueprint $table) {
            $table->dropForeign('monthly_reimbursements_user_id_foreign');
            $table->dropForeign('monthly_reimbursements_driver_profile_id_foreign');
        });
        Schema::dropIfExists('monthly_reimbursements');
    }
}
