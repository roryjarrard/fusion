<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyContractTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_contract_ticket')) { return; }
        Schema::create('company_contract_ticket', function (Blueprint $table) {

            $table->integer('contract_id')->unsigned();
            $table->foreign('contract_id')->references('contract_id')->on('company_contracts');

            $table->integer('ticket_id')->unsigned();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_contract_ticket', function (Blueprint $table) {
            $table->dropForeign(['contract_id']);
        });
        Schema::dropIfExists('company_contract_ticket');
    }
}
