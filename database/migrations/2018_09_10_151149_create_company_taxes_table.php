<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_taxes')) { return; }
        Schema::create('company_taxes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->boolean('use_quarterly_tax_adjustment')->default(false);
            $table->boolean('reports_display_monthly_tax_limit')->default(false);


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_taxes', function (Blueprint $table) {
            $table->dropForeign('company_taxes_company_id_foreign');
        });
        Schema::dropIfExists('company_taxes');
    }
}
