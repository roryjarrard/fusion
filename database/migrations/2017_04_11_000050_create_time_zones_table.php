<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('time_zones')) { return; }
        Schema::create('time_zones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short_name', 4);
            $table->string('name', 50);
            $table->tinyInteger('country_id')->comment('1=US, 2=Canada, 3=both. Use binary & in queries');
            $table->smallInteger('utc_offset');
            $table->timestamps();
            $table->unique('short_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('time_zones', function (Blueprint $table) {
            $table->dropIndex('time_zones_short_name_unique');
        });
        Schema::dropIfExists('time_zones');
    }
}
