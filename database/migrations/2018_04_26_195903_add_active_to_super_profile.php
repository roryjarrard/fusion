<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveToSuperProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('super_profiles') || Schema::hasColumn('super_profiles', 'active')) { return; }
        Schema::table('super_profiles', function($table)
        {
            $table->boolean('active')->after('user_id')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('super_profiles', function (Blueprint $table) {
            $table->dropIfExists('active');
        });
    }
}
