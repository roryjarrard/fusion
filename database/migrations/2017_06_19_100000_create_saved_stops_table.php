<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('saved_stops')) { return; }
        Schema::create('saved_stops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->boolean('active')->default(true)->comment('Allow companies to turn on and off');

            $table->string('stop_number')->nullable();
            $table->double('latitude');
            $table->double('longitude');
            $table->string('street');
            $table->string('street2')->nullable();
            $table->string('city');
            $table->string('zip_postal'); // string for postal codes
            $table->integer('state_province_id')->unsigned();
            $table->foreign('state_province_id')->references('id')->on('state_provinces');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');

            #TODO only used for conversion, remove these
            $table->integer('company_stop_id')->unsigned()->nullable()->comment("temporary, will be removed after conversion");
            $table->integer('driver_stop_id')->unsigned()->nullable()->comment("temporary, will be removed after conversion");

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saved_stops', function (Blueprint $table) {
            $table->dropForeign('saved_stops_company_id_foreign');
            $table->dropForeign('saved_stops_state_province_id_foreign');
            $table->dropForeign('saved_stops_country_id_foreign');
        });
        Schema::dropIfExists('saved_stops');
    }
}
