<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateSelectorDefaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('rate_selector_defaults')) { return; }
        Schema::create('rate_selector_defaults', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->comment('This is nullable for now as these are not yet user specific');
            $table->smallInteger('vehicle_year');
            $table->tinyInteger('country_id')->nullable();
            $table->smallInteger('state_province_id')->nullable();
            $table->string('zip_postal_code')->nullable();
            $table->tinyInteger('mileage_band_id')->default(5)->comment('Default 20,000 - 25,000');
            $table->tinyInteger('territory_type_id')->default(1)->comment('Default home_city');
            $table->string('territory_list')->nullable();
            $table->integer('vehicle_id')->nullable();
            $table->enum('capital_cost',['msrp','invoice','blue_book'])->nullable();
            $table->tinyInteger('resale_condition_id')->nullable();
            $table->tinyInteger('retention')->nullable();
            $table->decimal('business_use_percent', 4, 1)->nullable();
            $table->tinyInteger('capital_cost_adj')->default(0);
            $table->tinyInteger('fuel_economy_adj')->default(0);
            $table->tinyInteger('maintenance_adj')->default(0);
            $table->tinyInteger('repair_adj')->default(0);
            $table->tinyInteger('depreciation_adj')->default(0);
            $table->tinyInteger('insurance_adj')->default(0);
            $table->tinyInteger('fuel_price_adj')->default(0);
            $table->tinyInteger('resale_value_adj')->default(0);
            $table->tinyInteger('capital_cost_tax_adj')->default(0);
            $table->tinyInteger('monthly_payment_adj')->default(0);
            $table->tinyInteger('finance_cost_adj')->default(0);
            $table->tinyInteger('fee_renewal_adj')->default(0);
            $table->decimal('fixed_adj', 7, 2)->default(0);
            $table->decimal('variable_adj', 6, 2)->default(0);
            $table->enum( 'service_plan', ['463', 'FAVR', 'Canada', 'Consulting'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
