<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('notifications')) { return; }
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('template_slug')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('area');
            $table->enum('severity', ['low', 'medium', 'high'])->default('low');
            $table->string('title');
            $table->string('content');
            $table->string('note')->nullable()->comment('Custom note for the recipient');
            $table->string('icon')->nullable();
            $table->string('link')->nullable()->comment('JSON so we can set views in fusion, contains button_title, page, and subpage');
            $table->boolean('dismissible')->default(false);
            $table->enum('visible_to', ['all', 'driver', 'manager', 'admin', 'super'])->default('all');
            $table->boolean('read')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification', function (Blueprint $table) {
            $table->dropForeign('notifications_user_id_foreign');
        });

        Schema::dropIfExists('notifications');
    }
}
