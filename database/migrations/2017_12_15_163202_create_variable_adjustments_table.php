<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariableAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('variable_adjustments')) { return; }
        Schema::create('variable_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            //TODO: MAKE SEVERITY AND AREA ENUM WITH A DEFAULT
            $table->string('description');
            $table->boolean('displayed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variable_adjustments');
    }
}
