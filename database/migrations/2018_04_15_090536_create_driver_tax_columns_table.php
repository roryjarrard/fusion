<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTaxColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('driver_tax_columns')) { return; }
        Schema::create('driver_tax_columns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->unique(['name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_tax_columns', function (Blueprint $table) {
            $table->dropUnique('driver_tax_columns_name_unique');
        });
        Schema::dropIfExists('driver_tax_columns');
    }
}
