<?php /** @noinspection PhpUndefinedMethodInspection */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('driver_profiles')) { return; }
        Schema::create('driver_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('active')->default(1);
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('division_id')->unsigned()->nullable();
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->bigInteger('street_light_id')->unsigned();
            $table->string('employee_number')->nullable();
            $table->date('start_date')->nullable();
            $table->date('stop_date')->nullable();
            $table->string('job_title')->nullable();
            $table->integer('address_id')->unsigned()->nullable();
            $table->integer('vehicle_profile_id')->unsigned()->nullable()->comment('Vehicle Profile');
            $table->foreign('vehicle_profile_id')->references('id')->on('vehicle_profiles');
            $table->integer('personal_vehicle_id')->unsigned()->nullable()->comment('Vehicle Driven by Driver');
            $table->foreign('personal_vehicle_id')->references('id')->on('personal_vehicles');
            $table->string('cost_center')->nullable();
            $table->integer('mileage_band_id')->unsigned()->nullable();
            $table->foreign('mileage_band_id')->references('id')->on('mileage_bands');
            $table->integer('territory_type_id')->unsigned()->nullable()->comment('home city, home state, multi cities, multi states');
            $table->foreign('territory_type_id')->references('id')->on('territory_types');
            $table->string('territory_list')->nullable()->comment('list of cities or states');
            $table->integer('editing_user_id')->unsigned()->nullable()->comment('user who created (changed) this profile');
            $table->foreign('editing_user_id')->references('id')->on('users');
            $table->integer( 'fuel_city_id')->unsigned()->nullable();
            $table->foreign('fuel_city_id')->references('id')->on('fuel_cities');
            $table->boolean('car_policy_accepted')->default(false);
            $table->integer('reimbursement_detail_id')->unsigned()->nullable();
            $table->foreign('reimbursement_detail_id')->references('id')->on('reimbursement_details');

            $table->tinyInteger('miroute_status')->default(0)->commnent('0=not miroute, 1=miroute, 2=demo, 3=excluded ');

            $table->integer('insurance_review_id')->unsigned()->nullable();
            $table->foreign('insurance_review_id')->references('id')->on('driver_insurance_reviews');
            $table->integer('license_review_id')->unsigned()->nullable();
            //$table->foreign('license_review_id')->references('id')->on('driver_license_reviews');

            $table->date('next_insurance_review_date')->nullable();
            $table->date('next_license_review_date')->nullable();
            // TODO: add after insurances
            /*$table->integer('license_review_id')->unsigned()->nullable();
            $table->foreign('license_review_id')->references('id')->on('driver_license_reviews');*/
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['user_id', 'deleted_at'], 'driver_profiles_user_id_deleted_at_unique');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_profiles', function (Blueprint $table) {
            $table->dropForeign('driver_profiles_user_id_foreign');
            $table->dropForeign('driver_profiles_company_id_foreign');
            $table->dropForeign('driver_profiles_division_id_foreign');
            $table->dropForeign('driver_profiles_territory_type_id_foreign');
            $table->dropForeign('driver_profiles_fuel_city_id_foreign');
            $table->dropUnique('driver_profiles_user_id_deleted_at_unique');
            $table->dropForeign('driver_profiles_reimbursement_details_id_foreign');
            $table->dropForeign('driver_profiles_insurance_id_foreign');
            $table->dropForeign('driver_profiles_license_id_foreign');
            $table->dropIndex('user_id');
        });

        Schema::dropIfExists('driver_profiles');
    }
}
