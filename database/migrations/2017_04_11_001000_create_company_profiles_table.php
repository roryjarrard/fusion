<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_profiles')) { return; }
        Schema::create('company_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->string('website')->nullable();
            $table->integer('industry_id')->unsigned()->nullable(); // TODO: Import Industries
            $table->foreign('industry_id')->references('id')->on('company_industries');

            // address
            $table->string('street');
            $table->string('street2')->nullable();
            $table->string('city');
            $table->string('zip_postal');
            $table->integer('state_province_id')->unsigned()->nullable();
            $table->foreign('state_province_id')->references('id')->on('state_provinces');
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('time_zone_id')->unsigned()->nullable(); // TODO: Import Time Zones
            $table->foreign('time_zone_id')->references('id')->on('time_zones');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_profiles', function (Blueprint $table) {
            $table->dropForeign('company_profiles_company_id_foreign');
            $table->dropForeign('company_profiles_state_province_id_foreign');
            $table->dropForeign('company_profiles_country_id_foreign');
            $table->dropForeign('company_profiles_time_zone_id_foreign');
            $table->dropForeign('company_profiles_industry_id_foreign');
        });
        Schema::dropIfExists('company_profiles');
    }
}
