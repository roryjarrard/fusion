<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('divisions')) { return; }
        Schema::create('divisions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('market')->nullable();
            $table->string('name');
            $table->unique(['company_id', 'name', 'market']);
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['company_id','name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('divisions', function (Blueprint $table) {
            $table->dropForeign('divisions_company_id_foreign');
            $table->dropUnique('divisions_company_id_name_unique');
        });

        Schema::dropIfExists('divisions');
    }
}
