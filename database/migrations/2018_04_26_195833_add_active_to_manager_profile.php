<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveToManagerProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('manager_profiles') || Schema::hasColumn('manager_profiles', 'active')) { return; }
        Schema::table('manager_profiles', function($table)
        {
            $table->boolean('active')->after('manager_id')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manager_profiles', function (Blueprint $table) {
            $table->dropIfExists('active');
        });
    }
}
