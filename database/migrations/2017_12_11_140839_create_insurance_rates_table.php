<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('insurance_rates')) { return; }
        Schema::create('insurance_rates', function (Blueprint $table) {
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');

            $table->integer('state_province_id')->unsigned();
            $table->foreign('state_province_id')->references('id')->on('state_provinces');

            $table->integer('year')->unsigned();

            $table->string('zip_code', 5)->nullable();
            $table->foreign('zip_code')->references('zip_code')->on('zip_codes');

            $table->string('postal_code')->nullable();
            $table->foreign('postal_code')->references('postal_code')->on('postal_codes');

            $table->double('latitude');
            $table->double('longitude');

            $table->decimal('monthly_cost');
            $table->string('city_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurance_rates', function (Blueprint $table) {
            $table->dropForeign('insurance_rates_country_id_foreign');
            $table->dropForeign('insurance_rates_state_province_id_foreign');
            $table->dropForeign('insurance_rates_zip_code_foreign');
            $table->dropForeign('insurance_rates_postal_code_foreign');
        });
        Schema::dropIfExists('insurance_rates');
    }
}
