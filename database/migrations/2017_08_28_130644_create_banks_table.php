<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('banks')) { return; }
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->tinyInteger('status');
            $table->string('bank_name', 40)->nullable()->comment('Mandatory for US');
            $table->char('immediate_destination', 9)->nullable()->comment('Mandatory for US');
            $table->char('immediate_origin', 10)->nullable()->comment('Mandatory for US');
            $table->char('immediate_origin_name', 23)->nullable()->comment('Mandatory for US');
            $table->char('company_id', 10)->nullable()->comment('Mandatory for US');
            $table->char('company_name', 23)->nullable()->comment('Mandatory for US');
            $table->char('originating_dfi', 8)->nullable()->comment('Mandatory for US');
            $table->char('originator_id', 10)->nullable()->comment('Mandatory for US');
            $table->char('originator_name', 15)->nullable()->comment('Mandatory for US');
            $table->char('transit_number', 5)->nullable()->comment('Mandatory for Canada');
            $table->char('account_number', 11)->nullable()->comment('Mandatory for Canada');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}

