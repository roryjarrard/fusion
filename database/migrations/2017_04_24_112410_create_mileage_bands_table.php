<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMileageBandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('mileage_bands')) { return; }
        Schema::create('mileage_bands', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mr_id')->unsigned();
            $table->integer('mileage_start')->unsigned();
            $table->integer('mileage_end')->unsigned();
            $table->integer('kbb_mileage')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mileage_bands');
    }
}
