<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKBBVehiclesUSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('kbb_vehicles_us')) { return; }
        Schema::create('kbb_vehicles_us', function (Blueprint $table) {

            $table->integer('vehicle_id')->unsigned();
            $table->foreign('vehicle_id')->references('id')->on('vehicles');

            $table->smallInteger('model_year')->unsigned();
            $table->smallInteger('resale_year')->unsigned();
            $table->string('kbb_model_id',8)->nullable();
            $table->string('kbb_equipment_class_code',3)->nullable();
            $table->string('kbb_sequence_key',4)->nullable();
            $table->integer('resale_condition_id')->unsigned();
            $table->foreign('resale_condition_id')->references('id')->on('resale_conditions');
            $table->mediumInteger('base_cost');
            $table->mediumInteger('equipment_adj')->nullable();
            $table->smallInteger('region_id')->unsigned()->nullable();
            $table->integer('state_id')->unsigned()->nullable();
            $table->foreign('state_id')->references('id')->on('state_provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kbb_vehicles_us', function (Blueprint $table) {
            $table->dropForeign('kbb_vehicles_us_vehicle_id_foreign');
            $table->dropForeign('kbb_vehicles_us_resale_condition_id_foreign');
            $table->dropForeign('kbb_vehicles_us_state_province_id_foreign');
        });
        Schema::dropIfExists('kbb_vehicles_us');
    }
}
