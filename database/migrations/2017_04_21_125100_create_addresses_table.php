<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('addresses')) { return; }
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id'); // will refer to driver_profile user_id
            $table->string('street');
            $table->string('street2')->nullable();
            $table->string('city');
            $table->string('zip_postal'); // string for postal codes
            $table->integer('state_province_id')->unsigned();
            $table->foreign('state_province_id')->references('id')->on('state_provinces');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();

            $table->double('map_latitude')->nullable()->comment("Used to display map without altering lat&lng which could impact reimbursement");
            $table->double('map_longitude')->nullable()->comment("Used to display map without altering lat&lng which could impact reimbursement");

            $table->softDeletes();
            $table->timestamps();
            $table->index(['user_id', 'deleted_at'],'address_user_id_deleted_at_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropForeign('addresses_state_province_id_foreign');
            $table->dropForeign('addresses_country_id_foreign');
            $table->dropIndex('address_user_id_deleted_at_index');
        });

        Schema::dropIfExists('addresses');
    }
}
