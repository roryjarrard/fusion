<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKBBVehiclesCATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('kbb_vehicles_ca')) { return; }
        Schema::create('kbb_vehicles_ca', function (Blueprint $table) {

            $table->integer('vehicle_id')->unsigned();
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
            $table->smallInteger('model_year')->unsigned();
            $table->smallInteger('resale_year')->unsigned();
            $table->integer('mileage_band_id')->unsigned();
            $table->foreign('mileage_band_id')->references('id')->on('mileage_bands');
            $table->integer('resale_condition_id')->unsigned();
            $table->foreign('resale_condition_id')->references('id')->on('resale_conditions');
            $table->mediumInteger('base_cost');
            $table->smallInteger('region_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kbb_vehicles_ca', function (Blueprint $table) {
            $table->dropForeign('kbb_vehicles_ca_vehicle_id_foreign');
            $table->dropForeign('kbb_vehicles_ca_resale_condition_id_foreign');
            $table->dropForeign('kbb_vehicles_ca_mileage_band_id_foreign');
        });
        Schema::dropIfExists('kbb_vehicles_ca');
    }
}
