<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyApproversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('monthly_approvers')) { return; }
        Schema::create('monthly_approvers', function (Blueprint $table) {
            $table->integer('approval_id')->unsigned();
            $table->foreign('approval_id')->references('id')->on('monthly_approvals');
            $table->integer('manager_id')->unsigned()->nullable();
            $table->integer('administrator_id')->unsigned()->nullable();
            $table->integer('override_user_id')->unsigned()->nullable();
            $table->dateTime('approval_date')->nullable();
            $table->unique(['approval_id','manager_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_approvers');
    }
}
