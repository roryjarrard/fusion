<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_profiles')) { return; }
        Schema::create('vehicle_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_id')->unsigned();
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('name_postfix', 32);
            $table->tinyInteger('retention');
            $table->tinyInteger('max_vehicle_age');
            $table->enum('capital_cost',['msrp','invoice','blue_book']);
            $table->integer('resale_condition_id')->unsigned();
            $table->decimal('business_use_percent', 4, 1);
            $table->boolean('calculate_fixed')->default(true);
            $table->decimal('preset_fixed',7,2)->default(0.00);
            $table->boolean('calculate_variable')->default(true);
            $table->decimal('preset_variable',7,2)->default(0.00);
            $table->tinyInteger('capital_cost_adj')->default(0);
            $table->tinyInteger('fuel_economy_adj')->default(0);
            $table->tinyInteger('maintenance_adj')->default(0);
            $table->tinyInteger('repair_adj')->default(0);
            $table->tinyInteger('depreciation_adj')->default(0);
            $table->tinyInteger('insurance_adj')->default(0);
            $table->tinyInteger('fuel_price_adj')->default(0);
            $table->tinyInteger('resale_value_adj')->default(0);
            $table->tinyInteger('capital_cost_tax_adj')->default(0);
            $table->tinyInteger('monthly_payment_adj')->default(0);
            $table->tinyInteger('finance_cost_adj')->default(0);
            $table->tinyInteger('fee_renewal_adj')->default(0);
            $table->decimal('fixed_adj', 7, 2)->default(0);
            $table->decimal('variable_adj', 6, 2)->default(0);
            $table->string('fixed_adj_text', 60)->nullable();
            $table->string('variable_adj_text', 60)->nullable();
            $table->decimal('business_use_percent_back',4,1)->nullable();
            $table->boolean('proposed')->default(false)->comment('Is this vehicle profile profile proposed?');
            $table->integer('created_by')->unsigned()->nullable()->comment('This is the user_id of the creator. It is nullable because we do not have historical creators');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_profiles');
    }
}
