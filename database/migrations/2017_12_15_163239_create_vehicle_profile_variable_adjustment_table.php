<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleProfileVariableAdjustmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_profile_variable_adjustment')) { return; }
        Schema::create('vehicle_profile_variable_adjustment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_profile_id')->unsigned()->index();
            $table->foreign('vehicle_profile_id')->references('id')->on('vehicle_profiles');
            $table->integer('variable_adjustment_id')->unsigned()->index();
            $table->foreign('variable_adjustment_id', 'vehicle_profile_variable_adjustment_var_adj_id_foreign')->references('id')->on('variable_adjustments');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_profile_variable_adjustment');
    }
}
