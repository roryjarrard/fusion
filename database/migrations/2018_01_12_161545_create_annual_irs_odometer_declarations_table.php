<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualIrsOdometerDeclarationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('annual_irs_odometer_declarations')) { return; }
        Schema::create('annual_irs_odometer_declarations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('personal_vehicle_id')->unsigned()->nullable();
            $table->foreign('personal_vehicle_id')->references('id')->on('personal_vehicles');
            $table->integer('odometer')->nullable();
            $table->smallInteger('minimum_vehicle_year')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('annual_irs_odometer_declarations', function (Blueprint $table) {
            $table->dropForeign('annual_irs_odometer_declarations_user_id_foreign');
            $table->dropForeign('annual_irs_odometer_declarations_personal_vehicle_id_foreign');
        });

        Schema::dropIfExists('annual_irs_odometer_declarations');
    }
}
