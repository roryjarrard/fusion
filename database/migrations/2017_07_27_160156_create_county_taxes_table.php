<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountyTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('county_taxes')) {
            return;
        }
        Schema::create('county_taxes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('county_id')->unsigned();
            $table->foreign('county_id')->references('id')->on('counties');

            $table->integer('state_province_id')->unsigned();
            $table->foreign('state_province_id')->references('id')->on('state_provinces');

            $table->smallInteger('year')->unsigned();

            $table->decimal('county_sales_tax', 8, 3);
            $table->decimal('license_fee', 8, 3);
            $table->decimal('title_fee', 8, 3);
            $table->decimal('registration_fee', 8, 3);
            $table->decimal('renewal_fee', 8, 3);
            $table->decimal('other_fees', 8, 3);
            $table->index(['year', 'county_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('county_taxes', function (Blueprint $table) {
            $table->dropForeign('county_taxes_county_id_foreign');
            $table->dropIndex('county_taxes_year_county_id_index');
            $table->dropForeign('county_taxes_state_province_id_foreign');
        });
        Schema::dropIfExists('county_taxes');
    }
}
