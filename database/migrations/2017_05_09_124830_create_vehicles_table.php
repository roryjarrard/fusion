<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicles')) { return; }
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('vehicle_categories');
            $table->string('name');
            $table->smallInteger('year')->unsigned();
            $table->decimal('msrp', 9, 2)->default(0);
            $table->decimal('msrp_ca', 9, 2)->default(0);
            $table->decimal('invoice', 9, 2)->default(0);
            $table->decimal('invoice_ca', 9, 2)->default(0);
            $table->decimal('blue_book', 9, 2)->default(0);
            $table->decimal('epa_city', 4, 1)->default(0);
            $table->decimal('epa_highway', 4, 1)->default(0);
            $table->decimal('epa_city_ca', 4, 1)->default(0);
            $table->decimal('epa_highway_ca', 4, 1)->default(0);
            $table->tinyInteger('blended_vehicle')->default(0);
            $table->integer('mileage_group_id')->default(0);
            $table->integer('geo_group_id')->default(0);
            $table->integer('kbb_vehicle_id')->nullable();
            $table->tinyInteger('rank')->nullable();
            $table->tinyInteger('favr')->default(0);
            $table->integer('leading_vehicle')->unsigned();
            $table->integer('related_to')->unsigned();
            $table->integer('last_year_id')->unsigned()->nullable();
            $table->integer('display_order')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropForeign('vehicles_category_id_foreign');
        });

        Schema::dropIfExists('vehicles');
    }
}
