<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('interest_rates')) { return; }
        Schema::create('interest_rates', function (Blueprint $table) {
            $table->smallInteger('year')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->decimal('interest_rate',5,2);
            $table->timestamps();
            $table->unique(['country_id','year']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interest_rates', function (Blueprint $table) {
            $table->dropForeign('interest_rates_country_id_foreign');
            $table->dropUnique('interest_rates_country_id_year');
        });
        Schema::dropIfExists('interest_rates');
    }
}
