<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyMileagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('daily_mileages')) { return; }
        Schema::create('daily_mileages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->enum('last_state', ['start_day','where','driving','stopped','end_day','end_previous_day','day_ended'])->nullable()->default(null);
            $table->date('trip_date');
            $table->string('destination', 500)->nullable();
            $table->string('business_purpose', 500)->nullable();
            $table->integer('starting_odometer')->nullable();
            $table->integer('ending_odometer')->nullable();
            $table->integer('business_mileage')->nullable()->comment('calculated for commuter drivers as ending odometer - starting_odometer - commuter mileage');
            $table->integer('commuter_mileage')->nullable();
            $table->integer('personal_mileage')->nullable()->comment('ending odometer - starting odometer - business mileage. In old CarData AP_Personal.personal_2');
            $table->integer('gap_mileage')->nullable()->comment('personal between days. In old CarData AP_Personal.personal_1');
            $table->timestamps();
            $table->unique(['user_id','trip_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_mileages', function (Blueprint $table) {
            $table->dropUnique('daily_mileages_user_id_trip_date_unique');
        });
        Schema::dropIfExists('daily_mileages');

    }
}
