<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('change_trackers')) { return; }
        Schema::create('change_trackers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('target')->unsigned()->comment('id of any given model');

            $table->integer('author')->unsigned()->comment('id of user making the change');
            $table->foreign('author')->references('id')->on('users');

            $table->integer('company_id')->unsigned()->nullable()->comment('id of company if available for user or vehicile profile');
            $table->foreign('company_id')->references('id')->on('companies');

            $table->string('model');
            $table->string('attribute');
            $table->string('action');

            $table->string('old_value')->nullable()->comment('may be null on create');
            $table->string('new_value')->nullable()->comment('may be null on delete');

            $table->text('notes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('change_trackers', function (Blueprint $table) {
            $table->dropForeign('change_trackers_by_whom_foreign');
            $table->dropForeign('change_trackers_company_id_foreign');
        });
        Schema::dropIfExists('change_trackers');
    }
}
