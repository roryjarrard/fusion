<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoReimbursementReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('no_reimbursement_reasons')) { return; }
        Schema::create('no_reimbursement_reasons', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->smallInteger('year')->unsigned();
            $table->tinyInteger('month');
            $table->bigInteger('street_light_id')->unsigned();
            $table->unique(['user_id', 'year', 'month'],'no_reimbursement_reasons_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('no_reimbursement_reasons', function (Blueprint $table) {
            $table->dropUnique('no_reimbursement_reasons_unique');
            $table->dropForeign('no_reimbursement_reasons_user_id_foreign');
        });
        Schema::dropIfExists('no_reimbursement_reasons');
    }
}
