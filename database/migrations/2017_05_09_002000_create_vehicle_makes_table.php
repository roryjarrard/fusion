<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleMakesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_makes')) { return; }
        Schema::create('vehicle_makes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index('make_name_idx');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_makes', function (Blueprint $table) {
            $table->dropIndex('make_name_idx');
        });

        Schema::dropIfExists('vehicle_makes');

    }
}
