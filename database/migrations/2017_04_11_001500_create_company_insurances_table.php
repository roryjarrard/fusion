<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('company_insurances')) {
            Schema::create('company_insurances', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('company_id')->unsigned();
                $table->foreign('company_id')->references('id')->on('companies');

                $table->enum('insurance_responsibility', ['CarData', 'Client'])->nullable();
                $table->boolean('notify_insurance_expiry')->default(false);
                $table->boolean('remove_reimbursement_without_insurance')->default(false);
                $table->enum('insurance_review_interval', ['Annually', 'On_Renewal'])->nullable()->comment('On_Renewal can be considered on expiry');
                $table->smallInteger('insurance_review_day')->unsigned()->default(0)->comment('Required only if the insurance_review_interval is Annually');
                $table->smallInteger('insurance_review_month')->unsigned()->default(0)->comment('Required only if the insurance_review_interval is Annually');
                $table->string('insurance_fax_number')->nullable();

                // days and dates
                $table->date('initial_insurance_grace_end_date')->nullable()->comment('Date to begin insurance enforcement for company');
                $table->smallInteger('new_driver_insurance_grace_days')->unsigned()->default(0)->comment('Days from start date individual driver has to submit insurance');

                // amounts
                $table->mediumInteger('bodily_liability_per_person')->default(0);
                $table->mediumInteger('bodily_liability_per_accident')->default(0);
                $table->mediumInteger('property_damage')->default(0);
                $table->mediumInteger('deductible')->default(0);
                $table->mediumInteger('comprehensive_deductible')->default(0);
                $table->mediumInteger('collision_deductible')->default(0);
                $table->mediumInteger('medical')->default(0);
                $table->mediumInteger('uninsured_per_person')->default(0);
                $table->mediumInteger('uninsured_per_accident')->default(0);
                $table->mediumInteger('public_liability')->default(0)->comment('Canada only');
                $table->boolean('business_use')->default(false)->comment('Is business use insurance required');
                $table->boolean('additional_insured_additional_interest')->default(false);

                $table->softDeletes();
                $table->timestamps();
            });
            return;
        }

        if (!Schema::hasColumn('company_insurances', 'insurance_review_month'))
        {
            Schema::table('company_insurances', function($table)
            {
                $table->smallInteger('insurance_review_month')->after('insurance_review_interval')->unsigned()->default(0)->comment('Required only if the insurance_review_interval is Annually');
                $table->smallInteger('insurance_review_day')->after('insurance_review_month')->unsigned()->default(0)->comment('Required only if the insurance_review_interval is Annually');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_insurances', function (Blueprint $table) {
            $table->dropForeign('company_insurances_company_id_foreign');
        });
        Schema::dropIfExists('company_insurances');
    }
}
