<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverStopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('driver_stop')) { return; }
        Schema::create('driver_stop', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('saved_stop_id')->unsigned();
            $table->foreign('saved_stop_id')->references('id')->on('saved_stops');

            $table->date('effective_date')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_stop', function (Blueprint $table) {
            $table->dropForeign('driver_stop_user_id_foreign');
            $table->dropForeign('driver_stop_saved_stop_id_foreign');
        });
        Schema::dropIfExists('driver_stop');
    }
}
