<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('manager_profiles')) { return; }
        Schema::create('manager_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('manager_id')->unsigned();
            $table->boolean('active')->default(1);
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('manager_number')->nullable();
            $table->text('approval_description')->nullable();
            $table->integer('address_id')->unsigned()->nullable();
            $table->enum('assigned_drivers', ['all', 'none', 'some'])->default('none');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manager_profiles', function (Blueprint $table) {
            $table->dropForeign('manager_profiles_user_id_foreign');
            $table->dropForeign('manager_profiles_company_id_foreign');
        });
        Schema::dropIfExists('manager_profiles');
    }
}
