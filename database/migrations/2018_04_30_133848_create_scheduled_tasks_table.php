<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduledTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scheduled_tasks')) { return; }
        Schema::create('scheduled_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id')->unsigned()->nullable();
            $table->foreign('task_id')->references('id')->on('master_tasks');
            $table->string('name', 80);
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullable();
            $table->string('status',45);
            $table->string('short_description', 129)->nullable();;
            $table->mediumText('long_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheduled_tasks', function (Blueprint $table) {
            $table->dropForeign('scheduled_tasks_task_id_foreign');
        });
        Schema::dropIfExists('scheduled_tasks');
    }
}

// 'name', 'start_date', 'end_date', 'status', 'short_description', 'long_description', 'task_id'