<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenanceRepair2010sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('maintenance_repair_2010')) { return; }
        Schema::create('maintenance_repair_2010', function (Blueprint $table) {
            $table->integer('vehicle_id')->unsigned();
            $table->integer('state_province_id')->undigned();
            $table->smallInteger('mileage');
            $table->integer('maintenance');
            $table->integer('repair');
            $table->primary(['vehicle_id','state_province_id','mileage'], 'maintenance_repair_2010_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenance_repair_2010');
    }
}
