<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIrsStandardRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('irs_standard_rates')) { return; }
        Schema::create('irs_standard_rates', function (Blueprint $table) {
            $table->smallInteger('year')->unsigned();
            $table->tinyInteger('month');
            $table->decimal('rate', 6, 2);
            $table->unique(['year','month']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('irs_standard_rates');    }
}
