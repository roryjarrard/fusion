<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerDivisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('manager_division')) { return; }
        Schema::create('manager_division', function (Blueprint $table) {
            $table->integer('manager_id')->unsigned();
            //$table->foreign('manager_id')->references('manager_id')->on('manager_profiles');

            $table->integer('division_id')->unsigned();
            $table->foreign('division_id')->references('id')->on('divisions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manager_division', function (Blueprint $table) {
            //$table->dropForeign('manager_division_manager_id_foreign');
            $table->dropForeign('manager_division_division_id_foreign');
        });
        Schema::dropIfExists('manager_division');
    }
}
