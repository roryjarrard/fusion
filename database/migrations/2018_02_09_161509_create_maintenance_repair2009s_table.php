<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenanceRepair2009sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('maintenance_repair_2009')) { return; }
        Schema::create('maintenance_repair_2009', function (Blueprint $table) {
            $table->integer('vehicle_id')->unsigned();
            $table->integer('state_province_id')->unsigned();
            $table->smallInteger('mr_mileage_band_id');
            $table->smallInteger('maintenance_y1');
            $table->smallInteger('maintenance_y2');
            $table->smallInteger('maintenance_y3');
            $table->smallInteger('maintenance_y4');
            $table->smallInteger('maintenance_y5');
            $table->smallInteger('repair_y1');
            $table->smallInteger('repair_y2');
            $table->smallInteger('repair_y3');
            $table->smallInteger('repair_y4');
            $table->smallInteger('repair_y5');
            $table->primary(['vehicle_id','state_province_id','mr_mileage_band_id'], 'maintenance_repair_2009_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenance_repair2009');
    }
}
