<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverStreetLightStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * id, color, title, description, service_plan, insurance_related, license_related, created_at, updated_at
         */
        if (Schema::hasTable('driver_street_light_statuses')) { return; }
        Schema::create('driver_street_light_statuses', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->enum('color',['Yellow','Green','Red'])->default('Yellow');
            $table->string('title');
            $table->string('description');
            $table->enum('service_plan', ['463', 'FAVR', 'ALL']);
            $table->tinyInteger('insurance_related')->default(0);
            $table->tinyInteger('license_related')->default(0);
            $table->tinyInteger('active_related')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_street_light_statuses');
    }
}
