<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMirouteDriverOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('miroute_driver_options')) { return; }
        Schema::create('miroute_driver_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->smallInteger('access')->comment('0 - restricted from MI-Route, 1 - Demo access');
            $table->string('tracking_method', 40);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('miroute_driver_options', function (Blueprint $table) {
            $table->dropForeign('miroute_driver_options_user_id_foreign');
        });

        Schema::dropIfExists('miroute_driver_options');
    }
}
