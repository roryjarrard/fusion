<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tracking_changes')) { return; }
        Schema::create('tracking_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('changed_id')->unsigned();
            $table->integer('by_whom')->unsigned();
            $table->string('change_type', 60);
            $table->string('change_description', 60);
            $table->string('old_value', 445);
            $table->string('new_value', 445);
            $table->string('notes', 80);
            $table->dateTime('change_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_changes');
    }
}
