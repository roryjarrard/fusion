<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_fees')) { return; }
        Schema::create('company_fees', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->date('from_date');
            $table->date('to_date')->nullable();
            $table->enum('fee',['driver','file','invoice']);
            $table->decimal('amount', 5, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_fees', function (Blueprint $table) {
            $table->dropForeign('company_fees_company_id_foreign');
        });
        Schema::dropIfExists('company_fees');
    }
}
