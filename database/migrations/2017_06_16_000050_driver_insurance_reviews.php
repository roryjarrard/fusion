<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DriverInsuranceReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('driver_insurance_reviews')) { return; }
        Schema::create('driver_insurance_reviews', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id', 'fk_user_id')->references('id')->on('users');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id', 'fk_company_id')->references('id')->on('companies');

            $table->dateTime('next_review_date')->nullable();
            $table->dateTime('expiration_date')->nullable();

            $table->integer('approved')->nullable()->default(null)->comment('null = not reviewed, 0 = partial rejection, 1 = approved, 2 = rejected/historical');

            $table->integer('approved_by')->unsigned()->nullable()->default(null);
            $table->foreign('approved_by', 'fk_approved_by')->references('id')->on('users')->comment('final approver id');
            $table->dateTime('approval_date')->nullable();

            $table->boolean('bodily_liability_per_person')->default(false);
            $table->boolean('bodily_liability_per_accident')->default(false);
            $table->boolean('property_damage')->default(false);
            $table->boolean('deductible')->default(false);
            $table->boolean('comprehensive_deductible')->default(false);
            $table->boolean('collision_deductible')->default(false);
            $table->boolean('medical')->default(false);
            $table->boolean('uninsured_per_person')->default(false);
            $table->boolean('uninsured_per_accident')->default(false);
            $table->boolean('public_liability')->default(false);
            $table->boolean('business_use')->default(false);

            $table->boolean('no_policy_term_dates')->default(false);
            $table->boolean('vehicle_match')->default(false);
            $table->boolean('document_illegible')->default(false);
            $table->boolean('document_current')->default(false);
            $table->boolean('name_missing')->default(false);
            $table->boolean('other')->default(false);


            $table->text('comment')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->timestamp('processing_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_insurance_reviews', function (Blueprint $table) {
            $table->dropForeign('fk_user_id');
            $table->dropForeign('fk_company_id');
            $table->dropForeign('fk_approved_by');
        });
        Schema::dropIfExists('driver_insurance_reviews');
    }
}
