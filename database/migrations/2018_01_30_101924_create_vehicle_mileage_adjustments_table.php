<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleMileageAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_mileage_adjustments')) { return; }
        Schema::create('vehicle_mileage_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('miles');
            $table->mediumInteger('model_year');
            $table->mediumInteger('resale_year');
            $table->mediumInteger('price1');
            $table->mediumInteger('adjustment1');
            $table->mediumInteger('price2');
            $table->mediumInteger('adjustment2');
            $table->mediumInteger('price3');
            $table->mediumInteger('adjustment3');
            $table->mediumInteger('price4');
            $table->mediumInteger('adjustment4');
            $table->mediumInteger('price5');
            $table->mediumInteger('adjustment5');
            $table->mediumInteger('price6');
            $table->mediumInteger('adjustment6');
            $table->mediumInteger('price7');
            $table->mediumInteger('adjustment7');
            $table->mediumInteger('price8');
            $table->mediumInteger('adjustment8');
            $table->index(['miles','model_year','resale_year']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_mileage_adjustments', function (Blueprint $table) {
     //       $table->dropindex('vehicle_mileage_adjustments_miles_model_year_resale_year_index');
        });
        Schema::dropIfExists('vehicle_mileage_adjustments');
    }
}
