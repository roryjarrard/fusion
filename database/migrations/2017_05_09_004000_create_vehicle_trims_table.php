<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTrimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_trims')) { return; }
        Schema::create('vehicle_trims', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('year')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->foreign('model_id')->references('id')->on('vehicle_models');
            $table->smallInteger('order_by');
            $table->string('name');
            $table->timestamps();
            $table->index(['year','model_id','order_by']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_trims', function (Blueprint $table) {
            $table->dropIndex('vehicle_trims_model_id_foreign');
            $table->dropIndex('vehicle_trims_year_model_id_order_by_index');
        });

        Schema::dropIfExists('vehicle_trims');
    }
}
