<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('fuel_cities')) { return; }
        Schema::create('fuel_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('state_province_id')->unsigned();
            $table->foreign('state_province_id')->references('id')->on('state_provinces');
            $table->string('lookup_key');
            $table->float('latitude', 8, 4);
            $table->float('longitude', 8, 4);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fuel_cities', function (Blueprint $table) {
            $table->dropForeign('fuel_cities_country_id_foreign');
            $table->dropForeign('fuel_cities_state_province_id_foreign');
        });
        Schema::dropIfExists('fuel_cities');
    }
}
