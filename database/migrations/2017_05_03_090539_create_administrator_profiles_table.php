<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministratorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('administrator_profiles')) { return; }
        Schema::create('administrator_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('administrator_id')->unsigned();

            $table->boolean('active')->default(1);
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->string('administrator_number')->nullable()->comment('Employee number, not phone number');
            $table->string('administrator_phone')->nullable()->comment('Default phone, other numbers may override per functionality');

            $table->boolean('company_approver')->default(false)->nullable();
            $table->string('company_approver_phone')->nullable();
            $table->boolean('company_coordinator')->default(false)->nullable();
            $table->string('company_coordinator_phone')->nullable();

            $table->boolean('insurance_coordinator')->default(false)->nullable();
            $table->string('insurance_coordinator_phone')->nullable();

            $table->boolean('miroute_coordinator')->default(false)->nullable();
            $table->string('miroute_coordinator_phone')->nullable();

            $table->boolean('is_limited')->default(false)->nullable();
            $table->integer('address_id')->unsigned()->nullable();
            $table->enum('assigned_divisions', ['all','none','some'])->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('administrator_profiles', function (Blueprint $table) {
            $table->dropForeign('administrator_profiles_user_id_foreign');
            $table->dropForeign('administrator_profiles_company_id_foreign');
        });
        Schema::dropIfExists('administrator_profiles');
    }
}
