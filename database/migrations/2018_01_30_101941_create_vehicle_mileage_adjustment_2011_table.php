<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleMileageAdjustment2011Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_mileage_adjustment_2011')) { return; }
        Schema::create('vehicle_mileage_adjustment_2011', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('model_year');
            $table->smallInteger('resale_year');
            $table->integer('mileage_group_id');
            $table->integer('mileage_range_min');
            $table->integer('mileage_range_max');
            $table->enum('adjustment_type',['value','percent']);
            $table->double('adjustment',7,3);
            $table->index(['model_year','resale_year','mileage_group_id','mileage_range_min','mileage_range_max'], 'vehicle_mileage_adjustment_2011_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_mileage_adjustment_2011', function (Blueprint $table) {
            $table->dropindex('vehicle_mileage_adjustment_2011_idx');
        });
        Schema::dropIfExists('vehicle_mileage_adjustment_2011');
    }
}
