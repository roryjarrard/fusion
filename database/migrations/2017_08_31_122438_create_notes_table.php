<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('notes')) { return; }
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('note_categories');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->integer('driver_id')->unsigned()->nullable();
            $table->foreign('driver_id')->references('id')->on('users');

            $table->integer('vehicle_profile_id')->unsigned()->nullable();
            $table->foreign('vehicle_profile_id')->references('id')->on('vehicle_profiles');

            $table->string('title');
            $table->text('content');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->dropForeign('notes_category_id_foreign');
            $table->dropForeign('notes_user_id_foreign');
            $table->dropForeign('notes_company_id_foreign');
            $table->dropForeign('notes_driver_id_foreign');
            $table->dropForeign('notes_vehicle_profile_id_foreign');
        });
        Schema::dropIfExists('notes');
    }
}
