<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostalCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('postal_codes')) { return; }
        Schema::create('postal_codes', function (Blueprint $table) {
            $table->string('postal_code',6);
            $table->decimal('longitude',11,8);
            $table->decimal('latitude',11,8);
            $table->string('city_name',80);
            $table->integer('province_id')->unsigned();
            $table->foreign('province_id')->references('id')->on('state_provinces');
            $table->timestamps();
            $table->primary('postal_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('postal_codes', function (Blueprint $table) {
            $table->dropPrimary('postal_codes_primary');
            $table->dropForeign('postal_codes_province_id_foreign');
        });
        Schema::dropIfExists('postal_codes');
    }
}
