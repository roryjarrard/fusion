<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministratorDivisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('administrator_division')) { return; }
        Schema::create('administrator_division', function (Blueprint $table) {
            $table->integer('administrator_id')->unsigned();
            //$table->foreign('administrator_id')->references('administrator_id')->on('administrator_profiles');

            $table->integer('division_id')->unsigned();
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('administrator_division', function (Blueprint $table) {
            //$table->dropForeign('administrator_division_administrator_id_foreign');
            $table->dropForeign('administrator_division_division_id_foreign');
        });
        Schema::dropIfExists('administrator_division');
    }
}
