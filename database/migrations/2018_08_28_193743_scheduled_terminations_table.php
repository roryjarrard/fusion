<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScheduledTerminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scheduled_terminations')) {
            return;
        }
        Schema::create('scheduled_terminations', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->date('scheduled_date');
            $table->date('stop_date');
            $table->timestamp('executed_at')->nullable();
            $table->index(['scheduled_date', 'user_id']);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheduled_terminations', function (Blueprint $table) {
            $table->dropIndex('scheduled_terminations_scheduled_date_user_id_index');
        });

        Schema::dropIfExists('scheduled_terminations');

    }
}
