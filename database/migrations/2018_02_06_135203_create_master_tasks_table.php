<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('master_tasks')) { return; }
        Schema::create('master_tasks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('script');
            $table->string('description');

            $table->enum('interval', ['hourly', 'daily', 'weekly', 'monthly', 'annually']);

            $table->string('minute')->nullable();
            $table->string('hour')->nullable();
            $table->string('day')->nullable();
            $table->enum('day_of_week', ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'])->nullable();
            $table->string('month')->nullable();

            $table->dateTime('last_run')->nullable();
            $table->string('last_status')->nullable();


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_tasks');
    }
}
