<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTaxQuartersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_tax_quarters')) { return; }
        Schema::create('company_tax_quarters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tax_quarter_id')->unsigned();
            $table->foreign('tax_quarter_id')->references('id')->on('tax_quarters');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->smallInteger('valid_until_year')->nullable();
            $table->timestamps();
            $table->unique(['company_id','created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


        Schema::table('company_tax_quarters', function (Blueprint $table) {
            $table->dropForeign('company_tax_quarters_tax_quarter_id_foreign');
            $table->dropForeign('company_tax_quarters_company_id_foreign');
        });

        Schema::dropIfExists('company_tax_quarters');
    }
}
