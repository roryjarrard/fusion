<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedStopCompanyContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('saved_stop_company_contract')) { return; }
        Schema::create('saved_stop_company_contract', function (Blueprint $table) {

            $table->integer('saved_stop_id')->unsigned();
            $table->foreign('saved_stop_id')->references('id')->on('saved_stops');

            $table->integer('contract_id')->unsigned();
            $table->foreign('contract_id')->references('contract_id')->on('company_contracts');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saved_stop_company_contract', function (Blueprint $table) {
            $table->dropForeign(['saved_stop_id']);
            $table->dropForeign(['contract_id']);
        });
        Schema::dropIfExists('saved_stop_company_contract');
    }
}
