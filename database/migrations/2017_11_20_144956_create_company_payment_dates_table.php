<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPaymentDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_payment_dates')) { return; }
        Schema::create('company_payment_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('year')->unsigned();
            $table->integer('month')->unsigned();
            $table->date('banking_lock_date')->comment('Last date for changes to driver banking information');
            $table->date('driver_payment_date');
            $table->date('received_date')->comment('Date funds for reimbursement are received from client');
            $table->date('approved_date')->nullable();
            $table->timestamps();

            $table->unique(['company_id', 'year', 'month'], 'company_payment_dates_company_id_year_month_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_payment_dates', function (Blueprint $table) {
            $table->dropUnique('company_payment_dates_company_id_year_month_unique');
        });
        Schema::dropIfExists('company_payment_dates');
    }
}
