<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivisionStopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('division_stop')) { return; }
        Schema::create('division_stop', function (Blueprint $table) {
            $table->integer('division_id')->unsigned();
            $table->foreign('division_id')->references('id')->on('divisions');

            $table->integer('saved_stop_id')->unsigned();
            $table->foreign('saved_stop_id')->references('id')->on('saved_stops');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saved_stops', function (Blueprint $table) {
            $table->foreign('division_stop_division_id_foreign');
            $table->foreign('division_stop_saved_stop_id_foreign');
        });
        Schema::dropIfExists('division_stop');

    }
}
