<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyAdministrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_administrations')) { return; }
        Schema::create('company_administrations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->enum('management', ['CarData', 'Client']);
            $table->enum('driver_calls_and_emails', ['CarData', 'Client']);
            $table->enum('driver_changes', ['CarData', 'Client']);
            $table->enum('account_maintenance', ['CarData', 'Client']);
            $table->enum('monthly_report_review', ['CarData', 'Client']);
            $table->enum('traffic_light_status', ['CarData', 'Client']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_administrations', function (Blueprint $table) {
            $table->dropForeign('company_administrations_company_id_foreign');
        });
        Schema::dropIfExists('company_administrations');
    }
}
