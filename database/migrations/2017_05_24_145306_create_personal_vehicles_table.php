<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('personal_vehicles')) { return; }
        Schema::create('personal_vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('make_id')->unsigned()->nullable();
            $table->foreign('make_id')->references('id')->on('vehicle_makes');
            $table->integer('model_id')->unsigned()->nullable();
            $table->foreign('model_id')->references('id')->on('vehicle_models');
            $table->integer('trim_id')->unsigned()->nullable();
            $table->foreign('trim_id')->references('id')->on('vehicle_trims');
            $table->string('note')->nullable()->comment('store original make and model if we could not find such names in vehicle make or/and models');
            $table->smallInteger('year')->unsigned();
            $table->integer('odometer')->nullable();
            $table->integer('old_vehicle_odometer')->unsigned()->nullable();
            $table->integer('vehicle_cost')->unsigned()->nullable();
            $table->smallInteger('depreciation_method')->nullable();
            $table->dateTime('age_changed')->nullable();
            $table->dateTime('cost_changed')->nullable();
            $table->dateTime('odometer_changed')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personal_vehicles', function (Blueprint $table) {
            $table->dropForeign('personal_vehicles_user_id_foreign');
            $table->dropForeign('personal_vehicles_make_id_foreign');
            $table->dropForeign('personal_vehicles_model_id_foreign');
        });

        Schema::dropIfExists('personal_vehicles');
    }
}
