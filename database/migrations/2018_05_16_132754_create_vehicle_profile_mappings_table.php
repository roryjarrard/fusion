<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleProfileMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_profile_mappings')) { return; }
        Schema::create('vehicle_profile_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('old_vehicle_profile_id')->unsigned();
            $table->foreign('old_vehicle_profile_id')->references('id')->on('vehicle_profiles');
            $table->integer('proposed_vehicle_profile_id')->unsigned();
            $table->foreign('proposed_vehicle_profile_id')->references('id')->on('vehicle_profiles');
            $table->boolean('approved')->nullable()->comment('Approval status of the mapping. Null is awaiting approval, 0 is rejected, 1 is approved.');
            $table->integer('approved_by')->unsigned()->nullable()->comment('The user_id of the super who approved this mapping.');
            $table->foreign('approved_by')->references('id')->on('users');
            $table->string('note')->nullable();
            $table->integer('created_by')->unsigned()->nullable()->comment('The user_id of the super who created this mapping.');
            $table->foreign('created_by')->references('id')->on('users');
            $table->integer('applied_by')->unsigned()->nullable()->comment('The user_id of the super who applied this mapping.');
            $table->foreign('applied_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_profile_mappings', function (Blueprint $table) {
            $table->dropForeign('vehicle_profile_mappings_company_id_foreign');
            $table->dropForeign('vehicle_profile_mappings_old_vehicle_profile_id_foreign');
            $table->dropForeign('vehicle_profile_mappings_proposed_vehicle_profile_id_foreign');
            $table->dropForeign('vehicle_profile_mappings_approved_by_foreign');
            $table->dropForeign('vehicle_profile_mappings_created_by_foreign');
            $table->dropForeign('vehicle_profile_mappings_applied_by_foreign');
        });

        Schema::dropIfExists('vehicle_profile_mappings');
    }
}
