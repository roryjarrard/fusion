<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('fuel_prices')) { return; }
        Schema::create('fuel_prices', function (Blueprint $table) {
            $table->integer('fuel_city_id')->unsigned();
            $table->foreign('fuel_city_id')->references('id')->on('fuel_cities');
            $table->decimal('price', 6, 3);
            $table->date('updated_at');

            $table->unique(['fuel_city_id', 'updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fuel_prices', function (Blueprint $table) {
            $table->dropForeign('fuel_prices_fuel_city_id_foreign');
        });
        Schema::dropIfExists('fuel_prices');
    }
}
