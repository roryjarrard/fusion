<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('super_profiles')) { return; }
        Schema::create('super_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
$table->boolean('active')->default(false);
            // permissions
            $table->boolean('is_account_executive')->default(false);
            $table->boolean('is_insurance_processor')->default(false);
            $table->boolean('is_license_processor')->default(false);
            $table->boolean('is_payment_processor')->default(false);
            $table->boolean('is_service_associate')->default(false);
            $table->boolean('is_vehicle_profile_approver')->default(false);
            $table->boolean('is_announcement_approver')->default(false);
            $table->boolean('can_view_eft')->default(false);
            $table->boolean('can_view_login_information')->default(false);
            $table->boolean('can_view_database_tables')->default(false);
            $table->boolean('is_database_maintainer')->default(false);
            $table->boolean('is_bank_maintainer')->default(false);
            $table->boolean('is_caa_maintainer')->default(false);
            $table->boolean('is_tasks_maintainer')->default(false);
            $table->boolean('has_all_permissions')->default(false);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('super_profiles', function (Blueprint $table) {
            $table->dropForeign('super_profiles_user_id_foreign');
            $table->dropForeign('super_profiles_company_id_foreign');
        });
        Schema::dropIfExists('super_profiles');
    }
}
