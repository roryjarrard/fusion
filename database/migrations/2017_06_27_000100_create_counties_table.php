<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('counties')) { return; }
        Schema::create('counties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('state_provinces');
            $table->integer('aaa_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('counties', function (Blueprint $table) {
            $table->dropForeign('counties_state_id_foreign');
        });
        Schema::dropIfExists('counties');
    }
}
