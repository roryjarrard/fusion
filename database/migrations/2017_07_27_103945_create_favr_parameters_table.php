<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavrParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('favr_parameters')) { return; }

        Schema::create('favr_parameters', function (Blueprint $table) {
            $table->increments('year');
            $table->mediumInteger('vehicle_cost_limit');
            $table->mediumInteger('truck_cost_limit');
            $table->decimal('standard_rate', 4, 3);
            $table->decimal('vehicle_cost_percentage', 3, 2);
            $table->decimal('profile_vehicle_percentage', 3, 2);
            $table->decimal('maximum_tax_rate', 5, 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favr_parameters');
    }
}
