<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_vehicles')) { return; }
        Schema::create('company_vehicles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->enum('vehicle_responsibility', ['CarData', 'Client', 'Declined'])->nullable();
            $table->boolean('enforce_vehicle_age')->default(false);
            $table->boolean('enforce_car_policy_acceptance')->default(false);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_vehicles', function (Blueprint $table) {
            $table->dropForeign('company_vehicles_company_id_foreign');
        });
        Schema::dropIfExists('company_vehicles');
    }
}
