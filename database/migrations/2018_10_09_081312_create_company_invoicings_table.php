<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInvoicingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_invoicings')) { return; }
        Schema::create('company_invoicings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->enum('invoicing_interval', ['Annually', 'Monthly'])->nullable();
            $table->decimal('per_driver_fee', 5, 2)->default(0.00);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_invoicings', function (Blueprint $table) {
            $table->dropForeign('company_invoicings_company_id_foreign');
        });
        Schema::dropIfExists('company_invoicings');
    }
}
