<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReimbursementCalculatorSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reimbursement_calculator_sessions')) { return; }
        Schema::create('reimbursement_calculator_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->comment('This is nullable for the default record, but may be user specifc');
            $table->string('company_name')->nullable();
            $table->smallInteger('vehicle_year')->nullable()->comment('Will default to most current year for which we have vehicles');
            $table->smallInteger('fuel_year')->nullable();
            $table->smallInteger('fuel_month_id')->nullable();
            $table->tinyInteger('country_id')->default(1);
            $table->smallInteger('state_province_id')->default(44)->comment('Default to Texas');
            $table->string('zip_postal_code')->default(77001)->comment('Default to Houston');
            $table->string('city_name')->default('Houston')->comment('Default to Houston');
            $table->tinyInteger('mileage_band_id')->default(5)->comment('Default 20,000 - 25,000');
            $table->tinyInteger('territory_type_id')->default(1)->comment('Default home_city');
            $table->string('territory_list')->nullable();
            $table->integer('vehicle_id')->nullable()->comment('Default will be variable based on front end and year');
            $table->enum('capital_cost',['msrp','invoice','blue_book'])->nullable()->default('invoice');
            $table->tinyInteger('resale_condition_id')->nullable()->default(3)->comment('Default to Trade-In Fair');
            $table->tinyInteger('retention')->nullable()->default(4)->comment('Default to 4 years');
            $table->decimal('business_use_percent', 4, 1)->nullable()->default(75)->comment('Default to 75% FAVR');
            $table->tinyInteger('capital_cost_adj')->default(0);
            $table->tinyInteger('fuel_economy_adj')->default(0);
            $table->tinyInteger('maintenance_adj')->default(0);
            $table->tinyInteger('repair_adj')->default(0);
            $table->tinyInteger('depreciation_adj')->default(0);
            $table->tinyInteger('insurance_adj')->default(0);
            $table->tinyInteger('fuel_price_adj')->default(0);
            $table->tinyInteger('resale_value_adj')->default(0);
            $table->tinyInteger('capital_cost_tax_adj')->default(0);
            $table->tinyInteger('monthly_payment_adj')->default(0);
            $table->tinyInteger('finance_cost_adj')->default(0);
            $table->tinyInteger('fee_renewal_adj')->default(0);
            $table->decimal('fixed_adj', 7, 2)->default(0);
            $table->decimal('variable_adj', 6, 2)->default(0);
            $table->enum( 'service_plan', ['463', 'FAVR', 'Canada', 'Consulting'])->nullable()->default('FAVR');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
