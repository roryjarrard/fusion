<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('company_licenses')) {
            Schema::create('company_licenses', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('company_id')->unsigned();
                $table->foreign('company_id')->references('id')->on('companies');


                $table->enum('license_responsibility', ['CarData', 'Client'])->nullable();
                $table->enum('license_review_interval', ['Annually', 'On_Renewal'])->nullable()->comment('On_Renewal can be considered on expiry');
                $table->smallInteger('license_review_day')->unsigned()->default(0)->comment('Required only if the license_review_interval is Annually');
                $table->smallInteger('license_review_month')->unsigned()->default(0)->comment('Required only if the license_review_interval is Annually');

                $table->boolean('notify_license_expiry')->default(false);
                $table->string('license_fax_number')->nullable();

                // days and dates
                $table->date('initial_license_enforcement_date')->nullable()->comment('Date to begin license enforcement for company');
                $table->smallInteger('new_driver_license_grace_days')->unsigned()->default(0)->comment('Days from start date individual driver has to submit license');

                $table->softDeletes();
                $table->timestamps();
            });
            return;
        }

        if (!Schema::hasColumn('company_licenses', 'license_review_month')) {
            Schema::table('company_licenses', function ($table) {
                $table->smallInteger('license_review_month')->after('license_review_interval')->unsigned()->default(0)->comment('Required only if the license_review_interval is Annually');
                $table->smallInteger('license_review_day')->after('license_review_month')->unsigned()->default(0)->comment('Required only if the license_review_interval is Annually');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_licenses', function (Blueprint $table) {
            $table->dropForeign('company_licenses_company_id_foreign');
        });
        Schema::dropIfExists('company_licenses');
    }
}
