<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('notification_templates')) { return; }
        Schema::create('notification_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('creator')->nullable();
            $table->string('area');
            $table->enum('severity', ['low', 'medium', 'high'])->default('low');
            $table->string('title');
            $table->string('content');
            $table->string('icon')->nullable();
            $table->string('link')->nullable()->comment('JSON so we can set views in fusion, contains button_title, page, and subpage');
            $table->boolean('dismissible')->default(false);
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_templates');
    }
}
