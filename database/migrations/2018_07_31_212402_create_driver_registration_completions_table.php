<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverRegistrationCompletionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('driver_registration_completions')) { return; }
        Schema::create('driver_registration_completions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('information_completed')->unsigned()->nullable();
            $table->boolean('address_completed')->unsigned()->nullable();
            $table->boolean('vehicle_completed')->unsigned()->nullable();
            $table->boolean('insurance_completed')->unsigned()->nullable();
            $table->boolean('license_completed')->unsigned()->nullable();
            $table->boolean('banking_completed')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
