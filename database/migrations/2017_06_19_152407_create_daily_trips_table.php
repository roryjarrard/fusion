<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('daily_trips')) { return; }
        Schema::create('daily_trips', function (Blueprint $table) {
            $table->increments('id');
            $table->date('trip_date')->nullable();
            $table->integer('saved_stop_id')->unsigned()->nullable();
            $table->foreign('saved_stop_id')->references('id')->on('saved_stops');
            $table->integer('daily_mileage_id')->unsigned();
            $table->foreign('daily_mileage_id')->references('id')->on('daily_mileages');
            $table->integer('sequence_number')->nullable();
            $table->enum('event', ['start', 'stopped', 'end']);
            $table->string('business_purpose')->nullable();
            $table->decimal('mileage_original', 8,2);
            $table->decimal('mileage_adjusted', 8,2)->nullable();
            $table->string('adjustment_comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_trips', function (Blueprint $table) {
            $table->dropForeign('daily_trips_saved_stop_id_foreign');
            $table->dropForeign('daily_trips_daily_mileage_id_foreign');
        });
        Schema::dropIfExists('daily_trips');
    }
}
