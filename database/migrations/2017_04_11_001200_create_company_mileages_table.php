<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyMileagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('company_mileages')) return;
        Schema::create('company_mileages', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->enum('mileage_entry_method',['Mi-Route', 'Monthly', 'Monthly Personal', 'Daily Log', 'Weekly Log', 'Daily Commuter'])->nullable();
            $table->enum('mileage_band_audit_method', ['Business to Annual', 'Business Mileage'])->nullable();
            $table->boolean('client_views_mileage_band_audit')->default(false);

            // days and dates
            $table->date('miroute_transition_date')->nullable();

            $table->smallInteger('new_driver_mileage_grace_days')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_mileages', function (Blueprint $table) {
            $table->dropForeign('company_mileages_company_id_foreign');
        });
        Schema::dropIfExists('company_mileages');
    }
}
