<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleCategoryInsuranceAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_category_insurance_adjustments')) { return; }
        Schema::create('vehicle_category_insurance_adjustments', function (Blueprint $table) {
            $table->integer('vehicle_category_id')->unsigned();
            $table->foreign('vehicle_category_id', 'vcat_id_foreign')->references('id')->on('vehicle_categories');
            $table->integer('year')->unsigned();
            $table->decimal('insurance_adjustment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_category_insurance_adjustments', function (Blueprint $table) {
            $table->dropForeign('vcat_id_foreign');
        });
        Schema::dropIfExists('vehicle_category_insurance_adjustments');
    }
}
