<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleProfileApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicle_profile_approvals')) { return; }
        Schema::create('vehicle_profile_approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_profile_id')->unsigned();
            $table->foreign('vehicle_profile_id')->references('id')->on('vehicle_profiles');
            $table->boolean('approved')->nullable();
            $table->integer('approver_id')->unsigned()->nullable();
            $table->foreign('approver_id')->references('id')->on('users')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_profile_approvals', function (Blueprint $table) {
            $table->dropForeign('vehicle_profile_approvals_approver_id_foreign');
            $table->dropForeign('vehicle_profile_approvals_vehicle_profile_id_foreign');
        });

        Schema::dropIfExists('vehicle_profile_approvals');
    }
}
