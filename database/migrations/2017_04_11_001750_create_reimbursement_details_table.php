<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReimbursementDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reimbursement_details')) { return; }
        Schema::create('reimbursement_details', function (Blueprint $table) {
            $table->increments('id');
            $table->double('capital_cost');
            $table->smallInteger('capital_cost_adj')->default(0);
            $table->double('resale_value');
            $table->smallInteger('resale_value_adj')->default(0);
            $table->double('tax');
            $table->smallInteger('tax_adj')->default(0);
            $table->double('monthly_payment')->nullable();
            $table->smallInteger('monthly_payment_adj')->default(0);
            $table->double('depreciation');
            $table->smallInteger('depreciation_adj')->default(0);
            $table->double('finance_cost')->nullable();
            $table->smallInteger('finance_cost_adj')->default(0);
            $table->double('insurance');
            $table->string('insurance_city', 40)->nullable();
            $table->smallInteger('insurance_adj')->default(0);
            $table->double('fee_renewal');
            $table->smallInteger('fee_renewal_adj')->default(0);
            $table->double('fuel_economy');
            $table->smallInteger('fuel_economy_adj')->default(0);
            $table->smallInteger('fuel_price_adj')->default(0);
            $table->double('maintenance');
            $table->smallInteger('maintenance_adj')->default(0);
            $table->double('repair');
            $table->smallInteger('repair_adj')->default(0);
            $table->decimal('fixed_adj',7,2)->default(0);
            $table->decimal('cents_per_mile_adj',7,2)->default(0);
            $table->decimal('fixed_reimbursement',7,2)->default(0);
            $table->decimal('cents_per_mile',8,4)->default(0);
            $table->decimal('business_use_percent', 4, 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reimbursement_details');
    }
}
