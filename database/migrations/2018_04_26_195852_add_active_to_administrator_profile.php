<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveToAdministratorProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('administrator_profiles') || Schema::hasColumn('administrator_profiles', 'active')) { return; }
        Schema::table('administrator_profiles', function($table)
        {
            $table->boolean('active')->after('administrator_id')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('administrator_profiles', function (Blueprint $table) {
            $table->dropIfExists('active');
        });
    }
}
