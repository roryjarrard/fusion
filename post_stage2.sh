#!/bin/bash
DATE=$(date +%F.%H.%M)
STAGE='stage2'
FOLDER="fusion.dev.$DATE"
MPATH="/var/www/vhosts/car_data/staging/$STAGE"
FILE="deploy.$STAGE.tgz"
OWNER='cdstaging:apache'

sudo mkdir -p ${MPATH}/releases/${FOLDER}
sudo ln -sTf ${FOLDER} ${MPATH}/releases/latest
sudo ln -sTf ${MPATH}/releases/latest my_latest
sudo mv ${FILE} my_latest/

sh -c "cd my_latest && sudo tar -zxvf $FILE"
sh -c "cd my_latest && sudo rm -Rf $FILE"
sh -c "cd my_latest && sudo chown -R $OWNER ."
sh -c 'cd my_latest && composer update'
sh -c 'cd my_latest && yarn install'
sh -c 'cd my_latest && mkdir -p storage/app/public/docs'
sh -c 'cd my_latest && cp ../current/.env .'
sh -c 'cd my_latest && yarn run production'
sh -c 'cd my_latest && chmod -R gu+w storage'
sh -c 'cd my_latest && chmod -R guo+w storage'
sh -c 'cd my_latest && php artisan cache:clear'
sh -c "cd $STAGE/releases && ln -sTf \$(readlink current) previous"
sh -c "cd $STAGE/releases && ln -sTf \$(readlink latest) current"
sh -c "cd my_latest/public && ln -sTf $MPATH/upload storage"
sh -c "cd my_latest/public && sudo chown -R $OWNER storage"
sh -c 'cd my_latest/public && sudo find storage -type d -exec chmod 777 {} \;'
sh -c 'cd my_latest/public && sudo find storage -type f -exec chmod 666 {} \;'
sh -c 'cd my_latest && php artisan route:clear'
sh -c 'cd my_latest && php artisan route:cache'
sh -c "cd $STAGE/releases && rm latest"
rm my_latest
sudo service httpd restart
exit
