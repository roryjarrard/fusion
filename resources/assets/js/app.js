import 'babel-polyfill';

require('./bootstrap');

import store from './store';

import {Common, Constants} from "./utilities";

window.Common = new Common();
Vue.prototype.Common = window.Common; // Frank 2018-10-10 makes common available in the component html
window.Constants = Constants;

import vSelect from 'vue-select-checkbox';

Vue.component('v-select', vSelect);

import VueFlashMessage from 'vue-flash-message';

Vue.use(VueFlashMessage);
require('vue-flash-message/dist/vue-flash-message.min.css');

import VueCurrencyFilter from 'vue-currency-filter';

Vue.use(VueCurrencyFilter, {
    symbol: '$',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
});

import Autocomplete from 'v-autocomplete';

Vue.use(Autocomplete);


let partialPath = './components/partials/';

/**
 * New datepicker with its default options
 *
 * When you are using it, bind :config="datePickerOptions"
 *
 * (You can overwrite this locally if you please in the component)
 * see:
 *      data() {
 *          return {
 *              datePickerOptions: 'foo'
 *          }
 *      }
 *
 *      beforeCreate() {
 *          console.log(this.datePickerOptions);  // logs the options below'
 *      }
 *
 *      mounted() {
 *          console.log(this.datePickerOptions);  // logs foo
 *      }
 */
import VueBootstrapDatePicker from 'vue-bootstrap-datetimepicker';

Vue.component('vue-bootstrap-date-picker', VueBootstrapDatePicker);
Vue.prototype.datePickerOptions = {
    format: 'YYYY-MM-DD',
    useCurrent: false,
    icons: {
        date: "fas fa-calendar",
    },
};

Vue.component('driver-portal', require('./components/driver/DriverPortal.vue').default);
Vue.component('manager-portal', require('./components/manager/ManagerPortal.vue').default);
Vue.component('administrator-portal', require('./components/administrator/AdministratorPortal.vue').default);
Vue.component('super-portal', require('./components/super/SuperPortal.vue').default);
Vue.component('super-session', require('./components/super/SuperSession').default);
Vue.component('administrator-session', require('./components/administrator/AdministratorSession').default);
Vue.component('manager-session', require('./components/manager/ManagerSession').default);
Vue.component('search', require('./components/partials/Search').default);
Vue.component('breadcrumbs', require(partialPath + 'Breadcrumbs.vue').default);
Vue.component('dropdown-submenu', require(partialPath + 'DropdownSubmenu.vue').default);
Vue.component('dynamic-logo', require(partialPath + 'DynamicLogo.vue').default);
Vue.component('driver-debug', require(partialPath + 'DriverDebug.vue').default);
/**
 * These must be global because they are used directly in a blade template that can't import
 */
Vue.component('driver-information-tile', require('./components/driver/DriverInformationTile').default);
Vue.component('driver-debug', require('./components/partials/DriverDebug').default);
Vue.component('status-indicator', require('./components/driver/StatusIndicator').default); // davery

// notifications
Vue.component('notifications', require('./components/partials/Notifications.vue').default);

Vue.component('driver-registration', require('./components/registration/RegistrationView.vue').default);

// PASSPORT
Vue.component('passport-clients', require('./components/passport/Clients').default);
Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients').default);
Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens').default);

/**
 * Directive so we can watch clicks outside of an element by using:
 *  <some-element v-click-outside="callbackfuncton"/>
 *  Frank
 */
Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
        el.clickOutsideEvent = function (event) {
            // here I check that click was outside the el and his children
            if (!(el == event.target || el.contains(event.target))) {
                // and if it did, call method provided in attribute value
                vnode.context[binding.expression](event);
            }
        };
        document.body.addEventListener('click', el.clickOutsideEvent);
    },
    unbind: function (el) {
        document.body.removeEventListener('click', el.clickOutsideEvent);
    },
});


new Vue({
    el: '#app',
    mounted() {
        //console.log('APP CREATED');
        window.onhashchange = function () {
            let locationHash = window.location.hash.slice(1);
            //console.log('locationHash: ' + locationHash);
            let newView = locationHash.slice(0, locationHash.search('/'));
            //console.log("newView: " + newView + ' new locationHash ' + locationHash);
            let newSubView = locationHash.slice(locationHash.search('/')).slice(1);
            //console.log("newSubView: " + newSubView);

            if (store.state.view != newView) {
                //console.log('app: change view from ' + store.state.view + ' to ' + window.location.hash.slice(1));
                window.Common.setView(newView, newSubView);
            } else if (store.state.subView != newSubView) {
                //console.log('app: setting subview to ' + newSubView);
                window.Common.setSubView(newSubView);
            }
        };
    },
    store
});
