import {Errors} from '../utilities';

export default class Notification {
    constructor(data) {
        if ( Common.isEmpty(data) ) {
            this.id = null;
            this.creator = null;
            this.area = '';
            this.severity = -1;
            this.title = '';
            this.content = '';
            this.icon = null;
            this.link = {id: -1};
            this.dismissible = true;
        } else {
            this.id = data.id;
            this.creator = data.creator;
            this.area = data.area;
            this.severity = Common.isEmpty(data.severity) ? -1 : data.severity;
            this.title = data.title;
            this.content = data.content;
            this.icon = Common.isEmpty(data.icon) ? -1 : data.icon;

            // we do this to handle null value links and to deal with the selection of links for notifications
            if ( Common.isEmpty(data.link) ) {
                this.link = {id: -1}
            } else {
                if (typeof data.link == 'string' || data.link instanceof String) {
                    this.link = JSON.parse(data.link);
                } else {
                    this.link = data.link;
                }
            }

            this.dismissible = data.dismissible;
        }

        this.errors = new Errors;
    }

    /**
     * Validate a specific field, or all fields
     * @param field_name
     */
    validate(field_name = null) {
        //console.log('validating ' + field_name);
        console.log('validating');
        // if no specific field is given, validate all fields
        if ( Common.isEmpty(field_name) ) {
            for (let field in this) {
                this.isValid(field);
            }
        } else {
            this.isValid(field_name);
        }
    }

    /**
     * Specific validation rules for each field
     * @param field_name
     */
    isValid(field_name) {

        switch (field_name) {
            // exit for fields not requiring validation
            case 'errors':
            case 'creator':
                break;
            case 'area':
                // validate area
                if (this.area.length > 250) {
                    this.errors.add('area', ['Maximum length of area is 255 characters']);
                } else if ( this.area.length < 1 ) {
                    this.errors.add('area', ['Area is required']);
                } else {
                    this.errors.clear('area')
                }
                break;
            case 'severity':
                if ( !['low','medium','high'].includes(this.severity) ) {
                    this.errors.add('severity', ['Please select a severity']);
                } else {
                    this.errors.clear('severity')
                }
                break;
            case 'title':
                // validate title
                if (this.title.length > 250) {
                    this.errors.add('title', ['Maximum length of the title is 255 characters']);
                } else if ( this.title.length < 1 ) {
                    this.errors.add('title', ['Title is required']);
                } else {
                    this.errors.clear('title')
                }

                break;
            case 'content':
                // validate content
                if (this.content.length > 250) {
                    this.errors.add('content', ['Maximum length of the content is 255 characters']);
                } else if ( this.content.length < 1 ) {
                    this.errors.add('content', ['Content is required']);
                } else {
                    this.errors.clear('content')
                }
                break;
            default:
            // do nothing
        }
    }

}