/**
 * Created by Frank on 18/10/2017.
 *
 * Data Drivers have
 */
export default class Driver {
    constructor() {
        this.driverProfile = {
            user_id: -1,
            active: 1,
            division_id: -1,
            street_light_id: 1, // new driver always has this value
            employee_number: '',
            start_date: null,
            stop_date: null,
            job_title: '',
            address_id: -1,

            vehicle_profile_id: -1,
            personal_vehicle_id: null,
            cost_center: '',
            mileage_band_id: -1,
            territory_type_id: -1,
            territory_list: {},
            editing_user_id: null
        };

        //A drivers associated managers
        this.driverManager = {};

        this.address = {
            street: '',
            street2: null,
            city: '',
            state_province_id: -1,
            zip_postal: '',
            country_id: -1,
        };
    }

}