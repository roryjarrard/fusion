/**
 * Created by Frank on 18/10/2017.
 *
 * Data common to super users
 */
export default class Super {
    constructor () {
        this.superProfile = {
            user_id: -1,
            active: 1,
            super_id: -1,

            // permissions
            is_account_executive: 0,
            is_insurance_processor: 0,
            is_payment_processor: 0,
            is_service_associate: 0,
            is_vehicle_profile_approver: 0,
            can_view_eft: 0,
            can_view_login_information: 0,
            can_view_database_tables: 0,
            is_database_maintainer: 0,
            is_bank_maintainer: 0,
            is_caa_maintainer: 0,
            is_tasks_maintainer: 0,
            has_all_permissions: 0,

        };
    }
}