/**
 * Created by Frank on 18/10/2017.
 *
 * Data common to managers
 */
// MANAGER PROFILE
export default class Manager {
    constructor () {
        this.managerProfile = {
            user_id : -1,
            active: 1,
            manager_id : -1,
            company_id : -1,
            manager_number : null,
            approval_description : null,
            address_id : null
        };

        // A manager's assigned drivers
        this.managerDriver = {};
    }
}