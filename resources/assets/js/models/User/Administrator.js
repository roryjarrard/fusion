/**
 * Created by Frank on 18/10/2017.
 *
 * Data common to administrators
 */

export default class Administrator {
    constructor () {
        this.administratorProfile = {
            user_id: -1,
            active: 1,
            administrator_id: -1,
            company_id: -1,
            administrator_number: null,
            administrator_phone: null,
            company_approver: null,
            company_approver_phone: null,
            company_coordinator: null,
            company_coordinator_phone: null,
            insurance_coordinator: null,
            insurance_coordinator_phone: null,
            license_coordinator: null,
            license_coordinator_phone: null,
            miroute_coordinator: null,
            miroute_coordinator_phone: null,

            //TODO: Ask Rory about this
            service_associate: null,
            service_associate_phone: null,
            account_executive: null,
            account_executive_phone: null,
            is_limited: null,
            address_id: null
        };

        //An administrator's associated divisions
        this.administratorDivision = {};
    }
}