import {DriverModel, ManagerModel, AdministratorModel, SuperModel} from "./index"; // models/User

export default class User {
    // users
    constructor() {
        this.username = '';
        this.email = '';
        this.personal_email = '';
        this.first_name = '';
        this.last_name = '';
        this.company_id = -1;

        this.is_driver = false;
        this.is_manager = false;
        this.is_administrator = false;
        this.is_super = false;

        this.driver = new (DriverModel);
        this.manager = new (ManagerModel);
        this.administrator = new (AdministratorModel);
        this.super = new (SuperModel);
    }
}

