import {Vehicle} from "./index"; // models
import {Errors} from '../utilities';

export default class VehicleProfile {

    constructor(data) {
        if (Common.isEmpty(data)) {
            this.company_id = -1;
            this.vehicle_id = -1;
            this.vehicle_year = -1;

            this.name_postfix = '';

            this.vehicle_profile_status = 1;
            this.capital_cost = -1;
            this.resale_condition_id = -1;
            this.resale_condition = null;
            this.retention = 5;

            this.max_vehicle_age = 4;
            this.business_use_percent = 71.4;
            this.calculate_fixed = 1;
            this.preset_fixed = 0.00;
            this.calculate_variable = 1;
            this.preset_variable = 0.00;

            this.capital_cost_adj = 0;
            this.resale_value_adj = 0;
            this.capital_cost_tax_adj = 0;
            this.monthly_payment_adj = 0;
            this.depreciation_adj = 0;
            this.finance_cost_adj = 0;
            this.insurance_adj = 0;
            this.fee_renewal_adj = 0;

            this.maintenance_adj = 0;
            this.fuel_price_adj = 0;
            this.fuel_economy_adj = 0;
            this.repair_adj = 0;

            this.fixed_adj = 0;
            this.fixed_adj_text = '';
            this.variable_adj = 0;
            this.variable_adj_text = '';
            this.proposed = 0;

            this.created_by = null;

            this.creator = null;
            this.creator_name = null;

            this.vehicle = new Vehicle;

            this.variableAdjustments = [];

            this.note = {
                title: '',
                content: ''
            }
            this.driver_profiles = [];
            this.driver_count = 0;
            this.deleted_at = null;
        } else {
            this.id = data.id
            this.company_id = data.company_id;
            this.vehicle_id = data.vehicle_id;
            this.vehicle_year = data.vehicle_year;
            this.name_postfix = data.name_postfix;

            this.vehicle_profile_status = data.vehicle_profile_status;
            this.capital_cost = data.capital_cost;
            this.resale_condition_id = data.resale_condition_id;
            this.resale_condition = _.cloneDeep(data.resale_condition);
            this.retention = data.retention;

            this.max_vehicle_age = data.max_vehicle_age;
            this.business_use_percent = data.business_use_percent;
            this.calculate_fixed = data.calculate_fixed;
            this.preset_fixed = data.preset_fixed;
            this.calculate_variable = data.calculate_variable;
            this.preset_variable = data.preset_variable;

            this.capital_cost_adj = data.capital_cost_adj;
            this.resale_value_adj = data.resale_value_adj;
            this.capital_cost_tax_adj = data.capital_cost_tax_adj;
            this.monthly_payment_adj = data.monthly_payment_adj;
            this.depreciation_adj = data.depreciation_adj;
            this.finance_cost_adj = data.finance_cost_adj;
            this.insurance_adj = data.insurance_adj;
            this.fee_renewal_adj = data.fee_renewal_adj;

            this.maintenance_adj = data.maintenance_adj;
            this.fuel_price_adj = data.fuel_price_adj;
            this.fuel_economy_adj = data.fuel_economy_adj;
            this.repair_adj = data.repair_adj;

            this.fixed_adj = data.fixed_adj;
            this.fixed_adj_text = Common.isEmpty(data.fixed_adj_text) ? '' : data.fixed_adj_text;
            this.variable_adj = data.variable_adj;
            this.variable_adj_text = Common.isEmpty(data.variable_adj_text) ? '' : data.variable_adj_text;
            this.proposed = data.proposed;

            this.created_by = data.created_by;
            this.creator = Common.isEmpty(data.creator) ? null : data.creator;
            this.creator_name = Common.isEmpty(this.creator) ? null : this.creator.first_name + ' ' + this.creator.last_name;

            this.vehicle = _.cloneDeep(data.vehicle);

            this.variableAdjustments = _.cloneDeep(data.variableAdjustments);

            this.note = _.cloneDeep(data.note);
            this.driver_profiles = _.cloneDeep(data.driver_profiles);
            this.driver_count = data.driver_count;
            this.deleted_at = data.deleted_at;
        }

        this.errors = new Errors;

    }

    /**
     * Validate a specific field, or all fields
     * @param field_name
     */
    validate(field_name = null) {
        //console.log('validating ' + field_name);
        console.log('validating');
        // if no specific field is given, validate all fields
        if (Common.isEmpty(field_name)) {
            for (let field in this) {
                this.isValid(field);
            }
        } else {
            this.isValid(field_name);
        }
    }


    /**
     * Specific validation rules for each field
     * @param field_name
     */
    isValid(field_name) {

        switch (field_name) {
            // exit for fields not requiring validation
            case 'errors':
            case 'vehicle':
            case 'variableAdjustments':
                break;
            case 'vehicle_year':
                if (this.vehicle_year < 0) {
                    this.errors.add('vehicleProfile.vehicle_year', ['Please select a year']);
                } else {
                    this.errors.clear('vehicleProfile.vehicle_year');
                }
                break;
            case 'vehicle_id':
                if (this.id < 0) {
                    this.errors.add('vehicleProfile.vehicle_id', ['Please select a vehicle']);
                } else {
                    this.errors.clear('vehicleProfile.vehicle_id');
                }
                break;
            case 'name_postfix':
                if (Common.isEmpty(this.name_postfix)) {
                    this.errors.add('vehicleProfile.name_postfix', ['A postfix is required']);
                } else {
                    this.errors.clear('vehicleProfile.name_postfix');
                }

                break;
            case 'capital_cost':
                if (Common.isEmpty(this.capital_cost) || this.capital_cost < 1) {
                    this.errors.add('vehicleProfile.capital_cost', ['Please select a capital cost']);
                } else {
                    this.errors.clear('vehicleProfile.capital_cost');
                }
                break;
            case 'capital_cost':
                if (Common.isEmpty(this.capital_cost) || this.capital_cost < 1) {
                    this.errors.add('vehicleProfile.capital_cost', ['Please select a capital cost']);
                } else {
                    this.errors.clear('vehicleProfile.capital_cost');
                }
                break;
            case 'resale_condition_id':
                if (Common.isEmpty(this.resale_condition_id) || this.resale_condition_id < 1) {
                    this.errors.add('vehicleProfile.resale_condition_id', ['Please select a resale condition']);
                } else {
                    this.errors.clear('vehicleProfile.resale_condition_id');
                }
                break;
            case 'retention':
                if (Common.isEmpty(this.retention) || this.retention < 2 || 6 < this.retention) {
                    this.errors.add('vehicleProfile.retention', ['Retention must be between 2 and 6 years']);
                } else {
                    this.errors.clear('vehicleProfile.retention');
                }
                break;
            case 'max_vehicle_age':
                if (Common.isEmpty(this.max_vehicle_age) || this.max_vehicle_age < 1) {
                    this.errors.add('vehicleProfile.max_vehicle_age', ['Invalid max vehicle age']);
                } else {
                    this.errors.clear('vehicleProfile.max_vehicle_age');
                }
                break;
            case 'business_use_percent':
                if (Common.isEmpty(this.business_use_percent) || this.business_use_percent < 0 || this.business_use_percent > 100) {
                    this.errors.add('vehicleProfile.business_use_percent', ['Invalid business use percent']);
                } else {
                    this.errors.clear('vehicleProfile.business_use_percent');
                }
                break;
            /**
             * These following two watches, on caluclate_variable and calculate_fixed are here to deal with the scenario
             * that the user deselected calculate variable, received and error from the server, and wants to reselect
             * calculate variable. Now whenever on of these checkboxes is clicked, the errors on the fixed reimbursement,
             * and variable reimbursement preset fields will be cleared
             */
            case 'calculate_variable':
                this.errors.clear('vehicleProfile.preset_variable');
                break;
            case 'calculate_fixed':
                this.errors.clear('vehicleProfile.preset_fixed');
                break;
            case 'preset_variable':
                if (this.preset_variable < 0 || 1000 < this.preset_variable) {
                    this.errors.add('vehicleProfile.preset_variable', ['Invalid variable reimbursement preset']);
                } else {
                    this.errors.clear('vehicleProfile.preset_variable');
                }
                break;
            case 'preset_fixed':
                if (this.preset_fixed < 0 || 10000 < this.preset_fixed) {
                    this.errors.add('vehicleProfile.preset_fixed', ['Invalid fixed reimbursement preset']);
                } else {
                    this.errors.clear('vehicleProfile.preset_fixed');
                }
                break;
            case 'capital_cost_adj':
                if (this.capital_cost_adj < -100 || 100 < this.capital_cost_adj) {
                    this.errors.add('vehicleProfile.capital_cost_adj', ['Invalid capital cost adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.capital_cost_adj');
                }
                break;
            case 'capital_cost_tax_adj':
                if (this.capital_cost_tax_adj < -100 || 100 < this.capital_cost_tax_adj) {
                    this.errors.add('vehicleProfile.capital_cost_tax_adj', ['Invalid capital cost tax adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.capital_cost_tax_adj');
                }
                break;
            case 'resale_value_adj':
                if (this.resale_value_adj < -100 || 100 < this.resale_value_adj) {
                    this.errors.add('vehicleProfile.resale_value_adj', ['Invalid resale value adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.resale_value_adj');
                }
                break;
            case 'monthly_payment_adj':
                if (this.monthly_payment_adj < -100 || 100 < this.monthly_payment_adj) {
                    this.errors.add('vehicleProfile.monthly_payment_adj', ['Invalid monthly payment adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.monthly_payment_adj');
                }
                break;
            case 'depreciation_adj':
                if (this.depreciation_adj < -100 || 100 < this.depreciation_adj) {
                    this.errors.add('vehicleProfile.depreciation_adj', ['Invalid depreciation adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.depreciation_adj');
                }
                break;
            case 'insurance_adj':
                if (this.insurance_adj < -100 || 100 < this.insurance_adj) {
                    this.errors.add('vehicleProfile.insurance_adj', ['Invalid insurance adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.insurance_adj');
                }
                break;
            case 'finance_cost_adj':
                if (this.finance_cost_adj < -100 || 100 < this.finance_cost_adj) {
                    this.errors.add('vehicleProfile.finance_cost_adj', ['Invalid finance cost adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.finance_cost_adj');
                }
                break;
            case 'fee_renewal_adj':
                if (this.fee_renewal_adj < -100 || 100 < this.fee_renewal_adj) {
                    this.errors.add('vehicleProfile.fee_renewal_adj', ['Invalid fee / renewal adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.fee_renewal_adj');
                }
                break;
            case 'fuel_economy_adj':
                if (this.fuel_economy_adj < -100 || 100 < this.fuel_economy_adj) {
                    this.errors.add('vehicleProfile.fuel_economy_adj', ['Invalid fuel economy adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.fuel_economy_adj');
                }
                break;
            case 'fuel_price_adj':
                if (this.fuel_price_adj < -100 || 100 < this.fuel_price_adj) {
                    this.errors.add('vehicleProfile.fuel_price_adj', ['Invalid fuel price adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.fuel_price_adj');
                }
                break;
            case 'maintenance_adj':
                if (this.maintenance_adj < -100 || 100 < this.maintenance_adj) {
                    this.errors.add('vehicleProfile.maintenance_adj', ['Invalid maintenance adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.maintenance_adj');
                }
                break;
            case 'repair_adj':
                if (this.repair_adj < -100 || 100 < this.repair_adj) {
                    this.errors.add('vehicleProfile.repair_adj', ['Invalid repairs adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.repair_adj');
                }
                break;
            case 'fixed_adj':
                if (this.fixed_adj < -100 || 100 < this.fixed_adj) {
                    this.errors.add('vehicleProfile.fixed_adj', ['Invalid fixed adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.fixed_adj');
                }
                break;
            case 'variable_adj':
                if (this.variable_adj < -100 || 100 < this.variable_adj) {
                    this.errors.add('vehicleProfile.variable_adj', ['Invalid variable adjustment']);
                } else {
                    this.errors.clear('vehicleProfile.variable_adj');
                }
                break;
            case 'fixed_adj_text':
                if ( Common.isEmpty(this.fixed_adj) ) {
                    this.errors.clear('vehicleProfile.fixed_adj_text');
                }
                if (this.fixed_adj_text.search(/^[a-zA-Z0-9_., ]*$/) === -1) {
                    this.errors.add('vehicleProfile.fixed_adj_text', ['Invalid reason']);
                } else {
                    this.errors.clear('vehicleProfile.fixed_adj_text');
                }
                break;
            case 'variable_adj_text':
                if ( Common.isEmpty(this.variable_adj) ) {
                    this.errors.clear('vehicleProfile.variable_adj_text');
                }
                if (this.variable_adj_text.search(/^[a-zA-Z0-9_., ]*$/) === -1) {
                    this.errors.add('vehicleProfile.variable_adj_text', ['Invalid reason']);
                } else {
                    this.errors.clear('vehicleProfile.variable_adj_text');
                }
                break;
            default:
            // do nothing
        }
    }
}

