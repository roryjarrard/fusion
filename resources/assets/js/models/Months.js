/**
 * Created by Frank on 04/04/2018.
 */
export default [
            {id: 1, name: 'January', short_name: 'Jan'},
            {id: 2, name: 'February', short_name: 'Feb'},
            {id: 3, name: 'March', short_name: 'Mar'},
            {id: 4, name: 'April', short_name: 'Apr'},
            {id: 5, name: 'May', short_name: 'May'},
            {id: 6, name: 'June', short_name: 'Jun'},
            {id: 7, name: 'July', short_name: 'Jul'},
            {id: 8, name: 'August', short_name: 'Aug'},
            {id: 9, name: 'September', short_name: 'Sep'},
            {id: 10, name: 'October', short_name: 'Oct'},
            {id: 11, name: 'November', short_name: 'Nov'},
            {id: 12, name: 'December', short_name: 'Dec'},
        ]