/**
 * Created by Frank on 13/03/2018.
 */

import {Errors} from '../utilities';
import {EventBus} from '../event-bus.js';

export default class Bank {
    constructor(data) {
        if ( Common.isEmpty(data) ) {
            this.id = null;
            this.country_id = -1;
            this.status = -1;
            this.bank_name = '';
            this.immediate_destination = '';
            this.immediate_origin = '';
            this.immediate_origin_name = '';
            this.company_id = '';
            this.company_name = '';
            this.originating_dfi = '';
            this.originator_id = '';
            this.originator_name = '';
            this.transit_number = '';
            this.account_number = '';
        } else {
            this.id = data.id;
            this.country_id = data.country_id;
            this.status = data.status;
            this.bank_name = data.bank_name;
            this.immediate_destination = data.immediate_destination;
            this.immediate_origin = data.immediate_origin;
            this.immediate_origin_name = data.immediate_origin_name;
            this.company_id = data.company_id;
            this.company_name = data.company_name;
            this.originating_dfi = data.originating_dfi;
            this.originator_id = data.originator_id;
            this.originator_name = data.originator_name;
            this.transit_number = data.transit_number;
            this.account_number = data.account_number;
        }

        this.errors  = new Errors;
    }

    /**
     * Validate a specific field, or all fields
     * @param field_name
     */
    validate(field_name = null) {
        //console.log('validating ' + field_name);

        // if no specific field is given, validate all fields
        if ( Common.isEmpty(field_name) ) {
            for (let field in this) {
                this.isValid(field);
            }
        } else {
            this.isValid(field_name);
        }
    }

    /**
     * Specific validation rules for each field
     * @param field_name
     */
    isValid(field_name) {

        // fields common to both US & Canada
        switch(field_name) {
            // exit for fields not requiring validation
            case 'errors':
            case 'id':
                break;
            case 'country_id':
                // validate country_id
                if (this.country_id != 1 && this.country_id != 2) {
                    this.errors.add('country_id', ['Select a country']);
                } else {
                    this.errors.clear('country_id')
                }
                break;
            case 'bank_name':
                // validate bank_name
                if (this.bank_name.search(/^[a-zA-Z0-9 ]{1,23}$/) == -1) {
                    this.errors.add('bank_name', ['The bank name must be 1-23 alphanumeric characters']);
                } else {
                    this.errors.clear('bank_name');
                }
                break;
            case 'status':
                // validate bank_name
                if (this.status != 0 && this.status != 1) {
                    this.errors.add('status', ['Please select a status']);
                } else {
                    this.errors.clear('status');
                }
                break;
        }

        // US Only Validation
        if (this.country_id == 1) {

            switch (field_name) {

                case 'immediate_destination':
                    // validate immediate destination
                    // should be 9 digits. In the ach it is a blank space followed by 9 numbers
                    if (this.immediate_destination.search(/^\d{9}$/) == -1) {
                        this.errors.add('immediate_destination', ['The immediate destination must be a 9 digit number']);
                    } else {
                        this.errors.clear('immediate_destination');
                    }
                    break;
                case 'immediate_origin':
                    // validate immediate destination
                    // should be 9 digits. In the ach it is a blank space followed by 9 numbers
                    if (this.immediate_origin.search(/^\d{9,10}$/) == -1) {
                        this.errors.add('immediate_origin', ['The immediate origin must be a 9-10 digit number']);
                    } else {
                        this.errors.clear('immediate_origin');
                    }
                    break;
                case 'immediate_origin_name':
                    // validate immediate_origin_name
                    // should be 9 digits. In the ach it is a blank space followed by 9 numbers
                    if (this.immediate_origin_name.search(/^[a-zA-Z0-9 ]{1,23}$/) == -1) {
                        this.errors.add('immediate_origin_name', ['The immediate origin name must be 1-23 alphanumeric characters']);
                    } else {
                        this.errors.clear('immediate_origin_name');
                    }
                    break;
                case 'company_id':
                    // validate company_id
                    // should be 10 digits.
                    if (this.company_id.search(/^[a-zA-Z0-9]{1,23}$/) == -1) {
                        this.errors.add('company_id', ['The company identification must be a 10 digit number']);
                    } else {
                        this.errors.clear('company_id');
                    }
                    break;
                case 'company_name':
                    // should be 1-16 alphanumeric characters.
                    if (this.company_name.search(/^[a-zA-Z0-9 ]{1,23}$/) == -1) {
                        this.errors.add('company_name', ['The company name must be 1-23 alphanumeric characters']);
                    } else {
                        this.errors.clear('company_name');
                    }
                    break;
                case 'originating_dfi':
                    // validate originating_dfi
                    // should be 8 digits
                    if (this.originating_dfi.search(/^\d{8}$/) == -1) {
                        this.errors.add('originating_dfi', ['The originating DFI must be an 8 digit number']);
                    } else {
                        this.errors.clear('originating_dfi');
                    }
                    break;
            }

        // Canada Specific Validation
        } else if (this.country_id == 2) {

            switch (field_name) {
                case 'originator_id':
                    // validate originator_id
                    // should be 10 alphanumeric characters.
                    if (this.originator_id.search(/^[a-zA-Z0-9]{10}$/) == -1) {
                        this.errors.add('originator_id', ['The originator ID must be 10 alphanumeric characters']);
                    } else {
                        this.errors.clear('originator_id');
                    }
                    break;
                case 'originator_name':
                    // validate originator_name
                    // NOTE on some docs this is referred to as the originator's short name
                    if (this.originator_name.search(/^[a-zA-Z0-9]{1,15}$/) == -1) {
                        this.errors.add('originator_name', ['The originator name must be 1-15 alphanumeric characters']);
                    } else {
                        this.errors.clear('originator_name');
                    }
                    break;
                case 'transit_number':
                    // validate transit_number
                    if (this.transit_number.search(/^\d{3,5}$/) == -1) {
                        this.errors.add('transit_number', ['The branch/transit number must be 3-5 digits']);
                    } else {
                        this.errors.clear('transit_number');
                    }
                    break;
                case 'account_number':
                    // validate account_number
                    // NOTE on some docs this is referred to as the Drawee Account #
                    if (this.account_number.search(/^\d{1,12}$/) == -1) {
                        this.errors.add('account_number', ['The account number must be 1-12 digits']);
                    } else {
                        this.errors.clear('account_number');
                    }
                    break;
            }
        }
    }

    /**
     * Submit the bank to the database, requires mode == 'add' or mode == 'edit' as parameter
     *      ex. this.bank.submit('add');
     * @param mode
     */
    submit(mode) {

        let url = mode == 'add' ? '/addBank' : '/updateBank';

        axios.post(url, {bank: this}).then( response => {
            EventBus.$emit('bank-submission-successful', this);
        }).catch( errors => {
            this.errors.errors = errors.response.data;
        })

    }
}
