/**
 * Created by Frank on 11/12/2017.
 *
 * Tabs for the notification component
 */
export default [
    {
        name: 'Unread',
        icon: 'envelope',
        active: true,
        includeToggle: true,
    },
    {
        name: 'Read',
        icon: 'envelope-open',
        active: false,
        includeToggle: true,
    },
    // {
    //     name: 'Driver',
    //     icon: 'life-bouy',
    //     active: false,
    //     includeToggle: true,
    //
    // },
    // {
    //     name: 'Manager',
    //     icon: 'id-card-o',
    //     active: false,
    //     includeToggle: true,
    // },
    // {
    //     name: 'Administrator',
    //     icon: 'id-card-o',
    //     active: false,
    //     includeToggle: true,
    // },
    // {
    //     name: 'Super',
    //     icon: 'life-bouy',
    //     active: false,
    //     includeToggle: true,
    //
    // },
]