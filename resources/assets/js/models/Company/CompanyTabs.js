/**
 * Created by Frank on 22/11/2017.
 */
export default [
    {name: 'Options', icon: 'gears', active: true, includeToggle: false},
    {name: 'Contacts', icon: 'users', active: false, includeToggle: false},
    {
        name: 'Financials',
        icon: 'money',
        active: false,
        includeToggle: true,
        // showTab: !Common.isEmpty(this.company.options.use_company_financials_module) ? true : false,
    },
    {
        name: 'Insurance',
        icon: 'life-bouy',
        active: false,
        includeToggle: true,
        // showTab: !Common.isEmpty(this.company) ? this.company.options.use_company_insurance_module : false
    },
    {
        name: 'License',
        icon: 'id-card-o',
        active: false,
        includeToggle: true,
        // showTab: !Common.isEmpty(this.company) ? this.company.options.use_company_license_module : false
    },
    {
        name: 'Mileage',
        icon: 'road',
        active: false,
        includeToggle: true,
        // showTab: !Common.isEmpty(this.company) ? this.company.options.use_company_mileage_module : false
    }
]