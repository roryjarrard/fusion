/**
 * Created by Frank on 29/11/2017.
 */
export default class CompanyContact {
    // Company
    constructor(company_id = -1) {
        this.id = 0;
        this.company_id = company_id;
        this.type = -1;
        this.title = '';
        this.first_name = '';
        this.last_name = '';
        this.phone_number = '';
        this.fax_number = '';
        this.email = '';
        this.mobile_number = '';
        // not stored in the database, for the front end only
        this.unsubmitted = true;
    }
}