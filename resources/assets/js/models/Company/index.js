export {default as Company} from './Company';
export {default as CompanyContact} from './CompanyContact';
export {default as CompanyProfile} from './CompanyProfile';
export {default as CompanyTabs} from './CompanyTabs';

// export {default as CompanyInsurance} from './CompanyInsurance';
// export {default as CompanyLicense} from './CompanyLicense';
// export {default as CompanyMileage} from './CompanyMileage';
// export {default as CompanyOptions} from './CompanyOptions';

