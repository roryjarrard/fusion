/**
 * Created by Frank on 22/11/2017.
 */
export default class CompanyProfile {
    // Company
    constructor() {
        this.company_id = -1;
        this.website = '';
        this.industry_id = -1;
        this.street = '';
        this.street2 = '';
        this.city = '';
        this.state_province_id = -1;
        this.zip_postal = '';
        this.country_id = '';
        this.time_zone_id = -1;
    }
}