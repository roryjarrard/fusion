/**
 * Created by Frank on 05/01/2018.
 */


export default class Address {
    constructor() {
        this.street = '';
        this.street2 = null;
        this.city = '';
        this.state_province_id = -1;
        this.zip_postal = '';
        this.country_id = -1
    }
}
