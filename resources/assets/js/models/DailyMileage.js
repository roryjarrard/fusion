export default class DailyMileage {
    constructor(data) {
        if (Common.isEmpty(data)) {
            this.id = -1;
            this.user_id = -1;
            this.trip_date = '';
            this.last_state = '';
            this.destination = '';
            this.business_purpose = '';
            this.starting_odometer = null;
            this.ending_odometer = null;
            this.business_mileage = 0;
            this.commuter_mileage = 0;
            this.personal_mileage = 0;
            this.gap_mileage = 0;
            this.created_at = null;
            this.updated_at = null;
        } else {
            this.id = data.id;
            this.user_id = data.user_id;
            this.trip_date = data.trip_date;
            this.last_state = data.last_state;
            this.destination = data.destination;
            this.business_purpose = data.business_purpose;
            this.starting_odometer = data.starting_odometer;
            this.ending_odometer = data.ending_odometer;
            this.business_mileage = data.business_mileage;
            this.commuter_mileage = data.commuter_mileage;
            this.personal_mileage = data.personal_mileage;
            this.gap_mileage = data.gap_mileage;
            this.created_at = data.created_at;
            this.updated_at = data.updated_at;
        }
    }


     updateData(obj) {
        console.log('trying to update', obj);
        return new Promise((resolve, reject) => {
            for (let field in obj) {
                if (this.hasOwnProperty(field)) {
                    this[field] = obj[field];
                }
            }

            resolve();
        });
    }
}