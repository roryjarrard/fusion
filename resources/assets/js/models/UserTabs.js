export default [
    {name: 'Driver Profile', icon: 'car', active: false, includeToggle: true},
    {name: 'Manager Profile', icon: 'users', active: false, includeToggle: true},
    {name: 'Administrator Profile', icon: 'key', active: false, includeToggle: true},
    {name: 'Super Profile', icon: 'universal-access', active: false, includeToggle: true}
]