export default class Vehicle {
    constructor() {
        this.id = -1;
        this.user_id = -1;
        this.year = -1;
        this.make_id = -1;
        this.model_id = -1;
        this.trim_id = -1;
        this.note = null;
        this.odometer = 0; // davery - changed from null
        this.old_vehicle_odometer = null;
        this.vehicle_cost = null;
        this.depreciation_method = null;
        this.age_changed = null;
        this.odometer_changed = null;
    }
}