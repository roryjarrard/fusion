/**
 * This class takes care of some style intricacies that are hard to deal with in straight css
 * It will attach the correct backgrounds on resize, and ensure that our app always takes a
 * minimum height of the full screen.
 * It can then set the minimum height for our portals
 */
export default class styleFixer {
    constructor(user_type = null) {
        if (user_type) {
            this.user_type = user_type;
            this.portal_id = null;

            /**
             * Round up to the nearest 10px. This ensures that there is no overflow,
             * and any whitespace at the bottom is near invisible.
             * Rounding to the straight ceiling will not work, and still produce overflow
             * @type {number}
             */
            this.minimum_background_height = $('#app').height() - Math.ceil($('#fusionHeader').height() / 10 ) * 10;

            /**
             * Bind fix main content height to a resize event
             */
            let self = this;
            $(window).on('resize', function() {
                self.minimum_background_height = $('#app').height() - Math.ceil($('#fusionHeader').height() / 10 ) * 10;
                self.fixMainContentHeight();
            })
        }



    }

    /**
     * Set the main content height
     */
    fixMainContentHeight() {
        console.log(this.minimum_background_height);
        $('#main-content').css('min-height', String(this.minimum_background_height )+ 'px');
    }

    /**
     * Set the minimum height for our 'portals'
     */
    fixPortalHeight() {
        this.portal_id = '#' + this.user_type + 'Portal';

        let top = $(this.portal_id).position().top;

        let min_height = $('#main-content').height() - (top + 30);

        $(this.portal_id).css('min-height', String(min_height)+ 'px');

    }



}




