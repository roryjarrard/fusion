import Vue from 'vue';
import Vuex from 'vuex';

require('es6-promise').polyfill();

Vue.use(Vuex);

const state = {
    // system
    start_time: new Date().getTime(), // no setter, this is it
    view: '',
    subView: '',

    // all users
    user_id: -1,
    user_type: '',
    username: '',
    user_email: '',
    user_personal_email: '',
    user_full_name: '',
    highest_authenticated_user_type: '',
    country_id: -1,
    userAddress: {}, // although only drivers have addresses right now
    menu: [], // main menu, dynamic by user type
    active_role: '',
    is_driver: false,
    is_manager: false,
    is_administrator: false,

    is_super: false,
    // driver
    driver_mileage_entry_method: '',
    driver_mileage_lock_day: 0,
    mileage_approval_end_day: 0,

    driver_payment_date: 0,
    driverDivision: {division_id: -1, division_name: ''},
    driver_time_zone: {},
    driver_street_light_array: {},
    driverProfile: {},

    personalVehicle: {},
    // driver, manager, administrator
    company_id: -1,
    company_slug: '',
    company_name: '',
    company_plan: '',
    company_start_year: -1,
    company_stop_year: -1,

    company_selections: {},
    company_report_labels: [],

    // manager, administrator
    managerDrivers: [],
    // session values used by super/admin
    session_company_id: -1,
    session_company_name: '',
    session_country_id: -1, // match the company in session's country
    session_division_id: -1,
    session_division_name: '',
    session_driver_id: -1,
    session_manager_id: -1,
    session_administrator_id: -1,
    session_user_id: -1,
    session_role: '',
    session_companies: '',
    session_from_date: -1,
    session_to_date: -1,
    session_year: -1,
    session_month: -1,
    session_day: -1,
    session_quarter: -1,
    session_vehicle_profile_id: -1,
    payment_responsibility_cardata: false,
    company_country_filter: 0,
    company_uses_insurance_module_filter: false,
    company_has_predefined_stops: false,
    company_uses_quarterly_tax_adjustment: false,
    company_uses_miroute: false,
    search_user_email: '',
    search_parameter: '',


    editedUser: {},

    inactive_company_id: -1,


    inactive_company_name: '',
    active_manager_id: -1,
    inactive_manager_id: -1,
    activeManagerProfile: {},
    inactiveManagerProfile: {},
    activeAdministratorProfile: {},
    inactiveAdministratorProfile: {},
    active_administrator_id: -1,

    inactive_administrator_id: -1,
    // Super Permissions
    is_account_executive: false,
    is_insurance_processor: false,
    is_payment_processor: false,
    is_service_associate: false,
    is_vehicle_profile_approver: false,
    is_announcement_approver: false,
    can_view_eft: false,
    can_view_login_information: false,
    can_view_database_tables: false,
    is_database_maintainer: false,
    is_bank_maintainer: false,
    is_caa_maintainer: false,
    is_tasks_maintainer: false,
    has_all_permissions: false,

    impersonator_id: -1,
    impersonator_role: '', // admin, super, manager
    impersonator_username: '',
    impersonator_page: '', // do we need this?
    first_impersonator_id: -1,
    first_impersonator_role: '',
    first_impersonator_username: '',
    first_impersonator_page: '', // do we need this?

    /**
     * Used to lock all inputs on a page.
     * This is used on the AddEditUserPage to lock everything for 'show'
     */
    lock_all_input: false,
};

const getters = {
    // system
    getStartTime: (state) => {
        return state.start_time;
    },

    getView: (state) => {
        return state.view;
    },

    getSubView: (state) => {
        state.subView = state.subView === 'null' ? null : state.subView;
        return state.subView;
    },

    getSessionCompanyId: (state) => {
        return state.session_company_id;
    },

    getSessionCompanyName: (state) => {
        return state.session_company_name;
    },

    getSessionCountryId: (state) => {
        return state.session_country_id;
    },

    getSessionDivisionId: (state) => {
        return state.session_division_id;
    },

    getSessionDivisionName: (state) => {
        return state.session_division_name;
    },

    getSessionUserId: (state) => {
        return state.session_user_id;
    },

    getSessionDriverId: (state) => {
        return state.session_driver_id;
    },

    getSessionManagerId: (state) => {
        return state.session_manager_id;
    },

    getSessionQuarter: (state) => {
        return state.session_quarter;
    },

    getSessionVehicleProfileId: (state) => {
        return state.session_vehicle_profile_id;
    },

    getManagerProfile: (state) => {
        return state.activeManagerProfile;
    },

    getActiveManagerProfile: (state) => {
        return state.activeManagerProfile;
    },

    getInactiveManagerProfile: (state) => {
        return state.inactiveManagerProfile;
    },

    getAdministratorProfile: (state) => {
        return state.activeAdministratorProfile;
    },

    getActiveAdministratorProfile: (state) => {
        return state.activeAdministratorProfile;
    },

    getInactiveAdministratorProfile: (state) => {
        return state.inactiveAdministratorProfile;
    },

    getSessionAdministratorId: (state) => {
        return state.session_administrator_id;
    },

    getActiveRole: (state) => {
        return state.active_role;
    },

    getSessionRole: (state) => {
        return state.session_role;
    },

    getSessionFromDate: (state) => {
        return state.session_from_date;
    },

    getSessionToDate: (state) => {
        return state.session_to_date;
    },

    getSessionYear: (state) => {
        return state.session_year;
    },

    getSessionMonth: (state) => {
        return state.session_month;
    },

    getSessionDay: (state) => {
        return state.session_day;
    },

    getSessionCompanies: (state) => {
        return state.session_companies;
    },

    getSearchUserEmail: (state) => {
        return state.search_user_email;
    },

    getEditedUser: (state) => {
        return state.editedUser;
    },

    getPaymentResponsibilityCardata: (state) => {
        return state.payment_responsibility_cardata;
    },

    getCompanyCountryFilter: (state) => {
        return state.company_country_filter;
    },


    getUserId: (state) => {
        return state.user_id;
    },

    getUsername: (state) => {
        return state.username;
    },

    getUserEmail: (state) => {
        return state.user_email;
    },

    getUserPersonalEmail: (state) => {
        return state.user_personal_email;
    },

    getUserType: (state) => {
        return state.user_type;
    },

    getUserAddress: (state) => {
        return state.userAddress;
    },

    getMenu: (state) => {
        return state.menu;
    },

    getIsDriver: (state) => {
        return state.is_driver;
    },

    getIsManager: (state) => {
        return state.is_manager;
    },

    getIsAdministrator: (state) => {
        return state.is_administrator;
    },

    getIsSuper: (state) => {
        return state.is_super;
    },

    getActiveAdministratorId: (state) => {
        return state.active_administrator_id;
    },

    getInactiveAdministratorId: (state) => {
        return state.inactive_administrator_id;
    },

    getActiveManagerId: (state) => {
        return state.active_manager_id;
    },

    getInactiveManagerId: (state) => {
        return state.inactive_manager_id;
    },

    getManagerDrivers: (state) => {
        return state.managerDrivers;
    },

    getHighestAuthenticatedUserType: (state) => {
        return state.highest_authenticated_user_type;
    },

    getUserFullName: (state) => {
        return state.user_full_name;
    },

    getCountryId: (state) => {
        return state.country_id;
    },

    getImpersonatorId: (state) => {
        return state.impersonator_id;
    },

    getImpersonatorRole: (state) => {
        return state.impersonator_role;
    },

    getImpersonatorUsername: (state) => {
        return state.impersonator_username;
    },

    getImpersonatorPage: (state) => {
        return state.impersonator_page;
    },

    getFirstImpersonatorId: (state) => {
        return state.first_impersonator_id;
    },

    getFirstImpersonatorRole: (state) => {
        return state.first_impersonator_role;
    },

    getFirstImpersonatorUsername: (state) => {
        return state.first_impersonator_username;
    },

    getFirstImpersonatorPage: (state) => {
        return state.first_impersonator_page;
    },

    getCompanyId: (state) => {
        return state.company_id;
    },

    getCompanySlug: (state) => {
        return state.company_slug;
    },

    getCompanyName: (state) => {
        return state.company_name;
    },

    getInactiveCompanyId: (state) => {
        return state.inactive_company_id;
    },

    getInactiveCompanyName: (state) => {
        return state.inactive_company_name;
    },

    getCompanyPlan: (state) => {
        return state.company_plan;
    },

    getCompanyStartYear: (state) => {
        return state.company_start_year;
    },

    getCompanyStopYear: (state) => {
        return state.company_stop_year;
    },

    getCompanySelection: (state) => (selection) => {
        return state.company_selections[selection];
    },

    getDriverProfile: (state) => {
        return state.driverProfile;
    },

    getPersonalVehicle: (state) => {
        return state.personalVehicle;
    },

    getDriverStreetLightId: (state) => {
        return state.driverProfile.street_light_id;
    },

    getDriverEmployeeNumber: (state) => {
        return state.driverProfile.employee_number;
    },

    getDriverStartDate: (state) => {
        return state.driverProfile.start_date;
    },

    getDriverStopDate: (state) => {
        return !state.driverProfile.stop_date ? '' : state.driverProfile.stop_date;
    },

    getMileageApprovalEndDay: (state) => {
        return state.mileage_approval_end_day;
    },

    getDriverPaymentDate: (state) => {
        return state.driver_payment_date;
    },

    getDriverJobTitle: (state) => {
        return state.driverProfile.job_title;
    },

    getDriverVehicleProfileId: (state) => {
        return state.driverProfile.vehicle_profile_id;
    },

    getDriverPersonalVehicleId: (state) => {
        return state.driverProfile.personal_vehicle_id;
    },

    getDriverCostCenter: (state) => {
        return state.driverProfile.cost_center;
    },

    getDriverMileageBandId: (state) => {
        return state.driverProfile.mileage_band_id;
    },

    getDriverTerritoryTypeId: (state) => {
        return state.driverProfile.territory_type_id;
    },

    getDriverTerritoryList: (state) => {
        return state.driverProfile.territory_list;
    },

    getDriverFuelCityId: (state) => {
        return state.driverProfile.fuel_city_id;
    },

    getDriverCarPolicyAccepted: (state) => {
        return state.driverProfile.car_policy_accepted;
    },

    getDriverReimbursementDetailId: (state) => {
        return state.driverProfile.reimbursement_detail_id;
    },

    getDriverMileageEntryMethod: (state) => {
        return state.driver_mileage_entry_method;
    },

    getDriverTimeZone: (state) => {
        return state.driver_time_zone;
    },


    getDriverStreetLightArray: (state) => {
        return state.driver_street_light_array;
    },


    getDriverMileageLockDay: (state) => {
        return state.driver_mileage_lock_day;
    },


    getDriverDivision: (state) => {
        return state.driverDivision;
    },

    getIsAccountExecutive: (state) => {
        return state.is_account_executive;
    },

    getIsInsuranceProcessor: (state) => {
        return state.is_insurance_processor;
    },

    getIsPaymentProcessor: (state) => {
        return state.is_payment_processor;
    },

    getIsServiceAssociate: (state) => {
        return state.is_service_associate;
    },

    getIsVehicleProfileApprover: (state) => {
        return state.is_vehicle_profile_approver;
    },

    getIsAnnouncementApprover: (state) => {
        return state.is_announcement_approver;
    },

    getCanViewEft: (state) => {
        return state.can_view_eft;
    },

    getCanViewLoginInformation: (state) => {
        return state.can_view_login_information;
    },

    getCanViewDatabaseTables: (state) => {
        return state.can_view_database_tables;
    },

    getIsDatabaseMaintainer: (state) => {
        return state.is_database_maintainer;
    },

    getIsBankMaintainer: (state) => {
        return state.is_bank_maintainer;
    },

    getIsCaaMaintainer: (state) => {
        return state.is_caa_maintainer;
    },

    getCompanyHasPredefinedStops: (state) => {
        return state.company_has_predefined_stops;
    },

    getCompanyUsesInsuranceModuleFilter: (state) => {
        return state.company_uses_insurance_module_filter;
    },

    getCompanyUsesQuarterlyTaxAdjustment: (state) => {
        return state.company_uses_quarterly_tax_adjustment;
    },

    getCompanyUsesMiroute: (state) => {
        return state.company_uses_miroute;
    },

    getIsTasksMaintainer: (state) => {
        return state.is_tasks_maintainer;
    },

    getHasAllPermissions: (state) => {
        return state.has_all_permissions;
    },

    getSearchParameter: (state) => {
        return state.search_parameter;
    },

    getLockAllInput: (state) => {
        return state.lock_all_input;
    },

    getCompanyReportLabels: (state) => {
        return state.company_report_labels;
    }
};

const mutations = {
    // system

    // start_time: no setter, set when store is instantiated

    updateView(state, newView) {
        // console.log('in store setting view to ' + newView);
        state.view = newView;
    },

    updateSubView(state, newSubView) {
        if (newSubView === 'null') newSubView = null;
        state.subView = newSubView;
    },

    // all users
    updateSessionCompanyId(state, newCompanyId) {
        if (parseInt(newCompanyId)) {
            state.session_company_id = parseInt(newCompanyId);
        }
    },

    updateSessionCompanyName(state, newCompanyName) {
        // console.log("setting name in store to " + newCompanyName);
        state.session_company_name = newCompanyName;
    },

    updateSessionCountryId(state, newCountryId) {
        //console.log('this worked with new country id ' + newCountryId);
        state.session_country_id = newCountryId;
    },

    updateSessionDivisionId(state, newDivisionId) {
        if (parseInt(newDivisionId)) {
            state.session_division_id = newDivisionId;
        }
    },

    updateSessionDivisionName(state, newDivisionName) {
        state.session_division_name = newDivisionName;
    },

    updateSessionUserId(state, newUserId) {
        if (parseInt(newUserId)) {
            state.session_user_id = newUserId;
        }
    },

    updateSessionDriverId(state, newDriverId) {
        // console.log('setting session_driver_id to ' + newDriverId);
        if (parseInt(newDriverId)) {
            state.session_driver_id = newDriverId;
        }
    },

    updateSessionManagerId(state, newManagerId) {
        if (parseInt(newManagerId)) {
            state.session_manager_id = newManagerId;
        }
    },

    updateSessionQuarter(state, newQuarter) {
        state.session_quarter = newQuarter;
    },

    updateSessionVehicleProfileId(state, newId) {
        if (parseInt(newId)) {
            state.session_vehicle_profile_id = newId;
        }
    },

    updateSessionAdministratorId(state, newAdministratorId) {
        if (parseInt(newAdministratorId)) {
            state.session_administrator_id = newAdministratorId;
        }
    },

    updateActiveRole(state, newRole) {
        state.active_role = newRole;
    },

    updateSessionRole(state, newRole) {
        state.session_role = newRole;
    },

    updateSessionCompanies(state, newCompanies) {
        state.session_companies = newCompanies;
    },

    updateSessionFromDate(state, newDate) {
        state.session_from_date = newDate;
    },

    updateSessionToDate(state, newDate) {
        state.session_to_date = newDate;
    },

    updateSessionYear(state, newYear) {
        state.session_year = newYear;
    },

    updateSessionMonth(state, newMonth) {
        state.session_month = newMonth;
    },

    updateSessionDay(state, newDay) {
        state.session_day = newDay;
    },

    updateSearchUserEmail(state, newEmail) {
        console.log('updating search_user_email to ' + newEmail);
        state.search_user_email = newEmail;
    },

    updateEditedUser(state, newUser) {
        state.editedUser = newUser;
    },

    updatePaymentResponsibilityCardata(state, newResponsibility) {
        state.payment_responsibility_cardata = newResponsibility;
    },

    updateCompanyCountryFilter(state, newFilter) {
        if (parseInt(newFilter)) {
            state.company_country_filter = newFilter;
        }
    },

    updateUserId(state, newUserId) {
        state.user_id = newUserId;
    },

    updateUsername(state, newUsername) {
        state.username = newUsername;
    },

    updateUserEmail(state, newEmail) {
        state.user_email = newEmail;
    },

    updateUserPersonalEmail(state, newEmail) {
        state.user_personal_email = newEmail;
    },

    updateUserType(state, newUserType) {
        state.user_type = newUserType;
    },

    updateUserAddress(state, newAddress) {
        state.userAddress = newAddress;
    },

    updateMenu(state, newMenu) {
        state.menu = newMenu;
    },

    updateIsDriver(state, newIsDriver) {
        state.is_driver = newIsDriver;
    },

    updateIsManager(state, newIsManager) {
        state.is_manager = newIsManager;
    },

    updateIsAdministrator(state, newIsAdministrator) {
        state.is_administrator = newIsAdministrator;
    },

    updateIsSuper(state, newIsSuper) {
        state.is_super = newIsSuper;
    },

    updateActiveManagerProfile(state, newProfile) {
        state.activeManagerProfile = newProfile;
    },

    updateInactiveManagerProfile(state, newProfile) {
        state.inactiveManagerProfile = newProfile;
    },

    updateActiveAdministratorProfile(state, newProfile) {
        state.activeAdministratorProfile = newProfile;
    },

    updateInactiveAdministratorProfile(state, newProfile) {
        state.inactiveAdministratorProfile = newProfile;
    },

    updateActiveManagerId(state, newActiveManagerId) {
        state.active_manager_id = newActiveManagerId;
    },

    updateInactiveManagerId(state, newInactiveManagerId) {
        state.inactive_manager_id = newInactiveManagerId;
    },

    updateManagerDrivers(state, newDrivers) {
        state.managerDrivers = newDrivers;
    },

    updateActiveAdministratorId(state, newActiveAdministratorId) {
        state.active_administrator_id = newActiveAdministratorId;
    },

    updateInactiveAdministratorId(state, newInactiveAdministratorId) {
        state.inactive_administrator_id = newInactiveAdministratorId;
    },

    updateUserFullName(state, newUserName) {
        state.user_full_name = newUserName;
    },

    updateHighestAuthenticatedUserType(state, newHighestType) {
        state.highest_authenticated_user_type = newHighestType;
    },

    updateCountryId(state, newCountryId) {
        state.country_id = newCountryId;
    },

    updateImpersonatorId(state, newId) {
        state.impersonator_id = newId;
    },

    updateImpersonatorRole(state, newType) {
        state.impersonator_role = newType;
    },

    updateImpersonatorUsername(state, newName) {
        state.impersonator_username = newName;
    },

    updateImpersonatorPage(state, newPage) {
        state.impersonator_page = newPage;
    },

    updateFirstImpersonatorId(state, newId) {
        state.first_impersonator_id = newId;
    },

    updateFirstImpersonatorRole(state, newType) {
        state.first_impersonator_role = newType;
    },

    updateFirstImpersonatorUsername(state, newName) {
        state.first_impersonator_username = newName;
    },

    updateFirstImpersonatorPage(state, newPage) {
        state.first_impersonator_page = newPage;
    },

    updateCompanyId(state, newCompanyId) {
        state.company_id = newCompanyId;
    },

    updateCompanySlug(state, newCompanySlug) {
        state.company_slug = newCompanySlug;
    },

    updateCompanyName(state, newCompanyName) {
        state.company_name = newCompanyName;
    },

    updateInactiveCompanyId(state, newInactiveCompanyId) {
        state.inactive_company_id = newInactiveCompanyId;
    },

    updateInactiveCompanyName(state, newInactiveCompanyName) {
        state.inactive_company_name = newInactiveCompanyName;
    },

    updateCompanyPlan(state, newPlan) {
        state.company_plan = newPlan;
    },

    updateCompanyStartYear( state, year) {
        state.company_start_year = year;
    },

    updateCompanyStopYear( state, year) {
        state.company_stop_year = year;
    },

    updateCompanySelections(state, newSelections) {
        state.company_selections = newSelections;
    },

    updateDriverProfile(state, newProfile) {
        state.driverProfile = newProfile;
    },

    updatePersonalVehicle(state, newPersonalVehicle) {
        state.personalVehicle = newPersonalVehicle;
    },

    updateDriverMileageEntryMethod(state, newEntryType) {
        state.driver_mileage_entry_method = newEntryType;
    },

    updateDriverTimeZone(state, newTimeZone) {
        state.driver_time_zone = newTimeZone;
    },

    updateDriverStreetLightArray(state, newArray) {
        state.driver_street_light_array = newArray;
    },

    updateDriverMileageLockDay(state, newDay) {
        state.driver_mileage_lock_day = newDay;
    },

    updateDriverDivision(state, newDiv) {
        state.driverDivision.division_id = newDiv.division_id;
        state.driverDivision.division_name = newDiv.division_name;
    },

    updateIsAccountExecutive: (state, bool) => {
        state.is_account_executive = bool;
    },

    updateIsInsuranceProcessor: (state, bool) => {
        state.is_insurance_processor = bool;
    },

    updateCompanyHasPredefinedStops(state, newVal) {
        state.company_has_predefined_stops = newVal;
    },

    updateCompanyUsesInsuranceModuleFilter(state, newVal) {
        state.company_uses_insurance_module_filter = newVal;
    },

    updateCompanyUsesQuarterlyTaxAdjustment(state, newVal) {
        state.company_uses_quarterly_tax_adjustment = newVal;
    },

    updateCompanyUsesMiroute(state, newVal ) {
        state.company_uses_miroute = newVal;
    },

    updateIsPaymentProcessor: (state, bool) => {
        state.is_payment_processor = bool;
    },

    updateIsServiceAssociate: (state, bool) => {
        state.is_service_associate = bool;
    },

    updateIsVehicleProfileApprover: (state, bool) => {
        state.is_vehicle_profile_approver = bool;
    },

    updateIsAnnouncementApprover: (state, bool) => {
        state.is_announcement_approver = bool;
    },

    updateCanViewEft: (state, bool) => {
        state.can_view_eft = bool;
    },

    updateCanViewLoginInformation: (state, bool) => {
        state.can_view_login_information = bool;
    },

    updateCanViewDatabaseTables: (state, bool) => {
        state.can_view_database_tables = bool;
    },

    updateIsDatabaseMaintainer: (state, bool) => {
        state.is_database_maintainer = bool;
    },

    updateIsBankMaintainer: (state, bool) => {
        state.is_bank_maintainer = bool;
    },

    updateIsCaaMaintainer: (state, bool) => {
        state.is_caa_maintainer = bool;
    },

    updateIsTasksMaintainer: (state, bool) => {
        state.is_tasks_maintainer = bool;
    },

    updateHasAllPermissions: (state, bool) => {
        state.has_all_permissions = bool;
    },

    updateMileageApprovalEndDay: (state, newDay) => {
        state.mileage_approval_end_day = newDay;
    },

    updateDriverPaymentDate: (state, newDate) => {
        state.driver_payment_date = newDate;
    },

    updateSearchParameter: (state, param) => {
        state.search_parameter = param;
    },

    updateLockAllInput: (state, param) => {
        state.lock_all_input = param;
    },

    updateCompanyReportLabels: (state, param) => {
        state.company_report_labels = param;
    }
};

export default new Vuex.Store({
    state,
    getters,
    mutations
});