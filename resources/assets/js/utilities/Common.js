import store from '../store.js';
import months from '../models/Months';
import {EventBus} from "../event-bus";
import moment from 'moment';

export default class Common {
    constructor() {
        this.vue = new Vue({
            store
        });

        this.months = months;

        // use this object to keep track of existing sorts
        this.existing_sorts = {};
        // whenever the view us changed dump the existing sorts
        EventBus.$on('view-changed', () => {
            this.existing_sorts = {};
        })
    }

    /**
     * This function acts like store commit, and updates the store, but does not update session.
     * It is used so that we can update our local ledger (the store) and send data to the server, but have the server
     * compare its past session with our current values.
     *
     * Example use case:
     *  We are on a report and just changed company id in some component, and had a year of 2015 in session
     *  We want to be able to access that change in company immediately on the front end, by using the store so we
     *  can send the data to the server. The front end doesn't want to wait for another request checking the validity
     *  of the year to resolve, instead we push that logic to the server, and it gets the company id, invalid year,
     *  and does what it needs to do.
     *
     *
     * @param func
     * @param data
     */
    simpleCommit(func, data) {
        this.vue.$store.commit(func, data);
    }

    storeCommit(func, data) {
        return new Promise((resolve, reject) => {
            // view is a special case
            if (func === 'view') {
                console.log('setting this view in Common', data);
                this.vue.$store.commit('updateView', data.view);
                this.vue.$store.commit('updateSubView', data.subView);

                // now set the view and subView in session
                axios.post('/setView', {view: data.view, subView: data.subView});
            } else {
                this.vue.$store.commit(func, data);

                // the following are exceptions that we DO want in session
                if (func === 'updateSessionCompanyId') {
                    console.log('common: setting session_company_id', data);
                    this.setSession('session_company_id', data);
                }
                if (func === 'updateSessionCompanyName') {
                    this.setSession('session_company_name', data);
                }
                if (func === 'updateSessionYear') {
                    this.setSession('session_year', data);
                }
                if (func === 'updateSessionMonth') {
                    this.setSession('session_month', data);
                }
                if (func === 'updateSessionDay') {
                    this.setSession('session_day', data);
                }
                if (func === 'updateSessionUserId') {
                    this.setSession('session_user_id', data);
                }
                if (func === 'updateSearchUserEmail') {
                    this.setSession('search_user_email', data);
                }
                if (func === 'updateSessionDriverId') {
                    console.log('updating session driver id ' + data);
                    this.setSession('session_driver_id', data);
                }
                if (func === 'updateSessionRole') {
                    this.setSession('session_role', data);
                }
                if (func === 'updateEditedUser') {
                    this.setSession('editedUser', data);
                }
                if (func === 'updateSessionCountryId') {
                    this.setSession('session_country_id', data);
                }
                if (func === 'updateSessionVehicleProfileId') {
                    this.setSession('session_vehicle_profile_id', data);
                }
            }
            resolve();
        });
    }

    storeGet(func) {
        let state = this.vue.$store.state;
        return new Promise((resolve, reject) => {
            switch (func) {
                case 'getUserType':
                    resolve(state.user_type);
            }
        });
    }

    /**
     * Navigates to given view and page
     * Wrapper for fire function
     * - page will default in View if not supplied
     *
     * @param view
     * @param subView
     */
    setView(view, subView) {
        subView = subView === 'null' ? null : subView;
        window.location = '#' + view + '/' + subView;
        this.storeCommit('view', {view: view, subView: subView});
        //this.storeCommit('updateSessionCompanyId', this.vue.$store.state.session_company_id);
        console.log("setView", view);
        EventBus.$emit('view-changed');
        EventBus.$emit('subView-changed', subView);
    }

    /**
     * Sets a subview, using the view currently in store
     * @param subView
     */
    setSubView(subView) {
        subView = subView === 'null' ? null : subView;
        let obj = {view: this.vue.$store.state.view, subView: subView};
        //console.log('subView set to ' + subView, obj);
        this.setView(obj.view, obj.subView);
        //this.storeCommit('view', obj);
        //EventBus.$emit('subView-changed', subView);
    }

    /**
     * Fires given event and passes optional data
     * - useType will prepend the user type and hyphen to the event (e.g. 'manager-[EVENT]')
     *
     * @param event
     * @param data
     * @param useType
     */
    fire(event, data = null, useType = null) {

        if (typeof useType !== 'undefined' && useType !== null) {
            axios.get('/getMe').then(response => {
                var myEvent = response.data.user.userType.charAt(0).toUpperCase() + response.data.user.userType.slice(1) + '-' + event;

                this.vue.$emit(myEvent, data);
            });
        }
        else {
            this.vue.$emit(event, data);
        }

    }

    /**
     * Registers listener for supplied event with supplied callback
     *
     * @param event
     * @param callback
     */
    listen(event, callback) {
        //console.log('Events listening ' + event);
        this.vue.$on(event, callback);
    }

    checkCommit(func, data) {
        // console.log('in checkCommit', func, data);
        if (!this.isEmpty(data)) {
            //console.log('setting ' + func + ' to ' + data);
            this.storeCommit(func, data);
        }
    }

    processInitialValues(data) {
        let start = new Date();
        // these values will be the same for all user types
        this.checkCommit('updateUserType', data.user_type);
        this.checkCommit('updateUserId', data.user_id);
        this.checkCommit('updateUsername', data.username);
        this.checkCommit('updateUserEmail', data.user_email);
        this.checkCommit('updateUserPersonalEmail', data.user_personal_email);
        this.checkCommit('updateUserFullName', data.user_full_name);
        this.checkCommit('updateIsDriver', data.is_driver);
        this.checkCommit('updateIsManager', data.is_manager);
        this.checkCommit('updateIsAdministrator', data.is_administrator);
        this.checkCommit('updateIsSuper', data.is_super);
        this.checkCommit('updateView', data.view);
        this.checkCommit('updateSubView', data.subView);
        this.checkCommit('updateImpersonatorId', data.impersonator_id);
        this.checkCommit('updateImpersonatorRole', data.impersonator_role);
        this.checkCommit('updateImpersonatorUsername', data.impersonator_username);
        this.checkCommit('updateImpersonatorPage', data.impersonator_page);
        this.checkCommit('updateFirstImpersonatorId', data.first_impersonator_id);
        this.checkCommit('updateFirstImpersonatorRole', data.first_impersonator_role);
        this.checkCommit('updateFirstImpersonatorUsername', data.first_impersonator_username);
        this.checkCommit('updateFirstImpersonatorPage', data.first_impersonator_page);
        this.checkCommit('updateUserAddress', data.userAddress);
        this.checkCommit('updateMenu', data.menu);
        this.checkCommit('updateSessionCompanyId', data.session_company_id);
        this.checkCommit('updateCompanyPlan', data.company_plan);
        this.checkCommit('updateCompanySlug', data.company_slug);
        this.checkCommit('updateSessionYear', data.session_year);
        this.checkCommit('updateSessionMonth', data.session_month);
        this.checkCommit('updateSessionDay', data.session_day);
        this.checkCommit('updateSessionCompanies', data.session_companies);
        this.checkCommit('updateSessionDriverId', data.session_driver_id);
        this.checkCommit('updateSessionUserId', data.session_user_id);
        console.log('process initial values session driver id ' + data.session_driver_id);
        this.checkCommit('updateActiveRole', data.active_role);
        this.checkCommit('updateSessionRole', data.session_role);
        this.checkCommit('updateHighestAuthenticatedUserType', data.highest_authenticated_user_type);
        this.checkCommit('updateCompanyReportLabels', data.company_report_labels);

        switch (data.user_type) {
            case 'super':
                this.checkCommit('updateSessionCompanyName', data.session_company_name);
                this.checkCommit('updateIsAccountExecutive', data.is_account_executive);
                this.checkCommit('updateIsInsuranceProcessor', data.is_insurance_processor);
                this.checkCommit('updateIsPaymentProcessor', data.is_payment_processor);
                this.checkCommit('updateIsServiceAssociate', data.is_service_associate);
                this.checkCommit('updateIsVehicleProfileApprover', data.is_vehicle_profile_approver);
                this.checkCommit('updateIsAnnouncementApprover', data.is_announcement_approver);
                this.checkCommit('updateCanViewEft', data.can_view_eft);
                this.checkCommit('updateCanViewLoginInformation', data.can_view_login_information);
                this.checkCommit('updateCanViewDatabaseTables', data.can_view_database_tables);
                this.checkCommit('updateIsDatabaseMaintainer', data.is_database_maintainer);
                this.checkCommit('updateIsBankMaintainer', data.is_bank_maintainer);
                this.checkCommit('updateIsCaaMaintainer', data.is_caa_maintainer);
                this.checkCommit('updateIsTasksMaintainer', data.is_tasks_maintainer);
                this.checkCommit('updateHasAllPermissions', data.has_all_permissions);
                this.checkCommit('updateEditedUser', data.editedUser);
                console.log('UPDATE EDITED USER', data.editedUser);
                this.checkCommit('updateCompanySelections', data.company_selections);
                this.checkCommit('updateSearchUserEmail', data.search_user_email);
                console.log('HERE', data);
                this.checkCommit('updateSessionVehicleProfileId', data.session_vehicle_profile_id);
                break;
            case 'driver':
                this.checkCommit('updateDriverMileageLockDay', data.mileage_lock_day);
                // this.checkCommit('updateDriverMileageLockDay', data.mileage_lock_driver_day);
                this.checkCommit('updateCompanyId', data.company_id);
                this.checkCommit('updateSessionCompanyId', data.company_id);
                this.checkCommit('updateCompanyName', data.company_name);
                this.checkCommit('updateSessionCompanyName', data.company_name);
                this.checkCommit('updateDriverMileageEntryMethod', data.mileage_entry_method);
                //console.log('got company plan', data.company_plan);
                this.checkCommit('updateCompanyPlan', data.company_plan);
                this.checkCommit('updateDriverDivision', data.division);
                this.checkCommit('updateCountryId', data.country_id);
                this.checkCommit('updateCompanySelections', data.company_selections);
                this.checkCommit('updateDriverTimeZone', data.driver_time_zone);
                this.checkCommit('updateDriverStreetLightArray', data.driver_street_light_array);
                this.checkCommit('updateDriverProfile', data.driverProfile);
                this.checkCommit('updatePersonalVehicle', data.personalVehicle);
                this.checkCommit('updateMileageApprovalEndDay', data.mileage_approval_end_day);
                this.checkCommit('updateDriverPaymentDate', data.driver_payment_date);
                break;
            case 'manager':
                // console.log('i am a manager and i see this', data);
                this.checkCommit('updateActiveManagerId', data.active_manager_id);
                this.checkCommit('updateCompanyId', data.company_id);
                this.checkCommit('updateCountryId', data.country_id);
                this.checkCommit('updateSessionCompanyId', data.company_id);
                this.checkCommit('updateCompanyName', data.company_name);
                this.checkCommit('updateSessionCompanyName', data.company_name);
                this.checkCommit('updateActiveManagerProfile', data.activeManagerProfile);
                this.checkCommit('updateInactiveManagerProfile', data.inactiveManagerProfile);
                this.checkCommit('updateManagerDrivers', data.managerDrivers);
                this.checkCommit('updateCompanySelections', data.company_selections);
                break;
            case 'administrator':
                this.checkCommit('updateActiveAdministratorId', data.active_administrator_id);
                this.checkCommit('updateCompanyId', data.company_id);
                this.checkCommit('updateSessionCompanyId', data.company_id);
                this.checkCommit('updateCompanyName', data.company_name);
                this.checkCommit('updateSessionCompanyName', data.company_name);
                this.checkCommit('updateCompanySelections', data.company_selections);
                this.checkCommit('updateActiveAdministratorProfile', data.activeAdministratorProfile);
                this.checkCommit('updateInactiveAdministratorProfile', data.inactiveAdministratorProfile);
                this.checkCommit('updateEditedUser', data.editedUser);
                break;
        }
        //alert("Process initial data in " + this.getTimeRunning(start));
    }

    /**
     * Return authenticated user values from UserController
     *
     * All parameters are optional
     *
     * - With no parameters, return logged in user
     * - With user_id specified, get user from given id in `users` table
     * - With user_id and user_type_id specified, get the appropriate profile (Driver, Manager, Administrator) and
     *   derive the user from that profile (assume user_id is manager_id in ManagerProfile, etc.)
     *
     * @param set_in_session
     * @param user_id
     * @param user_type_id
     * @returns object
     */
    getMe(set_in_session = 1, user_id = null, user_type_id = null) {
        //console.log('set_in_session, user_id, user_type_id ', set_in_session, user_id, user_type_id);
        // if user_type_id is -1, this is a false alarm called before the id was set in the client-side code
        if (user_type_id === -1 || user_type_id === '-1') {
            return null;
        }

        //console.log(user_id, 'empty_user_id', this.isEmpty(user_id));

        // append parameters
        let url = '/getUser';
        if (!this.isEmpty(user_id)) {
            //console.log('i should be setting user id here', user_id);
            url += '/' + user_id;
        }
        if (!this.isEmpty(user_type_id)) {
            url += '/' + user_type_id;
        }

        //console.log('using this method', url);

        // return promise of user data
        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {
                    let user = response.data.user;
                    //console.log('response in getme', user);

                    // this.storeCommit('updateView', user.view);
                    // this.storeCommit('updateSubView', user.subView);

                    if (false /** set_in_session == 1 */) {
                        let s6 = this.storeCommit('updateDriverMileageLockDay', user.mileage_lock_day);

                        Promise.all([s6]).then(response => {
                            if (user.impersonator_id) {
                                this.storeCommit('updateImpersonatorId', user.impersonator_id);
                            }

                            if (user.driverProfile) {
                                this.storeCommit('updateCompanyId', user.driverProfile.company_id);
                                this.storeCommit('updateCompanyName', user.driverProfile.company_name);
                                this.storeCommit('updateDriverMileageEntryMethod', user.mileage_entry_method);
                                axios.get('/getDriverDivision/' + user.id).then(response => {
                                    let division = {
                                        'division_id': response.data.division_id,
                                        'division_name': response.data.division_name
                                    };
                                    this.storeCommit('updateDriverDivision', division);
                                });
                                axios.get('/getCompanyCountry/' + user.driverProfile.company_id).then(response => {
                                    this.storeCommit('updateCountryId', response.data.country_id);
                                });
                                axios.get('/getCompanySelections/' + user.driverProfile.company_id).then(response => {
                                    this.storeCommit('updateCompanySelections', response.data);
                                });
                            }
                        });

                        if (user.administratorProfiles.length) {
                            this.storeCommit('updateActiveAdministratorId', user.administratorProfiles[0].administrator_id);
                        }

                        if (user.managerProfiles.length) {
                            this.storeCommit('updateActiveManagerId', user.managerProfiles[0].manager_id);
                        }

                        if (user.is_super) {
                            this.storeCommit('updateSessionCompanyId', user.session_company_id);
                        }

                        if (!this.isEmpty(user.company_plan)) {
                            this.storeCommit('updateCompanyPlan', user.company_plan);
                        }

                        axios.get('/getHighestAuthenticatedUserType').then(response => {
                            this.storeCommit('updateHighestAuthenticatedUserType', response.data);
                        });

                    }

                    resolve(response.data);
                })
                .catch(error => {
                    // console.log(error);
                    //reject(error)
                });
        });
    }

    /**
     * Find session value for param
     *
     * @param param : string
     */
    getSession(param) {
        return new Promise((resolve, reject) => {
            axios.post('/getSessionValues', {keys: [param]})
                .then(response => {
                    let val = this.isEmpty(response.data[param]) ? -1 : response.data[param];
                    resolve(val);
                })
                .catch(error => {
                    reject(error.response.data);
                });
        });
    }

    /**
     * Return object of key-value pairs for multiple session values
     *
     * @param arr : array
     * @returns Object
     */

    /*getMultipleSessionValues(arr) {
     let vals = {};
     for (let field in arr) {
     vals[field] = this.getSession(field);
     }
     //// console.log('in the common function');
     //// console.log(vals);

     return new Promise((resolve, reject) => {
     resolve(vals);
     });
     }*/

    /**
     * When switching between profiles, current link may not be valid for new user type,
     * will redirect to default view.
     *
     * @param link
     * @param linkInfo
     * @returns {boolean}
     */
    validLink(link, linkInfo) {
        //console.log('validLink testing', link, linkInfo);
        let isValid = false;
        let categoryIndex, linkIndex;

        //console.log('LINK INFO');
        //console.log(linkInfo);
        if (!this.isEmpty(link) && !this.isEmpty(linkInfo)) {
            for (categoryIndex = 0; categoryIndex < linkInfo.length; categoryIndex++) {
                for (linkIndex = 0; linkIndex < linkInfo[categoryIndex].data.length; linkIndex++) {
                    if (link === linkInfo[categoryIndex].data[linkIndex].id) {
                        isValid = true;
                    }
                }
            }

        }

        return isValid;
    }

    /**
     * Test for links not explicitly added to menu but are a subpage, menu will be informed
     * which parent link should be highlighted.
     *
     * @param link
     * @param data
     * @returns {*}
     */
    correctLink(link, data) {
        let correctLink = link;

        let i;
        for (i = 0; i < data.length; i++) {
            if (data[i].id === link) {
                correctLink = data[i].subpage_of;
            }
        }

        return correctLink;
    }

    compareValues(key, order = 'asc') {

        return function (a, b) {

            if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
                // property doesn't exist on either object
                return 0;
            }

            const varA = (typeof a[key] === 'string') ?
                a[key].toUpperCase() : a[key];
            const varB = (typeof b[key] === 'string') ?
                b[key].toUpperCase() : b[key];

            let comparison = 0;
            if (varA > varB) {
                comparison = 1;
            } else if (varA < varB) {
                comparison = -1;
            }

            return (
                (order == 'desc') ? (comparison * -1) : comparison
            );
        };
    }

    localeString(number, separator, group_size) {
        let split_number = ('' + number).split('.'), s = '', i, j;

        separator || (separator = ',');

        group_size || group_size === 0 || (group_size = 3);

        i = split_number[0].length;
        while (i > group_size) {
            j = i - group_size;
            s = separator + split_number[0].slice(j, i) + s;
            i = j;
        }

        s = split_number[0].slice(0, i) + s;
        split_number[0] = s;

        return split_number.join('.');
    }


    flattenCompany(company) {
        let newCompany = {};
        for (let item in company) {

            if (typeof company[item] !== 'object') {
                newCompany[item] = company[item];
            }
            else {
                if (item == 'deleted_at') newCompany[item] = company[item]; // null will show as object
                else {
                    for (let field in company[item]) {
                        newCompany[field] = company[item][field];
                    }
                }
            }
        }
        return newCompany;
    }

    flattenUser(user) {
        let newUser = {};
        for (let item in user) {
            if (typeof user[item] !== 'object') {
                newUser[item] = user[item];
            }
            else {
                if (item == 'deleted_at') newUser[item] = user[item]; // null will show object
                else {
                    for (let field in user[item]) {
                        newUser[field] = user[item][field];
                    }
                }
            }
        }
        return newUser;
    }

    /**
     * Sets key to value in session
     *
     * @param key
     * @param val
     */
    setSession(key, val) {
        //console.log('in setSession key='+key+' and val='+val);
        return new Promise((resolve, reject) => {
            axios.post('/setSession', {key: key, value: val}).then(response => {
                resolve();
            });
        });
    }

    daySuffix(day) {
        switch (day) {
            case 1:
                return 'st';
                break;
            case 2:
                return 'nd';
                break;
            case 3:
                return 'rd';
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 0:
                return 'th';
                break;
            default:
                return '';

        }
    }

    /**
     * Will return true or false based on whether the given date is a holiday
     * (currently it does not return true for OPTIONAL holidays) TODO: add optional param
     *
     * @param date
     * @param country_id
     * @param state_province_id
     * @returns {Promise}
     */
    isHoliday(date, country_id = null, state_province_id = null) {

        let url = '/isHoliday/' + date;

        if (country_id != null && state_province_id != null) {
            url += '/' + country_id + '/' + state_province_id;
        }

        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {
                    resolve(response.data.is_holiday);
                })
                .catch(error => {
                    reject(error.response.data.is_holiday);
                });
        });
    }

    /**
     * Determines which dates in an array of dates are holidays TODO: optional flag
     *
     * @param dates
     * @param country_id
     * @param state_province_id
     * @returns {Promise}
     *
     * Note: datePairs will be an array of objects
     *      ex.
     *          [  { date: Y-m-d, is_holiday: boolean} , ... ]
     */
    areAnyHolidays(dates, country_id = null, state_province_id = null) {

        let url = '/areAnyHolidays/' + dates;

        if (country_id != null && state_province_id != null) {
            url += '/' + country_id + '/' + state_province_id;
        }

        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {
                    resolve(response.data.datePairs);
                })
                .catch(error => {
                    reject(error.response.data.datePairs);
                });
        });
    }

    stricmp(str1, str2) {
        if (this.isEmpty(str1) || this.isEmpty(str2)) {
            return false;
        }
        return _.toLower(str1) === _.toLower(str2);
    }

    in_array(val, arr) {
        for (let field in arr) {
            if (arr[field] === val) {
                return true;
            }
        }
        return false;
    }

    /**
     * TODO: add other permissions as required
     *
     * For now only avaiable for super permissions!
     *
     * Checks to see if the current user has the given permission
     * @param permission
     *     same column name as on the table, ex. is_account_executive on super_profiles
     * @returns boolean (true if user has permission, else false)
     */
    checkUserPermission(permission) {
        let url = 'checkUserPermission/' + this.vue.$store.state.user_id + '/' + this.vue.$store.state.user_type + '/' + permission;

        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {
                    //// console.log(response.data)
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response.data);
                });
        });
    }

    getDuration(time) {
        return (time - this.vue.$store.state.start_time) / 1000;
    }


    logDuration(label) {
        // console.log(label + ' after ' + this.getDuration(new Date().getTime()) + ' sec.');
    }

    splitWordOnUppercase(whole_word) {
        if (this.isEmpty(whole_word)) {
            return false;
        }
        for (let letter in whole_word) {
            if (whole_word[letter] == whole_word[letter].toUpperCase()) {
                let first = whole_word.slice(0, letter) === '' ? null : whole_word.slice(0, letter);
                let last = whole_word.slice(letter);
                return [first, last];
            }
        }
        return false;
    }

    isEmpty(thing) {
        //console.log('got this thing ', thing, 'of type ', typeof thing);
        let truthy = false;
        switch (typeof thing) {
            case 'null':
            case 'undefined':
                truthy = true;
                break;
            case 'boolean':
            case 'function':
                truthy = false;
                break;
            case 'object':
                truthy = _.isEmpty(thing);
                break;
            case 'string':
                truthy = thing === 'null' ? true : !thing;
                break;
            case 'number':
                // special for Fusion, we want the number to be not -1
                truthy = thing === -1;
                break;
        }
        return truthy;
    }

    isInt(thing) {
        return !isNaN(parseInt(thing));
    }

    formatCapitalCost(capital_cost) {
        switch (capital_cost) {
            case 'blue_book':
                return 'Blue Book';
            case 'msrp':
                return 'MSRP';
            case 'invoice':
                return 'Invoice';
        }
    }

    getVehicleCategory(category_id) {
        let url = '/getVehicleCategory/' + category_id;
        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {
                    //// console.log(response.data)
                    resolve(response.data.category);
                })
                .catch(error => {
                    reject(error.response.data);
                });
        });
    }

    getResaleCondition(resale_condition_id) {
        let id = Number(resale_condition_id);
        switch (id) {
            case 1:
                return 'Trade-In Excellent';
            case 2:
                return 'Trade-In Good';
            case 3:
                return 'Trade-In Fair';
            case 4:
                return 'Wholesale';
            case 5:
                return 'Retail';
            case 6:
                return 'Private Party Excellent';
            case 7:
                return 'Private Party Good';
            case 8:
                return 'Private Party Fair';
            default:
                return 'Error - Invalid Resale Condition';
        }
    }

    getMileageBand(mileage_band_id) {
        switch (mileage_band_id) {
            case 1:
                return '2,500 - 5,000';
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                let mileage_start = (mileage_band_id - 1) * 5000;
                let mileage_end = mileage_start + 5000;
                return mileage_start.toLocaleString(undefined, {maximumFractionDigits: 0}) + ' - ' + mileage_end.toLocaleString(undefined, {maximumFractionDigits: 0});
            case 11:
                return '50,000+';
            default:
                return 'Error - Invalid Mileage Band';
        }
    }

    roundNumber(number, precision = 0) {
        let x = Number(number);
        return x.toFixed(precision);
    }

    addCommaSeparator(number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    isValidZipCode(zip, state_id = null) {
        if (zip.search(/^\d{5}([\-]?\d{4})?$/) == -1) {
            return false;
        }

        if (state_id) {
            return new Promise((resolve, reject) => {
                axios.get('/getGeoDataFromZipPostal/' + 1 + '/' + zip + '/' + true).then(response => {
                    resolve(response.data.state_id == state_id);
                }).catch(errors => {
                    reject();
                });
            });

        } else {
            return true;
        }
    }

    isValidPostalCode(postal, province_id = null) {
        if (postal.search(/^([a-zA-Z]\d[a-zA-Z]).*$/) == -1) {
            return false;
        }

        if (province_id) {
            console.log('CHECKING IS VALID ZIP POSTAL');
            console.log('province is ' + province_id);
            return new Promise((resolve, reject) => {
                axios.get('/getGeoDataFromZipPostal/' + 2 + '/' + postal + '/' + true)
                    .then(response => {
                        console.log('province is ' + province_id);
                        console.log(province_id == response.data.province_id);
                        resolve(response.data.province_id == province_id);
                    }).catch(errors => {
                    reject();
                });
            });
        } else {
            return true;
        }
    }

    capitalizeFirstLetter(word) {
        return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    }

    isValidDate(val) {
        let d = moment(val);
        return d.isValid();
    }

// Provides the data for displayItems within Menu Pages
    createMenuPageDataRev(getMenu, getMenuName) {
        let menuData = this.getMenuPageData(getMenu, getMenuName);
        let menuItems = menuData.data;
        let menuIcon = menuData.menuIcon;
        let displayItems = [];
        let tempItem = null;
        // dropDownMenu.testMethod();
        for (let index in menuItems) {
            // If label exists
            if (menuItems[index].label) {
                // if the label has been set previously push the item and clear with next label
                if (tempItem != null) {
                    displayItems.push(tempItem);
                }

                // push into temp since there is no value in tempItem
                tempItem = {icon: menuIcon, label: menuItems[index].label, items: []};
            } else {
                tempItem.items.push(menuItems[index]);
            }
        }
        displayItems.push(tempItem);
        return [displayItems, menuIcon];
    }
    getMenuPageColor(getMenu, getMenuName){
        return this.getMenuPageData(getMenu, getMenuName).color;
    }

// This will provide the correct data set for createMenuPageDataRev() to parse though
    getMenuPageData(getMenu, menu_name) {
        for (let category in getMenu) {
            if (menu_name === getMenu[category].name) {
                return getMenu[category];
            }
        }
    }

    /**
     * Reproduced from PHP function in conversion/helpers
     *
     * @param start
     */
    getTimeRunning(start) {
        let totalTime = '';
        let end = new Date();
        let elapsed = (end - start) / 1000;
        let minutes = elapsed / 60;
        let hours = minutes / 60;

        if (elapsed < 60) {
            totalTime = elapsed + ' seconds';
        } else if (elapsed < 3600) {
            totalTime = minutes + ' min ' + (elapsed % 60) + ' seconds';
        } else {
            totalTime = hours + ' hours ' + (minutes % 60) + ' min ' + (elapsed % 60) + ' seconds';
        }

        return totalTime;
    }


    /**
     * This function will force a download of a file from the response block of an ajax request.
     * Given a filename (this is just what you want to call the file) and a blob type.
     *
     * @param data (the data from the ajax response object ex. response.data)
     * @param filename (string with extension)
     * @param blob_type (string)
     */
    downloadFileResponse(data, filename, blob_type) {
        let blob = new Blob([data], {type: blob_type});

        if (window.navigator && window.navigator.msSaveOrOpenBlob) { // for IE
            window.navigator.msSaveOrOpenBlob(blob, filename);
        } else {
            let downloadUrl = URL.createObjectURL(blob);
            let a = document.createElement("a");
            a.href = downloadUrl;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
        }
    }

    /**
     * Get a month name from an id (1 = Jan, 2 = Feb, ...)
     *
     * @param month_id
     * @param short_name - optional boolean (true returns Jan for month_id 1 instead of January
     * @returns {*}
     */
    getMonthName(month_id, short_name = false)
    {
        return this.months.find(x => x.id == month_id)[(short_name ? 'short_name' : 'name')];
    }

    getReportLabel(key, company_id = null) {

        let label = this.vue.$store.state.company_report_labels.find(x => x.key == key);

        if (!this.isEmpty(label)) {
            return label.value;
        } else {
            if (this.isEmpty(company_id)) {
                return key;
            }

            axios.get('/getReportLabel/' + key + '/' + company_id).then( response => {
                return response.data;
            })
        }
    }

    getCompanyReportLabels(company_id) {
        return new Promise((resolve) => {
            if (this.isEmpty(company_id)) resolve([]);
            axios.get('/getCompanyReportLabels/' + company_id).then( response => {
                resolve(response.data);
            }).catch( error => {
                console.log(error);
                resolve([]);
            })
        })
    }

    /**
     * Note rows is passed by reference
     *
     * @param col - the data key in our rows object, ex.
     *  col could be 'first_name' if
     * rows = [
     *      {first_name: asdf},
     *      {first_name: bbbb},
     *  ]
     * @param rows
     */
    sortByNumber(col, rows) {
        if (!(col in this.existing_sorts)) this.existing_sorts[col] = 1;
        if (!this.existing_sorts[col]) {
            rows.sort(function (a, b) {
                return a[col] - b[col];
            });
        } else {
            rows.sort(function (a, b) {
                return b[col] - a[col];
            });
        }
        this.existing_sorts[col] = Math.abs(1 - this.existing_sorts[col]);
    }

    /**
     * Note rows is passed by reference
     *
     * @param col - the data key in our rows object, ex.
     *  col could be 'first_name' if
     * rows = [
     *      {first_name: asdf},
     *      {first_name: bbbb},
     *  ]
     * @param rows
     */
    sortByWord(col, rows) {
        console.log(this.existing_sorts);
        if (!(col in this.existing_sorts)) this.existing_sorts[col] = 1;
        if (!this.existing_sorts[col]) {
            rows.sort(function (a, b) {
                let wordA = a[col].trim().toLowerCase();
                let wordB = b[col].trim().toLowerCase();

                if (wordA < wordB) {
                    return -1;
                }
                if (wordA > wordB) {
                    return 1;
                }
                // words must be equal
                return 0;
            });
        } else {
            rows.sort(function (a, b) {
                let wordA = a[col].trim().toLowerCase();
                let wordB = b[col].trim().toLowerCase();

                if (wordA > wordB) {
                    return -1;
                }
                if (wordA < wordB) {
                    return 1;
                }
                // words must be equal
                return 0;
            });
        }
        this.existing_sorts[col] = Math.abs(1 - this.existing_sorts[col]);
    }


}
