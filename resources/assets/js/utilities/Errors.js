export default class Errors {

  constructor() {
    this.errors = {}
  }

  record(errors) {
    this.errors = errors
  }

  get(field) {
    if (this.errors[field]) {
      return this.errors[field][0]
    }
  }

  has(field) {
    let hasField = this.errors.hasOwnProperty(field)
    return hasField
  }

  any() {
    return Object.keys(this.errors).length
  }

  clear(field) {
    if (field)
      delete this.errors[field]
    else
      this.errors = {}
  }

  add(field, error_message) {
    this.errors[field] = error_message
  }
}