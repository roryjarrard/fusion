import {Errors} from "./index"; // utilities

export default class Form {

    constructor(data) {
        this.errors = new Errors();
        this.data = _.cloneDeep(data);
    }

    post(url) {
        return this.submit('post', url);
    }

    put(url) {
        return this.submit('put', url);
    }

    delete(url) {
        return this.submit('delete', url);
    }

    /**
     * Submit the form
     *
     * @param requestType
     * @param url
     * @returns {Promise}
     */
    submit(requestType, url) {
        return new Promise((resolve, reject) => {
            axios[_.toLower(requestType)](url, this.data)
                .then(response => {
                    this.onSuccess(response.data);

                    resolve(response.data);
                })
                .catch(error => {
                    this.onFail(error.response.data);

                    reject(error.response.data);
                })
        });
    }

    onSuccess(data) {
        this.reset();
    }

    onFail(errors) {
        this.errors.record(errors);
    }

    reset() {
        for (let field in this.data) {
            this.data[field] = null;
        }
        this.errors.clear();
    }
}