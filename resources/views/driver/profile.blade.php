@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h2>{{ $user->first_name . ' ' . $user->last_name }}</h2>
            </div>
        </div>

        @if ($user->hasRole(1))
            <div class="row">
                <div class="col-md-2">
                    Address
                </div>
                <div class="col-md-10">
                    {{ $user->driverProfile->address()->street }}<br>
                    @if (!empty($user->driverProfile->address()->stree2))
                        {{ $user->driverProfile->address()->street2 }}<br>
                    @endif
                    {{ $user->driverProfile->address()->city }}
                    , {{ $user->driverProfile->address()->getState()->short_name }}  {{ $user->driverProfile->address()->zip_postal }}
                </div>
            </div>
        @endif

    </div>


@endsection