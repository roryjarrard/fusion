<!DOCTYPE html>
<html lang="en" style="height: 100%;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="init" id="initVals" ref="initVals" content="{{ $initial_values or 'empty' }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {!! HTML::style('css/app.css') !!}

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body class="{{ 'is-' . session()->get('active_role') }}" style="height: 100%;">
<div id="app" class="{{ 'is-' . session()->get('active_role') }}" style="height: 100%;">

    <header id="fusionHeader"
            class="container site-header pr-3 pl-3">
        <div class="row" style="padding: 10px 0 5px;">
            @include('partials.mainnav')
        </div>

        @if(Auth::user())
            @include('partials.submenu')
        @endif
    </header>

    <div id="main-content" class="container">

    <!-- @if (Auth::check())
        @include('partials.breadcrumbs')
        @include('partials.submenu')

    @endif -->

        <super-session></super-session>
        <administrator-session></administrator-session>
        <manager-session></manager-session>
        {{--
            This flash message must sit here so it is rendered once and available for all user types
            Within your component, to flash a message look at the vue-flash-message docs
            and simply do this.flash('Your message', 'predefined-class ex. success');
        --}}
        <flash-message id="flashMessage"></flash-message>
        @yield('content')

        {{--<div class="row">
            <div class="col-md-12" style="">
                @yield('content')
            </div>
        </div>--}}
    </div>
</div>

<script src="/js/manifest.js"></script>
<script src="/js/vendor.js"></script>
<script src="/js/app.js"></script>
</body>
</html>
