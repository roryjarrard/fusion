<div class="row submenuView" style="min-height:52px;">
    <div class="col-sm-12" style="position: relative">
        @if (Auth::user() && (!Auth::user()->isSuper() || Auth::user()->isImpersonating()))
            <div class="fusion-by-cardata">
                <h6>Fusion</h6>
                <div>
                    <span>Powered by</span>
                    <img src="/img/cardata-logo.png" alt="CarData">
                </div>
            </div>
        @endif
        {{-- spans necessary to display both components in same blade view --}}
        <span>
            <dropdown-submenu></dropdown-submenu>
        </span>
        @if(Auth::user() && Auth::user()->isImpersonating())
            <span>
                <driver-debug></driver-debug>
            </span>
        @endif

    </div>
</div>