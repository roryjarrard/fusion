<?php $route = \Route::currentRouteName() ?>
<div class="container">
    <div id="driverSubmenu" class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle top-link {{ in_array($route, ['manager.mileage_approval', 'manager.link_to_driver', 'manager.viewed_tutorials']) ? 'active' : '' }}" data-toggle="dropdown" href="#" role="btn"
                       aria-haspopup="true" aria-expanded="false">Tools</a>
                    <div class="dropdown-menu">
                        {!! link_to_route('manager.mileage_approval', 'Mileage Approval', null, ['class'=>'dropdown-item'. ($route == 'manager.mileage_approval' ? ' active' : ' two'), 'id'=>'mileageApprovalLink']) !!}
                        {!! link_to_route('manager.link_to_driver', 'Link to a Driver', null, ['class'=>'dropdown-item'. ($route == 'manager.link_to_driver' ? ' active' : ''), 'id'=>'linkToDriverLink']) !!}
                        {!! link_to_route('manager.viewed_tutorials', 'View Tutorials', null, ['class'=>'dropdown-item'. ($route == 'manager.viewed_tutorials' ? ' active' : ''), 'id'=>'viewTutorialLink']) !!}
                        {!! link_to_route('manager.car_policy', Auth::user()->managerProfiles[0]->company->name .  ' Car Policy', null, ['class'=>'dropdown-item'. ($route == 'manager.car_policy' ? ' active' : ''), 'id'=>'viewTutorialLink']) !!}
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle top-link {{ in_array($route, ['manager.reimbursement_totals', 'manager.fuel_prices', 'manager.vehicle_report', 'traffic_light']) ? 'active' : '' }}" data-toggle="dropdown" href="#" role="btn"
                       aria-haspopup="true" aria-expanded="false">Reports</a>
                    <div class="dropdown-menu">
                        {!! link_to_route('manager.reimbursement_totals', 'Reimbursement Totals', null, ['class'=>'dropdown-item'. ($route == 'manager.reimbursement_totals' ? ' active' : ' two'), 'id'=>'reimbusementTotalsLink']) !!}
                        {!! link_to_route('manager.fuel_prices', 'Fuel Prices', null, ['class'=>'dropdown-item'. ($route == 'manager.link_to_driver' ? ' active' : ''), 'id'=>'fuelPricesLink']) !!}
                        {!! link_to_route('manager.vehicle_report', 'Vehicle Report', null, ['class'=>'dropdown-item'. ($route == 'manager.vehicle_report' ? ' active' : ''), 'id'=>'viewTutorialLink']) !!}
                        <div class="dropdown-divider"></div>
                        {!! link_to_route('manager.traffic_light', 'Traffic Light', null, ['class'=>'dropdown-item'. ($route == 'manager.traffic_light' ? ' active' : ''), 'id'=>'trafficLightReportLink']) !!}
                    </div>
                </li>

            </ul>
        </div>
    </div>
</div>