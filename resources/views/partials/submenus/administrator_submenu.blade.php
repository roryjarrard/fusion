<div class="container">
    <div id="administratorSubmenu" class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="dropdown active">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Tools

                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Company</a></li>
                        <li><a href="#">Divisions</a></li>
                        <li><a href="#">Vehicle Profiles</a></li>
                        <li><a href="#">Administrators</a></li>
                        <li><a href="#">Managers</a></li>
                        <li><a href="#">Drivers</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Link to Driver</a></li>
                        <li><a href="#">Add Many Drivers (Excel)</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Annual Review</a></li>
                        <li><a href="#">The Rare Comparison Tool</a></li>
                        <li><a href="#">Reimbursement Calculator</a></li>
                        <li><a href="#">Cents per Kilometer</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Reimbursement

                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Fixed and Variable</a></li>
                        <li><a href="#">All Fields Report</a></li>
                        <li><a href="#">Reimbursement Totals - Time Period</a></li>
                        <li><a href="#">Drivers Without Reimbursement</a></li>
                        <li><a href="#">Enrollment Audit</a></li>
                        <li><a href="#">Fuel Prices</a></li>
                        <li><a href="#">IRS Standard Mileage Rates</a></li>
                        <li><a href="#">CRA Standard Mileage Rates</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Vehicle and Traffic Light</a></li>
                        <li><a href="#">Vehicle</a></li>
                        <li><a href="#">Recent Mileage Logs</a></li>
                        <li><a href="#">Mileage</a></li>
                        <li><a href="#">Daily Mileage Details</a></li>
                        <li><a href="#">Manager</a></li>
                        <li><a href="#">Address and Email</a></li>
                        <li><a href="#">Viewed Tutorials by Drivers</a></li>
                        <li><a href="#">Vehicle Expired</a></li>
                        <li><a href="#">Quarterly Tax Adjustment Report</a></li>
                        <li><a href="#">IRS 463 Summary of Quarterly Tax Adjustments</a></li>
                        <li><a href="#">IRS FAVR Annual Tax Adjustment Report</a></li>
                        <li><a href="#">Unsubmitted Mileage ????</a></li>
                        <li><a href="#">Mileage Band Audit</a></li>
                        <li><a href="#">Fuel Car Tax Report</a></li>
                        <li><a href="#">Bank Account</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Payments

                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Master Payment Notice</a></li>
                        <li><a href="#">Direct Deposit Payment Schedule</a></li>
                        <li><a href="#">Manager Approval Status</a></li>
                        <li><a href="#">Pay File</a></li>
                        <li><a href="#">Prorate Fixed Reimbursement</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Miscellaneous

                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Password</a></li>
                        <li><a href="#">Email</a></li>
                        <li><a href="#">Login Info</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Mi-Route

                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Mi-Route Stop Visits</a></li>
                        <li><a href="#">Mileage Adjustment</a></li>
                        <li><a href="#">Daily Mileage using Mi-Route</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>