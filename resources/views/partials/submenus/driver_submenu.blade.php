<?php $route = \Route::currentRouteName() ?>
<div class="container">
    <div id="driverSubmenu" class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills">
                <li class="dropdown" role="presentation">
                    <a class="dropdown-toggle top-link {{ in_array($route, ['driver.trips', 'driver.mileage', 'driver.destinations']) ? 'active' : '' }}" data-toggle="dropdown" href="#" role="btn"
                       aria-haspopup="true" aria-expanded="false">Mileage <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>{!! link_to_route('driver.trips', 'Daily Trips', null, ['class'=>'dropdown-item'. ($route == 'driver.trips' ? ' active' : ' two'), 'id'=>'tripsLink']) !!}</li>
                        <li>{!! link_to_route('driver.mileage', 'Monthly Mileage', null, ['class'=>'dropdown-item'. ($route == 'driver.mileage' ? ' active' : ''), 'id'=>'mileageLink']) !!}</li>
                        <li>{!! link_to_route('driver.destinations', 'Destination Addresses', null, ['class'=>'dropdown-item'. ($route == 'driver.destinations' ? ' active' : ''), 'id'=>'destinationsLink']) !!}</li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle top-link {{ in_array($route, ['driver.monthly', 'driver.schedule', 'driver.policy']) ? 'active' : '' }}" data-toggle="dropdown" href="#" role="btn"
                       aria-haspopup="true" aria-expanded="false">Reimbursement</a>
                    <div class="dropdown-menu">
                        {!! link_to_route('driver.monthly', 'Monthly', null, ['class'=>'dropdown-item'. ($route == 'driver.monthly' ? ' active' : ''), 'id'=>'monthlyLink']) !!}
                        {!! link_to_route('driver.schedule', 'Driver Schedule & Fuel Map', null, ['class'=>'dropdown-item'. ($route == 'driver.schedule' ? ' active' : ''), 'id'=>'scheduleLink']) !!}
                        {!! link_to_route('driver.policy', Auth::user()->driverProfile->company->name . ' Car Policy', null, ['class'=>'dropdown-item'. ($route == 'driver.policy' ? ' active' : ''), 'id'=>'policyLink']) !!}
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle top-link {{ in_array(Request::path(), ['vehicle', 'license', 'insurance', 'address']) ? 'active' : '' }}" data-toggle="dropdown" href="#" role="btn"
                       aria-haspopup="true" aria-expanded="false">{!! Auth::user()->first_name !!} Profile</a>
                    <div class="dropdown-menu">
                        {!! link_to_route('driver.vehicle', 'Vehicle', null, ['class'=>'dropdown-item'. (Request::path() == 'vehicle' ? ' active' : ''), 'id'=>'vehicleLink']) !!}
                        {!! link_to_route('driver.license', 'License', null, ['class'=>'dropdown-item'. (Request::path() == 'license' ? ' active' : ''), 'id'=>'licenseLink']) !!}
                        {!! link_to_route('driver.insurance', 'Insurance', null, ['class'=>'dropdown-item'. (Request::path() == 'insurance' ? ' active' : ''), 'id'=>'insuranceLink']) !!}
                        {!! link_to_route('driver.address', 'Address', null, ['class'=>'dropdown-item'. (Request::path() == 'address' ? ' active' : ''), 'id'=>'addressLink']) !!}
                        {!! link_to_route('driver.direct_pay', 'Direct Pay', null, ['class'=>'dropdown-item'. (Request::path() == 'direct_pay' ? ' active' : ''), 'id'=>'directPayLink']) !!}
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>