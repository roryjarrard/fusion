<div class="container">
    <div id="superSubmenu" class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="dropdown active">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Tools
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Link to Driver</a></li>
                        <li><a href="#">Link to Manager</a></li>
                        <li><a href="#">Link to Admin</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Companies</a></li>
                        <li><a href="#">Divisions</a></li>
                        <li><a href="#">Vehicle Profiles</a></li>
                        <li><a href="#">Admins</a></li>
                        <li><a href="#">Managers</a></li>
                        <li><a href="#">Drivers</a></li>
                        <li><a href="#">Add Many Driver (Excel)</a></li>
                        <li><a href="#">Send Passwords</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Change Tracker</a></li>
                        <li><a href="#">Find Driver / Manager / Administrator</a></li>
                        <li><a href="#">Client Information</a></li>
                        <li><a href="#">Logins History</a></li>
                        <li><a href="#">Mobile Logins History</a></li>
                        <li><a href="#">Scheduled Tasks Log</a></li>
                        <li><a href="#">Master Tasks Control</a></li>
                        <li><a href="#">Reject Insurance Documents</a></li>
                        <li><a href="#">Insurance and License Processors</a></li>
                        <li><a href="#">Drivers with Expired Insurance</a></li>
                        <li class="divider"></li>
                        <li><a href="#">The Rate Comparison Tool</a></li>
                        <li><a href="#">Reimbursement Calculator for Administrators</a></li>
                        <li><a href="#">Rate Comparison with Proposed Vehicles</a></li>
                        <li><a href="#">Reimbursement Calculator</a></li>
                        <li><a href="#">Company Rates vs. CarData Rates</a></li>
                        <li><a href="#">Cents per Kilometer</a></li>
                        <li><a href="#">Draft Rate Report</a></li>
                        <li><a href="#">Annual Reviews</a></li>
                        <li><a href="#">US Database</a></li>
                        <li><a href="#">CA Database</a></li>

                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Reports
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Fixed and Variable</a></li>
                        <li><a href="#">Reimbursement Totals - Time Period</a></li>
                        <li><a href="#">All Fields Report</a></li>
                        <li><a href="#">Drivers Without Reimbursement</a></li>
                        <li><a href="#">Enrolment Audit</a></li>
                        <li><a href="#">Fuel Prices</a></li>
                        <li><a href="#">IRS Standard Mileage Rates</a></li>
                        <li><a href="#">CRA Standard Mileage Rates</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Vehicle and Traffic Light</a></li>
                        <li><a href="#">Vehicle</a></li>
                        <li><a href="#">Activity</a></li>
                        <li><a href="#">Recent Mileage Details</a></li>
                        <li><a href="#">Mileage</a></li>
                        <li><a href="#">Daily Mileage Details</a></li>
                        <li><a href="#">Bank Accounts</a></li>
                        <li><a href="#">Manager</a></li>
                        <li><a href="#">Address and Email</a></li>
                        <li><a href="#">Driver Dates</a></li>
                        <li><a href="#">Viewed Tutorials by Drivers</a></li>
                        <li><a href="#">Insurance Expired</a></li>
                        <li><a href="#">Vehicle Expired</a></li>
                        <li><a href="#">Inactive Drivers</a></li>
                        <li><a href="#">Reimbursement</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Quarter Definitions</a></li>
                        <li><a href="#">Quarterly Tax Adjustment Report</a></li>
                        <li><a href="#">Summary of Quarterly Tax Adjstments</a></li>
                        <li><a href="#">IRS FAVR Annual Tax Adjustment Report</a></li>
                        <li><a href="#">Mileage Band Audit</a></li>
                        <li><a href="#">Fuel Card Report</a></li>
                        <li><a href="#">Unsubmitted Mileage</a></li>
                        <li><a href="#">5000 Limit Report</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Payments
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Master Payment Notice</a></li>
                        <li><a href="#">Manager Approval Status</a></li>
                        <li><a href="#">Prorate Reimbursement</a></li>
                        <li><a href="#">Prorate Reimbursement for FAVR</a></li>
                        <li><a href="#">Pay File</a></li>
                        <li><a href="#">Prorate Adjustment Report</a></li>
                        <li><a href="#">Direct Pay Date</a></li>
                        <li><a href="#">Fixed 10% Discrepancy Report</a></li>
                        <li><a href="#">Client Invoice Report</a></li>
                        <li><a href="#">Driver Invoice Report</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Banks</a></li>
                        <li><a href="#">Manual ACH File</a></li>
                        <li><a href="#">Manual EFT File</a></li>
                        <li><a href="#">Direct Deposit Payment Schedule</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Miscellaneous
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Password</a></li>
                        <li><a href="#">Email</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Brickwork</a></li>
                        <li><a href="#">Vehicle Category Insurance Adjustment</a></li>
                        <li><a href="#">US Interest Rates</a></li>
                        <li><a href="#">US Monthly Insurance Rates</a></li>
                        <li><a href="#">US Maintenance & Repairs</a></li>
                        <li><a href="#">KBB Vehicles</a></li>
                        <li><a href="#">KBB Older Trims</a></li>
                        <li><a href="#">IRS Standard Mileage Rate</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Canadian Annual Fees</a></li>
                        <li><a href="#">Canadian Interest Rates</a></li>
                        <li><a href="#">Monthly Insurance Rates</a></li>
                        <li><a href="#">Resale Values</a></li>
                        <li><a href="#">CRA Standard Rate</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Communications</a></li>
                        <li><a href="#">Bulletin Board</a></li>
                        <li><a href="#">2017 Database</a></li>
                        <li><a href="#">CAA</a></li>
                        <li><a href="#">CPSA</a></li>
                        <li><a href="#">Charts</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>