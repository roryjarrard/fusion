<div class="col-sm-4" id="companyLogo">
    @if(Auth::user() && (!Auth::user()->isSuper() || Auth::user()->isImpersonating()))
        <?php $user = Auth::user()->isImpersonating() ? Auth::user()->impersonatedUser() : Auth::user(); ?>

        @if (!empty($company_logo))
            <img src="{{ $company_logo }}" alt="" style="height: 40px;">
        @else
            <h2>
                @if (session()->has('session_company_id'))
                    {{ App\Company::find(session()->get('session_company_id'))->name }}
                @else
                    {{ $user->company_name() }}
                @endif
            </h2>
        @endif
    @else
        <img src="/img/cardata-logo.png" alt="" width="150px">
    @endif
</div>

<div class="col-sm-4">
    {{-- USER IS SIGNED IN --}}
    @if(Auth::user())
        <div class="level-item has-text-centered role-switcher">
            <?php $user = Auth::user()->isImpersonating() ? Auth::user()->impersonatedUser() : Auth::user(); ?>

            {{-- DRIVER HAS MULTIPLE ROLES --}}
            @if($user->isDriver() && ($user->isManager() || $user->isAdmin() || $user->isSuper()))
                <?php $href = 1 & $user->driverProfile->street_light_id ? '#' : route('switchRole', ['role' => 'driver'])  ?>
                <a href="{{ $href }}"
                   class="btn btn-sm driver-role @if(Session('active_role')=='driver') active @endif">Driver</a>
            @endif

            {{-- MANAGER HAS MULTIPLE ROLES --}}
            @if($user->isManager() && ($user->isDriver() || $user->isAdmin() || $user->isSuper()))
                @if(!Session('impersonator_id') || (Session('impersonator_id') > 0 && Session('impersonator_role') == 'super'))
                    <a href="{{ route('switchRole', ['role'=>'manager']) }}"
                       class="btn btn-sm manager-role @if(Session('active_role')=='manager') active @endif">Manager</a>
                @endif
            @endif

            {{-- ADMINISTRATOR HAS MULTIPLE ROLES --}}
            @if($user->isAdmin() && ($user->isDriver() || $user->isManager() || $user->isSuper()))
                @if(!Session('impersonator_id') || (Session('impersonator_id') > 0 && Session('impersonator_role') == 'super'))
                    <a href="{{ route('switchRole', ['role'=>'administrator']) }}"
                       class="btn btn-sm admin-role @if(Session('active_role')=='administrator') active @endif">Administrator</a>
                @endif
            @endif

            {{-- SUPER HAS MULTIPLE ROLES --}}
            @if($user->isSuper() && ($user->isDriver() || $user->isManager() || $user->isAdmin()))
                @if(!Session('impersonator_id') || (Session('impersonator_id') > 0 && Session('impersonator_role') == 'super'))
                    <a href="{{ route('switchRole', ['role'=>'super']) }}"
                       class="btn btn-sm super-role @if(Session('active_role')=='super') active @endif">Super</a>
                @endif
            @endif

            {{-- USER IS LINKED --}}
            @if(Auth::check() && Auth::user()->isImpersonating())

                <a href="{{ route('stop.impersonating', []) }}"
                   class="btn btn-secondary btn-sm btn-default" id="stopImpersonatingButton">
                    Return to {{ session()->get('impersonator_username') }}
                </a>

            @endif
        </div>
    @endif
</div>

<div class="col-sm-4">
    <ul class="unstyled-list float-right mb-0">
        @if (Auth::guest())
            <li><a href="{{ route('login') }}" style="padding: 0;">Login</a></li>
        @else
            <li style="" class="float-left pl-3">
                <!--<a href="#"><i class="fas fa-search" style=""></i></a>-->
                {{-- SEARCH AREA, HIDE FOR NON-SUPER FOR NOW --}}
                @if(Auth::user()->isSuper() && !Auth::user()->isImpersonating())
                    <search></search>
                @endif
            </li>

            @if(($user->isDriver() && session()->get('active_role') == 'driver') || (Auth::check() && Auth::user()->isImpersonating() && Auth::user()->impersonatedUser()->isDriver() && session()->get('active_role') == 'driver' ) )
            <!-- driver status indicator -->
                <li style="" class="float-left pl-3">
                    <status-indicator></status-indicator>
                </li>
            @endif

            <li style="" class="float-left pl-3 mr-4">
                <notifications></notifications>
            </li>

            <li class="nav-item dropdown pt-0 pr-0 pb-0 pl-3 float-right user-menu">
                <a class="nav-link dropdown-toggle p-0" data-toggle="dropdown" href="#" role="btn"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user" style="vertical-align: unset;"></i> Welcome, {{ Auth::user()->first_name }}
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>

                    <a id="profileLink" class="dropdown-item {{ session()->get('active_role') . '-profile' }}">My
                        Profile</a>
                    {{--<a href="{{ route('view.profile', "My Profile", []) }}" class="dropdown-item">My Profile</a>--}}
                </div>
            </li>
        @endif
    </ul>
</div>

<span id="printable" style="display: none; position: absolute; right: 0; padding: 0; margin: 0;">
    <?php echo "Printed On: " . date('F jS, Y'); ?>
</span>