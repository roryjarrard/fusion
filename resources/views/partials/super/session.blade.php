<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Session Values</h3>


            {{ Form::open(['class'=>'form', 'id'=>'superSessionForm']) }}
            <div class="row">

                <div class="col-md-4" id="companyDropdown">
                    <div class="form-group">
                        <?php $company_id = !empty(session('company_id')) && session('company_id') > -1 ? session('company_id') : null; ?>
                        {!! Form::label('Company') !!}
                        {!! Form::select('companies', App\Company::orderBy('name')->pluck('name', 'id'), $company_id, ['name'=>'company', 'class'=>'form-control', 'id'=>'companySelect', 'placeholder'=>'None']) !!}
                    </div>
                </div>

                <?php $hidden = !empty(session('company_id')) && session('company_id') > -1 && count(App\Company::find(session('company_id'))->countries) > 1 ?: 'style=display:none' ?>
                <div class="col-md-4" id="countryDropdown" {{ $hidden }}>
                    <div class="form-group">
                        <?php $country_id = !empty(session('country_id')) && session('country_id') > -1 ? session('country_id') : null; ?>
                        {!! Form::label('Country') !!}
                        {!! Form::select('countries', App\Country::all()->pluck('name', 'id'), $country_id, ['name'=>'country', 'class'=>'form-control', 'id'=>'countrySelect', 'placeholder'=>'None']) !!}
                    </div>
                </div>

                <div class="col-md-4" id="divisionDropdown">
                    <div class="form-group">
                        <?php $division_id = !empty(session('division_id')) && session('division_id') > -1 ? session('division_id') : null; ?>
                        {!! Form::label('Division') !!}
                        {!! Form::select('divisions', App\Division::where('company_id', session('company_id'))->orderBy('name')->pluck('name', 'id'), $division_id, ['name'=>'division', 'class'=>'form-control', 'id'=>'divisionSelect','placeholder'=>'None']) !!}
                    </div>
                </div>

            </div>
            {{ Form::close() }}


        </div>
    </div>
</div>