@extends('layouts.app')


@section('content')

    @include('partials.super.session')

    <h2>Users:</h2>

    @foreach ($users as $user)
        {{ $user->last_name . ', ' . $user->first_name }}<br>
    @endforeach

@endsection