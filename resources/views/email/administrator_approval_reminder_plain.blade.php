@if( $data->company->direct_pay_day == 0)
{{$data->user->first_name }} {{$data->user->last_name }},

The payment approval deadline for the {{$data->dates->previous_month_name}}, {{$data->dates->previous_year}} has passed.

You can view its status by selecting Manager Approval Status from the Payments menu on CarData Online.
If all the Manager approvals have occurred, you may approve the Manager Approval Status as the Admin Approver.
If any Manager approvals are missing, you may override them as the Admin Approver.

If you have any questions, contact {{$data->coordinator->name}} by replying to this email or by calling {{$data->coordinator->phone}}.

www.cardataconsultants.com

@else
{{$data->user->first_name }} {{$data->user->last_name }},

Please be advised that the payment approval deadline for the {{$data->dates->previous_month_name}}, {{$data->dates->previous_year}} reimbursement is today.
You can view its status by selecting Manager Approval Status from the Payments menu on CarData Online.
If all the Manager approvals have occurred, you may approve the Manager Approval Status as the Admin Approver.
If any Manager approvals are missing you may override them as the Admin Approver.

The {{$data->dates->previous_month_name}}, {{$data->dates->previous_year}} reimbursement payment is due to CarData by {{$data->dates->received_date}}.
Select Master Payment Notice from the Payments menu to view the details of the payment.

If you have any questions, contact {{$data->coordinator->name}} by replying to this email or by calling {{$data->coordinator->phone}}.

www.cardataconsultants.com

@endif