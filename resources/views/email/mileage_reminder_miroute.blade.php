{{$data->driver->first_name}} {{$data->driver->last_name}},

CarData Online is open for any mileage adjustments or missed trip additions, until {{$data->lock_date}}, Midnight {{$data->time_zone->short_name}} ({{$data->time_zone->name}})
If you have any corrections to make, please login to CarData Online at www.cardataconsultants.com before the deadline to ensure reimbursement.

@if( $data->mileage_discrepancies)
Please be advised that the following business mileage will not be included in reimbursement, as the date was closed on a different day from the actual trip.
Please login to CarData online at www.cardataconsultants.com and edit your day, to include this mileage if appropriate.

@foreach ($data->mileage_discrepancies as $difference)
{{$difference}}
@endforeach
@endif

Please do not hesitate to contact me, at {{$data->coordinator->email}}, or {{$data->coordinator->phone}}, if you have any questions or concerns.
