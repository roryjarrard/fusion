Hi {{$data->driver->first_name}},

This is a reminder that between Jan 1 – 30, the Annual IRS FAVR Verification for the Vehicle Reimbursement Program is required.

@if ($data->company->insurance_review_interval == 'Annually')
Insurance documents can only be received between Jan 1 – 30th.  You can scan and e-mail to insurance@cardataconsultants.com OR fax to 1-708-575-1999. Only do this between Jan 1 - 30th.
@endif

On January 1st your Annual Declaration Page will be ready for you to verify your current Vehicle information, and to update your CURRENT Odometer reading when you login to CarData Online.

You will:
Enter your Current Odometer; check the check box, and "Submit Compliance Declaration" at the bottom of the page.

Per IRS FAVR Rules and Regulations, the VRP participant must provide

(1) Make, Model, Year
(2) The Odometer Reading
@if ($data->company->insurance_review_interval == 'Annually')
(3) Written Proof of the Insurance Coverage required by {{$data->company->name}} – Policy documentation
@endif

The above must be completed between January 1 - January 30, {{$data->year}} or you will not be compliant to receive a tax free reimbursement.

@if ($data->company->insurance_review_interval == 'Annually')
Please circle the following on your documentation before you send it in.

@foreach ($data->insurance_details as $insurance)
   {{$insurance->name}} {{$insurance->amount}} {{$insurance->affix}}
@endforeach
@if ($data->business_use_required)
   Proof of Business Use Insurance
@endif
   Your Name
   Make, Model, and year of vehicle
   Policy term dates – these must be current, please do not send in expired policy documentation
@endif

If you have any questions, please do not hesitate to contact {{$data->coordinator->name}}, at {{$data->coordinator->email}}, or please call {{$data->coordinator->phone}}.

Thanks so much.
