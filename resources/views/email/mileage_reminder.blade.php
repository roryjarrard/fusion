If you have already entered your mileage, please disregard this email.

This is a reminder that you have until {{$data->lock_date}}, Midnight {{$data->time_zone->short_name}} ({{$data->time_zone->name}}), to enter mileage for {{$data->month_name}}, {{$data->year}}.

If you have any questions, contact {{$data->coordinator->name}}, by replying to this email or by calling {{$data->coordinator->phone}}.

www.cardataconsultants.com

