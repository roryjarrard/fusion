{{$data->driver->first_name}} {{$data->driver->last_name}},

@if ($data->insurance_warning)
Please be advised that the grace period to submit your insurance documentation has expired, and your status is {{$data->color}} - {{$data->street_light_names}}.
You must submit insurance documentation as required by {{$data->company->name}}.

Remember to Circle the following information on your documents before you submit them to CarData.

@foreach( $data->circle as $circle)
    {!!$circle!!}
@endforeach

@foreach ($data->insurance_amounts as $insurance)
    {{$insurance->name}} {{$insurance->amount}} {{$insurance->affix}}
@endforeach

Two options to send your insurance documentation.
1.  Scan and e-mail a PDF copy to {{$data->coordinator->name}} at {{$data->coordinator->email}} or
2.  Fax a copy of your updated insurance documents to {{$data->coordinator->fax}}.
Make sure to write your name and your company's name on the fax cover page.
@endif  {{-- insurance warning --}}

If you have any questions, contact {{$data->coordinator->name}} by replying to this email or by calling {{$data->coordinator->phone}}.

www.cardataconsultants.com