<p>
    Hello from CarData Consultants!
</p>

<p>
    Your new verification code is {{ $code }}. It will be valid for 24 hours from the time it was sent.
</p>

<p>
    The CarData Team
</p>