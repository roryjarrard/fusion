{{$data->driver->first_name}} {{$data->driver->last_name}},

@if ($data->insurance_warning)
Please be advised that the grace period to submit your insurance documentation has expired, and your status is now {{$data->color}} No Insurance Compliance.
@if ( $data->company->remove_reimbursement_without_insurance)
As per the {{$data->company->name}} Policy and Procedure Manual, failure to submit your insurance documentation, by the deadline, will result in no reimbursement payment.
@elseif ( $data->company->id == WASTE_MANAGEMENT_USA )
As per the {{$data->company->name}} Policy and Procedure Manual, failure to submit your insurance documentation will reduce your reimbursement by the fixed (base) amount.
@else
As per FAVR rules and regulations, in order to receive a non-taxable reimbursement, you must submit proof of insurance coverage that meets the minimum requirements for {{$data->company->name}}.
@endif

Remember to Circle the following information on your documents before you submit them to CarData.

@foreach( $data->circle as $circle)
    {!!$circle!!}
@endforeach

@foreach ($data->insurance_amounts as $insurance)
    {{$insurance->name}} {{$insurance->amount}} {{$insurance->affix}}
@endforeach

Two options to send your insurance documentation.
1.  Scan and e-mail a PDF copy to {{$data->coordinator->name}} at {{$data->coordinator->email}} or
2.  Fax a copy of your updated insurance documents to {{$data->coordinator->fax}}.
Make sure to write your name and your company's name on the fax cover page.

@endif  {{-- insurance_warning --}}
@if ($data->vehicle_age_warning)
Please be advised that the grace period to change your vehicle age has expired, and your status is Green-No Vehicle Age Compliance.
As per FAVR rules and regulations, in order to receive a non-taxable reimbursement, your vehicle model year must be {{$data->favr['minimum_year']}} or newer.
@if ($data->company->id == WASTE_MANAGEMENT_USA)
As per the {{$data->company->name}} Policy and Procedure Manual, the depreciation component of your fixed reimbursement will be removed from your payment.
@endif {{-- WM USA --}}
@endif {{-- vehicle_age_warning --}}
@if ($data->vehicle_cost_warning)
Please be advised that the grace period to change your vehicle cost has expired.  Your status is Green-No Vehicle Cost Compliance.
As per FAVR rules and regulations, in order to receive a non-taxable reimbursement, you must have a vehicle in service that meets the IRS required minimum cost of ${{$data->favr['minimum_vehicle_cost_formatted']}}
@endif {{-- vehicle_cost_warning --}}
If you have any questions, contact {{$data->coordinator->name}} by replying to this email or by calling {{$data->coordinator->phone}}.

www.cardataconsultants.com


