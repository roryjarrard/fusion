Mileage entries for Drivers for {{$data->month}}, {{$data->year}} have been locked.

You have until {{$data->lock_date}} to change a driver's {{$data->month}}, {{$data->year}} mileage, by using the Link to Driver feature on CarData Online.

This is a reminder that you have until {{$data->approval_date}} to approve the Mileage Approval Report  available in the Tools menu on CarData Online.

If you have any questions, contact the Coordinator, {{$data->coordinator->name}}, by replying to this email or by calling {{$data->coordinator->phone}}.

www.cardataconsultants.com
