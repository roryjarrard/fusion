Hi {{$data->user->first_name }} {{$data->user->last_name }},

Thank you for submitting your vehicle insurance documentation. In reviewing this document, we noticed that you are missing the following coverage:

@foreach ($data->unmetInsuranceRequirements as $unmet)
    {{$unmet->name}} {{$unmet->amount}} {{$unmet->affix}}
@endforeach

Please contact your insurance company to have your policy adjusted accordingly. Then re-submit the documentation, in PDF format, to CarData through the 'My Profile' page.

For your reference, {{$data->company_name}} requires the following coverage and policy details to be listed on your vehicle insurance documentation:

- Your name
- Make, model, and year of vehicle
- Policy term (from - to date)
@foreach ($data->overallInsuranceRequirements as $insurance)
- {{$insurance->name}} {{$insurance->amount}} {{$insurance->affix}}
@endforeach

If you have any questions, contact {{$data->coordinator->name}} by replying to this email or by calling {{$data->coordinator->phone}}.

www.cardataconsultants.com