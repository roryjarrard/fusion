Hi {{$data->driver->first_name}} {{$data->driver->last_name}},

Please be advised that your insurance documentation has expired, and your status has changed to {{$data->streetLight->color}}:

@foreach ($data->streetLight->data as $status)
    - {{$status->title }} - {{$status->description}}
@endforeach

As per FAVR rules and regulations, in order to receive a non-taxable reimbursement, you must submit proof of insurance coverage that meets the minimum requirements for {{$data->company->name}}.

@if ( $data->company->id == WASTE_MANAGEMENT_USA )
As per the {{$data->company->name}} Policy and Procedure Manual, failure to submit your insurance documentation will reduce your reimbursement by the fixed (base) amount.

@endif
Please log in to CarData online for detailed instructions on what is required. The instructions are found under the 'Reimbursement' tab on the '{{$data->company->name}} Car Policy' page.

Remember to CIRCLE the following information on your insurance policy documents before you re-submit them to CarData.

CIRCLE

@foreach( $data->circle as $circle)
    {!!$circle!!}
@endforeach

@foreach ($data->insurance_amounts as $insurance)
    {{$insurance->name}} {{$insurance->amount}} {{$insurance->affix}}
@endforeach

Two options to send your insurance documentation:

1.  Scan and e-mail a PDF copy to {{$data->coordinator->name}} at {{$data->coordinator->email}} or
2.  Fax a copy of your updated insurance documents to {{$data->company->insurance_fax_number}}

If you have any questions, please do not hesitate to contact  {{$data->coordinator->name}} at {{$data->coordinator->email}}

