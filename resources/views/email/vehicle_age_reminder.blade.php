Hi {{$data->first_name}},

Please be advised that your vehicle will not meet the Vehicle Age Policy Requirement for {{$data->company_name}} in three months.
Your Traffic Light Status will be moved to Red-No Vehicle Age Compliance, no reimbursement, on January 15, {{$data->year}}.
To Add a New Vehicle to the Program, please go to the yellow driver profile tab; select Vehicle and Add the new vehicle.

Please do not hesitate to contact your administrator, {{$data->coordinator->name}} at {{$data->coordinator->phone}} or {{$data->coordinator->email}} if you have any questions.

