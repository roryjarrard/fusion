This is a reminder that the reimbursement payment for {{$data->previous_month_name}}, {{$data->previous_month_year}} from {{$data->company_name}} is due today.
Select Master Payment Notice from the Payments menu on CarData Online to view or download it.
Please disregard this automated message if the payment has already been sent.

If you have any questions, contact {{$data->coordinator->name}}, by replying to {{$data->coordinator->email}} email address or by calling {{$data->coordinator->phone}}.
