Hi {{$data->driver->first_name}} {{$data->driver->last_name}},

Please be advised that one of your submitted insurance documents has been rejected.

It has been rejected for the following reasons:
@foreach($data->rejectionReasons as $reason)
  -  {!!$reason!!}
@endforeach

@if ($data->custom_rejection_message)
The following message was added by {{$data->processor->first_name}} {{$data->processor->last_name}}:

    {{$data->custom_rejection_message}}
@endif

If you have any questions, please do not hesitate to contact {{$data->processor->first_name}} {{$data->processor->last_name}} at {{$data->processor->email}}.

wwww.cardataconsultants.com