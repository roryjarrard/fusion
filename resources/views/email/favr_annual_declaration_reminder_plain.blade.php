Hi {{$data->name}},

This is a reminder that between Jan 1 – 30, the Annual IRS FAVR Verification for the Vehicle Reimbursement Program is required.

@if ($data->company->insurance_review_interval == 'Annually' && $data->no_annual_declaration && $data->no_insurance)
As of today, two critical actions are not completed on your account within CarData Online. They are:

   1) Completion of your VRP Annual Declaration Page
   2) Submitting your insurance documents to CarData

Both need to be completed by January 30, $year, in order for you to receive all of the vehicle reimbursements you are entitled to.
@if ($data->company->remove_reimbursement_without_insurance)
Failure to complete the Declaration will result in your reimbursement being taxable, and no insurance compliance will result in the fixed and variable portions of your reimbursement being removed.
@else
   Failure to complete the Declaration will result in your reimbursement being taxable.
@endif

Per IRS Rules and Regulations, an employee receiving a vehicle reimbursement must provide the following information within 30 days after the beginning of each calendar year:

(1) Make, Model, Year of the vehicle they drive for business
(2) Written Proof of the Insurance Coverage identified
(3) Vehicle Odometer Reading

To become compliant with IRS regulations please follow these two steps:

1. Login to CarData Online at www.cardataconsultants.com to complete your annual IRS non-taxable FAVR compliance declaration.
2. Submit your insurance documentation showing proof of coverage amounts to CarData's insurance FAX line at 708-575-1999, or scan and e-mail to insurance@cardataconsultants.com.

Please circle the following on your documentation before you send it in.
@foreach( $data->circle as $circle)
   {!!$circle!!}
@endforeach
@foreach ($data->insurance_details as $insurance)
   {{$insurance->name}} {{$insurance->amount}} {{$insurance->affix}}
@endforeach

@elseif ($data->no_annual_declaration)
As of today, one critical action is not completed on your account within CarData Online. It is:

   Completion of your VRP Annual Declaration Page

It needs to be completed by January 30, $year, in order for you to receive all of the vehicle reimbursements you are entitled to.
@if ($data->company->remove_reimbursement_without_insurance)
Failure to complete the Declaration will result in your reimbursement being taxable, and no insurance compliance will result in the fixed and variable portions of your reimbursement being removed.
@else
Failure to complete the Declaration will result in your reimbursement being taxable.
@endif

Per IRS Rules and Regulations, that the employee must provide
(1)  Make, Model, Year; and
(2) the Odometer Reading,

within 30 days after the beginning of each calendar year, you are required to complete the Annual Declaration Page in CarData online.

PLEASE LOG INTO CARDATA AT www.cardataconsultants.com AND COMPLETE YOUR ANNUAL IRS NON TAXABLE FAVR COMPLIANCE DECLARATION,
by entering your current odometer; check the check box to declare the information to be true, and 'Submit Compliance Declaration' at the bottom of the page.
@elseif ($data->company->insurance_review_interval == 'Annually' && $data->no_insurance)
We have not received your annual submission of Insurance Policy Documentation.

@if ($data->company->remove_reimbursement_without_insurance)
It is critical that this be completed by January 30, $year or your reimbursement will be taxable, and no insurance compliance will result in the fixed and variable portions of your reimbursement being removed.
@else
It is critical that this be completed by January 30, $year or your reimbursement will be taxable.
@endif

Per IRS Rules and Regulations, that the employee must provide, Written Proof of the Insurance Coverage required, within 30 days after the beginning of each calendar year, you are required to submit your Insurance Policy documentation, by fax, to 708-575-1999 or scan and e-mail to insurance@cardataconsultants.com

PLEASE SUBMIT YOUR INSURANCE DOCUMENTATION before January 30, {{$data->year}}

Insurance  Coverage Minimums are:

@foreach( $data->circle as $circle)
   {!!$circle!!}
@endforeach
@foreach ($data->insurance_details as $insurance)
   {{$insurance->name}} {{$insurance->amount}} {{$insurance->affix}}
@endforeach
@endif

If you have any questions, please do not hesitate to contact {{$data->coordinator->name}}, at {{$data->coordinator->email}}, or please call {{$data->coordinator->phone}}.

Thanks so much.
