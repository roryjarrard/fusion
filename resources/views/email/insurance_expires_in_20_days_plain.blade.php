{{$data->driver->first_name}} {{$data->driver->last_name}}

Please be advised that your policy documentation in CarData Online will expire in 20 days.

As per our agreement with {{$data->company->name}}, you will not receive reimbursement if we do not receive your renewal documentation by the deadline.

Please log in to CarData online for detailed instructions on what is required. The instructions are found under the yellow 'Driver Profile' tab in the 'Insurance' selection.

Remember to CIRCLE the following information on your insurance policy documents before you re-submit them to CarData.

CIRCLE

@foreach( $data->circle as $circle)
    {!!$circle!!}
@endforeach

@foreach ($data->insurance_amounts as $insurance)
    {{$insurance->name}} {{$insurance->amount}} {{$insurance->affix}}
@endforeach

Two options to send your insurance documentation:

1.  Scan and e-mail a PDF copy to {{$data->coordinator->name}} at {{$data->coordinator->email}} or
2.  Fax a copy of your updated insurance documents to {{$data->company->insurance_fax_number}}

If you have any questions, please do not hesitate to contact  {{$data->coordinator->name}} at {{$data->coordinator->email}}

