Hi {{$data->name}},

Per IRS FAVR Rules and & Regulations, those not meeting a minimum of 5,000 business miles in a calendar year - prorated from the month you began the program - will have all reimbursement compared to the IRS Business Standard Mileage Rate.  Excess reimbursement will be included as taxable income.

An employee not meeting the 5,000 annual mileage requirement may be removed from the VRP.

Your expected mileage for {{$data->months}} is {{$data->mileage_required}}.

You may still have mileage to submit for the previous month.

As of {{$data->last_month}} your current submitted mileage is: {{$data->business_mileage}}.  Your expected mileage at this date for compliance should be {{$data->mileage_required}}.

Please make sure that your mileage submission is up to date, and review with your manager if you believe that you should no longer be on the program.

If you have any questions, please do not hesitate to contact {{$data->coordinator->name}} at {{$data->coordinator->email}}

www.cardataconsultants.com
