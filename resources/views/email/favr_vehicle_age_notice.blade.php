Hi {{$data->first_name}},

@if( $data->company_id != WASTE_MANAGEMENT_USA )
Please be advised that the model year of your vehicle {{$data->vehicle_year}} will no longer meet the minimum year {{$data->max_vehicle_age}} for IRS FAVR Vehicle Age Compliance, in {{$data->year}}, and you may see taxable income quarterly.
If you have questions about the program requirements, the {{$data->company_name}} Car Policy can be found under the Purple Reimbursement Tab; select {{$data->company_name}} Car Policy.

To register a new vehicle in the Vehicle Reimbursement Program, please follow the steps below:
@else
Please be advised that the model year of your vehicle, {{$data->vehicle_year}}, no longer meets the minimum year of {{$data->max_vehicle_age}} for IRS FAVR Vehicle Age Compliance, and you may see taxable income quarterly, in {{$data->year}}.
If you have questions about the program requirements, the {{$data->company_name}} Car Policy can be found under the Purple Reimbursement Tab; select {{$data->company_name}} Car Policy.

To register a new vehicle, and remain compliant in the Vehicle Reimbursement Program, please follow the steps below:
@endif

1)  Log in to CarData Online
2)  Under the Yellow Driver Profile Tab; select Vehicle
3)  Select the "Add New Vehicle Button"
4)  Complete the page, remember you will need the odometer for your new vehicle, and the ending odometer of your previous vehicle
5)  Scroll to the bottom, and click the  "Add New Vehicle" button.
6)  You will then have 30 days to send in your insurance documentation for your new vehicle

If you have any questions, please do not hesitate to contact {{$data->coordinator->name}}, at {{$data->coordinator->email}} or {{$data->coordinator->phone}}

