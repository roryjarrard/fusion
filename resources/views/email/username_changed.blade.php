Hi {{ $data->user->first_name }} {{ $data->user->last_name }},

This is a notification that the username associated with your CarData Fusion Account has been updated.

Your new username is: {{ $data->user->username }}

You will be able to log in securely with your existing password.

If you have any questions, contact the Coordinator {{$data->coordinator->name}}, by email at {{$data->coordinator->email}} or by calling {{$data->coordinator->phone}}.


www.cardataconsultants.com