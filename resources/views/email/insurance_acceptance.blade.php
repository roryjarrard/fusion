Hi {{$data->user->first_name }} {{$data->user->last_name }},

Thank you for submitting your vehicle insurance documentation. You have met the {{$data->company_name}} insurance requirements.

@if ($data->streetLight->color == 'Green')
@if($data->streetLight->id == 2)
@if($data->service_plan == 'FAVR')
Your street light in CarData Fusion is green-reimbursement. You have met your IRS FAVR Compliance Status.
@else
Your street light in CarData Fusion is green-reimbursement.
@endif
@else
{{--THESE MUST BE FAVR STATUSES AS THERE IS NO GREEN + FOR OTHER PLANS--}}
Your current IRS FAVR Compliance is:

@foreach ($data->streetLight->data as $status)
    - {{$status->description}}
@endforeach

As per IRS rules and regulations, in order to receive a non-taxable reimbursement, you must meet all FAVR requirements.
If you would like to make changes to your driver profile, please login to CarData Fusion, click your name at the top right corner of the screen and select the 'My Profile' page.

@endif
@elseif($data->streetLight->color == 'Red')
Please be advised that your status is red for the following reasons:

@foreach ($data->streetLight->data as $status)
    - {{$status->description}}
@endforeach

With a red street light, you will not receive reimbursement each month.
@endif

If you have any questions, contact {{$data->coordinator->name}} by replying to this email or by calling {{$data->coordinator->phone}}.

www.cardataconsultants.com