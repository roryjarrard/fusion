Hi {{$data->first_name}},

Please be advised that as per {{$data->company_name}} Vehicle Age Policy, your Traffic Light Status has been moved to Red-No Vehicle Age Compliance.
You will not receive reimbursement in Red-No Vehicle Age Compliance status.
Personal Vehicles for {{$data->company_name}} must be of the year {{$data->max_year}} or later.

To change to a new vehicle in CarData, please login and go to the yellow driver profile tab; select vehicle, and add your new vehicle information.
Remember you must be in Green Reimbursement status no later than January 31,to receive any reimbursement for January.

Please do not hesitate to contact your administrator, {{$data->coordinator->name}} at {{$data->coordinator->phone}} or {{$data->coordinator->email}} if you have any questions.


