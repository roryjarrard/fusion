Hi {{$data->admin_name}},

Please be advised that the following employees will not receive reimbursement on the current Master Payment Report based on the following explanations

@foreach($data->drivers as $driver)
    {!!$driver->last_name!!} {!!$driver->first_name!!}: {!!$driver->description!!}
@endforeach

If you have any questions, please do not hesitate to contact {{$data->coordinator->name}} at {{$data->coordinator->email}} or {{$data->coordinator->phone}}

