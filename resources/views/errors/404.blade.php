@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            We are sorry, but we can't seem to locate that page. Please <a href="/">return to the portal</a>.
        </p>
    </div>

@endsection