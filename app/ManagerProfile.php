<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\DriverProfile as DriverProfile;

class ManagerProfile extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'manager_id', 'active', 'company_id', 'manager_number', 'approval_description', 'address_id', 'assigned_drivers'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function drivers($with_deleted = 0)
    {
        if ($with_deleted == 1) {

            $driver_ids = \DB::table('users')->join('manager_driver', 'manager_driver.driver_id', 'users.id')
                ->where('manager_driver.manager_id', $this->manager_id)
                ->where('manager_driver.deleted_at',null)
                ->pluck('users.id')->all();

            return User::whereIn('id', $driver_ids)->select('id', 'first_name', 'last_name', 'deleted_at')->orderBy('last_name')->orderBy('first_name')->get();
        } else {

            $driver_ids = \DB::table('users')->join('manager_driver', 'manager_driver.driver_id', 'users.id')
                ->join('driver_profiles','manager_driver.driver_id','driver_profiles.id')
                ->where('driver_profiles.active',1)
                ->where('manager_driver.deleted_at',null)
                ->where('users.deleted_at', null)
                ->where('manager_driver.manager_id', $this->manager_id)->pluck('users.id')->all();
            return User::whereIn('id', $driver_ids)->select('id', 'first_name', 'last_name')->orderBy('last_name')->orderBy('first_name')->get();
        }
    }

    public function divisions(){

        $divisions = [];
        if( $this->assigned_drivers == 'all') {
            $divisions = Division::where('company_id', $this->company_id)->get();
        } elseif( $this->assigned_drivers != 'none') {
            $include_inactive_drivers = empty(session()->get('include_inactive_drivers')) ? 0 : session()->get('include_inactive_drivers');
            $drivers = $this->drivers($include_inactive_drivers);
            if (!empty($drivers)) {
                foreach ($drivers as $driver) {
                    if (!in_array($driver->driverProfile->division, $divisions)) {
                        $divisions[] = $driver->driverProfile->division;
                    }
                }
            }
        }
        return $divisions;
    }

    public function driver($driver_id)
    {
        return DriverProfile::where('user_id', $driver_id)->first();
    }

    public function address()
    {
        //return $this->hasOne('App\Address');

        // davery 8-22-18 - discussed w/ Rory
        dd('Need to talk about how this will work as whether your a driver or manager,
        right now state show\'s the same address based on store getUserAddress');

        dd($this->address_id);

        return Address::where('id', $this->address_id)->first();
    }
}
