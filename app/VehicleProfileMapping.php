<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleProfileMapping extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'company_id',
        'old_vehicle_profile_id',
        'proposed_vehicle_profile_id',
        'submitted_for_approval',
        'approved',
        'approved_by',
        'note',
        'created_by',
        'applied_by',
    ];
}
