<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleSuperCategory extends Model
{
    protected $table = 'vehicle_super_categories';
    public $timestamps = false;
    protected $fillable = ['id', 'name'];
}
