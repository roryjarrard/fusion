<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxesFeesUS extends Model
{
    protected $table = 'taxes_fees_us';

    protected $fillable = ['year', 'county_id', 'sales_tax', 'personal_property_tax'];
}
