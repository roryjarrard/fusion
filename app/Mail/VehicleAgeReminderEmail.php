<?php
/**
 * Created by PhpStorm.
 * User: Jerzy 15
 * Date: 6/22/2018
 * Time: 3:09 PM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class VehicleAgeReminderEmail  extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = 'email.vehicle_age_reminder';

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject('CarData Online: Vehicle Age Policy Reminder')
            ->text($email_template)->with('data', $this->data);
    }
}