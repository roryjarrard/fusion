<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MileageReminderEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->data->company->id == 54 || $this->data->company->id == 56) {
            $email_template = 'email.mileage_reminder_waste_management';
        } elseif ($this->data->company->mileage_entry_method == "Mi-Route") {
            $email_template = 'email.mileage_reminder_miroute';
        } else {
            $email_template = 'email.mileage_reminder';
        }

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject('CarData Online: Mileage Lock Reminder')
            ->text($email_template)->with('data', $this->data);
    }
}
