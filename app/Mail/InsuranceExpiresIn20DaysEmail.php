<?php
/**
 * Created by PhpStorm.
 * User: Jerzy 15
 * Date: 6/18/2018
 * Time: 11:19 AM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Http\Traits\CompanyTraits;

class InsuranceExpiresIn20DaysEmail extends Mailable
{
    use Queueable, SerializesModels, CompanyTraits;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_plain = 'email.insurance_expires_in_20_days_plain';

        // generate dynamic items to be circled
        $business_use = $this->getCompanyBusinessUse($this->data->company->id);
        $circle = $this->data->circle;
        $i = 4;
        if( $business_use) {
            $circle[] = "{$i}.  Proof of Business Use Insurance";
            $i++;
        }
        if( count($this->data->insurance_amounts) ) {
            $circle[] = "{$i}.  Coverage Amounts required by " . $this->data->company->name;
        }
        $this->data->circle = $circle;

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject('Insurance Documents Required in 20 days')
            ->text($email_plain)->with('data', $this->data);
    }

}