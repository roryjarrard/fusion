<?php
/**
 * Created by PhpStorm.
 * User: Jerzy 15
 * Date: 7/3/2018
 * Time: 9:18 AM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentReminderEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = 'email.payment_reminder';

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->cc($this->data->cc)
            ->subject('CarData Online: Direct Pay Deadline')
            ->text($email_template)->with('data', $this->data);
    }
}