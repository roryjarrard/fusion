<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdministratorApprovalReminderEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_plain = 'email.administrator_approval_reminder_plain';

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject('CarData Online: Variable Payment Approval Deadline')
            ->text($email_plain)->with('data', $this->data);
    }
}
