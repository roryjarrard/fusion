<?php
/**
 * Created by PhpStorm.
 * User: Jerzy 15
 * Date: 6/11/2018
 * Time: 11:08 AM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Below5000MilesEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_plain = 'email.below_5000_miles_plain';

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject('Warning 5,000 min bus Miles Compliance at Risk')
            ->text($email_plain)->with('data', $this->data);
    }

}