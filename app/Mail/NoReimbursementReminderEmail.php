<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NoReimbursementReminderEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = 'email.no_reimbursement_reminder';

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject('CarData Online: Drivers Without Reimbursement')
            ->text($email_template)->with('data', $this->data);
    }
}
