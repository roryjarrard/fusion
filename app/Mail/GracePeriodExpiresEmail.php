<?php
/**
 * Created by PhpStorm.
 * User: Jerzy 15
 * Date: 6/18/2018
 * Time: 11:19 AM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Http\Traits\CompanyTraits;

class GracePeriodExpiresEmail extends Mailable
{
    use Queueable, SerializesModels, CompanyTraits;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$email_plain = 'email.insurance_expires_in_20_days_plain';
        if ($this->data->company->id ==  WASTE_MANAGEMENT_CANADA) {
            $email_template = 'email.grace_expire_wmca';
        } elseif ($this->data->service_plan == PLAN_FAVR) {
            $email_template = 'email.grace_expire_favr';
        } else {
            $email_template = 'email.grace_expire_463';
        }

        // generate dynamic items to be circled
        $business_use = $this->getCompanyBusinessUse($this->data->company->id);
        $circle = $this->data->circle;
        $i = 4;
        if( $business_use) {
            $circle[] = "{$i}.  Proof of Business Use Insurance";
            $i++;
        }
        if( count($this->data->insurance_amounts) ) {
            $circle[] = "{$i}.  Coverage Amounts required by " . $this->data->company->name;
        }
        $this->data->circle = $circle;

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject('CarData Online: Your Grace Period has Expired')
            ->text($email_template)->with('data', $this->data);
    }

}