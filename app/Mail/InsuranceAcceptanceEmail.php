<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InsuranceAcceptanceEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_plain = 'email.insurance_acceptance';

        return $this
            ->from($this->data->coordinator->email)
            ->subject('CarData Online: Insurance Accepted')
            // ->view($email_template)->with('data', $this->data) // HTML email
            ->text($email_plain)->with('data', $this->data);
    }
}
