<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ManagerApprovalReminderEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = 'email.manager_approval_reminder';
        $email_plain = 'email.manager_approval_reminder_plain';

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject('CarData Online: Manager Approval Reminder')
           // ->view($email_template)->with('data', $this->data) // HTML email
            ->text($email_plain)->with('data', $this->data);
    }
}
