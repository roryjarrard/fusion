<?php
/**
 * Created by PhpStorm.
 * User: Jerzy 15
 * Date: 6/11/2018
 * Time: 2:39 PM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FAVRAnnualDeclarationReminderEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_plain = 'email.favr_annual_declaration_reminder_plain';

        $circle = $this->data->circle;
        if( $this->data->business_use_required) {
            $circle[] = "Proof of Business Use Insurance";
        }
        $this->data->circle = $circle;

        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject('Reminder: VRP Annual Declaration Requirements')
            ->text($email_plain)->with('data', $this->data);
    }

}

