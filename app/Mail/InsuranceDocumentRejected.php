<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InsuranceDocumentRejected extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_plain = 'email.insurance_document_rejected';

        // construct the required fields for the email
        $content = new \stdClass();
        $content->processor = User::find($this->data->processor_id);
        $content->custom_rejection_message = sizeof($this->data->custom_rejection_message) > 0 ? $this->data->custom_rejection_message : null;
        $content->rejectionReasons = [];
        foreach($this->data->reasons as $reason) {
            if ($reason['value'] == 1) {
                $content->rejectionReasons[] = $reason['text'];
            }
        }

        $content->driver = User::find($this->data->user_id);

        return $this
            ->from($content->processor->email)
            ->subject('CarData Online: Insurance Document Rejected')
            ->text($email_plain)->with('data', $content);
    }
}
