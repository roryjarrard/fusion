<?php
/**
 * Created by PhpStorm.
 * User: Jerzy 15
 * Date: 6/18/2018
 * Time: 11:19 AM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Http\Traits\CompanyTraits;

class InsuranceExpiresTodayEmail extends Mailable
{
    use Queueable, SerializesModels, CompanyTraits;

    protected $data;

    /**
     * Create a new message instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if( $this->data->company->service_plan == 'FAVR') {
            $email_plain = 'email.insurance_expires_FAVR_plain';
        } else {
            $email_plain = 'email.insurance_expires_463_plain';
        }

        $subject = $this->data->company->id == WASTE_MANAGEMENT_USA  ?
            "YOUR INSURANCE GRACE PERIOD HAS EXPIRED"
            :
            "Insurance Documents Required";

        // generate dynamic items to be circled
        $business_use = $this->getCompanyBusinessUse($this->data->company->id);
        $circle = $this->data->circle;
        $i = 4;
        if( $business_use) {
            $circle[] = "{$i}.  Proof of Business Use Insurance";
            $i++;
        }
        if( count($this->data->insurance_amounts) ) {
            $circle[] = "{$i}.  Coverage Amounts required by " . $this->data->company->name;
        }
        $this->data->circle = $circle;



        return $this
            ->from(ALERTS_EMAIL_ADDRESS)
            ->subject($subject)
            ->text($email_plain)->with('data', $this->data);
    }

}