<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReimbursementCalculatorSession extends Model
{
    //
    protected $fillable = [
        'user_id',
        'company_name',
        'vehicle_year',
        'fuel_year',
        'fuel_month_id',
        'country_id',
        'state_province_id',
        'zip_postal_code',
        'city_name',
        'mileage_band_id',
        'territory_type_id',
        'territory_list',
        'vehicle_id',
        'capital_cost',
        'resale_condition_id',
        'retention',
        'business_use_percent',
        'capital_cost_adj',
        'fuel_economy_adj',
        'maintenance_adj',
        'repair_adj',
        'depreciation_adj',
        'insurance_adj',
        'fuel_price_adj',
        'resale_value_adj',
        'capital_cost_tax_adj',
        'monthly_payment_adj',
        'finance_cost_adj',
        'fee_renewal_adj',
        'fixed_adj',
        'variable_adj',
        'service_plan',
    ];
}
