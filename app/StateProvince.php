<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateProvince extends Model
{
    protected $fillable = ['name', 'short_name', 'country_id', 'use_state_id', 'time_zone_id', 'region_id'];

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function timeZone()
    {
        return $this->belongsTo('App\TimeZone');
    }

    public function holidays()
    {
        return $this->hasMany('App\Holiday', 'state_province_id', 'id');
    }
}
