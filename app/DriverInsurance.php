<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverInsurance extends Model
{
    protected $fillable = [
        'user_id',
        'bodily_liability_per_person',
        'bodily_liability_per_accident',
        'property_damage',
        'deductible',
        'comprehensive_deductible',
        'collision_deductible',
        'medical',
        'uninsured_per_person',
        'uninsured_per_accident',
        'public_liability',
        'business_use',
        'expiration_date',
        'next_review_date',
        'is_legible',
        'approved',
        'approver_id',
        'comment',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->user->driverProfile->conpany;
    }
}
