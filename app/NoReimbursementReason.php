<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoReimbursementReason extends Model
{
    protected $fillable = ['user_id', 'year', 'month', 'street_light_id'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
