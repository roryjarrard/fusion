<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountyTax extends Model
{
    protected $fillable = ['county_id', 'year', 'county_sales_tax', 'county_sales_tax', 'license_fee', 'title_fee',
        'registration_fee', 'renewal_fee', 'other_fees', 'emission_fee' ];

    public function county(){
        return $this->belongsTo('App\County', 'county_id');
    }
}
