<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
    protected $fillable = ['name', 'make_id'];

    public function make() {
        return $this->hasMany('App\VehicleMake');
    }
}
