<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentRejectionReason extends Model
{
    protected $fillable = ['reason', 'favr'];
}
