<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordResetApi extends Model
{
    protected $fillable = ['user_id', 'code', 'expires_at'];
}
