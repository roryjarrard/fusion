<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MirouteUserAccess extends Model
{
    protected $fillable = ['user_id'];
}
