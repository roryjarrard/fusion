<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'name', 'year', 'msrp', 'msrp_ca', 'invoice', 'invoice_ca', 'blue_book', 'epa_city', 'epa_city_ca',
        'epa_highway', 'epa_highway_ca', 'blended_vehicle', 'mileage_group_id', 'geo_group_id',
        'kbb_vehicle_id', 'rank', 'favr', 'leading_vehicle', 'related_to', 'last_year_id', 'display_order'
    ];

    public function category() {
        return $this->hasOne('App\VehicleCategory', 'id', 'category_id');
    }

    public function formatted_name()
    {
        return $this->year . ' ' . $this->name . ' trim level' . $this->rank;
    }
}
