<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResaleCondition extends Model
{
    protected $fillable = ['id', 'name'];
}
