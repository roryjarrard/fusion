<?php namespace App\Handlers\Events;

use App\ChangeTracker;
use App\Company;
use App\DriverProfile;
use Illuminate\Support\Facades\Auth;

class DriverProfileEvents
{

    protected $random_variable = null;
    private $model_name = 'Driver Profile';

    public function __construct()
    {

    }

    /**
     * This function is called whenever DriverProfile::create(data) is utilized in the project. Since the driver profile
     * utilizes soft deletes, making actions for deleted or updated would be redundant.
     *
     * @param DriverProfile $driverProfile
     */
    public function driverProfileCreated(DriverProfile $driverProfile)
    {
        $company_id =  $driverProfile->company_id;

        $oldDriverProfile = DriverProfile::onlyTrashed()
            ->where('user_id', $driverProfile->user_id)
            ->orderBy('deleted_at', 'desc')
            ->first()->toArray();

        $dp = $driverProfile->toArray();
        foreach ($dp as $k => $v) {
            if ($v !== $oldDriverProfile[$k]) {
                if (!in_array($k, ['id', 'deleted_at', 'created_at', 'updated_at'])) {
                    $ct = ChangeTracker::create([
                        'target' => $dp['user_id'],
                        'author' => Auth::user()->id,
                        'company_id' => $company_id,
                        'model' => $this->model_name,
                        'attribute' => $k,
                        'action' => 'Update',
                        'old_value' => $oldDriverProfile[$k],
                        'new_value' => $v,
                        'notes' => ''
                    ]);
                }
                //\Log::info("Attribute changed {$k} from {$oldDriverProfile[$k]} to {$dp[$k]}");
            }
        }


        /*if ($driverProfile->street_light_id != $oldDriverProfile->street_light_id) {
            $oldLight = $oldDriverProfile->streetLight()->first();
            $newLight = $driverProfile->streetLight()->first();

            $oldValue = $oldLight->color . ' ' . $oldLight->title;
            $newValue = $newLight->color . ' ' . $newLight->title;

            $ct = ChangeTracker::create([
                'to_whom' => $driverProfile->user_id,
                'by_whom' => Auth::user()->id,
                'change_type' => 'Street',
                'change_description' => 'Street Light',
                'old_value' => $oldValue,
                'new_value' => $newValue,
                'notes' => 'Update to street light'
            ]);
        }*/


        /**
         * This function has a sole purpose of setting reimbursement detail id to null
         * This will ensure that reimbursement is recalculated at the start of next month
         * And it will force the correct reimbursement value to be calculated on pages
         * where reimbursement data is shown
         */
//        $this->resetReimbursementDetailId($driverProfile->id);

//        // create a dictionary to hold the values we will or willn't recalculate
//        $valuesToRecalculate = [
//            'closest_fuel_city_id' => false,
//            'fixed_reimbursement' => false,
//        ];
//
//        // get the old driver profile
//        $oldDriverProfile = \App\DriverProfile::onlyTrashed()
//            ->where('user_id', $driverProfile->user_id)
//            ->orderBy('deleted_at', 'desc')
//            ->first();
//
//        /** call the trait to compare the two driver profiles, store the changes
//            I propose a return object of the form
//                changes = [
//                    'field_name' = [
//                        'old' = x,
//                        'new' = y,
//                    ],
//                   ...
//                ]
//        */
//
//        $changes = $this->compareDriverProfies($driverProfile, $oldDriverProfile);
//
//        // go through the potential list of changes, and note what calculations are required
//
//        if ( array_key_exists("address_id", $changes) ) {
//
//            /**
//             *   Here we know address was changed.
//             *   This could have an effect on BOTH the nearest fuel city and the FIXED REIMBURSEMENT
//             *   We thus update the valuesToRecalculate to reflect these changes
//             */
//            $valuesToRecalculate['closest_fuel_city_id'] = true;
//            $valuesToRecalculate['fixed_reimbursement'] = true;
//
//        }
//
//        if ( 'some other condition ...' ) {
//
//        }
//
//
//        /**
//         *   Once we have exhausted all conditions we check valuesToRecalculate and perform the appropriate
//         *   recalculations and updates
//         */
//        if ($valuesToRecalculate['closest_fuel_city_id'] == true) {
//
//            //call the trait to calculate closest fuel city
//            $new_closest_fuel_city_id = $this->findClosestFuelCityId($driverProfile->address());
//
//            /**
//             *  Using the query builder and NOT eloquent ensures no laravel model events are fired.
//             *  Furthermore, we have our event listener on CREATED. Since we soft delete and recreate,
//             *  an eloquent update COULD NOT ever trigger this function.
//             */
//
//            \DB::table('driver_profiles')
//                ->where('id', $driverProfile->id)
//                ->update('closest_fuel_city_id', $new_closest_fuel_city_id);
//
//        }
//
//        if ($valuesToRecalculate['fixed_reimbursement'] == true) {
//
//            //call the trait to calculate fixed reimbursement for driver
//            $new_fixed_reimbursement = $this->calculateFixedReimbursementForDriver($driverProfile->id);
//
//            \DB::table('driver_profiles')
//                ->where('id', $driverProfile->id)
//                ->update('fixed_reimbursement', $new_fixed_reimbursement);
//
//        }
    }

    public function driverProfileUpdated(DriverProfile $driverProfile)
    {

        /**
         * This function has a sole purpose of setting reimbursement detail id to null
         * This will ensure that reimbursement is recalculated at the start of next month
         * And it will force the correct reimbursement value to be calculated on pages
         * where reimbursement data is shown
         */
//        $this->resetReimbursementDetailId($driverProfile->id);
    }

    public function driverProfileDeleted(DriverProfile $driverProfile)
    {

    }

    /**
     * This function has a sole purpose of setting reimbursement detail id to null
     * This will ensure that reimbursement is recalculated at the start of next month
     * And it will force the correct reimbursement value to be calculated on pages
     * where reimbursement data is shown
     *
     * @param driver_profile_id (int) - id of a driver_profiles record
     */
    private function resetReimbursementDetailId($driver_profile_id)
    {
        \DB::table('driver_profiles')
            ->where('id', $driver_profile_id)
            ->update([
                'reimbursement_detail_id' => null
            ]);
    }
}