<?php namespace App\Handlers\Events;

use App\Address;
use App\PostalCode;
use App\TimeZone;
use App\ZipCode;

class AddressEvents
{

    protected $random_variable = null;

    public function __construct()
    {

    }

    public function addressCreating(Address $address) {
        // check if we have this zip or postal code
        \Debugbar::info('address Creating:');

        $company_id = $address->user->driverProfile->company_id;

        $oldAddress = Address::onlyTrashed()
            ->where('user_id', $address->user_id)
            ->orderBy('deleted_at', 'desc')
            ->first()->id;

        $ct = ChangeTracker::create([
            'to_whom' => $address->user_id,
            'by_whom' => Auth::user()->id,
            'company_id' => $company_id,
            'change_type' => 'Address',
            'change_description' => 'New Address',
            'old_value' => $oldAddress,
            'new_value' => $address->id,
            'notes' => ''
        ]);

        $key = 'AIzaSyB4cVyUmAOKJC0foRNHAjROST06W7U8Iow';
        //TODO: get google key from env.

        $zip = $address->country_id == 1 ?
            PostalCode::find($address->zip_postal) :
            ZipCode::find($address->zip_postal);
        if (!$zip) {
            $postal_code = $address->zip_postal;
            $city_name = $address->city;
            $data = StateProvince::select('short_name')->find($address->state_province_id);
            $state_province = $data->short_name;

            $address_encoded = urlencode("$city_name,$postal_code,$state_province");
            $url = "https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=$address_encoded&key=$key";
            $response = file_get_contents($url);
            $json = json_decode($response, TRUE);

            if( isset($json['results'][0])) {
                $lat = $json['results'][0]['geometry']['location']['lat'];
                $lng = $json['results'][0]['geometry']['location']['lng'];

                if ($address->country_id == 1) { // US
                    $zip = new ZipCode;
                    $zip->zip_code = $address->zip_postal;
                    $zip->longitude = $lng;
                    $zip->latitude = $lat;
                    $zip->city_name = $address->city;
                    $zip->state_id = $address->state_province_id;

                    $time = time();
                    $url = "https://maps.googleapis.com/maps/api/timezone/json?location=$lat,$lng&timestamp=$time&key=$key";
                    $response = file_get_contents($url);
                    $json = json_decode($response, TRUE);
                    \Debugbar::info('TIMEZONE response:');
                    \Debugbar::info($json);
                    $tzn = $json['results']['timeZoneName'];
                    $rec = TimeZone::where('name',$tzn)->first();
                    \Debugbar::info($rec);

                    // $zip->save();
                } elseif ($address->country_id == 2) { // Canada

                }
            }
        }
    }
}
