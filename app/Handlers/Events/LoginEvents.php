<?php namespace App\Handlers\Events;

use App\Login;
use App\User;

class LoginEvents {

    protected $random_variable = null;

    public function __construct()
    {

    }

    public function loginAttempting($request)
    {
        /*
        \Log::info('Login Attempting....', (array)$request);
        $credentials = $request->credentials;
        $username = $credentials['username'];
        \Log::info('credentials',$credentials);
        //$username = $credentials['username'];
        \Log::info('username',[$username]);
        \Log::info('User Info', ['Agent' => $_SERVER['HTTP_USER_AGENT']]);
        */
    }

    public function loginSuccess($request)
    {
        \Log::info('Login Success....', (array)$request);

        $username = $request->user->username;
        $ip = $_SERVER['REMOTE_ADDR'];
        $address = gethostbyaddr($ip);
        $platform = $_SERVER['HTTP_USER_AGENT'];

        $login = new Login;
        $login->username = $username;
        $login->ip = $ip;
        $login->address = $address;
        $login->login_type = 'fusion';
        $login->platform = $platform;
        $login->access = 'granted';
        $login->save();

    }

    public function loginFailed($request)
    {
        \Log::info('Login Failed', (array)$request);

        $credentials = $request->credentials;
        $username = $credentials['username'];
        $password = $credentials['password'];

        $ip = $_SERVER['REMOTE_ADDR'];
        $address = gethostbyaddr($ip);
        $platform = $_SERVER['HTTP_USER_AGENT'];

        $reason = '';
        $user = User::where('username',$username)->first();
        if( !$user ) {
            $reason = ' unknown username';
        } else {
            //$input['password'] = \Hash::make($password); //TODO:  <-- should do this
            $entered_password = \Hash::make('secret'); //TODO: <-- instead of this
            if( $entered_password != $user->password ) {
                $reason = ' invalid password';
            } else if(!empty($user->deleted_at)) {
                $reason = ' inactive user';
            }
        }

        $login = new Login;
        $login->username = $username;
        $login->ip = $ip;
        $login->address = $address;
        $login->login_type = 'fusion';
        $login->platform = $platform;
        $login->access = 'failed' . $reason;
        $login->save();

    }

    public function logout($request)
    {
        \Log::info('Logout', (array)$request);
    }

    public function lockout($request)
    {
        \Log::info('Lockout', (array)$request);
    }
}
