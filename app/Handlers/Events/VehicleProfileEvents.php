<?php namespace App\Handlers\Events;

use App\SuperProfile;
use App\NotificationTemplate;
use App\VehicleProfile;
use App\VehicleProfileApproval;

class VehicleProfileEvents
{

    protected $random_variable = null;

    public function __construct()
    {

    }

    /**
     * When a new VehicleProfile is created, set the vehicle profile to be awaiting approval and,
     * notify all supers who are vehicle profile approvers
     */
    public function vehicleProfileCreated(VehicleProfile $vehicleProfile)
    {

        // these vehicle profiles were created on the rate comparison tool and should not alert approvers
        if ($vehicleProfile->proposed == 1) {
            return;
        }

        VehicleProfileApproval::create([ 'vehicle_profile_id' => $vehicleProfile['id'] ]);

        //find all vehicle profile approvers
        $users_to_notify = SuperProfile::where('is_vehicle_profile_approver', true)->get()->pluck('user_id')->all();

        //get the appropriate notification
        $notification = NotificationTemplate::where('slug', 'new-vehicle-profile-approval')->first();

        //send notification to the supers, ensure it is visible only to supers
        $notification->fusionNotify($users_to_notify, 'super');

    }


    public function vehicleProfileUpdated(VehicleProfile $vehicleProfile)
    {
        if ($vehicleProfile['proposed'] != 1) {
            return;
        } else {

            // if a proposed vehicle profile is updated, we delete its former approval record. It will go into a state
            // of awaiting submission for approval.
            $approval = VehicleProfileApproval::where('vehicle_profile_id', $vehicleProfile['id'])->first();
            if ($approval) {
                $approval->forceDelete();
            }

        }


    }


    public function vehicleProfileDeleted(VehicleProfile $vehicleProfile)
    {

    }


}
