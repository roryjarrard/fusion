<?php namespace App\Handlers\Events;

use App\PersonalVehicle;
use App\AnnualIrsOdometerDeclaration;
use App\DriverProfile;

class PersonalVehicleEvents
{

    protected $random_variable = null;

    public function __construct()
    {

    }

    // TODO: DISCUSS FURTHER AND WHETHER OR NOT TO DELETE ALL OF THESE

    /**
     * When a new personalVehicle is created, if the driver's company has service plan FAVR
     * Create an annual irs odometer declaration record
     *
     * @param PersonalVehicle $personalVehicle
     */
    public function personalVehicleCreated(PersonalVehicle $personalVehicle)
    {
//        $driverProfile = DriverProfile::where('user_id', $personalVehicle->user_id)->first();
//
//        $service_plan = $driverProfile->company->options->service_plan;
//
//        if ($service_plan == 'FAVR') {
//
//            $minimum_vehicle_year = date('Y') - $driverProfile->vehicleProfile->max_vehicle_age;
//
//            AnnualIrsOdometerDeclaration::create([
//                'user_id' => $personalVehicle->user_id,
//                'personal_vehicle_id', $personalVehicle->id,
//                'odometer' => $personalVehicle->odometer,
//                'minimum_vehicle_year' => $minimum_vehicle_year,
//            ]);
//
//        }


    }

    /**
     * When a personalVehicle is updated, if the driver's company has service plan FAVR
     * Create an annual irs odometer declaration record
     *
     * @param PersonalVehicle $personalVehicle
     */
    public function personalVehicleUpdated(PersonalVehicle $personalVehicle)
    {
//        $driverProfile = DriverProfile::where('user_id', $personalVehicle->user_id)->first();
//
//        $service_plan = $driverProfile->company->options->service_plan;
//
//        if ($service_plan == 'FAVR') {
//
//            $declaration = AnnualIrsOdometerDeclaration::where('personal_vehicle_id', $personalVehicle->id)
//                ->orderBy('created_at', 'desc')
//                ->first();
//
//            $declaration->update([
//                'odometer' => $personalVehicle->odometer,
//            ]);
//        }
    }


    public function personalVehicleDeleted(PersonalVehicle $personalVehicle)
    {

    }
}

