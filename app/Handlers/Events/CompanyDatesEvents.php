<?php namespace App\Handlers\Events;

use App\ChangeTracker;
use App\Company;
use App\CompanyDates;
use Illuminate\Support\Facades\Auth;

class CompanyDatesEvents
{

    protected $random_variable = null;
    private $model_name = 'Company Dates';

    public function __construct()
    {

    }

    /**
     * This function is called whenever CompanyDates::create(data) is utilized in the project. Since the company dates
     * utilizes soft deletes, making actions for deleted or updated would be redundant.
     *
     * @param DriverProfile $driverProfile
     */
    public function companyDatesCreated(CompanyDates $companyDates)
    {
        $company_id =  $companyDates->company_id;

        $oldDates = CompanyDates::onlyTrashed()
            ->where('company_id', $company_id)
            ->orderBy('deleted_at', 'desc')
            ->first()->toArray();

        $cd = $companyDates->toArray();
        foreach ($cd as $k => $v) {
            if ($v !== $oldDates[$k]) {
                if (!in_array($k, ['id', 'deleted_at', 'created_at', 'updated_at'])) {
                    $ct = ChangeTracker::create([
                        'target' => $cd['user_id'],
                        'author' => Auth::user()->id,
                        'company_id' => $company_id,
                        'model' => $this->model_name,
                        'attribute' => $k,
                        'action' => 'Update',
                        'old_value' => $oldDates[$k],
                        'new_value' => $v,
                        'notes' => ''
                    ]);
                }
                //\Log::info("Attribute changed {$k} from {$oldDriverProfile[$k]} to {$dp[$k]}");
            }
        }
    }

    public function companyDatesUpdated($companyDates)
    {
        \Debugbar::info('XXXXXXXXXXXXXXXXXXXXXX');
        die('this is not working');
        \Log::info('Updated Company Dates ' . var_dump($companyDates) . var_dump($companyDates->getOriginal()));
    }

    public function companyDatesUpdating($companyDates)
    {
        \Log::info('Updating Company Dates ' . var_dump($companyDates) . var_dump($companyDates->getOriginal()));
    }
}