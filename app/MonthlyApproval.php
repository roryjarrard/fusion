<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyApproval extends Model
{
    public $timestamps = false;

    protected $fillable = ['company_id', 'year', 'month', 'file_fee', 'driver_fee', 'approval_date'];

    public function approver() {
        return $this->hasMany('App\MonthlyApprover');
    }
}
