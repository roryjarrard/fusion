<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReimbursementDetail extends Model
{
    // in _adj we keep percentage of adjustments

    protected $fillable = ['capital_cost', 'capital_cost_adj', 'resale_value', 'resale_value_adj', 'tax', 'tax_adj',
        'depreciation', 'depreciation_adj', 'monthly_payment', 'monthly_payment_adj', 'insurance', 'insurance_city',
        'insurance_adj', 'finance_cost', 'finance_cost_adj', 'fee_renewal', 'fee_renewal_adj', 'fuel_economy',
        'fuel_economy_adj', 'maintenance', 'maintenance_adj', 'repair', 'repair_ad', 'fixed_adj', 'cents_per_mile_adj',
        'fixed_reimbursement', 'cents_per_mile', 'business_use_percent'];
}
