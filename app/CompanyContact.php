<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyContact extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id', 'type', 'title', 'first_name', 'last_name', 'email', 'phone_number',
        'mobile_number', 'fax_number'];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    // TODO: add relations for all coordinators
}
