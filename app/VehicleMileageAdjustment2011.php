<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleMileageAdjustment2011 extends Model
{
    protected $table = 'vehicle_mileage_adjustment_2011';

    protected $fillable = ['model_year', 'resale_year', 'mileage_group_id', 'mileage_range_min', 'mileage_range_max',
        'adjustment_type', 'adjustment'];
}
