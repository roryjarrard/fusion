<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MileageBand extends Model
{
    protected $fillable = ['mr_id', 'mileage_start', 'mileage_end', 'kbb_mileage'];


    public function driverProfiles()
    {
        return $this->hasMany('App\DriverProfile','mileage_band_id', 'id');
    }

    public function name()
    {
        return ($this->id < 11) ? number_format($this->mileage_start) . ' - ' . number_format($this->mileage_end)
            :
            '> ' . number_format($this->mileage_start);
    }
}
