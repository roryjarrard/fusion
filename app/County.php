<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    protected $fillable = ['name', 'state_id', 'aaa_id'];

    public function stateProvince() {
        return $this->belongsTo('App\StateProvince');
    }
}
