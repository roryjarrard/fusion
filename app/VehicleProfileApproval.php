<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleProfileApproval extends Model
{

    protected $fillable = [
        'vehicle_profile_id',
        'approved',
        'approver_id',
        'note',
    ];

    //
    public function vehicleProfile() {
        $this->belongsTo('App\VehicleProfile');
    }
}
