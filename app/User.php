<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Database\Eloquent\SoftDeletes;
use HttpOz\Roles\Traits\HasRole;
use HttpOz\Roles\Contracts\HasRole as HasRoleContract;

class User extends Authenticatable implements HasRoleContract
{
    use HasApiTokens, Notifiable, HasRole;

    //use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active', 'username', 'email', 'personal_email', 'password', 'first_name', 'last_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function accessToken()
    {
        return $this->hasMany('App\OauthAccessToken');
    }

    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }

    public function isDriver()
    {
//        return $this->isImpersonating() ? DriverProfile::where('user_id', $this->id)->count() : DriverProfile::where('user_id', $this->id)->where('active', 1)->count();
        return DriverProfile::where('user_id', $this->id)->count() > 0;
    }

    public function isManager()
    {
//        return $this->isImpersonating() ? ManagerProfile::where('user_id', $this->id)->count() : ManagerProfile::where('user_id', $this->id)->where('active', 1)->count();
        return ManagerProfile::where('user_id', $this->id)->count() > 0;
    }

    public function isAdmin()
    {
//        return $this->isImpersonating() ? AdministratorProfile::where('user_id', $this->id)->count() : AdministratorProfile::where('user_id', $this->id)->where('active', 1)->count();
        return AdministratorProfile::where('user_id', $this->id)->count() > 0;
    }

    public function isSuper()
    {
        return $this->hasRole(4);
    }

    public function company()
    {
        if ($this->isDriver()) {
            return $this->driverProfile->company;
        } elseif ($this->isManager()) {
            return $this->managerProfiles[0]->company;
        } elseif ($this->isAdmin()) {
            return $this->administratorProfiles[0]->company;
        } else {
            return null;
        }
    }

    public function companies()
    {
        return $this->belongsToMany('App\Company')->withPivot('role_id');
    }

    public function address()
    {
        return $this->hasOne('App\Address');
    }

    public function banking($country_id = null, $masked = null)
    {

        //TODO FIX THIS OLD WAY WAS COMMENTED
        if ($country_id == 1 || $country_id == 2) {
//            $banking = \DB::table('user_bank_accounts')->where('user_id', $this->id)
//                ->where('country_id', $country_id)
//                ->whereNull('deleted_at')
//                ->first();
            $banking = \DB::table('user_bank_accounts')->where('user_id', $this->id)
                ->where('country_id', $country_id)
                ->first();
        } else {
//            $banking = \DB::table('user_bank_accounts')->where('user_id', $this->id)->whereNull('deleted_at')->first();
            $banking = \DB::table('user_bank_accounts')->where('user_id', $this->id)->first();
        }

        if ($banking == null) {
            return null;
        }

        if ($banking->country_id == 1) {
            $banking->routing_number = decrypt($banking->routing_number);
        }

        $banking->account_number = decrypt($banking->account_number);

        // TODO FORCING MASKED BANKING INFO FOR TESTING
        if ($masked) {
            //mask account number
            $last_four_digits = substr($banking->account_number, -4);
            $length = strlen($banking->account_number);
            $banking->account_number = str_repeat('x', $length - 4) . $last_four_digits;

            //mask routing number
            $banking->routing_number = 'xxxxxx' . substr($banking->routing_number, -3);
        }

        return $banking;
    }

    public function profile()
    {
        return $this->hasOne('App\UserProfile');
    }

    public function driverProfile()
    {
        return $this->hasOne('App\DriverProfile');
    }

    public function managerProfiles()
    {
        return $this->hasMany('App\ManagerProfile');
    }

    public function administratorProfiles()
    {
        return $this->hasMany('App\AdministratorProfile');
    }

    public function superProfile()
    {
        return $this->hasOne('App\SuperProfile');
    }

    public function managerProfile($company_id)
    {
        return ManagerProfile::where('user_id', $this->id)->where('company_id', $company_id)->first();
    }

    public function administratorProfile($company_id)
    {
        return AdministratorProfile::where('user_id', $this->id)->where('company_id', $company_id)->first();
    }

    public function setImpersonating($id)
    {
        \Session::put('impersonate', $id);
    }

    public function stopImpersonating()
    {
        \Session::forget('impersonate');
    }

    public function isImpersonating()
    {
        return \Session::has('impersonate');
    }

    public function impersonatedUser()
    {
        return User::find(session()->get('impersonate'));
    }

    public function mileages()
    {
        return $this->hasMany('App\MonthlyMileage');
    }

    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    /*public function driver_insurances()
    {
        return $this->hasMany('App\DriverInsurance');
    }*/

    public function insurance_reviews()
    {
        return $this->hasMany('App\DriverInsuranceReview');
    }

    /**
     * Sends an internal notification to this user, requires the notification template's slug (title, hyphenated, lowercase)
     */
    public function fusionNotify($slug, $note = null, $visible_to = 'all')
    {
        $notifications = $this->notifications;
        $existingNotification = $notifications->filter(function ($notification) use ($slug) {
            return $notification->slug == $slug;
        })->first();
        if (!empty($existingNotification)) {
            $existingNotification->read = 0;
            $existingNotification->save();
            return;
        }


        NotificationTemplate::where('slug', $slug)
            ->first()
            ->fusionNotify($this->id, $note, $visible_to);
    }

    public function company_name()
    {
        $company_name = '';
        if ($this->isDriver()) {
            $company_name = $this->driverProfile->company->name;
        } elseif ($this->isManager()) {
            $company_name = $this->managerProfiles[0]->company->name;
        } elseif ($this->isAdmin()) {
            $company_name = $this->administratorProfiles[0]->company->name;
        }

        return $company_name;
    }

    public function savedStops()
    {
        return $this->belongsToMany('App\SavedStop', 'driver_stop');
    }
}
