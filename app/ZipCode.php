<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{
    protected $fillable = ['zip_code', 'longitude', 'latitude', 'city_name', 'state_id', 'county_id', 'time_zone_id'];

    public $primaryKey = 'zip_code';

    public function state() {
        return $this->belongsTo('App\StateProvince', 'state_id');
    }

    public function timeZone(){
        return $this->belongsTo('App\TimeZone', 'time_zone_id');
    }
}
