<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyTrip extends Model
{
    protected $fillable = [
        'trip_date',
        'saved_stop_id',
        'daily_mileage_id',
        'sequence_number',
        'event',
        'business_purpose',
        'mileage_original',
        'mileage_adjusted',
        'adjustment_comment'
    ];

    public function dailyMileage() {
        return $this->belongsTo('App\DailyMileage');
    }

    public function savedStop() {
        return $this->belongsTo('App\SavedStop');
    }
}