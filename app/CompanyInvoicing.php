<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyInvoicing extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id', 'invoicing_interval', 'per_driver_fee'];
}
