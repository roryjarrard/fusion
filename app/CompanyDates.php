<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyDates extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id', 'mileage_reminder_day', 'second_mileage_reminder_day',
        'mileage_lock_driver_day', 'mileage_lock_all_day', 'mileage_approval_end_day',
        'direct_pay_day', 'pay_file_day', 'no_reimbursement_notification_day'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($companyDates) {
            ChangeTracker::create([
                'target' => $companyDates->id,
                'author' => Auth()->user()->id,
                'company_id' => $companyDates->company_id,
                'model' => 'Company Dates',
                'attribute' => 'Object',
                'action' => 'created',
                'old_value' => null,
                'new_value' => $companyDates->id,
                'notes' => '',
            ]);
        });

        static::updated(function ($companyDates) {
            $dates = $companyDates->toArray();
            $oldDates = $companyDates->getOriginal();

            foreach ($dates as $k => $v) {
                if (!in_array($k, ['id', 'created_at', 'updated_at', 'deleted_at'])) {
                    if ($v != $oldDates[$k]) {
                        ChangeTracker::create([
                            'target' => $companyDates->id,
                            'author' => Auth()->user()->id,
                            'company_id' => $companyDates->company_id,
                            'model' => 'Company Dates',
                            'attribute' => $k,
                            'action' => 'updated',
                            'old_value' => $oldDates[$k],
                            'new_value' => $v,
                            'notes' => '',
                        ]);
                    }
                }
            }
        });

        static::deleted(function ($companyDates) {
            ChangeTracker::create([
                'target' => $companyDates->id,
                'author' => Auth()->user()->id,
                'company_id' => $companyDates->company_id,
                'model' => 'Company Dates',
                'attribute' => 'Object',
                'action' => 'deleted',
                'old_value' => $companyDates->id,
                'new_value' => null,
                'notes' => '',
            ]);
        });
    }
}
