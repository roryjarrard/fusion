<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxesFeesCA extends Model
{
    protected $table = 'taxes_fees_ca';

    protected $fillable = ['year', 'state_province_id', 'sales_tax', 'initial_registration', 'annual_renewal', 'annual_safety'];
}
