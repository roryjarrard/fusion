<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CraRate extends Model
{
    protected $fillable = ['year', 'below_5000', 'above_500' ];
}
