<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyMileage extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id',  'mileage_entry_method', 'mileage_band_audit_method',
        'client_views_mileage_band_audit', 'miroute_transition_date', 'new_driver_mileage_grace_days'];

    public function company() {
        return $this->belongsTo('App\Company');
    }
}
