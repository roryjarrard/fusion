<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverTax extends Model
{
    protected $table = 'driver_taxes';

    protected $fillable = ['driver_id','year'];
}