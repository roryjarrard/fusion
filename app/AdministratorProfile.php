<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdministratorProfile extends Model
{
    use SoftDeletes;

    protected $casts = ['limited' => 'boolean'];
    protected $fillable = [
        'user_id',
        'active',
        'company_id',
        'administrator_id',
        'administrator_number',
        'administrator_phone',
        'company_approver',
        'company_approver_phone',
        'company_coordinator',
        'company_coordinator_phone',
        'miroute_coordinator',
        'miroute_coordinator_phone',
        'insurance_coordinator',
        'insurance_coordinator_phone',
        'is_limited',
        'address_id',
        'assigned_divisions'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function address()
    {
        //return $this->hasOne('App\Address');

        return Address::where('user_id', $this->user_id)->first();
    }

    /**
     * @return $divisions
     *
     * if admin has 'some' assigned divisions, return those assigned to him
     * elseif admin has 'all' divisions assigned, return all divisions for the company
     * else admin has 'none', so return null;
     */
    public function divisions() {
        $divisions = [];
        if ($this->assigned_divisions == 'some') {
            $divisions = \DB::table('administrator_profiles')->join('administrator_division', 'administrator_division.administrator_id', 'administrator_profiles.administrator_id')
                ->join('divisions', 'divisions.id', 'administrator_division.division_id')
                ->where('administrator_profiles.administrator_id', $this->administrator_id)
                ->whereNull('administrator_profiles.deleted_at')
                ->select('divisions.id', 'divisions.company_id', 'divisions.name', 'divisions.market')->get();
        } elseif ($this->assigned_divisions == 'all') {
            $divisions =  \DB::table('divisions')->where('divisions.company_id', $this->company_id)->get();
        }
        return $divisions;
    }
}
