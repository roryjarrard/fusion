<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed $model
 */
class PersonalVehicle extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'make_id',
        'model_id',
        'trim_id',
        'year',
        'odometer',
        'old_vehicle_odometer',
        'vehicle_cost',
        'depreciation_method',
        'note'
    ];

    protected $dates = ['deleted_at'];

    public function make()
    {
        return $this->belongsTo('App\VehicleMake');
    }

    public function model()
    {
        return $this->belongsTo('App\VehicleModel');
    }

    public function annualIrsOdometerDeclaration()
    {
        return $this->hasMany('App\AnnualIrsOdometerDeclaration', 'personal_vehicle_id', 'id');
    }
}
