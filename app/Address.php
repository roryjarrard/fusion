<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'street', 'street2', 'city', 'zip_postal', 'state_province_id', 'country_id',
        'latitude', 'longitude', 'map_latitude', 'map_longitude'];

    protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function stateProvince() {
        return $this->belongsTo('App\StateProvince');
    }

}
