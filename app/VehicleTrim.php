<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleTrim extends Model
{
    protected $fillable = ['name', 'model_id', 'year', 'order_by'];
}
