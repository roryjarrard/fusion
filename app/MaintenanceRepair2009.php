<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintenanceRepair2009 extends Model
{
    protected $table = 'maintenance_repair_2009';
    protected $fillable = ['vehicle_id','state_province_id','mr_mileage_band_id',
        'maintenance_y1','maintenance_y2', 'maintenance_y3', 'maintenance_y4', 'maintenance_y5',
        'repair_y1', 'repair_y3', 'repair_y3', 'repair_y4', 'repair_y5'];
}
