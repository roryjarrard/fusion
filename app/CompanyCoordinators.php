<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class CompanyCoordinators extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id', 'company_coordinator_id', 'company_coordinator_phone', 'cardata_coordinator_id',
        'cardata_coordinator_phone', 'insurance_coordinator_id', 'insurance_coordinator_phone', 'insurance_fax_number',
        'license_coordinator_id', 'license_coordinator_phone', 'license_fax_number', 'miroute_coordinator_id',
        'miroute_coordinator_phone', 'service_associate_id', 'account_executive_id'];

    /**
     * These event listeners do not have to be "registered" anywhere. However,
     * the model has to be "pulled" to trigger the event.
     *
     * Example:
     * $model->where('relation_id', 100)->delete() WILL NOT trigger
     * $model->where('relateion_id', 100)->first()->delete() WILL trigger
     *
     * Or use get() and loop if multiple instances.
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Upon creation we would not have 'old' values. You have to get the last
         * softDeleted object
         */
        static::created(function (CompanyCoordinators $model) {
            $newValues = $model->toArray();
            $oldValues = CompanyCoordinators::onlyTrashed()
                ->where('company_id', $model->company_id)
                ->orderBy('deleted_at', 'desc')
                ->first()->toArray();

            $user_id = auth()->user() ? auth()->user()->id : null;

            /**
             * Loop on key=>value pairs
             */
            foreach ($newValues as $k => $v) {
                if (!in_array($k, ['id', 'created_at', 'updated_at', 'deleted_at'])) {
                    if ($v !== $oldValues[$k]) {
                        ChangeTracker::create([
                            'target' => $newValues['id'],
                            'author' => $user_id,
                            'company_id' => $newValues['company_id'],
                            'model' => 'CompanyProfile',
                            'attribute' => $k,
                            'action' => 'updated',
                            'old_value' => $oldValues[$k],
                            'new_value' => $newValues[$k],
                            'notes' => ''
                        ]);
                    }
                }
            }
        });

        /**
         * When updating, get original values, then turn both new
         * and original values to arrays. Loop through keys, then
         * compare new and original values, and create a ChangeTracker
         * for every change.
         *
         * Attach company_id to all changes.
         *
         * Ignoring timestamps and primary keys.
         */
        static::updated(function (CompanyProfile $model) {
            $newValues = $model->toArray();
            $oldValues = $model->getOriginal(); // this returns an array

            $user_id = auth()->user() ? auth()->user()->id : null;

            /**
             * Loop on key=>value pairs
             */
            foreach ($newValues as $k => $v) {
                if (!in_array($k, ['id', 'created_at', 'updated_at', 'deleted_at'])) {
                    if ($v !== $oldValues[$k]) {
                        ChangeTracker::create([
                            'target' => $newValues['id'],
                            'author' => $user_id,
                            'company_id' => $newValues['company_id'],
                            'model' => 'CompanyCoordinator',
                            'attribute' => $k,
                            'action' => 'updated',
                            'old_value' => $oldValues[$k],
                            'new_value' => $newValues[$k],
                            'notes' => ''
                        ]);
                    }
                }
            }
        });

        /**
         * When deleting there is no "new" values.
         */
        static::deleted(function (CompanyAdministration $model) {
            $user_id = auth()->user() ? auth()->user()->id : null;
            ChangeTracker::create([
                'target' => $model->id,
                'author' => $user_id,
                'company_id' => $model->company_id,
                'model' => 'CompanyCoordinator',
                'attribute' => 'Model',
                'action' => 'deleted',
                'old_value' => $model->id,
                'new_value' => null,
                'notes' => ''
            ]);
        });
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
