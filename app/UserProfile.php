<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserProfile extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'phone', 'mobile_phone', 'work_phone', 'extension'];

    public function user () {
        return $this->belongsTo('App\User');
    }
}
