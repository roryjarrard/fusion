<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyReimbursement extends Model
{

    protected $fillable = [
        'user_id',
        'company_id',
        'driver_profile_id',
        'year',
        'month',
        'fixed_reimbursement',
        'variable_reimbursement',
        'adjustment_amount',
        'vehicle_depreciation',
        'taxable_amount',
        'within_grace_period',
        'fixed_status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function driverProfile()
    {
        return $this->belongsTo('App\DriverProfile');
    }

}
