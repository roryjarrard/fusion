<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduledTask extends Model
{
    protected $fillable = ['task_id', 'name', 'start_date', 'end_date', 'status', 'short_description', 'long_description'];
}
