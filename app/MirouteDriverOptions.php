<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class MirouteDriverOptions extends Model
{
    protected $fillable = ['user_id', 'access', 'tracking_method'];
}
