<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxQuarter extends Model
{
    protected $fillable = ['q1', 'q2', 'q3', 'q4'];
}
