<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostalCode extends Model
{
    protected $fillable = ['postal_code', 'longitude', 'latitude', 'city_name', 'province_id'];

    public $primaryKey = 'postal_code';
    public $incrementing = false;

    public function province() {
        return $this->belongsTo('App\StateProvince', 'province_id');
    }
}
