<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CompanyTaxes extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'company_id',
        'use_quarterly_tax_adjustment',
        'reports_display_monthly_tax_limit',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
