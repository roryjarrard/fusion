<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyProfile extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id', 'website', 'industry_id', 'street', 'street2', 'city', 'state_province_id',
        'zip_postal', 'country_id', 'time_zone_id',];

    /**
     * These event listeners do not have to be "registered" anywhere. However,
     * the model has to be "pulled" to trigger the event.
     *
     * Example:
     * $model->where('relation_id', 100)->delete() WILL NOT trigger
     * $model->where('relateion_id', 100)->first()->delete() WILL trigger
     *
     * Or use get() and loop if multiple instances.
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Upon creation we would not have 'old' values. You have to get the last
         * softDeleted object
         */
        static::created(function (CompanyProfile $model) {
            $newValues = $model->toArray();
            $oldValues = CompanyProfile::onlyTrashed()
                ->where('company_id', $model->company_id)
                ->orderBy('deleted_at', 'desc')
                ->first()->toArray();

            $user_id = auth()->user() ? auth()->user()->id : null;

            /**
             * Loop on key=>value pairs
             */
            foreach ($newValues as $k => $v) {
                if (!in_array($k, ['id', 'created_at', 'updated_at', 'deleted_at'])) {
                    if ($v !== $oldValues[$k]) {

                        $oldValue = $oldValues[$k];
                        $newValue = $newValues[$k];

                        switch($k) {
                            case 'industry_id':
                                $oldValue = CompanyIndustry::find($oldValue)->name;
                                $newValue = CompanyIndustry::find($newValue)->name;
                                break;
                        }
                        ChangeTracker::create([
                            'target' => $newValues['id'],
                            'author' => $user_id,
                            'company_id' => $newValues['company_id'],
                            'model' => 'CompanyProfile',
                            'attribute' => $k,
                            'action' => 'created',
                            'old_value' => $oldValue,
                            'new_value' => $newValue,
                            'notes' => ''
                        ]);
                    }
                }
            }
        });

        /**
         * When updating, get original values, then turn both new
         * and original values to arrays. Loop through keys, then
         * compare new and original values, and create a ChangeTracker
         * for every change.
         *
         * Attach company_id to all changes.
         *
         * Ignoring timestamps and primary keys.
         */
        static::updated(function (CompanyProfile $model) {
            $newValues = $model->toArray();
            $oldValues = $model->getOriginal(); // this returns an array

            $user_id = auth()->user() ? auth()->user()->id : null;

            /**
             * Loop on key=>value pairs
             */
            foreach ($newValues as $k => $v) {
                if (!in_array($k, ['id', 'created_at', 'updated_at', 'deleted_at'])) {
                    if ($v !== $oldValues[$k]) {
                        ChangeTracker::create([
                            'target' => $newValues['id'],
                            'author' => $user_id,
                            'company_id' => $newValues['company_id'],
                            'model' => 'CompanyProfile',
                            'attribute' => $k,
                            'action' => 'updated',
                            'old_value' => $oldValues[$k],
                            'new_value' => $newValues[$k],
                            'notes' => ''
                        ]);
                    }
                }
            }
        });

        /**
         * When deleting there is no "new" values.
         */
        static::deleted(function (CompanyAdministration $model) {
            $user_id = auth()->user() ? auth()->user()->id : null;
            ChangeTracker::create([
                'target' => $model->id,
                'author' => $user_id,
                'company_id' => $model->company_id,
                'model' => 'CompanyProfile',
                'attribute' => 'Model',
                'action' => 'deleted',
                'old_value' => $model->id,
                'new_value' => null,
                'notes' => ''
            ]);
        });
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function state_province()
    {
        return $this->belongsTo('App\StateProvince');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function timezone()
    {
        return $this->belongsTo('App\TimeZone');
    }

    public function industry()
    {
        return $this->belongsTo('App\CompanyIndustry');
    }
}
