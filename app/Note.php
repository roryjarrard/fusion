<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use SoftDeletes;

    protected $fillable = ['category_id', 'user_id', 'company_id', 'driver_id', 'vehicle_profile_id', 'title', 'content'];

    public function category()
    {
        return $this->belongsTo('App\NoteCategory');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function driver()
    {
        return DriverProfile::where('user_id', $this->driver_id)->first();
    }
}
