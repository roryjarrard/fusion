<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverRegistrationCompletion extends Model
{

    protected $fillable = [
        'user_id',
        'information_completed',
        'address_completed',
        'vehicle_completed',
        'insurance_completed',
        'license_completed',
        'banking_completed'
    ];

    public function streetLight() {
        return $this->belongsTo('App\DriverProfile');
    }
}
