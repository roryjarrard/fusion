<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],

        // driverProfile events
        'driverProfile.created' => [
            'App\Handlers\Events\DriverProfileEvents@driverProfileCreated',
        ],
        'driverProfile.updated' => [
            'App\Handlers\Events\DriverProfileEvents@driverProfileUpdated',
        ],
        'driverProfile.deleted' => [
            'App\Handlers\Events\DriverProfileEvents@driverProfileDeleted',
        ],

        // personalVehicle events
        'personalVehicle.created' => [
            'App\Handlers\Events\PersonalVehicleEvents@personalVehicleCreated',
        ],
        'personalVehicle.updated' => [
            'App\Handlers\Events\PersonalVehicleEvents@personalVehicleUpdated',
        ],
        'personalVehicle.deleted' => [
            'App\Handlers\Events\PersonalVehicleEvents@personalVehicleDeleted',
        ],

        // vehicleProfile events
        'vehicleProfile.created' => [
            'App\Handlers\Events\VehicleProfileEvents@vehicleProfileCreated',
        ],
        'vehicleProfile.updated' => [
            'App\Handlers\Events\VehicleProfileEvents@vehicleProfileUpdated',
        ],
        'vehicleProfile.deleted' => [
            'App\Handlers\Events\VehicleProfileEvents@vehicleProfileDeleted',
        ],

        // address events
        'address.creating' => [
            'App\Handlers\Events\AddressEvents@addressCreating',    // handling unknown zip/postal code
        ],

        /**
         * login events
         */
        'Illuminate\Auth\Events\Attempting' => [
            'App\Handlers\Events\LoginEvents@loginAttempting',
        ],

        'Illuminate\Auth\Events\Failed' => [
            'App\Handlers\Events\LoginEvents@loginFailed',
        ],

        'Illuminate\Auth\Events\Login' => [
            'App\Handlers\Events\LoginEvents@loginSuccess',
        ],

        'Illuminate\Auth\Events\Logout' => [
            'App\Handlers\Events\LoginEvents@logout',
        ],

        'Illuminate\Auth\Events\Lockout' => [
            'App\Handlers\Events\LoginEvents@lockout',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
