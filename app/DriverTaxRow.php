<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverTaxRow extends Model
{
    protected $fillable = ['name', 'type', 'order'];
}
