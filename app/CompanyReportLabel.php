<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyReportLabel extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'key', 'company_id', 'value'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
