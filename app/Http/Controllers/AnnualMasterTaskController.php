<?php

namespace App\Http\Controllers;

use App\Calculators\StreetLightCalculator;
use App\DriverProfile;
use App\DriverInsuranceReview;
use App\ZipCode;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

use App\Mail\Below5000MilesEmail;
use App\Mail\FAVRAnnualDeclarationEmail;
use App\Mail\FAVRAnnualDeclarationReminderEmail;
use App\Mail\FAVRVehicleAgeReminderEmail;
use App\Mail\FAVRVehicleAgeNoticeEmail;
use App\Mail\VehicleAgeReminderEmail;
use App\Mail\VehicleAgeNoticeEmail;

use Illuminate\Support\Facades\Mail;
use App\Http\Traits\CommonTraits;   // to log scheduled task run
use App\Http\Traits\CompanyTraits;


class AnnualMasterTaskController extends Controller
{
    use CommonTraits;
    use CompanyTraits;
    private $street_light;

    public function __construct()
    {
        $this->middleware('auth');
        $this->street_light = new StreetLightCalculator();
    }

    public function stub($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 79,81,120
     *
     * @param $task_id
     * @param $task_name
     */
    public function favr5000LimitEmailWarning($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/below_5000_miles.log', 0, Logger::INFO);
        $logger->pushHandler($handler);

        try {

            $long_description = '';
            $emails_sent = 0;
            $limit_miles = 5000;
            $year = date("Y");

            $company_query = <<<EOQ
SELECT c.id, c.name, m.new_driver_mileage_grace_days
FROM companies c
JOIN company_mileages m ON c.id = m.company_id
WHERE co.service_plan = 'FAVR'
ORDER BY c.name
EOQ;

            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$company->name</b> ";

                $coordinator = $this->getCoordinatorData($company);
                $from = $coordinator->email;

                // get driver entered program this year before November
                $driver_query = <<<EOQ
SELECT u.last_name, u.first_name, u.email, SUM(mm.business_mileage) AS business_mileage, COUNT(mr.month) AS months
  FROM users u
  JOIN driver_profiles dp ON u.id = dp.user_id AND dp.company_id = $company->id AND dp.deleted_at IS NULL
  JOIN monthly_mileages mm ON u.id = mm.user_id
  JOIN monthly_reimbursements mr ON mm.user_id = mr.user_id AND mm.year = mr.year AND mm.month = mr.month AND mr.variable_reimbursement > 0 
  WHERE  mm.year = {$year} AND dp.start_date <= '{$year}-11-01' AND (dp.street_light_id & 1) = 0   
  AND ( $year != year(dp.start_date ) OR  MONTH(dp.start_date) <= mr.month )
  GROUP BY 1,2, 3
  HAVING SUM(mm.business_mileage) < ( $limit_miles/12 * COUNT(mr.month) )
  ORDER BY 1,2
EOQ;
                $drivers = \DB::select(\DB::raw($driver_query));
                foreach ($drivers as $driver) {

                    $data = (object)[
                        'from' => $from,
                        'business_mileage' => number_format($driver->business_mileage),
                        'name' => $driver->first_name,
                        'months' => $driver->months == 1 ? '1 month' : "$driver->months months",
                        'mileage_required' => number_format(5000 / 12 * $driver->months),
                        'last_month' => date("F Y", strtotime("last month")),
                        'coordinator' => $coordinator
                    ];
                    $logger->addInfo($task_name, ['data' => $data]);

                    Mail::to($driver->email)->send(new Below5000MilesEmail($data));

                    $long_description .= " $driver->last_name $driver->first_name,";
                    $emails_sent++;
                    sleep(2); // TODO remove sleep(2) as we are testing with mailtrap

                }
            }

            $short_description = "Emails were sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    /**
     * master_tasks id: 95
     *
     * @param $task_id
     * @param $task_name
     */
    public function populatingMonthlyMileage($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 100
     *
     * @param $task_id
     * @param $task_name
     */
    public function favrAnnualRequirementReminder($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 102
     *
     * @param $task_id
     * @param $task_name
     */
    public function favrAnnualDeclaration($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/favr_annual_declaration.log', 0, Logger::INFO);
        $logger->pushHandler($handler);

        $long_description = '';
        $short_description = '';
        $emails_sent = 0;

        try {

            $red_terminated = RED_TERMINATED;
            $new_driver = YELLOW_NEW_DRIVER;
            $year = date("Y");
            $beginning_of_year = $year . '-01-01';

            $company_query = <<<EOQ
            SELECT c.id, c.name, cp.country_id, ci.remove_reimbursement_without_insurance, ci.insurance_review_interval
            FROM companies c
            JOIN company_options co ON co.company_id = c.id AND co.service_plan = 'FAVR' 
            JOIN company_profiles cp ON cp.company_id = c.id AND cp.deleted_at IS NULL
            JOIN company_insurances ci ON co.company_id = c.id AND ci.deleted_at IS NULL
            WHERE c.deleted_at IS NULL
            ORDER BY c.name
EOQ;
            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$company->name</b> ";

                $coordinator = $this->getCoordinatorData($company->id);
                $from = $coordinator->email;
                $insurance_amounts = $this->getCompanyInsuranceAmount($company->id, $company->country_id);
                $business_use_required = $this->getCompanyBusinessUse($company->id);


                $driver_query = <<<EOQ
                SELECT u.id, u.last_name, u.first_name, u.email
                FROM users u 
                JOIN driver_profiles dp ON dp.user_id = u.id AND dp.deleted_at IS NULL
                WHERE dp.start_date < '$beginning_of_year'
                AND dp.street_light_id & $new_driver = 0
                AND dp.street_light_id & $red_terminated = 0
                ORDER BY u.last_name, u.first_name
                LIMIT 3
EOQ;
                $drivers = \DB::select(\DB::raw($driver_query));
                foreach ($drivers as $driver) {

                    $data = (object)[
                        'from' => $from,
                        'company' => $company,
                        'driver' => $driver,
                        'year' => $year,
                        'insurance_details' => $insurance_amounts,
                        'business_use_required' => $business_use_required,
                        'coordinator' => $coordinator
                    ];

                    if ($emails_sent < 6) {
                        Mail::to($driver->email)->send(new FAVRAnnualDeclarationEmail($data));
                    }
                    $emails_sent++;
                    sleep(2); // TODO remove sleep(2)

                }

            }
            $this->scheduledTaskEnded($task_id, $log_id, 'TODO Give Message', '');

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }

    }

    # TODO: DUPLICATE? What is this?
    public function FAVRAnnualDeclarationReminder($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/favr_annual_declaration_reminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);

        try {

            $long_description = '';
            $emails_sent = 0;
            $red_terminated = RED_TERMINATED;
            $circle = ['Your Name', 'Make, Model and Year of Vehicle', 'Policy term dates - these must be current, please do not send in expired policy documentation'];

            $year = date("Y");
            // in case when we run it in December
            if (date("n") == 12) {
                $year += 1;
            }

            $company_query = <<<EOQ
SELECT c.id, c.name, m.new_driver_mileage_grace_days, cp.country_id, ci.insurance_review_interval, ci.remove_reimbursement_without_insurance
FROM companies c
JOIN company_insurances ci ON ci.company_id = c.id AND ci.deleted_at IS NULL
JOIN company_mileages m ON c.id = m.company_id AND m.deleted_at IS NULL
JOIN company_profiles AS cp ON cp.company_id = c.id AND cp.deleted_at IS NULL
JOIN company_options co ON co.company_id = c.id AND co.deleted_at IS NULL
WHERE co.service_plan = 'FAVR'
ORDER BY c.name
EOQ;
            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$company->name</b> ";

                $logger->addInfo('company', [$company->name]);

                $coordinator = $this->getCoordinatorData($company->id);
                $from = $coordinator->email;
                $insurance_details = $this->getCompanyInsuranceAmount($company->id, $company->country_id);
                $business_use_required = $this->getCompanyBusinessUse($company->id);

                $driver_query = <<<EOQ
SELECT u.id, u.last_name, u.first_name, u.email, d.street_light_id
FROM users u 
JOIN driver_profiles d ON d.user_id = u.id AND d.deleted_at IS NULL 
  AND (( d.street_light_id & 1) = 0 && ( d.street_light_id & $red_terminated) = 0) AND d.deleted_at IS NULL
WHERE d.company_id = $company->id
ORDER BY 2,3
EOQ;
                $drivers = \DB::select(\DB::raw($driver_query));
                foreach ($drivers as $driver) {

                    $logger->addInfo('driver', [$driver->last_name . ' ' . $driver->first_name]);

                    $no_annual_declaration = $driver->street_light_id & GREEN_NO_ANNUAL_DECLARATION ? true : false;
                    $no_insurance = $driver->street_light_id & GREEN_NO_INSURANCE_COMPLIANCE ||
                    $driver->street_light_id & RED_AWAITING_INSURANCE ||
                    $driver->street_light_id & RED_INSURANCE_EXPIRED ||
                    $driver->street_light_id & RED_INSUFFICIENT_INSURANCE ? true : false;

                    if ($no_insurance || $company->insurance_review_interval == 'On_Renewal' && $no_annual_declaration) {
                        $data = (object)[
                            'from' => $from,
                            'name' => $driver->first_name,
                            'year' => $year,
                            'no_annual_declaration' => $no_annual_declaration,
                            'no_insurance' => $no_insurance,
                            'company' => $company,
                            'insurance_details' => $insurance_details,
                            'circle' => $circle,
                            'business_use_required' => $business_use_required,
                            'coordinator' => $coordinator
                        ];
                        // $logger->addInfo($task_name, ['data' => $data]);


                        Mail::to($driver->email)->send(new FAVRAnnualDeclarationReminderEmail($data));

                        $long_description .= " $driver->last_name $driver->first_name,";
                        $emails_sent++;
                        sleep(2);
                    }

                }   // drivers
            }   // companies

            $short_description = "Emails were sent to $emails_sent drivers";
            $this->scheduledTaskEnded($log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    /**
     * master_tasks id: 111
     *
     * @param $task_id
     * @param $task_name
     */
    public function favrVehicleAgeReminder($task_id, $task_name)
    {

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/favr_vehicle_age_reminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);

        $year = date("Y") + 1; // we are sending it in October if theit vehicle is too old next year
        $year = date("Y"); // TODO remove this line - testing only
        $short_description = '';
        $long_description = '';
        $emails_sent = 0;
        $favr_plan = PLAN_FAVR;
        $no_vehicle_age_complaince = GREEN_NO_VEHICLE_AGE_COMPLIANCE;
        $red_vehicle_too_old = RED_VEHICLE_TOO_OLD;
        $red_terminated = RED_TERMINATED;

        $company_query = <<<EOQ
SELECT c.id, c.name 
FROM companies c
JOIN company_options co ON co.company_id = c.id AND co.service_plan = '$favr_plan'
WHERE c.deleted_at IS NULL 
ORDER BY c.name 
EOQ;

        $companies = \DB::select(\DB::raw($company_query));
        foreach ($companies as $company) {

            if (!empty($long_description)) {
                $long_description .= "<br>";
            }
            $long_description .= "<b>$company->name</b> ";
            $logger->addInfo('company', [$company->name]);

            $coordinator = $this->getCoordinatorData($company->id, 'vehicle_responsibility');
            $from = $coordinator->email;

            $driver_query = <<<EOQ
SELECT u.id, u.last_name, u.first_name, u.email, vp.max_vehicle_age, pv.year AS vehicle_year FROM users u 
JOIN driver_profiles dp ON u.id = dp.user_id AND dp.deleted_at IS NULL AND dp.company_id = $company->id
    AND dp.street_light_id & $no_vehicle_age_complaince = 0 AND dp.street_light_id & $red_terminated = 0
JOIN vehicle_profiles vp ON dp.vehicle_profile_id = vp.id
JOIN personal_vehicles pv ON pv.user_id = u.id AND pv.deleted_at IS NULL AND ( $year - pv.year ) > vp.max_vehicle_age
ORDER BY u.last_name, u.first_name
EOQ;

            $drivers = \DB::select(\DB::raw($driver_query));
            foreach ($drivers as $driver) {

                $data = (object)[
                    'from' => $from,
                    'first_name' => $driver->first_name,
                    'year' => $year,
                    'vehicle_year' => $driver->vehicle_year,
                    'max_vehicle_age' => $driver->max_vehicle_age,
                    'company_name' => $company->name,
                    'coordinator' => $coordinator
                ];

                @Mail::to($driver->email)->send(new FAVRVehicleAgeReminderEmail($data));
                sleep(2);
                $emails_sent++;
                $long_description .= "$driver->last_name $driver->first_name, ";
                // TODO remove sleep(2);

            }
        }

        try {

            $short_description = "Emails were sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    /**
     * master_tasks id: 98
     *
     * @param $task_id
     * @param $task_name
     */
    public function favrAnnualRequirementStatus($task_id, $task_name)
    {

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/favr_vehicle_age_notice.log', 0, Logger::INFO);
        $logger->pushHandler($handler);

        $year = date("Y");
        $beginning_of_year = date("Y-01-01");
        $last_year = $year - 1;
        $grace_period_ends = "$year-01-30";
        $short_description = '';
        $long_description = '';
        $favr_plan = PLAN_FAVR;
        $updated_total = 0;

        try {

            // TODO add condition to check if reimbursement has been run on January 1st
            /*
            $query             = "SELECT end_date, status FROM AP_Scheduled_Task where task_id = 94
            and start_date >= '$year-01-01' and start_date < '$year-01-02' order by end_date desc";
            $res               = $mysqli->query($query);
            if (!$res) {
                $long_desc .= "Cannot execute $query: " . $mysqli->error;
                $short_desc = "SQL problem";
                record_task_ended($log_id, $short_desc, $long_desc, 'Failed');
                exit(100);
            }
            */

            $company_query = <<<EOQ
SELECT c.id, c.name, ci.remove_reimbursement_without_insurance, ci.insurance_review_interval
FROM companies c
JOIN company_insurances ci ON ci.company_id = c.id AND ci.deleted_at IS NULL
JOIN company_options co ON co.company_id = c.id AND co.service_plan = '$favr_plan' AND co.deleted_at IS NULL
WHERE c.deleted_at IS NULL 
ORDER BY c.name
EOQ;

            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $company) {

                $updated = 0;
                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$company->name</b> ";
                $logger->addInfo('company', [$company->name]);

                $drivers = DriverProfile::where('start_date', '<', $beginning_of_year)->where('company_id', $company->id);
                foreach ($drivers as $driver_profile) {

                    $new_street_light_id = $this->street_light->addToStreetLight($driver_profile->street_light_id, GREEN_NO_ANNUAL_DECLARATION);

                    if ($company->remove_reimbursement_without_insurance) {
                        $add_status = RED_AWAITING_INSURANCE;
                        $remove_status = GREEN_NO_INSURANCE_COMPLIANCE;
                    } else {
                        $add_status = GREEN_NO_INSURANCE_COMPLIANCE;
                        $remove_status = RED_AWAITING_INSURANCE;
                    }
                    $new_street_light_id = $this->street_light->removeFromStreetLight($new_street_light_id, $remove_status);

                    if ($new_street_light_id & $add_status) {
                        $updated++;
                        $updated_total++;

                        $new_driver_profile = $driver_profile->replicate();
                        $driver_profile->delete();
                        $new_street_light = $new_street_light_id;
                        $new_driver_profile->street_light_id = $new_street_light;
                        $new_driver_profile->save();
                        continue;   // no changes required for insurance
                    }

                    $new_street_light_id = $this->street_light->addToStreetLight($new_street_light_id, $add_status);

                    if ($company->insurance_review_interval == 'Annualy') {
                        $new_driver_profile = $driver_profile->replicate();
                        $driver_profile->delete();
                        $new_driver_profile->street_light_id = $new_street_light_id;
                        $new_driver_profile->save();

                        // TODO Talk to Rory how to do it? Update or Insert New one?
                        // $insurance_review = DriverInsuranceReview::updated()
                        $updated++;
                        $updated_total++;

                    }
                }

            }


            $logger->addInfo('Completed', ['short' => $short_description, 'description' => $long_description]);
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, '', 'Completed');

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }

    }

    /**
     * master_tasks id: 97
     *
     * @param $task_id
     * @param $task_name
     */
    public function favrVehicleAgeNotice($task_id, $task_name)
    {

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/favr_vehicle_age_notice.log', 0, Logger::INFO);
        $logger->pushHandler($handler);

        $year = date("Y") + 1;
        $short_description = '';
        $long_description = '';
        $emails_sent = 0;
        $favr_plan = PLAN_FAVR;
        $green_vehicle_age_compliance = GREEN_NO_VEHICLE_AGE_COMPLIANCE;
        $red_terminated = RED_TERMINATED;

        try {
            $company_query = <<<EOQ
    SELECT c.id, c.name, cv.vehicle_responsibility 
    FROM companies c
    JOIN company_options co ON c.id = co.company_id AND co.service_plan = '$favr_plan' and co.deleted_at IS NULL
    JOIN company_vehicles cv on c.id = cv.company_id AND cv.enforce_vehicle_age = 1 
    WHERE cv.vehicle_responsibility IS NOT NULL AND c.deleted_at IS NULL
    ORDER BY c.name
EOQ;
            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$company->name</b> ";
                $logger->addInfo('company', [$company->name]);

                $coordinator = $this->getCoordinatorData($company->id, 'vehicle_responsibility');
                $from = $coordinator->email;

                $driver_query = <<<EOQ
    SELECT u.id, u.last_name, u.first_name, u.email, vp.max_vehicle_age, dp.street_light_id, pv.year as vehicle_year 
    FROM users u 
    JOIN driver_profiles dp ON u.id = dp.user_id AND dp.deleted_at IS NULL AND dp.company_id = $company->id
        AND dp.street_light_id & $green_vehicle_age_compliance = 0 AND dp.street_light_id & $red_terminated = 0
    JOIN vehicle_profiles vp ON dp.vehicle_profile_id = vp.id
    JOIN personal_vehicles pv ON pv.user_id = u.id AND pv.deleted_at IS NULL AND ( $year - pv.year ) > vp.max_vehicle_age
    ORDER BY u.last_name, u.first_name
EOQ;

                $drivers = \DB::select(\DB::raw($driver_query));
                foreach ($drivers as $driver) {

                    $data = (object)[
                        'from' => $from,
                        'first_name' => $driver->first_name,
                        'vehicle_year' => $driver->vehicle_year,
                        'year' => $year,
                        'max_vehicle_age' => $year - $driver->max_vehicle_age,
                        'company_id' => $company->id,
                        'company_name' => $company->name,
                        'coordinator' => $coordinator
                    ];

                    @Mail::to($driver->email)->send(new FAVRVehicleAgeNoticeEmail($data));
                    $emails_sent++;
                    $logger->addInfo('driver', ['last_name' => $driver->last_name, 'first_name' => $driver->first_name, 'sent' => $emails_sent]);

                    sleep(2);
                    $long_description .= "{$driver->last_name} {$driver->first_name}, ";
                    // TODO remove sleep(2);

                    $driver_profile = DriverProfile::where('user_id', $driver->id)->first();
                    $new_driver_profile = $driver_profile->replicate();
                    $driver_profile->delete();
                    $new_street_light = $this->addToStreetLight($driver->street_light_id, GREEN_NO_VEHICLE_AGE_COMPLIANCE);
                    $new_driver_profile->street_light_id = $new_street_light;
                    $new_driver_profile->save();

                }   //  drivers

            }   //  companies

            $short_description = "Emails were sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }

    }

    /**
     * master_tasks id: 107
     *
     * @param $task_id
     * @param $task_name
     */
    public function vehicleAgeReminder($task_id, $task_name)
    {

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/vehicle_age_reminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);

        $year = date("Y") + 1; // we are sending it in October if theit vehicle is too old next year
        $short_description = '';
        $long_description = '';
        $emails_sent = 0;
        $favr_plan = PLAN_FAVR;
        $red_vehicle_too_old = RED_VEHICLE_TOO_OLD;
        $red_terminated = RED_TERMINATED;

        try {
            $company_query = <<<EOQ
SELECT c.id, c.name, cv.vehicle_responsibility 
FROM companies c
JOIN company_options co ON c.id = co.company_id AND  co.service_plan != '$favr_plan'
JOIN company_vehicles cv on c.id = cv.company_id AND cv.enforce_vehicle_age = 1  
WHERE cv.vehicle_responsibility IS NOT NULL
ORDER BY c.name
EOQ;
            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$company->name</b> ";
                $logger->addInfo('company', [$company->name]);

                $coordinator = $this->getCoordinatorData($company->id, 'vehicle_responsibility');
                $from = $coordinator->email;

                $driver_query = <<<EOQ
SELECT u.id, u.last_name, u.first_name, u.email, vp.max_vehicle_age FROM users u 
JOIN driver_profiles dp ON u.id = dp.user_id AND dp.deleted_at IS NULL AND dp.company_id = $company->id
    AND dp.street_light_id & $red_vehicle_too_old = 0 AND dp.street_light_id & $red_terminated = 0
JOIN vehicle_profiles vp ON dp.vehicle_profile_id = vp.id
JOIN personal_vehicles pv ON pv.user_id = u.id AND pv.deleted_at IS NULL AND ( $year - pv.year ) > vp.max_vehicle_age
ORDER BY u.last_name, u.first_name
EOQ;

                $drivers = \DB::select(\DB::raw($driver_query));
                foreach ($drivers as $driver) {

                    $data = (object)[
                        'from' => $from,
                        'first_name' => $driver->first_name,
                        'year' => $year,
                        'company_name' => $company->name,
                        'coordinator' => $coordinator
                    ];

                    @Mail::to($driver->email)->send(new VehicleAgeReminderEmail($data));
                    sleep(2);
                    $emails_sent++;
                    $long_description .= "$driver->last_name $driver->first_name, ";
                    // TODO remove sleep(2);

                }   //  drivers

            }   //  companies

            $short_description = "Emails were sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    /**
     * master_tasks id: 108
     *
     * @param $task_id
     * @param $task_name
     */
    public function vehicleAgeNotice($task_id, $task_name)
    {

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/vehicle_age_notice.log', 0, Logger::INFO);
        $logger->pushHandler($handler);

        $year = date("Y") + 1;
        $short_description = '';
        $long_description = '';
        $emails_sent = 0;
        $favr_plan = PLAN_FAVR;
        $red_vehicle_too_old = RED_VEHICLE_TOO_OLD;
        $red_terminated = RED_TERMINATED;

        try {
            $company_query = <<<EOQ
    SELECT c.id, c.name, cv.vehicle_responsibility 
    FROM companies c
    JOIN company_options co ON c.id = co.company_id AND co.service_plan != '$favr_plan'
    JOIN company_vehicles cv ON c.id = cv.company_id AND cv.enforce_vehicle_age = 1  
    WHERE cv.vehicle_responsibility IS NOT NULL
    ORDER BY c.name
EOQ;
            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$company->name</b> ";
                $logger->addInfo('company', [$company->name]);

                $coordinator = $this->getCoordinatorData($company->id, 'vehicle_responsibility');
                $from = $coordinator->email;

                $driver_query = <<<EOQ
    SELECT u.id, u.last_name, u.first_name, u.email, vp.max_vehicle_age, dp.street_light_id FROM users u 
    JOIN driver_profiles dp ON u.id = dp.user_id AND dp.deleted_at IS NULL AND dp.company_id = $company->id
        AND dp.street_light_id & $red_vehicle_too_old = 0 AND dp.street_light_id & $red_terminated = 0
    JOIN vehicle_profiles vp ON dp.vehicle_profile_id = vp.id
    JOIN personal_vehicles pv ON pv.user_id = u.id AND pv.deleted_at IS NULL AND ( $year - pv.year ) > vp.max_vehicle_age
    ORDER BY u.last_name, u.first_name
EOQ;

                $drivers = \DB::select(\DB::raw($driver_query));
                foreach ($drivers as $driver) {

                    $data = (object)[
                        'from' => $from,
                        'first_name' => $driver->first_name,
                        'max_year' => $year - $driver->max_vehicle_age,
                        'company_name' => $company->name,
                        'coordinator' => $coordinator
                    ];

                    @Mail::to($driver->email)->send(new VehicleAgeNoticeEmail($data));
                    $emails_sent++;
                    $logger->addInfo('driver', ['last_name' => $driver->last_name, 'first_name' => $driver->first_name, 'sent' => $emails_sent]);

                    sleep(2);
                    $long_description .= "$driver->last_name $driver->first_name, ";
                    // TODO remove sleep(2);

                    $driver_profile = DriverProfile::where('user_id', $driver->id)->first();
                    $new_driver_profile = $driver_profile->replicate();
                    $driver_profile->delete();
                    $new_street_light = $this->addToStreetLight($driver->street_light_id, RED_VEHICLE_TOO_OLD);
                    $new_driver_profile->street_light_id = $new_street_light;
                    $new_driver_profile->save();

                }   //  drivers

            }   //  companies

            $short_description = "Emails were sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    /**
     * master_tasks id: 101
     *
     * @param $task_id
     * @param $task_name
     */
    public function wmcaAnnualInsuranceReminder($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 121
     *
     * @param $task_id
     * @param $task_name
     */
    public function wmcaAnnualInsuranceStatus($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 122
     *
     * @param $task_id
     * @param $task_name
     */
    public function wmcaAnnualDeclarationReminder($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 123
     *
     * @param $task_id
     * @param $task_name
     */
    public function favrWmVehicleAgeNotice($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }
}