<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\CompanyReportLabel;

/**
 * This controller is used in the design pattern for generating reports
 * which involves the server managing the session data with 1 large call,
 * as opposed to many small calls.
 *
 * It is implemented on the Fixed and Variable report, see:
 *  FixedVariable.vue
 *  ReportSession.vue
 *
 * Class ReportController
 * @package App\Http\Controllers
 */
class ReportController extends Controller
{
    /**
     * The following functions are used to generate correct session data for reports
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getReportSessionData(Request $request)
    {
        /**
         * Default generate to false
         * if all is set we will fire an event on the front end to generate the report
         */
        $generate = false;

        $view = $request->view;
        if (empty($view) || $view == -1) {
            return response('Invalid request', 400);
        }

        $selectList = [];
        $selects = null;
        $options = null;
        switch ($view) {
            case 'fixedVariable':
                /**
                 * NOTE!
                 * Here call the selects by the name of the value on which they do the v-model!
                 */
                $selects = ['company_id', 'division_id', 'year', 'month'];
                $otherSessionValues = [
                    'country_id' => null,
                    'company_name' => null,
                ];
                break;
        }

        /**
         * construct the husk of our returnable object
         * it is a list of selects keyed by type containing
         * a parameter (the value on the front end we v-model on)
         * a default value from session
         * a list of options
         *
         * We also construct a presentValues dictionary of the following format
         *  [ some_variable => [session_value => x, request_value => y, 'value_to_use' => null],
         *      ...
         *  ]
         */
        $presentValues = [];
        foreach ($selects as $key) {
            $presentValues[$key] = [
                'session_value' => session()->get('session_' . $key),
                'request_value' => $request->{$key},
                'value_to_use' => null, // we will determine this later
            ];

            $selectList[$key] = [
                'parameter' => $key,
                'value' => null, // we will determine this later
                'options' => null, // we will determine this later also
            ];
        }

        $this->reconcileSessionAndSelectedValues($presentValues, $view, $generate);

        foreach ($selects as $key) {
            $selectList[$key]['value'] = $presentValues[$key]['value_to_use'];
            $selectList[$key]['options'] = $this->getSelectOptions($key, $view);
        }

        $this->getOtherSessionValues($presentValues, $otherSessionValues);

        return [
            'generate' => $generate,
            'selectList' => $selectList,
            'otherSessionValues' => $otherSessionValues
        ];
    }


    public function getSelectOptions($parameter, $view)
    {
        $options = null;
        $query = null;
        switch ($view) {
            /**
             * These are the selects for reimbursement reports
             * options so far are company_id, division_id, year, month
             */
            case 'fixedVariable':
                /**
                 * If the parameter is company_id on the fixed variable report, we need
                 * companies who have reimbursement data
                 */
                if ($parameter == 'company_id') {
                    $query = <<<EOQ
select c.name as name, c.id as value
from companies c
join monthly_reimbursements mr on c.id = mr.company_id
group by c.id, c.name
order by c.name asc
EOQ;
                } else if ($parameter == 'division_id') {
                    $session_company_id = session()->get('session_company_id');
                    $query = <<<EOQ
select d.name as name, d.id as value
from divisions d
where company_id = {$session_company_id}
order by name desc
EOQ;
                } else if ($parameter == 'year') {
                    $session_company_id = session()->get('session_company_id');
                    $query = <<<EOQ
select year as name, year as value 
from monthly_reimbursements
where company_id = {$session_company_id}
group by name, value
order by name desc
EOQ;
                } else if ($parameter == 'month') {
                    $session_company_id = session()->get('session_company_id');
                    $session_year = session()->get('session_year');
                    $query = <<<EOQ
select m.name as name, mr.month as value 
from monthly_reimbursements mr
join months m on m.id = mr.month
where company_id = {$session_company_id}
and year = {$session_year}
group by name, value
order by value asc
EOQ;
                }


                break;
        }


        if (!isset($query)) {
            abort(400, 'Failed to get select options');
        }

        return \DB::select(\DB::raw($query));
    }


    public function reconcileSessionAndSelectedValues(&$presentValues, $view, &$generate)
    {

        switch ($view) {
            /**
             * These cases are for reimbursement reports that have 4 selects
             * namely: company, division, year and month
             * Selected values are validated against reimbursement
             */
            case 'fixedVariable':

                /**
                 * If the session company is not set, go to all -1
                 *
                 * If the company requested does not match the session company,
                 * update division, year and month to defaults
                 */
                if ($presentValues['company_id']['request_value'] == -1) {
                    $presentValues['company_id']['value_to_use'] = -1;
                    session()->put('session_company_id', -1);
                    $presentValues['division_id']['value_to_use'] = -1;
                    session()->put('session_division_id', -1);
                    $presentValues['year']['value_to_use'] = -1;
                    session()->put('session_year', -1);
                    $presentValues['month']['value_to_use'] = -1;
                    session()->put('session_month', -1);
                    $generate = false;
                } else if ($presentValues['company_id']['session_value'] != $presentValues['company_id']['request_value']) {
                    $presentValues['company_id']['value_to_use'] = $presentValues['company_id']['request_value'];
                    session()->put('session_company_id', $presentValues['company_id']['value_to_use']);
                    $presentValues['division_id']['value_to_use'] = -1;
                    session()->put('session_division_id', -1);
                    $presentValues['year']['value_to_use'] = $this->getMostRecentReimbursementYearFromCompanyId($presentValues['company_id']['value_to_use']);
                    session()->put('session_year', $presentValues['year']['value_to_use']);
                    $presentValues['month']['value_to_use'] = $this->getMostRecentReimbursementMonthFromCompanyId($presentValues['company_id']['value_to_use'], $presentValues['year']['value_to_use']);
                    session()->put('session_month', $presentValues['month']['value_to_use']);
                    $generate = true;
                } else {
                    // the company is the same, update all parameters to be the requested ones, valid or defaults
                    $presentValues['company_id']['value_to_use'] = $presentValues['company_id']['request_value'];
                    $presentValues['division_id']['value_to_use'] = $this->getValidatedDivisionId($presentValues['company_id']['value_to_use'], $presentValues['division_id']['request_value']);
                    session()->put('session_division_id', $presentValues['division_id']['value_to_use']);
                    $presentValues['year']['value_to_use'] = $this->getValidatedReimbursementYear($presentValues['company_id']['value_to_use'], $presentValues['year']['request_value']);
                    session()->put('session_year', $presentValues['year']['value_to_use']);
                    $presentValues['month']['value_to_use'] = $this->getValidatedReimbursementMonth($presentValues['company_id']['value_to_use'], $presentValues['year']['value_to_use'], $presentValues['month']['request_value']);
                    session()->put('session_month', $presentValues['month']['value_to_use']);
                    $generate = true;
                }
                break;
        }
    }

    public function getMostRecentReimbursementYearFromCompanyId($company_id)
    {
        return \DB::table('monthly_reimbursements')
            ->selectRaw('max(year) as year')
            ->where('company_id', $company_id)
            ->first()
            ->year;
    }

    public function getMostRecentReimbursementMonthFromCompanyId($company_id, $max_year = null)
    {
        return \DB::table('monthly_reimbursements')
            ->selectRaw('max(month) as month')
            ->where('company_id', $company_id)
            ->where('year', (isset($max_year) ? $max_year : $this->getMostRecentReimbursementYearFromCompanyId($company_id)))
            ->orderBy('month', 'desc')
            ->first()
            ->month;
    }

    public function getValidatedReimbursementYear($company_id, $year)
    {
        if (\DB::table('monthly_reimbursements')->where('company_id', $company_id)
                ->where('year', $year)
                ->limit(1)
                ->get()->count() > 0) {
            return $year;
        } else {
            return $this->getMostRecentReimbursementYearFromCompanyId($company_id);
        }
    }

    public function getValidatedReimbursementMonth($company_id, $year, $month)
    {
        if (\DB::table('monthly_reimbursements')->where('company_id', $company_id)
                ->where('year', $year)
                ->where('month', $month)
                ->limit(1)
                ->get()->count() > 0) {
            return $month;
        } else {
            return $this->getMostRecentReimbursementMonthFromCompanyId($company_id, $year);
        }

    }

    public function getValidatedDivisionId($company_id, $division_id)
    {
        return (\DB::table('divisions')
                ->where('company_id', $company_id)
                ->where('id', $division_id)
                ->get()
                ->count() > 0) ? $division_id : -1;
    }

    /**
     * Based on the selections, returns any other required session values
     * @param $presentValues
     * @param $otherSessionValues
     */
    public function getOtherSessionValues($presentValues, &$otherSessionValues)
    {
        if (array_key_exists('country_id', $otherSessionValues) && session()->get('session_company_id') > 0) {
            $country_id = Company::find($presentValues['company_id']['value_to_use'])->profile->country_id;
            $otherSessionValues['country_id'] = $country_id;
            session()->put('session_country_id', $country_id);
        } else {
            session()->put('session_country_id', -1);
        }

        if (array_key_exists('company_name', $otherSessionValues) && session()->get('session_company_id') > 0) {
            $company_name = Company::find($presentValues['company_id']['value_to_use'])->name;
            $otherSessionValues['company_name'] = $company_name;
            session()->put('session_company_name', $company_name);
        } else {
            session()->put('session_company_name', '');
        }

    }


    /**
     * Returns the label for the provided data_key, based on the provided company_id
     *
     * For example
     *      getReportLabel('division_id', 57 **pulte** ) returns 'Market' instead of 'Division'
     *
     * @param $data_key
     * @param $company_id
     * @return string
     */
    public function getReportLabel($key, $company_id)
    {
        $label = CompanyReportLabel::where('company_id', $company_id)
            ->where('key', $key)
            ->first()
            ->value;

        return !empty($label) ? $label : $key;
    }



}
