<?php

namespace App\Http\Controllers;

use App\Company;
use App\DailyMileage;
use App\DailyTrip;
use App\Login;
use App\Mail\ResetCodeSent;
use App\MirouteDriverOptions;
use App\OauthAccessToken;
use App\PasswordResetApi;
use App\SavedStop;
use App\StateProvince;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Passport\TokenRepository;
use Lcobucci\JWT\Parser as JwtParser;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use App\Http\Traits\CommonTraits;
use App\Calculators\StreetLightCalculator;


class ApiController extends AccessTokenController
{
    private $today;
    private $apiLog;
    private $streetLightCalculator;

    public function __construct(AuthorizationServer $server, TokenRepository $tokens, JwtParser $jwt)
    {
        // create a log for today
        $this->today = date('Y-m-d');

        CommonTraits::delete_older_than(storage_path('logs/api'), 3600 * 24 * 7);

        $this->apiLog = new Logger('api');
        $this->apiLog->pushHandler(new StreamHandler(storage_path('logs/api/api.' . $this->today . '.log')), Logger::INFO);

        $this->streetLightCalculator = new StreetLightCalculator();


        parent::__construct($server, $tokens, $jwt);
    }

    #region PRE-LOGIN

    /**
     * Test if api user has valid token in DB.
     * If you get to this auth route, you have a valid token!
     *
     * @return array
     */
    public function validateToken()
    {
        return ['message' => 'valid token'];
    }

    /**
     * Return client secret to avoid hard-coding on app.
     * MiRoute will always be client #2
     *
     * @return array
     */
    public function requestSecret()
    {
        $client_secret = \DB::table('oauth_clients')->where('id', 2)->first()->secret;
        return ['client_secret' => $client_secret];
    }

    #endregion

    #region LOGIN / LOGOUT

    /**
     * This is LOGIN
     * Override built-in token login/issue
     *
     * @param ServerRequestInterface $request
     * @return array
     */
    public function issueToken(ServerRequestInterface $request)
    {
        // use 'username' entry, but allow username or email
        $requestBody = $request->getParsedBody();
        $username = $requestBody['username'];
        $userTime = $requestBody['userTime'];

        # TODO: check to make sure secret is correct for client id

        $this->apiLog->info('Login Attempt', ['username' => $username]);
        try {
            // find user and check credentials
            $user = (new \App\User)->where('deleted_at', null)->where('username', $username)->orWhere('email', $username)->firstOrFail();

            // make sure this is a driver
            if (empty($user) || empty($user->driverProfile)) {
                $this->loginFailed($username, 'No user or not a driver');
                $this->apiLog->info('FAILED Login Attempt', ['username' => $username, 'reason' => 'No user or not a driver']);
                throw new OAuthServerException($user->driverProfile->company->mileage->mileage_entry_method, 6, 'invalid_user_type', 401);
            }

            // make sure driver is from Mi-Route company
            /*
            $this->apiLog->info('CHECK id Mi-Route or Test Driver', ['username' => $username, 'user_id' => $user->id]);
            if( $user->driverProfile->company->mileage->mileage_entry_method != 'Mi-Route') {

                // however, test drivers are allowed
                $testing_user = (new \App\MirouteDriverOption)
                    ->where('user_id',$user->id)
                    ->where('access','=',1)
                    ->where('updated_at','<', date("Y-m-d"))
                    ->first();
                $this->apiLog->info('After checking if test driver', ['username' => $username, 'user_id' =>$user->id,  'testing_user' => $testing_user]);
                if( empty($testing_user)) {
                    $this->loginFailed($username, 'Not a Mi-Route user');
                    $this->apiLog->info('FAILED Login Attempt', ['username' => $username, 'reason' => 'Not a Mi-Route driver']);
                    throw new OAuthServerException($user->driverProfile->company->mileage->mileage_entry_method, 6, 'not_miroute_driver', 401);
                }
                $this->apiLog->info('TESTING driver found', ['username' => $username]);
            }
            $this->apiLog->info('Passed Mi-Route user check', ['username' => $username, 'entry_method' => $user->driverProfile->company->mileage->mileage_entry_method]);

            // make sure driver is not excluded
            $excluded_user = (new \App\MirouteDriverOption)
                ->where('user_id',$user->id)
                ->where('access','=',0)
                ->where('updated_at','<', date("Y-m-d"))
                ->first();
            if( !empty($excluded_user)) {
                $this->apiLog->info('FAILED Login Attempt', ['username' => $username, 'reason' => 'Restricted from Mi-Route']);
                $this->loginFailed($username,'You are restricted from Mi-Route. Please use Fusion instead.');
                throw new OAuthServerException($user->driverProfile->company->mileage->mileage_entry_method, 6, 'excluded_driver', 401);
            }
            $this->apiLog->info('Passed user Exclusion check', ['username' => $username]);
            */
            // issue token
            $tokenResponse = parent::issueToken($request);

            // convert response
            $content = $tokenResponse->getContent();

            // convert json to array
            $data = json_decode($content, true);
            // $this->apiLog->info('Token Response', ['data' => $data]);

            if (isset($data['error'])) {
                $this->loginFailed($username, 'Invalid credentials');
                $this->apiLog->info('FAILED Login Attempt', ['username' => $username, 'reason' => 'Invalid credentials']);
                throw new OAuthServerException('The user credentials were incorrect.', 6, 'invalid_credentials', 401);
            }


            // get userInfo for use by app, add access token to user
            $this->apiLog->info("Gathering User Info", ['user_id'=>$user->id]);
            $responseObject = $this->gatherUserInfo($user->id, $userTime);
            $responseObject['access_token'] = $data['access_token'];

            $this->apiLog->info('SUCCESSFUL Login Attempt', [
                'username' => $username,
                'user_id' => $user->id,
                'userTime' => $userTime,
                'last_entry_date' => $responseObject['last_entry_date'],
                'last_starting_odometer' => $responseObject['last_starting_odometer'],
                'last_ending_odometer' => $responseObject['last_ending_odometer'],
                'miroute_page' => $responseObject['miroute_page'],
                'lastDrivingRecord' => $responseObject['lastDrivingRecord']]);

            $this->loginSuccess($username);

            return json_encode($responseObject);
        } catch (ModelNotFoundException $e) {
            // email or username not found
            $this->apiLog->info('FAILED Login Attempt', ['username' => $username, 'reason' => 'No user with this username or email', 'error' => $e->getMessage()]);
            return Response::create(['code' => 401, 'message' => $e->getMessage()], 401);
        } catch (OAuthServerException $e) {
            $this->apiLog->info('FAILED Login Attempt', ['username' => $username, 'reason' => 'OAuth Server Exception', 'error' => $e->getMessage()]);
            return Response::create(['code' => 401, 'message' => $e->getMessage()], 401);
        } catch (\Exception $e) {
            $this->apiLog->info('FAILED Login Attempt', ['username' => $username, 'reason' => 'General Error', 'error' => $e->getMessage()]);
            return Response::create(['code' => 401, 'message' => $e->getMessage()], 404);
        }
    }

    /**
     * Log api user out and remove token from DB
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        if (auth()->user()) {
            $user = auth()->user();
            //$user->api_token = null; // clear api token not used because of passport
            $user->accessToken()->delete();
            //$user->save();

            return response()->json([
                'message' => 'Thank you for using our application',
            ]);
        }

        return response()->json([
            'error' => 'Unable to logout user',
            'code' => 401,
        ], 401);
    }

    #endregion

    #region RESET PASSWORD

    public function validateEmailForResetCode(Request $request)
    {

        $validation_code = mt_rand(100000, 999999);

        $user = User::where('email', $request->email)->first();

        if (empty($user)) {
            return Response::create(['code' => 422, 'message' => 'User not found'], 422);
        }

        $date = new \DateTime;
        $expiration_date = $date->add(new \DateInterval('P1D'))->format('Y-m-d H:i:s');

        $reset = (new PasswordResetApi)->create([
            'user_id' => $user->id,
            'code' => $validation_code,
            'expires_at' => $expiration_date
        ]);

        Mail::to($user->email)->send(new ResetCodeSent($validation_code));
        return ['username' => $user->username, 'validation_code' => $validation_code];
    }

    public function validateResetCode(Request $request)
    {
        $today = date('Y-m-d H:i:s');
        PasswordResetApi::where('expires_at', '<=', $today)->delete();

        $user = User::where('email', $request->email)->first();

        if (empty($user)) {
            return Response::create(['code' => 401, 'message' => 'User not found'], 401);
        }
        $user_codes = PasswordResetApi::where('user_id', $user->id)->where('expires_at', '>', $today)->get();

        if (empty($user_codes) || $user_codes->count() == 0) {
            return Response::create(['code' => 401, 'message' => 'No verification codes for user found'], 401);
        }

        $matched = false;
        foreach ($user_codes as $ucode) {
            if ($ucode->code == $request->code) {
                $matched = true;
            }
        }

        if (!$matched) {
            return Response::create(['code' => 404, 'message' => 'Unable to verify code'], 404);
        } else {
            return ['message' => 'user code verified'];
        }
    }

    public function submitNewPassword(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (empty($user)) {
            return Response::create(['code' => 401, 'message' => 'User not found'], 401);
        }

        $user->password = \Hash::make($request->password);
        $user->save();

        PasswordResetApi::where('user_id', $user->id)->delete();

        return ['message' => 'User password updated', 'username' => $user->username];
    }

    #endregion

    #region FIND INFO FOR USER

    /**
     * Return User model data for api user.
     * 'get' is the external request.
     *
     * @param Request $request
     * @return mixed
     */
    public function getUserInfo(Request $request)
    {
        $userTime = empty($request->userTime) ? date('Y-m-d H:i:s') : $request->userTime;
        return $this->gatherUserInfo($request->user()->id, $userTime);
    }

    /**
     * Internal function to gather user information.
     *
     * @param $id
     * @param $userTime
     * @return array
     */
    private function gatherUserInfo($id, $userTime)
    {
        $this->apiLog->info('In gatherUserInfo', []);
        $user = User::find($id);
        $userDate = $this->getUserDate($userTime);

        # TODO: return id with coordinators (add to model in miroute)
        # TODO: move to separate function?
        $coordinators = $user->driverProfile->company->coordinators;

        # TODO : what data to include for coordinators
        $companyCoordinator = $coordinators->company_coordinator_id ? User::where('id', $coordinators->company_coordinator_id)->select('email', 'first_name', 'last_name')->first()->toArray() : null;
        if ($companyCoordinator)
            $companyCoordinator['phone'] = $coordinators->company_coordinator_phone;

        $cardataCoordinator = $coordinators->cardata_coordinator_id ? User::where('id', $coordinators->cardata_coordinator_id)->select('email', 'first_name', 'last_name')->first()->toArray() : null;
        if ($cardataCoordinator)
            $cardataCoordinator['phone'] = $coordinators->cardata_coordinator_phone;


        $mirouteCoordinator = $coordinators->miroute_coordinator_id ? User::where('id', $coordinators->miroute_coordinator_id)->select('email', 'first_name', 'last_name')->first()->toArray() : null;
        if ($mirouteCoordinator)
            $mirouteCoordinator['phone'] = $coordinators->miroute_coordinator_phone;

        // determine last driving state
        $lastDrivingRecord = $this->findLastDrivingDay($id, $userTime);
        //$this->apiLog->info('Got Last Driving Record', ['user_id'=>$user->id, 'last'=>$lastDrivingRecord]);
        $driving_state = $lastDrivingRecord['last_state'];
        //$this->apiLog->info('Got Last State', ['user_id'=>$user->id, 'last_state'=>$driving_state]);
        if ($lastDrivingRecord['ending_odometer'] > 0 && $driving_state != 'day_ended') {
            DailyMileage::find($lastDrivingRecord['id'])->update([
                'last_state' => 'day_ended'
            ]);
            $driving_state = 'day_ended';
        }
        $dailyTrips = !empty($lastDrivingRecord) ? DailyTrip::where('daily_mileage_id', $lastDrivingRecord['id'])->with('savedStop')->get() : [];
        //$this->apiLog->info('Got Daily Trips', ['user_id'=>$user->id, 'dailyTrips'=>$dailyTrips]);
        $trips = empty($dailyTrips) || $dailyTrips->count() == 0 ? [] : $this->convertDailyTrips($dailyTrips);
        //$this->apiLog->info('Got Trips', ['user_id'=>$user->id, 'trips'=>$trips]);

        $street_light_id = $user->driverProfile->street_light_id;
        $this->apiLog->info('Got Street Light Id', ['user_id'=>$user->id, 'street_light_id'=>$street_light_id]);
        $street_light_color = strtolower($this->streetLightCalculator->colorOfStreetLight($street_light_id));
        $this->apiLog->info('Street Light', [$street_light_id => $street_light_color]);
        $driverStatusItems = $this->getDriverStatusItems($user->id);
        $this->apiLog->info('Street Light Items', ['items' => $driverStatusItems]);
        $options = MirouteDriverOptions::where('user_id', $user->id)->first();
        $tracking_method = !empty($options) ? $options->tracking_method : 'point2point';

        return [
            'user_id' => $user->id,
            'company_id' => $user->driverProfile->company->id,
            'username' => $user->username,
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'street_light_id' => $street_light_id,
            'street_light_color' => $street_light_color,
            'driverStatusItems' => $driverStatusItems,
            'companyCoordinator' => $companyCoordinator,
            'cardataCoordinator' => $cardataCoordinator,
            'mirouteCoordinator' => $mirouteCoordinator,
            'mileage_entry_method' => $user->driverProfile->company->mileage->mileage_entry_method,
            'lastDrivingRecord' => $lastDrivingRecord,
            'last_entry_date' => empty($lastDrivingRecord) ? $userDate : $lastDrivingRecord['trip_date'],
            'last_starting_odometer' => empty($lastDrivingRecord) ? null : $lastDrivingRecord['starting_odometer'],
            'last_ending_odometer' => empty($lastDrivingRecord) ? null : $lastDrivingRecord['ending_odometer'],
            'miroute_page' => $driving_state,
            'tracking_method' => $tracking_method,
            'trips' => $trips,
            'use_stop_contract' => $user->driverProfile->company->options->use_stop_contract
        ];
    }

    public function findLastDrivingDay($user_id, $userTime)
    {
        $userDate = $this->getUserDate($userTime);
        //$this->apiLog->info('User Date', ['user_id'=>$user_id, 'userTime'=>$userTime, 'userDate'=>$userDate]);

        // remove empty records

        $emptyRecords = DailyMileage::where('user_id', $user_id)
            ->whereNull('starting_odometer')
            ->whereNull('ending_odometer')
            ->get();
        foreach($emptyRecords as $dm) {
            DailyTrip::where('daily_mileage_id', $dm->id)->delete();
            $dm->delete();
        }
        //$this->apiLog->info('Deleted empty records', ['user_id'=>$user_id]);

        // get last record
        $dailyMileageRecord = DailyMileage::where('user_id', $user_id)->orderBy('trip_date', 'desc')->first();
        //$this->apiLog->info('Initial dailyMileageRecord', ['user_id'=>$user_id, 'dailyMileageRecord'=>$dailyMileageRecord]);
        if (empty($dailyMileageRecord)) {
            //$this->apiLog->info('Daily Mileage Empty', ['user_id'=>$user_id]);
            // this is probably a new driver
            $dailyMileageRecord = ['id' => -1, 'trip_date' => $userDate, 'last_state' => 'start_day', 'starting_odometer' => null, 'ending_odometer' => null];
        } else {
            //$this->apiLog->info('Daily Mileage Not Empty', ['user_id'=>$user_id]);
            $dailyMileageRecord = $dailyMileageRecord->toArray();
            //$this->apiLog->info('Converted object to array', ['user_id'=>$user_id, 'dailyMileageRecord'=>$dailyMileageRecord]);
            if ($dailyMileageRecord['trip_date'] == $userDate) {
                //$this->apiLog->info('Record from today', ['user_id'=>$user_id]);
                // user has already logged in today
                // logic for last_state should be in place elsewhere (as driver actions were taken)
            } else {
                //$this->apiLog->info('Record from previous day', ['user_id'=>$user_id]);
                // record from a previous day
                if (empty($dailyMileageRecord['ending_odometer'])) {
                    //$this->apiLog->info('No ending odometer', ['user_id'=>$user_id]);
                    // this an unclosed day, return this record and make user end the day
                    DailyMileage::find($dailyMileageRecord['id'])->update(['last_state' => 'end_previous_day']);
                    //$this->apiLog->info('Updated last state to end previous', ['user_id'=>$user_id]);
                    $dailyMileageRecord['last_state'] = 'end_previous_day';
                    //$this->apiLog->info('Set array last state to end previous', ['user_id'=>$user_id]);
                } else {
                    //$this->apiLog->info('There was an ending odometer', ['user_id'=>$user_id, 'ending'=>$dailyMileageRecord['ending_odometer']]);
                    // ending_odometer was set, update this record if needed and let user start new day
                    if ($dailyMileageRecord['last_state'] != 'day_ended') {
                        //$this->apiLog->info('Last state was not day_ended', ['user_id'=>$user_id]);
                        DailyMileage::find($dailyMileageRecord['id'])->update(['last_state' => 'day_ended']);
                        //$this->apiLog->info('Updated record to day_ended', ['user_id'=>$user_id]);
                    }
                    $dailyMileageRecord = ['id' => -1, 'trip_date' => $userDate, 'last_state' => 'start_day', 'starting_odometer' => null, 'ending_odometer' => null];
                }
            }
        }

        return $dailyMileageRecord;
    }

    function getNearbyStops(Request $request)  # $date, $latitude, $longitude)
    {
        $user_id = auth()->user()->id;
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $date = new \DateTime($request->date);
        $date = $date->modify('+1 day')->format('Y-m-d');

        # TODO this needs to be 1 for production
        $max_distance = 5.0; // km

        $this->apiLog->info('Getting Driver Stops', $request->all());
        $company_id = auth()->user()->driverProfile->company->id;

        $allDriverStops = SavedStop::join('driver_stop', 'driver_stop.saved_stop_id', 'saved_stops.id')->where('driver_stop.user_id', $user_id)->get();

        $r = 6378.7;    // distance in km
        foreach ($allDriverStops as $key => $stop) {
            $distance = $r * acos(sin($latitude / 57.2958) * sin($stop->latitude / 57.2958) +
                    cos($latitude / 57.2958) * cos($stop->latitude / 57.2958) * cos($stop->longitude / 57.2958 - $longitude / 57.2958));
            if ($distance <= $max_distance) {
                $stop->distance = $distance;
            } else {
                $allDriverStops->forget($key);
            }
        }

        $stops = $this->convertSavedStops($allDriverStops);

        return $stops;
    }

    #endregion

    #region CONVERSIONS and CALCULATIONS
    /**
     * Extract just the date from the whole userTime string
     *
     * @param $userTime
     * @return mixed
     */
    public function getUserDate($userTime)
    {
        return explode(' ', $userTime)[0];
    }

    public function convertDailyTrips($dailyTrips)
    {
        $this->apiLog->info('Converting Daily Trips', []);
        $trips = [];
        foreach ($dailyTrips as $dt) {
            $trips[] = [
                'id' => $dt->id,
                'created_at' => $dt->created_at,
                'event' => $dt->event,
                'business_purpose' => $dt->business_purpose,
                'mileage_adjusted' => empty($dt->mileage_adjusted) ? 0 : $dt->mileage_adjusted,
                'mileage_original' => empty($dt->mileage_original) ? 0 : $dt->mileage_original,
                'sequence_number' => $dt->sequence_number,
                'time' => !empty($dt->created_at) ? date('h:i a', strtotime(explode(':', explode(' ', $dt->created_at)[1])[0] . ':' . explode(':', explode(' ', $dt->created_at)[1])[0])) : null,
                'stop' => $this->convertSavedStops([$dt->savedStop])[0]
            ];
        }

        return $trips;
    }

    public function convertSavedStops($selectedStops)
    {
        $this->apiLog->info('Converting Saved Stops', [$selectedStops]);
        $stops = [];
        if (!empty($selectedStops)) {
            foreach ($selectedStops as $ss) {
                $stop = [
                    'id' => $ss->id,
                    'distance' => $ss->distance, // this was added when we calculated distance
                    'city' => $ss->city,
                    'company_id' => $ss->company_id,
                    'company_stop_id' => $ss->company_stop_id,
                    'country' => $ss->country_id == 1 ? 'US' : 'CA',
                    'country_id' => $ss->country_id,
                    'created_at' => $ss->created_at,
                    'driver_stop_id' => $ss->driver_stop_id,
                    'latitude' => $ss->latitude,
                    'longitude' => $ss->longitude,
                    'name' => $ss->name,
                    'state' => StateProvince::find($ss->state_province_id)->name,
                    'state_province_id' => $ss->state_province_id,
                    'street' => $ss->street,
                    'street2' => $ss->street2,
                    'zip_postal' => $ss->zip_postal
                ];
                $this->apiLog->info('Converted Stop', ['stop' => $stop]);
                $stops[] = $stop;
            }
        }

        usort($stops, function ($s1, $s2)  {
            if ($s1['distance'] == $s2['distance']) return 0;
            return $s1['distance'] < $s2['distance'] ? -1 : 1;
        });

        return $stops;
    }

    private function calculateDistance($stop_id_1, $stop_id_2)
    {
        $stop1 = SavedStop::find($stop_id_1);
        $stop2 = SavedStop::find($stop_id_2);

        $latitude_1 = $stop1->latitude;
        $longitude_1 = $stop1->longitude;
        $latitude_2 = $stop2->latitude;
        $longitude_2 = $stop2->longitude;

        $r = 6378.7;    // distance in km
        $distance = $r * acos(sin($latitude_1 / 57.2958) * sin($latitude_2 / 57.2958) +
                cos($latitude_1 / 57.2958) * cos($latitude_2 / 57.2958) * cos($longitude_2 / 57.2958 - $longitude_1 / 57.2958));

        $converted_distance = auth()->user()->driverProfile->company->profile->country_id == 2 ? $distance : $distance * 0.62137119;

        $this->apiLog->info('Computing Distance', ['distance' => $converted_distance, 'units' => auth()->user()->driverProfile->company->profile->country_id == 2 ? 'km' : 'mi']);

        return $converted_distance;
    }

    #endregion

    #region API ACTIONS

    /**
     * End (enter ending odometer) for a previously unclosed day in the past
     *
     * @param Request $request
     * @return array
     */
    public function endPreviousDay(Request $request)
    {
        $userDate = explode(' ', $request->userTime)[0];
        $ending_day = $request->ending_day;
        $ending_odometer = $request->ending_odometer;
        $user_id = auth()->user()->id;
        $this->apiLog->info('Ending Previous Day', $request->all());

        // this is the previous day we were ending
        $lastDay = DailyMileage::where('trip_date', $ending_day)->where('user_id', $user_id)->first();

        // determine final mileage for day
        $personal_mileage = ($ending_odometer - $lastDay->starting_odometer) - $lastDay->business_mileage;
        $lastDay->update(['ending_odometer' => $ending_odometer, 'personal_mileage' => $personal_mileage, 'last_state' => 'day_ended']);

        // create new record for today
        $currentDay = DailyMileage::create([
            'user_id' => $user_id,
            'last_state' => 'start_day',
            'trip_date' => $userDate,
            'starting_odometer' => null,
            'ending_odometer' => null
        ]);

        return ['daily_mileage_id' => $currentDay->id, 'last_state' => 'start_day', 'last_ending_odometer' => $ending_odometer];
    }

    /**
     * Submit final odometer for previously un-ended day.
     * We don't use userTime because we need the exact day specified in the login process.
     *
     * @param Request $request
     * @return array
     */
    public function endDay(Request $request)
    {
        $ending_day = $request->ending_day;
        $ending_odometer = $request->ending_odometer;
        $user_id = auth()->user()->id;
        $this->apiLog->info('Ending Previous Day', $request->all());

        // this should be a record for 'today' (user time, not server time)
        $lastDay = DailyMileage::where('trip_date', $ending_day)->where('user_id', $user_id)->first();
        $lastTrip = DailyTrip::where('daily_mileage_id', $lastDay->id)->orderBy('sequence_number', 'desc')->first();
        if (!empty($lastTrip)) {
            $lastTrip->event = 'end';
            $lastTrip->save();
        }

        // determine final mileage for day
        $personal_mileage = ($ending_odometer - $lastDay->starting_odometer) - $lastDay->business_mileage;
        $lastDay->update(['ending_odometer' => $ending_odometer, 'personal_mileage' => $personal_mileage, 'last_state' => 'day_ended']);

        return ['daily_mileage_id' => $lastDay->id, 'last_state' => 'start_day'];
    }

    public function submitStartingOdometer(Request $request)
    {
        $userDate = $this->getUserDate($request->userTime);

        $dailyMileage = DailyMileage::where('user_id', auth()->user()->id)->where('trip_date', $userDate)->first();

        if (empty($dailyMileage)) {
            $dailyMileage = DailyMileage::create([
                'user_id' => auth()->user()->id,
                'trip_date' => $userDate,
                'starting_odometer' => $request->starting_odometer
            ]);
        }

        $saved_stop = SavedStop::find($request->stop_id);

        // we need the last daily mileage odometer to calculate gap mileage
        $lastDailyMileage = DailyMileage::where('user_id', auth()->user()->id)->where('trip_date', '<', $userDate)->orderBy('trip_date', 'desc')->first();
        $gap_mileage = empty($lastDailyMileage) ? 0 : $request->starting_odometer - $lastDailyMileage->ending_odometer;

        $dailyMileage->update([
            'last_state' => 'driving',
            'destination' => $saved_stop->name,
            'business_purpose' => $request->business_purpose,
            'starting_odometer' => $request->starting_odometer,
            'business_mileage' => 0,
            'commuter_mileage' => 0,
            'personal_mileage' => 0,
            'gap_mileage' => $gap_mileage
        ]);

        $dailyTrip = DailyTrip::create([
            'saved_stop_id' => $request->stop_id,
            'daily_mileage_id' => $dailyMileage->id,
            'sequence_number' => 1,
            'event' => 'start',
            'business_purpose' => $request->business_purpose,
            'mileage_original' => 0.0,
            'mileage_adjusted' => 0.0
        ]);

        $returnDailyTrip = $this->convertDailyTrips([$dailyTrip])[0];

        return ['success' => (!empty($dailyTrip) && $dailyTrip->id > 0), 'starting_trip_id' => ($dailyTrip ? $dailyTrip->id : -1), 'last_state' => 'driving', 'daily_trip' => $returnDailyTrip, 'daily_mileage_id' => $dailyMileage->id];
    }

    /**
     * This is called when stopping from driving state, only updates last_state from 'driving' to 'stopped'
     * Could be leveraged to do more.
     *
     * @param Request $request
     * @return array
     */
    public function updateDailyMileageLastState(Request $request)
    {
        $this->apiLog->info('Updating Last State', $request->all());
        $daily_mileage_id = $request->daily_mileage_id;
        $last_state = $request->last_state;
        $userDate = explode(' ', $request->userTime)[0];


        $daily_mileage = $daily_mileage_id > -1 ? DailyMileage::find($daily_mileage_id) : DailyMileage::where('trip_date', $userDate)->where('user_id', auth()->user()->id)->first();

        $daily_mileage->update(['last_state' => $last_state]);

        return ['daily_mileage' => $daily_mileage];
    }

    /**
     * Updates the driver's status, and returns an array of stops near the driver's current location
     * @param Request $request
     * @return array|Response
     */
    public function atStop(Request $request)
    {
        $this->apiLog->info('At A Stop', $request->all());

        $userDate = explode(' ', $request->userTime)[0];
        $daily_mileage_id = $request->daily_mileage_id;
        $business_purpose = $request->business_purpose;
        $event = $request->event;
        $destination = $request->destination; // also saved stop name
        $saved_stop_id = $request->saved_stop_id;

        $dailyMileage = $daily_mileage_id > 0 ? DailyMileage::find($daily_mileage_id) : DailyMileage::where('user_id', auth()->user()->id)->where('trip_date', $userDate)->first();
        if ($dailyMileage->ending_odometer > 0) {
            return Response::create(['code' => 401, 'message' => 'day_ended'], 404);
        }
        $dailyMileage->destination = $dailyMileage->destination . ' &rarr; ' . $destination;
        $dailyMileage->business_purpose = $dailyMileage->business_purpose . '; ' . $business_purpose;

        // find last stop
        $lastStop = DailyTrip::where('daily_mileage_id', $dailyMileage->id)->orderBy('id', 'desc')->limit(1)->first();

        $mileage_original = $this->calculateDistance($lastStop->saved_stop_id, $saved_stop_id);

        $newTripData = [
            'saved_stop_id' => $saved_stop_id,
            'daily_mileage_id' => $dailyMileage->id,
            'sequence_number' => $lastStop->sequence_number + 1,
            'event' => $event,
            'business_purpose' => $business_purpose,
            'mileage_original' => $mileage_original
        ];

        $newDailyTrip = DailyTrip::create($newTripData);

        $dailyMileage->business_mileage = $dailyMileage->business_mileage + $mileage_original;
        $dailyMileage->last_state = $event;
        $dailyMileage->save();

        $returnTrips = $this->convertDailyTrips(DailyTrip::where('daily_mileage_id', $dailyMileage->id)->get());
        $returnLastStop = $this->convertDailyTrips([$newDailyTrip])[0];
        return ['message' => 'stop_submitted', 'lastStop' => $returnLastStop, 'trips' => $returnTrips, 'dailyMileage' => $dailyMileage];
    }

    public function submitNewStop(Request $request)
    {
        $data = $request->stop;
        $this->apiLog->info('New Stop Submitted', ['stop data' => $data, 'userTime' => $request->userTime]);
        $user_id = auth()->user()->id;

        // most location found from state
        $stateProvince = StateProvince::where('short_name', $data['state'])->first();
        $data['state_province_id'] = $stateProvince->id;
        $data['country_id'] = $stateProvince->country_id;
        $data['active'] = true;

        $newStop = SavedStop::create($data);
        $newStop->driver_stop_id = $newStop->id;
        $newStop->save();
        $newStopTransformed = $this->convertSavedStops([$newStop])[0];

        $userDate = explode(' ', $request->userTime)[0];
        $driver_stop = \DB::select(\DB::raw("insert into driver_stop (user_id, saved_stop_id, effective_date) values ({$user_id}, {$newStop->id}, '{$userDate}')"));
        $this->apiLog->info('Driver Stop Query', ['driver_stop' => $driver_stop]);

        return ['message' => 'success', 'new stop' => $newStopTransformed];
    }

    #endregion

    public function resumeDriving(Request $request)
    {
        $this->apiLog->info("Resume Driving", ['user_id' => auth()->user()->id, 'trip_date' => $request->trip_date]);
        $mileage = DailyMileage::where('trip_date', $request->trip_date)->where('user_id', 29473)->first();
        $trip = $mileage->trips->last();
        $trip->update(['event' => 'stopped']);
        $mileage->update(['last_state' => 'stopped', 'ending_odometer' => null]);

        // get userInfo for use by app, add access token to user
        $responseObject = $this->gatherUserInfo(auth()->user()->id, $request->userTime);

        return ['userInfo' => $responseObject];
    }

    public function updateDailyTrip(Request $request)
    {
        $user_id = auth()->user()->id;
        $trip = $request->trip;
        $dailyTrip = DailyTrip::find($trip['id']);
        $this->apiLog->info('Updating DailyTrip', ['user_id' => $user_id, 'trip_id' => $trip['id'], 'daily_trip'=>$dailyTrip]);
        $dailyTrip->business_purpose = $trip['business_purpose'];
        $dailyTrip->mileage_adjusted = $trip['mileage_adjusted'];
        $dailyTrip->save();

        $this->updateDailyMileage($dailyTrip->daily_mileage_id);

        return ['message' => 'success'];
    }

    protected function updateDailyMileage($id) {
        $original_mileage_sum = 0;
        $original_adjusted_sum = 0;
        $combined_destination = '';
        $combined_purpose = '';
        $dailyMileage = DailyMileage::find($id);

        $trips = DailyTrip::where('daily_mileage_id', $id)->orderBy('sequence_number')->get();
        foreach ($trips as $trip) {
            $original_mileage_sum += $trip->mileage_original;
            $original_adjusted_sum += $trip->mileage_adjusted;
            $combined_destination .= $trip->savedStop->name . " &rarr; ";
            $combined_purpose .= $trip->business_purpose . " &rarr; ";
        }

        $combined_destination_arr = explode('&rarr;', $combined_destination);
        array_pop($combined_destination_arr);
        $combined_destination = trim(implode('&rarr;', $combined_destination_arr));

        $combined_purpose_arr = explode('&rarr;', $combined_purpose);
        array_pop($combined_purpose_arr);
        $combined_purpose = trim(implode('&rarr;', $combined_purpose_arr));

        $dailyMileage->destination = $combined_destination;
        $dailyMileage->business_purpose = $combined_purpose;
        $sum = $original_mileage_sum + $original_adjusted_sum;
        $dailyMileage->business_mileage = $sum;

        $dailyMileage->save();
    }

    private function loginSuccess($username)
    {

        $ip = $_SERVER['REMOTE_ADDR'];
        $address = gethostbyaddr($ip);
        $platform = $_SERVER['HTTP_USER_AGENT'];

        $login = new Login;
        $login->username = $username;
        $login->ip = $ip;
        $login->address = $address;
        $login->platform = $platform;
        $login->login_type = 'miroute';
        $login->access = 'granted';
        $login->save();

    }

    private function loginFailed($username, $reason)
    {

        $ip = $_SERVER['REMOTE_ADDR'];
        $address = gethostbyaddr($ip);
        $platform = $_SERVER['HTTP_USER_AGENT'];

        $login = new Login;
        $login->username = $username;
        $login->ip = $ip;
        $login->address = $address;
        $login->platform = $platform;
        $login->login_type = 'miroute';
        $login->access = 'failed: ' . $reason;
        $login->save();
    }

    /**
     * Direct the API to log some information
     *
     * @param Request $request
     */
    public function apiLog(Request $request)
    {
        $this->apiLog->info('API Notification', ['user_id' => auth()->user()->id, 'user_time' => $request->userTime, 'message' => $request->message]);
    }

    public function setMirouteDriverOptions(Request $request)
    {
        $options = MirouteDriverOptions::where('user_id', auth()->user()->id)->first();
        if (empty($options)) {
            $options = MirouteDriverOptions::create([
                'user_id' => auth()->user()->id,
                'tracking_method' => $request->tracking_method
            ]);
        } else {
            $options->update(['tracking_method' => $request->tracking_method]);
        }

        return ['options_id' => $options->id];
    }

    public function getLastEndingOdometer()
    {
        $user_id = auth()->user()->id;
        $lastEndingRecord = DailyMileage::where('user_id', $user_id)
            ->where('ending_odometer', '>', 0)
            ->select('ending_odometer')
            ->orderBy('trip_date', 'desc')
            ->first();
        $last_ending_odometer = !empty($lastEndingRecord) ? $lastEndingRecord->ending_odometer : null;
        return ['last_ending_odometer' => $last_ending_odometer];
    }

    /**
     * Removes a trip from sequence, then reorders the day's trips and recalculates mileage
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function deleteDailyTrip(Request $request)
    {
        $trip_id = $request->trip_id;
        $this->apiLog->info("Deleting trip", ['user_id'=>auth()->user()->id, 'trip_id'=>$trip_id]);
        // delete trip with given id, it is a hard delete
        $trip = DailyTrip::find($trip_id);
        $deleted_sequence_number = $trip->sequence_number;
        $daily_mileage_id = $trip->daily_mileage_id;
        $trip->delete();

        // decrease sequence number above given request by 1
        $res = DailyTrip::where('daily_mileage_id', $daily_mileage_id)
            ->where('sequence_number', '>', $deleted_sequence_number)
            ->update(['sequence_number' => \DB::raw('sequence_number - 1')]);

        // if first trip was deleted, change event to start, clear mileage_original, mileage_adjusted and comments
        if ($deleted_sequence_number == 1) {
            $firstTrip = DailyTrip::where('daily_mileage_id', $daily_mileage_id)
                ->where('sequence_number', 1)->first();
            if ($firstTrip) {
                $firstTrip->event = 'start';
                $firstTrip->business_purpose = 'STARTING DAY';
                $firstTrip->mileage_original = 0;
                $firstTrip->mileage_adjusted = null;
                $firstTrip->adjustment_comment = null;
                $firstTrip->save();
            }
        } else {
            // calculate distance for next trip if any
            $nextTrip = DailyTrip::where('daily_mileage_id', $daily_mileage_id)
                ->where('sequence_number', $deleted_sequence_number)->first();
            if ($nextTrip) {
                $nextTrip->mileage_original = $this->calculateTripMileage($nextTrip);
                $nextTrip->save();
            }
        }

        $this->updateDailyMileage($daily_mileage_id);

        return ['message' => 'success'];
    }

    protected function calculateTripMileage($trip)
    {
        $sequence_number = $trip->sequence_number;
        if ($sequence_number == 1) {
            return 0.0;
        } else {
            $to['latitude'] = $trip->savedStop->latitude;
            $to['longitude'] = $trip->savedStop->longitude;

            // get previous stop;
            $lastTrip = DailyTrip::where('daily_mileage_id', $trip->daily_mileage_id)
                ->where('sequence_number', '<', $sequence_number)
                ->orderBy('sequence_number', 'desc')
                ->first();
            if ($lastTrip) {
                $from['latitude'] = $lastTrip->savedStop->latitude;
                $from['longitude'] = $lastTrip->savedStop->longitude;

                return $this->getDrivingDistance($from, $to);  // from Google

            } else {
                return 0.0; // could not find previous trip
            }
        }
    }

    protected function getDrivingDistance($from, $to, $country = 1)
    {
        $lat1 = $from['latitude'];
        $long1 = $from['longitude'];
        $lat2 = $to['latitude'];
        $long2 = $to['longitude'];

        if ($lat1 == $lat2 && $long1 == $long2) {
            return 0;
        }

        // to avoid pointing to Africa west cost
        if ($lat1 * $lat2 * $long1 * $long2 == 0) {
            return 0;
        }

        // TO DO get google api key from configuration file
        //global $ga;
        //$key = $ga['google_api_key'];

        $key = 'AIzaSyB4cVyUmAOKJC0foRNHAjROST06W7U8Iow';

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&key=$key";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
        $dist *= $country == 1 ? 0.00062137 : 0.001; //converting meters to miles or km

        return round($dist, 2);
    }

    /**
     * Return all items in the driver status as an array of objects that can be displayed in the driver dashboard
     *
     * @param $user_id
     * @return array
     */
    public function getDriverStatusItems($user_id) {
        $statusItems = [];
        $statuses = $this->streetLightCalculator->driverStreetLightData($user_id);

        $this->apiLog->info('Street Light before foreach', ['statuses'=> $statuses]);

        foreach ($statuses as $status) {
            $statusItems[] = [
                'id' => $status->id,
                'color' => strtolower($status->color),
                'title' => ucwords($status->title) == 'Reimbursement' ? 'You Qualify For Reimbursement' : ucwords($status->title),
                'plan' => $status->service_plan
            ];
        }
        //$this->apiLog->info('STATUS', $statusItems);
        return $statusItems;
    }
}
