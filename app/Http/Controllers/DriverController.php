<?php

namespace App\Http\Controllers;

use App\AnnualIrsOdometerDeclaration;
use App\Calculators\StreetLightCalculator;
use App\CompanyInsurance;
use App\CompanyLicense;
use App\CompanyPaymentDate;
use App\DailyTrip;
use App\DriverInsurance;
use App\DriverInsuranceReview;
use App\DriverInsuranceDocument;
use App\Http\Traits\DriverTraits;
use App\Http\Traits\UtilityTraits;
use App\Http\Traits\CompanyTraits;
use App\MonthlyMileage;
use App\TimeZone;
use Barryvdh\Debugbar\Middleware\Debugbar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use App\Address;
use App\DriverProfile;
use App\DriverRegistrationCompletion;
use App\DriverStreetLightStatus;
use App\ManagerProfile;
use App\MonthlyReimbursement;
use App\PersonalVehicle;
use App\PostalCode;
use App\SavedStop;
use App\StateProvince;
use App\User;
use App\VehicleProfile;
use App\ZipCode;
use App\DailyMileage;
use App\DocumentRejectionReason;
use App\Company;
use App\Http\Controllers\ReimbursementController;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf;
use Symfony\Component\Debug\Debug;
use Illuminate\Support\Facades\Validator as Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use \Datetime;
use Carbon\Carbon;

use App\Http\Traits\CommonTraits;
use App\Http\Traits\ValidationTraits;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use App\Mail\InsuranceRejectionEmail; // overall package rejection
use App\Mail\InsuranceAcceptanceEmail;
use App\Mail\InsuranceDocumentRejected; // single document
use Illuminate\Support\Facades\Mail;


class DriverController extends Controller
{
    use CompanyTraits;
    use UtilityTraits;
    use CommonTraits;
    use DriverTraits;
    use ValidationTraits;

    protected $reimbursementController;
    protected $streetLightCalculator;

    public function __construct(ReimbursementController $reimbursementController)
    {
        $this->reimbursementController = $reimbursementController;
        $this->streetLightCalculator = new StreetLightCalculator();
        $this->middleware('auth');

        // create a log for today
        $this->today = date('Y-m-d');
        CommonTraits::delete_older_than(storage_path('logs/driver'), 3600 * 24 * 7);
        $this->log = new Logger('driver');
        $this->log->pushHandler(new StreamHandler(storage_path('logs/driver/driver.' . $this->today . '.log')), Logger::INFO);
    }

    public function viewProfile(Request $request)
    {
        if ($request->ajax()) {
            return response()->json(['message' => 'That worked']);
        }
        return view('driver.profile')->with('user', $request->user());
    }

    /**
     * @param Request $request . Expected user_id, year and month
     * extracts odometers, business and personal mileage if difference in odometer reading is greated than 1000
     * @return mixed ['starting','ending', 'business', 'personal']
     */
    public function getMileagePerVehicle(Request $request)
    {
        $data_per_vehicle = [];

        $user_id = $request->user_id;
        $year = $request->year;
        $month = $request->month;

        $data_per_vehicle = $this->getMileagePerVehicleInternal($user_id, $year, $month);


//        \Debugbar::info($data_per_vehicle);
        return response()->json([
            'mileage' => $data_per_vehicle
        ]);
    }

    /**
     * @param int $user_id
     * @param int $year
     * @param int $monthu
     * @return array $data_per_vehicle
     */
    private function getMileagePerVehicleInternal($user_id, $year, $month)
    {

//        \Debugbar::info("getMileagePerVehicle: u=" . $user_id . " y=" . $year . " m=" . $month);

        $from_date = sprintf("%04d-%02d-01", $year, $month);
        $to_month = $month == 12 ? 1 : $month + 1;
        $to_year = $month == 12 ? $year + 1 : $year;
        $to_date = sprintf("%04d-%02d-01", $to_year, $to_month);

        // data per vehicle
        $v_business = 0;
        $v_starting_odometer = 0;
        $v_ending_odometer = 0;
        $v_personal = 0;

        $data_per_vehicle = [];

        // daily entries from current month entries
        $records = DailyMileage::where('user_id', $user_id)->where('trip_date', '>=', $from_date)->where('trip_date', '<', $to_date)->orderBy('trip_date')->get();

        // last odometer from previous months
        $previous_entry = DailyMileage::where('user_id', $user_id)
            ->whereNotNull('ending_odometer')
            ->where('ending_odometer', '>', 0)
            ->where('trip_date', '<', $from_date)
            ->orderBy('trip_date', 'desc')->first();
        $last_odometer = $previous_entry != null ? $previous_entry->ending_odometer : null;

        if ($last_odometer > 0) {
            $v_starting_odometer = $last_odometer;
            $v_ending_odometer = $last_odometer;
        }


        foreach ($records as $r) {

            // buggers replaced zeroes with nulls!
            if (empty($r['starting_odometer']) && empty($r['ending_odometer']) && empty($r['business_mileage'])) {
                continue;
            }
            if (empty($r['starting_odometer'])) $r['starting_odometer'] = 0;
            if (empty($r['ending_odometer'])) $r['ending_odometer'] = 0;
            if (empty($r['business_mileage'])) $r['business_mileage'] = 0;

            if ($r['starting_odometer'] > 0 && $r['ending_odometer'] > 0 && abs($v_ending_odometer - $r['starting_odometer']) <= 1000) {
                $v_business += $r['business_mileage'];
                $v_ending_odometer = $r['ending_odometer'];
            } elseif ($r['starting_odometer'] > 0 && empty($r['ending_odometer']) && abs($v_ending_odometer - $r['starting_odometer']) <= 1000) {
                $v_business += $r['business_mileage'];
            } elseif (empty($r['starting_odometer']) && $r['ending_odometer'] > 0 && abs($v_ending_odometer - $r['ending_odometer']) <= 1000) {
                $v_business += $r['business_mileage'];
                $v_ending_odometer = $r['ending_odometer'];
            } elseif (empty($r['starting_odometer']) && empty($r['ending_odometer']) && $r['business_mileage'] > 0) {
                $v_business += $r['business_mileage'];
            } else {
                $v_personal = $v_ending_odometer - $v_starting_odometer - $v_business;
                if ($v_personal != 0 || $v_business != 0) {
                    $data_per_vehicle[] = ['starting' => $v_starting_odometer, 'ending' => $v_ending_odometer, 'business' => $v_business, 'personal' => $v_personal];
                }
                // reset values
                $v_business = $r['business_mileage'];
                $v_starting_odometer = $r['starting_odometer'];
                $v_ending_odometer = $r['ending_odometer'];
            }
        }
        $v_personal = $v_ending_odometer - $v_starting_odometer - $v_business;
        if ($v_business != 0 || $v_personal != 0) {
            $data_per_vehicle[] = ['starting' => $v_starting_odometer, 'ending' => $v_ending_odometer, 'business' => $v_business, 'personal' => $v_personal];
        }

        return $data_per_vehicle;
    }

    /**
     * Returns mileage entries for supplied month and year in request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMileage(Request $request)
    {
        $mileage = [];

        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();

        $mileage_entry_method = $user->driverProfile->company->mileage->mileage_entry_method;
        $miroute = $mileage_entry_method == 'Mi-Route' ? true : false;
        $commuter = $mileage_entry_method == 'Daily Commuter' ? true : false;

        // get days in given month and year
        // mktime(hour, minute, second, month, day, year)
        $days_in_month = date('t', mktime(0, 0, 0, $request->month, 1, $request->year));


        // initialize mileage array
        for ($day = 1; $day <= $days_in_month; $day++) {
            if (!$miroute) {
                $mileage[$day] = [
                    'id' => null,
                    'date' => date('D d', mktime(0, 0, 0, $request->month, $day, $request->year)),
                    'date_value' => date('Y-m-d', mktime(0, 0, 0, $request->month, $day, $request->year)),
                    'destination' => '',
                    'purpose' => '',
                    'starting_odometer' => null,
                    'ending_odometer' => null,
                    'business_mileage' => null,
                    'personal_mileage' => null,
                    'commuter_mileage' => null
                ];
            } else {
                $mileage[$day] = [
                    'id' => null,
                    'date' => date('M d', mktime(0, 0, 0, $request->month, $day, $request->year)),
                    'date_value' => date('Y-m-d', mktime(0, 0, 0, $request->month, $day, $request->year)),
                    'destination' => '',
                    'purpose' => '',
                    'starting_odometer' => 0,
                    'ending_odometer' => 0,
                    'business_mileage' => 0,
                    'personal_mileage' => 0,
                    'trips' => 0
                ];
            }
        }

        $from_date = sprintf("%04d-%02d-01", $request->year, $request->month);
        $to_month = $request->month == 12 ? 1 : $request->month + 1;
        $to_year = $request->month == 12 ? $request->year + 1 : $request->year;
        $to_date = sprintf("%04d-%02d-01", $to_year, $to_month);

        $records = DailyMileage::where('user_id', $user->id)->where('trip_date', '>=', $from_date)->where('trip_date', '<', $to_date)->orderBy('trip_date')->get();
        \Debugbar::info($records);

        foreach ($records as $record) {
            $dd = (int)substr($record->trip_date, -2, 2);
            $mileage[$dd]['id'] = $record->id;
            $mileage[$dd]['starting_odometer'] = $record->starting_odometer;
            $mileage[$dd]['ending_odometer'] = $record->ending_odometer;
            $mileage[$dd]['business_mileage'] = $record->business_mileage;
            $mileage[$dd]['commuter_mileage'] = $record->commuter_mileage;
            $mileage[$dd]['destination'] = str_replace("â†’", "->", $record->destination);
            $mileage[$dd]['business_purpose'] = $record->business_purpose;
            if ($record->business_mileage != '' && ($record->starting_odometer == '' || $record->ending_odometer == '')) {
                $mileage[$dd]['personal_mileage'] = 'n/a';
            } else {
                $mileage[$dd]['personal_mileage'] = $record->personal_mileage;
            }
            if ($miroute) {
                $count = 0;
                foreach ($record->trips as $trip) {
                    if ($trip->event != 'start') {
                        $count++;
                    }
                }
                $mileage[$dd]['trips'] = $count;
            }

            // if personal mileage is empty, try to populate it from odometer readings.
        }

        \Debugbar::info($mileage);
        return response()->json([
            'mileage' => $mileage
        ]);
    }

    /**
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReimbursementYears(Request $request)
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $userId = $user->id;
        $years = MonthlyReimbursement::select('year')->where('user_id', $userId)->distinct()->orderBy('year')->get();

        if (count($years) > 0) {
            $startYear = $years[0]->year;
            $endYear = $years[count($years) - 1]->year;
        } else {
            $startYear = null;
            $endYear = null;
        }

        \Debugbar::info($startYear);
        \Debugbar::info($endYear);
        return response()->json([
            'startYear' => $startYear,
            'endYear' => $endYear
        ]);
    }

    public function getDepreciationValues(Request $request)
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $userId = $user->id;
        $values = MonthlyReimbursement::select('month', 'vehicle_depreciation')->where('user_id', $userId)
            ->where('year', $request->year)->orderBy('month')->get();
        $total = 0;
        foreach ($values as $row) {
            $total += $row->vehicle_depreciation;
        }
        return ['depreciation' => $values, 'total' => $total];
    }

    public function getReimbursementForYear(Request $request)
    {
        //dd($request);
        $reimbursement = [];
        if ($request->user_id) {
            $user = User::find($request->user_id);
        } else {
            $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        }
        $company = $user->driverProfile->company;

        $service_plan = $company->options->service_plan;

        if ($service_plan == 'FAVR') { // find previous fixed rate and its driver profile.
            $values = MonthlyReimbursement::select('fixed_reimbursement', 'driver_profile_id')->
            where('user_id', $user->id)->where('year', '<', $request->year)->
            orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
            if ($values) {
                $last_fixed = $values->fixed_reimbursement;
                $last_profile = $values->driver_profile_id;
            } else {
                $last_fixed = null;
                $last_profile = null;
            }
        }

        $WM_note = $company->id == 54 || $company->id == 56 ? $this->populate_note_for_WM($company->id) : null;
        $show_red_ball = null;

        $show_tax_limit = $company->show_tax_limit > 0 ? true : false;

        $mileage_entry_method = $user->driverProfile->company->mileage->mileage_entry_method;
        $commuter = $mileage_entry_method == 'Daily Commuter' ? true : false;


        $total_fixed = 0;
        $total_adjustment = 0;
        $total_fixed_paid = 0;
        $total_variable = 0;
        $total_mileage = 0;
        $total_total = 0;
        $total_tax_limit = 0;
        $total_depreciation = 0;
        $total_commuter = 0;
        $total_taxable_reimbursement = 0;


        $empty = [
            'month_id' => 0,
            'month' => null,
            'fixed' => 0,
            'adjustment' => 0,
            'fixed_paid' => 0,
            'mileage' => 0,
            'commuter_mileage' => 0,
            'centPerMile' => 0,
            'variable' => 0,
            'total' => 0,
            'direct_pay_date' => null,
            'picture' => null,
            'color' => null,
            'street_light_id' => null,
            'depreciation' => 0,
            'tax_limit' => 0,
            'taxable_reimbursement' => 0,
            'show_link' => 1,
            'within_grace_period' => 0,

            'driver_profile_id' => null,
            'impact_on_reimbursement' => 0,
            'last_driver_profile_id' => null,
        ];

        if ($show_tax_limit) {
            $mileage_rate_arr = $this->reimbursementController->getMileageRateArr(1, $request->year);
        }

        for ($mm = 1; $mm <= 12; $mm++) {
            $empty['month'] = date('F', mktime(0, 0, 0, $mm, 1, $request->year));
            $reimbursement[$mm] = $empty;
        }

        if ($commuter) {
            $comm_arr = [];
            for ($mm = 1; $mm <= 12; $mm++) {
                $comm_arr[$mm] = 0;
            }
            $from_date = sprintf("%04d-01-01", $request->year);
            $to_date = sprintf("%04d-01-01", $request->year + 1);
            $query = "SELECT MONTH(dm.trip_date) as month, IFNULL(SUM(dm.commuter_mileage),0) as commuter " .
                "FROM daily_mileages dm WHERE dm.user_id = $user->id AND dm.trip_date >= '$from_date' " .
                "AND trip_date < '$to_date' GROUP BY 1";
            $comm_totals = \DB::select($query);
            foreach ($comm_totals as $comm) {
                $comm_arr[$comm->month] = $comm->commuter;
            }

        }

        $reimb = \DB::table('monthly_reimbursements AS mr')->leftJoin('monthly_mileages AS mm', function ($join) {
            $join->on('mr.user_id', '=', 'mm.user_id');
            $join->on('mr.year', '=', 'mm.year');
            $join->on('mr.month', '=', 'mm.month');
        })
            ->join('driver_profiles as dp', 'mr.driver_profile_id', '=', 'dp.id')
            ->leftJoin('monthly_approvals as ma', function ($join) {
                $join->on('mr.year', '=', 'ma.year');
                $join->on('mr.month', '=', 'ma.month');
                $join->on('dp.company_id', '=', 'ma.company_id');
            })
            ->select('mr.*', 'mm.business_mileage', 'ma.approval_date', 'dp.street_light_id')
            ->where('mr.user_id', $user->id)
            ->where('mr.year', $request->year)
            ->orderBy('mr.month')->get();

        foreach ($reimb as $r) {
            $month = $r->month;
            $fixed = $r->fixed_reimbursement;
            $total_fixed += $fixed;
            $adjustment = $r->adjustment_amount;
            $total_adjustment += $adjustment;
            $fixed_paid = $fixed - $adjustment;
            $total_fixed_paid += $fixed_paid;
            $mileage = $r->business_mileage;
            $total_mileage += $mileage;
            if ($commuter) {
                $total_commuter += $comm_arr[$month];
            }
            $centPerMile = $r->variable_reimbursement;

            $color = strtolower($this->streetLightCalculator->colorOfReimbursementStatus($r));

            $variable = $this->reimbursementController->getVariableReimbursement($company->id, $request->year, $month, $centPerMile, $mileage);

            //$variable = round($mileage * $centPerMile / 100, 2);
            $total_variable += $variable;
            $total = $fixed + $variable;
            $total_total += $total;
            $total_depreciation += $r->vehicle_depreciation;
            $pay_date = !empty($r->approval_date) ? date("D, M jS, Y", strtotime($r->approval_date)) : null;

            //$mileage_rate = $this->reimbursementController->getMileageRate(1,$request->year,$month);
            //\Debugbar::info('rate = ' . $mileage_rate);
            if ($show_tax_limit) {
                $tax_limit = round($mileage * $mileage_rate_arr[$month], 2);
                $total_tax_limit += $tax_limit;
                $taxable_reimbursement = max(0, $total - $tax_limit);
                $total_taxable_reimbursement += $taxable_reimbursement;
            } else {
                $tax_limit = 0;
                $taxable_reimbursement = 0;
            }


            // $total_taxable_reimbursement += max( 0, round($total - $taxable, 2) );
            $reimbursement[$month] = [
                'month_id' => $month,
                'month' => date('F', mktime(0, 0, 0, $month, 1, $request->year)),
                'fixed' => number_format($fixed, 2),
                'adjustment' => number_format($r->adjustment_amount, 2),
                'fixed_paid' => number_format($fixed_paid, 2),
                'mileage' => number_format($mileage),
                'commuter_mileage' => $commuter ? $comm_arr[$month] : 0,
                'centPerMile' => $centPerMile * 10000 % 100 > 0 ? number_format($centPerMile, 4) : number_format($centPerMile, 2),
                'variable' => number_format($variable, 2),
                'total' => number_format($total, 2),
                'direct_pay_date' => $pay_date,
                'street_light_id' => $r->fixed_status,
                'color' => $color,
                'depreciation' => number_format($r->vehicle_depreciation, 2),
                'tax_limit' => number_format($tax_limit, 2),
                'taxable_reimbursement' => number_format($taxable_reimbursement, 2),
                'show_link' => 1,
                'within_grace_period' => $r->within_grace_period,
                'fixed_status' => $r->fixed_status,
                'last_driver_profile_id' => $service_plan == 'FAVR' ? $last_profile : null,
                'driver_profile_id' => $r->driver_profile_id,
                'impact_on_reimbursement' => $service_plan == 'FAVR' && $r->fixed_reimbursement != $last_fixed
                && $r->driver_profile_id != $last_profile ? 1 : 0
            ];

            $impact_in_reimb = $service_plan == 'FAVR' && $r->fixed_reimbursement != $last_fixed
            && $r->driver_profile_id != $last_profile ? 1 : 0;
            $last_fixed = $fixed;
            $last_profile = $r->driver_profile_id;
        }
        $reimbursement[] = [
            'month' => "TOTALS",
            'fixed' => number_format($total_fixed, 2),
            'adjustment' => number_format($total_adjustment, 2),
            'fixed_paid' => number_format($total_fixed_paid, 2),
            'mileage' => number_format($total_mileage),
            'commuter_mileage' => number_format($total_commuter),
            'centPerMile' => '',
            'variable' => number_format($total_variable, 2),
            'total' => number_format($total_total, 2),
            'direct_pay_date' => null,
            'status' => null,
            'depreciation' => number_format($total_depreciation, 2),
            'tax_limit' => $total_tax_limit,
            'taxable_reimbursement' => $total_taxable_reimbursement,
            'show_link => 0'

        ];

        return response()->json(['reimbursement' => $reimbursement, 'WM_note' => $WM_note, 'show_red_ball' => $show_red_ball]);
    }

    public function getFavrReimbursementStatus(Request $request)
    {
        $light_id = $request->fixed_status;
        $in_grace = $request->within_grace_period;
        $company_id = $request->company_id;
        $company_name = $request->company_name;
        $no_reimb_without_insurance = $request->no_reimb_without_insurance;
        $all_lock_day = $request->all_lock_day;

        \Debugbar::info("getFavrReimbursementStatus ... explanations for ", $request);

        $street_arr = [];
        $remove_no_taxable_phrase = false;
        $glue = '\n';
        $status_arr = DriverStreetLightStatus::all()->sortBy('title');
        foreach ($status_arr as $row) {
            $street_arr[$row['id']] = ucwords($row['title']);
        }

        $ret_arr = [];

        if ($light_id & GREEN_NO_BANKING_DATA) {
            $ret_arr[] = ['status' => $street_arr[GREEN_NO_BANKING_DATA], 'impact' => "No reimbursement as bank information was not recorded before {$all_lock_day}th of this month."];
        }

        if ($light_id & GREEN_NO_DEPRECIATION_COMPLIANCE) {
            $ret_arr[] = ['status' => $street_arr[GREEN_NO_DEPRECIATION_COMPLIANCE], 'impact' => 'Your entire reimbursement amount is taxable.'];
        }

        $description = '';
        $reason = '';
        if ($light_id & GREEN_NO_INSURANCE_COMPLIANCE || $light_id & RED_AWAITING_INSURANCE ||
            $light_id & RED_INSUFFICIENT_INSURANCE || $light_id & RED_INSURANCE_EXPIRED
        ) {
            if ($in_grace) {
                $description .= "Your reimbursement will be paid non taxable when in the 30 day grace period.\n
                    \nSubsequent reimbursements will be paid as follows:\n";
                if ($no_reimb_without_insurance) {
                    $remove_no_taxable_phrase = true;
                    $description .= "Due to the $company_name policy for Insurance Compliance, the reimbursement will be forfeited when documentation is not received during the 30 day grace period.";
                }
            } elseif ($no_reimb_without_insurance) {
                $remove_no_taxable_phrase = true;
                $description .= "Due to the $company_name policy for Insurance Compliance, the reimbursement will be forfeited when documentation is not received during the 30 day grace period.";
            }
        } else {
            if ($company_id == 54 && ($light_id & GREEN_NO_VEHICLE_AGE_COMPLIANCE || $light_id & GREEN_NO_VEHICLE_COST_COMPLIANCE)) {
                $remove_no_taxable_phrase = true;
                if ($light_id & GREEN_NO_VEHICLE_AGE_COMPLIANCE) {
                    if ($in_grace) {
                        if (!empty($description)) $description .= "\n";
                        $description .= "Your fixed and variable reimbursement will be paid non taxable as per the IRS rules and regulations in the 30 day grace period.\n";
                        $description .= "Subsequent reimbursements will be paid as follows:\n";
                    }
                    $description .= "Due to the $company_name policy for Vehicle Age Compliance, the Depreciation has been removed from your fixed reimbursement.\n";

                } elseif ($light_id & GREEN_NO_VEHICLE_COST_COMPLIANCE) {
                    $description .= "With no Vehicle Cost Compliance, your reimbursement will be paid non-taxable in grace period.\n";
                    $description .= "Subsequent reimbursements will be paid as follows:\n:";
                }
                $description .= "Per IRS Rules & Regulations your reimbursement will be compared against the IRS Standard Mileage Rate to determine if any reimbursement is taxable.\n";
            }

            \Debugbar::info('Status arr for ' . $light_id);
            \Debugbar::info($street_arr);
            foreach ($street_arr as $id => $name) {
                if ($id & $light_id) {
                    $reason .= $name . "\n";
                }
            }
            \Debugbar::info('REASON=' . $reason);

            $ret_arr[] = ['status' => $reason, 'impact' => $description];
        }
        return ['status_arr' => $ret_arr];
    }

    private function populate_note_for_WM($company_id)
    {

        \Debugbar::info("populate_note_for_WM for $company_id");
        $rec = CompanyPaymentDate::where('company_id', $company_id)
            ->where('year', date("Y"))->where('month', date("n"))
            ->select('driver_payment_date')->first();
        $this_month_payment_day = $rec->driver_payment_date;
        \Debugbar::info("Payment Day = " . $this_month_payment_day);

        $this_month = Carbon::createFromFormat('Y-m-d', $this_month_payment_day);
        $last_month = new Carbon();
        $last_month->subMonth();


        $txt = "NOTE: Fixed Reimbursements are paid for the current month, " . $this_month->format('F') .
            " is paid on " . $this_month->format('F jS') .
            ". Variable Reimbursements are paid for the past month, after you have submitted your business mileage, " .
            "e.g. " . $last_month->format('F') . " mileage is paid on " .
            $this_month->format('jS') . ".";
        return $txt;
    }

    public function getAddress(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/');
        }

        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();

        $address = $user->driverProfile->address();

        \Debugbar::info("Address:");
        \Debugbar::info($address);

        if (empty($address->longitude)) {

            $key = 'AIzaSyB4cVyUmAOKJC0foRNHAjROST06W7U8Iow';
            //TODO: get google key from env.
            $data = StateProvince::select('short_name')->find($address->state_province_id);
            $province = $data->short_name;
            $full_address = "$address->street, $address->city_name, $address->zip_postal_code, $province";
            $address_encoded = urlencode($full_address);

            \Debugbar::info("Address to get lat/lng: = " . $full_address);


            $url = "https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address={$address_encoded}&key={$key}";
            $response = file_get_contents($url);
            $json = json_decode($response, TRUE);
            \Debugbar::info('JSON');
            \Debugbar::info($json);
            if (isset($json['results'][0])) {
                $address->latitude = $json['results'][0]['geometry']['location']['lat'];
                $address->longitude = $json['results'][0]['geometry']['location']['lng'];
                $address->save();
                \Debugbar::info("lat/lng: = " . $address->latitude . ' / ' . $address->longitude);
            }
        }
        $address->stateProvince = StateProvince::find($address->state_province_id);

        return response()->json([
            'address' => $address
        ]);
    }

    /**
     * @param Request $request
     * @return boolean validated, string error_message
     */
    public function validateZipCode(Request $request)
    {
        $validated = true;
        $error_message = null;

        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $address = $user->driverProfile->address();
        if ($address->country_id == 1) { // USA
            $zipPostal = ZipCode::where('zip_code', $address->zip_postal);
            if ($zipPostal == null) {
                $validated = false;
                $error_message = 'Zip code is unknown. Please contact CarData coordinator';
            } else {
                if ($zipPostal->state_id != $address->state_province_id) {
                    $validated = false;
                    $error_message = 'Invalid Zip and State combination';
                }
            }
        } else { // Canada
            $zipPostal = PostalCode::where('postal_code', $address->zip_postal);
            if ($zipPostal == null) {
                $validated = false;
                $error_message = 'Postal code is unknown. Please contact CarData coordinator';
            } else {
                if ($zipPostal->province_id != $address->state_province_id) {
                    $validated = false;
                    $error_message = 'Invalid Postal Code and Province combination';
                }
            }
        }

        return response()->json([
            'validated' => $validated,
            'error_message' => $error_message
        ]);

    }

    /**
     * This function will insert a new personal vehicle
     *
     * TODO: this function needs to use the new updateDriverProfile trait!!!
     * TODO: this function also needs to create an irs annual odometer declaration if favr for the current year,
     *
     * @param Request $request
     */
    public function insertPersonalVehicle(Request $request, $registration = false)
    {
        \Debugbar::info('Insert Personal Vehicle called ...');
        // \Debugbar::info($request);


        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();


        $this->validate($request, [
            'year' => 'required|integer|min:1',
            'make_id' => 'required|integer|min:1',
            'model_id' => 'required|integer|min:1',
            'odometer' => 'required|integer'
        ],
            [
                'year.min' => 'Please Select Year',
                'make_id.min' => 'Please Select Make. If not found, use Trim',
                'model_id.min' => 'Please Select Model. If not found, use Trim'
            ]);


        $request['user_id'] = $user->id;
        \Debugbar::info($request);

        $currentVehicle = $user->driverProfile->personalVehicle();

        $vehicleData = $request->all();

        $vehicleData['trim_id'] = $vehicleData['trim_id'] == -1 ? null : $vehicleData['trim_id'];

        $newVehicle = PersonalVehicle::create($vehicleData);
        $driver_profile = DriverProfile::where('user_id', $newVehicle->user_id)->first();

        if ($registration == true) {
            // if in registration, simply update the personal vehicle and save the driver profile
            $driver_profile->personal_vehicle_id = $newVehicle->id;
            $driver_profile->save();

        } else {


            if ($newVehicle) {
                $currentVehicle->delete();
                $new_id = $newVehicle->id;
                $editing_user = Auth::user();

                // duplicate driver profile, set new personal vehicle and user id who made change, delete previous driver profile
                $new_driver_profile = $driver_profile->replicate();
                $new_driver_profile->personal_vehicle_id = $new_id;
                $new_driver_profile->editing_user_id = $editing_user->id;

                //todo
                // check if we supposed to change driver status ( company_insurances.
                // check if we have any restriction on car policy age ( company_options.enforce_vehicle_age, vehicle_profiles.max_vehicle_age )


                $new_driver_profile->save();
                $driver_profile->delete();

            }
        }


        return ['personalVehicle' => $newVehicle];

    }

    /**
     * This function will update a new personal vehicle
     *
     * @param Request $request
     */
    public function updatePersonalVehicle(Request $request, $registration = false)
    {

        $this->validate($request, [
            'year' => 'required|integer|min:1',
            'make_id' => 'required|integer|min:1',
            'model_id' => 'required|integer|min:1',
            'odometer' => 'required|integer'
        ],
            [
                'year.min' => 'Please Select Year',
                'make_id.min' => 'Please Select Make. If not found, use Trim',
                'model_id.min' => 'Please Select Model. If not found, use Trim'
            ]);

        $personalVehicle = PersonalVehicle::find($request->id);
        $personalVehicle->update($request->all());
        return ['personalVehicle' => $personalVehicle];
    }


    /**
     * Function to update (soft delete recreate behind the scenes) a driver's address.
     *
     * @param Request $request
     *      must contain an address object, with a structure defined by the database table addresses
     * @return array
     *      returns the new driverProfile and new address objects so that they can be committed to the store
     */
//    public function updateDriverAddress(Request $request, $registration = false) // removed davery - 7-27-18
    // can only pass through Request or params, not both
    public function updateDriverAddress(Request $request, $registration = false)
    {
         //dd($request->all());

        // VALIDATE NEW ADDRESS OBJECT
        try {
            $this->validateAddress($request->address);
        } catch (\Exception $e) {
            return response(
                $e->getMessage(), 400
            );
        }

        // $user_id = $request['address']['user_id']; // removed davery - 7-27-18
        $user_id = $request->user_id; // added davery - 7-27-18
        $registration = $request->registration; // added davery - 7-27-18

        $addressObj = [
            'user_id' => $user_id,
            'street' => $request['address']['street'],
            'city' => $request['address']['city'],
            'zip_postal' => $request['address']['zip_postal'],
            'state_province_id' => $request['address']['state_province_id'],
            'country_id' => $request['address']['country_id'],
        ];

        if (!empty($request['address']['street2'])) { // just in case it's not in the request
            $addressObj['street2'] = $request['address']['street2'];
        }

        //dd($addressObj);

        // CREATE NEW ADDRESS RECORD
        $newAddress = Address::create($addressObj);

        // load the stateProvince relation for data such as state_province.name and abbreviation
        $newAddress->stateProvince;

        /**
         * Calls trait to update the driver profile with the new address.
         * This trait takes care of soft deleting the old driver profile, as well as the old address
         *
         * updatedDriverProfile is just that, the updated driver profile
         */
        if ($registration != true) {
            $updatedDriverProfile = $this->updateDriverProfile(
                $user_id,
                [
                    'address_id' => $newAddress->id
                ]
            );
        } else {
            // we are in registration
            $updatedDriverProfile = DriverProfile::where('user_id', $user_id)->first();
            Address::find($updatedDriverProfile->address_id)->forceDelete();
            $updatedDriverProfile->address_id = $newAddress->id;
            $updatedDriverProfile->save();
        }


        /**
         * returns the driver profile, as well as the new address object.
         * both of these can now be committed to the store on the front end.
         */
        return ['driverProfile' => $updatedDriverProfile, 'address' => $newAddress];

    }

    private function validateGeoData($address)
    {
        \Debugbar::info('validateGeoData started ....');
        $city = $address['city'];
        $zip_postal = $address['zip_postal'];
        $state_province_id = $address['state_province_id'];
        $country_id = $address['country_id'];
        $zip_name = $country_id == 1 ? "Zip Code" : "Postal Code";
        $errors = [];
        $found_errors = false;

        \Debugbar::info('zip=' . $zip_postal . ' state=' . $state_province_id);

        $zip = $country_id == 1 ? ZipCode::find($zip_postal) : PostalCode::find($zip_postal);

        if (!$zip) {

            return ['errors' => true, 'message' => "Unknown $zip_name. Please contact CarData CSA."];

        } else {

            $zip_state_province_id = $country_id == 1 ? $zip->state_id : $zip->province_id;
            // check if zip, city and state match
            $found_match = false;
            if (strtolower($zip->city_name) == strtolower(trim($city)) && $zip_state_province_id == $state_province_id) {
                return ['errors' => $found_errors, 'message' => null];
            }

            if (!$found_match) { // check state
                if ($zip_state_province_id != $state_province_id) {
                    \Debugbar::error('Invalid State / Zip Code combination');
                    $errors[] = $country_id == 1 ? "Invalid State / Zip Code combination" : "Invalid Province / Postal Code combination";
                    $found_errors = true;
                }

                if (strtolower($zip->city_name) != strtolower(trim($city))) {
                    $errors[] = "Provided city: " . $city . " does not match city from this postal code: " . $zip->city_name;
                }
            }

            return ['errors' => $found_errors, 'message' => $errors];
        }
    }

    public function updateDriverNameAndEmail(Request $request)
    {
        \Debugbar::info($request);
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
        ]);

        $user = Auth::user();

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->save();

        return response()->json(['message' => 'success']);
    }

    public function updateMiRouteOdometer(Request $request)
    {
        $this->log->info('Update Miroute Odometer', $request->all());

        $data = (array)$request->all();
        $validator = Validator::make($data, [
            'starting_odometer' => 'required|numeric|min:1'
        ], ['starting_odometer.required' => 'Please enter starting odometer',
            'starting_odometer.min' => 'Odometer have to be greater than 0',
        ]);
        $validator->validate();


        $mileage = DailyMileage::find($request->id);
        $this->log->info('Daily Mileage for User', ['user_id' => $request->user_id, 'dailyMileage' => $mileage]);
        if (!$mileage) {
            $mileage = DailyMileage::create($request->all());
            $this->log->info('Created new Daily Mileage for User', ['user_id' => $request->user_id, 'dailyMileage' => $mileage]);
            // dd($mileage);
        } else {
            $mileage->update($request->all());

            if ($mileage->ending_odometer > 0) {
                $mileage->last_state = 'day_ended';
            }


            \Debugbar::info('Show me Mileage:');
            \Debugbar::info($mileage);
            // in case we have daily trip for this day, we have to recalculate personal mileage.
            $mileage->personal_mileage = max(0, $mileage->ending_odometer - $mileage->starting_odometer - $mileage->business_mileage);
            $mileage->gap_mileage = $this->getGapMileage($mileage->user_id, $mileage->trip_date, $mileage->starting_odometer);
            $mileage->save();
        }

        list($year, $month) = explode('-', $request->trip_date, -1);

        $this->updateMonthlyMileage($request->user_id, $year, $month);
    }

    private function getDrivingDistance($from, $to, $country = 1)
    {
        $lat1 = $from['latitude'];
        $long1 = $from['longitude'];
        $lat2 = $to['latitude'];
        $long2 = $to['longitude'];

        if ($lat1 == $lat2 && $long1 == $long2) {
            return 0;
        }

        // to avoid pointing to Africa west cost
        if ($lat1 * $lat2 * $long1 * $long2 == 0) {
            return 0;
        }

        // TO DO get google api key from configuration file
        //global $ga;
        //$key = $ga['google_api_key'];

        $key = 'AIzaSyB4cVyUmAOKJC0foRNHAjROST06W7U8Iow';

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&key=$key";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
        $dist *= $country == 1 ? 0.00062137 : 0.001; //converting meters to miles or km

        return round($dist, 2);
    }

    private function calculateTripMileage($trip)
    {
        $sequence_number = $trip->sequence_number;
        if ($sequence_number == 1) {
            return 0.0;
        } else {
            $to['latitude'] = $trip->savedStop->latitude;
            $to['longitude'] = $trip->savedStop->longitude;
            \Debugbar::info('stop geo ' . $to['latitude'] . ' ' . $to['longitude']);

            // get previous stop;
            $lastTrip = DailyTrip::
            where('daily_mileage_id', $trip->daily_mileage_id)
                ->where('sequence_number', '<', $sequence_number)
                ->orderBy('sequence_number', 'desc')
                ->first();
            if ($lastTrip) {
                $from['latitude'] = $lastTrip->savedStop->latitude;
                $from['longitude'] = $lastTrip->savedStop->longitude;

                return $this->getDrivingDistance($from, $to);  // from Google

            } else {
                return 0.0; // could not find previous trip
            }
        }
    }

    /**
     * @param Request $request : new trip data
     * @return array mixed business_mileage from all trips
     */
    public function insertNewTrip(Request $request)
    {
        //dd($request);

        $data = (array)$request;


        $this->validate($request, [
            'saved_stop_id' => 'required|integer|min:1',
            //'business_purpose' => 'required', // should be optional
        ],
            [
                'saved_stop_id.min' => 'Please Select Stop',
                //'business_purpose.required' => 'Please Enter Business Purpose'
            ]);

        /** Contract Use */
        if ($request->use_stop_contract == 1) {
            // validate business purpose
            if (!preg_match('/^\d{4}$/', $request->business_purpose)) {
                return response(json_encode(
                    ['business_purpose' => ['Invalid contract']]
                ), 400);
            }
            // if the contract was manually entered to a driver only stop, we do not associate the stop
        }

        $newTrip = new DailyTrip();
        $newTrip->fill($request->all());
        $daily_mileage_id = $newTrip->daily_mileage_id;
        $newTrip->mileage_original = 0;
        $newTrip->save();

        $google_distance = $this->calculateTripMileage($newTrip);
        $newTrip->mileage_original = $google_distance;
        $newTrip->save();

        return ['business_mileage' => $this->updateDailyMileageFromTips($daily_mileage_id)];
    }

    public function recalculateTrips(Request $request)
    {
        \Debugbar::info('recalculateTrips:');
        \Debugbar::info($request);

        //dd($request);

        $daily_mileage_id = $request['daily_mileage_id'];
        $dailyMileage = DailyMileage::find($daily_mileage_id);

        foreach ($request['drag_data'] as $drag) {
            \Debugbar::info($drag);
            if ($drag['from'] == null) {
                \Debugbar::info('set business mileage to 0 for trip ' . $drag['to']);
                $this->setUpStartingTrip($daily_mileage_id, $drag['to']);
            } else {
                \Debugbar::info('Call recalculateDistance with ' . $request['user'] . ' ' . $request['date'] . ' ' . $drag['from'] . ' ' . $drag['to']);
                $this->recalculateDistance($daily_mileage_id, $drag['from'], $drag['to']);
            }
        }

        // get ids with sequence before reorder:

        $trips = $dailyMileage->trips;
        $id_arr = [];
        foreach ($trips as $trip) {
            $id_arr[$trip->sequence_number] = $trip->id;
        }

        \Debugbar::info('Change Sequence');
        foreach ($request['sort_data'] as $newSort) {
            $this->changeTripSequence($id_arr[$newSort['from']], $newSort['to']);
        }

        \Debugbar::info('Update Daily from Trips');
        $this->updateDailyMileageFromTips($daily_mileage_id);

    }

    private function changeTripSequence($id, $to)
    {
        \Debugbar::info('changeTripSequence id=' . $id . " to " . $to);
        $trip = DailyTrip::find($id);
        $trip->sequence_number = $to;
        $trip->save();
    }

    private function setUpStartingTrip($daily_mileage_id, $sequence_number)
    {
        \Debugbar::info('setUpStartingTrip id=' . $daily_mileage_id . " s=" . $sequence_number);
        $trip = DailyTrip::where('daily_mileage_id', $daily_mileage_id)->where('sequence_number', $sequence_number)->first();
        $trip->business_purpose = 'STARTING DAY';
        $trip->mileage_original = 0;
        $trip->mileage_adjusted = 0;
        $trip->adjustment_comment = null;
        $trip->event = 'start';
        \Debugbar::info('before saving as 1st trip');
        \Debugbar::info($trip);
        $trip->save();
        \Debugbar::info('after saving as 1st trip');
        \Debugbar::info($trip);

    }

    private function recalculateDistance($daily_mileage_id, $from, $to)
    {
        //\Debugbar::info('recalculateDistance id=' . $daily_mileage_id . " from=" / $from . ' to=' . $to);
        $startTrip = DailyTrip::where('daily_mileage_id', $daily_mileage_id)->where('sequence_number', $from)->first();
        $endTrip = DailyTrip::where('daily_mileage_id', $daily_mileage_id)->where('sequence_number', $to)->first();

        $from_geo['latitude'] = $startTrip->savedStop->latitude;
        $from_geo['longitude'] = $startTrip->savedStop->longitude;
        $to_geo['latitude'] = $endTrip->savedStop->latitude;
        $to_geo['longitude'] = $endTrip->savedStop->longitude;

        $distance = $this->getDrivingDistance($from_geo, $to_geo);

        $endTrip->mileage_original = $distance;
        $endTrip->mileage_adjusted = null;
        $endTrip->adjustment_comment = null;
        \Debugbar::info('events: start=' . $startTrip->event . " end=" . $endTrip->event);
        if ($endTrip->event == 'start') {
            $endTrip->event = 'stopped';
            $endTrip->business_purpose = '... please enter business purpose ...';
        } elseif ($endTrip->event == 'end') {
            $endTrip->event = 'stopped';
        }
        $endTrip->save();

    }

    /**
     * @param Request $request : daily_trips.id
     * @return array mixed business_mileage from all trips
     */
    public function updateTrip(Request $request)
    {
        \Debugbar::info($request);

        $data = (array)$request->all();
        $validator = Validator::make($data, [
            'saved_stop_id' => 'required|integer|min:1',
            //'business_purpose' => 'required',
            'mileage_adjusted' => 'nullable|numeric',
        ],
            [
                'saved_stop_id.min' => 'Please Select Stop',
                //'business_purpose.required' => 'Please Enter Business Purpose',
                'mileage_adjusted.numeric' => 'Non numeric character found',
            ]);

        /** Contract Use */
        if ($request->use_stop_contract == 1) {
            // validate business purpose
            if (!preg_match('/^\d{4}$/', $request->business_purpose)) {
                return response(json_encode(
                    ['business_purpose' => ['Invalid contract']]
                ), 400);
            }
        }

        /*$validator->sometimes('adjustment_comment', 'required', function ($data) {
            return $data->mileage_adjusted != 0;
        });*/

        $validator->validate();

        $trip = DailyTrip::find($request->id);

        $old_saved_stop_id = $trip['saved_stop_id'];
        $old_mileage_adjustment = $trip['mileage_adjusted'];

        $updated_trip = $request->all();
        $trip->fill($updated_trip);
        $trip->save();

        $daily_mileage_id = $trip->daily_mileage_id;

        if ($old_saved_stop_id != $trip['saved_stop_id']) {

            // calculate distance to previous location
            $trip->mileage_original = $this->calculateTripMileage($trip);
            $trip->save();

            // update next trip if any
            $nextTrip = DailyTrip::
            where('daily_mileage_id', $trip->daily_mileage_id)
                ->where('sequence_number', '>', $trip->sequence_number)
                ->orderBy('sequence_number')
                ->first();
            if ($nextTrip) {
                $nextTrip->mileage_original = $this->calculateTripMileage($nextTrip);
                $nextTrip->save();
            }
            \Debugbar::info('DIFFERENT STOPS');
            return ['business_mileage' => $this->updateDailyMileageFromTips($daily_mileage_id)];

        } else if ($old_mileage_adjustment != $trip['mileage_adjusted']) {
            \Debugbar::info('DIFFERENT ADJUSTMENTS');
            return ['business_mileage' => $this->updateDailyMileageFromTips($daily_mileage_id)];

        } else {
            \Debugbar::info('NOTHING TO DO');
            return ['business_mileage' => null];  // means do not update business mileage on page
        }
    }

    /**
     * @param Request $request : trip_id
     * @return array
     */
    public function deleteTrip(Request $request)
    {

        \Debugbar::info($request);

        // delete trip with given id, it is a hard delete
        $trip = DailyTrip::find($request->id);
        $deleted_sequence_number = $trip->sequence_number;
        $daily_mileage_id = $trip->daily_mileage_id;
        $trip->delete();

        // decrease sequence number above given request by 1
        $res = DailyTrip::
        where('daily_mileage_id', $request->daily_mileage_id)
            ->where('sequence_number', '>', $request->sequence_number)
            ->update(['sequence_number' => \DB::raw('sequence_number - 1')]);

        // if first trip was deleted, change event to start, clear mileage_original, mileage_adjusted and comments
        if ($request->sequence_number == 1) {
            $firstTrip = DailyTrip::where('daily_mileage_id', $request->daily_mileage_id)
                ->where('sequence_number', 1)->first();
            if ($firstTrip) {
                $firstTrip->event = 'start';
                $firstTrip->business_purpose = 'STARTING DAY';
                $firstTrip->mileage_original = 0;
                $firstTrip->mileage_adjusted = null;
                $firstTrip->adjustment_comment = null;
                $firstTrip->save();
            }
        } else {
            // calculate distance for next trip if any
            $nextTrip = DailyTrip::where('daily_mileage_id', $request->daily_mileage_id)
                ->where('sequence_number', $deleted_sequence_number)->first();
            if ($nextTrip) {
                $nextTrip->mileage_original = $this->calculateTripMileage($nextTrip);
                $nextTrip->save();
            }
        }
        return ['business_mileage' => $this->updateDailyMileageFromTips($daily_mileage_id)];
    }

    /**
     * @param int $user_id
     * @param date $trip_date
     * @param int $starting_odometer
     * @return int
     */
    protected function getGapMileage($user_id, $trip_date, $starting_odometer)
    {

        $previous_odometer = DailyMileage::select('ending_odometer')
            ->where('user_id', $user_id)
            ->where('trip_date', '<', $trip_date)
            ->whereNotNull('ending_odometer')
            ->orderBy('trip_date', 'desc')->first();

        $gap_mileage = ($starting_odometer > 0 && $previous_odometer->ending_odometer > 0 &&
            abs($starting_odometer - $previous_odometer->ending_odometer) < 1000) ?
            $starting_odometer - $previous_odometer->ending_odometer : 0;

        \Debugbar::info('getGapMileage: u=' . $user_id . ' date=' . $trip_date . ' pr odo=' .
            $previous_odometer->ending_odometer . ' st odo=' . $starting_odometer .
            ' -> ' . $gap_mileage);

        return $gap_mileage;
    }

    public function updateDailyMileage(Request $request)
    {
        $mileage = DailyMileage::where('user_id', $request->user_id)->where('trip_date', $request->trip_date)
            ->update($request->all());
        if (!$mileage) {
            $mileage = DailyMileage::create($request->all());
        }


        /*
        // calculate gap mileage
        $previous_odometer = DailyMileage::select('ending_odometer')
            ->where('user_id',$request->user_id)
            ->where('trip_date','<',$request->trip_date)
            ->whereNotNull('ending_odometer')
            ->orderBy('trip_date','desc')->first();

        $gap_mileage = ( $request->starting_odometer > 0 && $previous_odometer->ending_odometer > 0 &&
                abs($request->starting_odometer - $previous_odometer->ending_odometer) < 1000 ) ?
            $request->starting_odometer - $previous_odometer->ending_odometer : 0;
        */

        $mileage = DailyMileage::where('user_id', $request->user_id)->where('trip_date', $request->trip_date)->first();
        $id = $mileage->id;

        $mileage->gap_mileage = $this->getGapMileage($request->user_id, $mileage->trip_date, $mileage->starting_odometer);
        $mileage->save();


        // generate trip record
        $trip = DailyTrip::where('daily_mileage_id', $id)->first();
        if (!$trip) {
            $trip = new DailyTrip();
        }
        $trip->daily_mileage_id = $id;
        $trip->sequence_number = 1;
        $trip->event = 'stopped';
        $trip->business_purpose = $request->business_purpose;
        $trip->mileage_original = empty($request->business_mileage) ? 0 : $request->business_mileage;

        $trip->save();

        // update monthly with sum of business mileage and last odometer
//        if( !is_null($request->ending_odometer) && !is_null($request->business_purpose)) {
//            list($year, $month) = explode('-', $request->trip_date, -1);
//            $this->updateMonthlyMileage($request->user_id, $year, $month);
//        }
        list($year, $month) = explode('-', $request->trip_date, -1);
        $this->updateMonthlyMileage($request->user_id, $year, $month);
    }

    /**
     * @param $daily_mileage_id
     * @return mixed business_mileage
     */
    public function updateDailyMileageFromTips($daily_mileage_id)
    {

        \Debugbar::info('updateDailyMileageFromTips');

        $res = \DB::table('daily_trips')
            ->select(\DB::raw('sum(round(mileage_original+ifnull(mileage_adjusted,0),1)) as total'))
            ->where('daily_mileage_id', $daily_mileage_id)->first();
        $business_mileage = round($res->total);

        // we round as we store it as integer in daily mileage. The same is with personal,

        \Debugbar::info('TOTAL MILEAGE: ' . $business_mileage);

        $dm = DailyMileage::find($daily_mileage_id);

        $dm->gap_mileage = $this->getGapMileage($dm->user_id, $dm->trip_date, $dm->starting_odometer);
        if ($dm->starting_odometer > 0 && $dm->ending_odometer > 0) {
            $dm->personal_mileage = max(0, round($dm->ending_odometer - $dm->starting_odometer - $business_mileage));
        } else {
            $dm->personal_mileage = 0;
        }
        \Debugbar::info('PERSONAL MILEAGE: ' . $dm->personal_mileage);
        $dm->business_mileage = $business_mileage;
        $dm->save();

        // update monthly
        $user_id = $dm->user_id;
        list($year, $month) = explode('-', $dm->trip_date, -1);
        $this->updateMonthlyMileage($user_id, $year, $month);

        return $business_mileage;
    }

    public function updateMonthlyMileage($user_id, $year, $month)
    {
        $from_date = sprintf("%4d-%02d-01", $year, $month);
        $next_month = $month == 12 ? 1 : $month + 1;
        $next_year = $month == 1 ? $year + 1 : $year;
        $to_date = sprintf("%4d-%02d-01", $next_year, $next_month);

        \Debugbar::info('updateMonthlyMileage from ' . $from_date . ' to ' . $to_date . " u=" . $user_id);

        $total_mileage = DailyMileage::
        where('user_id', $user_id)
            ->where('trip_date', '>=', $from_date)
            ->where('trip_date', '<', $to_date)->sum('business_mileage');

        $dataPerVehicle = $this->getMileagePerVehicleInternal($user_id, $year, $month);
        \Debugbar::info('$dataPerVehicle:');
        \Debugbar::info($dataPerVehicle);
        $total_personal = 0;
        foreach ($dataPerVehicle as $data) {
            $total_personal += $data['personal'];
        }
        \Debugbar::info('total_personal:');
        \Debugbar::info($total_personal);

        $query = 'SELECT ending_odometer, starting_odometer FROM daily_mileages WHERE user_id = ? AND trip_date >= ? ' .
            'AND trip_date < ? AND ( ending_odometer > 0 OR starting_odometer > 0 ) ORDER BY trip_date DESC LIMIT 1';
        $odometers = \DB::select($query, [$user_id, $from_date, $to_date]);

        $odometer = $odometers[0];
        $last_odometer = $odometer->ending_odometer > 0 ? $odometer->ending_odometer : $odometer->starting_odometer;

        $monthly = MonthlyMileage::
        where('user_id', $user_id)
            ->where('year', $year)
            ->where('month', $month)->first();

        if (!$monthly) {
            $monthly = new MonthlyMileage();
        }
        $monthly->user_id = $user_id;
        $monthly->year = $year;
        $monthly->month = $month;
        $monthly->business_mileage = $total_mileage;
        $monthly->ending_odometer = $last_odometer;
        $monthly->personal_mileage = $total_personal; // was $total_personal->total with original query

        $monthly->save();

    }

    public function updateDailyMileageWithText(Request $request)
    {   // no need to update monthly or personal

        \Debugbar::info($request);

        $mileage = DailyMileage::where('user_id', $request->user_id)->where('trip_date', $request->trip_date)->update($request->all());
        if (!$mileage) {
            $mileage = DailyMileage::create($request->all());
        }

        // calculate gap mileage
        $previous_odometer = DailyMileage::select('ending_odometer')
            ->where('user_id', $request->user_id)
            ->where('trip_date', '<', $request->trip_date)
            ->whereNotNull('ending_odometer')
            ->orderBy('trip_date', 'desc')->first();

        \Debugbar::info('previous odometer: ' . $previous_odometer->ending_odometer);

        $gap_mileage = ($request->starting_odometer > 0 && $previous_odometer->ending_odometer > 0 &&
            abs($request->starting_odometer - $previous_odometer->ending_odometer) < 1000) ?
            $request->starting_odometer - $previous_odometer->ending_odometer : 0;

        $mileage = DailyMileage::where('user_id', $request->user_id)->where('trip_date', $request->trip_date)->first();
        $mileage->gap_mileage = $gap_mileage;
        $mileage->save();

        \Debugbar::info('Mileage after seved:');
        \Debugbar::info($mileage);


        $mileage = DailyMileage::where('user_id', $request->user_id)->where('trip_date', $request->trip_date)->first();
        $id = $mileage->id;

        // generate trip record

        $trip = DailyTrip::where('daily_mileage_id', $id)->first();
        if (!$trip) {
            $trip = new DailyTrip();
        }
        $trip->daily_mileage_id = $id;
        $trip->sequence_number = 1;
        $trip->event = 'stopped';
        $trip->business_purpose = $request->business_purpose;
        if ($request->business_mileage == null) {
            $trip->mileage_original = 0;
        } else {
            $trip->mileage_original = $request->business_mileage;
        }
        $trip->save();

    }

    public function getDailyMileageData(Request $request)
    {
        $user_id = $request->user_id;
        $date = $request->date;

        // remove empty records
        DailyMileage::where('user_id', $user_id)
            ->whereNull('starting_odometer')
            ->whereNull('ending_odometer')
            ->delete();

        // last odometer
        $lastDailyMileage = DailyMileage::where('user_id', $user_id)
            ->whereNotNull('starting_odometer')
            ->where('starting_odometer', '>', 0)
            ->where('trip_date', '<', $date)
            ->orderBy('trip_date', 'desc')->first();

        if ($lastDailyMileage != null) {
            $last_date = $lastDailyMileage->trip_date;
            list($year, $month, $day) = explode('-', $last_date);
            $temp_date = mktime(0, 0, 0, $month, $day, $year);
            $last_date_formatted = date('M jS, Y', $temp_date);
            $last_state = $lastDailyMileage->last_state;

            if ($lastDailyMileage->ending_odometer > 0) {
                // this day was closed
                if ($lastDailyMileage->last_state != 'day_ended') {
                    $lastDailyMileage->last_state = 'day_ended';
                    $last_state = 'day_ended';
                    $lastDailyMileage->save();
                }
            } else {
                $lastDailyMileage->last_state = 'end_previous_day';
                $last_state = 'end_previous_day';
                $lastDailyMileage->save();
            }
        }

        // daily mileage record
        $currentDailyMileage = DailyMileage::where('user_id', $user_id)->where('trip_date', $date)->first();
        if (empty($currentDailyMileage)) {
            $currentDailyMileage = ['id' => -1, 'user_id' => $user_id, 'trip_date' => $date, 'last_state' => 'start_day', 'starting_odometer' => null, 'ending_odometer' => null];
        }

        return [
            'currentDailyMileage' => $currentDailyMileage,
            'lastDailyMileage' => $lastDailyMileage,
            'last_state' => $last_state,
            'last_date' => $last_date,
            'last_date_formatted' => $last_date_formatted
        ];
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDailyTrips(Request $request)
    {
        $daily_mileage_id = $request['daily_mileage_id'];
        $trips = DailyTrip::where('daily_mileage_id', $daily_mileage_id)->orderBy('sequence_number')->get();
        $trip_count = 0;

        //\Debugbar::info($trips);

        $last_destination_obj = null;
        $last_destination = null;
        $trip_items = count($trips);
        foreach ($trips as $trip) {
            if (!empty($trip->saved_stop_id)) {   // only Mi-Route trips have saved stop id
                //$trip->destination = !empty($last_destination) ? $last_destination . ' -> <strong>' .
                // $trip->savedStop->name . "</strong>" : "<strong>" . $trip->savedStop->name . "</strong>";

                if (empty($last_destination)) {
                    $trip->start_day = $trip->savedStop->name;
                } else {
                    $trip->stopping_point = $trip->savedStop->name;
                    $trip->starting_point = $last_destination;

                    if (!empty($last_destination_obj)) { // get lat & lng for Google Map
                        $trip->stopping_point_lat = $trip->savedStop->latitude;
                        $trip->stopping_point_lng = $trip->savedStop->longitude;

                        $trip->starting_point_lat = $last_destination_obj->latitude;
                        $trip->starting_point_lng = $last_destination_obj->longitude;
                    }

                }

                }
                if ($trip->event === 'end') {
                    $trip->end_day = $trip->savedStop->name;
                }

                $last_destination_obj = $trip->savedStop;
                $last_destination = $trip->savedStop->name;

                // change empty string of mileage adjusted to 0
                $trip->mileage_adjusted = empty($trip->mileage_adjusted) ? 0 : $trip->mileage_adjusted;

            $trip_count++;
        }

        //\Debugbar::info($trips);

        return response()->json(['trips' => $trips, 'trip_count' => $trip_count]);
    }


    public function getDestinationAddresses(Request $request)
    {
        \Debugbar::info($request);

        $offset = ($request->pagination_page - 1) * $request->page_size;

        $message = "";
        $stops = [];
        $data_size = 0;
        $destinationAddresses = [];

        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        //$addresses = DestinationAddress::where('driver_id',$user->id)->orderBy('name')->get();

        $where_clause = "WHERE ds.user_id = {$user->id}";
        if (!empty($request->search_param)) {
            $where_clause .= " and (ss.name like '%{$request->search_param}%' or ss.city like '%{$request->search_param}%' or ss.street like '%{$request->search_param}%')";
        }

        $query = <<<EOQ
SELECT ss.id AS `id`, ds.user_id AS `driver_id`, ss.name AS `name`, 
	ss.street AS `address`, ss.city, ss.zip_postal, ss.latitude, ss.longitude, 
	c.name AS `country`, sp.name AS `state`
FROM 
saved_stops ss
JOIN driver_stop ds ON ds.saved_stop_id = ss.id
JOIN state_provinces sp ON sp.id = ss.state_province_id
JOIN countries c ON c.id = ss.country_id
{$where_clause}
ORDER BY ss.name
EOQ;

        $destinationAddresses = \DB::select(\DB::raw($query));

        if ($request->use_stop_contract == 1) {
            foreach($destinationAddresses as $da) {
                $da->contracts = \DB::table('saved_stop_company_contract')
                    ->where('saved_stop_id', $da->id)
                    ->get();
            }
        }

        // get final count of stops
        $data_size = sizeof($destinationAddresses);

        // now get our window (return all for full_screen view)
        $addresses = $request->full_screen ? $destinationAddresses : array_slice($destinationAddresses, $offset, $request->page_size);

        $message = 'success';

        return ['message' => $message, 'destinationAddresses' => $addresses, 'data_size' => $data_size];
    }

    /**
     * Creates a destination address.
     */
    public function addDestinationAddress(Request $request)
    {
        $stop = new SavedStop;
        if($request->use_stop_contract == 1) {
            return $stop->createFromGoogleUsingContracts($request->all());
        } else {
            return $stop->createFromGoogle($request->all());
        }

    }

    public function renameDestinationAddress(Request $request)
    {

        \Debugbar::info('renameDestinationAddress:');
        \Debugbar::info($request);

        $this->validate($request, [
            'name' => 'required'
        ]);
        \Debugbar::info('passed validation:');

        // \Debugbar::info($request);
        SavedStop::find($request->id)->update($request->all());
        \Debugbar::info('updated:');

    }

    public function DestinationAddress(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state_province_id' => 'required|integer|min:1',
            'zip_postal' => 'required'
        ]);

    }


    /**
     * Return all personal vehicles for the authenticated user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPersonalVehicles($user_id)
    {
        $vehicles = PersonalVehicle::withTrashed()->where('user_id', $user_id)->orderBy('created_at', 'desc')->get();

        foreach ($vehicles as $vehicle) {
            $vehicle->odometer = number_format($vehicle->odometer);
            $vehicle->vehicle_cost = number_format($vehicle->vehicle_cost);
            $vehicle->old_vehicle_odometer = number_format($vehicle->old_vehicle_odometer);
            $vehicle->make = $vehicle->make_id > 0 ? \DB::table('vehicle_makes')->where('id', $vehicle->make_id)->pluck('name')[0] : '';
            $vehicle->model = $vehicle->model_id > 0 ? \DB::table('vehicle_models')->where('id', $vehicle->model_id)->pluck('name')[0] : '';
            $vehicle->trim = $vehicle->trim_id > 0 ? \DB::table('vehicle_trims')->where('id', $vehicle->trim_id)->pluck('name')[0] : '';
        }

        return response()->json([
            'vehicles' => $vehicles
        ]);
    }

    public function getPreviousPersonalVehicle()
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $vehicle = PersonalVehicle::withTrashed()->where('user_id', $user->id)->whereNotNull('deleted_at')->orderBy('created_at', 'desc')->first();
        if (!empty($vehicle)) {
            $vehicle->make = empty($vehicle->make_id) ? '' : \DB::table('vehicle_makes')->where('id', $vehicle->make_id)->pluck('name')[0];
            $vehicle->model = empty($vehicle->model_id) ? '' : \DB::table('vehicle_models')->where('id', $vehicle->model_id)->pluck('name')[0];
            $vehicle->trim = empty($vehicle->trim_id) ? '' : \DB::table('vehicle_trims')->where('id', $vehicle->trim_id)->pluck('name')[0];
        }

        \Debugbar::info($vehicle);

        return response()->json([
            'vehicle' => $vehicle
        ]);
    }

    /**
     * Returns a specific personal vehicle when given the personal vehicle id
     *
     * @param null $id
     *
     * NOTE!
     *   THIS FUNCTION RETURNS TWO OBJECTS IN THE JSON RESPONSE:
     *
     *      personalVehicle is in fact the personal vehicle
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPersonalVehicle($id = null)
    {

        if ($id > 0) {
            $personalVehicle = PersonalVehicle::withTrashed()->find($id);
            $personalVehicle->make_name = $personalVehicle->make != null ? $personalVehicle->make->name : 'Not Found. Please use Trim';
            $personalVehicle->model_name = $personalVehicle->model != null ? $personalVehicle->model->name : 'Not Found. Please use Trim';

        } else {
            $personalVehicle = new PersonalVehicle;
            $personalVehicle['id'] = -1;
            $personalVehicle['year'] = -1;
            $personalVehicle['make_id'] = -1;
            $personalVehicle['model_id'] = -1;
            $personalVehicle['odometer'] = null;
            $personalVehicle['make_name'] = '';
            $personalVehicle['model_name'] = '';
            $personalVehicle['vehicle_cost'] = 0;
            $personalVehicle['depreciation_method'] = null;
        }

        // get company attributres: favr
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $user_id = $user->id;
        $driver_profile = User::find($user_id)->driverProfile;

        //TODO: this is ugly and needs to be fixed, vehicleFavrLimits is BROKEN!, need data converted
        //
        if (Company::find($driver_profile->company->id)->options->service_plan == 'FAVR') {
            $favrParameters = $driver_profile->vehicleFavrParameters();
        } else {
            $favrParameters = [];
        }

        return response()->json(['personalVehicle' => $personalVehicle, 'favrParameters' => $favrParameters]);
    }

    public function getUserPersonalVehicle($user_id)
    {
        $personalVehicle = User::find($user_id)->driverProfile->personalVehicle;
        $message = '';
        if (!empty($personalVehicle)) {
            $personalVehicle->make = $personalVehicle->make->name;
            $personalVehicle->model = $personalVehicle->model->name;
            $message = 'success';
        } else {
            $personalVehicle = null;
            $message = 'error';
        }
        return ['personalVehicle' => $personalVehicle, 'message' => $message];
    }

    public function countPersonalVehicles()
    {

        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();

        $vehicles = PersonalVehicle::withTrashed()->where('user_id', $user->id)->count();

        return response()->json(['countPersonalVehicles' => $vehicles]);
    }


    public function getVehicleFavrParameters($user_id)
    {
        return ['favrParameters' => User::find($user_id)->driverProfile->vehicleFavrParameters()];
    }

    public function getDriverVehicleProfile()
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();

        $vehicle = VehicleProfile::with('vehicle', 'vehicle.category')->find($user->driverProfile->vehicle_profile_id);
        return response()->json([
            'vehicle' => $vehicle
        ]);

    }


    public function getDivisionName()
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        return response()->json([
            'division_name' => $user->driverProfile->division->name
        ]);
    }

    public function getAllDriverProfileData()
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $user_id = $user->id;

        $data = (object)[
            'user_id' => $user_id,
            'last_name' => $user->last_name,
            'first_name' => $user->first_name,
            'email' => $user->email,
            'company_id' => $user->driverProfile->company_id,
            'company_name' => $user->driverProfile->company->name,
            'employee_number' => $user->driverProfile->employee_number,
            'division' => $user->driverProfile->division->name,
            'car_policy_accepted' => $user->driverProfile->car_policy_accepted,
            'start_date' => $user->driverProfile->start_date,
            'stop_date' => $user->driverProfile->stop_date,
            'street' => $user->driverProfile->address()->street,              //TODO: store address once, no need for repeated address() queries
            'street2' => $user->driverProfile->address()->street2,
            'city' => $user->driverProfile->address()->city,
            'zip_postal' => $user->driverProfile->address()->zip_postal,
            'state' => $user->driverProfile->address()->stateProvince->name,
            'country_id' => $user->driverProfile->address()->country_id,
            'territory_type_id' => $user->driverProfile->territory_type_id,
            'territory_list' => $user->driverProfile->territory_list,
            'pv_year' => !empty($user->driverProfile->personalVehicle) ? $user->driverProfile->personalVehicle->year : '',
            'pv_make' => !empty($user->driverProfile->personalVehicle->make_id) ? $user->driverProfile->personalVehicle->make->name : '',
            'pv_model' => !empty($user->driverProfile->personalVehicle->model_id) ? $user->driverProfile->personalVehicle->model->name : '',
            'pv_trim' => !empty($user->driverProfile->personalVehicle) ? $user->driverProfile->personalVehicle->trim : '',
            'street_light_id' => $user->driverProfile->street_light_id,
            'vp_year' => !empty($user->driverProfile->vehicleProfile) ? $user->driverProfile->vehicleProfile->vehicle->year : '',
            'vp_category' => !empty($user->driverProfile->vehicleProfile->category_id) ? $user->driverProfile->vehicleProfile->vehicle->category->name : '',
            'vp_name' => !empty($user->driverProfile->vehicleProfile) ? $user->driverProfile->vehicleProfile->vehicle->name : '',
            'start' => $user->driverProfile->mileageBand->mileage_start,
            'end' => null, $user->driverProfile->mileageBand->mileage_end
        ];

        if ($data->territory_type_id == 1) {
            $data->territory = $data->city;
        } else if ($data->territory_type_id == 2) {
            $data->territory = $data->state;
        } else if ($data->territory_type_id == 3) {
            $arr = explode(',', $data->territory_list);
            $cities = \DB::table('fuel_cities as fc')
                ->select('fc.name', 'sp.short_name')
                ->join('state_provinces as sp', 'fc.state_province_id', '=', 'sp.id')
                ->whereIn('fc.id', $arr)
                ->orderBy('fc.name')->get();
            $terr_arr = [];
            foreach ($cities as $city) {
                $terr_arr[] = $city->name . ' ' . $city->short_name;
            }
            $data->territory = implode(', ', $terr_arr);
        } else if ($data->territory_type_id == 4) {
            $arr = explode(',', $data->territory_list);
            $states = StateProvince::whereIn('id', $arr)->select('name')->orderBy('name')->get();
            $terr_arr = [];
            foreach ($states as $state) {
                $terr_arr[] = $state->name;
            }
            $data->territory = implode(', ', $terr_arr);
        }

        if (!empty($data->street2)) {
            $data->combined_streets = $data->street . ' ' . $data->street2;
        } else {
            $data->combined_streets = $data->street;
        }

        $data->status = ucfirst($this->getDriverStatusColorAndDescriptions($data->street_light_id));

        return response()->json([
            'driverData' => $data
        ]);
    }

    /**
     * returns color and description of status street light id
     *
     * if status include 1 bit return yellow
     * assign green as color
     * if any of current color bit is marked as read, override green color with red.
     *
     * join descriptions of each bits with ', '
     *
     * @param $currentStatus
     * @return string
     */
    public function getDriverStatusColorAndDescriptions($currentStatus)
    {
        $statuses = DriverStreetLightStatus::all();

        $description = '';
        $separator = '';
        $color = 'green';

        if ($currentStatus & 1) {
            $color = 'yellow';
        }

        foreach ($statuses as $status) {
            if ((int)$currentStatus & $status->id) {
                $description = $description . $separator . $status->description;
                $separator = ', ';
            }
            if ($currentStatus > 2 && (int)$currentStatus & $status->id && $status->color == 'red') {
                $color = 'red';
            }
        }

        return ucwords($color . ' - ' . $description);
    }

    public function getDriverStops()
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $driver_stops = [];
        $stops = \DB::select(\DB::raw("SELECT ss.id, ss.name, ss.city, ds.* FROM saved_stops ss
  JOIN driver_stop ds ON ss.id = ds.saved_stop_id 
  WHERE ds.user_id = {$user->id} and ds.effective_date <= curdate()
  ORDER BY 2,3"));

        foreach ($stops as $stop) {
            $driver_stops[] = ['id' => $stop->id, 'name' => $stop->name . ", " . $stop->city];
        }

        return ['driver_stops' => $driver_stops];
    }

    public function getDriverTerritory()
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $territory_type_id = $user->DriverProfile->territory_type_id;
        if ($territory_type_id == 1) {
            $territory = $user->DriverProfile->Address()->city;
        } else if ($territory_type_id == 2) {
            $territory = $user->DriverProfile->Address()->getState()->name;
        } else if ($territory_type_id == 3) {
            $territory_arr = explode(',', $user->DriverProfile->territory_list);
            $cities = \DB::table('fuel_cities AS fc')->join('state_provinces as sp', 'sp.id', '=', 'fc.state_province_id')->select('fc.name', 'sp.short_name')->whereIn('fc.id', $territory_arr)->get();
            /*
            $cities = \DB::table('fuel_cities')
                ->join('state_provinces','state_provinces.id','=','fuel_cities.state_province_id')
                ->select('fuel_cities.name,state_provinces.short_name')
                ->whereIn('fuel_cities.id',[$territory_list])
                ->get();
            */
            $arr = [];
            foreach ($cities as $city) {
                $arr[] = $city->name . ' ' . $city->short_name;
            }
            $territory = implode(', ', $arr);
        } else {
            $territory = 'States';
        }
        return response()->json([
            'territory' => $territory
        ]);
    }

    public function getFuelCitiesAndPrices($average = null)
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $address = json_decode(json_encode($user->driverProfile->address()));
        $country_id = $address->country_id;
        $zip_postal = str_replace(' ', '', $address->zip_postal);
        $territory_type_id = $user->driverProfile->territory_type_id;
        $territoryList = $user->driverProfile->territory_list;

        //get the lat long of the driver's address (approximate)

        //America
        if ($country_id == 1) {
            $homeCoordinates = \DB::table('zip_codes')->where('zip_code', $zip_postal)
                ->select('longitude', 'latitude')
                ->first();
            //Canada
        } else {
            $homeCoordinates = \DB::table('postal_codes')->where('postal_code', $zip_postal)
                ->select('longitude', 'latitude')
                ->first();
        }

        $today = date("2016-10-10"); //TODO: MAKE TODAY'S DATE

        //is 1/2 the length/width of the square surrounding home coordinates in which we capture fuel cities
        $distance_in_miles = 400;

        $miles_per_degree_latitude = 69;
        $equatorial_length_one_degree_longitude = 69.172;

        $miles_per_degree_longitude = cos(deg2rad($homeCoordinates->latitude)) * $equatorial_length_one_degree_longitude;

        $latitude_bounds = $distance_in_miles / $miles_per_degree_latitude;
        $longitude_bounds = $distance_in_miles / $miles_per_degree_longitude;

        $max_latitude = $homeCoordinates->latitude + $latitude_bounds;
        $min_latitude = $homeCoordinates->latitude - $latitude_bounds;

        $max_longitude = $homeCoordinates->longitude + $longitude_bounds;
        $min_longitude = $homeCoordinates->longitude - $longitude_bounds;


        if ($average == null) {

            $fuelCities = \DB::table('fuel_cities')->join('fuel_prices', 'fuel_prices.fuel_city_id', '=', 'fuel_cities.id')
                ->selectRaw('*, null as driver_territory')
                ->where('fuel_cities.latitude', '>=', $min_latitude)
                ->where('fuel_cities.latitude', '<=', $max_latitude)
                ->where('fuel_cities.longitude', '>=', $min_longitude)
                ->where('fuel_cities.longitude', '<=', $max_longitude)
                ->where('fuel_cities.country_id', '=', $country_id)
                ->where('fuel_prices.updated_at', '=', $today)
                ->where('deleted_at', '=', null)
                ->get();
            //HOME CITY
            if ($territory_type_id == 1) {
                $closestFuelCity = new \stdClass;
                $min_distance = 10000;
                foreach ($fuelCities as $fuelCity) {
                    $fuelCity->driver_territory = false;
                    $distance_between_city_and_home =
                        sqrt(pow(($homeCoordinates->longitude - $fuelCity->longitude), 2)
                            + pow(($homeCoordinates->latitude - $fuelCity->latitude), 2)
                        );

                    if ($min_distance > $distance_between_city_and_home) {
                        $min_distance = $distance_between_city_and_home;
                        $closestFuelCity = $fuelCity;
                    }
                }
                $closestFuelCity->driver_territory = true;

                //HOME STATE
            } else if ($territory_type_id == 2) {
                foreach ($fuelCities as $fuelCity) {
                    if ($fuelCity->state_province_id = $address->state_province_id) {
                        $fuelCity->driver_territory = true;
                    } else {
                        $fuelCity->driver_territory = false;
                    }
                }
                //MULTI CITY
            } else if ($territory_type_id == 3) {
                $driverTerritoryFuelCities = \DB::table('fuel_cities')->join('fuel_prices', 'fuel_prices.fuel_city_id', '=', 'fuel_cities.id')
                    ->selectRaw('*, true as driver_territory')
                    ->whereRaw('fuel_cities.id in (' . $territoryList . ')')
                    ->where('fuel_prices.updated_at', '=', $today)
                    ->get();
                \Debugbar::info($driverTerritoryFuelCities);

                $fuelCities = $driverTerritoryFuelCities->merge($fuelCities);
                $fuelCities = $fuelCities->unique('name');

                //MULTI STATE
            } else {
                $driverTerritoryFuelCities = \DB::table('fuel_cities')->join('fuel_prices', 'fuel_prices.fuel_city_id', '=', 'fuel_cities.id')
                    ->selectRaw('*, true as driver_territory')
                    ->whereRaw('fuel_cities.state_province_id in (' . $territoryList . ')')
                    ->where('fuel_prices.updated_at', '=', $today)
                    ->get();


                \Debugbar::info($driverTerritoryFuelCities);

                $fuelCities = $driverTerritoryFuelCities->merge($fuelCities);
                $fuelCities = $fuelCities->unique('name');
            }


        } else if ($average != null) {
            //select average prices prices
            $last_day_previous_month = new DateTime();
            $last_day_previous_month->modify("last day of previous month");
            $last_day_previous_month->format('Y-m-d');

            $first_day_previous_month = new DateTime();
            $first_day_previous_month->modify("first day of previous month");
            $first_day_previous_month->format('Y-m-d');

            $fuelCities = \DB::table('fuel_cities')->join('fuel_prices', 'fuel_prices.fuel_city_id', '=', 'fuel_cities.id')
                ->selectRaw('fuel_cities.name as name, cast(AVG(fuel_prices.price) as decimal(19,3)) as price, 
                                fuel_cities.latitude as latitude, fuel_cities.longitude as longitude, null as driver_territory')
                ->where('fuel_cities.latitude', '>=', $min_latitude)
                ->where('fuel_cities.latitude', '<=', $max_latitude)
                ->where('fuel_cities.longitude', '>=', $min_longitude)
                ->where('fuel_cities.longitude', '<=', $max_longitude)
                ->where('fuel_cities.country_id', '=', $country_id)
                ->where('fuel_prices.updated_at', '>=', $first_day_previous_month)
                ->where('fuel_prices.updated_at', '<=', $last_day_previous_month)
                ->where('deleted_at', '=', null)
                ->groupBy(['fuel_cities.id', 'fuel_cities.name', 'fuel_cities.latitude', 'fuel_cities.longitude'])
                ->get();
            //HOME CITY
            if ($territory_type_id == 1) {
                $closestFuelCity = new \stdClass();
                $min_distance = 10000;
                foreach ($fuelCities as $fuelCity) {
                    $fuelCity->driver_territory = false;
                    $distance_between_city_and_home =
                        sqrt(pow(($homeCoordinates->longitude - $fuelCity->longitude), 2)
                            + pow(($homeCoordinates->latitude - $fuelCity->latitude), 2)
                        );

                    if ($min_distance > $distance_between_city_and_home) {
                        $min_distance = $distance_between_city_and_home;
                        $closestFuelCity = $fuelCity;
                    }
                }
                $closestFuelCity->driver_territory = true;

                //HOME STATE
            } else if ($territory_type_id == 2) {
                foreach ($fuelCities as $fuelCity) {
                    if ($fuelCity->state_province_id = $address->state_province_id) {
                        $fuelCity->driver_territory = true;
                    } else {
                        $fuelCity->driver_territory = false;
                    }
                }
                //MULTI CITY
            } else if ($territory_type_id == 3) {
                $driverTerritoryFuelCities = \DB::table('fuel_cities')->join('fuel_prices', 'fuel_prices.fuel_city_id', '=', 'fuel_cities.id')
                    ->selectRaw('fuel_cities.name as name, cast(AVG(fuel_prices.price) as decimal(19,3)) as price,
                                    fuel_cities.latitude as latitude, fuel_cities.longitude as longitude, true as driver_territory')
                    ->whereRaw('fuel_cities.id in (' . $territoryList . ')')
                    ->where('fuel_prices.updated_at', '>=', $first_day_previous_month)
                    ->where('fuel_prices.updated_at', '<=', $last_day_previous_month)
                    ->groupBy('fuel_cities.id')
                    ->get();
                \Debugbar::info($driverTerritoryFuelCities);

                $fuelCities = $driverTerritoryFuelCities->merge($fuelCities);
                $fuelCities = $fuelCities->unique('name');

                //MULTI STATE
            } else {
                $driverTerritoryFuelCities = \DB::table('fuel_cities')->join('fuel_prices', 'fuel_prices.fuel_city_id', '=', 'fuel_cities.id')
                    ->selectRaw('fuel_cities.name as name, cast(AVG(fuel_prices.price) as decimal(19,3)) as price,
                                    fuel_cities.latitude as latitude, fuel_cities.longitude as longitude, true as driver_territory')
                    ->whereRaw('fuel_cities.state_province_id in (' . $territoryList . ')')
                    ->where('fuel_prices.updated_at', '>=', $first_day_previous_month)
                    ->where('fuel_prices.updated_at', '<=', $last_day_previous_month)
                    ->groupBy('fuel_cities.id')
                    ->get();
                \Debugbar::info($driverTerritoryFuelCities);

                $fuelCities = $driverTerritoryFuelCities->merge($fuelCities);
                $fuelCities = $fuelCities->unique('name');
            }
        }

        //\Debugbar::info($fuelCitiesInTerritory);
        return response()->json([
            'fuelCities' => $fuelCities,
            'homeCoordinates' => $homeCoordinates
        ]);
    }


    public function getDriverContent()
    {
        return response()->json(['message', 'banana']);
    }

    /**
     * Returns the managers indexed by id to which a given driver is assigned.
     * The manager object is beefed up with a first_name and last_name from the associated user.
     * To get managers assigned to ALL drivers, call:
     *  /getDriverManagers/-1/company_id
     *
     * @param Request $request (contains a driver id) note the driver id is really the user_id
     * @return array of managerProfile objects (managers)
     *
     */
    public function getDriverManagers(Request $request)
    {
        // get all managers who are assigned all for a given company

        $manager_ids = \DB::table('manager_driver')
            ->select()
            ->where('driver_id', $request->driver_id)
            ->get()
            ->pluck('manager_id')
            ->toArray();

        $company_id = DriverProfile::where('user_id', $request->driver_id)->get()->pluck('company_id');
        if ($request->driver_id == -1) {
            $company_id = $request->company_id;
        }

        $managerRows = ManagerProfile::where('company_id', $company_id)->get();

        $managers = [];

        foreach ($managerRows as $managerRow) {
            if ($managerRow->assigned_drivers == 'all' || in_array($managerRow->manager_id, $manager_ids)) {
                $u = $managerRow->user;
                $managerRow->first_name = $u->first_name;
                $managerRow->last_name = $u->last_name;
                $managers[$managerRow->manager_id] = $managerRow;
            }
        }

        return response()->json([
            'managers' => $managers
        ]);
    }


    public function trips()
    {
        return view('driver.home')->with('message', 'This is the trips page');
    }

    public function mileage()
    {
        return view('driver.home')->with('message', 'This is the mileage page');
    }

    public function monthlyReimbursement()
    {
        return view('driver.home')->with('message', 'This is the monthly reimbursement page');
    }

    public function schedule()
    {
        return view('driver.home')->with('message', 'This is the schedule page');
    }

    public function policy()
    {
        return view('driver.home')->with('message', 'This is the policy page');
    }

    public function vehicle()
    {
        return view('driver.home')->with('message', 'This is the driver vehicle page');
    }

    public function insurance()
    {
        return view('driver.home')->with('message', 'This is the driver insurance vehicle page');
    }

    public function license()
    {
        return view('driver.home')->with('message', 'This is the driver license page');
    }

    public function address()
    {
        return view('driver.home')->with('message', 'This is the driver address page');
    }

    public function directPay()
    {
        return view('driver.home')->with('message', 'This is the driver direct pay page');
    }

    public function password()
    {
        return view('driver.home')->with('message', 'This is the driver password page');
    }

    public function email()
    {
        return view('driver.home')->with('message', 'This is the driver email page');
    }

    public function updateSessionVehicleMake(Request $request)
    {
        session()->put('session_make_id', $request->session_make_id);
        return response()->json(['session_make_id' => $request->session_make_id]);
    }

    public function updateSessionVehicleModel(Request $request)
    {
        session()->put('session_model_id', $request->session_model_id);
        return response()->json(['session_model_id' => $request->session_model_id]);
    }

    public function getMileageBands()
    {
        $mileageBands = \DB::table('mileage_bands')->get();
        return response()->json(['mileageBands' => $mileageBands]);
    }

    public function getTerritoryTypes()
    {
        $territoryTypes = \DB::table('territory_types')->get();
        return response()->json(['territoryTypes' => $territoryTypes]);
    }

    public function getDriverBanking($country_id = null, $masked = null)
    {

        /**
         * TODO
         *
         * note frank 2018-08-30
         * I am forcing masked if you use this request for now.
         */

        if (session()->has('impersonate')) {
            $user = User::find(session()->get('impersonate'));
            $banking = $user->banking($country_id, true); // force masked
        } else {
            $user = Auth::user();
            /**
             * HERE
             */
//            $banking = $user->banking($country_id, $masked); // allow choices
            $banking = $user->banking($country_id, true); // allow choices
        }

        \Debugbar::info($banking);
        return response()->json(['banking' => $banking]);
    }

    public function addDriverBanking(Request $request)
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $banking = $request;
        if ($banking->country_id == 1) {
            $banking->routing_number = encrypt($banking->routing_number);
        }

        $banking->account_number = encrypt($banking->account_number);

        \Debugbar::info($banking);

        if ($request->country_id == 1) {
            \DB::table('user_bank_accounts')->insert([
                'user_id' => $user->id,
                'country_id' => $banking->country_id,
                'account_number' => $banking->account_number,
                'routing_number' => $banking->routing_number,
                'account_type' => $banking->account_type,
            ]);
        } elseif ($request->country_id == 2) {
            \DB::table('user_bank_accounts')->insert([
                'user_id' => $user->id,
                'country_id' => $banking->country_id,
                'account_number' => $banking->account_number,
                'branch_number' => $banking->branch_number,
                'institution_number' => $banking->institution_number,
            ]);
        }

    }

    public function updateDriverBanking(Request $request)
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();

        $banking = $request;

        //encrypt
        if ($banking->country_id == 1) {
            $banking->routing_number = encrypt($banking->routing_number);
        }

        $banking->account_number = encrypt($banking->account_number);

        if ($request->country_id == 1) {
            \DB::table('user_bank_accounts')->where('user_id', $user->id)
                ->where('country_id', $request->country_id)
                ->update(['account_number' => $banking->account_number,
                    'routing_number' => $banking->routing_number,
                    'account_type' => $banking->account_type
                ]);
        } elseif ($request->country_id == 2) {
            \DB::table('user_bank_accounts')->where('user_id', $user->id)
                ->where('country_id', $request->country_id)
                ->update(['account_number' => $banking->account_number,
                    'branch_number' => $banking->branch_number,
                    'institution_number' => $banking->institution_number,
                ]);
        }

    }

    public function getMostRecentInsuranceReview($user_id)
    {
        return [
            'insuranceReview' => DriverInsuranceReview::where('user_id', $user_id)
                ->orderBy('id', 'desc')
                ->first()
        ];
    }

    /**
     * This function is called to reject a single insurance document.
     * For example if it was illegible.
     *
     * It will send a driver an email with the reasons as to why, and send a custom rejection
     * message if applicable
     *
     * It will return response 200 on success and 400 on error
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function rejectSingleInsuranceDocument(Request $request)
    {
        try {
            $updateDictionary = [];
            for ($i = 0; $i < sizeof($request->reasons); $i++) {
                $updateDictionary[$request->reasons[$i]['column']] = $request->reasons[$i]['value'];
            }
            $updateDictionary['approved'] = 0; // override the 2s
            $updateDictionary['processing_date'] = date('Y-m-d H:i:s');
        \DB::table('driver_insurance_documents')
            ->where('id', $request->document_id)
            ->update($updateDictionary);
        } catch(\Exception $e) {
            return response($e->getMessage(), 400);
        }


        /**
         * If all goes well, send an email
         */
        $email = User::find($request->user_id)->email;
        @Mail::to($email)->send(new InsuranceDocumentRejected((object)$request->all()));

    }

    /**
     * Gets a single driver insurance document based on the id
     * @param $document_id
     * @return array
     */
    public function getDriverInsuranceDocument($document_id) {
        return ['document' => DriverInsuranceDocument::find($document_id)];
    }

    public function getMostRecentLicenseReview($user_id)
    {

    }

    public function getDriverInsuranceProcessingDocuments($user_id)
    {
        $company_id = DriverProfile::where('user_id', $user_id)->first()->company_id;
        $username = User::find($user_id)->username;

        $url = $company_id . '/insurance';

        $files = Storage::disk('upload_docs')->allFiles($url);

        \Debugbar::error($files);

        if (count($files) > 0) {
            foreach ($files as $k => $v) {
                if (stripos($v, 'Ins') === false) {
                    unset($files[$k]);
                } elseif (stripos($v, $user_id . '_') === false) {
                    unset($files[$k]);
                }
            }
        }
        return ['files' => $files];
    }

    public function getDriverDocuments($doc_type, $user_id)
    {
        $driverDocuments = [];
        $latest_file_name = "";
        $company_id = DriverProfile::where('user_id', $user_id)->first()->company->id;
        $username = User::find($user_id)->username;

        /**
         * Assume no documents uploaded, if necessary we will change this
         */
        $url = $company_id . '/';
        if ($doc_type == 'insurance') {
            $latest_file_name = "No insurance on file";
            $url .= 'insurance';
        }
        if ($doc_type == 'license') {
            $latest_file_name = "No license on file";
            $url .= 'license';
        }

        /**
         * Pull all filenames from storage
         */
        $files = Storage::disk('upload_docs')->allFiles($url);

        /**
         * If we have files, loop through them.
         */
        if (count($files) > 0) {
            foreach ($files as $f) {
                // break the full filename apart by path /'s
                $parts = explode('/', $f);
                // the filename is the last element of the array
                $filename = array_pop($parts);
                /**
                 * format will be userid-Y-m-d.pdf
                 * Thus parts will be :
                 * [user_id, year, month, day.pdf]
                 *
                 * If the user id is not our user's id, skip the record
                 */
                $parts = explode('-',$filename);
                if ((int)$parts[0] != $user_id) {
                    continue;
                } else {
                    $driverDocuments[] = $filename;
                }
            }

            /**
             * Sort the driver documents
             *
             * http://php.net/natsort
             */
            natsort($driverDocuments);
            $driverDocuments = array_reverse($driverDocuments);
            $latest_file_name = $driverDocuments[0];
        }

        // finally, there may be no files, reset to empty array so .length will work on front end
        if (empty($driverDocuments)) {
            $driverDocuments = [];
        }
        return ['files' => $driverDocuments, 'latest_file_name' => $latest_file_name];
    }

    public function uploadDriverLicenseDocument(Request $request)
    {
        $user_id = $this->getFunctionalUser();
        $user = User::find($user_id);
        $username = $user->username;
        $company_id = DriverProfile::where('user_id', $user_id)->first()->company_id;

        $fileInfo = $request->file;
        $extension = $fileInfo->extension();


        // find all driver files for this driver
        $storage_dir = $company_id . '/license';
        $other_files = Storage::disk('upload_docs')->allFiles($storage_dir);
        $driver_file_count = 1;

        foreach ($other_files as $df) {
            $df = str_replace($storage_dir . '/', "", $df);

            if (stripos($df, "_" . $user_id . "_") !== false || stripos($df, '_' . $username . date('_Y_m_')) !== false) {
                $driver_file_count += 1;
            }
        }

        $date = date('_Y_m_');
        $file_name = $user_id . '_DL_' . $username . $date . $driver_file_count . '.' . $extension;

        $fileInfo->storeAs($storage_dir, $file_name);

    }

    /**
     * This function is called when an insurance doc is uploaded for a driver
     * If the document is NOT a pdf, it will be temporarily saved as an img, then converted to a pdf.
     * TODO we need front end to only accept jpeg, png, and pdf.
     *
     * It will create a an insurance review if appropriate
     *
     * It will not notify approvers, that is done on the front end by another call.
     * @param Request $request
     * @return string|void
     */
    public function uploadDriverInsuranceDocument(Request $request)
    {
        if (!$request->file) return;

        // get user info
        $user_id = $this->getFunctionalUser();
        $user = User::find($user_id);
        $username = $user->username;
        $driverProfile = $user->driverProfile;
        $company_id = $driverProfile->company_id;
        $date = date('Y-m-d');


        $fileInfo = $request->file;
        $extension = $fileInfo->extension();
        $this->log->info('Uploading insurance', [$fileInfo, $extension]);

        // define correct storage directory for company and type (insurance)
        $storage_dir = 'public/docs/' . $company_id . '/insurance';

        /**
         * Generate the correct filename following what is done on AddManyDrivers
         */
        $count = 0;
        do {
            $new_filename = $user_id . '-' . $date . ($count == 0 ? '' : '('.$count.')') . '.pdf';
            $existingFile = \DB::table('driver_insurance_documents')
                ->where('filename', $new_filename)
                ->first();
            $count++;
        } while(!empty($existingFile));

        /**
         * If the extension is not pdf, convert the img to pdf.
         * Create a dompdf package instance
         * Write up some bogus html
         * Render the html using dompdf
         * Output a pdf to a new file
         * delete the old file
         * make the file_name = new_file_name (the pdf)
         */
        if ( $extension != 'pdf') {
            // temporarily store the file if it is an image
            $tmp_filename = 'tmp' . '-' . $user_id . '.' . $extension;
            $fileInfo->storeAs($storage_dir, $tmp_filename);

            $img_path = base_path() . '/storage/app/public/docs/' . $company_id . '/insurance/';

            $dompdf = new \Dompdf\Dompdf();
            $html = "
        <html>
        <body>
        <img src='" . $img_path . $tmp_filename ."' style='max-width: 595px'>
        </body>
        </html>
        ";

            $dompdf->loadHtml($html);
            $dompdf->render();
            $output = $dompdf->output();
            $this->log->info('Save Path', ['path' => $img_path . $new_filename, $output]);
            Storage::disk('upload_docs')->put($company_id . '/insurance/'.$new_filename, $output);
            //file_put_contents($img_path . $new_filename, $output);

            // remove that img file if it exists
            Storage::delete($storage_dir . '/' . $tmp_filename);
        } else {
//            Storage::disk('upload_docs')->put($company_id . '/insurance/'. $new_filename, $fileInfo);
            $fileInfo->storeAs($company_id . '/insurance', $new_filename, 'upload_docs');
        }


        /**
         * Case No Insurance package:
         *     create package
         *     assign document to package
         *     assign package to driver profile
         * Case Package Exists
         *     if the package on driver profile is approved
         *         create a new package unapproved (if there isn't a package already queued up!)
         *         assign document to that package
         *         leave current package on driver profile until 'date of next review' (scheduled task will take care of this)
         *     else if the package on driver profile is unapproved
         *         assign document to that package
         */
        $existingReview = $driverProfile->insuranceReview;
        if (empty($existingReview)) {
            // create the new package
            $newReview = DriverInsuranceReview::create([
                'user_id' => $user_id,
                'company_id' => $company_id,
                'approved' => null,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            // create driver_insurance_document record
            $document = DriverInsuranceDocument::create([
                'filename' => $new_filename,
                'insurance_review_id' => $newReview->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            // assign package to the driver profile
            $newDriverProfile = $driverProfile->replicate();
            $driverProfile->delete();
            $newDriverProfile->insurance_review_id = $newReview->id;
            $newDriverProfile->save();

        } else { // review exists


            if ( $existingReview->approved == 1) {

                /**
                 * To ensure that the driver doesn't inadvertently create many new reviews by uploading
                 * many new documents when they have an existing one, we do a quick query to see if there is
                 * an existing review with created_at greater than the existing review!
                 */
                $newReview = \DB::table('driver_insurance_reviews')
                    ->where('user_id', $user_id)
                    ->where('created_at', '>', $existingReview->created_at)
                    ->first();

                if (empty($newReview)) {
                    $newReview = DriverInsuranceReview::create([
                        'user_id' => $user_id,
                        'company_id' => $company_id,
                        'approved' => null,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                }

                // create driver_insurance_document record and assign it to the correct review
                $document = DriverInsuranceDocument::create([
                    'filename' => $new_filename,
                    'insurance_review_id' => $newReview->id,
                    'created_at' => date('Y-m-d H:i:s')
                ]);

                // we do not assign the new package in this case

            } else { // review is unapproved, so we assign the new document to it

                $document = DriverInsuranceDocument::create([
                    'filename' => $new_filename,
                    'insurance_review_id' => $existingReview->id,
                    'created_at' => date('Y-m-d H:i:s')
                ]);

                // we set the review's approval back to null so it shows in awaiting processing
                $existingReview->approved = null;
                $existingReview->save();


                // the driver profile should have the review already attached.

            }
        }

        return $new_filename;
    }

    public function getDriverNotes($driver_id)
    {
        $notes = DriverProfile::where('user_id', $driver_id)->first()->notes();

        return ['notes' => $notes];
    }

    /**
     * Todo update this to conform with userid-Y-m-d.pdf standard
     * @param $doc
     * @param $user_id
     * @return mixed|string
     */
    protected function renameDriverDocument($doc, $user_id)
    {
        /*$name = '';
        $file_parts_count = count(explode('_', $doc));
        switch ($file_parts_count) {
            case 4:
                // this is already the new format
                $name = $doc;
                break;
            case 1:
                // this is the original production format

        }*/
        $doc_parts = explode('/', $doc);
        $file_name = array_pop($doc_parts);
        $name = $file_name; // must return as-is if we don't rename
        $location = implode('/', $doc_parts);
        $username = User::find($user_id)->username;
        $this->log->info("Renaming file", [$file_name]);

        if (stripos($name, $username) !== 0) {
            return $name;
        }
        // old format / new format

        // insurance: userId-YYYY-mm-num.pdf / Ins_username_YYYY_mm_dd_sequence.pdf
        if (stripos($file_name, $user_id) === 0) {
            $name_parts = explode('-', $file_name); // 0:userId, 1:year, 2:month, 3:sequence
            $name = $user_id . '_Ins_' . $username . '_' . $name_parts[1] . '_' . $name_parts[2] . '_' . $name_parts[3];

            \Storage::move($doc, $location . '/' . $name);
        } // license: DL_userId_sequence.pdf / DL_username_YYYY_mm_dd_sequence.pdf
        elseif (stripos($file_name, $user_id) === 3) {
            $name_parts = explode('_', $file_name); // 0:DL, 1:userId, 2:sequence
            $date = date('_Y_m_');
            $name = $user_id . '_DL_' . $username . $date . $name_parts[2];
            \Storage::move($doc, $location . '/' . $name);
        }
        return $name;
    }

    /**
     * Returns user desired for given functionality (signed in user, or user being impersonated)
     *
     * @return mixed
     */
    protected function getFunctionalUser()
    {
        return (empty(session()->get('impersonate')) || session()->get('impersonate') == -1) ? session()->get('authenticated_user_id') : session()->get('impersonate');
    }

    /**
     *
     * Finds changes in Driver Profiles and display them
     *
     * @param Request $request
     * @return array
     */
    public function getChangesInDriverProfile(Request $request)
    {

        // TODO: implement pro-rate here as well
        // TODO: display date format based on driver profile country.

        $found_changes = [];
        $search_columns = ['street_light_id', 'start_date', 'stop_date', 'address_id', 'vehicle_profile_id',
            'personal_vehicle_id', 'mileage_band_id', 'territory_type_id', 'territory_list'];

        $dpo = DriverProfile::withTrashed()->find($request->driver_profile_old);
        $dpn = DriverProfile::withTrashed()->find($request->driver_profile_new);
        \Debugbar::info('GET CHANGES FOR ' . $request->driver_profile_old . ' & ' . $request->driver_profile_new);
        \Debugbar::info($dpo);
        \Debugbar::info($dpn);

        if ($dpo && $dpn) {

            foreach ($search_columns as $column) {

                if ($dpo->$column != $dpn->$column) {

                    switch ($column) {
                        case 'address_id':
                            $found_changes[] = [
                                'name' => 'Address',
                                'old' => $this->formattedAddress($dpo->$column),
                                'new' => $this->formattedAddress($dpn->$column)];
                            break;
                        case 'mileage_band_id':
                            $found_changes[] = [
                                'name' => 'Mileage Band',
                                'old' => $this->formattedMileageBand($dpo->$column),
                                'new' => $this->formattedMileageBand($dpn->$column)];
                            break;
                        case 'vehicle_profile_id':
                            $found_changes[] = [
                                'name' => 'Vehicle Profile',
                                'old' => $this->formattedVehicleProfile($dpo->$column),
                                'new' => $this->formattedVehicleProfile($dpn->$column)];
                            break;
                        case 'territory_type_id':
                            $found_changes[] = [
                                'name' => 'Fuel Territory Type',
                                'old' => $this->formattedTerritoryType($dpo->$column),
                                'new' => $this->formattedTerritoryType($dpn->$column)];
                            break;
                        case 'territory_list':
                            $found_changes[] = [
                                'name' => 'Fuel Territory',
                                'old' => $this->formattedTerritoryList($dpo->territory_type_id, $dpo->$column),
                                'new' => $this->formattedTerritoryList($dpn->territory_type_id, $dpn->$column)];
                            break;

                        case 'start_date':
                            $found_changes[] = [
                                'name' => 'Start Date',
                                'old' => $this->formattedDate($dpo->start_date, "jS M, Y"),
                                'new' => $this->formattedDate($dpn->start_date, "jS M, Y")];
                            break;

                        case 'stop_date':
                            $found_changes[] = [
                                'name' => 'Stop Date',
                                'old' => $this->formattedDate($dpo->stop_date, "M jS, Y"),
                                'new' => $this->formattedDate($dpn->stop_date, "M jS, Y")];
                            break;

                        case 'street_light_id':
                            $found_changes[] = [
                                'name' => 'Driver Status',
                                'old' => explode(': ', $this->formattedStreetLight($dpo->street_light_id)),
                                'new' => explode(': ', $this->formattedStreetLight($dpn->street_light_id))];
                            break;
                    }
                }
            }

        }

        return ['changes' => $found_changes];
        /*
        \Debugbar::info("address id = " . $dpo->address_id );
        $address = $this->formattedAddress($dpo->address_id);
        \Debugbar::info("Address =" . $address );
        return ['changes' => $address];
        */

    }

    public function acceptCarPolicy()
    {
        if (session()->has('impersonate')) {
            return null; //should never reach this point.
        }

        $driverProfile = Auth::user()->driverProfile;
        $newDriverProfile = $driverProfile->replicate();
        $driverProfile->delete();
        $newDriverProfile->car_policy_accepted = true;
        $newDriverProfile->save();

        return ['message' => 'successful acceptance'];
    }

    public function getAnnualIrsOdometerDeclarationForCurrentYear()
    {
        $driverProfile = Auth::user()->driverProfile;

        return [
            'declaration' => $driverProfile->annualIrsOdometerDeclaration()
        ];
    }

    public function getAllAnnualIrsOdometerDeclarations()
    {
        $driverProfile = Auth::user()->driverProfile;

        return [
            'declarations' => $driverProfile->allAnnualIrsOdometerDeclarations()
        ];
    }

    public function addAnnualIrsOdometerDeclaration($odometer)
    {
        if (session()->has('impersonate')) {
            return null; //should never reach this point.
        }

        $driverProfile = Auth::user()->driverProfile;

        $minimum_vehicle_year = date('Y') - $driverProfile->vehicleProfile->max_vehicle_age;

        AnnualIrsOdometerDeclaration::create([
            'user_id' => $driverProfile->user_id,
            'personal_vehicle_id', $driverProfile->personal_vehicle_id,
            'odometer' => $odometer,
            'minimum_vehicle_year' => $minimum_vehicle_year,
        ]);

    }

    /**
     * Get color and description of streetlight
     * Get extra data if license or insurance
     * Use user_id not driver_profile_id as it is always available
     *
     * @param $user_id
     * @return array
     */
    public function getDriverStreetLightData($user_id)
    {
        return $this->streetLightCalculator->driverStreetLightData($user_id);
    }

    public function getDriverTimeZone($driver_id)
    {
        $address = User::find($driver_id)->driverProfile->address();
        $country_id = $address->country_id;
        \Debugbar::info($address);
        $zipPostal = $country_id == 1 ? ZipCode::find($address->zip_postal) : PostalCode::find(str_replace(' ', '', $address->zip_postal));
        $timeZone = TimeZone::find($zipPostal->time_zone_id);
        return ['timeZone' => $timeZone];
    }

    public function getDriverDivision($driver_id)
    {
        $division = DriverProfile::where('user_id', $driver_id)->first()->division;
        return ['division_id' => $division->id, 'division_name' => $division->name];
    }

    public function getDriverAttributes(Request $request)
    {
        $driverAttributes = [];

        $driverProfile = DriverProfile::where('user_id', $request->user_id)->first();

        foreach ($request['attributes'] as $k => $v) {
            $attributes[$v] = $driverProfile[$v];
        }
        return $attributes;
    }


    public function addComment($request)
    {
        try {
            $this->validateComment($request->comment);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        // add the comment and stuff man

    }


    public function updateComment($request)
    {
        try {
            $this->validateComment($request->comment);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        // update the comment and stuff man

    }

    public function getMileageByMonthForDriver($user_id, $year = -1, $mileage = 'business')
    {
        if ($year < 1) {
            $year = date('Y');
            $next_year = date('Y', strtotime('+1 year'));
        }

        $next_year = date('Y', strtotime('+1 year', strtotime($year)));

        $mileage_type = $mileage . '_mileage';

        if ($mileage === 'commuter') {
//            dd('commuter: '.$mileage);
            $monthlyMileage = \DB::select(\DB::raw(<<<EOQ
            select
            sum(mm.commuter_mileage) as `mileage`
            from daily_mileages mm
            where mm.user_id = {$user_id} and mm.trip_date >= '{$year}-01-01' AND mm.trip_date < '${next_year}-01-01'
EOQ
            ));
//            dd($monthlyMileage);
        } else {
            $monthlyMileage = \DB::select(\DB::raw(<<<EOQ
select
  mm.month, sum(mm.${mileage_type}) as `mileage`
from users u
join monthly_mileages mm on mm.user_id = u.id
where u.id = {$user_id} and mm.year = {$year}
group by mm.month
EOQ
            ));
        }
        return ['monthlyMileage' => $monthlyMileage];
    }

    public function checkIfCommuterDriver($user_id)
    {
        $commuter = User::find($user_id)->driverProfile->company->mileage->mileage_entry_method;
        return ['commuter' => $commuter];
    }

    public function insertDriverInsurancesRecord($user_id)
    {
        $driverProfile = User::find($user_id)->driverProfile;

        $record = DriverInsuranceReview::create([
            'user_id' => $user_id,
            'company_id' => $driverProfile->company_id,
            'next_review_date' => $driverProfile->next_isurance_review_date,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        $message = $record ? 'success' : 'fail';

        return ['message' => $message];
    }


    /**
     * This function will gather all data for the links to all insurance packages for a driver
     * This is used beneath the approval options on the individual insurance approval page
     */
    public function getAllInsuranceReviewLinks($user_id)
    {
        $reviews = DriverInsuranceReview::where('user_id', $user_id)->orderBy('created_at', 'desc')->get();

        $reviewBuckets = [
            'future' => null,
            'current' => null,
            'past' => []
        ];

        $index = 0;
        $future_review_exists = false;
        foreach($reviews as $review) {
            /**
             * trim down the review data to the essentials
             * id, document count, creation date
             */
            $tmpReview = new \stdClass();
            $tmpReview->id = $review->id;
            $tmpReview->document_count = $review->insurance_documents->count();
            $tmpReview->created_at = $review->created_at;
            if (!isset($review->approved)) {
                $tmpReview->status_message = 'Awaiting Processing';
            } else if ($review->approved == 1) {
                $tmpReview->status_message = 'Approved';
            } else {
                $tmpReview->status_message = 'Incomplete';
            }
            $tmpReview->approved = $review->approved;
            $review = $tmpReview;


            /**
             * if we are at the most recently created review, check if it is the active review (ie on driver profile)
             * otherwise there can only be one and we mark it as future review
             */
            if ( $index == 0) {
                if ( $review->id == DriverProfile::where('user_id', $user_id)->first()->insurance_review_id ) {
                    $reviewBuckets['current'] = $review;
                } else {
                    $future_review_exists = true;
                    $reviewBuckets['future'] = $review;
                }
            }

            /**
             * If we are at the first index and we have a future review, the 1st index must be current
             * otherwise it is a historical review so put it in the past bucket
             */
            if ($index == 1 && $future_review_exists) {
                $reviewBuckets['current'] = $review;
            } else if($index == 1) {
                $reviewBuckets['past'][] = $review;
            }

            /**
             * Finally for all reviews with indices >= 2 they must be put in the past bucket
             */
            if ($index >= 2) {
                $reviewBuckets['past'][] = $review;
            }

            $index++;
        }

        return ['reviewLinks' => $reviewBuckets];
    }

    /**
     * This function is called to load the data whenever a user goes to the
     * InsuranceApproval.vue page
     * @return array
     */
    public function getDriverInsurancesRecords()
    {

        /**
         * These are the records awaiting processing
         */
        $records = \DB::select(\DB::raw(<<<EOQ
SELECT u.id AS `user_id`, u.first_name, u.last_name, u.email
, c.id AS `company_id`, c.name AS `company_name`, dp.street_light_id
, dir.expiration_date, dir.id as insurance_review_id
FROM users u
JOIN driver_profiles dp ON dp.user_id = u.id
JOIN companies c ON c.id = dp.company_id
JOIN driver_insurance_reviews dir ON dir.user_id = u.id
WHERE dir.approved is null and dp.deleted_at is null
EOQ
        ));

        foreach ($records as $record) {
            // get driver's review where approved = null & first document of review where processing_date = null
            $review = DriverInsuranceReview::where('user_id', $record->user_id)->whereRaw('approved is null')->first();
            $latest_document = $review->insurance_documents->first();
            if ($latest_document) {
                $record->latest_document_uploaded = date('Y-m-d', strtotime($latest_document->created_at));
            }
            $calc = new StreetLightCalculator();
            $getStreetLight = $calc->driverStreetLightData($record->user_id);
            $record->streetLight = $getStreetLight[0]->color;
        }


        /**
         * Get drivers processed today
         */
        $tomorrow = date('Y-m-d H:i:s', strtotime('+1 day'));
        $yesterday = date('Y-m-d H:i:s', strtotime('-1 day'));
        $processedRecords = \DB::select(
            \DB::raw(<<<EOQ
SELECT u.id AS `user_id`, u.first_name, u.last_name, u.email
, c.id AS `company_id`, c.name AS `company_name`, dp.street_light_id
, dir.expiration_date, dir.id as insurance_review_id
FROM users u
JOIN driver_profiles dp ON dp.user_id = u.id
JOIN companies c ON c.id = dp.company_id
JOIN driver_insurance_reviews dir ON dir.user_id = u.id
WHERE dir.processing_date > '$yesterday' and dir.processing_date < '$tomorrow'
AND dp.deleted_at is null and dir.approved is not null
EOQ
)
        );

        /**
         * Loop through old records to format
         */
        foreach ($processedRecords as $processedRecord) {
            // get the processed review
            $review = DriverInsuranceReview::find($processedRecord->insurance_review_id);
            $latest_document = $review->insurance_documents->first();
            if ($latest_document) {
                $processedRecord->latest_document_uploaded = date('Y-m-d', strtotime($latest_document->created_at));
            }
            $calc = new StreetLightCalculator();
            $getStreetLight = $calc->driverStreetLightData($processedRecord->user_id);
            $processedRecord->streetLight = $getStreetLight[0]->color;
        }

        if (!$records) {
            $message = 'No driver insurance records';
        } else {
            $message = 'success';
        }

        return ['records' => $records, 'processedRecords' => $processedRecords, 'message' => $message];
    }

    /**
     * This function is called by IndividualInsuranceApproval.vue
     * It will get all the data required for the review from only the insurance review id!
     * If you need the first available package and want it from just a user_id, supply
     * insurance_review_id = -1 and a valid user id
     *
     * It will also return an added value:
     *
     *  [ 'editable' ] for ease of use on the front end
     *
     * If is editable is false, the front end will be locked (historical)
     * else it will be the current review / review in queue
     *
     * @param Request
     *    {
     *      insurance_review_id, user_id
     *    }
     * @return array
     */
    public function getDriverInsuranceApprovalRecord(Request $request)
    {
        $insurance_review_id = $request->insurance_review_id;
        $user_id = $request->user_id;

        /**
         * Get the review, the associated documents and the user
         */
        if ($insurance_review_id > 0) {
            $review = DriverInsuranceReview::find($insurance_review_id);
            $user = User::find($review->user_id);
            $user_id = $user->id;
        } else {
            $user = User::find($user_id);
            $insurance_review_id = $user->driverProfile->insurance_review_id;
            $review = DriverInsuranceReview::find($insurance_review_id);
        }

        $documents = $review->insurance_documents()->get();

        $editable = (
            empty($review->approved) // approved is null so its a future or present review
            || $review->id == $user->driverProfile->insurance_review_id // it is definitely the active approved or rejected review
        );

        /**
         * Get the most recent driver profile record for the given insurance review ID
         */
        $driverProfile = DriverProfile::withTrashed()
            ->where('user_id', $user->id)
            ->where('insurance_review_id', $insurance_review_id)
            ->orderBy('created_at', 'desc')
            ->first();

        /**
         * If the driver profile is empty,
         * there is an approved review on the driver profile.
         * They have uploaded a new insurance document.
         * A new review has been created, but it is not the active one
         * ie. its id is NOT on ANY driver profile yet
         * therefore we grab the current active driver profile,
         */
        if ( empty($driverProfile) ) {
            $driverProfile = $user->driverProfile;
        }

        /**
         * Get the personal vehicle associated with his review
         * Select personal vehicle, where the user id is this user id,
         * and the personal vehicle was created at before the review was created
         * and the deleted at is null or greater than when the review was created
         * (only 1 pv can be active at a time, so this was the pv active when the review was created)
         * in the case the user switched vehicles mid review then the new vehicle takes precedence
         * so we order by created at desc to get the first one
         */
        $q = <<<EOQ
created_at < '$review->created_at' and ( deleted_at > '$review->created_at' or deleted_at is null )
EOQ;
        $personalVehicle = PersonalVehicle::withTrashed()
            ->where('user_id', $user->id)
            ->whereRaw($q)
            ->orderBy('created_at', 'desc')
            ->first();
        /**
         * If we do not find one due to conversion issues with dates, try again.
         */
        if (empty($personalVehicle)) {
            $personalVehicle = PersonalVehicle::withTrashed()
                ->where('user_id', $user->id)
                ->orderBy('created_at', 'desc')
                ->first();
        }

        $make = !empty($personalVehicle) && $personalVehicle->make ? $personalVehicle->make->name : 'Unknown make';
        $model = !empty($personalVehicle) && $personalVehicle->model ? $personalVehicle->model->name : 'Unknown model';
        $year = !empty($personalVehicle) && $personalVehicle->year ? $personalVehicle->year : 'Unkown Year';
        $date_added = !empty($personalVehicle) && $personalVehicle->created_at ? date_format($personalVehicle->created_at, 'Y-m-d') : 'Not found';

        // Company Insurance
        $companyInsurance = $driverProfile->company->insurance;

        // Street Light Status
        $calc = new StreetLightCalculator();
        $streetLight = $calc->driverStreetLightData($user->id);


        $review->next_insurance_review_date = $this->getNextInsuranceReviewDate($driverProfile);

        return [
            'user' => [
                'user_id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'username' => $user->username
            ],
            'vehicle' => [
                'date_added' => $date_added,
                'make' => $make,
                'model' => $model,
                'year' => $year
            ],
            'streetLight' => [
                'color' => $streetLight[0]->color,
                'description' => $streetLight[0]->description
            ],
            'companyInsurance' => $companyInsurance,
            'review' => $review,
            'documents' => $documents,
            'editable' => $editable
        ];
    }


    /**
     * Private function will return the next insurance review date for a given driver
     * If the driver has a next insurance review date on their profile then that is returned
     * Otherwise we generate a default:
     *      for companies on annual renewal we go to the next occurence of the company defaults
     *      for companies on expiry we simply fill it with today's date. The csa will know what to do
     *
     * @param $driverProfile
     * @return $next_insurance_review_date
     */
    private function getNextInsuranceReviewDate($driverProfile)
    {
        $next_insurance_review_date = null;

        /**
         * Here we need to get the date of next review
         * If one is not present on the driver profile make it the company default
         */
        $company = Company::find($driverProfile->company_id);

        // if the company is annual
        if ( $company->insurance->insurance_review_interval == 'Annually' ) {

            // if the driver has no next review date set, go to the next company default!
            if ( !empty($driverProfile->next_insurance_review_date) ) {
                $next_insurance_review_date = $driverProfile->next_insurance_review_date;
            } else {
                $next_insurance_review_date = $this->getNextOccurenceOfMonthAndDay(
                    $company->insurance->insurance_review_month,
                    $company->insurance->insurance_review_day
                );
            }
        } else {
            // the company is on expiry
            if ( !empty($driverProfile->next_insurance_review_date) ) {
                $next_insurance_review_date = $driverProfile->next_insurance_review_date;
            } else {
                $next_insurance_review_date = date('Y-m-d');
            }
        }

        return $next_insurance_review_date;
    }

    public function updateSingleInsuranceDocument(Request $request)
    {
//        dd($request->all());
        \Debugbar::info($request->all());
        //dd($request->all());
        $documents = DriverInsuranceDocument::where('insurance_review_id', $request->review_id)->get();

//        dd($documents);
        \Debugbar::info($documents);

        $document = DriverInsuranceDocument::find($request->id);

        if ($request->columns) {
            foreach ($request->columns as $key => $value) {
                $document[$key] = $value;
            }
        } else {
            $document[$request->column] = $request->value;
        }

        // need to account for if cancel changes to remove all below
        $document->approved = 2;
        $document->processor_id = Auth::id();
        $document->processing_date = Carbon::now();



        if ($document->save()) {
            $message = 'success';
            /**
             * Reconcile all documents with the package
             */
            $this->reconcileInsuranceDocumentsAndReviewPackage($request->review_id);
            $documents = DriverInsuranceDocument::where('insurance_review_id', $request->review_id)->get();
        } else {
            $message = 'fail';
            $documents = null;
        }



        return ['message' => $message, 'documents' => $documents];
    }

    public function updateMultipleInsuranceDocuments(Request $request)
    {
        dd($request->all());
        $object['review_id'] = $request->review_id;
        $object['columns'] = [];

        foreach ($request->changes as $change) {
            $object['id'] = array_shift($change);
            $object['columns'] = $change;
            $this->updateSingleInsuranceDocument(new Request($object));
        }
    }

    /**
     * This function is triggered by the 'Mark as Incomplete' or the 'Approve' button
     * on the insurance processing page
     *
     * It will either approve or reject a package
     * If the package is the current active package we will
     *      calculate street light
     *      create a new driver profile
     *      reset reimbursement details id to null (force the reimbursement calculation to go on in the future)
     *
     * otherwise there is an approved package on the driver profile at the present time
     * if the new package is approved :
     *      calculate street light
     *      create a new driver profile
     *      reset reimbursement details id to null (force the reimbursement calculation to go on in the future)
     * else the new package is rejected:
     *      we do not want to throw the driver with an active valid package to red light
     *      so we let the scheduled task take care of it in this case.
     *      the scheduled task will change the active package at the correct time, and set street light
     *      or perhaps he submits an extra document and then it gets approved
     *      simply reject the package
     *
     *  NOTE only when a package is accepted do we update the date_of_next_review on the driver profile.
     *       otherwise a scheduled task will take care of managing it.
     *      ex. on date of next review if it runs (driver hadn't submitted docs in time) a new review will be
     *          created and the driver will have a status change and the date of next review will be set to
     *          company default / 1 year out from the old one
     *
     * When insurance is rejected, the rejection email from resources/views/email/insurance_rejected.blade.php
     * If insurance is approved we send insurance_approved.blade.php
     *
     * @param Request $request
     *
     *  request = {
     *      insurance_review_id,
     *      user_id,
     *      approved (bool 1 = approve, 0 = reject),
     *      next_insurance_review_date
     *  }
     * @return array
     */
    public function processInsurance(Request $request)
    {

        try {
            /**
             * Gather the review, the processor id, and the date
             * Set the review ready to save
             * Determine whether this is the current active package
             */
            $review = DriverInsuranceReview::find($request->insurance_review_id);
            $processor_id = Auth::id();
            $now = date('Y-m-d H:i:s');
            $driverProfile = DriverProfile::where('user_id', $request->user_id)->first();
            $company = Company::find($driverProfile->company_id);
            $is_active_review = $driverProfile->insurance_review_id == $request->insurance_review_id;

            $review->approved = $request->approved;
            $review->approved_by = $processor_id;
            $review->processing_date = $now;
            $review->save();

            if ($request->approved) {
                // remove any insurance related status
                $new_street_light_id = $this->streetLightCalculator->calculateStreetLightIdInsuranceCompliance(
                    $driverProfile->street_light_id,
                    $company->options->service_plan,
                    $company->insurance->remove_reimbursement_without_insurance,
                    true,
                    true,
                    false
                );

                // whatever package was approved becomes the active package, so replicate old driver profile
                $newDriverProfile = $driverProfile->replicate();
                $driverProfile->delete();

                // update the driver profile if the new status != old status and then set reimb detail id = null
                if ($driverProfile->street_light_id != $new_street_light_id) {
                    $newDriverProfile->street_light_id = $new_street_light_id;
                    $newDriverProfile->reimbursement_detail_id = null;
                }

                $newDriverProfile->insurance_review_id = $review->id;
                $newDriverProfile->next_insurance_review_date = $request->next_insurance_review_date;
                $newDriverProfile->save();


            } else {

                // review rejected
                if ($is_active_review) {

                    // add insurance related status, remove awaiting if applicable
                    $new_street_light_id = $this->streetLightCalculator->calculateStreetLightIdInsuranceCompliance(
                        $driverProfile->street_light_id,
                        $company->options->service_plan,
                        $company->insurance->remove_reimbursement_without_insurance,
                        true,
                        false,
                        false
                    );

                    // update the driver profile if the new status != old status and then set reimb detail id = null
                    if ($driverProfile->street_light_id != $new_street_light_id) {
                        $newDriverProfile = $driverProfile->replicate();
                        $driverProfile->delete();
                        $newDriverProfile->street_light_id = $new_street_light_id;
                        $newDriverProfile->reimbursement_detail_id = null;
                        $newDriverProfile->save();
                    }
                    // otherwise the status hasn't changed, its just another rejected insurance

                }
                /**
                 * else the new package is rejected:
                 *      we do not want to throw the driver with an active valid package to red light
                 *      so we let the scheduled task take care of it in this case.
                 *      the scheduled task will change the active package at the correct time, and set street light
                 *      or perhaps he submits an extra document and then it gets approved
                 *      simply reject the package
                 * DO NOTHING
                 */
            }
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        /**
         * Send the emails
         */
        $this->sendInsuranceProcessedEmail($driverProfile->user_id, $request->insurance_review_id);

        return ['review' => $review];
    }

    /**
     * The following method is called after an insurance is processed.
     *
     * It will send a rejection, or acceptance email to the driver.
     * If insurance was accepted, and street light was changed, the driver will be alerted of that fact.
     *
     * @param $user_id
     * @param $insurance_review_id
     * @param $old_street_light_id
     * @param $new_street_light_id
     */
    private function sendInsuranceProcessedEmail($user_id, $insurance_review_id)
    {
        // this is the data object passed to the email blade template
        $data = new \stdClass();

        // get the user we processed and his company
        $data->user = User::find($user_id);
        $company = Company::find($data->user->driverProfile->company_id);
        $data->company_name = $company->name;

        // get the review that was processed
        $insuranceReview = DriverInsuranceReview::find($insurance_review_id);

        // get the coordinator
        $data->coordinator = $this->getCoordinatorData($company->id);

        // gather all company insurance minimums
        $data->overallInsuranceRequirements = $this->getCompanyInsuranceAmount($company->id, $company->profile->country_id);


        /**
         * If the insurance was approved, gather the send the approval email
         * Else it was rejected, send the rejection email
         */
        if ($insuranceReview->approved) {

            // gather street light data
            $data->streetLight = new \stdClass();
            $data->streetLight->data = $this->streetLightCalculator->driverStreetLightData($user_id);
            $data->streetLight->color = $this->streetLightCalculator->colorOfStreetLight($data->user->driverProfile->street_light_id);
            $data->service_plan = $company->options->service_plan;

            @Mail::to($data->user->email)->send(new InsuranceAcceptanceEmail($data));

        } else {

            /**
             * Filter all of the company's insurance requirements against which booleans are checked on the
             * insurance review object.
             */
            $data->unmetInsuranceRequirements = array_filter(
                $data->overallInsuranceRequirements, function($req) use ($insuranceReview) {
                    return $insuranceReview->{$req->database_column_name} == false;
            });

            @Mail::to($data->user->email)->send(new InsuranceRejectionEmail($data));
        }

    }

    /**
     * This PRIVATE function will be called server side any time a document is updated
     * It will reconcile all booleans on the documents using OR operations
     *
     * @param $insurance_review_id
     */
    private function reconcileInsuranceDocumentsAndReviewPackage($insurance_review_id)
    {

        /**
         * Get the review and the documents
         */
        $review = DriverInsuranceReview::find($insurance_review_id);
        $documents = DriverInsuranceDocument::where('insurance_review_id', $insurance_review_id)->get()->toArray();

        /**
         * Sorry!
         */
        $fields_to_reconcile = [
            'bodily_liability_per_person',
            'bodily_liability_per_accident',
            'property_damage',
            'deductible',
            'comprehensive_deductible',
            'collision_deductible',
            'medical',
            'uninsured_per_person',
            'uninsured_per_accident',
            'public_liability',
            'business_use',
            'no_policy_term_dates',
            'vehicle_match',
            'document_illegible',
            'document_current',
            'name_missing',
            'other',
        ];

        /**
         * loop through fields to reconcile
         * set requirement met to default of false
         * loop through docs
         * if its met, by this doc, or some previous doc, set it to true,
         * finally let the review's requirement = whether or not the requirement was met
         * save it
         */
        foreach($fields_to_reconcile as $insurance_requirement) {
            $requirement_met = false;
            foreach($documents as $doc) {
                $requirement_met = $doc[$insurance_requirement] || $requirement_met;
            }
            $review->{$insurance_requirement} = $requirement_met;
        }
        $review->save();
    }


    public function driverDivision($id)
    {
        $division = \App\Division::find($id);
        return response()->json(['division' => $division]);
    }

    public function driverAddress($id)
    {
        $address = \App\Address::find($id);
        return response()->json(['address' => $address]);

    }

    public function getDriverRegistrationCompletion($user_id) {
        $driverProfile = DriverProfile::where('user_id', $user_id)->first();

        return empty($driverProfile) ? null : $driverProfile->registrationCompletion;
    }

    public function completeDriverRegistrationModule(Request $request)
    {
        $user_id = Auth::user()->id;

        $registrationCompletion = DriverRegistrationCompletion::where('user_id', $user_id)->first();

        if (empty($registrationCompletion)) {
            $registrationCompletion = DriverRegistrationCompletion::create([
                'user_id' => $user_id
            ]);
        }

        $registrationCompletion->update([
            (string)$request->module_column_name => true
        ]);

    }

    public function completeDriverRegistration(Request $request)
    {
        $driverProfile = DriverProfile::where('user_id', $request->user_id)->first();
        $company = Company::find($driverProfile->company_id);
        $modules = $request->modules;

        $current_year = Carbon::today()->year;

        /**
         * Todo:
         * we can change this at a later date, but for now I am going to calculate street light based
         * on data from the modules passed AND data from the database just to get this ticket done yo.
         * I have no idea how insurance is being processed, but if we hide yellow light drivers from the
         * processing page I can simply check whether review exists for a given driver to see if he uploaded docs.
         */
        $new_street_light_id = $driverProfile->street_light_id;

        $new_street_light_id = $this->streetLightCalculator->removeFromStreetLight($new_street_light_id, YELLOW_NEW_DRIVER);

        // Banking
        if ($modules['driver_banking']['using'] == true) {
            if ($modules['driver_banking']['completed'] != true) {
                $new_street_light_id = $this->streetLightCalculator->addToStreetLight($new_street_light_id, GREEN_NO_BANKING_DATA);
            }
        }

        // Insurance
        if ($company->options->use_company_insurance_module == true) {
            $firstInsuranceReview = \DB::table('driver_insurance_reviews')
                ->where('user_id', $request->user_id)
                ->first();

            if (empty($firstInsuranceReview)) {
                //driver has not supplied insurance
                if ($company->options->service_plan == 'FAVR') {
                    // FAVR
                    $new_street_light_id = $this->streetLightCalculator->addToStreetLight($new_street_light_id, GREEN_NO_INSURANCE_COMPLIANCE);
                } else {
                    $new_street_light_id = $this->streetLightCalculator->addRedStatus($new_street_light_id, RED_AWAITING_INSURANCE);
                }
            } else {
                // driver has supplied insurance
                if ($company->options->service_plan == 'FAVR') {
                    // FAVR
                    $new_street_light_id = $this->streetLightCalculator->addToStreetLight($new_street_light_id, GREEN_INSURANCE_AWAITING_PROCESSING);
                } else {
                    $new_street_light_id = $this->streetLightCalculator->addRedStatus($new_street_light_id, RED_INSURANCE_AWAITING_PROCESSING);
                }
            }
        }

        // License TODO

        if ($company->options->use_company_license_module == true && Schema::hasTable('driver_license_reviews')) {
            $firstInsuranceReview = \DB::table('driver_license_reviews')
                ->where('user_id', $request->user_id)
                ->first();

            if (empty($firstInsuranceReview)) {
                //driver has not supplied insurance
                if ($company->options->service_plan == 'FAVR') {
                    // FAVR
                    $new_street_light_id = $this->streetLightCalculator->addToStreetLight($new_street_light_id, GREEN_NO_INSURANCE_COMPLIANCE);
                } else {
                    $new_street_light_id = $this->streetLightCalculator->addRedStatus($new_street_light_id, RED_AWAITING_INSURANCE);
                }
            } else {
                // driver has supplied insurance
                if ($company->options->service_plan == 'FAVR') {
                    // FAVR
                    $new_street_light_id = $this->streetLightCalculator->addToStreetLight($new_street_light_id, GREEN_INSURANCE_AWAITING_PROCESSING);
                } else {
                    $new_street_light_id = $this->streetLightCalculator->addRedStatus($new_street_light_id, RED_INSURANCE_AWAITING_PROCESSING);
                }
            }
        }


        /** Personal Vehicle */
        // Vehicle Age
        if ($company->vehicles->enforce_vehicle_age) {
            $new_street_light_id = $this->streetLightCalculator->calculateStreetLightIdVehicleAgeCompliance(
                $new_street_light_id,
                $company->options->service_plan,
                $driverProfile->vehicleProfile->max_vehicle_age,
                $current_year - $driverProfile->personalVehicle->year
            );
        }

        if ($company->options->service_plan == 'FAVR') {
            // VEHICLE COST COMPLIANCE
            $current_vehicle_cost = $driverProfile->personalVehicle->vehicle_cost;
            $minimum_vehicle_cost = $driverProfile->vehicleFavrParameters()['minimum_vehicle_cost'];
            $vehicle_meets_minimum_cost_requirement = $current_vehicle_cost >= $minimum_vehicle_cost;
            $new_street_light_id = $this->streetLightCalculator->calculateStreetLightIdVehicleCostCompliance($new_street_light_id, $vehicle_meets_minimum_cost_requirement);

            // DEPRECIATION COMPLIANCE
            $depreciation_is_compliant = $driverProfile->personalVehicle->depreciation_method == 1 || empty($driverProfile->personalVehicle->depreciation_method);
            $new_street_light_id = $this->streetLightCalculator->calculateStreetLightIdDepreciationCompliance($new_street_light_id, $depreciation_is_compliant);
        }

        $newDriverProfile = $driverProfile->replicate();
        $driverProfile->delete();
        $newDriverProfile->street_light_id = $new_street_light_id;
        $newDriverProfile->save();

        return;
    }

    public function getLoginHistory(Request $request)
    {
        $company_id = $request->company_id;
        $division_id = $request->division_id;
        $driver_id = $request->driver_id;
        $from_date = $request->from_date;
        $to_date = $request->to_date;

        $company_where = $company_id > 0 ? " AND c.id = $company_id" : '';
        $division_where = $division_id > 0 ? " AND di.id = $division_id " : '';
        $driver_where = $driver_id > 0 ? " AND u.id = $driver_id " : '';

        $records = \DB::select(\DB::raw(<<<EOQ
SELECT l.date_time, u.last_name, u.first_name, di.name AS division, l.login_type, l.access, c.name as company
  FROM logins l, users u, driver_profiles dr, divisions di, companies c
  WHERE l.username = u.username AND u.id = dr.user_id AND dr.deleted_at IS NULL 
  AND di.id = dr.division_id
  AND dr.company_id = c.id
  $company_where $division_where $driver_where
  AND l.date_time >= '$from_date' AND l.date_time <= '$to_date'
  ORDER BY 1 DESC,2,3
EOQ
        ));

        \Debugbar::info($records);

        return ['records' => $records];

    }

    public function endPreviousDay(Request $request)
    {
        if ($request->daily_mileage_id > 0) {
            $lastDailyMileage = DailyMileage::find($request->daily_mileage_id)->update([
                'ending_odometer' => $request->ending_odometer,
                'last_state' => 'day_ended'
            ]);
        }
    }
}
