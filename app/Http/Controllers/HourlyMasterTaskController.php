<?php
/**
 * Created by PhpStorm.
 * User: Jerzy 15
 * Date: 7/25/2018
 * Time: 12:53 PM
 */

namespace App\Http\Controllers;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use App\Http\Traits\CommonTraits;
use Illuminate\Support\Facades\Storage;

class HourlyMasterTaskController extends Controller
{
    use CommonTraits;

    public function stub($task_id, $task_name)
    {
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
    }

    /**
     * master_tasks id: 85
     *
     * @param $task_id
     * @param $task_name
     */
    public function deleteTmpFiles($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);

        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/deleteTmpFiles.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Starting', ['task_id' => $task_id, 'task_name' => $task_name]);

        try {
            $files = Storage::disk('upload_tmp')->allFiles();
            $logger->addInfo('Files found', ['count' => count($files)]);

            if (count($files) > 0) {
                foreach ($files as $f) {
                    if ($f == 'public/tmp/.gitignore') {
                        continue;
                    }
                    Storage::delete($f);
                }
            }
            $logger->addInfo("Success", ['time' => date('Y-m-d H:i:s')]);
            $this->scheduledTaskEnded($task_id, $log_id, "deleted temp files", "", 'Completed');

        } catch (\Exception $e) {

            $logger->addInfo('Failed', ['error' => 'Not completed', 'error_message' => $e->getMessage()]);
            $short_description = "Could not delete tmp files";
            $this->scheduledTaskEnded($log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    /**
     * master_tasks id: 127
     *
     * @param $task_id
     * @param $task_name
     */
    public function websiteRequests($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/websiteRequests.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, "Not implemented", "");
    }

    public function geocodeQueueProcessor($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/geocodeQueueProcessor.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, "Not implemented", "");
    }

    /**
     * master_tasks id: 137
     *
     * @param $task_id
     * @param $task_name
     */
    public function mirouteAddressUpdate($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/mirouteAddressUpdate.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, "Not implemented", "");
    }
}