<?php

namespace App\Http\Controllers;

use App\Country;
use App\County;
use App\Note;
use App\NoteCategory;
use App\ResaleCondition;
use App\TimeZone;
use App\Month;
use App\Notification;
use App\Holiday;
use App\NotificationTemplate;
use App\PostalCode;
use App\StateProvince;
use App\Vehicle;
use App\ZipCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\UtilityTraits;
use App\Http\Traits\ValidationTraits;

class UtilityController extends Controller
{

    use UtilityTraits;
    use ValidationTraits;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Set session values with key=>value input
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setSession(Request $request)
    {

        \Debugbar::info("in setSession with key: {$request->key} and value " . json_encode($request->value) . " and previous value " . json_encode(session()->get($request->key)));
        session()->forget($request->key);
        session()->put($request->key, $request->value);
        \Debugbar::info("{$request->key} now " . json_encode(session()->get($request->key)));
        return response()->json(['message' => 'set ' . $request->key . ' to ' . json_encode($request->value)]);
    }

    public function setView(Request $request)
    {
        \Debugbar::info('In controller setView');
        \Debugbar::info($request->all());
        session()->forget('view');
        session()->forget('subView');
        session()->put('view', $request->view);
        session()->put('subView', $request->subView);
    }

    public function setMultipleSessionValues(Request $request)
    {
        \Debugbar::info('setMultipleSessionValues');
        \Debugbar::info($request->all());
        foreach ($request->all() as $set) {
            \Debugbar::error($set);
            //\Debugbar::info('setting ' . $k . ' to ' . $v);
            session()->put($set['key'], $set['value']);
        }
        return;
    }

    /**
     * Return values in session based on requested keys array
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSessionValues(Request $request)
    {
        \Debugbar::info($request);
        $keys = $request->keys;
        $returnValues = [];

        foreach ($keys as $key) {
            $returnValues[$key] = empty(session()->get($key)) ? -1 : session()->get($key);
        }

        return response()->json($returnValues);
    }

    /**
     * Return a value from our configuration. The parameter must include
     *   the config file (app, database, etc.), a dot, and the data name
     *
     * example: app.notification_interval
     *
     * @param $field
     * @return mixed
     */
    public function getConfigValue($field)
    {
        return config($field);
    }

    /**
     * Return states or provinces based on the country id in request
     * 1 - US
     * 2 - CA
     *
     * We use 'states' for simplicity here, but on the front-end provinces will be shown if specified.
     *
     * @param $country_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatesProvinces($country_id)
    {
        $states = StateProvince::where('country_id', $country_id)->orderBy('name')->get();
        $label = $country_id == 1 ? 'states' : 'provinces';
        $session_state_id = empty(session()->get('session_state_id')) ? -1 : session()->get('session_state_id');
        $session_province_id = empty(session()->get('session_province_id')) ? -1 : session()->get('session_province_id');
        return response()->json([$label => $states, 'session_state_id' => $session_state_id, 'session_province_id' => $session_province_id]);
    }

    public function getCounties($state_id)
    {
        $counties = County::where('state_id', $state_id)->orderBy('name')->get();
        return response()->json(['counties' => $counties]);
    }

    public function getZipCodes($state_id)
    {
//        $zipcodes = ZipCode::where('state_id',$state_id)->where('zip_code','like',"$zip_code%")->orderBy('zip_code')->get();
        $zipcodes = ZipCode::where('state_id', $state_id)->orderBy('zip_code')->get();
        \Debugbar::info('Zip Codes for ' . $state_id);
        \Debugbar::info($zipcodes);
        return ['zipcodes' => $zipcodes];
    }

    public function getPostalCodes($province_id, $postal_code = null)
    {
        if ($postal_code) {
            $postalcodes = PostalCode::where('province_id', $province_id)->where('postal_code', 'like', "$postal_code%")->orderBy('postal_code', 'asc')->get();
//        $postalcodes = PostalCode::where('province_id',$province_id)->orderBy('postal_code','asc')->limit(20)->get();
            \Debugbar::info('Postal Codes for ' . $province_id . " starting with $postal_code");
            \Debugbar::info($postalcodes);

        } else {
            $postal_codes = PostalCode::where('province_id', $province_id)->orderBy('postal_code')->get();
        }
        return ['postalcodes' => $postalcodes];
    }

    public function getCounty($zip_code)
    {
        return County::find(ZipCode::find($zip_code)->county_id);
    }

    public function getDaysForMonthAndYear(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $days = [];

        // get days in given month and year
        // mktime(hour, minute, second, month, day, year)
        $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));

        // loop through month by day
        for ($day = 1; $day <= $days_in_month; $day++) {
            $days[] = $day;
        }

        return response()->json(['days' => $days]);
    }

    public function getMonths($disabled_future_months = null, $disabled_present_month = null)
    {
        if ($disabled_future_months) {
            $months = Month::where('id', '<=', date('n'))->get();
            if ($disabled_present_month) {
                $months->pop();
            }
        } else {
            $months = Month::all();
        }

        return ['months' => $months];
    }

    public function getCompanyPlanValues()
    {
        $plans = [];
        $temp_plans = \DB::table('company_options')->select(\DB::raw('distinct service_plan'))
            ->whereNotNull('service_plan')->get();
        foreach ($temp_plans as $plan) {
            if (!empty($plan)) {
                $plans[] = ['id' => $plan->service_plan, 'name' => $plan->service_plan];
            }
        }
        return ['plans' => $plans];
    }

    public function getCompanyMileageEntryValues()
    {
        $mileage_entries = [];
        $temp_entries = \DB::table('company_mileages')->select(\DB::raw('distinct mileage_entry_method'))
            ->whereNotNull('mileage_entry_method')->get();
        foreach ($temp_entries as $entry) {
            if (!empty($entry)) {
                $mileage_entries[] = ['id' => $entry->mileage_entry_method, 'name' => $entry->mileage_entry_method];
            }
        }
        return ['mileage_entries' => $mileage_entries];
    }

    public function getCountries()
    {
        $countries = Country::all();
        // \Debugbar::info($countries);
        return ['countries' => $countries];
    }

    public function getNoteCategories()
    {
        $note_categories = NoteCategory::all();
        return ['note_categories' => $note_categories];
    }

    public function getNoteCategory($id)
    {
        $note_category = NoteCategory::find($id);
        return ['note_category' => $note_category];
    }

    public function updateNoteCategory(Request $request)
    {
        $noteCategory = NoteCategory::find($request->id);
        $noteCategory->title = $request->title;
        $noteCategory->save();
        return ['message' => 'Note category updated'];
    }

    public function addNoteCategory($title)
    {
        \Debugbar::info('title ' . $title);
        $noteCategory = NoteCategory::create(['title' => $title]);
        return ['category_id' => $noteCategory->id, 'message' => 'Note category ' . $noteCategory->title . ' created'];
    }

    public function getResaleConditions()
    {
        $resale_conditions = [];
        $resales = ResaleCondition::all();
        foreach ($resales as $resale) {
            $resale_conditions[$resale['id']] = $resale['name'];
        }
        return ['resale_conditions' => $resale_conditions];
    }

    public function getTimeZones()
    {
        return ['timezones' => TimeZone::all()];
    }

    public function uploadFile(Request $request)
    {
        # TODO: how do deploy and link the storage folder
        // see https://laravel.com/api/5.4/Illuminate/Http/UploadedFile.html for $request->file
        $fileInfo = $request->file;

        \Debugbar::info($fileInfo);
        \Debugbar::info($fileInfo->path());

        $fileInfo->storeAs('public/my_storage', 'license_file.' . $fileInfo->extension());
    }

    public function findCities($input, $country_id = null)
    {
        if ($country_id == null || $country_id == -1) {
            $cities = \DB::table('fuel_cities')->join('state_provinces', 'state_provinces.id', 'fuel_cities.state_province_id')
                ->where('fuel_cities.name', 'like', $input . '%')
                ->select('fuel_cities.id as id', 'fuel_cities.name as name', 'state_provinces.short_name as state_province')->get();
        } else {
            $cities = \DB::table('fuel_cities')->join('state_provinces', 'state_provinces.id', 'fuel_cities.state_province_id')
                ->where('fuel_cities.name', 'like', $input . '%')
                ->where('fuel_cities.country_id', $country_id)
                ->select('fuel_cities.id as id', 'fuel_cities.name as name', 'state_provinces.short_name as state_province')->get();
        }

        return ['cities' => $cities];
    }


    public function addNewNote(Request $request)
    {
        \Debugbar::info("My note " . json_encode($request->all()));

        $note = Note::create([
            'category_id' => $request->category_id,
            'user_id' => $request->user_id,
            'company_id' => $request->company_id < 0 ? null : $request->company_id,
            'driver_id' => $request->driver_id < 0 ? null : $request->driver_id,
            'vehicle_id' => $request->vehicle_id < 0 ? null : $request->vehicle_id,
            'title' => $request->title,
            'content'=>$request->get('content')
        ]);

        $success = !empty($note);

        // TODO: there is really no reason to fail, as we would have caught errors on the front end
        $message = !$success ? "Failed to create note" : "Created note " . $note->id;

        return ['success' => $success, 'message'=>$message, 'note_id' => $note->id];
    }


    public function getAllNotificationTemplates()
    {
        return NotificationTemplate::all();
    }

    /**
     * @return array
     */
    public function getPageListByCategory()
    {
        $list = [];
        // user types start at 1 not 0
        for ($i = 1; $i <= 4; $i++) {
            $pageCategories = app('\App\Http\Controllers\PortalController')->Menu($i);
            foreach ($pageCategories as $category) {
                if (!array_key_exists('name', $category)) {
                    continue;
                }
                $list[$category['name']] = array_filter($category['data'], function ($sub_array) {
                    return !(array_key_exists('divider', $sub_array) || array_key_exists('label', $sub_array));
                });
            }

        }
        return $list;
    }

    /**
     * @param Request $request
     * This functions requires the request contain:
     *      notification - a full notification object (does not need to be stored in the database, but must be
     *                                                  that format)
     *      recipient_type - integer: 1 = driver, 2 = manager, 3 = administrator, 4 = super
     *      recipients - an array of user_ids
     *
     *
     */
    public function sendNotificationFromNotificationsView(Request $request)
    {
        try {
            $this->validateNotification($request->notification);
        } catch (\Exception $e) {
            return response(
                $e->getMessage(), 400
            );
        }

        foreach ($request->recipients as $user_id) {
            Notification::create([
                'user_id' => $user_id,
                'area' => $request->notification['area'],
                'severity' => $request->notification['severity'],
                'title' => $request->notification['title'],
                'content' => $request->notification['content'],
                'icon' => ($request->notification['icon'] == -1 ? null : $request->notification['icon']),
                'link' => json_encode($request->notification['link']),
                'note' => (isset($request->notification['note']) ? $request->notification['note'] : null),
            ]);
        }

        return $request->recipients;


    }

    public function addNotificationTemplate(Request $request)
    {
        $data = $request->all();
        // don't worry about unique slugs, sluggable will take care of it
        if (isset($data['notification']['link'])) {
            $data['notification']['link'] = is_string($data['notification']['link']) ? $data['notification']['link'] : json_encode($data['notification']['link']);
        }
        $new_id = NotificationTemplate::create($data['notification'])->id;

        return ['id' => $new_id];
    }

    public function updateNotificationTemplate(Request $request)
    {
        $data = $request->all();
        $notification = NotificationTemplate::find($request->notification['id']);
        if (isset($data['notification']['link'])) {
            $data['notification']['link'] = is_string($data['notification']['link']) ? $data['notification']['link'] : json_encode($data['notification']['link']);
        }
        $notification->update($data['notification']);
        $notification->save();

    }

    public function deleteNotificationTemplate(Request $request)
    {
        $notification = NotificationTemplate::find($request->notification_id);
        $notification->delete();
    }


    /**
     * Grabs notifications for the current user, that are visible to their active role (ie. driver)
     *
     * @return unreadNotifications, readNotifications, unread_count
     */
    public function getNotifications()
    {
        //get the active role and notifications based upon that?
        $user = session()->has('impersonate') ? \App\User::find(session()->get('impersonate')) : \Auth::user();

        $active_role = session()->get('active_role');

        $unread_count = 0;
        $unreadNotifications = [];
        $readNotifications = [];

        foreach ($user->notifications as $notification) {
            if ($notification->visible_to == $active_role || $notification->visible_to == 'all') {
                if ($notification->read == 0) {
                    $unread_count++;
                    $unreadNotifications[] = $notification;
                } else {
                    $readNotifications[] = $notification;
                }
            }
        }


        return response()->json([
            'unreadNotifications' => $unreadNotifications,
            'readNotifications' => $readNotifications,
            'unread_count' => $unread_count,
        ]);
    }

    /**
     * This function will the notifications that are visible to the current user's active role, as read
     *
     * A user logged in as a driver, will not have their manager only notifications marked as read!
     */
    public function markNotificationsRead()
    {

        $user = session()->has('impersonate') ? \App\User::find(session()->get('impersonate')) : \Auth::user();

        $active_role = session()->get('active_role');

        \DB::table('notifications')
            ->whereRaw('user_id = ? and ( visible_to = ? or visible_to = ? )', [$user->id, $active_role, 'all'])
            ->update([
                'read' => 1,
                'updated_at' => \DB::raw('NOW()'),
            ]);

    }


    /**
     * This function will return all holidays for a given year and country id
     *
     * @param Request $request
     *      $request will consist of an object containg a year and country id
     *
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getHolidays(Request $request)
    {
        $this->validate($request, [
            'year' => 'required|numeric|min:2017',
            'country_id' => 'required|numeric|min:1|max:2'
        ]);

        $holidays = Holiday::where('year', $request->year)
            ->where('country_id', $request->country_id)
            ->get();

        if ($holidays->count() == 0) {
            return null;
        }

        return response()->json([
            'holidays' => $holidays
        ]);
    }

    /**
     * Gets the holiday object for a given date.
     * Date must be Y-m-d
     *
     * @param $date
     * @return array
     */
    public function getHolidayOn($date)
    {
        return ['holiday' => $this->getHoliday($date)];
    }

    /**
     * This function will return a collection of all holidays for a specific state or province in a specific year
     *
     * @param $state_province_id
     * @param $year
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHolidaysStateProvinceYear($state_province_id, $year)
    {
        return response()->json([
            'holidays' => StateProvince::find($state_province_id)->holidays->where('year', $year)
        ]);
    }

    /**
     * This function adds holidays to the database
     *
     * @param Request $request
     *      $request should be a dictionary, where the keys are state or province names, and the values are
     *          an array of holiday objects
     *      ex: ['Alberta': [
     *              {
     * "date": {
     * "day": 1,
     * "month": 1,
     * "year": 2017,
     * "dayOfWeek": 7
     * },
     * "localName": "New Year's Day / Jour de l'An",
     * "englishName": "New Year's Day"
     *              },
     *              ...
     *              ]
     *          ]
     */
    public function addHolidays(Request $request)
    {
        $country_id = null;
        foreach ($request->all() as $key => $value) {
            $state_province_id = StateProvince::where('name', $key)->pluck('id')->first();

            if ($country_id == null) {
                $country_id = StateProvince::where('name', $key)->pluck('country_id')->first();
            }

            //value here is an array for holidays for the given state province
            foreach ($value as $holiday) {
                \DB::table('holidays')->insert([
                    'year' => $holiday['date']['year'],
                    'month' => $holiday['date']['month'],
                    'day' => $holiday['date']['day'],
                    'day_of_week' => $holiday['date']['dayOfWeek'],
                    'country_id' => $country_id,
                    'state_province_id' => $state_province_id,
                    'name' => $holiday['englishName'],
                    'note' => array_key_exists('note', $holiday) ? $holiday['note'] : null,
                ]);
            }
        }
    }

    /**
     * This function updates an existing holiday
     *
     * @param Request $request
     *      $request should be a holiday object (Holiday.php)
     */
    public function updateHoliday(Request $request)
    {
        $this->validate($request, [
            'year' => 'required|numeric|min:2017',
            'month' => 'required|numeric|min:1',
            'day' => 'required|numeric|min:1',
            'day_of_week' => 'required|numeric|min:1',
            'country_id' => 'required|numeric|min:1|max:2',
            'state_province_id' => 'required|numeric|min:1',
        ]);

        \DB::table('holidays')->where('id', $request->id)
            ->update([
                'year' => $request->year,
                'month' => $request->month,
                'day' => $request->day,
                'day_of_week' => $request->day_of_week,
                'name' => $request->name,
                'note' => $request->note
            ]);

        return;
    }


    public function isHoliday($date, $country_id = null, $state_province_id = null)
    {
        $count = 0;

        $arr = explode('-', $date);
        $year = (int)($arr[0]);
        $month = (int)($arr[1]);
        $day = (int)($arr[2]);

        if ($country_id && $state_province_id) {
            $count = \DB::table('holidays')
                ->select()
                ->where('country_id', $country_id)
                ->where('state_province_id', $state_province_id)
                ->where('year', $year)
                ->where('month', $month)
                ->where('day', $day)
                ->count();
        } else if ($country_id) {
            $count = \DB::table('holidays')
                ->select()
                ->where('country_id', $country_id)
                ->where('year', $year)
                ->where('month', $month)
                ->where('day', $day)
                ->count();
        } else if ($state_province_id) {
            $count = \DB::table('holidays')
                ->select()
                ->where('state_province_id', $state_province_id)
                ->where('year', $year)
                ->where('month', $month)
                ->where('day', $day)
                ->count();
        } else {
            $count = \DB::table('holidays')
                ->select()
                ->where('year', $year)
                ->where('month', $month)
                ->where('day', $day)
                ->count();
        }

        return ['is_holiday' => $count > 0 ? true : false];

    }

    public function areAnyHolidays($date, $country_id = null, $state_province_id = null)
    {
        //TODO: complete this Frank
    }

    public function addZipPostalCode(Request $request)
    {
        \Debugbar::info('addPostaCode:');
        \Debugbar::info($request);

        $this->validate($request, [
            'zip_postal_code' => 'required',
            'city_name' => 'required',
            'state_province_id' => 'required|integer|min:1',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);

        $new = $request->country_id == 1 ? new ZipCode : new PostalCode;
        if ($request->country_id == 1) {
            $new->zip_code = $request->zip_postal_code;
            $new->state_id = $request->state_province_id;
            $new->time_zone_id = 6; // TO DO get it from Google after activation of
            $new->county_id = $request->county_id;
        } else {
            $new->postal_code = $request->zip_postal_code;
            $new->province_id = $request->state_province_id;
        }
        $new->latitude = $request->latitude;
        $new->longitude = $request->longitude;
        $new->city_name = $request->city_name;

        $new->save();
        \Debugbar::info('addPostaCode: ... added');

    }

    /**
     * @param Request $request : city_name, state_province_id, zip_postal_code, country_id
     * @return array
     */
    public function getLngLatFromGoogle(Request $request)
    {

        \Debugbar::info('Process getLngLatFromGoogle for');
        \Debugbar::info($request);

        /// JUST FOR TEST
        ///
        /// return [ 'city_name'=>'Carol Stram', 'latitude'=>41.9125286, 'longitude'=>-88.1347927, 'county_id' => null,
        ///    'county_name' => 'DuPage', 'timezone_id' => 6, 'prevent_insert' => 0, 'message' => null];
        /// JUST FOR TEST
        ///

        // check if we have it already
        if ($request->country_id == 1) {
            $rec = ZipCode::find($request->zip_postal_code);
            $zip_label = 'Zip Code';
        } else {
            $rec = PostalCode::find($request->zip_postal_code);
            $zip_label = 'Postal Code';
        }
        if ($rec) {
            return ['message' => "We already have this $zip_label $request->zip_postal_code in database",
                'latitude' => $rec->latitude, 'longitude' => $rec->longitude, 'city_name' => $rec->city_name,
                'prevent_insert' => 1];
        }

        $message = null;
        $city_name = null;
        $county_id = null;
        $timezone_id = null;

        $key = 'AIzaSyB4cVyUmAOKJC0foRNHAjROST06W7U8Iow';
        //TODO: get google key from env.
        $postal_code = $request->zip_postal_code;
        $city_name = $request->city_name;
        $data = StateProvince::select('short_name')->find($request->state_province_id);
        $province = $data->short_name;
        $address_encoded = urlencode("$city_name,$postal_code,$province");

        $url = "https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=$address_encoded&key=$key";
        $response = file_get_contents($url);
        $json = json_decode($response, TRUE);

        \Debugbar::info('returned:');
        \Debugbar::info($json);

        if (!isset($json['results'][0])) {

            return ['city_name' => null, 'latitude' => null, 'longitude' => null, 'prevent_insert' => 0,
                'message' => 'Invalid combination of Postal Code, City and Province'];
        }

        $address_component = $json['results'][0]['address_components'];
        foreach ($address_component as $row) {
            if ($row['types'][0] == 'locality') {
                $city_name = $row['long_name'];
            }
            if ($row['types'][0] == 'administrative_area_level_2') {
                $county_name = $row['long_name'];
                $county = str_replace(' County', '', $county_name);
                $rec = County::where('name', $county)->where('state_id', $request->state_province_id)->first();
                if ($rec) {
                    $county_id = $rec->id;
                } else {
                    \Debugbar::error('Unknown county ' . $county . ' in ');
                }
            }
        }
        if ($city_name == null) {
            $message = 'Google found post code with lng/lat but could not attach city';
        }
        $lat = $json['results'][0]['geometry']['location']['lat'];
        $lng = $json['results'][0]['geometry']['location']['lng'];

        $time = time();


        if ($request->country_id == 1) {
            // we also need time zone for zip_codes table
            $geo_encoded = urlencode("$lat,$lng");
            $url = "https://maps.googleapis.com/maps/api/timezone/json?location=$lat,$lng&timestamp=$time&key=$key";
            $response = json_decode(file_get_contents($url), true);
            $timeZoneName = $response['timeZoneName'];
            $rec = TimeZone::where('name', $timeZoneName)->first();
            if ($rec) {
                $timezone_id = $rec->id;
            }
        }


        return ['city_name' => $city_name, 'latitude' => $lat, 'longitude' => $lng, 'county_id' => $county_id,
            'county_name' => $county, 'timezone_id' => $timezone_id, 'prevent_insert' => 0, 'message' => $message];
    }

    public function getVehicle($id)
    {
        return ['vehicle' => Vehicle::find($id)];
    }


    /**
     *  Function to retrieve all fuel cities, keyed by their respective state or province
     *      ex.
     *          $fuelCities = [
     *              'Alaska' = [
     *                  { id: 61, name: 'Anchorage' ... } ,
     *                  { ... }
     *                      .
     *                      .
     *                      .
     *              ],
     *              'Alabama' = [
     *                  { ... },
     *              .
     *              .
     *              .
     *          ]
     *
     * @param null $country_id (optional)
     * @return array
     *
     * NOTE: fuelCity objects do NOT match the database exactly. Here I am grabbing both the state_province_abbreviation for each
     *          fuelCity. Each fuelCity has EXTRA information, but contains ALL of the information present on the table.
     */
    public function getFuelCities($country_id = null)
    {
        if (!empty($country_id)) {
            $fuelCities = \DB::table('fuel_cities')
                ->join('state_provinces', 'fuel_cities.state_province_id', '=', 'state_provinces.id')
                ->select('fuel_cities.*', 'state_provinces.name as state_province_name', 'state_provinces.short_name as state_province_short_name')
                ->where('fuel_cities.country_id', $country_id)
                ->get();
        } else {
            $fuelCities = \DB::table('fuel_cities')
                ->join('state_provinces', 'fuel_cities.state_province_id', '=', 'state_provinces.id')
                ->select('fuel_cities.*', 'state_provinces.name as state_province_name', 'state_provinces.short_name as state_province_short_name')
                ->get();
        }

        $fuelCitiesByStateProvince = [];

        foreach ($fuelCities as $fuelCity) {
            if (!array_key_exists($fuelCity->state_province_name, $fuelCitiesByStateProvince)) {
                $fuelCitiesByStateProvince[$fuelCity->state_province_name] = [];
            }

            // check if malformed UTF8, skip bad ones TODO: find a better solution to the problem
            if (!preg_match('//u', $fuelCity->name)) {
                continue;
            }

            $fuelCitiesByStateProvince[$fuelCity->state_province_name][] = $fuelCity;
        }

        return ['fuelCities' => $fuelCitiesByStateProvince];
    }

    /**
     * Function will return the associated geodata for a zip_postal_code
     *      NOTE country_id is required
     *
     * @param $country_id
     * @param $zip_postal_code
     * @param $return_state_province - optional, load the state/province relationshp and return it as well
     */
    public function getGeoDataFromZipPostal($country_id, $zip_postal_code, $get_state_province = false)
    {
        if ($country_id == 1) {
            // USA
            $zipCode = ZipCode:: find($zip_postal_code);
            if ($get_state_province) {
                $zipCode->state;
            }
            return $zipCode;
        } else {
            // CANADA
            $postalCode = PostalCode::find($zip_postal_code);
            if ($get_state_province) {
                $postalCode->province;
            }
            return $postalCode;
        }
    }

    /**
     * Will return a correctly formatted address object from the given google maps autocomplete data
     *
     * @param Request $request
     */
    public function parseGoogleAddressData(Request $request)
    {
        $address = new \stdClass();

        $errors = false;

        // lat long
        if (!empty($request->longitude) && !empty($request->latitude)) {
            $address->longitude = $request->longitude;
            $address->latitude = $request->latitude;
        } else {
            $errors = true;
        }

        // state_province_id, state_province and country_id
        if (!empty($request->administrative_area_level_1)) {
            $stateProvince = StateProvince::where('short_name', $request->administrative_area_level_1)->first();

            if (empty($stateProvince)) {
                abort(401, 'Unable to complete the request');
            }

            $address->state_province_id = $stateProvince->id;
            $address->state_province = $stateProvince;
            $address->country_id = $stateProvince->country_id;
        } else {
            $errors = true;
        }

        // street city zip
        if (!empty($request->locality) && !empty($request->street_number) && !empty($request->route)) {
            $address->city = $request->locality;
            $address->street = $request->street_number . ' ' . $request->route;
            $address->zip_postal = $request->postal_code;
        } else {
            $errors = true;
        }

        if ($errors) {
            abort(400, 'Unable to complete the request');
        } else {
            return ['address' => $address];
        }
    }

    public function getStateProvince($state_province_id = -1)
    {
        $stateProvince = $state_province_id > 0 ? StateProvince::find($state_province_id) : null;
        return ['stateProvince' => $stateProvince];
    }

    public function getCitiesFromSearchTerm($search_term)
    {
        $cities = [];

        $query = "
            select 
              postal_codes.city_name as city_name, 
              state_provinces.short_name as short_name, 
              any_value(state_provinces.id) as state_province_id, 
              any_value(postal_codes.latitude) as latitude, 
              any_value(postal_codes.longitude) as longitude, 
              min(postal_codes.postal_code) as zip_postal_code
            from postal_codes 
            join state_provinces on state_provinces.id = postal_codes.province_id
            where postal_codes.city_name like '%$search_term%'
            group by city_name, short_name
            order by city_name, short_name
        ";

        $result = \DB::select(\DB::raw($query));

        foreach ($result as $row) {
            // check if malformed UTF8, skip bad ones TODO: find a better solution to the problem
            if (!preg_match('//u', $row->city_name)) {
                continue;
            }

            $cities[] = [
                'name' => ucwords(strtolower($row->city_name)),
                'zip_postal_code' => $row->zip_postal_code,
                'abbreviation' => $row->short_name,
                'state_province_id' => $row->state_province_id,
                'country_id' => 2,
                'latitude' => $row->latitude,
                'longitude' => $row->longitude,
            ];
        }

        $query = "
            select 
              zip_codes.city_name as city_name, 
              state_provinces.short_name as short_name, 
              any_value(state_provinces.id) as state_province_id, 
              any_value(zip_codes.latitude) as latitude, 
              any_value(zip_codes.longitude) as longitude, 
              min(zip_codes.zip_code) as zip_postal_code
            from zip_codes 
            join state_provinces on state_provinces.id = zip_codes.state_id
            where zip_codes.city_name like '%$search_term%'
            group by city_name, short_name
            order by city_name, short_name
        ";

        $result = \DB::select(\DB::raw($query));;

        foreach ($result as $row) {
            // check if malformed UTF8, skip bad ones TODO: find a better solution to the problem
            if (!preg_match('//u', $row->city_name)) {
                continue;
            }

            $cities[] = [
                'name' => ucwords(strtolower($row->city_name)),
                'zip_postal_code' => $row->zip_postal_code,
                'abbreviation' => $row->short_name,
                'state_province_id' => $row->state_province_id,
                'country_id' => 1,
                'latitude' => $row->latitude,
                'longitude' => $row->longitude,
            ];
        }

        return ['cities' => $cities];
    }

    public function getFuelYears()
    {
        return ['fuelYears' => array_map(
            function ($row) {
                return $row->year;
            },
            \DB::select(\DB::raw('select distinct year(updated_at) as `year` from fuel_prices order by `year` desc'))
        )
        ];
    }

    /**
     * Given a slug, this function will copy the desired templates to the tmp directory,
     * and return the filename of the moved files
     *
     * @param $slug
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getTemplates($slug)
    {
        if (empty($slug)) {
            return response(['Invalid slug' => ['Invalid slug no template found']], 400);
        }

        $files = [];

        $tmp_dir = storage_path() . '/app/public/tmp/';

        switch ($slug) {
            case 'add-many-drivers':
                $path = base_path() . '/resources/assets/xls/';
                $files['us_template'] = 'add_many_drivers_template_us.xls';
                $files['ca_template'] = 'add_many_drivers_template_ca.xls';
                break;
            case 'pre-defined-stops':
                $path = base_path() . '/resources/assets/csv/';
                $files['us_template'] = 'pre_defined_stops_template_us.csv';
                $files['ca_template'] = 'pre_defined_stops_template_ca.csv';
                $files['us_contract_template'] = 'pre_defined_stops_contract_template_us.csv';
                $files['ca_contract_template'] = 'pre_defined_stops_contract_template_ca.csv';
                break;
        }

        $tmp_files = [];

        foreach ($files as $key => $filename) {
            copy($path . $filename, $tmp_dir . $filename);
            $tmp_files[$key] = $filename;
        }


        return ['files' => $tmp_files];

    }

    public function deleteTemporaryFile(Request $request)
    {
        $tmp_dir = storage_path() . '/app/public/tmp/';
        unlink($tmp_dir . $request->filename);

        return ['message' => $request->filename . ' deleted successfully'];
    }

    public function getHistoricalFuelData($country_id, $state_province_id, $fuel_city_id = null)
    {
        $country_id = \DB::connection()->getPdo()->quote($country_id);
        $state_province_id = \DB::connection()->getPdo()->quote($state_province_id);

        if (!empty($fuel_city_id)) {
            // for a specific city
            $fuel_city_id = \DB::connection()->getPdo()->quote($fuel_city_id);
            $query = <<<EOQ
SELECT 
  fc.name,
  year(fp.updated_at) AS year, 
  MONTH(fp.updated_at) AS month, 
  AVG(fp.price) as price, 
  count(fp.price) as `count`
FROM fuel_cities fc
JOIN fuel_prices fp ON fc.id = fp.fuel_city_id
JOIN state_provinces sp ON sp.id = fc.state_province_id
WHERE fc.country_id = {$country_id} AND fc.state_province_id = {$state_province_id} AND fc.id = {$fuel_city_id}
GROUP BY 1,2,3
ORDER BY `year` DESC, `month` DESC
EOQ;
        } else {
            // for a specific state/province
            $query = <<<EOQ
SELECT 
  sp.name, 
  year(fp.updated_at) AS year, 
  MONTH(fp.updated_at) AS month, 
  AVG(fp.price) as price, 
  count(fp.price) as `count`
FROM fuel_cities fc
JOIN fuel_prices fp ON fc.id = fp.fuel_city_id
JOIN state_provinces sp ON sp.id = fc.state_province_id
WHERE fc.country_id = {$country_id} AND fc.state_province_id = {$state_province_id}
GROUP BY 1,2,3
ORDER BY `year` DESC, `month` DESC
EOQ;
        }

        // Gather the properly formatted cities also

        $fuelData = \DB::select(\DB::raw($query));

        $query = <<<EOQ
SELECT 
  fc.name, 
  year(fp.updated_at) AS year, 
  MONTH(fp.updated_at) AS month, 
  AVG(fp.price) as price,
  fc.id,
  fc.latitude,
  fc.longitude
FROM fuel_cities fc
JOIN fuel_prices fp ON fc.id = fp.fuel_city_id
JOIN state_provinces sp ON sp.id = fc.state_province_id
WHERE fc.country_id = {$country_id} AND fc.state_province_id = {$state_province_id}
GROUP BY 1,2,3,5,6,7
ORDER BY `year` DESC, `month` DESC
EOQ;

        $cityPrices = \DB::select(\DB::raw($query));

        return ['fuelData' => $fuelData, 'cityPrices' => $cityPrices];
    }
}
