<?php

namespace App\Http\Controllers;

use App\MileageBand;
use Illuminate\Http\Request;
use App\Vehicle;
use App\VehicleSuperCategory;
use App\VehicleCategory;

class VehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get makes from db for dropdown values
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVehicleMakes()
    {
        $makes = \DB::table('vehicle_makes')->select('id', 'name')->get();

        return response()->json([
            'makes' => $makes
        ]);
    }

    public function getVehicleModels(Request $request)
    {
        \Debugbar::error('In getVehicleModels');
        \Debugbar::info($request);
        $session_make_id = $request->make_id;
        $session_year_id = $request->year_id;

        if ($session_make_id != -1)
        {
            $models = \DB::table('vehicle_models')->select('vehicle_models.id', 'vehicle_models.name')
                ->join('model_year','vehicle_models.id','=','model_year.model_id')
                ->where('make_id', $session_make_id)
                ->where('model_year.year',$session_year_id)
                ->orderBy('name')->get();
        }
        else
        {
            $models = [];
        }

        return response()->json([
            'models' => $models
        ]);
    }

    public function getVehicleTrims(Request $request) {

        $session_model_id = $request->model_id;
        $session_year_id = $request->year_id;
        \Debugbar::info('get trims for year = ' . $session_year_id . ' model = ' . $session_model_id);

        if( $session_model_id != -1 && $session_year_id != -1 ) {
            $trims = \DB::table('vehicle_trims')->select('id','name')->where('model_id',$session_model_id)->where('year',$session_year_id)->orderBy('order_by')->get();
        } else {
            $trims = [];
        }

        return response()->json([
            'trims' => $trims
        ]);

    }

    /**
     * This function returns the vehicle category object for a given category_id
     *
     * @param $category_id
     */
    public function getVehicleCategory($category_id)
    {
        return ['category' => VehicleCategory::find($category_id)];
    }

    /**
     * Returns vehicles separated by categories.
     * Returns an array of vehicles with disabled options representing the categories,
     * separating the array.
     */
    public function getVehiclesByCategories(Request $request) {
        $year = $request->year;
        $favr = $request->favr;

        \Debugbar::info('Find vehicles for year ' . $year . ' and FAVR = ' . $favr);
        if( $favr == 0 ) {
            $vehicles = \DB::table('vehicles')
                ->join('vehicle_categories','vehicles.category_id','=','vehicle_categories.id')
                ->select('vehicles.id', 'vehicles.year', 'vehicles.name as vehicle','rank','vehicle_categories.name as category','vehicles.category_id','vehicle_categories.display_order')
                ->where('vehicles.year',$year)
                ->orderBy('vehicle_categories.display_order','asc')
                ->orderBy('vehicles.display_order','asc')
                ->orderBy('vehicles.name','asc')
                ->get();
        } else {
            $vehicles = \DB::table('vehicles')
                ->join('vehicle_categories','vehicles.category_id','=','vehicle_categories.id')
                ->select('vehicles.id', 'vehicles.year', 'vehicles.name as vehicle','rank','vehicle_categories.name as category','vehicles.category_id','vehicle_categories.display_order')
                ->whereRaw("vehicles.`year` = {$year} and ( vehicles.favr = 1 )")
                ->orderBy('vehicle_categories.display_order','asc')
                ->orderBy('vehicles.display_order','asc')
                ->orderBy('vehicles.name','asc')
                ->get();

        }

        $last_category = '';
        $fv = [];
        $i=0;
        foreach( $vehicles as $v) {
            if( $v->category != $last_category ) {
                $fv[] = ["name" => $v->category, 'disabled' => 'disabled', 'class' => 'bold' ];
                $last_category = $v->category;
            }
            $name = $v->year . "  ";
            $name .=  $v->rank > 0 ? $v->vehicle . '[' . $v->rank . ']' : $v->vehicle;
            $fv[] = [ 'id' => $v->id, "name" => $name, 'disabled' => null, 'class' => 'shift' ];
            $i++;
        }
        \Debugbar::info("Found $i vehicles");

        return response()->json([
            'vehicles' => $fv
        ]);

    }

    public function getVehiclesBySuperCategories(Request $request)
    {
        $year = $request->year;
        // TODO DELETE THIS, FORCE YEAR 2017 FOR NOW


        $favr = $request->favr;

        \Debugbar::info('Find vehicles for year ' . $year . ' and FAVR = ' . $favr);
        if( $favr == 0 ) {
            $vehicles = \DB::table('vehicles')
                ->join('vehicle_categories','vehicles.category_id','=','vehicle_categories.id')
                ->join('vehicle_super_categories', 'vehicle_categories.super_category_id', '=', 'vehicle_super_categories.id')
                ->select('vehicles.id', 'vehicles.year', 'vehicles.name as name','rank',
                    'vehicle_categories.name as category','vehicles.category_id','vehicle_categories.display_order',
                    'vehicle_categories.super_category_id', 'vehicle_super_categories.name as super_category')
                ->where('vehicles.year',$year)
                ->orderBy('vehicle_categories.display_order','asc')
                ->orderBy('vehicles.display_order','asc')
                ->orderBy('vehicles.name','asc')
                ->orderBy('rank', 'desc')
                ->get();
        } else {
            $vehicles = \DB::table('vehicles')
                ->join('vehicle_categories','vehicles.category_id','=','vehicle_categories.id')
                ->join('vehicle_super_categories', 'vehicle_categories.super_category_id', '=', 'vehicle_super_categories.id')
                ->select('vehicles.id', 'vehicles.year', 'vehicles.name as name','rank',
                    'vehicle_categories.name as category','vehicles.category_id','vehicle_categories.display_order',
                    'vehicle_categories.super_category_id', 'vehicle_super_categories.name as super_category')
                ->whereRaw("vehicles.`year` = {$year} and ( vehicles.favr = 1 or vehicles.name like '%FAVR%' )")
                ->orderBy('vehicle_categories.display_order','asc')
                ->orderBy('vehicles.display_order','asc')
                ->orderBy('vehicles.name','asc')
                ->orderBy('rank', 'desc')
                ->get();
        }

        $vehiclesBySuperCategory = [];

        foreach ($vehicles as $v) {
            if ( !array_key_exists( $v->super_category, $vehiclesBySuperCategory) ) {
                $vehiclesBySuperCategory[$v->super_category] = [];
            }

            if ( !array_key_exists( $v->category, $vehiclesBySuperCategory[$v->super_category] ) ) {
                $vehiclesBySuperCategory[$v->super_category][$v->category] = [];
            }
            $vehiclesBySuperCategory[$v->super_category][$v->category][] = [
                'id' => $v->id,
                'name' => $v->name,
                'rank' => $v->rank
            ];

        }

        return ['vehicles' => $vehiclesBySuperCategory];


    }

    public function getVehiclesYearRange(){
        $minYear = Vehicle::where('year','>',0)->min('year');
        $maxYear = Vehicle::max('year');
        return ['min_year'=>$minYear, 'max_year'=>$maxYear];
    }

    public function getMileageBand($id) {
        return MileageBand::find($id);
    }
}