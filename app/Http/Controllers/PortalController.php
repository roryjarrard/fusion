<?php

namespace App\Http\Controllers;

use App\Address;
use App\AdministratorProfile;
use App\Company;
use App\CompanyPaymentDate;
use App\Http\Traits\CommonTraits;
use App\Http\Traits\DriverTraits;
use App\ManagerProfile;
use Illuminate\Http\Request;
use App\User;
use App\TimeZone;
use App\PostalCode;
use App\ZipCode;
use App\Http\Controllers\ReimbursementController;
use App\Calculators\StreetLightCalculator;

class PortalController extends Controller
{

    use CommonTraits;

    /**
     * Initially index for all roles looks similar but designed to break out by role for future needs/enhancements
     */

    private $streetLightCalculator;

    public function __construct()
    {
        $this->middleware('auth');
        $this->streetLightCalculator = new StreetLightCalculator();
    }

    public function redirectToPortal(Request $request)
    {
        \Debugbar::info('HERE');
        \Debugbar::info($request);

        $user = session()->has('impersonate') ? User::where('id', session()->get('impersonate'))->first() : $request->user();
        \Log::info('redirectToPortal', ['user' => $user]);

        session()->put('authenticated_user_id', \Auth::user()->id);
        session()->put('viewed_user_id', $user->id);

        // set order of importance here (later is higher IMPORTANCE)
        $role = '';
        $roles = [];
        if ($user->isManager()) {
            /*\Log::info('User is manager');*/
            $roles['manager'] = $user->managerProfiles[0]->active;
            $role = 'manager';
        }
        if ($user->isAdmin()) {
            /*\Log::info('User is admin');*/
            $roles['administrator'] = $user->administratorProfiles[0]->active;
            $role = 'administrator';
        }
        if ($user->isSuper()) {
            /*\Log::info('User is super');*/
            $roles['super'] = $user->superProfile->active;
            $role = 'super';
        }
        if ($user->isDriver()) {
            /*\Log::info('User is driver');*/
            $roles['driver'] = $user->driverProfile->active;
            $role = 'driver';
        }

        if (!empty($roles['super'])) {
            $role = 'super'; # assume no dual role for super
        } elseif (!empty($roles['driver'])) {
            $role = 'driver';
            if ($roles['driver'] !== 1 && $user->driverProfile->street_light_id === 1) {
                if (!empty($roles['administrator']) && $roles['administrator'] === 1) {
                    $role = 'administrator';
                } elseif (!empty($roles['manager']) && $roles['manager'] === 1) {
                    $role = 'manager';
                }
            }
        } elseif (!empty($roles['administrator'])) {
            $role = 'administrator';
            if (!empty($roles['driver']) && $roles['driver'] === 1 && $user->driverProfile->street_light_id !== 1) {
                $role = 'driver';
            } elseif (!empty($roles['manager']) && $roles['manager'] === 1) {
                $role = 'manager';
            }
        } elseif (!empty($roles['manager'])) {
            $role = 'manager';
            if (!empty($roles['driver']) && $roles['driver'] === 1 && $user->driverProfile->street_light_id !== 1) {
                $role = 'driver';
            } elseif (!empty($roles['administrator']) && $roles['administrator'] === 1) {
                $role = 'manager';
            }
        }
        /*\Log::info('User Role', ['role'=>$role]);*/

        // set highest level role
        if ($user->isDriver())
            $highest_authenticated_user_type = 'driver';
        if ($user->isManager())
            $highest_authenticated_user_type = 'manager';
        if ($user->isAdmin())
            $highest_authenticated_user_type = 'administrator';
        if ($user->isSuper())
            $highest_authenticated_user_type = 'super';

        // this is set on the role-switcher buttons
        if (session()->has('target_active_role')) {
            $role = session()->get('target_active_role');
            session()->forget('target_active_role');
        }

        /*\Log::info('Target Active Role', ['role'=>$role]);*/

        // check if impersonating
        $impersonator_id = session()->get('impersonator_id');
        $impersonator_role = session()->get('impersonator_role'); // sets in session as well, super, admin, manager or ''

        // start filling the initial values response
        $initial_values = [
            'user_id' => $user->id,
            'username' => $user->username,
            'user_email' => $user->email,
            'user_personal_email' => $user->personal_email,
            'user_full_name' => $user->first_name . ' ' . $user->last_name,
            'is_driver' => $user->isDriver(),
            'is_manager' => $user->isManager(),
            'is_administrator' => $user->isAdmin(),
            'is_super' => $user->isSuper(),
            'view' => session()->get('view'),
            'subView' => session()->get('subView'),
            'highest_authenticated_user_type' => $highest_authenticated_user_type,
            'impersonator_id' => $impersonator_id,
            'impersonator_role' => $impersonator_role,

            // TODO: these may be removed in the future, but needed for testing
            'session_year' => session()->has('session_year') && session()->get('session_year') > -1 ? session()->get('session_year') : date('Y'),
            'session_month' => session()->has('session_month') && session()->get('session_month') > -1 ? session()->get('session_month') : date('m'),
            'session_day' => session()->has('session_day') && session()->get('session_day') > -1 ? session()->get('session_day') : date('d'),
        ];

        // super needs all company info whether active role is super or linking as another user
        if (\Auth::user()->isSuper() || \Auth::user()->isAdmin()) {
            $where_clause_for_admin = '';
            if ($user->administratorProfiles->count() > 0) {
                $where_clause_for_admin = ' AND c.id = ' . $user->administratorProfiles[0]->company_id;
            }
            $query = <<<EOQ
SELECT
  distinct(c.id),
  c.name,
  c.active,
  cp.country_id,
  cf.payment_responsibility,
  ct.use_quarterly_tax_adjustment,
  co.service_plan,
  co.account_management_responsibility,
  cm.miroute_transition_date,
  co.use_company_insurance_module,
  CASE WHEN (SELECT count(*)
             FROM saved_stops ss
             WHERE ss.company_id = c.id) > 0
    THEN TRUE
  ELSE FALSE END AS `company_has_predefined_stops`,
  CASE WHEN cm.mileage_entry_method = 'Mi-Route' 
    OR cm.miroute_transition_date IS NOT NULL AND cm.miroute_transition_date > CURRENT_DATE() 
      THEN 1 ELSE 0 END AS `uses_miroute_filter`
FROM companies c
  JOIN company_payments cf ON cf.company_id = c.id
  JOIN company_profiles cp ON cp.company_id = c.id
  JOIN company_mileages cm ON cm.company_id = c.id
  JOIN company_options co ON co.company_id = c.id
  JOIN company_taxes ct ON ct.company_id = c.id
WHERE cf.deleted_at is NULL AND cp.deleted_at IS NULL AND co.deleted_at IS NULL
{$where_clause_for_admin}
ORDER BY c.name
EOQ;
            $initial_values['session_companies'] = \DB::select(\DB::raw($query)); // added WHERE cf... on JUNE 2018, when a profile is updated we soft delete the old. I was having duplicates in session -FRANK
        }

        // now fill in data based on the role being used now
        //$role = 'driver';
        switch ($role) {
            case 'super':
                $initial_values['user_type'] = 'super';
                $initial_values['active_role'] = 'super';
                session()->put('active_role', 'super');
                $initial_values['is_account_executive'] = $user->superProfile->is_account_executive;
                $initial_values['is_insurance_processor'] = $user->superProfile->is_insurance_processor;
                $initial_values['is_payment_processor'] = $user->superProfile->is_payment_processor;
                $initial_values['is_service_associate'] = $user->superProfile->is_service_associate;
                $initial_values['is_vehicle_profile_approver'] = $user->superProfile->is_vehicle_profile_approver;
                $initial_values['is_announcement_approver'] = $user->superProfile->is_announcement_approver;
                $initial_values['can_view_eft'] = $user->superProfile->can_view_eft;
                $initial_values['can_view_login_information'] = $user->superProfile->can_view_login_information;
                $initial_values['can_view_database_tables'] = $user->superProfile->can_view_database_tables;
                $initial_values['is_database_maintainer'] = $user->superProfile->is_database_maintainer;
                $initial_values['is_bank_maintainer'] = $user->superProfile->is_bank_maintainer;
                $initial_values['is_caa_maintainer'] = $user->superProfile->is_caa_maintainer;
                $initial_values['is_tasks_maintainer'] = $user->superProfile->is_tasks_maintainer;
                $initial_values['has_all_permissions'] = $user->superProfile->has_all_permissions;

                $initial_values['session_company_id'] = session()->get('session_company_id');
                /*\Log::info('On init I have session_company_id', ['id'=>$initial_values['session_company_id']]);*/
                if ($initial_values['session_company_id'] > 0) {
                    $company = Company::find($initial_values['session_company_id']);
                    $initial_values['company_selections'] = [
                        'payment_responsibility' => $company->payments->payment_responsibility,
                        'use_stop_contract' => $company->options->use_stop_contract,
                        'use_company_mileage_module' => $company->options->use_company_mileage_module,
                        'use_company_financials_module' => $company->options->use_company_financials_module,
                        'use_company_insurance_module' => $company->options->use_company_insurance_module,
                        'use_company_license_module' => $company->options->use_company_license_module,
                        'can_view_reimbursement_profile' => $company->options->can_view_reimbursement_profile,
                        'can_view_reimbursement_details' => $company->options->can_view_reimbursement_details,
                        'enforce_car_policy_acceptance' => $company->vehicles->enforce_car_policy_acceptance,
                        'reports_display_monthly_tax_limit' => $company->taxes->reports_display_monthly_tax_limit,
                        'use_quarterly_tax_adjustment' => $company->taxes->use_quarterly_tax_adjustment,
                        'account_management_responsibility' => $company->options->account_management_responsibility
                    ];
                    $initial_values['company_report_labels'] = $company->reportLabels;
                }
                $initial_values['session_company_name'] = isset($company) ? $company->name : '';
                $initial_values['company_plan'] = isset($company) ? $company->options->service_plan : '';
                $initial_values['session_user_id'] = session()->get('session_user_id');
                $initial_values['session_driver_id'] = session()->get('session_driver_id');
                $initial_values['userAddress'] = $this->UserAddress($user->id, 'super');
                $initial_values['session_vehicle_profile_id'] = session()->has('session_vehicle_profile_id') ?
                    session()->get('session_vehicle_profile_id') : -1;

                $initial_values['company_slug'] = '';

                $initial_values['editedUser'] = session()->get('editedUser');

                // we may want to auto-search on a given user coming back from impersonating
                $initial_values['search_user_email'] = session()->get('search_user_email');
                \Debugbar::error('search_user_email ' . $initial_values['search_user_email']);

                /**
                 * I moved menu down here, so that given all the initial values, I can determine
                 * the menu permissions, and which pages should / shouldn't be shown.
                 */
                $permissions = $this->getMenuPermissions($role, $initial_values);
                $initial_values['menu'] = $this->Menu('super', null, null, $permissions);
                return view('super.home')->with('initial_values', json_encode($initial_values))->with('company_slug', '');
                break;
            case 'administrator':
                $initial_values['user_type'] = 'administrator';
                $initial_values['active_role'] = 'administrator';
                session()->put('active_role', 'administrator');
                $initial_values['company_name'] = $user->administratorProfiles[0]->company->name;
                session()->put('session_company_name', $user->administratorProfiles[0]->company->name);

                $initial_values['activeAdministratorProfile'] = $user->administratorProfiles[0];
                $initial_values['active_administrator_id'] = $user->administratorProfiles[0]->administrator_id;

                $initial_values['company_id'] = $user->administratorProfiles[0]->company->id;
                session()->put('session_company_id', $user->administratorProfiles[0]->company->id);

                if (session()->get('session_company_id') > 0) {
                    $company = Company::find(session()->get('session_company_id'));
                    $initial_values['company_selections'] = [
                        'payment_responsibility' => $company->payments->payment_responsibility,
                        'use_stop_contract' => $company->options->use_stop_contract,
                        'use_company_mileage_module' => $company->options->use_company_mileage_module,
                        'use_company_financials_module' => $company->options->use_company_financials_module,
                        'use_company_insurance_module' => $company->options->use_company_insurance_module,
                        'use_company_license_module' => $company->options->use_company_license_module,
                        'can_view_reimbursement_profile' => $company->options->can_view_reimbursement_profile,
                        'can_view_reimbursement_details' => $company->options->can_view_reimbursement_details,
                        'enforce_car_policy_acceptance' => $company->vehicles->enforce_car_policy_acceptance,
                        'reports_display_monthly_tax_limit' => $company->taxes->reports_display_monthly_tax_limit,
                        'use_quarterly_tax_adjustment' => $company->taxes->use_quarterly_tax_adjustment,
                        'account_management_responsibility' => $company->options->account_management_responsibility
                    ];
                    $initial_values['company_report_labels'] = $company->reportLabels;
                }

                $initial_values['session_user_id'] = session()->get('session_user_id');
                $initial_values['session_driver_id'] = session()->get('session_driver_id');

                $company_logo = $this->getCompanyLogoUrl($user->administratorProfiles[0]->company->id);

                $initial_values['company_slug'] = str_slug($user->administratorProfiles[0]->company->name);

                if (count($user->administratorProfiles) > 1) {
                    $initial_values['inactiveAdministratorProfile'] = $user->administratorProfiles[1];
                    $initial_values['inactive_administrator_id'] = $user->administratorProfiles[1]->administrator_id;
                    $initial_values['inactive_company_id'] = $user->administratorProfiles[1]->company->id;
                    $initial_values['inactive_company_name'] = $user->administratorProfiles[1]->company->name;
                }
                $initial_values['userAddress'] = $this->UserAddress($user->id, 'administrator');
                $initial_values['editedUser'] = session()->get('editedUser');

                /**
                 * I moved menu down here, so that given all the initial values, I can determine
                 * the menu permissions, and which pages should / shouldn't be shown.
                 */
                $permissions = $this->getMenuPermissions($role, $initial_values);
                $initial_values['menu'] = $this->Menu('administrator', $initial_values['company_name'], null, $permissions);
                return view('administrator.home')
                    ->with('initial_values', json_encode($initial_values))
                    ->with('company_slug', str_slug($user->administratorProfiles[0]->company->name))
                    ->with('company_logo', $company_logo);
                break;
            case 'manager':
                $initial_values['user_type'] = 'manager';
                $initial_values['active_role'] = 'manager';
                session()->put('active_role', 'manager');

                $initial_values['activeManagerProfile'] = $user->managerProfiles[0];
                $initial_values['active_manager_id'] = $user->managerProfiles[0]->manager_id;

                $initial_values['company_id'] = $user->managerProfiles[0]->company->id;
                session()->put('session_company_id', $user->managerProfiles[0]->company->id);
                $initial_values['company_name'] = $user->managerProfiles[0]->company->name;
                session()->put('session_company_name', $user->managerProfiles[0]->company->name);
                $initial_values['country_id'] = $user->managerProfiles[0]->company->profile->country->id;

                $initial_values['company_selections'] = [
                    'payment_responsibility' => $user->managerProfiles[0]->company->payments->payment_responsibility,
                    'use_stop_contract' => $user->managerProfiles[0]->company->options->use_stop_contract,
                    'use_company_mileage_module' => $user->managerProfiles[0]->company->options->use_company_mileage_module,
                    'use_company_insurance_module' => $user->managerProfiles[0]->company->options->use_company_insurance_module,
                    'use_company_license_module' => $user->managerProfiles[0]->company->options->use_company_license_module,
                    'can_view_reimbursement_profile' => $user->managerProfiles[0]->company->options->can_view_reimbursement_profile,
                    'can_view_reimbursement_details' => $user->managerProfiles[0]->company->options->can_view_reimbursement_details,
                    'enforce_car_policy_acceptance' => $user->managerProfiles[0]->company->vehicles->enforce_car_policy_acceptance,
                    'reports_display_monthly_tax_limit' => $user->managerProfiles[0]->company->taxes->reports_display_monthly_tax_limit,
                    'use_quarterly_tax_adjustment' => $user->managerProfiles[0]->company->taxes->use_quarterly_tax_adjustment
                ];

                $initial_values['company_report_labels'] = $user->managerProfiles[0]->company->reportLabels;



                $initial_values['session_user_id'] = session()->get('session_user_id');
                $initial_values['session_driver_id'] = session()->get('session_driver_id');

                $company_logo = $this->getCompanyLogoUrl($user->managerProfiles[0]->company->id);

                $initial_values['managerDrivers'] = $user->managerProfiles[0]->drivers();
                $initial_values['company_slug'] = str_slug($user->managerProfiles[0]->company->name);

                if (count($user->managerProfiles) > 1) {
                    $initial_values['inactiveManagerProfile'] = $user->managerProfiles[1];
                    $initial_values['inactive_manager_id'] = $user->managerProfiles[1]->manager_id;
                    $initial_values['inactive_company_id'] = $user->managerProfiles[1]->company->id;
                    $initial_values['inactive_company_name'] = $user->managerProfiles[1]->company->name;
                }


                $initial_values['userAddress'] = $this->UserAddress($user->id, 'manager');

                /**
                 * I moved menu down here, so that given all the initial values, I can determine
                 * the menu permissions, and which pages should / shouldn't be shown.
                 */
                $permissions = $this->getMenuPermissions($role, $initial_values);
                // this has to come after company name is determined so it will show in the car policy
                $initial_values['menu'] = $this->Menu('manager', $initial_values['company_name'], null, $permissions);
                return view('manager.home')
                    ->with('initial_values', json_encode($initial_values))
                    ->with('company_slug', str_slug($user->managerProfiles[0]->company->name))
                    ->with('company_logo', $company_logo);
                break;
            case 'driver':
                /**
                 * TODO: I don't like what I'm about to do. We should move the reimbursement into a trait i can use
                 */
                /*
                $rc = new ReimbursementController;
                $reimb = $rc->generateReimbursement(date('Y'), date('m'), $user->driverProfile->company_id, $user->id);
                \Debugbar::info("my reimbursement");
                \Debugbar::info($reimb);
                */
                $initial_values['user_type'] = 'driver';
                $initial_values['active_role'] = 'driver';
                session()->put('active_role', 'driver');
                $initial_values['personalVehicle'] = $user->driverProfile->personalVehicle;
                $initial_values['driverProfile'] = $user->driverProfile;
                $initial_values['mileage_lock_day'] = $user->driverProfile->company->dates->mileage_lock_driver_day;
                /*$initial_values['mileage_lock_day'] = session()->has('impersonate') // different for driver and admin or super
                    ? $user->driverProfile->company->dates->mileage_lock_all_day
                    : $user->driverProfile->company->dates->mileage_lock_driver_day;*/
                $initial_values['mileage_approval_end_day'] = $user->driverProfile->company->dates->mileage_approval_end_day;
                /*$initial_values['driver_payment_date'] = CompanyPaymentDate::where('company_id', $user->driverProfile->company->id)
                    ->where('year', date('Y'))
                    ->where('month', date('mm'))->first()->driver_payment_date;*/
                $initial_values['driver_payment_date'] = $user->driverProfile->company->dates->direct_pay_day;
                $initial_values['company_id'] = $user->driverProfile->company_id;
                session()->put('session_company_id', $user->driverProfile->company_id);
                $company_logo = $this->getCompanyLogoUrl($user->driverProfile->company->id);
                $initial_values['company_name'] = $user->driverProfile->company->name;
                session()->put('session_company_name', $user->driverProfile->company->name);
                $initial_values['company_slug'] = str_slug($user->driverProfile->company->name);
                $initial_values['mileage_entry_method'] = $user->driverProfile->company->mileage->mileage_entry_method;
                $initial_values['company_plan'] = $user->driverProfile->company->options->service_plan;
                $initial_values['driver_street_light_array'] = $this->streetLightCalculator->driverStreetLightData($user->id);
                $initial_values['division'] = ['division_id' => $user->driverProfile->division->id, 'division_name' => $user->driverProfile->division->name];
                $initial_values['country_id'] = $user->driverProfile->company->profile->country->id;
                $initial_values['userAddress'] = $this->UserAddress($user->id, 'driver');
                $initial_values['session_user_id'] = session()->get('session_user_id');
                $initial_values['session_driver_id'] = session()->get('session_driver_id');
                \Debugbar::info('setting DRIVER session_driver_id to ' . session()->get('session_driver_id'));
                $initial_values['company_selections'] = [
                    'payment_responsibility' => $user->driverProfile->company->payments->payment_responsibility,
                    'use_stop_contract' => $user->driverProfile->company->options->use_stop_contract,
                    'use_company_mileage_module' => $user->driverProfile->company->options->use_company_mileage_module,
                    'use_company_insurance_module' => $user->driverProfile->company->options->use_company_insurance_module,
                    'use_company_license_module' => $user->driverProfile->company->options->use_company_license_module,
                    'can_view_reimbursement_profile' => $user->driverProfile->company->options->can_view_reimbursement_profile,
                    'can_view_reimbursement_details' => $user->driverProfile->company->options->can_view_reimbursement_details,
                    'enforce_car_policy_acceptance' => $user->driverProfile->company->vehicles->enforce_car_policy_acceptance,
                    'reports_display_monthly_tax_limit' => $user->driverProfile->company->taxes->reports_display_monthly_tax_limit,
                    'use_quarterly_tax_adjustment' => $user->driverProfile->company->taxes->use_quarterly_tax_adjustment
                ];

                $initial_values['company_report_labels'] = $user->driverProfile->company->reportLabels;

                $initial_values['driver_time_zone'] = $user->address->country_id == 1
                    ? TimeZone::find(ZipCode::find($user->address->zip_postal)->time_zone_id)
                    : TimeZone::find(PostalCode::find(str_replace(' ', '', $user->address->zip_postal))->time_zone_id);

                $permissions = $this->getMenuPermissions($role, $initial_values);
                $initial_values['menu'] = $this->Menu('driver', $initial_values['company_name'], $user->driverProfile->company->mileage->mileage_entry_method, $permissions);
                if ($user->driverProfile->street_light_id & 1) {
                    return view('driver.registration')
                        ->with('initial_values', json_encode($initial_values))
                        ->with('company_slug', str_slug($user->driverProfile->company->name))
                        ->with('company_logo', $company_logo);
                } else {
                    \Debugbar::info('Driver initial values');
                    \Debugbar::info($initial_values);
                    return view('driver.home')
                        ->with('initial_values', json_encode($initial_values))
                        ->with('company_slug', str_slug($user->driverProfile->company->name))
                        ->with('company_logo', $company_logo);
                }
                break;
        }
    }

    /**
     * @param Request $request
     * @return $this
     */
    public
    function viewProfile(Request $request)
    {
        return view('driver.profile')->with('user', $request->user());
    }

    public
    function linkToUser(Request $request)
    {
        $current_user_id = $request->current_user_id; // user who is linking
        $target_user_id = $request->target_user_id; // user to link to
        $current_active_role = $request->current_active_role; // role of linking user
        $current_page = $request->current_page; // page viewed when linking (so you can return there)
        $target_active_role = $request->target_active_role; // role of user to link to (optional)
        $from_session = $request->from_session; // from session component, just switch users being linked

        \Log::info('linkToUser', compact('current_user_id', 'target_user_id', 'current_active_role', 'current_page', 'target_active_role', 'from_session'));

        $linkingUser = User::find($current_user_id);

        // if you are already impersonating, and want to impersonate, you might want to switch between users

        if ($linkingUser->hasRole(1) && (!$linkingUser->hasRole(2) && !$linkingUser->hasRole(3) && !$linkingUser->hasRole(4) && !$from_session)) {
            return response()->json(['message' => 'Not allowed']);
        }

        if (session()->has('impersonator_id')) {
            if ($from_session) {
                session()->put('impersonate', $target_user_id);
                if (!empty($target_active_role)) {
                    session()->put('target_active_role', $target_active_role);
                } else {
                    session()->forget('target_active_role');
                }
            } else {
                // we know there is already a linked user, move everyone up the chain
                session()->put('first_impersonator_id', session()->get('impersonator_id'));
                session()->put('first_impersonator_role', session()->get('impersonator_role'));
                session()->put('first_impersonator_username', session()->get('impersonator_username'));
                session()->put('first_impersonator_page', session()->get('impersonator_page'));
            }
        }
        if (!$from_session) {
            session()->put('impersonator_id', $current_user_id);
            session()->put('impersonator_role', $current_active_role);
            session()->put('impersonator_username', $linkingUser->username);
            session()->put('impersonator_page', $current_page);
            session()->put('impersonate', $target_user_id);
            if (!empty($target_active_role)) {
                session()->put('target_active_role', $target_active_role);
            } else {
                session()->forget('target_active_role');
            }
        }

        \Log::info('Set session values', [
            'first_impersonator_id' => session()->get('first_impersonator_id'),
            'first_impersonator_role' => session()->get('first_impersonator_role'),
            'first_impersonator_username' => session()->get('first_impersonator_username'),
            'first_impersonator_page' => session()->get('first_impersonator_page'),
            'impersonator_id' => session()->get('impersonator_id'),
            'impersonator_role' => session()->get('impersonator_role'),
            'impersonator_username' => session()->get('impersonator_username'),
            'impersonator_page' => session()->get('impersonator_page'),
            'impersonate' => session()->get('impersonate'), 'target_active_role' => session()->get('target_active_role')]);

        // TODO: move these and see if they are even valid: rjarrard 2018-08-01
        if (session()->has('driverYear')) {
            session()->put('session_year', session()->get('driverYear'));
        } elseif (session()->has('admin_year')) {
            session()->put('session_year_id', session()->get('admin_year'));
        } else {
            session()->put('session_year_id', date("Y"));
        }

        if (session()->has('driverMonth')) {
            session()->put('session_month', session()->get('driverMonth'));
        } elseif (session()->has('admin_month')) {
            session()->put('session_month_id', session()->get('session_month_id'));
        } else {
            session()->put('session_month_id', date("n"));
        }
        session()->put('session_day_id', date("j"));

        if (session()->has('driverView')) {
            session()->put('view', session()->get('driverView'));
        }
        if (session()->has('driverSubView')) {
            session()->put('subView', session()->get('driverSubView'));
        } elseif (session()->has('admin_page')) {
            session()->put('page', session()->get('admin_page'));
        }

        return ['ok to impersonate'];
    }

    public
    function impersonate($id)
    {
        $user = User::where('id', $id)->first();

        \Debugbar::info('Staring to impersonate #' . $id . ', ' . $user->username);
        \Debugbar::info('driverYear: ' . session()->get('driverYear'));
        \Debugbar::info('driverMonth: ' . session()->get('driverMonth'));
        \Debugbar::info('driverView: ' . session()->get('driverView'));

        /**
         * Update session date
         * - driverYear used in MileageApproval
         * - admin_year used ib ManagerApprovalStatus
         */
        if (session()->has('driverYear')) {
            session()->put('session_year', session()->get('driverYear'));
        } elseif (session()->has('admin_year')) {
            session()->put('session_year_id', session()->get('admin_year'));
        } else {
            session()->put('session_year_id', date("Y"));
        }

        if (session()->has('driverMonth')) {
            session()->put('session_month', session()->get('driverMonth'));
        } elseif (session()->has('admin_month')) {
            session()->put('session_month_id', session()->get('session_month_id'));
        } else {
            session()->put('session_month_id', date("n"));
        }
        session()->put('session_day_id', date("j"));

        // Guard against driver impersonate
        if (\Auth::user()->hasRole(1) && (!\Auth::user()->hasRole(2) && !\Auth::user()->hasRole(3) && !\Auth::user()->hasRole(4))) {
            return response()->json(['message' => 'Not allowed']);
        } else {
            \Debugbar::info("user_id = " . $user);
            \Auth::user()->setImpersonating($user->id);
            if (session()->has('driverView')) {
                session()->put('view', session()->get('driverView'));
            }
            if (session()->has('driverSubView')) {
                session()->put('subView', session()->get('driverSubView'));
            } elseif (session()->has('admin_page')) {
                session()->put('page', session()->get('admin_page'));
            }

            return redirect('/');
        }
    }

    public
    function stopImpersonate()
    {
        session()->put('session_manager_id', -1);
        session()->put('session_administrator_id', -1);
        session()->forget('active_role');

        // reset session date to today

        if (session()->has('driver_year')) {
            session()->put('session_year_id', session()->get('admin_year'));
        } elseif (session()->has('admin_year')) {
            session()->put('session_year_id', session()->get('admin_year'));
        } else {
            session()->put('session_year_id', date("Y"));
        }

        if (session()->has('driver_month')) {
            session()->put('session_month_id', session()->get('driver_month'));
        } elseif (session()->has('admin_month')) {
            session()->put('session_month_id', session()->get('admin_month'));
        } else {
            session()->put('session_month_id', date("n"));
        }
        session()->put('session_day_id', date("j"));

        // remove admin values

        if (session()->has('driver_page')) {
            session()->put('page', session()->get('driver_page'));
        } elseif (session()->has('admin_page')) {
            session()->put('page', session()->get('admin_page'));
        } else {
            session()->put('page', 'users');
            session()->put('subpage', 'showUser');
        }


        // set back to linkToUser, unless admin->manager link
        session()->forget('driverView');
        session()->forget('driverSubView');
        session()->forget('driverYear');
        session()->forget('driverMonth');
        session()->forget('admin_page');
        session()->forget('admin_year');
        session()->forget('admin_month');

        if (session()->has('first_impersonator_id')) {
            session()->put('impersonate', session()->get('impersonator_id'));
            session()->put('target_active_role', session()->get('impersonator_role'));
            session()->put('view', session()->get('impersonator_page'));
            session()->put('impersonator_id', session()->get('first_impersonator_id'));
            session()->put('impersonator_role', session()->get('first_impersonator_role'));
            session()->put('impersonator_username', session()->get('first_impersonator_username'));
            session()->put('impersonator_page', session()->get('first_impersonator_page'));
            session()->forget('first_impersonator_id');
            session()->forget('first_impersonator_role');
            session()->forget('first_impersonator_username');
            session()->forget('first_impersonator_page');
        } else {
            /*session()->put('impersonate', session()->get('impersonator_id'));
            session()->put('target_active_role', session()->get('impersonator_role'));
            session()->put('target_page', session()->get('impersonator_page'));*/
            session()->put('view', session()->get('impersonator_page'));
            session()->put('target_active_role', session()->get('impersonator_role'));
            session()->forget('impersonator_id');
            session()->forget('impersonator_role');
            session()->forget('impersonator_username');
            session()->forget('impersonator_page');
            session()->forget('viewed_user_id');
            session()->forget('impersonate');
        }

        return redirect('/');
    }

    protected
    function getCompanyLogoUrl($company_id)
    {
        $uri = '/storage/company_logos/';

        $logo_exists = !empty(glob(public_path() . $uri . str_slug(Company::find($company_id)->name) . '.*'));
        if (!$logo_exists) {
            return null;
        }

        $logo_location = glob(public_path() . $uri . str_slug(Company::find($company_id)->name) . '.*')[0];
        $logo_array = explode('/', $logo_location);
        $file_name = end($logo_array);

        return $uri . $file_name;
    }

    protected
    function setImpersonatorRole($impersonator_id = -1)
    {
        if ($impersonator_id > 0) {
            if (\Auth::user()->isSuper()) {
                session()->put('impersonator_role', 'super');
                return 'super';
            } elseif (\Auth::user()->isAdmin()) {
                session()->put('impersonator_role', 'administrator');
                return 'administrator';
            } elseif (\Auth::user()->isManager()) {
                session()->put('impersonator_role', 'manager');
                return 'manager';
            }
        }
        session()->forget('impersonator_role');
        return '';
    }

}
