<?php

namespace App\Http\Controllers;
set_time_limit(0);
use App\Company;
use App\DriverProfile;
use App\DriverTax;
use App\DriverTaxDetail;
use App\FavrParameter;
use App\MonthlyMileage;
use App\MonthlyReimbursement;
use App\PersonalVehicle;
use App\DriverTaxRow;
use App\DriverTaxColumn;
use App\Http\Traits\CommonTraits;
use App\Http\Traits\DriverTraits;
use App\Http\Traits\ValidationTraits;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TaxController extends Controller
{
    use CommonTraits, DriverTraits, ValidationTraits;

    /**
     * Creates a new tax quarter definition for a given company.
     *
     * Request should be of the following format
     * Request = {
     *      StockRequestData: ...
     *
     *      newQuarterDefinition : {1: month_id, 2: month_id ... }
     *      company_id: #
     * }
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createNewQuarterDefinition(Request $request)
    {

        try {
            // validate
            $this->validateQuarters($request->newQuarterDefinition, $request->company_id);

            // invalidate the previous record (give it a termination time)
            \DB::table('company_tax_quarters')
                ->where('company_id', $request->company_id)
                ->whereNull('valid_until_year')
                ->update([
                    'valid_until_year' => date('Y') + 1,
                ]);

            /**
             * Check to see if there is an existing record in tax_quarters that has the quarters
             * match the current setup
             */
            $existingTaxQuarterRecord = \DB::table('tax_quarters')
                ->where('q1', $request->newQuarterDefinition['1'])
                ->where('q2', $request->newQuarterDefinition['2'])
                ->where('q3', $request->newQuarterDefinition['3'])
                ->where('q4', $request->newQuarterDefinition['4'])
                ->first();

            // if there is an existing quarter, use it's id. Else insert and use the new definition id
            if (!empty($existingTaxQuarterRecord)) {
                $new_tax_quarter_id = $existingTaxQuarterRecord->id;
            } else {
                \DB::table('tax_quarters')
                    ->insert([
                        'q1' => $request->newQuarterDefinition['1'],
                        'q2' => $request->newQuarterDefinition['2'],
                        'q3' => $request->newQuarterDefinition['3'],
                        'q4' => $request->newQuarterDefinition['4']
                    ]);

                $new_tax_quarter_id = \DB::getPdo()->lastInsertId();
            }


            // insert the new company_tax_quarters record
            \DB::table('company_tax_quarters')
                ->insert([
                    'company_id' => $request->company_id,
                    'tax_quarter_id' => $new_tax_quarter_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);


        } catch (\Exception $e) {
            // return the json encoded errors to the front end
            return response($e->getMessage(), 400);
        }

    }

    /**
     * Deletes a company tax quarter definition.
     *
     * As there can be only a single tax quarter queued up for the future, (currently not active)
     * we delete the given tax quarter. Then we set the previous definition to be active until an undetermined
     * time. Specifically, valid_until_year is set to null.
     *
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteCompanyQuarterDefinition(Request $request)
    {
        try {
            \DB::table('company_tax_quarters')
                ->where('id', $request->quarters['id'])
                ->delete();

            \DB::table('company_tax_quarters')
                ->where('company_id', $request->company_id)
                ->orderBy('valid_until_year', 'desc')
                ->limit(1)
                ->update([
                    'valid_until_year' => null
                ]);
        } catch (\Exception $e) {
            return response(400, $e->getTraceAsString());
        }

        return ['success' => true];
    }


    /**
     * Returns an array of all the historical tax quarter configurations for a given company
     *
     * @param $company_id
     * @return array
     */
    public function getCompanyQuarterHistory($company_id)
    {
        $quartersHistory = \DB::table('company_tax_quarters')
            ->join('tax_quarters', 'tax_quarters.id', 'company_tax_quarters.tax_quarter_id')
            ->select('tax_quarters.*', 'company_tax_quarters.*')
            ->where('company_id', $company_id)
            ->orderBy('created_at', 'desc')
            ->get();

        return ['quartersHistory' => $quartersHistory];
    }

    public function getQuarterDefinition(Request $request)
    {
        $company_id = $request->company_id;
        $year = $request->year;
        $quarter_months_years = $this->getQuarterMonthsYearsRelations($company_id, $year);
        return ['quarters' => array_keys($quarter_months_years)];
    }


    private function getQuarterMonthsYearsRelations($company_id, $year)
    {

        $old_quarter_array = ["Q1", "Q2", "Q3", "Q4"];
        $quarter_months = [];
        $quarter_array = [];

        \Debugbar::info("in getQuarterMonthsYearsRelations c=$company_id y=$year");

        foreach ($old_quarter_array as $value) {
            $q_months = $this->getCompanyQuarters($company_id, $year, $value);
            if (count($q_months) == 1) {
                $quarter_array[] = $value;
                $months = $q_months['ret_arr'];
                $mmm = (array)$months;
                $quarter_months[$value] = $mmm;
                $text = "$value - ";
                foreach ($mmm as $c_month => $c_year) {
                    $text .= $this->getMonthNames()[$c_month] . ' ';
                }
            } else {
                $months = $q_months['ret_arr'];
                $quarter_array[] = $value . " old";
                $quarter_months["$value" . " old"] = $months;
                $quarter_array[] = $value . " new";
                $quarter_months["$value" . " new"] = $months;
            }
        }

        return $quarter_months;

    }

    private function getInformationOnQuarterMonths($quarter_months)
    {

        $quarter_month_info = [];
        foreach ($quarter_months as $quarter => $months) {
            $text = "$quarter - ";
            foreach ($months as $c_month => $c_year) {
                $text .= $this->getMonthNames()[$c_month] . ' ';
            }
            $quarter_month_info[] = $text;
        }
        return $quarter_month_info;
    }

    /**
     * @return array
     */
    private function getTaxRowIds()
    {

        $row_id = [];
        $rec = DriverTaxRow::select('id', 'name')->orderBy('order')->get();
        foreach ($rec as $data) {
            $row_id[$data->name] = $data->id;
        }
        return $row_id;
    }

    public function getDriverAllQuartersTaxFAVR(Request $request)
    {
        $user_id = $request->user_id;
        $company_id = $request->company_id;
        $year = $request->year;
        $columns = $request->show_columns;
        $selected_quarter = $request->selected_quarter;

        //\Debugbar::info("Calculate taxes for user $user_id and year $year");

        $quarter_months_years = $this->getQuarterMonthsYearsRelations($company_id, $year);
        $quarter_month_info = $this->getInformationOnQuarterMonths($quarter_months_years);

        $tax_details = [];
        $tax_summary = [];

        // check if we already stored these taxes
        $driver_tax = DriverTax::select('id')->where('user_id', $user_id)->where('year', $year)->first();

        if ($driver_tax != null && $driver_tax->id > 0) {

            $res = $this->getTaxAdjustmentsFromTable($driver_tax->id, $columns);
            $tax_details = $res['tax_details'];
            $tax_summary = $res['tax_summary'];

        } else {

            $quarter_months_years = $this->getQuarterMonthsYearsRelations($company_id, $year);
            $quarter_month_info = $this->getInformationOnQuarterMonths($quarter_months_years);
            $standard_rate_array = $this->getIRRStandardRates($quarter_months_years);
            $quarter_months = $this->getQuarterMonthsYearsRelations($company_id, $year);

            $rec = DriverTaxColumn::select('id', 'name')->orderBy('id')->get();
            foreach ($rec as $value) {
                $column_name[$value->id] = $value->name;
            }

            $id_of_row = $this->getTaxRowIds(); // reverse array of driver tax_rows table

            // calculating taxable adjustments
            $total_fixed = 0;
            $total_variable = 0;
            $total_reimbursement = 0;
            $total_mileage = 0;
            $total_not_taxable = 0;
            $total_taxable = 0;

            foreach ($quarter_months_years as $quarter => $months_year) {

                if ($selected_quarter != '-1' && $selected_quarter != $quarter) {
                    continue;
                }

                $values = $this->DriverOneQuarterTaxFAVR($user_id, $company_id, $year, $months_year, $standard_rate_array);

                $tax_values[$quarter] = ['mileage' => $values->mileage, 'reimbursement' => $values->reimbursement,
                    'non_taxable' => $values->limit, 'taxable' => $values->taxable];

                $total_fixed += $values->fixed_reimbursement;
                $total_variable += $values->variable_reimbursement;
                $total_reimbursement += $values->reimbursement;
                $total_mileage += $values->mileage;
                $total_not_taxable += $values->limit;
                $total_taxable += $values->taxable;

                // populate tax data row for each quarter
                $row_id = $id_of_row[$quarter];
                $tax_data[$row_id][1] = $row_id;
                $tax_data[$row_id][2] = $values->mileage;
                $tax_data[$row_id][3] = $values->fixed_reimbursement;
                $tax_data[$row_id][4] = $values->variable_reimbursement;
                $tax_data[$row_id][5] = $values->reimbursement;
                $tax_data[$row_id][6] = $values->limit;
                $tax_data[$row_id][7] = $values->taxable;


                $tax_details[$quarter][$column_name[1]] = $row_id;
                $tax_details[$quarter][$column_name[2]] = $values->mileage;
                $tax_details[$quarter][$column_name[3]] = $values->fixed_reimbursement;
                $tax_details[$quarter][$column_name[4]] = $values->variable_reimbursement;
                $tax_details[$quarter][$column_name[5]] = $values->reimbursement;
                $tax_details[$quarter][$column_name[6]] = round($values->limit,2);
                $tax_details[$quarter][$column_name[7]] = round($values->taxable,2);

            }   // for each $quarter

            // check if we are below 5000, as if yes this total is stored in row
            $quarter_dates = $this->quarterDates($quarter, $quarter_months); // start and stop date of this quarter
            if ($this->missing5000MilesCompliance($user_id, $year) || $this->belowPartial5000Miles($user_id, $quarter_dates)) {

                $tax_data[11][1] = 11;
                $tax_data[11][2] = $total_mileage;
                $tax_data[11][5] = $total_reimbursement;
                $tax_data[11][6] = $total_not_taxable;
                $tax_data[11][7] = $total_taxable;

                $tax_details['QUARTERLY TOTALS'] = ['mileage' => $total_mileage, 'reimbursement' => $total_reimbursement,
                    'non_taxable' => round($total_not_taxable,2), 'taxable' => round($total_taxable,2)];

                // Calculate annual taxes.
                // Difference between annnual and above totals will be assigned to 5000 miles adjustments
                $data = $this->getAnnual463TaxAdjustment($user_id, $year, $quarter_months_years, $standard_rate_array);
                $annual_mileage = $data->mileage;
                $annual_reimbursement = $data->reimbursement;
                $annual_not_taxable = $data->limit;
                $annual_taxable = $data->taxable;

                $diff_mileage = $annual_taxable != 0 ? round($annual_mileage - $total_mileage, 2) : null;
                $diff_reimbursement = $annual_taxable != 0 ? round($annual_reimbursement - $total_reimbursement, 2) : null;
                $diff_limit = $annual_taxable != 0 ? round($annual_not_taxable - $total_not_taxable, 2) : null;
                $diff_taxable = $annual_taxable != 0 ? round($annual_taxable - $total_taxable, 2) : null;

                $key = 'FAVR ADJUSTMENT (No 5,000 Miles Limit Reached)';
                $tax_details[$key] = ['mileage' => $diff_mileage, 'reimbursement' => $diff_reimbursement,
                    'non_taxable' => $diff_limit, 'taxable' => $diff_taxable];


                $total_row_id = $id_of_row[$key];
                $tax_data[$total_row_id][1] = $row_id;
                $tax_data[$total_row_id][2] = $diff_mileage;
                $tax_data[$total_row_id][5] = $diff_reimbursement;
                $tax_data[$total_row_id][6] = $diff_limit;
                $tax_data[$total_row_id][7] = $diff_taxable;


                $tax_summary['TOTAL'] = ['mileage' => $annual_mileage, 'reimbursement' => $annual_reimbursement,
                    'non_taxable' => $total_not_taxable, 'taxable' => $annual_taxable];

                $key = 'TOTAL';
                $total_row_id = $id_of_row[$key];
                $tax_data[$total_row_id][1] = $row_id;
                $tax_data[$total_row_id][2] = $annual_mileage;
                //        $tax_data[$total_row_id][3] = $annual_fixed;
                //        $tax_data[$total_row_id][4] = $annual_variable;
                $tax_data[$total_row_id][5] = $annual_reimbursement;
                $tax_data[$total_row_id][6] = $annual_not_taxable;
                $tax_data[$total_row_id][7] = $annual_taxable;


            } else {

                $tax_summary['TOTAL'] = ['mileage' => $total_mileage, 'reimbursement' => $total_reimbursement,
                    'non_taxable' => $total_not_taxable, 'taxable' => $total_taxable];

                // populate tax_details row for totals
                $key = 'TOTAL';
                $total_row_id = $id_of_row[$key];
                $tax_data[$total_row_id][1] = $row_id;
                $tax_data[$total_row_id][2] = $total_mileage;
                $tax_data[$total_row_id][3] = $total_fixed;
                $tax_data[$total_row_id][4] = $total_variable;
                $tax_data[$total_row_id][5] = $total_reimbursement;
                $tax_data[$total_row_id][6] = $total_not_taxable;
                $tax_data[$total_row_id][7] = $total_taxable;

            }

            // $this->storeTaxDetails($user_id, $year, $tax_data);

        }

        return array(
            'quarter_month_info' => $quarter_month_info,
            'tax_details' => $tax_details,
            'tax_summary' => $tax_summary
        );
    }

    private function driverOneQuarterTaxFAVR($user_id, $company_id, $year, $months_year, $standard_rate_array)
    {
        $quarter_fixed = 0;
        $quarter_variable = 0;
        $quarter_mileage = 0;
        $quarter_not_taxable = 0;

        foreach ($months_year as $mm => $yyyy) {

            $reimb = \DB::table('monthly_mileages as mm')
                ->leftJoin('monthly_reimbursements as mr', function ($join) {
                    $join->on('mm.user_id', '=', 'mr.user_id');
                    $join->on('mm.month', '=', 'mr.month');
                    $join->on('mm.year', '=', 'mr.year');
                })
                ->join('driver_profiles as dp', 'mr.driver_profile_id', '=', 'dp.id')
                ->where('mm.user_id', '=', $user_id)
                ->where('mm.year', '=', $yyyy)
                ->where('mm.month', '=', $mm)
                ->select('mr.fixed_reimbursement', 'mr.adjustment_amount', 'mr.variable_reimbursement',
                    'mr.fixed_status', 'mr.within_grace_period', 'mr.month', 'dp.street_light_id',
                    'mm.business_mileage')
                ->first();

            // \Debugbar::info($reimb);

            if ($reimb != null) {

                $fixed_status = $reimb->fixed_status;
                $in_grace = $reimb->within_grace_period;

                if ($in_grace) {
                    $fixed_status = $this->removeFromStreetLight($fixed_status, GREEN_NO_VEHICLE_COST_COMPLIANCE);
                    $fixed_status = $this->removeFromStreetLight($fixed_status, GREEN_NO_VEHICLE_AGE_COMPLIANCE);
                    $fixed_status = $this->removeFromStreetLight($fixed_status, GREEN_NO_INSURANCE_COMPLIANCE);
                    $fixed_status = $this->removeFromStreetLight($fixed_status, RED_AWAITING_INSURANCE);
                    $fixed_status = $this->removeFromStreetLight($fixed_status, RED_INSUFFICIENT_INSURANCE);
                    $fixed_status = $this->removeFromStreetLight($fixed_status, RED_INSURANCE_EXPIRED);
                }
                // clear 5000 miles compliance as we have code below to handle it, it is on top of other taxes.
                $fixed_status = $this->removeFromStreetLight($fixed_status, GREEN_NO_5000_MILES_COMPLIANCE);

                if ($year < 2016) {
                    //CCS-186 found crazy code, but we have to be back compatible
                    if ($reimb->month == 12 && ($fixed_status & GREEN_NO_VEHICLE_AGE_COMPLIANCE) > 0) {
                        if ($this->vehicle_too_old_for_new_year($user_id, $year + 1)) { // TODO convert vehicle_too_old_for_new_year()
                            $fixed_status = $this->removeFromStreetLight($fixed_status, GREEN_NO_VEHICLE_AGE_COMPLIANCE);
                        }
                    }
                }


                // only months where we have no compliances should be included
                if ($fixed_status > GREEN_REIMBURSEMENT) {
                    $fixed_reimbursement = $reimb->fixed_reimbursement - $reimb->adjustment_amount;
                    $mileage = ($this->colorOfStreetLight($fixed_status) == 'Green') ? $reimb->business_mileage : 0;
                } else {
                    $fixed_reimbursement = 0;
                    $mileage = 0;
                }

                // \Debugbar::info("mm=$mm grace=$in_grace status=$fixed_status mi=$mileage fr=$fixed_reimbursement");

                $quarter_mileage += $mileage;
                $quarter_fixed += $fixed_reimbursement;
                $variable = round($this->getVariableReimbursement($company_id, $year, $mm, $reimb->variable_reimbursement, $mileage), 2);
                $quarter_variable += $variable;

                if (!($fixed_status & GREEN_NO_DEPRECIATION_COMPLIANCE)) {             // all taxable
                    $taxable_limit = $mileage * $standard_rate_array[(int)$mm] / 100;   // was not rounded on production :-( Do not change it as it will create 1 cent dicrepancy
                    // \Debugbar::info("limit = $taxable_limit = $mileage * " . $standard_rate_array[(int)$mm] . " / 100;");
                    $quarter_not_taxable += $taxable_limit;
                    // \Debugbar::info("$mm => $quarter_not_taxable += $mileage * " .
                    //    $standard_rate_array[(int)$mm] . " / 100;");
                } else {
                    $taxable_limit = 0;
                }
            }

        } // for each month within quarter

        $quarter_reimbursement = $quarter_fixed + $quarter_variable;
        $quarter_not_taxable = round($quarter_not_taxable, 2);
        $quarter_taxable = max(0, $quarter_reimbursement - $quarter_not_taxable);

        return (object)['mileage' => $quarter_mileage, 'reimbursement' => $quarter_reimbursement,
            'fixed_reimbursement' => $quarter_fixed, 'variable_reimbursement' => $quarter_variable,
            'limit' => $quarter_not_taxable, 'taxable' => $quarter_taxable];

    }

    public function getDriverAllQuartersTaxWM(Request $request)
    {
        $user_id = $request->user_id;
        $company_id = $request->company_id;
        $year = $request->year;
        $columns = $request->show_columns;
        $current_status = null;
        $previous_status = null;

        $tax_details = [];
        $tax_summary = [];
        $quarter_month_info = [];
        $tax_values = [];

        \Debugbar::info("Calling driverAllQuartersTaxWM ...");

        $rec = DriverTaxColumn::select('id', 'name')->orderBy('id')->get();
        foreach ($rec as $value) {
            $column_name[$value->id] = $value->name;
        }
        // check if we already stored these taxes
        $driver_tax = DriverTax::select('id')->where('user_id', $user_id)->where('year', $year)->first();
        if ($driver_tax != null && $driver_tax->id > 0) {

            $res = $this->getTaxAdjustmentsFromTable($driver_tax->id, $columns);
            $tax_details = $res['tax_details'];
            $tax_summary = $res['tax_summary'];

        } else {

            $quarter_months_years = $this->getQuarterMonthsYearsRelations($company_id, $year);
            $quarter_month_info = $this->getInformationOnQuarterMonths($quarter_months_years);
            $standard_rate_array = $this->getIRRStandardRates($quarter_months_years);

            $id_of_row = $this->getTaxRowIds(); // reverse array of driver tax_rows table

            \Debugbar::warning('$id_of_row',$id_of_row);

            // calculating taxable adjustments
            $total_fixed = 0;
            $total_variable = 0;
            $total_reimbursement = 0;
            $total_mileage = 0;
            $total_not_taxable = 0;
            $total_taxable = 0;


            \Debugbar::error('$quarter_months_years',$quarter_months_years);

            foreach ($quarter_months_years as $quarter => $months_year) {

                $data = $this->driverOneQuarterTaxWM($user_id, $quarter, $year, $months_year, $standard_rate_array);

                $tax_values[$quarter] = ['mileage' => $data->mileage, 'reimbursement' => $data->reimbursement,
                    'non_taxable' => $data->limit, 'taxable' => $data->taxable];

                $total_fixed += $data->fixed_reimbursement;
                $total_variable += $data->variable_reimbursement;
                $total_reimbursement += $data->reimbursement;
                $total_mileage += $data->mileage;
                $total_not_taxable += $data->limit;
                $total_taxable += $data->taxable;

                // populate tax data row for each quarter
                $row_id = $id_of_row[$quarter];
                $tax_data[$row_id][1] = $row_id;
                $tax_data[$row_id][2] = $data->mileage;
                $tax_data[$row_id][3] = $data->fixed_reimbursement;
                $tax_data[$row_id][4] = $data->variable_reimbursement;
                $tax_data[$row_id][5] = $data->reimbursement;
                $tax_data[$row_id][6] = $data->limit;
                $tax_data[$row_id][7] = $data->taxable;


                $tax_details[$quarter][$column_name[1]] = $row_id;
                $tax_details[$quarter][$column_name[2]] = $data->mileage;
                $tax_details[$quarter][$column_name[3]] = $data->fixed_reimbursement;
                $tax_details[$quarter][$column_name[4]] = $data->variable_reimbursement;
                $tax_details[$quarter][$column_name[5]] = $data->reimbursement;
                $tax_details[$quarter][$column_name[6]] = $data->limit;
                $tax_details[$quarter][$column_name[7]] = $data->taxable;

            }   // for each $quarter

            // check if we are below 5000, as if yes this total is stored in row
            $only_stop_date = false;
            $annnual_5000 = true;
            $quarter_dates = $this->annualDates($quarter_months_years);
            if ($this->missing5000MilesCompliance($user_id, $year)
                || date("Y") > 2018 && $this->belowPartial5000Miles($user_id, $quarter_dates, $only_stop_date, $annnual_5000)) {

                $tax_data[11][1] = 11;
                $tax_data[11][2] = $total_mileage;
                $tax_data[11][5] = $total_reimbursement;
                $tax_data[11][6] = $total_not_taxable;
                $tax_data[11][7] = $total_taxable;

                $tax_details['QUARTERLY TOTALS'] = ['mileage' => $total_mileage, 'reimbursement' => $total_reimbursement,
                    'non_taxable' => $total_not_taxable, 'taxable' => $total_taxable];

                // Calculate annual taxes.
                // Difference between annnual and above totals will be assigned to 5000 miles adjustments
                if ($year >= 2014) {
                    $data = $this->getAnnualWMTaxAdjustment($company_id, $user_id, $year, $quarter_months_years, $standard_rate_array);
                } else {
                    $data = $this->getAnnual463TaxAdjustment($user_id, $year, $quarter_months_years, $standard_rate_array);
                }
                $annual_mileage = $data->annual_mileage;
                $annual_reimbursement = $data->annual_reimbursement;
                $annual_not_taxable = $data->annual_limit;
                $annual_taxable = $data->annual_taxable;

                $diff_mileage = $annual_taxable != 0 ? round($annual_mileage - $total_mileage, 2) : null;
                $diff_reimbursement = $annual_taxable != 0 ? round($annual_reimbursement - $total_reimbursement, 2) : null;
                $diff_limit = $annual_taxable != 0 ? round($annual_not_taxable - $total_not_taxable, 2) : null;
                $diff_taxable = $annual_taxable != 0 ? round($annual_taxable - $total_taxable, 2) : null;

                $key = 'FAVR ADJUSTMENT (No 5,000 Miles Limit Reached)';
                $tax_details[$key] = ['mileage' => $diff_mileage, 'reimbursement' => $diff_reimbursement,
                    'non_taxable' => $diff_limit, 'taxable' => $diff_taxable];


                $total_row_id = $id_of_row[$key];
                $tax_data[$total_row_id][1] = $row_id;
                $tax_data[$total_row_id][2] = $diff_mileage;
                $tax_data[$total_row_id][5] = $diff_reimbursement;
                $tax_data[$total_row_id][6] = $diff_limit;
                $tax_data[$total_row_id][7] = $diff_taxable;


                $tax_summary['TOTAL'] = ['mileage' => $annual_mileage, 'reimbursement' => $annual_reimbursement,
                    'non_taxable' => $annual_not_taxable, 'taxable' => $annual_taxable];

                $key = 'TOTAL';
                $total_row_id = $id_of_row[$key];
                $tax_data[$total_row_id][1] = $row_id;
                $tax_data[$total_row_id][2] = $annual_mileage;
                //        $tax_data[$total_row_id][3] = $annual_fixed;
                //        $tax_data[$total_row_id][4] = $annual_variable;
                $tax_data[$total_row_id][5] = $annual_reimbursement;
                $tax_data[$total_row_id][6] = round($annual_not_taxable,2);
                $tax_data[$total_row_id][7] = round($annual_taxable,2);


            } else {

                $tax_summary['TOTAL'] = ['mileage' => $total_mileage, 'reimbursement' => $total_reimbursement,
                    'non_taxable' => round($total_not_taxable,2), 'taxable' => round($total_taxable,2)];

                // populate tax_details row for totals
                $key = 'TOTAL';
                $total_row_id = $id_of_row[$key];
                $tax_data[$total_row_id][1] = $row_id;
                $tax_data[$total_row_id][2] = $total_mileage;
                $tax_data[$total_row_id][3] = $total_fixed;
                $tax_data[$total_row_id][4] = $total_variable;
                $tax_data[$total_row_id][5] = $total_reimbursement;
                $tax_data[$total_row_id][6] = $total_not_taxable;
                $tax_data[$total_row_id][7] = $total_taxable;

            }

            // $this->storeTaxDetails($user_id, $year, $tax_data);

        }

        return array(
            'quarter_month_info' => $quarter_month_info,
            'tax_details' => $tax_details,
            'tax_summary' => $tax_summary
        );
    }

    private function driverOneQuarterTaxWM($user_id, $quarter, $year, $months_year, $standard_rate_array)
    {

        // reset quarter totals
        $quarter_fixed = 0;
        $quarter_variable = 0;
        $quarter_mileage = 0;
        $quarter_limit = 0;


        foreach ($months_year as $mm => $yyyy) {

            $taxable = 0;
            $last_yyyy = $mm == 1 ? $yyyy - 1 : $yyyy;
            $last_mm = $mm == 1 ? 12 : $mm - 1;

            if ($year < 2014) { // error found in 2014: mr_last_month.fixed_status AS  current_status,
                $reimb = \DB::select(\DB::raw("SELECT mm.business_mileage,
mr_last_month.fixed_reimbursement,
mr_last_month.adjustment_amount,
mr.variable_reimbursement, mm.user_id,
mr_last_month.fixed_status AS  current_status,
mr_last_month.fixed_status AS previous_status,
u.last_name, u.first_name,
mr_last_month.within_grace_period
FROM users u
LEFT JOIN monthly_reimbursements mr ON mr.user_id = u.id AND mr.year = $yyyy AND mr.month = $mm
LEFT JOIN monthly_reimbursements mr_last_month ON mr_last_month.year = $last_yyyy AND mr_last_month.month = $last_mm AND mr_last_month.user_id = u.id
LEFT JOIN monthly_mileages mm ON mm.user_id = u.id AND mm.user_id = u.id and mm.year = $yyyy and mm.month = $mm
WHERE u.id = $user_id "))[0];
            } else {
                $reimb = \DB::select(\DB::raw("SELECT mm.business_mileage,
mr_last_month.fixed_reimbursement,
mr_last_month.adjustment_amount,
mr.variable_reimbursement, mm.user_id,
mr.fixed_status AS  current_status,
mr_last_month.fixed_status AS previous_status,
u.last_name, u.first_name,
mr_last_month.within_grace_period
FROM users u
LEFT JOIN monthly_reimbursements mr ON mr.user_id = u.id AND mr.year = $yyyy AND mr.month = $mm
LEFT JOIN monthly_reimbursements mr_last_month ON mr_last_month.year = $last_yyyy AND mr_last_month.month = $last_mm AND mr_last_month.user_id = u.id
LEFT JOIN monthly_mileages mm ON mm.user_id = u.id AND mm.user_id = u.id and mm.year = $yyyy AND mm.month = $mm
WHERE u.id = $user_id "))[0];
            }

            if ($reimb != null) {

                $current_status = $reimb->current_status;
                $previous_status = $reimb->previous_status;
                $in_grace = $reimb->within_grace_period;
                $mileage = $reimb->business_mileage;

                // we goofed as we sent Q1 after we discovered it and than we did not activated it for 2015
                if ($quarter == 'Q1' && $mm == 1 && $current_status == 2 && $year < 2016) {
                    $previous_status = 2;
                }

                if ($in_grace) {
                    // fixed for previous month
                    $previous_status = $this->removeFromStreetLight($previous_status, GREEN_NO_VEHICLE_COST_COMPLIANCE);
                    $previous_status = $this->removeFromStreetLight($previous_status, GREEN_NO_VEHICLE_AGE_COMPLIANCE);
                    $previous_status = $this->removeFromStreetLight($previous_status, GREEN_NO_INSURANCE_COMPLIANCE);
                    $previous_status = $this->removeFromStreetLight($previous_status, RED_AWAITING_INSURANCE);
                    $previous_status = $this->removeFromStreetLight($previous_status, RED_INSUFFICIENT_INSURANCE);
                    $previous_status = $this->removeFromStreetLight($previous_status, RED_INSURANCE_EXPIRED);
                    // variable for current month
                    $current_status = $this->removeFromStreetLight($current_status, GREEN_NO_VEHICLE_COST_COMPLIANCE);
                    $current_status = $this->removeFromStreetLight($current_status, GREEN_NO_VEHICLE_AGE_COMPLIANCE);
                    $current_status = $this->removeFromStreetLight($current_status, GREEN_NO_INSURANCE_COMPLIANCE);
                    $current_status = $this->removeFromStreetLight($current_status, RED_AWAITING_INSURANCE);
                    $current_status = $this->removeFromStreetLight($current_status, RED_INSUFFICIENT_INSURANCE);
                    $current_status = $this->removeFromStreetLight($current_status, RED_INSURANCE_EXPIRED);
                }

                // clear 5000 miles compliance as we have code below to handle it, it is on top of other taxes.
                $previous_status = $this->removeFromStreetLight($previous_status, GREEN_NO_5000_MILES_COMPLIANCE);
                $current_status = $this->removeFromStreetLight($current_status, GREEN_NO_5000_MILES_COMPLIANCE);

                // only months where we have no compliance should be included
                if ($previous_status > GREEN_REIMBURSEMENT) {
                    $fixed_reimbursement = $reimb->fixed_reimbursement;
                    $adjustment_amount = $reimb->adjustment_amount;
                } else {
                    $fixed_reimbursement = 0;
                    $adjustment_amount = 0;
                }

                // WM has advance payment
                $real_fixed_status = ( $year < 2014 ) ? $current_status : $previous_status;

                if ( $real_fixed_status > GREEN_REIMBURSEMENT && !$in_grace ) {
                    $variable = round($mileage * $reimb->variable_reimbursement / 100, 2);
                } else {
                    $mileage = 0;
                    $variable = 0;
                }

                if( !$in_grace || $year < 2014) {
                    $quarter_mileage += $mileage;
                    $fixed_reimbursement = round($fixed_reimbursement - $adjustment_amount, 2);
                    $quarter_fixed += $fixed_reimbursement;
                    $variable_reimbursement = round( $mileage * $reimb->variable_reimbursement / 100 ,2);
                    $quarter_variable += $variable_reimbursement;
                    $reimbursement = $fixed_reimbursement + $variable_reimbursement;
                    $limit = $mileage * $standard_rate_array[$mm] / 100;
                    // \Debugbar::info("limit = $limit based on $mileage * $standard_rate_array[$mm]");
                    $quarter_limit += $limit;
                } else {
                    $reimbursement = 0;
                    $limit = 0;
                }

                if( date("Y-m-d") <= CUT_OFF_DATE ) { // we have bug on production. We already calculated it !!!!
                    // \Debugbar::info("BE BACK COMPATIBLE COMPARE " . date("Y-m-d") . ' vs ' . CUT_OFF_DATE);
                    $quarter_limit = round($quarter_mileage * $standard_rate_array[$mm] / 100, 2);
                   //  \Debugbar::info("limit = $quarter_limit based on $quarter_mileage * $standard_rate_array[$mm] / 100 ");
                }

                if( $current_status & GREEN_NO_DEPRECIATION_COMPLIANCE) {
                    $taxable = $reimbursement;
                } else {
                    $taxable = $reimbursement - $limit;
                }
            }

        } // for each month within quarter
        $quarter_limit = round($quarter_limit,2);

        $quarter_reimbursement = $quarter_fixed + $quarter_variable;
        $quarter_taxable = max(0, $quarter_reimbursement - $quarter_limit);


        return (object)['mileage' => $quarter_mileage, 'reimbursement' => $quarter_reimbursement,
            'fixed_reimbursement' => $quarter_fixed, 'variable_reimbursement' => $quarter_variable,
            'limit' => $quarter_limit, 'taxable' => $quarter_taxable];
    }

    /**
     * We can call everything here !!
     *
     * @param Request $request
     * @return array
     */
    public function getDriverAllQuartersTax463(Request $request)
    {
        $user_id = $request->user_id;
        $company_id = $request->company_id;
        $year = $request->year;
        $mileage_entry_type = $request->mileage_entry_type;
        $columns = $request->show_columns;
        $selected_quarter = $request->selected_quarter;

        $tax_details = [];
        $tax_summary = [];

        $rec = DriverTaxColumn::select('id', 'name')->orderBy('id')->get();
        foreach ($rec as $value) {
            $column_name[$value->id] = $value->name;
        }

        // check if we already stored these taxes
        $driver_tax = DriverTax::select('id')->where('user_id', $user_id)->where('year', $year)->first();

        if ($driver_tax != null && $driver_tax->id > 0) {

            $res = $this->getTaxAdjustmentsFromTable($driver_tax->id, $columns);
            $tax_details = $res['tax_details'];
            $tax_summary = $res['tax_summary'];

        } else {

            $quarter_months = $this->getQuarterMonthsYearsRelations($company_id, $year);
            $quarter_month_info = $this->getInformationOnQuarterMonths($quarter_months);
            $standard_rate_array = $this->getIRRStandardRates($quarter_months);

            $id_of_row = $this->getTaxRowIds(); // reverse array of driver tax_rows table

            // calculating taxable adjustments
            $total_fixed = 0;
            $total_variable = 0;
            $total_reimbursement = 0;
            $total_mileage = 0;
            $total_not_taxable = 0;
            $total_taxable = 0;

            foreach ($quarter_months as $quarter => $quarter_months_year) {

                if ($selected_quarter != '-1' && $selected_quarter != $quarter) {
                    continue;
                }

                $values = $this->driverOneQuarterTax463($user_id, $company_id, $year, $mileage_entry_type, $quarter_months_year, $standard_rate_array);

                $quarter_mileage = $values->mileage;
                $quarter_reimbursement = $values->reimbursement;
                $quarter_fixed = $values->fixed_reimbursement;
                $quarter_variable = $values->variable_reimbursement;
                $quarter_not_taxable = $values->limit;
                $quarter_taxable = $values->taxable;


                $tax_values[$quarter] = ['mileage' => $quarter_mileage, 'reimbursement' => $quarter_reimbursement,
                    'non_taxable' => $quarter_not_taxable, 'taxable' => $quarter_taxable];

                $total_fixed += $quarter_fixed;
                $total_variable += $quarter_variable;
                $total_reimbursement += $quarter_reimbursement;
                $total_mileage += $quarter_mileage;
                $total_not_taxable += $quarter_not_taxable;
                $total_taxable += $quarter_taxable;

                // populate tax data row for each quarter
                $row_id = $id_of_row[$quarter];
                $tax_data[$row_id][1] = $row_id;
                $tax_data[$row_id][2] = $quarter_mileage;
                $tax_data[$row_id][3] = $quarter_fixed;
                $tax_data[$row_id][4] = $quarter_variable;
                $tax_data[$row_id][5] = $quarter_reimbursement;
                $tax_data[$row_id][6] = $quarter_not_taxable;
                $tax_data[$row_id][7] = $quarter_taxable;


                $tax_details[$quarter][$column_name[1]] = $row_id;
                $tax_details[$quarter][$column_name[2]] = $quarter_mileage;
                $tax_details[$quarter][$column_name[3]] = $quarter_fixed;
                $tax_details[$quarter][$column_name[4]] = round($quarter_variable,2);
                $tax_details[$quarter][$column_name[5]] = round($quarter_reimbursement,2);
                $tax_details[$quarter][$column_name[6]] = round($quarter_not_taxable,2);
                $tax_details[$quarter][$column_name[7]] = round($quarter_taxable,2);

            }   // for each $quarter

            $tax_summary['TOTAL'] = ['mileage' => $total_mileage, 'reimbursement' => $total_reimbursement,
                'non_taxable' => round($total_not_taxable,2), 'taxable' => round($total_taxable,2)];

            // populate tax_details row for totals
            $key = 'TOTAL';
            $total_row_id = $id_of_row[$key];
            $tax_data[$total_row_id][1] = $row_id;
            $tax_data[$total_row_id][2] = $total_mileage;
            $tax_data[$total_row_id][3] = $total_fixed;
            $tax_data[$total_row_id][4] = round($total_variable,2);
            $tax_data[$total_row_id][5] = round($total_reimbursement,2);
            $tax_data[$total_row_id][6] = round($total_not_taxable,2);
            $tax_data[$total_row_id][7] = round($total_taxable,2);

            /// $this->storeTaxDetails($user_id, $year, $tax_data);

        }

        return array(
            'quarter_month_info' => $quarter_month_info,
            'tax_details' => $tax_details,
            'tax_summary' => $tax_summary
        );
    }

    private function driverOneQuarterTax463($user_id, $company_id, $year, $mileage_entry_type, $quarter_months_year, $standard_rate_array)
    {
        // reset quarter totals
        $quarter_fixed = 0;
        $quarter_variable = 0;
        $quarter_mileage = 0;
        $quarter_not_taxable = 0;

        foreach ($quarter_months_year as $mm => $yyyy) {

            $reimb = \DB::table('monthly_mileages as mm')
                ->leftJoin('monthly_reimbursements as mr', function ($join) {
                    $join->on('mm.user_id', '=', 'mr.user_id');
                    $join->on('mm.month', '=', 'mr.month');
                    $join->on('mm.year', '=', 'mr.year');
                })
                ->join('driver_profiles as dp', 'mm.user_id', '=', 'dp.user_id')
                ->where('mm.user_id', '=', $user_id)
                ->where('mm.year', '=', $yyyy)
                ->where('mm.month', '=', $mm)
                ->select('mr.fixed_reimbursement', 'mr.adjustment_amount', 'mr.variable_reimbursement', 'dp.street_light_id', 'mm.business_mileage')
                ->first();

            // Before 2011 we were included commuter mileage which was incorrect.
            $commuter = 0;
            if ($mileage_entry_type == 'Daily Commuter' && $year <= 2011) {
                $yyyymm = sprintf('%04d-%02d', $yyyy, $mm);
                $rec = \DB::table('daily_mileages')->where('user_id', $user_id)->where('trip_date', 'like', $yyyymm)->sum('commuter_mileage')->first();
            }

            if ($reimb != null) {

                $quarter_mileage += $reimb->business_mileage + $commuter;

                $quarter_fixed += $reimb->fixed_reimbursement - $reimb->adjustment_amount;
                $variable = round($this->getVariableReimbursement($company_id, $year, $mm, $reimb->variable_reimbursement, $reimb->business_mileage), 2);
                $quarter_variable += $variable;
                $quarter_not_taxable += $reimb->business_mileage * $standard_rate_array[$mm] / 100;

            }

        } // for each month within quarter

        $quarter_reimbursement = $quarter_fixed + $quarter_variable;
        $quarter_not_taxable = round($quarter_not_taxable, 2);
        $quarter_taxable = max(0, $quarter_reimbursement - $quarter_not_taxable);

        return (object)['mileage' => $quarter_mileage, 'reimbursement' => $quarter_reimbursement,
            'fixed_reimbursement' => $quarter_fixed, 'variable_reimbursement' => $quarter_variable,
            'limit' => $quarter_not_taxable, 'taxable' => $quarter_taxable];
    }

    private function getYearsFromQuarters($quarter_months)
    {
        $years = [];
        $found_year = '';
        foreach ($quarter_months as $months_years) {
            foreach ($months_years as $year) {
                if ($year != $found_year) {
                    $years[] = (int)$year;
                    $found_year = $year;
                }
            }
        }
        return array_unique($years);
    }

    public function getCompanyAnnualTax(Request $request) {
        $country_id = $request->country_id;
        $company_id = $request->company_id;
        $division_id = $request->division_id;
        $year = $request->year;

        $company = Company::find($company_id);
        $service_plan = $company->options->service_plan;
        $mileage_entry = $company->mileage->mileage_entry_type;

        $quarter_months = $this->getQuarterMonthsYearsRelations($company_id, $year);
        $standard_rate_array = $country_id == USA ?
            $this->getIRRStandardRates($quarter_months)
            :
            $this->getCRARates($quarter_months);

        $results = [];
        $found = 0;

        if( $year <= 2014) {
            $year_clause = "  mr.year = $year ";
        } else {
            $last_year = $year - 1;
            $next_year = $year + 1;
            $year_clause = " ( mr.year = $year OR mr.year = $last_year OR mr.year = $next_year ) ";
        }
        $division_clause = $division_id > 0 ? " AND dp.division_id = $division_id " : null;

        // select drivers: TODO what happened to business unit?
        $query = "SELECT u.id AS user_id, u.first_name, u.last_name, dp.employee_number,
        di.name AS division, dp.street_light_id, dp.cost_center, u.email,
        dp.start_date, dp.stop_date
        FROM users u JOIN driver_profiles dp ON u.id = dp.user_id
        JOIN divisions di ON di.id = dp.division_id
        WHERE dp.company_id = $company_id $division_clause
        AND dp.id = 
                  ( SELECT driver_profile_id FROM monthly_reimbursements mr 
                  WHERE mr.user_id = u.id AND $year_clause ORDER BY mr.month DESC LIMIT 1 )
        ORDER BY u.last_name, u.first_name";

        \Debugbar::info($query);

        $total_mileage = 0;
        $total_fixed_reimbursement = 0;
        $total_variable_reimbursement = 0;
        $total_reimbursement = 0;
        $total_limit = 0;
        $total_taxable = 0;

        $records = \DB::select(\DB::raw("$query"));

        foreach ($records as $record) {
           // \Debugbar::info("Processing $record->last_name $record->first_name");

            $line_arr = ['last_name' => $record->last_name, 'first_name' => $record->first_name,
                'employee_number' => $record->employee_number, 'division' => $record->division];


            $driver_mileage = 0;
            $driver_fixed_reimbursement = 0;
            $driver_variable_reimbursement = 0;
            $driver_reimbursement = 0;
            $driver_limit = 0;
            $driver_taxable = 0;

            $accumulative_mileage = 0; // for Canadian Drivers

            foreach ( $quarter_months as $quarter => $months_years) {
                if( $country_id == USA ) {
                    if( $service_plan == PLAN_463) {
                        $data = $this->driverOneQuarterTax463($record->user_id, $company_id, $year, $mileage_entry, $months_years, $standard_rate_array );
                    } else if( $company_id == WASTE_MANAGEMENT_USA) {
                        $data = $this->DriverOneQuarterTaxWM($record->user_id, $quarter, $year, $months_years, $standard_rate_array);
                    } else {
                        $data = $this->DriverOneQuarterTaxFAVR($record->user_id, $company_id, $year, $months_years, $standard_rate_array);
                    }
                } else {
                    $data = $this->driverAllQuartersTaxCanadaDriver($record->user_id, $company_id, $year, $mileage_entry, $months_years, $standard_rate_array, $accumulative_mileage);
                    $accumulative_mileage = $data->accumulated_mileage;
                }
                if( $data) {
                    $driver_mileage += $data->mileage;
                    $driver_fixed_reimbursement += $data->fixed_reimbursement;
                    $driver_variable_reimbursement += $data->variable_reimbursement;
                    $driver_reimbursement += $data->reimbursement;
                    $driver_limit += $data->limit;
                    $driver_taxable += $data->taxable;
                }
            }
            $driver_limit = round($driver_limit,2);


            if( $driver_taxable > 0 ) {
                $found++;
                $line_arr['mileage'] = $driver_mileage;
                $line_arr['fixed_reimbursement'] = $driver_fixed_reimbursement;
                $line_arr['variable_reimbursement'] = $driver_variable_reimbursement;
                $line_arr['reimbursement'] = $driver_reimbursement;
                $line_arr['limit'] = $driver_limit;
                $line_arr['taxable'] = $driver_taxable;

                $results[] = $line_arr;

                $total_mileage += $driver_mileage;
                $total_fixed_reimbursement += $driver_fixed_reimbursement;
                $total_variable_reimbursement += $driver_variable_reimbursement;
                $total_reimbursement += $driver_reimbursement;
                $total_limit += $driver_limit;
                $total_taxable += $driver_taxable;
            }
        }

        $totals = ['mileage'=>$total_mileage,
            'fixed_reimbursement'=>$total_fixed_reimbursement,
            'variable_reimbursement'=>$total_variable_reimbursement,
            'reimbursement'=>$total_reimbursement,
            'limit'=>$total_limit,
            'taxable'=>$total_taxable];

        return ['results'=>$results, 'totals'=>$totals, 'length' => $found];
    }

    public function getCompanyOneQuarterTax(Request $request)
    {
        $company_id = $request->company_id;
        $division_id = $request->division_id;
        $year = $request->year;
        $quarter = $request->selected_quarter;
        $mileage_entry_type = $request->mileage_entry_type;
        $ignore_5000_miles = isset($request->ignore_5000_miles) ? $request->ignore_5000_miles : false;
        $compliance_5000_found = false;

        $company = Company::find($company_id);
        $service_plan = $company->options->service_plan;
        $country_id = $company->profile->country_id;
        $approval_end_day = $company->dates->mileage_approval_end_day + 1;    // report available after approval end day + 1

        $quarter_months = $this->getQuarterMonthsYearsRelations($company_id, $year);
        $quarter_dates = $this->quarterDates($quarter, $quarter_months); // start and stop date of this quarter
        $eoq = $quarter_dates->quarter_end_date;
        $eoq_plus = $eoq->addDay($approval_end_day)->format("F jS, Y");

        $today = Carbon::now();
        if( $today->lt($quarter_dates->quarter_start_date)) {
            $message = "The selected quarter $quarter has not started yet. Results of this quarter will available on $eoq_plus";
            return array("results" => null, 'totals' => null, 'compliance_5000_found' => null, 'message' => $message );
        } else if($today->lt($quarter_dates->quarter_end_date)) {
            $message = "The selected quarter $quarter is ongoing.  Results of this quarter will available on $eoq_plus";
            return array("results" => null, 'totals' => null, 'compliance_5000_found' => null, 'message' => $message );
        }
        $quarter_months_year = $quarter_months[$quarter];
        if ($country_id == USA) {
            $standard_rate_array = $this->getIRRStandardRates($quarter_months);
        } else {
            $cra_rates = $this->getCRARates($quarter_months);
        }
        $quarter_dates = $this->quarterDates($quarter, $quarter_months); // start and stop date of this quarter
        // \Debugbar::info("quarter_dates -> " . $quarter_dates->first_quarter_start_date->format('Y-m-d') . ' ' . $quarter_dates->quarter_start_date->format("Y-m-d") . ' ' . $quarter_dates->quarter_end_date->format("Y-m-d"));

        // quarter can start in previous year and driver will not have records in current year. Corrected in 2015
        $years = $this->getYearsFromQuarters($quarter_months);
        if (count($years) == 1) {
            $year_clause = "mr.year = $years[0]";
        } else
            $year_clause = "mr.year IN ( " . implode(',', $years) . ')';

        $division_clause = $division_id > 0 ? " AND dp.division_id = $division_id " : null;

        if ($company_id != WASTE_MANAGEMENT_USA) {
            $query = "SELECT u.id, u.last_name, u.first_name, dp.employee_number, di.name AS division, 
                dp.street_light_id, dp.start_date, dp.stop_date, dp.cost_center, u.email
                FROM users u
                JOIN driver_profiles dp ON u.id = dp.user_id
                JOIN divisions di ON dp.division_id = di.id
                WHERE dp.company_id = $company_id $division_clause        
                AND dp.id = 
                  ( SELECT driver_profile_id FROM monthly_reimbursements mr 
                  WHERE mr.user_id = u.id AND $year_clause ORDER BY mr.month DESC LIMIT 1 )
                ORDER BY u.last_name, u.first_name";
        } else {
            if ($quarter == 'Q1' && $year >= 2016) {
                $last_year = $year - 1;
                $last_year_status = " OR year = $last_year AND month = 12 ";
            } else {
                $last_year_status = '';
            }
            $query = "SELECT u.id, u.last_name, u.first_name, dp.employee_number, di.name AS division, 
                dp.street_light_id, dp.start_date, dp.stop_date, dp.cost_center, u.email
                FROM users u
                JOIN driver_profiles dp ON u.id = dp.user_id
                JOIN divisions di ON dp.division_id = di.id
                WHERE dp.company_id = $company_id  $division_clause
                AND dp.id = 
                  ( SELECT driver_profile_id FROM monthly_reimbursements mr 
                  WHERE mr.user_id = u.id AND ( $year $last_year_status ) AND mr.fixed_status > 2 ORDER BY mr.month DESC LIMIT 1 )
                ORDER BY u.last_name, u.first_name";

        }

        $records = \DB::select(\DB::raw("$query"));

        $results = [];
        $totals = ['mileage' => 0, 'reimbursement' => 0, 'limit' => 0, 'taxable' => 0];
        $total_mileage = 0;
        $total_reimbursement = 0;
        $total_limit = 0;
        $total_taxable = 0;
        $total_total_taxable = 0;
        $total_taxable5000 = 0;
        foreach ($records as $record) {

            if ($country_id == USA) {
                if ($service_plan == PLAN_463) {
                    $data = $this->driverOneQuarterTax463($record->id, $company_id, $year, $mileage_entry_type, $quarter_months_year, $standard_rate_array);
                } elseif ($company_id == WASTE_MANAGEMENT_USA) {
                    $data = $this->driverOneQuarterTaxWM($record->id, $quarter, $year, $quarter_months_year, $standard_rate_array);
                } else {
                    $data = $this->driverOneQuarterTaxFAVR($record->id, $company_id, $year, $quarter_months_year, $standard_rate_array);
                }
            } else {
                $data = $this->driverOneQuarterTaxCanada($record->id, $company_id, $year, $mileage_entry_type, $quarter_months_year, $cra_rates, 0);
            }

            $data->total_taxable = $data->taxable;
            $data->taxable5000 = null;
            if ($service_plan == PLAN_FAVR && $ignore_5000_miles == false ) { // to compare with production we have to remove this new option TODO remove and false
                // find who quit this quarter and was below 5000 * 12 / months
                $check_only_stop_date = true;
                $business_mileage = $this->belowPartial5000Miles($record->id, $quarter_dates, $check_only_stop_date);
                if ($business_mileage > 0) {
                    if( $company_id != WASTE_MANAGEMENT_USA) {
                        $data5000 = $this->driverAnnualTax($record->id, $quarter_dates, $business_mileage, $standard_rate_array);
                    } else {
                        $data5000 = $this->driverAnnualTaxWM($company_id, $record->id, $year, $quarter_months, $standard_rate_array);
                    }
                    if ($data5000->taxable > 0 && $data5000->taxable > $data->taxable) {  // we have to report extra taxation based on inefficient mileage
                        $compliance_5000_found = true;
                        $data5000->taxable5000 = $data5000->taxable - $data->taxable;
                        $data5000->total_taxable = $data5000->taxable;
                        // override data with data5000, except taxable;
                        $data5000->taxable = $data->taxable;
                        $data = $data5000;
                    } else {
                        $data->taxable5000 = 0;
                    }
                }
            }

            // if Q4, find if we have no 5000 compliance in  reimbursement, compare against entire year.
            if( $quarter == 'Q4' && $this->missing5000MilesCompliance($record->id, $year) ) {
                \Debugbar::info("Driver $record->last_name $record->first_name is bellow partial 5000");

                // get annual adjustment
                if( $company_id != WASTE_MANAGEMENT_USA) {
                    $annual_data = $this->getAnnual463TaxAdjustment($record->id, $year, $quarter_months, $standard_rate_array);
                } else {
                    $annual_data = $this->driverAnnualTaxWM($company_id, $record->id, $year, $quarter_months, $standard_rate_array);
                }

                // get sum of quarterly adjustments from 1 to 3 as we have Q4
                $already_taxable = $data->taxable;
                foreach ($quarter_months as $quarter => $months_year) {
                    if ( $quarter == 'Q4') {
                        continue;
                    }
                    if( $company_id != WASTE_MANAGEMENT_USA) {
                        $values = $this->driverOneQuarterTaxFAVR($record->id, $company_id, $year, $months_year, $standard_rate_array);
                    } else {
                        $values = $this->driverOneQuarterTaxWM($record->id, $quarter, $year, $months_year, $standard_rate_array);
                    }
                    $already_taxable += $values->taxable;
                }   // for each $quarter
                $already_taxable = round($already_taxable,2);

                // if annual taxable > sum of quarterly adjustments, display 5000 difference
                if( $annual_data->taxable > $already_taxable ) {
                    $data->taxable5000 = $annual_data->taxable - $already_taxable;
                    $data->total_taxable = $data->taxable5000 + $data->taxable;
                    $compliance_5000_found = true;
                }

            }   // Q4 with no 5000 miles compliance

            if (empty($data->taxable) && empty($data->total_taxable)) {
                continue;
            } else {
                $record->mileage = $data->mileage;
                $record->reimbursement = round($data->reimbursement,2);
                $record->limit = round($data->limit,2);

                $record->taxable = round($data->taxable,2);
                $record->taxable5000 = round($data->taxable5000,2);
                $record->total_taxable = round($data->total_taxable,2);
                $results[] = $record;

                $total_mileage += $data->mileage;
                $total_reimbursement += $data->reimbursement;
                $total_limit += $data->limit;
                $total_taxable += $data->taxable;
                $total_total_taxable += $record->total_taxable;
                $total_taxable5000 += $record->taxable5000;

                $totals = ['mileage' => $total_mileage,
                    'reimbursement' => round($total_reimbursement,2),
                    'limit' => round($total_limit,2),
                    'taxable' => round($total_taxable,2),
                    'taxable5000' => round($total_taxable5000,2),
                    'total_taxable' => round($total_total_taxable,2)];
            }
        }   // loop by user

        return array("results" => $results, 'totals' => $totals, 'compliance_5000_found' => $compliance_5000_found, 'message' => null);
    }

    private function getTaxAdjustmentReportWith5000($company_id, $division_id, $year, $quarter)
    {

    }

    private function getTaxAdjustmentsFromTable($tax_id, $columns)
    {

        $column_list = implode(',', $columns);
        $tax_details = [];
        $tax_summary = [];

        $row = [];
        $last_quarter = ''; // break data by quarters
        $last_type = '';
        $rec = \DB::select(\DB::raw("SELECT dtr.name AS row_name, dtc.name AS column_name, dtd.amount, dtr.type
 FROM driver_taxes dt, driver_tax_details dtd, driver_tax_columns dtc, driver_tax_rows dtr
 WHERE dt.id = dtd.driver_tax_id AND dtd.driver_tax_column_id = dtc.id AND dtd.driver_tax_row_id = dtr.id
 AND dt.id = $tax_id AND dtc.id IN ( $column_list )
 ORDER BY dtr.`order` "));
        foreach ($rec as $data) {
            if ($last_quarter != $data->row_name) {
                if ($last_quarter == '') {              // initialise variables
                    $last_type = $data->type;
                    $last_quarter = $data->row_name;
                } else {                                // pass data from previous row
                    if ($last_type == 'D') {
                        $tax_details[$last_quarter] = $row;
                    } else {
                        $tax_summary[$last_quarter] = $row;
                    }
                }
                $last_quarter = $data->row_name;        // prepara data for next row
                $last_type = $data->type;
                $row = [];
            }
            $row[$data->column_name] = $data->amount;
        }
        if ($last_quarter != '') {                      // pass data from last row
            if ($last_type == 'D') {
                $tax_details[$last_quarter] = $row;
            } else {
                $tax_summary[$last_quarter] = $row;
            }
        }

        return ['tax_details' => $tax_details, 'tax_summary' => $tax_summary];
    }

    private function storeTaxDetails($user_id, $year, $tax_details)
    {

        $driver_tax = new DriverTax;
        $driver_tax->timestamps = false;
        $driver_tax->user_id = $user_id;
        $driver_tax->year = $year;
        $driver_tax->save();
        $id = $driver_tax->id;

        foreach ($tax_details as $row => $columns) {
            foreach ($columns as $column => $amount) {
                $rec = new DriverTaxDetail;
                $rec->timestamps = false;
                $rec->driver_tax_id = $id;
                $rec->driver_tax_row_id = $row;
                $rec->driver_tax_column_id = $column;
                $rec->amount = $amount;
                $rec->save();
            }
        }

    }

    public function getDriverAllQuartersTaxCanada(Request $request)
    {

        $user_id = $request->user_id;
        $company_id = $request->company_id;
        $year = $request->year;
        $mileage_entry_type = $request->mileage_entry_type;

        $tax_details = [];
        $tax_summary = [];

        $quarter_months = $this->getQuarterMonthsYearsRelations($company_id, $year);
        $quarter_month_info = $this->getInformationOnQuarterMonths($quarter_months);
        $cra_rates = $this->getCRARates($quarter_months);
        \Debugbar::info('CRA:', $cra_rates);

        // calculating taxable adjustments
        $total_reimbursement = 0;
        $total_mileage = 0;
        $total_not_taxable = 0;
        $total_taxable = 0;

        $accumulated_mileage = 0;
        foreach ($quarter_months as $quarter => $quarter_months_year) {

            $data = $this->driverOneQuarterTaxCanada($user_id, $company_id, $year, $mileage_entry_type, $quarter_months_year, $cra_rates, $accumulated_mileage);

            $tax_details[$quarter] = ['mileage' => $data->mileage, 'reimbursement' => $data->reimbursement,
                'non_taxable' => $data->limit, 'taxable' => $data->taxable];
            $accumulated_mileage = $data->accumulated_mileage;

            $total_reimbursement += $data->reimbursement;
            $total_mileage += $data->mileage;
            $total_not_taxable += $data->limit;
            $total_taxable += $data->taxable;

        }


        return array(
            // 'quarter_array' => $quarter_array,
            'quarter_month_info' => $quarter_month_info,
            'quarter_months_years' => $quarter_months,
            'tax_details' => $tax_details,
            'tax_summary' => $tax_summary
        );

    }

    private function driverOneQuarterTaxCanada($user_id, $company_id, $year, $mileage_entry_type, $quarter_months_year, $cra_rates, $accumulated_mileage)
    {

        // reset quarter totals
        $quarter_fixed = 0;
        $quarter_variable = 0;
        $quarter_mileage = 0;

        foreach ($quarter_months_year as $mm => $yyyy) {

            $reimb = \DB::table('monthly_mileages as mm')
                ->leftJoin('monthly_reimbursements as mr', function ($join) {
                    $join->on('mm.user_id', '=', 'mr.user_id');
                    $join->on('mm.month', '=', 'mr.month');
                    $join->on('mm.year', '=', 'mr.year');
                })
                ->join('driver_profiles as dp', 'mr.driver_profile_id', '=', 'dp.id')
                ->where('mm.user_id', '=', $user_id)
                ->where('mm.year', '=', $yyyy)
                ->where('mm.month', '=', $mm)
                ->select('mr.fixed_reimbursement', 'mr.adjustment_amount', 'mr.variable_reimbursement',
                    'mm.business_mileage')->first();

            // TODO do we need commuter here?!
            if ($mileage_entry_type == 'Daily Commuter') {
                $yyyymm = sprintf('%04d-%02d', $yyyy, $mm);
                $rec = \DB::table('daily_mileages')->where('user_id', $user_id)->where('trip_date', 'like', $yyyymm)->sum('commuter_mileage')->first();
            }

            if($reimb) {
                $quarter_mileage += $reimb->business_mileage;
                $quarter_fixed += $reimb->fixed_reimbursement - $reimb->adjustment_amount;
                $quarter_variable += round($this->getVariableReimbursement($company_id, $year, $mm, $reimb->variable_reimbursement, $reimb->business_mileage), 2);
            }
        }

        $quarter_reimbursement = $quarter_fixed + $quarter_variable;
        $rates = $cra_rates[$yyyy];

        $jk = $accumulated_mileage + $quarter_mileage;

        if (($accumulated_mileage + $quarter_mileage) <= 5000) {

            $quarter_not_taxable = $quarter_mileage * $rates['below_5000'];
            //\Debugbar::info("Below 5000 $accumulated_mileage + $quarter_mileage => $quarter_not_taxable = $quarter_mileage * " . $rates['below_5000']);

        } elseif ($accumulated_mileage <= 5000) {

            //\Debugbar::info("Accumulated below 5000, qm=$quarter_mileage");
            $mileage1 = min(5000 - $accumulated_mileage, $quarter_mileage);
            $mileage2 = max(0, $quarter_mileage - $mileage1);
            $quarter_not_taxable = $mileage1 * $rates['below_5000'] + $mileage2 * $rates['above_5000'];
            //\Debugbar::info("Accumulated $accumulated_mileage below 5000 => $quarter_not_taxable = $mileage1 * " . $rates['below_5000'] . " + $mileage2 * " . $rates['above_5000']);

        } else {

            $quarter_not_taxable = $quarter_mileage * $rates['above_5000'];
            //\Debugbar::info("Abowe 5000 $quarter_not_taxable = $quarter_mileage * " . $rates['above_5000']);

        }

        $quarter_taxable = max(0, $quarter_reimbursement - $quarter_not_taxable);
        $accumulated_mileage += $quarter_mileage;

        return (object)['mileage' => $quarter_mileage, 'reimbursement' => $quarter_reimbursement,
            'fixed_reimbursement' => $quarter_fixed, 'variable_reimbursement' => $quarter_variable,
            'limit' => $quarter_not_taxable, 'taxable' => $quarter_taxable,
            'accumulated_mileage' => $accumulated_mileage];

    }


    private function getCRARates($quarter_months)
    {
        \Debugbar::info("Get CRA based on ", $quarter_months);

        $first_entry = true;
        $years = [];
        $cra_rates = [];
        $last_year_entry = null;
        foreach ($quarter_months as $q_months) {
            foreach ($q_months as $yyyy) {
                if ($first_entry) {
                    $years[] = $yyyy;
                    $last_year_entry = $yyyy;
                    $first_entry = false;
                } else if ($yyyy != $last_year_entry) {
                    $years[] = $yyyy;
                }
            }
        }

        \Debugbar::info("Get CRA rates for $yyyy");
        foreach ($years as $yyyy) {
            $rec = \DB::table('cra_rates')->where('year', '<=', $yyyy)->orderBy('year', 'desc')->first();
            $cra_rates[$yyyy] = ['below_5000' => $rec->below_5000, 'above_5000' => $rec->above_5000];
        }

        return $cra_rates;
    }

    private function getCompanyQuarters($company_id, $year, $Qvalue, $start_mm = 0)
    {

        $ret_arr = [];

        $tax_quarter_id = (int)str_replace('Q', '', $Qvalue);

        // get most recent tax quarter definition
        $new_definition = \DB::table("tax_quarters as q")
            ->join('company_tax_quarters as cq', 'q.id', '=', 'cq.tax_quarter_id')
            ->whereRaw("YEAR(cq.created_at) <= $year")
            ->where('cq.company_id', $company_id)
            ->select('q.*', 'cq.created_at', 'cq.company_id')
            ->orderBy('cq.created_at', 'desc')->first();

        $new_months = $this->assignMonthsToQuarters($new_definition);

        if ($new_definition && $new_definition->id > 0) {       /// step 1

            // get previous tax quarter definition
            $last_definition = \DB::table("tax_quarters as q")
                ->join('company_tax_quarters as cq', 'q.id', '=', 'cq.tax_quarter_id')
                ->whereRaw("YEAR(cq.created_at) <= $year")
                ->where('cq.company_id', $company_id)
                ->select('q.*', 'cq.created_at', 'cq.company_id')
                ->orderBy('cq.created_at', 'desc')->first();

            $old_months = $this->assignMonthsToQuarters($last_definition);

             /// step 2

            if ($last_definition->id > 0 && $last_definition->id != $new_definition->id) {
                // in which quarter we started new definition?
                $started_month = (int)substr($new_definition->created_at,5,2);
                if( $start_mm > 0 ) {
                    $started_month = $start_mm;     /// step 3
                }

                $start_quarter = $new_months[$started_month];
                $old_start_quarter = $old_months[$started_month];

                $forward = $this->getQuarterDirection($old_months, $new_months);        /// step 4

                // copy months from old if old quarter was completed
                $found_quarter = false;
                for ($qq = 1; $qq < $old_start_quarter; $qq++) {
                    foreach ($old_months as $key => $value) {
                        if ($value == $qq && $value == $tax_quarter_id) {
                            $ret_array[$key] = $this->getQuarterYear($key, $value, $year);
                            $found_quarter = true;
                        }
                    }
                }
                if ($found_quarter && $forward == false) {                              /// step 5
                    $ret[0] = $this->sortedByYear($ret_array, $year);
                    return $ret;
                }

                if( $forward == false) {

                    ////////////////////////////////////////
                    ////// going back
                    ////////////////////////////////////////

                    if ($start_quarter != $tax_quarter_id) {
                        foreach ($new_months as $key => $value) {
                            if ($value == $tax_quarter_id) {
                                $ret_array[$key] = $this->getQuarterYear($key, $value, $year);
                            }
                        }
                    }
                    else {
                        foreach ($new_months as $key => $value) {
                            if ($value == $tax_quarter_id && $old_months[$key] == $tax_quarter_id) {
                                $ret_array[$key] = $this->getQuarterYear($key, $value, $year);
                            }
                        }
                    }
                }
                else {                  /// step 6

                    ////////////////////////////////////////
                    ////// going forward
                    ////////////////////////////////////////
                    if ($tax_quarter_id >= $old_start_quarter) {
//                    echo "copy new quarter<br>";
                        foreach ($new_months as $key => $value) {
                            if ($value == $tax_quarter_id) {
                                $ret_array[$key] = $this->getQuarterYear($key, $value, $year);
                            }
                        }
                    }
                    else if ($tax_quarter_id == ($old_start_quarter - 1)) {

                        foreach ($new_months as $key => $value) {
                            if ($value == $tax_quarter_id && $old_months[$key] != $tax_quarter_id) {
                                $ret2_array[$key] = $this->getQuarterYear($key, $value, $year);
                            }
                        }
                        $ret2_array = $this->sortedByYear($ret2_array, $year);
                        $ret[0] = $ret_array;
                        $ret[1] = $ret2_array;
                    }
                }
            } else {    // no modification are required

                foreach ($new_months as $mm => $mm_quarter) {
                    if ($mm_quarter == $tax_quarter_id) {
                        $ret_arr[$mm] = $this->getQuarterYear($mm, $tax_quarter_id, $year);
                    }
                }

            }
        }
        return ['ret_arr' => $ret_arr];       // returns month => year for provided quarter
    }

    private function  sortedByYear($ret_array, $year) {
    $return = array();
    foreach ($ret_array as $key => $value)
        if ($value == $year - 1)
            $return[$key] = $value;
    foreach ($ret_array as $key => $value)
        if ($value == $year)
            $return[$key] = $value;
    foreach ($ret_array as $key => $value)
        if ($value == $year + 1)
            $return[$key] = $value;
    return $return;
}


private function getQuarterDirection($last, $new)
    {
        for ($mm = 1; $mm <= 12; $mm++) {
            if (($last[$mm] == 2 || $new[$mm] == 2) && $last[$mm] != $new[$mm]) {
                $forward = $new[$mm] == 2 ? false : true;
                break;
            }
            if (($last[$mm] == 3 || $new[$mm] == 3) && $last[$mm] != $new[$mm]) {
                $forward = $new[$mm] == 3 ? false : true;
                break;
            }
            if (($last[$mm] == 4 || $new[$mm] == 4) && $last[$mm] != $new[$mm]) {
                $forward = $new[$mm] == 4 ? false : true;
                break;
            }
        }
        return $forward;
    }

    private function assignMonthsToQuarters($row)
    {
        if ($row == null) {
            return null;
        }

        $new = array();

        for ($mm = $row->q1; $mm < $row->q2; $mm++) {
            $new[$mm] = 1;
        }

        if ($row->q1 > $row->q2) {
            for ($mm = $row->q1; $mm <= 12; $mm++) {
                $new[$mm] = 1;
            }
            for ($mm = 1; $mm < $row->q2; $mm++) {
                $new[$mm] = 1;
            }
        }
        for ($mm = $row->q2; $mm < $row->q3; $mm++) {
            $new[$mm] = 2;
        }
        for ($mm = $row->q3; $mm < $row->q4; $mm++) {
            $new[$mm] = 3;
        }
        if ($row->q1 > 6) {
            for ($mm = $row->q4; $mm < $row->q1; $mm++) {
                $new[$mm] = 4;
            }
        } else {
            for ($mm = $row->q4; $mm <= 12; $mm++) {
                $new[$mm] = 4;
            }
            for ($mm = 1; $mm < $row->q1; $mm++) {
                $new[$mm] = 4;
            }
        }
        return $new;
    }

    /**
     * @param int $mm
     * @param int $quarter_id
     * @param int $year
     * @return int quaeter year
     */
    private function getQuarterYear($mm, $quarter_id, $year)
    {
        if ($quarter_id == 1 && $mm > 6) {               // Q1 is just before end of year, overlapping to next year
            return $year - 1;
        } elseif ($quarter_id == 4 && $mm < 6) {         // Q4 is in the beginning of year, overlapping from previous year
            return $year + 1;
        } else {
            return $year;                                 // no year overlap
        }
    }

    private function vehicle_too_old_for_new_year($user_id, $year)
    {

        $driver_profile = DriverProfile::where('user_id', '=', $user_id)->first();
        $driver_profile_year = $driver_profile->vehicleProfile->vehicle->year;

        $FAVR_data = FavrParameter::find($driver_profile_year)->first();
        $min_vehicle_year = $FAVR_data->minimum_year;

        $driver_vehicle = $this->get_driver_vehicle_array_before_year($user_id, $year);
        if ((int)$driver_vehicle->year == (int)$min_vehicle_year)
            return true;
        elseif ((int)$driver_vehicle->year < (int)$min_vehicle_year) {
            // check if it was the old car introduced before December
            $this_year = $year - 1;
            $car_entry_date = substr($driver_vehicle->created_at, 0, 10);
            $compare_with = "$this_year-12-01";
            if (strcmp($car_entry_date, $compare_with) == -1) {
                return false;
            } else
                return true;
        } else {
            return false;
        }
    }

    private function get_driver_vehicle_array_before_year($driver_id, $year)
    {
        $date = $year . "-01-01";
        return $driver_vehicle = PersonalVehicle::withTrashed()
            ->where('user_id', '=', $driver_id)->where('created_at', '<', $date)->orderBy('created_at')->first();
    }

    private function missing5000MilesCompliance($user_id, $year)
    {
        return MonthlyReimbursement::where('user_id', '=', $user_id)->where('year', '=', $year)
            ->where('fixed_status', '&', GREEN_NO_5000_MILES_COMPLIANCE)->select('user_id')->first();
    }

    private function driverAnnualTaxWM($company_id, $user_id, $year, $quarter_months, $standard_rate_array)
    {


        $current_status = null;
        $previous_status = null;

        $tax_details = [];
        $tax_summary = [];

        //$quarter_months = $this->getQuarterMonthsYearsRelations($company_id, $year);

        // calculating taxable adjustments
        $total_fixed = 0;
        $total_variable = 0;
        $total_reimbursement = 0;
        $total_mileage = 0;
        $total_not_taxable = 0;
        $total_taxable = 0;

        foreach ($quarter_months as $quarter => $months_year) {

            // reset quarter totals
            $quarter_fixed = 0;
            $quarter_variable = 0;
            $quarter_mileage = 0;
            $quarter_not_taxable = 0;

            foreach ($months_year as $mm => $yyyy) {

                // \Debugbar::info("mm=$mm yy=$yyyy");
                $last_yyyy = $mm == 1 ? $yyyy - 1 : $yyyy;
                $last_mm = $mm == 1 ? 12 : $mm - 1;

                $query = "SELECT mm.business_mileage,
mr_last_month.fixed_reimbursement,
mr_last_month.adjustment_amount,
mr.variable_reimbursement, mm.user_id,
mr.fixed_status AS  current_status,
mr_last_month.fixed_status AS previous_status,
u.last_name, u.first_name,
mr_last_month.within_grace_period
FROM users u
LEFT JOIN monthly_reimbursements mr ON mr.user_id = u.id AND mr.year = $yyyy AND mr.month = $mm
LEFT JOIN monthly_reimbursements mr_last_month ON mr_last_month.year = $last_yyyy AND mr_last_month.month = $last_mm AND mr_last_month.user_id = u.id
LEFT JOIN monthly_mileages mm ON mm.user_id = u.id AND mm.user_id = u.id and mm.year = $yyyy AND mm.month = $mm
WHERE u.id = $user_id ";
                $reimb = \DB::select(\DB::raw($query))[0];


                if ($reimb != null) {

                    $fixed_status = $reimb->current_status;
                    $previous_status = $reimb->previous_status;
                    $in_grace = $reimb->within_grace_period;

                    // we goofed as we sent Q1 after we discovered it and than we did not activated it for 2015
                    if ($quarter == 'Q1' && $mm == 1 && $fixed_status == 2 && $year < 2016) {
                        $previous_status = 2;
                    }

                    if ($in_grace) {
                        // fixed for previous month
                        $previous_status = $this->removeFromStreetLight($previous_status, GREEN_NO_VEHICLE_COST_COMPLIANCE);
                        $previous_status = $this->removeFromStreetLight($previous_status, GREEN_NO_VEHICLE_AGE_COMPLIANCE);
                        $previous_status = $this->removeFromStreetLight($previous_status, GREEN_NO_INSURANCE_COMPLIANCE);
                        $previous_status = $this->removeFromStreetLight($previous_status, RED_AWAITING_INSURANCE);
                        $previous_status = $this->removeFromStreetLight($previous_status, RED_INSUFFICIENT_INSURANCE);
                        $previous_status = $this->removeFromStreetLight($previous_status, RED_INSURANCE_EXPIRED);
                        // variable for current month
                        $fixed_status = $this->removeFromStreetLight($fixed_status, GREEN_NO_VEHICLE_COST_COMPLIANCE);
                        $fixed_status = $this->removeFromStreetLight($fixed_status, GREEN_NO_VEHICLE_AGE_COMPLIANCE);
                        $fixed_status = $this->removeFromStreetLight($fixed_status, GREEN_NO_INSURANCE_COMPLIANCE);
                        $fixed_status = $this->removeFromStreetLight($fixed_status, RED_AWAITING_INSURANCE);
                        $fixed_status = $this->removeFromStreetLight($fixed_status, RED_INSUFFICIENT_INSURANCE);
                        $fixed_status = $this->removeFromStreetLight($fixed_status, RED_INSURANCE_EXPIRED);
                    }

                    if ($mm == 1 && ($fixed_status & GREEN_NO_5000_MILES_COMPLIANCE)) {
                        $previous_status = GREEN_NO_5000_MILES_COMPLIANCE; // strange, but this is what we have on production. I added && (...)
                    }

                    // only months where we have no compliance should be included
                    if ($previous_status > GREEN_REIMBURSEMENT && !$in_grace) {
                        $mileage = $reimb->business_mileage;   // ?????  we have lock date here
                        $cents_per_mile = $reimb->variable_reimbursement;
                    } else {
                        $mileage = 0;
                        $cents_per_mile = 0;
                    }

                    if (!$in_grace || $year < 2014) {
                        $quarter_mileage += $mileage;
                        $non_taxable = round($mileage * $standard_rate_array[(int)$mm] / 100, 2);
                        $fixed_reimbursement = round($reimb->fixed_reimbursement - $reimb->adjustment_amount, 2);
                        $quarter_fixed += $fixed_reimbursement;
                        $variable = round($mileage * $reimb->variable_reimbursement / 100, 2);
                        $quarter_variable += $variable;
                    } else {
                        $fixed_reimbursement = 0;
                        $variable = 0;
                    }

                    // if( $mm == 1 ) { \Debugbar::info("c/m=$cents_per_mile mileage=$mileage vr=$variable");}

                    if (!($fixed_status & GREEN_NO_DEPRECIATION_COMPLIANCE)) {             // all taxable
                        $taxable_limit = $mileage * $standard_rate_array[(int)$mm] / 100;   // was not rounded on production :-( Do not change it as it will create 1 cent dicrepancy
                        $quarter_not_taxable += $taxable_limit;
                    } else {
                        $taxable_limit = 0;
                    }

                    $taxable = max(0, $fixed_reimbursement + $variable - $taxable_limit);
                    $reimbursement = $fixed_reimbursement + $variable;

                    // \Debugbar::info("MM=$mm $reimbursement = $fixed_reimbursement + $variable");

                }

            } // for each month within quarter


            $quarter_reimbursement = $quarter_fixed + $quarter_variable;
            $quarter_not_taxable = round($quarter_not_taxable, 2);
            $quarter_taxable = max(0, $quarter_reimbursement - $quarter_not_taxable);

            $total_fixed += $quarter_fixed;
            $total_variable += $quarter_variable;
            $total_reimbursement += $quarter_reimbursement;
            $total_mileage += $quarter_mileage;
            $total_not_taxable += $quarter_not_taxable;
            $total_taxable += $quarter_taxable;

        }   // for each $quarter


        return (object)['mileage' => $total_mileage, 'reimbursement' => $total_reimbursement, 'limit' => $total_not_taxable,
            'taxable' => $total_taxable];

    }

    private function getAnnual463TaxAdjustment($user_id, $year, $quarter_months_years, $irs_rates)
    {
        \Debugbar::info('in getAnnual463TaxAdjustment $quarter_months_years=', $quarter_months_years);

        for ($mm = 1; $mm <= 12; $mm++) {
            $mileage[$mm] = 0;
            $fixed[$mm] = 0;
            $variable[$mm] = 0;
            $reimbursement[$mm] = 0;
            $cent_per_mile[$mm] = 0;
            $accept_variable[$mm] = 0;
        }

        $m_rec = MonthlyMileage::where('user_id', '=', $user_id)
            ->where('year', '=', $year)->select('month', 'business_mileage')->get();
        foreach ($m_rec as $rec) {
            $mileage[$rec->month] = $rec->business_mileage;
        }

        $r_rec = MonthlyReimbursement::where('user_id', '=', $user_id)
            ->where('year', '=', $year)->get();
        foreach ($r_rec as $rec) {
            $fixed[$rec->month] = $rec->fixed_reimbursement - $rec->adjustment_amount;
            $variable[$rec->month] = round($mileage[$rec->month] * $rec->variable_reimbursement / 100, 2);
            if ($rec->variable_reimbursement > 0 && $year >= 2016) { // CCS-186. Before we accumulated mileage where reimbursement was not p
                $accept_variable[$rec->month] = 1;
            } else {
                $accept_variable[$rec->month] = 1;
            }
        }

        $annual_mileage = 0;
        $annual_variable = 0;
        $annual_fixed = 0;
        $annual_reimb = 0;
        $annual_limit = 0;
        $annual_taxable = 0;

        // in 2014 we corrected calculation as even annual tax should be based on quarterly totals not on monthly adjusttments
        if ($year < 2014) {
            for ($mm = 1; $mm <= 12; $mm++) {
                $annual_mileage += $accept_variable[$mm] * $mileage[$mm];
                $annual_variable += $variable[$mm];
                $annual_fixed += $fixed[$mm];
                $annual_reimb += $fixed[$mm] + $variable[$mm];
                $annual_limit += $accept_variable[$mm] * $mileage[$mm] * $irs_rates[$mm] / 100;
            }
            $annual_taxable = round($annual_reimb, 2) - round($annual_limit, 2);
        } else {
            foreach ($quarter_months_years as $quarter => $months_year) {
                $quarter_mileage = 0;
                $quarter_variable = 0;
                $quarter_fixed = 0;
                $quarter_reimb = 0;
                $quarter_limit = 0;
                foreach ($months_year as $mm => $yyyy) {
                    $quarter_mileage += $accept_variable[$mm] * $mileage[$mm];
                    $quarter_variable += $variable[$mm];
                    $quarter_reimb += $fixed[$mm] + $variable[$mm];
                    $quarter_fixed += $fixed[$mm];
                    $quarter_limit += $accept_variable[$mm] * $mileage[$mm] * $irs_rates[$mm] / 100;
                }

                $annual_mileage += $quarter_mileage;
                $annual_fixed += $quarter_fixed;
                $annual_variable += $quarter_variable;
                $annual_reimb += $quarter_reimb;
                $annual_limit += $quarter_limit;
                $annual_taxable += $quarter_reimb - round($quarter_limit, 2);

            }
        }
        return (object)['mileage' => $annual_mileage, 'fixed_reimbursement' => $annual_fixed,
            'variable_reimbursement' => $annual_variable, 'reimbursement' => $annual_reimb, 'limit' => $annual_limit,
            'taxable' => $annual_taxable];
    }

    private function getIRRStandardRates($quarter_months_years)
    {

        $irs_rate = [];

        \Debugbar::info('$quarter_months_years=',$quarter_months_years);

        foreach ($quarter_months_years as $month_year) {
            foreach ($month_year as $month => $year) {
                $query = "SELECT rate FROM irs_standard_rates " .
                    " WHERE ( year < $year OR year = $year AND month <= $month ) " .
                    " ORDER BY year DESC, month DESC LIMIT 1";
                $irs_rate[$month] = \DB::select(\DB::raw($query))[0]->rate;
            }
        }
        // \Debugbar::info('$irs_rate',$irs_rate);
        return $irs_rate;
    }


    /**
     * In case we do not have 12 months of reimbursement, return true
     * if driven mileage is < 5000 * months / 12
     * if driver started or quit during months of all taxable quarters
     *
     * @return int business mileage if below limit or null
     */
    private function
    belowPartial5000Miles($user_id, $quarter_dates, $only_stop_date = false, $annual = false)
    {
        $from_date = $quarter_dates->quarter_start_date;
        $to_date = $quarter_dates->quarter_end_date;
        $last_quarter_to_date = $quarter_dates->last_quarter_end_date;

        $year_clause = $from_date->year == $to_date->year ?
            " AND mr.year = $from_date->year AND mr.month >= $from_date->month AND mr.month <= $to_date->month "
            :
            " AND ( mr.year = $from_date->year AND mr.month >= $from_date->month OR " .
            "  mr.year = $to_date->year AND mr.month <= $to_date->month ) ";


        // driver start and stop dates based on reimbursements records from selected quarter
        $query = "SELECT dp.start_date, dp.stop_date FROM monthly_reimbursements mr, driver_profiles dp " .
            " WHERE mr.user_id = $user_id $year_clause AND mr.driver_profile_id = dp.id " .
            " ORDER BY mr.year DESC, mr.month DESC LIMIT 1";

        $rows = \DB::select(\DB::raw("$query"));

        if( $rows == null) {
            return null;
        }

        $row = $rows[0];

        // \Debugbar::info($query, $row);

        if (empty($row->stop_date) && !$annual) { // Not interested, still on the program if not for annual
            return null;
        }


        if ($row->stop_date) {  // try to find stop date within selected quarter
            list($yyyy, $mm, $dd) = explode('-', $row->stop_date);
            $stop_date = Carbon::create($yyyy, $mm, $dd);
            if ($stop_date->between($from_date, $to_date)) {

            } else {
                if ($only_stop_date) { // not this quarter we are interested.
                    return null;
                }
                $stop_date = $to_date;
            }
        }

        // we have to calculate mileage from max( first quarter, start date )
        $first_quarter_date = $quarter_dates->first_quarter_start_date;


        if ($row->start_date) { // try to find start date after first quarter month
            list($yyyy, $mm, $dd) = explode('-', $row->start_date);
            $start_date = Carbon::create($yyyy, $mm, 1); // we are based on entire months
            $start_date = $start_date->max($first_quarter_date); // maximum of these two
        }

        if( empty($stop_date)) {
            $month_difference = $last_quarter_to_date->diffInMonths($start_date) + 1;
        } else {
            $month_difference = $stop_date->diffInMonths($start_date) + 1;
        }

        if ($month_difference < 12) {
            $query = "SELECT SUM(business_mileage) AS business_mileage FROM monthly_mileages " .
                " WHERE user_id = $user_id AND " .
                " ( $start_date->year = $stop_date->year AND year = $start_date->year AND month >= $start_date->month AND month <= $stop_date->month OR " .
                "   $start_date->year != $stop_date->year AND " .
                "( year = $start_date->year AND month >= $start_date->month  OR year = $stop_date->year AND month <= $stop_date->month ))";

            $row = \DB::select(\DB::raw("$query"))[0];
            return $row->business_mileage * 12 / $month_difference < 5000 ? $row->business_mileage : null;
        }
        return null;
    }

    private function annualDates($quarter_months_years) {
        $from_year = null;
        $from_month = null;
        $to_year = null;
        $to_month = null;
        $first_record = true;
        foreach ($quarter_months_years as $months_years) {
            foreach( $months_years as $mm => $yyyy) {
                if( $first_record) {
                    $from_year = $yyyy;
                    $from_mm = $mm;
                    $first_record = false;
                }
                $to_year = $yyyy;
                $to_month = $mm;
            }
        }

        $from_date = Carbon::now();
        $from_date->year = $from_year;
        $from_date->month = $from_month;
        $from_date->day = 1;

        $to_date = Carbon::now();
        $to_date->year = $to_year;
        $to_date->month = $to_month;
        $to_date = $to_date->lastOfMonth();

        return (object)['first_quarter_start_date' => $from_date,
            'quarter_start_date' => $from_date,
            'quarter_end_date' => $to_date,
            'last_quarter_end_date' => $to_date];

    }

    private function quarterDates($quarter, $quarter_months)
    {
        $from_year = null;
        $from_month = null;
        $to_year = null;
        $to_month = null;
        $last_year = null;
        $last_month = null;
        foreach ($quarter_months as $c_quarter => $months_years) {

            if ($quarter != -1 && $quarter != $c_quarter) {
                continue;
            }
            foreach ($months_years as $mm => $yyyy) {
                if (!$from_year) {
                    $from_year = $yyyy;
                    $from_month = $mm;
                }
            }
            $to_year = $yyyy;
            $to_month = $mm;
        }

        $last_quarter = end($quarter_months);
        foreach ($last_quarter as $mm => $yyyy) {
            $last_year = $yyyy;
            $last_month = $mm;
        }

        $from_date = Carbon::now();
        $from_date->year = $from_year;
        $from_date->month = $from_month;
        $from_date->day = 1;

        $to_date = Carbon::now();
        $to_date->year = $to_year;
        $to_date->month = $to_month;
        $to_date = $to_date->lastOfMonth();

        $last_date = Carbon::now();
        $last_date->year = $last_year;
        $last_date->month = $last_month;
        $last_date = $last_date->lastOfMonth();

        $first_quarter_months_years = reset($quarter_months);
        $first_quarter_month = key($first_quarter_months_years);
        $first_quarter_year = $first_quarter_months_years[$first_quarter_month];
        $first_quarter_date = Carbon::create($first_quarter_year, $first_quarter_month, 1);

        return (object)['first_quarter_start_date' => $first_quarter_date,
            'quarter_start_date' => $from_date,
            'quarter_end_date' => $to_date,
            'last_quarter_end_date' => $last_date];
    }

    private function driverAnnualTax($user_id, $quarter_dates, $business_mileage, $standard_rate_array)
    {
        // calculate fixed, variable, reimbursement, limit and taxable
        $r_start = $quarter_dates->first_quarter_start_date;
        $r_stop = $quarter_dates->quarter_end_date;
        $year_clause = $r_start->year == $r_stop->year ?
            " AND mm.year = $r_start->year AND mm.month >= $r_start->month AND mm.month <= $r_stop->month"
            :
            " ( AND mm.year = $r_start->year AND mm.month >= $r_start->month OR 
                    mm.year = $r_stop->year AND mm.month <= $r_stop->month";

        $query = "SELECT mm.month, mm.business_mileage, mr.fixed_reimbursement, mr.adjustment_amount, mr.variable_reimbursement 
  FROM monthly_mileages mm
  LEFT JOIN monthly_reimbursements mr ON mm.user_id = mr.user_id AND mm.year=mr.year AND mm.month = mr.month
  WHERE mm.user_id = $user_id $year_clause";

        $fixed_reimbursement = 0;
        $variable_reimbursement = 0;
        $reimbursement = 0;
        $mileage = 0;
        $limit = 0;
        $taxable = 0;

        $rows = \DB::select(\DB::raw("$query"));

        foreach ($rows as $row) {
            $mileage += $row->business_mileage;
            $fixed_reimbursement += $row->fixed_reimbursement;
            $var = round($row->business_mileage * $row->variable_reimbursement / 100, 2);
            $variable_reimbursement += $var;
            $reimbursement += $row->fixed_reimbursement + $var;
            $limit += round($row->business_mileage * $standard_rate_array[$row->month] / 100, 2);
        }
        $taxable = max(0, $reimbursement - $limit);
        // \Debugbar::info("M=$mileage R=$reimbursement L=$limit T=$taxable" );

        return (object)['mileage' => $mileage, 'reimbursement' => $reimbursement,
            'fixed_reimbursement' => $fixed_reimbursement, 'variable_reimbursement' => $variable_reimbursement,
            'limit' => $limit, 'taxable' => $taxable];
    }

    private function getQuartersAllTaxableWMDriver($user_id, $quarter_dates, $business_mileage, $standard_rate_array)
    {
        // calculate fixed, variable, reimbursement, limit and taxable
        $r_start = $quarter_dates->first_quarter_start_date;
        $r_stop = $quarter_dates->quarter_end_date;

        $year_clause = $r_start->year == $r_stop->year ?
            " AND mm.year = $r_start->year AND mm.month >= $r_start->month AND mm.month <= $r_stop->month"
            :
            " ( AND mm.year = $r_start->year AND mm.month >= $r_start->month OR 
                    mm.year = $r_stop->year AND mm.month <= $r_stop->month";

        $query = "SELECT mm.month, mm.business_mileage, mr_1.fixed_reimbursement, mr_1.adjustment_amount, mr.variable_reimbursement 
  FROM monthly_mileages mm
  LEFT JOIN monthly_reimbursements mr ON mm.user_id = mr.user_id AND mm.year=mr.year AND mm.month = mr.month
  LEFT JOIN monthly_reimbursements mr_1 ON mm.user_id = mr_1.user_id AND( mm.month > 1 AND mm_1.month = (mm.month - 1) AND mm_1.year = mm.year OR 
  mm.month = 1 AND mm_1.month = 12 AND mm_1.year = (mm.year - 1)
  WHERE mm.user_id = $user_id $year_clause";

        $fixed_reimbursement = 0;
        $variable_reimbursement = 0;
        $reimbursement = 0;
        $mileage = 0;
        $limit = 0;
        $taxable = 0;

        $rows = \DB::select(\DB::raw("$query"));

        foreach ($rows as $row) {
            $mileage += $row->business_mileage;
            $fixed_reimbursement += $row->fixed_reimbursement;
            $var = round($row->business_mileage * $row->variable_reimbursement / 100, 2);
            $variable_reimbursement += $var;
            $reimbursement += $row->fixed_reimbursement + $var;
            $limit += round($row->business_mileage * $standard_rate_array[$row->month] / 100, 2);
        }
        $taxable = max(0, $reimbursement - $limit);
        // \Debugbar::info("M=$mileage R=$reimbursement L=$limit T=$taxable" );

        return (object)['mileage' => $mileage, 'reimbursement' => $reimbursement,
            'fixed_reimbursement' => $fixed_reimbursement, 'variable_reimbursement' => $variable_reimbursement,
            'limit' => $limit, 'taxable' => $taxable];
    }


    /** PULTE PAY FILE TAXABLE LIMIT FUNCTIONS ---------------------------------------------------------------------- */

    /**
     * This function is used in Pulte's pay file. It is for 463.
     *
     * it will return [
     *  'taxable' => amt
     *  'non_taxable' => amt
     *]
     * @param $mileage
     * @param $year
     * @param $month
     * @param $total_reimbursement
     * @return mixed
     */
    public function calculateTaxableAmount463($mileage, $year, $month, $total_reimbursement)
    {
        $non_taxable_limit = $this->calculateW2NonTaxableLimit($mileage, $year, $month);
        return $this->calculateTaxableAndNonTaxableAmounts($total_reimbursement, $non_taxable_limit);
    }

    /**
     * This grabs the irs standard rate over 100, multiplied by mileage to get a non-taxable limit
     */
    public function calculateW2NonTaxableLimit($mileage, $year, $month)
    {
        if ($year < 2012 && $month < 8) {
            $irs_standard_rate = \DB::table('irs_standard_rates')
                ->where('year', $year)
                ->where('month', $month)
                ->first()
                ->rate;
        } else {
            $irs_standard_rate = \DB::table('irs_standard_rates')
                ->where('year', $year)
                ->first()
                ->rate;
        }

        $non_taxable_limit = $mileage * $irs_standard_rate / 100;

        return ($year < 2015 || $year == 2015 && $month < 6) ? $non_taxable_limit :  round($non_taxable_limit, 2);
    }

    /**
     * Given total_reimbursement and a non taxable limit, this will return the taxable
     * and non-taxable amounts
     *
     * @param $total_reimbursement
     * @param $w2_non_taxable_limit
     * @return mixed
     */
    private function calculateTaxableAndNonTaxableAmounts($total_reimbursement, $w2_non_taxable_limit)
    {
        $taxable = max(0, $total_reimbursement - $w2_non_taxable_limit);
        $non_taxable = $total_reimbursement - $taxable;

        return [
            'taxable' => $taxable,
            'non_taxable' => $non_taxable
            ];
    }

    /** END PULTE PAY FILE TAXABLE LIMIT FUNCTIONS ------------------------------------------------------------------ */


}