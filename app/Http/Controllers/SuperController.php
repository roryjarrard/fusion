<?php

namespace App\Http\Controllers;

use App\DriverProfile;
use App\MileageBand;
use HttpOz\Roles\Models\Role;
use Illuminate\Http\Request;
use App\Bank;
use App\Company;
use App\Division;
use App\User;
use App\ManagerProfile;
use App\StateProvince;
use App\SuperProfile;
use App\VehicleProfile;
use App\VehicleProfileApproval;
use App\MasterTask;
use App\Http\Traits\ValidationTraits;

use Illuminate\Support\Facades\Storage; // for Pulte test
use Illuminate\Support\Facades\File;    // for Pulte test
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use App\ZipCode;
use App\PostalCode;

// TODO for Pulte
use App\Calculators\StreetLightCalculator;
use App\Address;
use OwenIt\Auditing\Models\Audit;


class SuperController extends Controller
{
    use ValidationTraits;
    protected $streetLightCalculator;

    public function __construct()
    {
        $this->middleware('auth');
        // TODO for Pulte
        $this->streetLightCalculator = new StreetLightCalculator();
    }

    /**
     * Remove session setting related to the super Vue, except
     * the company id that may be in session
     *
     * @return \Illuminate\Http\JsonResponse
     */
    #TODO this is going away
    public function resetSession()
    {

        session()->put('session_manager_id', -1);
        session()->put('session_administrator_id', -1);
        return response()->json(['message' => 'reset session values']);
    }

    /**
     * Return all users from session company and (optional) division id.
     * Optional parameter of $userType is not used buy may be utilized in the
     * future if only one user type is desired.
     *
     * @param string $userType
     * @return \Illuminate\Http\JsonResponse
     */
    # TODO: is this being used anywhere?
    /*public
    function getUsers($userType = 'all')
    {
        // initialize to empty arrays not null so we can get count=0
        $drivers = $managers = $administrators = [];

        //$company_id = session('company_id');
        $division_id = session('division_id');
        if (empty($division_id)) {
            $division_id = -1;
        }

        // we only retrieve users if a company is selected
        if (!empty($company_id) && $company_id != -1) {
            if ($division_id != -1):
                // drivers for company and division
                $drivers = \DB::table('users')->join('driver_profiles', 'driver_profiles.user_id', 'users.id')
                    ->where('driver_profiles.company_id', $company_id)->where('users.deleted_at', null)->where('driver_profiles.division_id', $division_id)
                    ->select('users.id', 'users.first_name', 'users.last_name')->orderBy('users.last_name')->get();
            else:
                // all company drivers
                $drivers = \DB::table('users')->join('driver_profiles', 'driver_profiles.user_id', 'users.id')
                    ->where('driver_profiles.company_id', $company_id)->where('users.deleted_at', null)->
                    select('users.id', 'users.first_name', 'users.last_name')->orderBy('users.last_name')->get();
            endif;

            $managers = \DB::table('users')->join('manager_profiles', 'manager_profiles.user_id', 'users.id')->where('users.deleted_at', null)->where('manager_profiles.company_id', $company_id)->get();
            $administrators = \DB::table('users')->join('administrator_profiles', 'administrator_profiles.user_id', 'users.id')->where('users.deleted_at', null)->where('administrator_profiles.company_id', $company_id)->get();
        }

        $user_count = count($drivers) + count($managers) + count($administrators);

        return response()->json([
            'drivers' => $drivers,
            'managers' => $managers,
            'administrators' => $administrators,
            'message' => 'got ' . $user_count . ' users'
        ]);
    }*/

    /**
     * Function to approve a vehicle profile
     *
     * Request contains
     *  vehicle_profile_id - int
     *  note - string
     *
     */
    public function approveVehicleProfile(Request $request)
    {
        $user_id = \Auth::user()->id;

        if (SuperProfile::where('user_id', $user_id)->pluck('is_vehicle_profile_approver')->first() == false) {
            abort(403);
        } else if ($request->all() == null) {
            abort(400, 'Invalid Request');
        }

        $now = date('Y-m-d H:i:s');


        \DB::table('vehicle_profile_approvals')
            ->where('vehicle_profile_id', $request->vehicle_profile_id)
            ->whereNull('deleted_at')
            ->update([
                'approved' => 1,
                'note' => $request->note,
                'approver_id' => $user_id,
                'updated_at' => $now,
            ]);
    }

    /**
     * Function to reject a vehicle profile
     *
     * Request contains
     *  vehicle_profile_id - int
     *  note - string
     *
     */
    public function rejectVehicleProfile(Request $request)
    {
        $user_id = \Auth::user()->id;

        if (SuperProfile::where('user_id', $user_id)->pluck('is_vehicle_profile_approver')->first() == false) {
            abort(403);
        } else if ($request->all() == null) {
            abort(400, 'Invalid Request');
        }

        $now = date('Y-m-d H:i:s');


        \DB::table('vehicle_profile_approvals')
            ->where('vehicle_profile_id', $request->vehicle_profile_id)
            ->whereNull('deleted_at')
            ->update([
                'approved' => 0,
                'note' => $request->note,
                'approver_id' => $user_id,
                'updated_at' => $now,
            ]);
    }

    /**
     * Gets all vehicle profiles which are awaiting approval.
     *
     * Returns an object (keyed by company names) of arrays of vehicle profiles
     *
     * ex.
     *      groupedVehicleProfiles['5ME LLC'] = [ array of unapproved vehicle profiles for 5ME LLC ]
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUnapprovedVehicleProfiles()
    {

        // new way based on vehicle profile approvals table
        $vehicleProfileIds = VehicleProfileApproval::whereNull('approved')->pluck('vehicle_profile_id')->all();

        $vehicleProfiles = VehicleProfile::whereIn('id', $vehicleProfileIds)
            ->with('vehicle')
            ->get();

        // group the vehicle profiles by company name and load the category
        $groupedVehicleProfiles = $vehicleProfiles->reduce(function ($groupedVehicleProfiles, $vehicleProfile) {
            $vehicleProfile->vehicle->category;
            $groupedVehicleProfiles[Company::where('id', $vehicleProfile->company_id)->pluck('name')->first()][] = $vehicleProfile;
            return $groupedVehicleProfiles;
        }, []);

        ksort($groupedVehicleProfiles);

        return response()->json(['groupedVehicleProfiles' => $groupedVehicleProfiles]);

    }

    public function getMasterTasks()
    {
        return ['tasks' => MasterTask::orderBy('name')->get()];
    }

    public function getAvailableReportColumns($report_type = "")
    {
        $columns = [];
        switch ($report_type) {
            case 'reimbursement':
                $columns = [
                    'user' => [
                        'username', 'first_name', 'last_name', 'email',
                    ],
                    'driver' => [
                        'street_light_id', 'division_id'
                    ],
                    'mileage' => [
                        'business_mileage', 'personal_mileage'
                    ],
                    'reimbursement' => [
                        'fixed_reimbursement', 'variable_reimbursement', 'adjustment_amount', 'taxable_amount'
                    ]
                ];
                $filters = [
                    'required'=> ['company', 'year', 'month'],
                    'optional' => ['division', 'driver']
                ];
                break;
            case 'user':
                $columns = [
                    'user' => [
                        'username', 'first_name', 'last_name', 'email'
                    ],
                    'driver' => [
                        'street_light_id', 'division_id', 'job_title', 'employee_number', 'start_date', 'stop_date'
                    ],
                    'manager' => [
                        'manager_number'
                    ],
                    'administrator' => [
                        'company_approver', 'administrator_number', 'administrator_phone'
                    ]
                ];
                $filters = [
                    'required' => ['company'],
                    'optional' => ['user_type', 'start_date', 'stop_date', 'division']
                ];
                break;
        }
        return ['columns' => $columns, 'filters' => $filters];
    }

    public function dynamicReportReimbursement (Request $request) {
        \Debugbar::info($request);

        $select = [];
        foreach ($request->get('user', []) as $f) {
            $select[] = "u." . $f;
        }
        foreach ($request->get('driver', []) as $f) {
            $select[] = "dp." . $f;
        }
        foreach ($request->get('mileage', []) as $f) {
            $select[] = "mm." . $f;
        }
        foreach ($request->get('reimbursement', []) as $f) {
            $select[] = "r." . $f;
        }
        $select = implode(', ', $select);

        $where = "where dp.company_id = " . $request->get('company_id') . " and r.month = " . $request->get('selected_month')
            . " and r.year = " . $request->get('selected_year') . " and mm.month = " . $request->get('selected_month')
            . " and mm.year = " . $request->get('selected_year');
        if ($request->has('division_id') && $request->get('divsion_id') > 0) {
            $where .= " and dp.division_id = " . $request->get('division_id');
        }
        if ($request->has('driver_id') && $request->get('driver_id')> 0) {
            $where .= " and dp.user_id = " . $request->get('driver_id');
        }

        $sql = <<<EOQ
select {$select} 
from users as u
  left join driver_profiles as dp on dp.user_id = u.id
  left join monthly_mileages as mm on mm.user_id = u.id
  left join monthly_reimbursements as r on r.user_id = u.id
{$where}
EOQ;
        $data = \DB::select(\DB::raw($sql));
    }


    /**
     * Get All Banks, along with a client count for each bank
     *
     * @return array
     */
    public function getAllBanks()
    {
        $banks = Bank::all();

        foreach ($banks as $bank) {
            $bank->client_count = $bank->company->count();
        }

        return ['banks' =>  $banks ];
    }


    public function addBank(Request $request)
    {
        //validate
        try {
            $this->validateBank($request->bank);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        //add
        Bank::create($request->bank);
    }

    public function updateBank(Request $request)
    {

        //validate
        try {
            $this->validateBank($request->bank);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        Bank::find($request->bank['id'])->update($request->bank);
    }

    public function getScheduledTaskNames(){
        $names = \DB::select(\DB::raw(<<<EOQ
SELECT name FROM master_tasks where deleted_at is null 
UNION
select '' as name
order by 1
EOQ
        ));

        return ['names' => $names];
    }

    public function getScheduledTasks(Request $request){
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $task_name = $request->task_name;
        $task_status = $request->task_status;

        $from_where = !empty($from_date ) ? " AND start_date >= '$from_date'" : '';
        $to_where = !empty($to_date ) ? " AND start_date <= '$to_date 23:59:59'" : '';
        $name_where = !empty($task_name) ? " AND name = '$task_name'" : '';
        $status_where = !empty($task_status) ? " AND status = '$task_status'" : '';

        $records = \DB::select(\DB::raw(<<<EOQ
SELECT * FROM scheduled_tasks 
WHERE 1=1 $from_where $to_where $name_where $status_where
ORDER BY start_date DESC
EOQ
));
        return ['records' => $records];
    }


    /// TESTING Pulte File

    public function processPulteFile(Request $request)
    {

        set_time_limit(0);
        \Debugbar::info('processPulteFile started');

        try {

            $jk = config('app.vhost_root');
            \Debugbar::info('vhost_root', $jk);

            return ['short_description' => 'Hello?', 'long_description' => $jk];

            $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/pulteDriverFile.log', 0, Logger::INFO);
            $logger = new Logger('tasks');
            $logger->pushHandler($handler);
            $logger->addInfo('Hello?', ['jk' => $jk]);


            $return_notes = [];
            $total_added = 0;
            $total_updated = 0;
            $total_terminated = 0;
            $total_be_terminated = 0;
            $total_deactivated = 0; // managers
            $total_reinstated = 0;  // managers

            //$log_id = $this->scheduledTaskStarted($request->task_id, $request->task_name);    // from CommonTraits

            $driver_count = 0;
            $created_at = date("Y-m-d H:i:s");
            $msg = '';
            $header = true;
            $err_array = [];
            $alerts = [];

            $company_id = 57; // Pulte Homes
            $path_to_files = $company_id . '/import_file/';

            $files = Storage::disk('upload_docs')->allFiles($path_to_files);

            \Debugbar::info('Pulte Files: ', $files);

            $columns = $this->pulteRecordColumns();
            $data = [];
            $i = 1;

            $vehicleProfilesNames = $this->getVehicleProfileNames($company_id);
            $mileageBandNames = $this->getMileageBandsNames();
            $divisionNames = $this->getDivisionNames($company_id);
            $stateProvincesNames = $this->getStateProvinces();


            // get all active drivers from this company
            // if driver is missing from this file, we have to delete it

            $missing_drivers = [];
            $drivers = \DB::table('driver_profiles as d')
                ->join('users as u', 'u.id', '=', 'd.user_id')
                ->whereNull('d.deleted_at')
                ->where('d.active', 1)
                ->where('d.company_id', $company_id)
                ->select('u.id', 'u.last_name', 'u.first_name')->get();
            foreach ($drivers as $driver) {
                $missing_drivers[$driver->id] = $driver->last_name . ' ' . $driver->first_name;
            }


            /* this sucks! 4000+ queries were generated. with above code only 23
            $all_drivers = DriverProfile::where('company_id',$company_id)->get();
            $missing_drivers = [];
            foreach( $all_drivers as $driver) {
                $missing_drivers[$driver->id] = $driver->user->last_name . ' ' . $driver->user->first_name;
            }
            */

            foreach ($files as $file) {

                $full_path = base_path() . '/storage/app/public/docs/' . $file;

                if (($handle = fopen($full_path, 'r')) !== false) {
                    while (($row = fgetcsv($handle, 1000, ',')) !== false) {
                        if ($header) {
                            $header = false;
                            continue;
                        }
                        if (count($row) != 23) {
                            $err_array[] = "Invalid number of fields " . count($row) . " in row $i";
                        }
                        $data[] = (object)$this->generatePulteRecord($row, $columns);
                        $i++;
                    }
                }

                $err_row = [];
                foreach ($data as $record_id => $record) {

                    \Debugbar::info('record', $record);
                    $action_taken = null;

                    $err = $this->pulteValidateRecord($record, $company_id, $vehicleProfilesNames, $mileageBandNames, $divisionNames);

                    if (!empty($err)) {
                        $row_num = $record_id + 2;
                        \Debugbar::error("Row $row_num $record->last_name $record->first_name ERROR: " . implode(',', $err));
                        $return_notes[] = "Row $row_num $record->last_name $record->first_name ERROR: " . implode(',', $err);
                        continue;
                    }

                    // check if we have a new division
                    ///////////////////////////////////////////////////////////////
                    if (!in_array($record->market, $divisionNames)) {
                        $new_division = Division::create([
                            'company_id' => $company_id,
                            'name' => $record->market
                        ]);
                        $division_id = $new_division->id;
                        $divisionNames[$division_id] = $record->market;
                        $alerts[] = "New division: $record->market was created for driver $record->last_name $record->first_name";
                    } else {
                        $division_id = Division::where('company_id', $company_id)
                            ->where('name', $record->market)->pluck('id');
                    }


                    // check if we have a new manager
                    ///////////////////////////////////////////////////////////////
                    \Debugbar::info("Check if we have this manager $record->manager_last_name $record->manager_first_name $record->manager_email ");

                    $r = $this->find_manager($company_id, $record);
                    \Debugbar::info('Returned from find_manager: ', $r);
                    $activated = $r->activated;

                    $manager = $r->manager;
                    if ($activated) {
                        $action_taken = 'REINSTATED';
                    }

                    if (!$manager) {
                        \Debugbar::info("Create a new manager");
                        $manager = $this->add_new_manager($company_id, $record);
                        $action_taken = 'REINSTATED';
                    }
                    \Debugbar::info('Manager: ', $manager);


                    // find driver by employee number
                    //////////////////////////////////////////////////////////////////////////
                    $driver = DriverProfile::where('company_id', $company_id)
                        ->where('employee_number', $record->employee_number)->first();

                    if (!$driver) { // add driver

                        \Debugbar::info('Calling Insert Driver $mileageBandNames');

                        $results = $this->add_new_driver($company_id, $divisionNames,
                            $stateProvincesNames, $vehicleProfilesNames, $mileageBandNames, $record);

                        if ($results->errors) {     // problems found
                            $alerts[] = array_merge($alerts, $results->errors);
                            continue;
                        }

                        $driver = $results->driver;
                        \Debugbar::info("Driver Profile after adding:", $driver);
                        $action_taken = 'ADDED';


                    } else { // update driver

                        \Debugbar::info("Calling Update Driver $record->last_name $record->first_name ");

                        $action_taken = $this->updatePulteDriver($driver->user_id, $manager->manager_id, $divisionNames, $stateProvincesNames,
                            $vehicleProfilesNames, $mileageBandNames, $record);

                        unset($missing_drivers[$driver->user_id]);  // not missing
                    }

                    $row_num = $record_id + 2;
                    \Debugbar::info("Row $row_num $record->last_name $record->first_name $action_taken");

                    if ($action_taken) {
                        if ($action_taken == 'REINSTATED' || $action_taken == 'DEACTIVATED') {
                            $return_notes[] = "Row $row_num $record->manager_last_name $record->manager_first_name $action_taken";
                        } else {
                            $return_notes[] = "Row $row_num $record->last_name $record->first_name $action_taken";
                        }

                        if ($action_taken == 'ADDED') $total_added++;
                        elseif ($action_taken == 'TERMINATED') $total_terminated++;
                        elseif ($action_taken == 'TO_BE_TERMINATED') $total_be_terminated++;
                        elseif ($action_taken == 'REINSTATED') $total_reinstated++;
                    }


                    \Debugbar::info("assignDriverToManager($driver->user_id, $manager->manager_id)");
                    $this->assignDriverToManager($driver->user_id, $manager->manager_id);
                    if ($manager->assigned_drivers == 'none') {
                        $manager->assigned_drivers == 'some';
                        $manager->save();
                    }

                }   // for each record in file

                // TODO limit deletion of missing drivers as we are testing on small sample of Pulte file
                $limit = 2;
                $i = 0;
                foreach ($missing_drivers as $user_id => $driver_name) { // we could not find it so terminate it
                    if ($i++ > 2) {
                        break;
                    }
                    $action_taken = $this->deleteMissingDriver($user_id, $manager->manager_id, $driver_name);
                    if ($action_taken == 'TERMINATED') {
                        $return_notes[] = "MISSING $driver_name TERMINATED";
                        $total_terminated++;
                    }
                }

                // update manager assign drivers to 'none' if there are no drivers assigned to them.
                // deactivate managers without drivers
                $res = $this->updateManagerAssignDrivers($company_id);
                if ($res->reinstated > 0) {
                    $total_reinstated += $res->reinstated;
                    $return_notes = array_merge($return_notes, $res->reinstated_arr);
                }
                if ($res->deactivated > 0) {
                    $total_deactivated += $res->deactivated;
                    $return_notes = array_merge($return_notes, $res->deactivated_arr);
                }

            }   // for each file

            $short_description = '';
            $short = [];
            if ($total_added > 0) $short[] = "Drivers Added $total_added";
            if ($total_terminated > 0) $short[] = "Drivers Terminated $total_terminated";
            if ($total_be_terminated > 0) $short[] = "Drivers to be terminated $total_be_terminated";
            if ($total_reinstated > 0) $short[] = "Managers Reinstated $total_reinstated";
            if ($total_deactivated > 0) $short[] = "Managers Deactivated $total_deactivated";

            return ['short_description' => implode(', ', $short), 'long_description' => $return_notes];

        } catch (\Exception $e) {
            return ['short_description' => 'Error', 'long_description' => $e->getMessage() . ' at line ' . $e->getLine() . ' at ' . $e->getFile()];
        }

    }

    private function deleteMissingDriver($user_id, $manager_id, $driver_name)
    {
        $action_taken = 'NONE';
        $driver_profile = DriverProfile::where('user_id',$user_id)->first();
        if ($driver_profile) {      // in case it was already soft deleted
            $color = $this->streetLightCalculator->colorOfStreetLight($driver_profile->street_light_id);
            if( $color == 'Green' || $color == 'Yellow' || $color == 'Red' && ($driver_profile->street_light_id & RED_TERMINATED) == 0 ) {

                $new_profile = $driver_profile->replicate();
                $driver_profile->delete();
                $new_profile->stop_date = date("Y-m-d");
                $new_profile->active = 0;
                $new_profile->street_light_id = RED_TERMINATED;
                $new_profile->save();
                $action_taken = 'TERMINATED';

                \Debugbar::info("Check missing terminated driver $driver_name user_id=$user_id");

                \DB::table('manager_driver')->where('driver_id',$user_id)
                        ->update(['deleted_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s")]);
            }
        }
        return $action_taken;
    }


    private function pulteRecordColumns() {
        return ['last_name','first_name','email','employee_number','job_title','street','city','state_province',
            'zip_postal','country', 'market','area','vehicle_profile','annual_mileage', 'territory', 'manager_id',
            'manager_last_name', 'manager_first_name', 'manager_email', 'stop_date', 'status', 'start_date',
            'rehire_date' ];
    }

    private function generatePulteRecord($row, $columns){
        $arr = [];
        foreach( $row as $i => $token) {
            $arr[$columns[$i]] = trim($token);
        }
        return $arr;
    }

    private function pulteValidateRecord($record, $company_id, $vehicleProfiles, $mileageBandNames, $divisionNames) {

        $err_arr = [];
        $alert_arr = [];

        \Debugbar::info('Validation started ...');

        if( empty($record->last_name))  { $err_arr[] = "Driver Last Name is missing"; }
        if( empty($record->last_name))  { $err_arr[] = "Driver First Name is missing"; }
        if( empty($record->email)){
            $err_arr[] = "Driver Email is missing for driver $record->last_name $record->first_name";
        } else {
            if (!preg_match("/^.+@.+\..+$/", $record->email)) {
                $err_arr[] = "Driver Email $record->email is invalid";
            }
        }
        if( empty($record->street)) { $err_arr[] = "Driver Street Address is missing"; }
        if( empty($record->city)) { $err_arr[] = "Driver City Address is missing"; }
        if( empty($record->zip_postal)) { $err_arr[] = "Driver Zip Address is missing"; }

        if( empty($record->state_province)){
            $err_arr[] = "Driver State Address is missing";
        } else if( ! StateProvince::where('short_name',$record->state_province)) {
            $err_arr[] = "Invalid State: $record->state_province abbreviation";
        }
        if( empty($record->country)) { $err_arr[] = "Driver Country Address is missing"; }

        else if( $record->country == 'US' && !empty($record->state_province) && !empty($record->zip_postal)) {
            $padded_zip = str_pad($record->zip_postal,5, '0', STR_PAD_LEFT);
            // \Debugbar::info('$padded_zip', $padded_zip);
            $zip =  ZipCode::find($padded_zip);
            if( !$zip) {
                $err_arr[] = "Unknown Driver Zip Adddress: $record->zip_postal";
            } else if( $zip->state->short_name != $record->state_province) {
                $err_arr[] = "Driver Zip Address $record->zip_postal was assigned to state $record->state_province instead of " . $zip->state->short_name;
            }
        }
        else if( $record->country == 'CA' && !empty($record->state_province) && !empty($record->zip_postal)) {
            $zip = PostalCode::find($record->zip_province);
            if( !$zip) {
                $err_arr[] = "Unknown Driver Postal Adddress: $record->zip_postal";
            } else if( $zip->state->short_name != $record->state_province) {
                $err_arr[] = "Driver Postal Address $record->zip_postal was assigned to province $record->state_province instead of " . $zip->state->short_name;
            }
        }
        if( $record->country != 'US' && $record->country != 'CA') {
            $err_arr[] = "Invalid Driver Country Address: $record->country";
        }

        if( empty($record->market)) {
            $err_arr[] = "Market Name is missing";
        }

        if( empty($record->vehicle_profile)) {
            $err_arr[] = "Vehicle Profile is missing";
        } else if( in_array($record->vehicle_profile,$vehicleProfiles ) === false) {
            $err_arr[] = "Invalid Vehicle Profile Postfix: $record->vehicle_profile";
        }

        if( empty($record->annual_mileage)) {
            $err_arr[] = "Mileage Band is missing";
        } else if( !in_array($record->annual_mileage,$mileageBandNames )) {
            $err_arr[] = "Invalid Mileage Band: $record->annual_mileage";
        }

        if( empty($record->territory)) {
            $err_arr[] = "Territory is missing";
        } else if( $record->territory != 'Home Address City') {
            $err_arr[] = "Invalid Territory type: $record->territory, Only Home Address City is implemented";
        }

        if( empty($record->manager_id)) {
            $err_arr[] = "Manager ID is missing";
        }
        if( empty($record->manager_last_name)) {
            $err_arr[] = "Manager Last Name is missing";
        }
        if( empty($record->manager_first_name)) {
            $err_arr[] = "Manager First Name is missing";
        }
        if( empty($record->manager_email)) {
            $err_arr[] = "Manager Email is missing";
        } else if (!preg_match("/^.+@.+\..+$/", $record->manager_email)) {
            $err_arr[] = "Manager Email: $record->email is invalid";
        }


        // check if we have such user related to driver and if names match
        if( !empty($record->email) && !empty($record->last_name) && !empty($record->first_name)) {
            $data = $this->get_user_from_email($company_id, $record->email);
            if( $data && ( $data->first_name != $record->first_name || $data->last_name != $record->last_name)) {
                $err_arr[] = "Record found with driver email, but first or last name do not match";
            }
            if( empty($data->driver_id) && $record->status == 'T' ) {
                $err_arr[] = "Cannot terminate driver that doesn't exist in the database: Employee ID: $record->employee_number";
            }
        }

        // check if we have such user related to manager
        if( !empty($record->manager_email) && !empty($record->manager_last_name) && !empty($record->manger_first_name)) {
            $data = $this->get_user_from_email($company_id, $record->manager_email);
            if( $data && ( $data->manager_first_name != $record->manager_first_name || $data->manager_last_name != $record->manager_last_name)) {
                $err_arr[] = "Record found with manager email, but first or last name do not match";
            }
        }

        if( $record->status == 'T') {
            if( empty($record->stop_date)) {
                $err_arr[] = "Term Date cannot be empty for terminated drivers.";
            } else {
                $termDate = strtotime($record->stop_date);
                if($termDate > time()) {
                    $err_arr[] = "Term Date cannot be in the future: $record->stop_date";
                }
            }
        }

        \Debugbar::info('Verification Ended ...',$err_arr);

        return $err_arr;

    }

    private function get_user_from_email($company_id, $email){
        $sql_email = str_replace("'","\'",$email);
        $sql = <<<EOQ
select u.id AS user_id, u.first_name, u.last_name, u.active, dp.id, mp.manager_id, ap.administrator_id 
from users as u
  left join driver_profiles dp ON u.id = dp.id AND dp.company_id = $company_id
  left join manager_profiles mp ON u.id = mp.user_id AND mp.company_id = $company_id
  left join administrator_profiles ap ON u.id = ap.user_id AND ap.company_id = $company_id
  where u.email = '$sql_email'
EOQ;
        $data = \DB::select(\DB::raw($sql));
        \Debugbar::info('get_user_from_email:',$sql,$data);
        return $data ? $data[0] : null ;

    }

    private function getVehicleProfileNames($company_id){

        $vp_arr = [];
        $vps = VehicleProfile::where('company_id', $company_id)->get();
        foreach( $vps as $v) {
            $vp_arr[$v->id] = trim($v->name_postfix);
        }
        return $vp_arr;
    }

    private function getMileageBandsNames(){
        $mbn_arr = [];
        $mbn = MileageBand::all();
        foreach( $mbn as $n) {
            if( $n->mileage_start != 50000) {
                $mbn_arr[$n->id] = $n->mileage_start . ' - ' . $n->mileage_end;
            } else {
                $mbn_arr[$n->id] = '> ' . $n->mileage_start;
            }
        }
        return $mbn_arr;
    }

    private function getDivisionNames($company_id){
        $div_arr = [];
        $div = Division::where('company_id', $company_id)->get();
        foreach( $div as $d){
            $div_arr[$d->id] = trim($d->name);
        }
        return $div_arr;
    }

    private function getStateProvinces(){
        $state_arr = [];
        $states = StateProvince::all();
        foreach( $states as $s){
            $state_arr[$s->id] = $s->short_name;
        }
        return $state_arr;
    }

    private function add_new_driver($company_id, $divisionNames, $stateProvincesNames,
                                    $vehicleProfileNames, $mileageBandNames, $record) {

        $alerts = null;
        $driverProfile = null;

        // checking if we have user record from manager or administrator
        $user_data = $this->get_user_from_email($company_id, $record->email);
        \Debugbar::info('$user_data',$user_data);

        $if_fail_delete = 0; // store what should be deleted if fail for any reason

        //////////////////////////////////////////////////////////////////////
        try {
            if (!$user_data) {
                \Debugbar::info("We have to create a new user record");
                $driverUser = User::create([
                    'username' => app('\App\Http\Controllers\UserController')->generateUsername($record->first_name, $record->last_name),
                    'first_name' => $record->first_name,
                    'last_name' => $record->last_name,
                    'email' => $record->email,
                    'password' => \Hash::make('secret'), // TODO replace this make('secret')
                ]);
                $user_id = $driverUser->id;
                $if_fail_delete = 1;
                \Debugbar::info("New user record was created with id=$user_id");
            } else {
                \Debugbar::info("User id $user_data->id was found based on his email");
                $user_id = $user_data->user_id;
            }

            $fullAddress = $record->street . ' ' . $record->city . ', ' . $record->state_province;
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($fullAddress) . "&key=" . config('app.google_key');
            $geocode = file_get_contents($url);
            $output = json_decode($geocode);
            $latitude = $output->results[0]->geometry->location->lat;
            $longitude = $output->results[0]->geometry->location->lng;

            \Debugbar::info("lat lng for $fullAddress", $latitude,$longitude);

            $state_province_id = array_search(trim($record->state_province),$stateProvincesNames);
            $driverAddress = Address::create([
                'user_id' => $user_id,
                'street' => $record->street,
                'city' => $record->city,
                'zip_postal' => $record->zip_postal,
                'state_province_id' => $state_province_id,
                'map_latitude' => $latitude,
                'map_longitude' => $longitude,
                'country_id' => $record->country == 'US' ? 1 : 2
            ]);
            $if_fail_delete = +2;
            $address_id = $driverAddress->id;
            \Debugbar::info('Address Created:', $driverAddress);

            $division_id = array_search(trim($record->market), $divisionNames);
            $vehicle_profile_id = array_search($record->vehicle_profile, $vehicleProfileNames);
            $mileage_band_id = array_search($record->annual_mileage, $mileageBandNames);

            $step = 4;
            $driverProfile = DriverProfile::create([
                'user_id' => $user_id,
                'active' => 1,
                'company_id' => $company_id,
                'division_id' => $division_id,
                'street_light_id' => 1,
                'employee_number' => $record->employee_number,
                'start_time' => date("Y-m-01"),
                'job_title' => $record->job_title,
                'address_id' => $address_id,
                'vehicle_profile_id' => $vehicle_profile_id,
                'cost_center' => $record->area,
                'mileage_band_id' => $mileage_band_id,
                'territory_type_id' => 1,
                'car_policy_accepted' => 0,
                'reimbursement_detail_id' => null
            ]);
            \Debugbar::info('Driver Profile created:',$driverProfile);
            $driver_id = $driverProfile->id;
            $if_fail_delete = +4;

        } catch (\Exception $e) {
            \Debugbar::error('line:' . $e->getLine() . " error: " . $e->getMessage());
            if ($if_fail_delete & 4) {
                \Debugbar::error('Could not create Driver Profile');
                $alerts[] = "Could not create Driver Profile for record->last_name $record-first_name. Skipped";
                if(isset($driver_id)) {
                    DriverProfile::find($driver_id)->delete();
                }
            }
            if ($if_fail_delete & 2) {
                \Debugbar::error('Could not create Address');
                $alerts[] = "Could not create Address for record->last_name $record->first_name. Skipped";
                if( isset($address_id)) {
                    Address::find($address_id)->delete();
                }
            }
            if ($if_fail_delete & 1) {
                \Debugbar::error('Could not create User');
                $alerts[] = "Could not create User for record->last_name $record->first_name. Skipped";
                if( isset($user_id)) {
                    User::find($user_id)->delete();
                }
            }
        }

        \Debugbar::error($alerts);

        return (object)['driver' => $driverProfile, 'errors' => $alerts];
    }

    private function updatePulteDriver($user_id, $manager_id, $divisionNames, $stateProvinceNames, $driverProfileNames,
                                   $mileageBandNames, $record) {
        // find differences
        $change_found = false;
        $action_taken = null;

        // prepare new profile if changes found
        $old_profile = DriverProfile::where('user_id', $user_id)->first();
        $new_profile = $old_profile->replicate();

        \Debugbar::info("Old profile:", $old_profile);
        \Debugbar::info("Old profile address", $old_profile->address());

        $new_profile->reimbursement_detail_id = null;   // be sure that we are not passing previous reimbursement details

        if( $divisionNames[$old_profile->division_id] != $record->market ) {
            $change_found = true;
            $new_profile->division_id = array_search($record->market, $divisionNames);
        }

        if( $mileageBandNames[$old_profile->mileage_band_id] != $record->annual_mileage) {
            $change_found = true;
            $new_profile->mileage_band_id = array_search($record->annual_mileage,$mileageBandNames);
        }

        if( $driverProfileNames[$old_profile->vehicle_profile_id] != $record->vehicle_profile) {
            $change_found = true;
            $new_profile->vehicle_profile_id = array_search($record->vehicle_profile,$driverProfileNames);
        }

        if( $old_profile->employee_number != $record->employee_number) {
            $change_found = true;
            $new_profile->employee_number = $record->employee_number;
        }

        if( $old_profile->job_title != $record->job_title) {
            $change_found = true;
            $new_profile->job_title = $record->job_title;
        }

        // we have only one territory = 1, so do not change it. Also territory list is null

        $old_address = $old_profile->address();
        $new_address = $old_address->replicate();
        if( $old_address->street != $record->street || $old_address->city != $record->city  ||
            $old_address->zip_postal != $record->zip_postal || $old_address->state) {
            $change_found = true;

            $old_address->delete();

            $fullAddress = $record->street . ' ' . $record->city . ', ' . $record->state_province;
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($fullAddress) . "&key=" . config('app.google_key');
            $geocode = file_get_contents($url);
            $output = json_decode($geocode);
            $latitude = $output->results[0]->geometry->location->lat;
            $longitude = $output->results[0]->geometry->location->lng;

            \Debugbar::info("lat lng for $fullAddress", $latitude,$longitude);

            $new_address->street = $record->street;
            $new_address->city = $record->city;
            $new_address->zip_postal = $record->zip_postal;
            $new_address->state_province_id = array_search($record->state_province, $stateProvinceNames);
            $new_address->map_latitude = $latitude;
            $new_address->map_longitude = $longitude;
            $new_address->created_at = date("Y-m-d H:i:s");
            $new_address->updated_at = date("Y-m-d H:i:s");
            $new_address->save();
            $address_id = $new_address->id;

            $new_profile->address_id = $address_id;

        }


        // the best part - status
        if( $record->status == 'T') {   // terminate driver

            $color = $this->streetLightCalculator->colorOfStreetLight($old_profile->street_light_id);
            if( $color == 'Green' || $color == 'Yellow' || $color == 'Red' && ($old_profile->street_light_id & RED_TERMINATED) == 0 ) {
                $change_found = true;
                if( $record->stop_date < date("Y-m-01")) {  // last month, so terminate now
                    $new_profile->stop_date = $record->stop_date;
                    $new_profile->active = 0;
                    $new_profile->street_light_id = RED_TERMINATED;
                    $action_taken = 'TERMINATED';
                    \DB::table('manger_driver')->where('manager_id',$manager_id)->where('driver_id',$user_id)
                        ->update('deleted_at', date("Y-m-d H:i:s"));
                } else {                                            // this month so terminate on Approval End + 2 next month

                    list($yyyy,$mm) = explode('-',date("Y-m"));
                    if( $mm == 12) { $mm = 1; $yyyy++; }

                    $scheduled_date = spintf("%04d-%02d-10", $yyyy, $mm);
                    \DB::table('scheduled_terminations')->insert([
                       'user_id' => $record->user_id,
                       'scheduled_date' => $scheduled_date,
                       'stop_date' => $record->stop_date
                    ]);
                    $action_taken = 'TO BE TERMINATED';
                }
            }
        } elseif( $record->status == 'A') {
            if( $old_profile->street_light_id & RED_TERMINATED) {
                $new_profile->street_light_id = 1;
                $new_profile->stop_date = null;
                $mm = date("n") == 12 ? 1 : date("n")+1;
                $yy = date("n") == 12 ? date("Y")+1 : date("Y");
                $new_profile->start_date = sprintf("%04d-%02d-01", $yy, $mm );
                $change_found = true;
                $action_taken = 'ADDED';
            }
        }

        if( $change_found ) {
            if( !$action_taken ) {
                $action_taken = 'UPDATED';
            }
            \Debugbar::info("New Driver Profile created");
            $old_profile->delete();
            $new_profile->save();
        } else {
            \Debugbar::info("No changes in Driver Profile");
            $action_taken = "No Changes";
        }

        return $action_taken;

    }

    private function find_manager($company_id, $record) {

        $activated = 0;
        // try by manager number
        $manager = ManagerProfile::where('company_id', $company_id)
            ->where('manager_number', $record->manager_id)->first();

        if( !$manager ) {
            // try by last name, first name and email

            \Debugbar::info("Manager not found by his number");

            $manager_query = <<<EOQ
SELECT m.* from manager_profiles m, users u 
  WHERE m.user_id = u.id AND u.last_name = '$record->manager_last_name' 
  AND u.first_name = '$record->manager_first_name'
  AND u.email = '$record->manager_email' 
  AND m.company_id = $company_id AND m.deleted_at IS NULL
EOQ;

            $managers = \DB::select(\DB::raw($manager_query));

            \Debugbar::info("Manager Profile found by email and name: ", $managers);

            if( $managers ) {

                \Debugbar::info("Update manager profile with number: $record->manager_id");
                $m = $managers[0];
                $manager = ManagerProfile::find($m->id);    // we need it to use $manager->save()
                $manager->manager_number = $record->manager_id;
                $manager->save();

            } else {
                \Debugbar::info("No such manager");
                $manager = null;
            }
        }

        if( $record->status == 'A' && $manager && $manager->active == 0 ) {   // create new active manager profile
            $new_manager = $manager->replicate();
            $new_manager->active = 1;
            $manager->delete();
            $new_manager->save();
            return (object)['activated' => 1, 'manager' => $new_manager];
        }
        return (object)['activated' => 0, 'manager' => $manager];
    }

    private function add_new_manager($company_id, $record) {

        $user = $this->get_user_from_email($company_id, $record->manager_email);
        \Debugbar::info("User from get_user_from_email: ", $user);

        if( $user ) {
            $user_id = $user->user_id;
            \Debugbar::info("We have user id: $user_id");
            // TODO update if inactive

        } else {

            $user = User::create([
                'username' => app('\App\Http\Controllers\UserController')
                    ->generateUsername($record->manager_first_name, $record->manager_last_name),
                'first_name' => $record->first_name,
                'last_name' => $record->last_name,
                'email' => $record->manger_email,
                'password' => \Hash::make('silver'),
            ]);
            $user_id = $user->id;
            \Debugbar::info("We create a new user: id= $user_id");
        }


        $manager = ManagerProfile::create([
            'user_id' => $user_id,
            'manager_id' => $user_id,
            'company_id' => $company_id,
            'manager_number' => $record->manager_id,
            'assigned_drivers' => 'some',
            'active' => 1
        ]);
        return $manager;

    }

    private function assignDriverToManager( $driver_id, $manager_id){

        $md = \DB::table('manager_driver')->where('manager_id',$manager_id)
            ->where('driver_id',$driver_id)->first();

        \Debugbar::info('Manager Driver record: ', $md);

        if( !$md || $md && $md->deleted_at != null ) {
            \Debugbar::info('Add New manager driver record');
            \DB::table('manager_driver')->insert([
                'manager_id' => $manager_id,
                'driver_id' => $driver_id,
                'created_at' => date('Y-m-d H:i:d'),
                'updated_at' => date('Y-m-d H:i:d')
            ]);
        }
    }

    private function updateManagerAssignDrivers($company_id) {

        $reinstated_total = 0;
        $deactivated_total = 0;
        $reinstated_names = [];
        $deactivated_names = [];

        // activate managers

        $sql = <<<EOQ
SELECT mp.id, u.last_name, u.first_name FROM manager_profiles mp, users u 
WHERE mp.company_id = $company_id AND mp.deleted_at IS NULL AND mp.assigned_drivers = 'none'
AND mp.user_id = u.id
        AND EXISTS 
        ( SELECT driver_id FROM manager_driver 
            WHERE manager_id = mp.manager_id AND deleted_at IS NULL LIMIT 1 )
EOQ;
        $managers = \DB::select(\DB::raw($sql));
        foreach($managers as $m){
            $manager = ManagerProfile::find($m->id);
            $new_manager = $manager->replicate();
            $manager->delete();
            $new_manager->active = 1;
            $new_manager->assigned_drivers = 'some';
            $new_manager->save();

            $reinstated_total++;
            $reinstated_names[] = $m->last_name . ' ' . $m->first_name . ' REINSTATED';
        }

        // deactivate managers without drivers

        $sql = <<<EOQ
SELECT mp.id, mp.active, u.last_name, u.first_name FROM manager_profiles mp
    JOIN users u ON mp.user_id = u.id
    LEFT JOIN manager_driver md ON mp.manager_id = md.manager_id AND md.deleted_at IS NULL
    WHERE mp.company_id = $company_id AND mp.assigned_drivers != 'none' AND mp.deleted_at IS NULL
    AND md.driver_id IS null
    ORDER BY u.last_name, first_name
EOQ;

        $managers = \DB::select(\DB::raw($sql));
        foreach($managers as $m){
            $manager = ManagerProfile::find($m->id);
            $new_manager = $manager->replicate();
            $manager->delete();
            $new_manager->active = 0;
            $new_manager->assigned_drivers = 'none';
            $new_manager->save();

            $deactivated_total++;
            $deactivated_names[] = $m->last_name . ' ' . $m->first_name . ' DEACTIVATED';
        }

        return (object)['reinstated' => $reinstated_total, 'reinstated_arr' => $reinstated_names,
                        'deactivated' => $deactivated_total, 'deactivated_arr' => $deactivated_names];
    }


    private function pulteErrorLog($id,$error) {

    }

    public function getCompanyAudits()
    {
        $audits = Audit::all();
        $modified = [];

        foreach ($audits as $audit) {
            $modified[] = $audit->getModified();
        }

        return ['audits' => $audits, 'modified' => $modified];
    }
}
