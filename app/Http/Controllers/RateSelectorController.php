<?php

namespace App\Http\Controllers;

use App\InterestRate;
use App\MileageBand;
use App\Month;
use App\PostalCode;
use App\RateSelectorDefaults;
use App\ReimbursementCalculatorSession;
use App\TerritoryType;
use App\Vehicle;
use App\ResaleCondition;
use App\StateProvince;
use App\ZipCode;
use App\Http\Controllers\ReimbursementController;
use Illuminate\Http\Request;

use App\Http\Traits\UtilityTraits;
use App\Http\Traits\CommonTraits;
use PhpOffice\PhpSpreadsheet;

class RateSelectorController extends Controller
{

    use UtilityTraits;
    use CommonTraits;

    protected $reimbursementController;

    public function __construct()
    {
        $this->middleware('auth');
        $this->reimbursementController = new ReimbursementController();
    }

    /**
     * This function does what its name says.
     *
     * In the future, we have the infrastructure to convert this to grab user specific defaults.
     *
     * For now, the defaults are the same across all users
     */
    public function getRateSelectorDefaultParameters()
    {
        return RateSelectorDefaults::first();
    }


    /**
     * Function to calculate rates for the rate selector.
     *
     * This function follows the outline of ReimbursementController@getDataForReimbursementFromDriver
     *
     * @param Request $request
     *  - request must contain:
     *      - parameters (an object of parameters chosen on the rate selector)
     *      - driverLocation (a lat/lng pair to complement the location data within parameters)
     */
    public function calculateReimbursementForRateSelector(Request $request)
    {
        $parameters = json_decode(json_encode($request->parameters));
        $driverLocation = json_decode(json_encode($request->driverLocation));

        // validation
        if ($request->validate) {
            $errors = [];
            // check zip/postal and state province match
            if ($parameters->country_id == 1) {
                $zip = ZipCode::find($parameters->zip_postal_code);
                if ($zip->state_id != $parameters->state_province_id) {
                    $errors['zip_postal_code'] = ['State and zip code do not match'];
                }
            } else {
                $postal = PostalCode::find($parameters->zip_postal_code);
                if ( !isset($postal) || $postal->province_id != $parameters->state_province_id) {
                    $errors['zip_postal_code'] = ['Province and postal code do not match'];
                }
            }

            if ($parameters->territory_type_id > 2) {
                if ( !isset($parameters->territory_list) || sizeof($parameters->territory_list) < 1 ) {
                    $errors['territory_list'] = ['A territory selection is required'];
                }
            }

            if ( !empty($errors) ) {
                return response($errors, 400);
            }
        }

        if ($request->from_reimbursement_calculator) {
            // insert/update a record in the reimbursement_calculator_sessions table

            $previousSession = ReimbursementCalculatorSession::where('user_id', $parameters->user_id)->first();

            if ( isset($previousSession) ) {
                // must pass array
                $previousSession->update($request->parameters);
            } else {
                // must pass array
                ReimbursementCalculatorSession::create($request->parameters);
            }

        }



        /**
         * In order to calculate reimbursement using the proxy methods we need to formulate an object
         * with a number of sub objects. These sub objects include a vehicle profile, as well as
         * geo_data
         */
        $data = new \stdClass();

        $data->vehicleProfile = new \stdClass();
        $data->geo_data = new \stdClass();
        $data->geo_data->address = new \stdClass();


        /** Fill the vehicleProfile object */
        $data->vehicleProfile->vehicle_id = $parameters->vehicle_id;
        $data->vehicleProfile->retention = $parameters->retention;
        $data->vehicleProfile->capital_cost = $parameters->capital_cost;
        $data->vehicleProfile->resale_condition_id = $parameters->resale_condition_id;
        $data->vehicleProfile->business_use_percent = $parameters->business_use_percent;
        $data->vehicleProfile->calculate_fixed = 1;
        $data->vehicleProfile->calculate_variable = 1;
        $data->vehicleProfile->capital_cost_adj = $parameters->capital_cost_adj;
        $data->vehicleProfile->capital_cost_tax_adj = $parameters->capital_cost_tax_adj;
        $data->vehicleProfile->fuel_economy_adj = $parameters->fuel_economy_adj;
        $data->vehicleProfile->maintenance_adj = $parameters->maintenance_adj;
        $data->vehicleProfile->repair_adj = $parameters->repair_adj;
        $data->vehicleProfile->depreciation_adj = $parameters->depreciation_adj;
        $data->vehicleProfile->insurance_adj = $parameters->insurance_adj;
        $data->vehicleProfile->fuel_price_adj = $parameters->fuel_price_adj;
        $data->vehicleProfile->resale_value_adj = $parameters->resale_value_adj;
        $data->vehicleProfile->capital_cost_tax_adj = $parameters->capital_cost_tax_adj;
        $data->vehicleProfile->monthly_payment_adj = $parameters->monthly_payment_adj;
        $data->vehicleProfile->finance_cost_adj = $parameters->finance_cost_adj;
        $data->vehicleProfile->fee_renewal_adj = $parameters->fee_renewal_adj;
        $data->vehicleProfile->fixed_adj = $parameters->fixed_adj;
        $data->vehicleProfile->variable_adj = $parameters->variable_adj;
        $data->vehicleProfile->business_use_percent_back = $parameters->business_use_percent;


        $data->vehicleProfile->vehicle = Vehicle::find($data->vehicleProfile->vehicle_id);


        //NOTE! Had to rename vehicleProfile to vehicle_profile... it is present in 153 places in the
        // reimbursement controller
        // TODO: untangle the vehicle_profile web
        // as of today, I do not have the time to go through all end points and change vehicle_profile to vehicleProfile
        $data->vehicle_profile = $data->vehicleProfile;

        /** Fill the geo_data object */
        $data->geo_data->year = $parameters->fuel_year;
        $data->geo_data->month = $parameters->fuel_month_id;



        // construct the address obj
        if ( isset($driverLocation->lat) ) {
            $data->geo_data->address->latitude = $driverLocation->lat;
            $data->geo_data->address->longitude = $driverLocation->lng;
        } else {
            $zipPostal = $parameters->country_id == 1 ? ZipCode::find($parameters->zip_postal_code) : PostalCode::find($parameters->zip_postal_code);
            $data->geo_data->address->latitude = $zipPostal->latitude;
            $data->geo_data->address->longitude = $zipPostal->longitude;
        }

        $data->geo_data->address->zip_postal = $parameters->zip_postal_code;
        $data->geo_data->address->state_province_id = $parameters->state_province_id;
        $data->geo_data->address->country_id = $parameters->country_id;

        // territory, mileage band, service plan
        $data->geo_data->mileage_band_id = $parameters->mileage_band_id;
        $data->geo_data->territory_type_id = $parameters->territory_type_id;
        $data->geo_data->territory_list = is_array($parameters->territory_list) ? implode(",", $parameters->territory_list) : $parameters->territory_list;
        $data->geo_data->plan = $parameters->service_plan;

        // force green
        $data->geo_data->street_light_id = GREEN_REIMBURSEMENT;

        // since we are calculating from scratch, we do not want to look in the reimbursement_details table
        $data->geo_data->reimbursement_detail_id = null;
        $data->geo_data->reimbursement_details = null;

        // calculate rates
        $fixedReimbursementData = $this->reimbursementController->calculateFixedReimbursementProxy($data);
        $variableReimbursementData = $this->reimbursementController->calculateVariableReimbursementProxy($data);

        /** FORMAT THE DATA */

        if ( isset($fixedReimbursementData['fixedReimbursement']) ) {
            $fixedReimbursementData['fixed_reimbursement'] = $fixedReimbursementData['fixedReimbursement'];
            unset($fixedReimbursementData['fixedReimbursement']);
        }

        if ( isset($variableReimbursementData['variableReimbursement']) ) {
            $variableReimbursementData['variable_reimbursement'] = $variableReimbursementData['variableReimbursement'];
            unset($variableReimbursementData['variableReimbursement']);
        }


        return [
            'fixedReimbursementData' => $fixedReimbursementData,
            'variableReimbursementData' => $variableReimbursementData,
        ];
    }


    public function generateRateSelectorPdf(Request $request)
    {

        $r = json_decode(json_encode($request->all()));

        // general, location, and vehicle data
        $mileageBand = MileageBand::find($r->parameters->mileage_band_id);
        $data = [
            'company_name' => $r->parameters->company_name,
            'state_province_name' => StateProvince::find($r->parameters->state_province_id)->name, // MADE ID
            'zip_postal' => $r->parameters->zip_postal_code,
            'vehicle_category' => $r->vehicle_category,
            'vehicle_profile' => $r->vehicle->name,
            'capital_cost_type' => ucwords( implode(' ', explode('_', $r->parameters->capital_cost) ) ),
            'resale_condition' => ResaleCondition::find($r->parameters->resale_condition_id)->name,
            'retention' => $r->parameters->retention . ' Years',
            'business_use_percent' => $r->parameters->business_use_percent . '%',
            'business_use_percent2' => $r->parameters->business_use_percent . '%',
            'wsj_prime' => InterestRate::where('year', $r->parameters->vehicle_year)
                            ->where('country_id', $r->parameters->country_id)
                            ->first()
                            ->interest_rate,
            'city_name' => $r->parameters->city,
            'fuel_territory' => $r->variableReimbursementData->variableReimbursementDetails->fuelAverage->geo_list,
            'mileage_band' => $this->formattedMileageBand($mileageBand->id),
            'trade_in_mileage' => $mileageBand->mileage_start * $r->parameters->retention . ' - ' . $mileageBand->mileage_end * $r->parameters->retention
        ];

        // gather fixed data
        $data['capital_cost'] = '$' . number_format($r->fixedReimbursementData->fixedReimbursementDetails->capitalCost->amount_adj, 2, '.', ',');
        $data['resale_value'] = '$' . number_format($r->fixedReimbursementData->fixedReimbursementDetails->resaleValue->amount_adj, 2, '.', ',');
        $data['depreciation'] = number_format($r->fixedReimbursementData->fixedReimbursementDetails->depreciation->amount_adj, 2, '.', ',');
        $data['capital_cost_tax'] = number_format($r->fixedReimbursementData->fixedReimbursementDetails->tax->amount_adj, 2);

        // sometimes this is not present
        if (isset($fixedReimbursementData->fixedReimbursementDetails->financeCost)) {
            $data['finance_cost'] = number_format($r->fixedReimbursementData->fixedReimbursementDetails->financeCost->amount_adj, 2);
        } else {
            $data['finance_cost'] = 0.00;
        }


        $data['insurance'] = number_format($r->fixedReimbursementData->fixedReimbursementDetails->insurance->amount_adj, 2);
        $data['fee_renewal'] = number_format($r->fixedReimbursementData->fixedReimbursementDetails->feeRenewal->amount_adj, 2);
        $data['total_fixed_costs'] = number_format($r->fixedReimbursementData->fixed_reimbursement / ($r->parameters->business_use_percent / 100), 2);
        $data['fixed_rate'] = '$'. number_format($r->fixedReimbursementData->fixed_reimbursement, 2, '.', ',');
        $data['fixed_rate_big'] = '$' . number_format($r->fixedReimbursementData->fixed_reimbursement, 2, '.', ',');


        // gather variable data
        $data['fuel_economy'] = number_format($r->variableReimbursementData->variableReimbursementDetails->fuelEconomy->amount_adj,2);
        $data['fuel_average'] = number_format($r->variableReimbursementData->variableReimbursementDetails->fuelAverage->amount_adj,2);
        $data['fuel'] = number_format($r->variableReimbursementData->variableReimbursementDetails->fuelPrice->amount,2);
        $data['maintenance'] = number_format($r->variableReimbursementData->variableReimbursementDetails->maintenance->amount_adj,2);
        $data['repair'] = number_format($r->variableReimbursementData->variableReimbursementDetails->repair->amount_adj,2);
        $data['variable_rate'] = '¢' . number_format($r->variableReimbursementData->variable_reimbursement,2);
        $data['variable_rate_big'] = '¢' . number_format($r->variableReimbursementData->variable_reimbursement,2);


        // Labels and units
        if ($r->parameters->country_id == 1) {
            $data['fuel_economy_label'] = ' (m/g):';
            $data['fuel_price_label'] = ' ($/g):';
            $data['zip_postal_label'] = "ZIP Code:";
            $data['state_province_label'] = "State:";
            $data['mile_km_label_big'] = 'mile';
        } else {
            $data['fuel_economy_label'] = ' (l/100km):';
            $data['fuel_price_label'] = ' (¢/l):';
            $data['zip_postal_label'] = "Postal Code:";
            $data['state_province_label'] = "Province:";
            $data['mile_km_label_big'] = 'km';
        }


        // path to file FROM the autoload
        $pdf = new \FPDM('../resources/assets/pdfs/calculated_rates_template.pdf');
        $pdf->Load($data,true);
        $pdf->Merge();
        $tmp_dir = storage_path() . '/app/public/tmp/';
        $file_name = 'calculated_rates_' . date("Y-m-d") . '.pdf';
        $pdf->Output('F', $tmp_dir . $file_name);

        return ['file_name' => $file_name];
    }

    public function generateRateSelectorExcel(Request $request)
    {
        // calculate reimbursement for each mileage band
        $mileageBands = MileageBand::all();
        $parameters = $request->parameters;
        $selected_mileage_band_id = $request->parameters['mileage_band_id'];
        $results = [];
        foreach($mileageBands as $mileageBand) {
            $parameters['mileage_band_id'] = $mileageBand->id;
            $request->merge(['parameters' => $parameters]);
            $results[$mileageBand->id] = json_decode(json_encode($this->calculateReimbursementForRateSelector($request)));
        }

        $parameters = json_decode(json_encode($request->parameters));

        $spreadsheet = PhpSpreadsheet\IOFactory::load(base_path() . '/resources/assets/xls/mileage_band_rate_sheet_template.xls');
        $worksheet = $spreadsheet->getActiveSheet();

        /** FORMAT AND INSERT THE DATA */
        $cellsToHighlightGrey = [];

        // title
        $title = 'MILEAGE BANDS RATE SHEET - ' . $parameters->service_plan;
        $worksheet->getCell('A1')->setValue($title);

        // date
        $date = date('F jS, Y', strtotime('now'));
        $worksheet->getCell('L3')->setValue($date);

        /** VEHICLE PROFILE */
        $vehicle = Vehicle::find($parameters->vehicle_id);
        $worksheet->getCell('B6')->setValue($vehicle->category->name);
        $worksheet->getCell('B7')->setValue($vehicle->formatted_name());

        // highlight the correct capital cost type
        switch($parameters->capital_cost) {
            case 'msrp':
                $cellsToHighlightGrey[] = 'B9';
                break;
            case 'blue_book':
                $cellsToHighlightGrey[] = 'C9';
                break;
            case 'invoice':
                $cellsToHighlightGrey[] = 'D9';
                break;
        }


        // highlight the correct resale condition type
        switch($parameters->resale_condition_id) {
            case 1:
                // trade in excellent
                $cellsToHighlightGrey[] = 'B10';
                break;
            case 2:
                // trade in good
                $cellsToHighlightGrey[] = 'C10';
                break;
            case 3:
                // trade in fair
                $cellsToHighlightGrey[] = 'D10';
                break;
            case 4:
                // wholesale
                $cellsToHighlightGrey[] = 'E10';
                break;
            case 5:
                // retail
                $cellsToHighlightGrey[] = 'F10';
                break;
            case 6:
                // pp excellent
                $cellsToHighlightGrey[] = 'G10';
                break;
            case 7:
                // pp good
                $cellsToHighlightGrey[] = 'H10';
                break;
            case 8:
                // pp fair
                $cellsToHighlightGrey[] = 'I10';
                break;
        }

        // retention
        $cellsToHighlightGrey[] = (chr(ord('A') + $parameters->retention - 1 )) . '11';

        /**
         * business use percent
         * we need to display 5 values
         * bup - 2*5, bup - 5, bup, bup + 5, bup + 2*5
         */
        $worksheet->getCell('B12')->setValue($parameters->business_use_percent - 10);
        $worksheet->getCell('C12')->setValue($parameters->business_use_percent - 5);
        $worksheet->getCell('D12')->setValue($parameters->business_use_percent);
        $worksheet->getCell('E12')->setValue($parameters->business_use_percent + 5);
        $worksheet->getCell('F12')->setValue($parameters->business_use_percent + 10);

        $cellsToHighlightGrey[] = 'D12';

        // determine and highlight business use percent

        // insert the finance rate
        $finance_rate = InterestRate::where('year', $parameters->vehicle_year)
            ->where('country_id', $parameters->country_id)->first()->interest_rate;
        $worksheet->getCell('B13')->setValue($finance_rate);


        /** DRIVER LOCALE */
        // city
        $worksheet->getCell('K6')->setValue($parameters->city_name);
        // state/province
        $state_province_label = $parameters->country_id == 1 ? 'State' : 'Province';
        $worksheet->getCell('J7')->setValue($state_province_label);
        $worksheet->getCell('K7')->setValue($parameters->stateProvince->name);
        // zip/postal
        $zip_postal_label = $parameters->country_id == 1 ? 'ZIP Code' : 'Postal Code';
        $worksheet->getCell('J8')->setValue($zip_postal_label);
        $worksheet->getCell('K8')->setValue($parameters->zip_postal_code);

        //territory label
        $territory_label = TerritoryType::find($parameters->territory_type_id)->name;
        $worksheet->getCell('K9')->setValue($territory_label);

        //territory type
        $territory_type_label = '';
        switch($parameters->territory_type_id) {
            case 1:
                $territory_type_label = 'City';
                break;
            case 2:
                $territory_type_label = $parameters->country_id == 1 ? 'State' : 'Province';
                break;
            case 3:
                $territory_type_label = 'Cities';
                break;
            case 4:
                $territory_type_label = $parameters->country_id == 1 ? 'States' : 'Provinces';
                break;
        }
        $worksheet->getCell('J10')->setValue($territory_type_label);
        $worksheet->getCell('K10')->setValue($results[1]->variableReimbursementData->variableReimbursementDetails->fuelAverage->geo_list);

        //fuel_month
        $fuel_month = Month::find($parameters->fuel_month_id)->name . ', ' . $parameters->fuel_year;
        $worksheet->getCell('K11')->setValue($fuel_month);


        // highlight the grey cells and bold the font
        foreach($cellsToHighlightGrey as $cell) {
            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'fill' => [
                    'fillType' => PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => [
                        'argb' => 'FFD3D3D3'
                    ]
                ]
            ];
            $spreadsheet->getActiveSheet()->getStyle($cell)->applyFromArray($styleArray);
        }

        /**
         * Loop through the resulting reimbursements for each mileage band
         */
        foreach($results as $mileage_band_id => $result) {
            // determine the column we are inserting into based upon the mileage band id
            //  ex. if mileage_band_id is 1, we insert into column B
            $column = (chr(ord('A') + $mileage_band_id ));

            // copy the reference to save typing
            $fixed = $result->fixedReimbursementData->fixedReimbursementDetails;
            $variable = $result->variableReimbursementData->variableReimbursementDetails;

            // trade in mileage
            $trade_in_mileage = $this->formattedTradeInMileage($mileage_band_id, $parameters->retention, true);
            $worksheet->getCell($column . '16')->setValue($trade_in_mileage);


            /** FIXED */
            // capital cost
            $worksheet->getCell($column . '18')->setValue($fixed->capitalCost->amount_adj);

            // resale value
            $worksheet->getCell($column . '19')->setValue($fixed->resaleValue->amount_adj);

            // depreciation
            $worksheet->getCell($column . '20')->setValue($fixed->depreciation->amount_adj);

            // tax
            // tax_label
            $worksheet->getCell( 'A21')->setValue($fixed->tax->title);
            // tax amount
            $worksheet->getCell($column . '21')->setValue($fixed->tax->amount_adj);

            // finance cost
            $worksheet->getCell($column . '22')->setValue(
                isset($fixed->financeCost) ? $fixed->financeCost->amount_adj : number_format(0)
            );

            // insurance
            $worksheet->getCell($column . '23')->setValue($fixed->insurance->amount_adj);

            // fee/renewal
            $worksheet->getCell($column . '24')->setValue($fixed->feeRenewal->amount_adj);

            // total before business use percent
            $worksheet->getCell($column . '25')->setValue(
                $fixed->fixedReimbursement->amount
            );

            // total after business use percent
            $worksheet->getCell($column . '26')->setValue($result->fixedReimbursementData->fixed_reimbursement);


            /** VARIABLE */
            // fuel economy
            $worksheet->getCell($column . '28')->setValue($variable->fuelEconomy->amount_adj);

            // fuel average
            $worksheet->getCell($column . '29')->setValue($variable->fuelAverage->amount_adj);

            // fuel price NOTE there is no adjustment here
            $worksheet->getCell($column . '30')->setValue($variable->fuelPrice->amount);

            // maintenance
            $worksheet->getCell($column . '31')->setValue($variable->maintenance->amount_adj);

            // repairs
            $worksheet->getCell($column . '32')->setValue($variable->repair->amount_adj);

            // cents per mile
            $worksheet->getCell($column . '33')->setValue($variable->centsPerMile->amount_adj);

        }


        // highlight the selected mileage band green 58fa58
        $column = (chr(ord('A') + $selected_mileage_band_id ));
        $cellsToHighlightGreen = [
            $column . '15:' . $column . '16',
            $column . '20:' . $column . '25',
            $column . '30:' . $column . '32'
        ];

        foreach($cellsToHighlightGreen as $index => $cell) {
            $styleArray = [
                'font' => [
                    'bold' => $index == 0 ? true : false,
                ],
                'fill' => [
                    'fillType' => PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => [
                        'argb' => 'FF58FA58'
                    ]
                ]
            ];
            $spreadsheet->getActiveSheet()->getStyle($cell)->applyFromArray($styleArray);
        }

        $tmp_dir = storage_path() . '/app/public/tmp/';
        $file_name = 'mileage_band_rate_sheet_' . date("Y-m-d") . '.xls';

        $writer = PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save($tmp_dir . $file_name);

        return ['file_name' => $file_name];
    }
}
