<?php /** @noinspection ALL */

namespace App\Http\Controllers;

use App\AdministratorProfile;
use App\CompanyAdministration;
use App\CompanyDates;
use App\CompanyInvoicing;
use App\CompanyPayments;
use App\CompanyVehicle;
use App\DailyTrip;
use App\DriverInsurance;
use App\DriverInsuranceReview;
use App\Notification;
use App\TerritoryType;
use Carbon\Carbon;
use App\CompanyBank;
use App\DivisionStop;
use App\SavedStop;
use App\DriverStop;
use App\StateProvince;
use App\SuperProfile;
use App\CompanyIndustry;
use App\Vehicle;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;

use App\Bank;
use App\Company;
use App\CompanyContact;
use App\CompanyFee;
use App\CompanyInsurance;
use App\CompanyLicense;
use App\CompanyMileage;
use App\CompanyOptions;
use App\CompanyProfile;
use App\CompanyCoordinators;
use App\CompanyPaymentDate;
use App\CompanyTaxes;

use App\CompanyContract;
use App\CompanyReportLabel;

use App\Address;
use App\Country;
use App\Division;
use App\FuelCity;
use App\User;
use App\DriverProfile;
use App\ManagerProfile;
use App\VehicleProfile;
use App\VehicleProfileMapping;
use App\VehicleProfileApproval;
use App\Note;
use App\NoteCategory;
use App\NotificationTemplate;
use App\MileageBand;
use App\PostalCode;
use App\ReimbursementDetail;
use App\ZipCode;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator as Validator;
use Illuminate\Validation\Rule as Rule;

use App\Http\Traits\UtilityTraits;
use App\Http\Traits\DriverTraits;
use App\Http\Traits\ValidationTraits;
use PhpOffice\PhpSpreadsheet;

class CompanyController extends Controller
{

    use UtilityTraits;
    use DriverTraits;
    use ValidationTraits;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store the new company and create default entries in all associated company_... tables
     * Request should contain only the name, and whether or not the company is a demo company
     * We exclude CompanContacts because each contact has 1 record. There is no company-wide
     * defaults.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO: add regex to backend validation
        try {
            $this->validateCreateCompany($request->all());
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        $company = (array)$request->all();

        $company['is_demo_company'] = $company['is_demo_company'] == true ? 1 : 0;

        $company_id = Company::create($company)->id;

        //Create entry in company_administrations table
        CompanyAdministration::create(['company_id' => $company_id]);

        //Create entry in company_contacts table
        CompanyContact::create(['company_id' => $company_id]);

        //Create entry in company_coordinators table
        CompanyCoordinators::create(['company_id' => $company_id]);

        //Create entry in company_dates table
        CompanyDates::create(['company_id' => $company_id]);

        //Create entry in company_fees table
        CompanyFee::create(['company_id' => $company_id]);

        //Create entry in company_insurances table
        CompanyInsurance::create(['company_id' => $company_id]);

        //Create entry in company_invoicing table
        CompanyInvoicing::create(['company_id' => $company_id]);

        //Create entry in company_licenses table
        CompanyLicense::create(['company_id' => $company_id]);

        //Create entry in company_mileages table
        CompanyMileage::create(['company_id' => $company_id]);

        //Create entry in company_options table
        CompanyOptions::create(['company_id' => $company_id]);

        //Create entry in company_payments table
        CompanyPayments::create(['company_id' => $company_id]);

        //Create entry in company_profiles table
        CompanyProfile::create(['company_id' => $company_id,
            'street' => '',
            'city' => '',
            'zip_postal' => '',
            'country_id' => $request->country_id,
        ]);

        CompanyTaxes::create(['company_id' => $company_id]);

        return response()->json(['company_id' => $company_id, 'company' => $company]);
    }

    protected function reformatCompanyByModules($c)
    {
        $company['name'] = $c->name;
        $company['is_demo_company'] = $c->is_demo_company;

        $company_profile['website'] = $c->website;
        $company_profile['industry_id'] = $c->industry;
        $company_profile['time_zone_id'] = $c->time_zone;
        $company_profile['street'] = $c->street;
        $company_profile['street2'] = $c->street2;
        $company_profile['city'] = $c->city;
        $company_profile['state_province_id'] = $c->state_province_id;

        $company_options['use_company_financials_module'] = $c->use_company_financials_module;
        $company_options['use_company_mileage_module'] = $c->use_company_mileage_module;
        $company_options['use_company_insurance_module'] = $c->use_company_insurance_module;
        $company_options['use_company_license_module'] = $c->use_company_license_module;
        $company_options['launch_date'] = $c->launch_date;
        $company_options['driver_passwords_date'] = $c->driver_passwords_date;
        $company_options['service_plan'] = $c->service_plan;
        $company_options['account_management_responsibility'] = $c->account_management_responsibility;

        $company['profile'] = $company_profile;
        $company['options'] = $company_options;

        return $company;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //request should contain a company object
        //pass the individual sub objects to the specific update functions listed below


    }

    public function updateCompanyProfile($company_id, Request $request)
    {

        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $profileData = (array)$request->profile;
        $coordinatorsData = (array)$request->coordinators;

        $data = $profileData + $coordinatorsData;

        $profileValidator = Validator::make($data, [
//            'website' => 'required',
            'street' => 'required',
            'city' => 'required',
            'zip_postal' => 'required',
            'state_province_id' => 'min:1',
            'country_id' => 'min:1',
            'cardata_coordinator_id' => 'min:1',
            'cardata_coordinator_phone' => 'required',
        ]);

        $profileValidator->validate();

        if ($profileData['country_id'] == 2) {
            $profileData['zip_postal'] = strtoupper(implode('', explode(' ', $profileData['zip_postal'])));
        }

        CompanyProfile::where('company_id', $company_id)
            ->delete();

        CompanyCoordinators::where('company_id', $company_id)
            ->delete();

        CompanyProfile::create($profileData);
        CompanyCoordinators::create($coordinatorsData);

        return;
    }

    public function updateCompanyOptions($company_id, Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $optionsData = (array)$request->options;
        \Debugbar::info($optionsData);
        $optionsValidator = Validator::make($optionsData, [
            'service_plan' => 'required',
            'launch_date' => 'required',
            'driver_passwords_date' => 'required'
        ]);

        $optionsValidator->validate();

        CompanyOptions::where('company_id', $company_id)
            ->delete();

        CompanyOptions::create($optionsData);
        return null;
    }

    /**
     * @param $company_id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function updateCompanyModuleOptions($company_id, Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $options = CompanyOptions::where('company_id', $company_id)
            ->replicate();

        CompanyOptions::where('company_id', $company_id)
            ->delete();

        $options->use_company_insurance_module = $request->options['use_company_insurance_module'];
        $options->use_company_invoicing_module = $request->options['use_company_invoicing_module'];
        $options->use_company_license_module = $request->options['use_company_license_module'];
        $options->use_company_mileage_module = $request->options['use_company_mileage_module'];
        $options->use_company_payments_module = $request->options['use_company_payments_module'];
        $options->use_company_vehicles_module = $request->options['use_company_vehicles_module'];

        $options->save();
        return null;
    }

    /**
     * Request contains all obj properties of a fee
     * @param Request $request
     */
    public function deleteCompanyFee(Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $today = date('Y-m-d');

        $feeToDelete = CompanyFee::find($request->id);

        // validate the deletion
        $fee_in_the_future = $feeToDelete->from_date > $today;

        if (!$fee_in_the_future) {
            return response(['invalid_fee_action' => ['Cannot delete fee unless it is in the future.']], 400);
        }

        $deleted_fee_from_date = $feeToDelete->from_date;
        $feeToDelete->forceDelete();

        /**
         * if there is an existing fee which is after the current fee we deleted, grab it. we will set its
         * from_date to the from_date of the deleted fee. If no other future fees exist, we set the to_date to
         * null on the fee directly preceeding the fee we deleted
         *
         * We get the next fee by grabbing the first fee where the from date is greater than the fee we just deleted
         * (this is a requirement) and then sorting the from dates in ascending order, so lowest from_date comes first
         */
        $existingFeeStartingAfterDeletedFee = CompanyFee::where('company_id', $request->company_id)
            ->where('from_date', '>', $deleted_fee_from_date)
            ->where('fee', 'like', $request->fee)
            ->orderBy('from_date', 'asc')
            ->first();

        if (empty($existingFeeStartingAfterDeletedFee)) {
            // we deleted the fee farthest in the future, so get the previous fee and set its to_date to null
            $feeBeforeDeletedFee = CompanyFee::where('company_id', $request->company_id)
                ->where('fee', $request->fee)
                ->orderBy('from_date', 'desc')
                ->first();
            if (!empty($feeBeforeDeletedFee)) {
                $feeBeforeDeletedFee->to_date = null;
                $feeBeforeDeletedFee->save();
            }
        } else {
            $existingFeeStartingAfterDeletedFee->from_date = $deleted_fee_from_date;
            $existingFeeStartingAfterDeletedFee->save();
        }

        return ['success' => 'Fee successfully deleted'];

    }

    public function getAllCompanyFees($company_id)
    {
        $company = Company::find($company_id);

        return [
            'currentFees' => $company->currentFees(),
            'allFees' => $company->allFees()
        ];
    }

    public function updateCompanyInsurance($company_id, Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $insuranceData = (array)$request->insurance;
        $coordinatorsData = (array)$request->coordinators;

        $data = $insuranceData + $coordinatorsData;

        $insuranceValidator = Validator::make($data, [
            'insurance_responsibility' => 'required|alpha',
            'insurance_fax_number' => 'required',
            'initial_insurance_grace_end_date' => 'required',
            'new_driver_insurance_grace_days' => 'required|min:0',
            'bodily_liability_per_person' => 'required|min:0',
            'bodily_liability_per_accident' => 'required|min:0',
            'property_damage' => 'required|min:0',
            'deductible' => 'required|min:0',
            'comprehensive_deductible' => 'required|min:0',
            'collision_deductible' => 'required|min:0',
            'medical' => 'required|min:0',
            'uninsured_per_person' => 'required|min:0',
            'uninsured_per_accident' => 'required|min:0',
            'public_liability' => 'required|min:0',
            'insurance_coordinator_id' => 'required|min:0',
            //'insurance_coordinator_phone' => 'min:0',
        ]);

        $insuranceValidator->validate();

        CompanyInsurance::where('company_id', $company_id)
            ->delete();


        /**
         * We are able to delete the coordinators record and create a new one because,
         * we are passing a full coordinators object in the request.
         */
        CompanyCoordinators::where('company_id', $company_id)
            ->delete();

        CompanyInsurance::create($insuranceData);
        CompanyCoordinators::create($coordinatorsData);

    }

    public function updateCompanyLicense($company_id, Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $licenseData = (array)$request->license;
        $coordinatorsData = (array)$request->coordinators;

        $data = $licenseData + $coordinatorsData;

        //TODO: DISCUSS VALIDATION REQUIREMENTS
        $licenseValidator = Validator::make($data, [
            'license_responsibility' => 'required|alpha',
            'license_fax_number' => 'required',
            'initial_license_enforcement_date' => 'required',
            'new_driver_license_grace_days' => 'min:0',
            'license_review_day' => 'min:0',
            'license_review_month' => 'min:-1',
            'license_coordinator_id' => 'min:0',

            //'license_coordinator_phone' => 'min:0',
        ]);

        $licenseValidator->validate();

        CompanyLicense::where('company_id', $company_id)
            ->delete();

        CompanyCoordinators::where('company_id', $company_id)
            ->delete();

        $licenseData['license_review_month'] = $licenseData['license_review_month'] == -1 ? 0 : $licenseData['license_review_month'];

        CompanyLicense::create($licenseData);
        CompanyCoordinators::create($coordinatorsData);

    }


    /**
     * @param $company_id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function updateCompanyMileage($company_id, Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $mileageData = (array)$request->mileage;
        $coordinatorsData = (array)$request->coordinators;

        $data = $mileageData + $coordinatorsData;

        //TODO: DISCUSS VALIDATION REQUIREMENTS
        $mileageValidator = Validator::make($data, [
            'mileage_entry_method' => 'required',
            'mileage_band_audit_method' => 'required',
            'mileage_lock_driver_day' => 'min:0',
            'mileage_lock_all_day' => 'min:0',
            'mileage_approval_end_day' => 'min:0',
            'mileage_reminder_day' => 'min:0',
            'second_mileage_reminder_day' => 'min:0',
            'new_driver_mileage_grace_days' => 'min:0'
        ]);

        $mileageValidator->validate();

        CompanyMileage::where('company_id', $company_id)
            ->delete();

        CompanyCoordinators::where('company_id', $company_id)
            ->delete();

        CompanyMileage::create($mileageData);
        CompanyCoordinators::create($coordinatorsData);
    }

    public function updateCompanyContacts($company_id, Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $companyContacts = $request->contacts;

        foreach ($companyContacts as $contact) {

            $validationRules = [
                'first_name' => 'required',
                'last_name' => 'required',
                'title' => 'required',
                'email' => 'required',
                'type' => 'required',
            ];

            if (empty($contact['phone_number'])) {
                $validationRules['mobile_number'] = 'required';
            } else if (empty($contact['mobile_number'])) {
                $validationRules['phone_number'] = 'required';
            }

            $contactValidator = Validator::make($contact, $validationRules);

            $contactValidator->validate();
        }

        CompanyContact::where('company_id', $company_id)
            ->delete();

        $contacts = [];
        foreach ($companyContacts as $companyContact) {
            $contacts[] = CompanyContact::create($companyContact);
        }

        return ['contacts' => $contacts];
    }


    /**
     * Function to update the company_taxes table
     */
    public function updateCompanyTaxes($company_id, Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $taxData = (array)$request->taxes;

        if ($taxData['reports_display_monthly_tax_limit'] && $taxData['use_quarterly_tax_adjustment']) {
            return response(
                json_encode(['reports_display_monthly_tax_limit' => [
                    'Only one checkbox can be selected at any time'
                ]], 400)
            );
        }

        CompanyTaxes::where('company_id', $company_id)
            ->delete();

        CompanyTaxes::create($taxData);
    }


    /**
     * Function called to inactivate a company
     *
     * It will go through all drivers, managers and admins, replicate the profiles, and
     * set active = 0
     *
     *
     * @param Request $request
     */
    public function inactivateCompany(Request $request)
    {
        //TODO: Check to ensure user is super with appropriate permissions


        //Soft delete all company records

        //Do not change this order, deactivate the company
        Company::where('id', $request->company_id)->update([
            'active' => 0
        ]);

        // gather all active drivers, managers and admins
        $profiles = [];
        $profiles['drivers'] = DriverProfile::where('company_id', $request->company_id)
            ->where('active', 1)->get();
        $profiles['managers'] = ManagerProfile::where('company_id', $request->company_id)
            ->where('active', 1)->get();
        $profiles['administrators'] = AdministratorProfile::where('company_id', $request->company_id)
            ->where('active', 1)->get();

        foreach ($profiles as $profile_type) {
            foreach ($profile_type as $profile) {
                $newProfile = $profile->replicate();
                $profile->delete();
                $newProfile->active = 0;
                $newProfile->save();
            }
        }

    }


    /**
     * Function called to reactivate a company
     *
     * It will go through all drivers, managers and admins, replicate the profiles, and
     * set active = 1 where time of inactivation <= to the time the inactive profile was made
     *
     *
     * @param Request $request
     */
    public function reactivateCompany($company_id)
    {
        $company = Company::where('id', $company_id)
            ->where('deleted_at', null)
            ->first();

        $time_of_inactivation = $company->updated_at;

        $company->active = 1;
        $company->save();

        // gather all active drivers, managers and admins
        // NOTE we look at created_at to grab only the records affected by the company inactivation.
        $profiles = [];
        $profiles['drivers'] = DriverProfile::where('company_id', $company_id)
            ->where('active', 0)
            ->where('created_at', '>=', $time_of_inactivation)
            ->get();
        $profiles['managers'] = ManagerProfile::where('company_id', $company_id)
            ->where('active', 0)
            ->where('created_at', '>=', $time_of_inactivation)
            ->get();
        $profiles['administrators'] = AdministratorProfile::where('company_id', $company_id)
            ->where('active', 0)
            ->where('created_at', '>=', $time_of_inactivation)
            ->get();

        foreach ($profiles as $profile_type) {
            foreach ($profile_type as $profile) {
                $newProfile = $profile->replicate();
                $profile->delete();
                $newProfile->active = 1;
                $newProfile->save();
            }
        }


    }


    /**
     * Get a list of potential coordinators for the dropdown select
     * @param Request $request
     *      $request must contain:
     *          responsibility = 'CarData' or 'Client'
     *          company_id (this is required if responsibility is client)
     * @return JsonResponse
     */
    public function getPotentialCoordinators(Request $request)
    {

        $coordinators = new \stdClass;

        if ($request->responsibility == 'CarData') {

            $coordinators->administrators = \DB::table('users')
                ->join('administrator_profiles', 'administrator_profiles.user_id', 'users.id')
                ->select('users.id', 'users.first_name', 'users.last_name')
                ->whereNull('users.deleted_at')
                ->whereNull('administrator_profiles.deleted_at')
                ->where('administrator_profiles.company_id', 41)
                ->where('administrator_profiles.is_limited', 0)
                ->orderBy('users.last_name')
                ->get();


            $coordinators->supers = \DB::table('users')
                ->join('super_profiles', 'super_profiles.user_id', 'users.id')
                ->select('users.id', 'users.first_name', 'users.last_name')
                ->whereNull('users.deleted_at')
                ->whereNull('super_profiles.deleted_at')
//                ->where('super_profiles.company_id', 41)
                ->orderBy('users.last_name')
                ->get();

        } elseif ($request->responsibility == 'Client') {

            $coordinators->administrators = \DB::table('users')
                ->join('administrator_profiles', 'administrator_profiles.user_id', 'users.id')
                ->select('users.id', 'users.first_name', 'users.last_name')
                ->whereNull('users.deleted_at')
                ->whereNull('administrator_profiles.deleted_at')
                ->where('administrator_profiles.company_id', $request->company_id)
                ->where('administrator_profiles.is_limited', 0)
                ->orderBy('users.last_name')
                ->get();

            $coordinators->supers = [];
        }

        return response()->json(['coordinators' => $coordinators]);
    }


    /**
     * Returns the CarData coordinator for the given company as a User object.
     *
     * @param $company_id
     * @return array
     */
    public function getCompanyCarDataCoordinator($company_id)
    {
        return [
            'coordinator' => User::find(Company::find($company_id)->coordinators->cardata_coordinator_id)
        ];

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return companies by level
     * 1 - just id and name
     * 2 - all company values
     *
     * @return JsonResponse
     */
    public function getCompanies($level = 1, $session_format = false, $from_session = false)
    {
        if ($session_format) {
            $companies = \DB::select(\DB::raw(<<<EOQ
SELECT
  c.id,
  c.name,
  c.active,
  cp.country_id,
  cf.payment_responsibility,
  ct.use_quarterly_tax_adjustment,
  co.service_plan,
  CASE WHEN (SELECT count(*)
             FROM saved_stops ss
             WHERE ss.company_id = c.id) > 0
    THEN TRUE
  ELSE FALSE END AS `company_has_predefined_stops`
FROM companies c
  JOIN company_payments cf ON cf.company_id = c.id
  JOIN company_profiles cp ON cp.company_id = c.id
  JOIN company_mileages cm ON cm.company_id = c.id
  JOIN company_options co ON co.company_id = c.id
  JOIN company_taxes ct ON ct.company_id = c.id
WHERE cf.deleted_at is NULL AND cp.deleted_at IS NULL AND co.deleted_at IS NULL
ORDER BY c.name
EOQ
            ));

        } else {
            $companies = Company::orderBy('name')->get();
        }

        $active_company_count = Company::where('active', 1)->get()->count();
        $inactive_company_count = Company::where('active', 0)->get()->count();

        return response()->json([
            'companies' => $companies,
            'active_company_count' => $active_company_count,
            'inactive_company_count' => $inactive_company_count
        ]);
    }

    /**
     * Return companies where payment responsibility is CarData's
     *  (used on ManualAchEftFilesPage.vue)
     *
     * @return JsonResponse
     */
    public function getCompaniesWherePaymentResponsibilityCarData($country_id = null)
    {
        if ($country_id == 1 || $country_id == 2) {
            $countries_list = [$country_id];
        } else {
            $countries_list = [1, 2];
        }


        $companies = \DB::table('companies')
            ->join('company_payments', function ($join) {
                $join->on('companies.id', '=', 'company_payments.company_id')
                    ->where('company_payments.payment_responsibility', '=', 'CarData');
            })
            ->join('company_profiles', 'companies.id', '=', 'company_profiles.company_id')
            ->select('companies.id', 'companies.name', 'company_profiles.country_id', 'companies.deleted_at')
            ->distinct('companies.id')
            ->whereIn('company_profiles.country_id', $countries_list)
            ->orderBy('companies.name')
            ->orderBy('companies.deleted_at', 'desc')
            ->get();

        $active_company_count = $companies->where('deleted_at', null)->count();
        $inactive_company_count = $companies->count() - $active_company_count;

        return response()->json([
            'companies' => $companies,
            'active_company_count' => $active_company_count,
            'inactive_company_count' => $inactive_company_count
        ]);
    }

    /**
     * Return company from id
     *
     * @param null $id
     * @return array
     */
    public function getCompany($id = -1)
    {
        // no id specified and no company id in session
        if ($id < 0) {
            return ['company' => (object)['name' => '', 'id' => -1]];
        }

        $company = Company::withTrashed()->where('id', $id)
            ->with('profile')
            ->with('options')
            ->with('mileage')
            ->with('license')
            ->with('insurance')
            ->with('invoicing')
            ->with('profile')
            ->with('contacts')
            ->with('coordinators')
            ->with('taxes')
            ->with('vehicles')
            ->with('payments')
            ->with('dates')
            ->with('administration')
            ->first();

        /** dummy up a new  processingBank, maybe we overwrite it, at least the front end will have the default */
        $company->payments->processingBank = new \stdClass();
        $company->payments->processingBank->id = -1;
        $company->dates = $company->dates();

        if ($company->options->use_company_financials_module) {
            if ($company->payments->payment_responsibility == 'CarData') {
                $company->payments->processingBank = $company->bank(false);
            }
            $company->payments->currentFees = $company->currentFees();
            $company->payments->allFees = $company->allFees();
        }

        return [
            'company' => $company,
            'driverCount' => $company->driverCount(),
            'managerCount' => $company->managerCount(),
            'administratorCount' => $company->administratorCount()
        ];
    }

    /**
     * Get division from id. Will reset id in session
     *
     * @param null $id
     * @return JsonResponse
     */
    public function getDivision($id = null)
    {
        $division = Division::find($id);

        return response()->json([
            'division' => $division
        ]);
    }

    /**
     * Level 1 is limited values (id and name), otherwise all values.
     * We assume that this is only called once a company is in session.
     *
     * @param int $company_id
     * @return array
     */
    public function getDivisions($company_id = -1, $from_session = false)
    {
        $divisions = [];

        if ($from_session && session()->has('impersonator_id') && session()->get('active_role') == 'driver') {
            $user = User::find(session()->get('impersonator_id'));
            $role = session()->get('impersonator_role');
        } else {
            $user = User::find(session()->get('viewed_user_id'));
            $role = session()->get('active_role');
        }

        if ($company_id > 0) {
            if ($role == 'super') {
                $divisions = Division::where('company_id', $company_id)->get();
            } else if ($role == 'administrator') {
                $divisions = AdministratorProfile::where('user_id', $user->id)->first()->divisions();
            } else if ($role == 'manager') {
                $divisions = ManagerProfile::where('user_id', $user->id)->first()->divisions();
            }
        }
        return ['divisions' => $divisions];
    }

    public function saveDivision(Request $request)
    {
        $division = Division::create($request->all());
        return ['message' => 'Division with id ' . $division->id . ' created.', 'session_division_id' => $division->id];
    }

    public function updateDivision($division_id, $division_name)
    {
        // selected division is in session
        $division = Division::find($division_id);
        $division->name = $division_name;
        $division->save();
        return response()->json(['message' => 'division ' . $division_id . ' updated']);
    }

    /**
     * There is no direct link from manager to division, so we need to get divisions from all drivers assigned to this
     * manager.
     *
     * @param $manager_id
     * @return array
     */
    protected function getManagerDivisions($manager_id)
    {
        $divisions = [];
        $manager = ManagerProfile::where('manager_id', $manager_id)->first();

        $include_inactive_drivers = empty(session()->get('include_inactive_drivers')) ? 0 : session()->get('include_inactive_drivers');

        $drivers = $manager->drivers($include_inactive_drivers);

        if (!empty($drivers)) {
            foreach ($drivers as $driver) {
                if (!in_array($driver->driverProfile->division, $divisions)) {
                    $divisions[] = $driver->driverProfile->division;
                }
            }
        } else {
            $divisions = Division::where('company_id', $manager->company_id)->get();
        }

        return $divisions;
    }

    public function getCompanyName($company_id)
    {
        if ($company_id > 0) {
            return response()->json(['company_name' => Company::find($company_id)->name]);
        }
    }

    /**
     * Get the country associated with the CompanyProfile
     *
     * @param $company_id
     * @return JsonResponse
     */
    public function getCompanyCountry($company_id)
    {
        // test against -1, sometimes this gets called before the company has been rendered
        if ($company_id > 0) {

            $country_id = Company::find($company_id)->profile->country_id;
            return response()->json(['country_id' => $country_id]);
        }
    }


    /**
     * Returns vehicle profiles for a given company
     *
     * @param $company_id
     * @return array of vehicle profiles
     *
     */
    public function getVehicleProfiles($company_id, $include_inactive = true, $include_proposed = false)
    {
        $active_count = 0;
        $inactive_count = 0;

        if ($include_inactive) {
            $vehicleProfiles = VehicleProfile::where('company_id', $company_id)
                ->whereIn('proposed', $include_proposed ? [0, 1] : [0])
                ->withTrashed()
                ->get();
        } else {
            $vehicleProfiles = VehicleProfile::where('company_id', $company_id)
                ->whereIn('proposed', $include_proposed ? [0, 1] : [0])
                ->get();
        }
        foreach ($vehicleProfiles as $vp) {
            $vp->vehicle;
            $vp->vehicle->category;
            $vp->note;
            $vp->approval;

            $vp->variableAdjustments = $vp->variableAdjustments()->get();
            if (!empty($vp->deleted_at)) {
                $inactive_count++;
            } else {
                $active_count++;
            }
        }

        return ['vehicleProfiles' => $vehicleProfiles, 'inactive_count' => $inactive_count, 'active_count' => $active_count];
    }

    public function getProposedVehicleProfiles($company_id)
    {
        $vehicleProfiles = VehicleProfile::where('company_id', $company_id)
            ->where('proposed', 1)
            ->get();

        $active_count = 0;
        foreach ($vehicleProfiles as $vp) {
            // load associated relationships
            $vp->vehicle;
            $vp->vehicle->category;
            $vp->note;
            $vp->approval;
            $vp->variableAdjustments = $vp->variableAdjustments()->get();
            $active_count++;
        }

        return ['vehicleProfiles' => $vehicleProfiles, 'inactive_count' => 0, 'active_count' => $active_count];

    }

    public function getVehicleProfilesAwaitingApproval($company_id)
    {
        \Log::info('getVehicleProfilesAwaitingApproval', ['company_id' => $company_id]);
        $vehicle_profile_ids = VehicleProfileApproval::where('approved', null)
            ->pluck('vehicle_profile_id')
            ->all();

        $vehicleProfiles = VehicleProfile::whereIn('id', $vehicle_profile_ids)
            ->where('company_id', $company_id)
            ->get();

        $active_count = 0;
        foreach ($vehicleProfiles as $vp) {
            // load associated relationships
            $vp->vehicle;
            $vp->vehicle->category;
            $vp->note;
            $vp->approval;
            $vp->variableAdjustments = $vp->variableAdjustments()->get();
            $active_count++;
        }

        return ['vehicleProfiles' => $vehicleProfiles, 'inactive_count' => 0, 'active_count' => $active_count];
    }

    public function getCompaniesWithVehicleProfilesAwaitingApproval()
    {
        $vehicle_profile_ids = VehicleProfileApproval::whereNull('approved')->pluck('vehicle_profile_id')->all();
        $company_ids = VehicleProfile::whereIn('id', $vehicle_profile_ids)->pluck('company_id')->all();

        return ['companies' => Company::whereIn('id', $company_ids)->get()];
    }

    public function getVehicleProfileMappings($company_id)
    {
        $mappings = \DB::table('vehicle_profile_mappings')
            ->select()
            ->where('company_id', $company_id)
            ->whereNull('applied_by')
            ->get();

        foreach ($mappings as $mapping) {
            $user = User::find($mapping->created_by);
            $mapping->creator = $user->first_name . ' ' . $user->last_name;
        }

        return ['mappings' => $mappings];
    }

    /**
     * Save proposed vehicle profile mappings for the Rate Comparison Tool.
     *
     * TODO: this could use a cleanup bigtime buds
     *
     * @param Request $request
     *      request must contain:
     *          - company_id
     *          - mappings (an array or obj of mapping objects see VehicleProfileMapping.php)
     */
    public function saveProposedVehicleProfileMappings(Request $request)
    {

        $user_id = Auth::user()->id;


        // select all mappings awaiting approval or approved
        $existingMappings = \DB::table('vehicle_profile_mappings')
            ->select()
            ->where('company_id', $request->company_id)
            ->whereRaw('(approved = 1 or approved is null)')
            ->get();

        // convert existing mappings to an associative array for use with eloquent create() method
        $existingMappings = json_decode(json_encode($existingMappings), true);

        // delete all unapplied mappings
        \DB::table('vehicle_profile_mappings')
            ->where('company_id', $request->company_id)
            ->whereNull('applied_by')
            ->delete();

        /**
         * If no mappings exist, we save all the mappings passed in the request.
         */
        if (sizeof($existingMappings) == 0) {
            foreach ($request->mappings as $m) {
                if ($m['proposed_vehicle_profile_id'] != -1) {
                    $m['company_id'] = $request->company_id;
                    $m['created_by'] = $user_id;
                    VehicleProfileMapping::create($m);
                }
            }

            /**
             * If mappings already exist, we must:
             *
             *      first create two mapping tables so we can compare by old_vehicle_profile_id then:
             *
             *      compare by old_vehicle_profile_id
             *
             *  A.  for existing mappings that have an old_vehicle_profile_id not present in the new
             *      mappings
             *          insert existing mapping record
             *
             *  B.  for existing mappings that have an old_vehicle_profile_id PRESENT in the new mappings:
             *
             *          if proposed_vehicle_profile_id has changed
             *              insert as approved = null (awaiting approval)
             *
             *          if proposed_vehicle_profile_id has not changed
             *              insert the existing record
             *
             *  C.  for mappings that are entirely new
             *          insert the record as approved = null (awaiting approval
             *
             */
        } else {

            $existingMapTable = [];
            // create the existing map table (allow us to key by old vehicle profile id)
            foreach ($existingMappings as $key => $value) {
                // skip the unmapped records
//                if ($value['proposed_vehicle_profile_id'] == -1) { continue; }

                // create the map table
                $existingMapTable[$value['old_vehicle_profile_id']] = $value;

                // add the company id to each mapping object
                $existingMapTable[$value['old_vehicle_profile_id']]['company_id'] = $request->company_id;

            }

            $newMapTable = [];
            // create the new request map table (allow us to key by old vehicle profile id)
            foreach ($request->mappings as $key => $value) {
                // skip the unmapped records
//                if ($value['proposed_vehicle_profile_id'] == -1) { continue; }

                // create the map table
                $newMapTable[$value['old_vehicle_profile_id']] = $value;

                // add the company id to each mapping object
                $newMapTable[$value['old_vehicle_profile_id']]['company_id'] = $request->company_id;

            }

            foreach ($existingMapTable as $key => $value) {
                // A.
                if (!array_key_exists($key, $newMapTable)) {
                    if (!($value['proposed_vehicle_profile_id'] == -1)) {
                        VehicleProfileMapping::create($value);
                    }


                    // B.
                } else {
                    if ($existingMapTable[$key]['proposed_vehicle_profile_id'] = $newMapTable[$key]['proposed_vehicle_profile_id']) {
//                        VehicleProfileMapping::where('old_vehicle_profile_id', $key)
//                            ->whereNull('applied_by')
//                            ->update($existingMapTable[$key]);
                        if (!($newMapTable[$key]['proposed_vehicle_profile_id'] == -1 || $newMapTable[$key]['proposed_vehicle_profile_id'] == "-1")) {
                            VehicleProfileMapping::create($newMapTable[$key]);
                        }

                    } else {
                        if (!($newMapTable[$key]['proposed_vehicle_profile_id'] == -1 || $newMapTable[$key]['proposed_vehicle_profile_id'] == "-1")) {
                            $newMapTable[$key]['approved'] = null;
                            VehicleProfileMapping::create($newMapTable[$key]);
                        }
                    }
                }
            }

            // C.
            foreach ($newMapTable as $key => $value) {
                if (array_key_exists($key, $existingMapTable)) {
                    continue;
                } else {
                    if (!($value['proposed_vehicle_profile_id'] == -1)) {
                        VehicleProfileMapping::create($value);
                    }
                }
            }
        }

        return ['mappings' => \DB::table('vehicle_profile_mappings')
            ->where('company_id', $request->company_id)
            ->get()];

    }


    public function approveVehicleProfileMappings(Request $request)
    {
        // select all mappings awaiting approval or approved
        \DB::table('vehicle_profile_mappings')
            ->where('company_id', $request->company_id)
            ->whereIn('old_vehicle_profile_id', $request->mappings)
            ->whereNull('applied_by')
            ->update([
                'approved' => 1,
                'approved_by' => Auth::user()->id,
                'note' => null
            ]);

        return null;
    }

    public function rejectVehicleProfileMappings(Request $request)
    {
        foreach ($request->mappings as $mapping) {
            \DB::table('vehicle_profile_mappings')
                ->where('company_id', $request->company_id)
                ->where('old_vehicle_profile_id', $mapping['old_vehicle_profile_id'])
                ->where('proposed_vehicle_profile_id', $mapping['proposed_vehicle_profile_id'])
                ->whereNull('applied_by')
                ->update([
                    'approved' => 0,
                    'approved_by' => Auth::user()->id,
                    'note' => $mapping['note'],
                ]);
        }


        return null;
    }

    public function applyVehicleProfileMappings(Request $request)
    {
        // validate the user todo: who can apply these?
        // VALIDATE MAPPING ]
        try {
            // make sure the mappings are approved
            foreach ($request->mappings as $mapping) {
                if ($mapping['approved'] != 1) {
                    throw new \Exception('All mappings must be approved in order to be applied');
                }
            }
        } catch (\Exception $e) {
            return response(
                $e->getMessage(), 400
            );
        }

        // create a map table so we can quickly find the new vehicle profile id based on the existing one
        // [old_vehicle_profile_id => new_vehicle_profile_id]
        $mapTable = [];
        foreach ($request->mappings as $key => $value) {
            // skip the unmapped records for safety
            if ($value['proposed_vehicle_profile_id'] == -1) {
                continue;
            }
            // create the map table
            $mapTable[$value['old_vehicle_profile_id']] = $value['proposed_vehicle_profile_id'];
        }

        // make all of the proposed vehicle profiles actual vehicle profiles
        VehicleProfile::whereIn('id', array_values($mapTable))
            ->where('proposed', 1)
            ->update([
                'proposed' => 0
            ]);

        // get all driver's for the affected vehicle profiles
        $drivers = DriverProfile::select('user_id', 'vehicle_profile_id')
            ->whereIn('vehicle_profile_id', array_keys($mapTable))
            ->where('active', 1)
            ->get();

        foreach ($drivers as $driver) {
            $this->updateDriverProfile(
                $driver['user_id'],
                [
                    'vehicle_profile_id' => $mapTable[$driver['vehicle_profile_id']]
                ]
            );
        }

        $oldVehicleProfiles = VehicleProfile::whereIn('id', array_keys($mapTable))
            ->get();

        foreach ($oldVehicleProfiles as $vp) {
            $vp->delete();
        }

        return ['message' => 'Mapping applied successfully'];
    }

    /**
     * Function to submit proposed vehicle profile mappings for approval.
     *
     * Mappings will already be saved at this point, so we simply update the 'submitted_for_approval'
     * column.
     *
     * @param Request $request
     * @return array
     */
    public function submitProposedVehicleProfileMappingsForApproval(Request $request)
    {
        VehicleProfileMapping::where('company_id', $request->company_id)
            ->whereIn('old_vehicle_profile_id', $request->mappings)
            ->update([
                'submitted_for_approval' => 1
            ]);

        $company_name = Company::find($request->company_id)->name;

        //find all vehicle profile approvers
        $users_to_notify = SuperProfile::where('is_vehicle_profile_approver', true)->get()->pluck('user_id')->all();

        //get the appropriate notification
        $notification = NotificationTemplate::where('slug', 'new-vehicle-profile-mapping')->first();
        $notification->content .= ' ' . $company_name;
        $notification->visible_to = 'all';


        //send notification to the supers, ensure it is visible only to supers
        $notification->fusionNotify($users_to_notify, 'super');

        return null;
    }

    /**
     * Function to submit proposed vehicle profiles for approval.
     *
     * This function will delete all old approval records for the vehicle profile ids submitted, and
     * create new ones. Effectively this will restart the approval process for each vehicle profile.
     *
     * $request must contain ids (an array of vehicle profile ids)
     *
     * @param Request $request
     */
    public function submitProposedVehicleProfilesForApproval(Request $request)
    {
        //TODO START HERE

        // delete old approvals and update easier for now

        // todo in the future, perhaps we only delete the rejected and awaiting approval records and then resubmit
        // todo leave the approved records as approved?

        foreach ($request->ids as $id) {
            $approval = VehicleProfileApproval::where('vehicle_profile_id', $id)
                ->first();

            if ($approval) {
                $approval->forceDelete();
            }

            VehicleProfileApproval::create(['vehicle_profile_id' => $id]);

        }

        $company_name = Company::find($request->company_id)->name;

        //find all vehicle profile approvers
        $users_to_notify = SuperProfile::where('is_vehicle_profile_approver', true)->get()->pluck('user_id')->all();

        //send notification to the supers, ensure it is visible only to supers
        $notification = NotificationTemplate::where('slug', 'proposed-vehicle-profiles-awaiting-approval')->first();

        $notification->content .= $company_name;
        $notification->visible_to = 'all';

        $notification->fusionNotify($users_to_notify, 'super');

        return null;
    }

    public function getVehicleProfilesForRateComparison($company_id)
    {
        $allVehicleProfiles = VehicleProfile::where('company_id', $company_id)->get();

        foreach ($allVehicleProfiles as $vp) {
            $vehicle = Vehicle::where('id', $vp->vehicle_id)->select('year', 'name')->first();
            $vp->vehicle = $vehicle;
            $vp->variableAdjustments = $vp->variableAdjustments()->get();
            $vp->resaleCondition;
            $vp->approval;
            $vp->note;
            $creator = $vp->creator();
            $vp->creator_name = $creator['first_name'] . ' ' . $creator['last_name'];
        }

        $vehicleProfiles = $allVehicleProfiles->filter(function ($value, $key) {
            // true in driverCount means where drivers have active = 1 and deleted_at null
            return $value->driverCount(true) > 0;
        });

        $proposedVehicleProfiles = $allVehicleProfiles->filter(function ($value, $key) {
            return $value->proposed == 1;
        })->values(); // the values removes the laravel imposed arbitrary keys and the result will be an array on the javascript side

        return ['vehicleProfiles' => $vehicleProfiles, 'proposedVehicleProfiles' => $proposedVehicleProfiles];

    }


    /**
     * Function to add a new vehicle profile. Proposed is defaulted to false.
     * To add a proposed vehicle profile simply ensure $request->vehicleProfile->proposed = 1
     * This function will return the id of the newly created vehicle profile.
     *
     * This function triggers a model event adding a vehicle profile approval record
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addVehicleProfile(Request $request)
    {
        try {
            $this->validateVehicleProfile($request->vehicleProfile);

            $note_passed = isset($request->note) && (sizeof($request->note['title']) > 1 || sizeof($request->note['content']) > 1);

            // if a note was passed in the request
            if ($note_passed) {
                $note = (array)$request->note;
                $note['category_id'] = NoteCategory::where('title', 'Vehicle Profile Note')->first()->id;
                $this->validateNote($note);
            }

        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }


        $vehicleProfile = VehicleProfile::create($request->vehicleProfile);
        $vehicleProfile->created_by = Auth::user()->id;
        $vehicleProfile->save();

        $vehicleProfile->variableAdjustments()->attach($request->variableAdjustmentIds);

        $vehicle_profile_id = $vehicleProfile->id;


        if ($note_passed) {
            Note::create([
                'user_id' => $vehicleProfile->created_by,
                'category_id' => $note['category_id'],
                'vehicle_profile_id' => $vehicle_profile_id,
                'title' => $note['title'],
                'content' => $note['content'],
            ]);
        }

        return response()->json([
            'vehicle_profile_id' => $vehicle_profile_id,
        ]);

    }


    /**
     * Updates a vehicle profile which has no drivers, no need to keep records of these changes, they do NOT
     * have any effects cascading through the system.
     *
     * Returns the id of a successfully updated record
     *
     * @param Request $request
     *      $request consists of a vehicle profile, and an array of variable adjustment ids
     */
    public function updateVehicleProfile(Request $request)
    {
        $vp = VehicleProfile::find($request['vehicleProfile']['id']);

        // we soft delete to avoid postfix collision error in the validator
        $vp->delete();

        try {
            $this->validateVehicleProfile($request->vehicleProfile);
        } catch (\Exception $e) {
            //if we catch the exception, restore the old vehicle profile
            VehicleProfile::withTrashed()->find($request['vehicleProfile']['id'])->restore();
            return response($e->getMessage(), 400);
        }

        $vp = VehicleProfile::withTrashed()->find($request['vehicleProfile']['id']);
        $vp->restore();
        $vp->update($request['vehicleProfile']);
        $vp->variableAdjustments()->sync($request['variableAdjustmentIds']);

        return response()->json([
            'vehicle_profile_id' => $vp->id,
        ]);
    }

    public function inactivateVehicleProfile($vehicle_profile_id)
    {
        $vehicleProfile = VehicleProfile::find($vehicle_profile_id);

        //first soft delete the vehicle profile
        $vehicleProfile->delete();

        //next soft delete all records in vehicle_profile_variable_adjustment
        \DB::table('vehicle_profile_variable_adjustment')
            ->whereRaw('vehicle_profile_id = ?', [$vehicle_profile_id])
            ->where('deleted_at', null)
            ->update([
                'deleted_at' => date('Y-m-d H:i:s'),
            ]);
    }

    public function reactivateVehicleProfile($vehicle_profile_id)
    {
        //get the most recently deleted record
        $vehicleProfile = VehicleProfile::withTrashed()
            ->where('id', $vehicle_profile_id)
            ->orderBy('deleted_at', 'desc')
            ->first();

        //store the time it was soft-deleted
        $deleted_at = $vehicleProfile->deleted_at;

        $vehicleProfile->restore();

        //get the variable adjustments which were deleted at the same time as, or after, the vehicle profile
        $ids = \DB::table('vehicle_profile_variable_adjustment')
            ->whereRaw('vehicle_profile_id = ?', [$vehicle_profile_id])
            ->where('deleted_at', '>=', $deleted_at)
            ->pluck('variable_adjustment_id')
            ->all();

        $vehicleProfile->variableAdjustments()->attach($ids);

        return ['new_vehicle_profile_id' => $vehicleProfile->id];

    }

    /**
     * Returns one vehicle profile with associated vehicle from a given id
     *
     * @param $vehicle_profile_id
     * @param $inactive default is false
     * @return vehicle profile as object
     */
    public function getVehicleProfile($vehicle_profile_id, $inactive = true, $get_drivers = false, $get_creator = false)
    {
        \Log::info('getVehicleProfile', ['vehicle_profile_id', $vehicle_profile_id]);
        \Debugbar::info("Find vehicle id=$vehicle_profile_id inactive=$inactive");

        if ($inactive == true) {
            $vehicleProfile = VehicleProfile::withTrashed()->where('id', $vehicle_profile_id)->with('vehicle')->first();
        } else {
            $vehicleProfile = VehicleProfile::where('id', $vehicle_profile_id)->with('vehicle')->first();
        }

        \Log::info('Vehicle Profile', [$vehicleProfile]);

        \Debugbar::info($vehicleProfile);
        $vehicleProfile->driver_count = $vehicleProfile->driverCount();
        $vehicleProfile->resale_condition = $vehicleProfile->resaleCondition;
        $company_id = $vehicleProfile->company_id;
        $cp = CompanyProfile::where('company_id', $company_id)->first();
        $vehicleProfile->company_country = $cp->country_id;
        $co = CompanyOptions::where('company_id', $company_id)->first();
        $vehicleProfile->company_service_plan = $co->service_plan;
        $vehicleProfile->note;
        $vehicleProfile->approval;

        $vehicleProfile->variableAdjustments = $vehicleProfile->variableAdjustments()->get();

        if ($get_drivers == true) {
            foreach ($vehicleProfile->driverProfiles as $dp) {
                $dp->user;
            };
            $vehicleProfile->driverCount();
        }

        $vehicleProfile->vehicle->category;

        if ($get_creator) {
            $vehicleProfile->creator = $vehicleProfile->creator();
        }

        return ['vehicleProfile' => $vehicleProfile];
    }

    public function deleteProposedVehicleProfile(Request $request)
    {
        $vehicleProfile = VehicleProfile::find($request->vehicle_profile_id);
        if ($vehicleProfile->approval) {
            $vehicleProfile->approval->forceDelete();
        }

        \DB::table('vehicle_profile_mappings')
            ->where('proposed_vehicle_profile_id', $request->vehicle_profile_id)
            ->delete();
        $vehicleProfile->forceDelete();

    }

    /**
     *  Returns the company module options for a given company, the data returned
     *  is restricted for drivers in registration
     *
     * @param $company_id
     * @return array
     */

    public function getCompanyModuleOptions($company_id)
    {
        $companyModuleOptions = CompanyOptions::where('company_id', $company_id)->select(
            'use_company_financials_module',
            'use_company_insurance_module',
            'use_company_invoicing_module',
            'use_company_license_module',
            'use_company_mileage_module',
            'use_company_payments_module',
            'use_company_vehicles_module',
            'service_plan'
        )->first();

        return ['companyModuleOptions' => $companyModuleOptions];
    }

    public function getCompanyIndustries($company_id = null)
    {
        $industries = CompanyIndustry::all();
        $company_industry_id = !empty($company_id) ? CompanyProfile::where('company_id', $company_id)->select('industry_id')->get() : -1;

        return [
            'industries' => $industries,
            'company_industry_id' => $company_industry_id
        ];
    }

    public function getInsuranceMinimums($company_id)
    {
        $insurance = Company::find($company_id)->insurance;
        return $insurance;
    }

    public function getCompanyMileageDates($company_id = null)
    {

        $local_company_id = $company_id;

        if ($company_id == null) {
            $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
            $local_company_id = $user->driverProfile->company_id;
        }

        $companyMileageDates = \DB::table('company_dates')
            ->join('company_mileages', 'company_mileages.company_id', 'company_dates.company_id')
            ->select('company_dates.mileage_lock_driver_day',
                'company_dates.mileage_lock_all_day',
                'company_dates.mileage_approval_end_day',
                'company_dates.mileage_reminder_day',
                'company_dates.second_mileage_reminder_day',
                'company_mileages.mileage_entry_method')
            ->where('company_id', '=', $local_company_id)
            ->first();


        return ['companyMileageDates' => $companyMileageDates];
    }


    /**
     * Returns a collection of the company payment dates. The timeframe depends on whether or not year
     *  and month parameters (optional) are provided.
     *
     *  Holiday information for each of the payment dates can be provided given $with_holidays is set to true
     *
     * @param null $company_id
     * @param bool $with_holidays
     * @param null $year
     * @param null $month
     * @return array
     */
    public function getCompanyPaymentDates($company_id = null, $with_holidays = false, $year = null, $month = null)
    {

        $local_company_id = $company_id;

        if ($company_id == null) {
            $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
            $local_company_id = $user->driverProfile->company_id;
        }

        //if no timeframe given return all dates for the company
        if ($year == null && $month == null) {
            \Debugbar::info('company ' . $company_id);
            $companyPaymentDates = \DB::table('company_payment_dates')
                ->select('year', 'month', 'banking_lock_date', 'driver_payment_date', 'received_date', 'approved_date')
                ->where('company_id', '=', $local_company_id)
                ->orderBy('year', 'asc')
                ->orderBy('month', 'asc')
                ->get();
            //else if year selected, return only dates for that year
        } else if ($year != null && $month == null) {
            $companyPaymentDates = \DB::table('company_payment_dates')
                ->select('year', 'month', 'banking_lock_date', 'driver_payment_date', 'received_date', 'approved_date')
                ->where('company_id', $local_company_id)
                ->where('year', '=', $year)
                ->orderBy('month', 'asc')
                ->get();
            //else return only the specific dates for the year and month
        } else {
            $companyPaymentDates = \DB::table('company_payment_dates')
                ->select('year', 'month', 'banking_lock_date', 'driver_payment_date', 'received_date', 'approved_date')
                ->where('company_id', '=', $local_company_id)
                ->where('year', '=', $year)
                ->where('month', '=', $month)
                ->first();
        }

        //gather holiday info only if requested
        if ($with_holidays == true) {
            $company = Company::find($local_company_id);

            foreach ($companyPaymentDates as $companyPaymentDate) {
                $companyPaymentDate->bankingLockDateHoliday = $this->getHoliday($companyPaymentDate->banking_lock_date, $company->country_id, $company->profile->state_province_id);
                $companyPaymentDate->driverPaymentDateHoliday = $this->getHoliday($companyPaymentDate->driver_payment_date, $company->country_id, $company->profile->state_province_id);
                $companyPaymentDate->receivedDateHoliday = $this->getHoliday($companyPaymentDate->received_date, $company->country_id, $company->profile->state_province_id);
                $companyPaymentDate->approvedDateHoliday = $this->getHoliday($companyPaymentDate->approved_date, $company->country_id, $company->profile->state_province_id);
            }
        }

        /**
         * If we find no payment dates, generate the default for the company
         */
        if ($companyPaymentDates->count() == 0) {
            $this->generateDefaultCompanyPaymentDates($company_id, $year);
            $this->getCompanyPaymentDates($company_id, true, $year);
        }

        return ['companyPaymentDates' => $companyPaymentDates];
    }

    /**
     * For a given year, this function generates default company payment date records for a given company on a given year.
     *
     *
     *
     * @param $company_id
     * @param $year
     * @return array
     */
    public function generateDefaultCompanyPaymentDates($company_id, $year)
    {
        $company = Company::find($company_id);

        if ($company->dates->direct_pay_day == 0) {
            abort(400, 'Cannot generate default payment dates if the default pay date is 0');
        }

        $companyPaymentDates = [];

        for ($i = 1; $i <= 12; $i++) {
            $companyPaymentDate = new CompanyPaymentDate;
            $companyPaymentDate->company_id = $company_id;
            $companyPaymentDate->year = $year;
            $companyPaymentDate->month = $i;
            $companyPaymentDate->banking_lock_date = $year . '-' . $i . '-' . ($company->dates->direct_pay_day - 3);
            $companyPaymentDate->driver_payment_date = $year . '-' . $i . '-' . $company->dates->direct_pay_day;
            $companyPaymentDate->received_date = $year . '-' . $i . '-' . ($company->dates->direct_pay_day - 3);
            $companyPaymentDate->approved_date = $year . '-' . $i . '-' . ($company->dates->mileage_approval_end_day + 1);
            $companyPaymentDate->created_at = date('Y-m-d H:i:s');
            $companyPaymentDate->save();

            $companyPaymentDate->bankingLockDateHoliday = $this->getHoliday($companyPaymentDate->banking_lock_date, $company->country_id, $company->profile->state_province_id);
            $companyPaymentDate->driverPaymentDateHoliday = $this->getHoliday($companyPaymentDate->driver_payment_date, $company->country_id, $company->profile->state_province_id);
            $companyPaymentDate->receivedDateHoliday = $this->getHoliday($companyPaymentDate->received_date, $company->country_id, $company->profile->state_province_id);
            $companyPaymentDate->approvedDateHoliday = $this->getHoliday($companyPaymentDate->approved_date, $company->country_id, $company->profile->state_province_id);

            $companyPaymentDates[] = $companyPaymentDate;
        }

        return ['companyPaymentDates' => $companyPaymentDates];
    }


    public function updateCompanyPaymentDates(Request $request)
    {
        foreach ($request->companyPaymentDates as $companyPaymentDate) {
            $v = Validator::make($companyPaymentDate, [
                'banking_lock_date' => 'required|date',
                'driver_payment_date' => 'required|date',
                'received_date' => 'required|date',
                'approved_date' => 'required|date',
            ]);

            $v->validate();

            $date_as_array = explode('-', $companyPaymentDate['banking_lock_date']);
            $year = $date_as_array[0];
            $month = $date_as_array[1];
            CompanyPaymentDate::where('year', $year)
                ->where('month', $month)
                ->where('company_id', $request->company_id)
                ->update([
                    'banking_lock_date' => $companyPaymentDate['banking_lock_date'],
                    'driver_payment_date' => $companyPaymentDate['driver_payment_date'],
                    'received_date' => $companyPaymentDate['received_date'],
                    'approved_date' => $companyPaymentDate['approved_date']
                ]);

        }


    }

    public function getCarPolicyDocuments($company_id)
    {
        \Debugbar::info('in getCarPolicyDocuments with ' . $company_id);

        $url = $company_id . '/car_policy/';

        $files = Storage::disk('upload_docs')->allFiles($url);
        \Debugbar::info($files);

        if (count($files) > 0) {
            foreach ($files as $f) {
                $parts = explode('/', $f);
                //remove first element and discard '/public/'
                array_shift($parts);
                //set pointer to end of the array, and grab filename
                $filename = end($parts);
                //if the second to last element (previous element to pointer)
                if (prev($parts) == 'related_documents') {
                    $parts = implode('/', $parts);
                    $relatedDocuments[] = ['filename' => $filename, 'url' => $parts];
                } else {
                    $parts = implode('/', $parts);
                    $carPolicyDocuments[] = ['filename' => $filename, 'url' => $parts];
                }
            }
        }

        // finally, there may be no files, reset to empty array so .length will work on front end
        if (empty($carPolicyDocuments)) {
            $carPolicyDocuments = [];
        }
        if (empty($relatedDocuments)) {
            $relatedDocuments = [];
        }

        return ['carPolicyDocuments' => $carPolicyDocuments, 'relatedDocuments' => $relatedDocuments];
    }

    public function renameCompanyDocument($doc, $company_name)
    {


//        $doc_parts = explode('/', $doc);
//        $file_name = array_pop($doc_parts);
//        $name = $file_name; // must return as-is if we don't rename
//        $location = implode('/', $doc_parts);
//
//        // old format / new format
//
//        // related_documents: company_name / ????
//
//
//
//        // insurance: userId-YYYY-mm-num.pdf / Ins_username_YYYY_mm_dd_sequence.pdf
//        if (stripos($file_name, $user_id) === 0) {
//            $name_parts = explode('-', $file_name); // 0:userId, 1:year, 2:month, 3:sequence
//            $name = 'Ins_' . $username . '_' . $name_parts[1] . '_' . $name_parts[2] . '_' . $name_parts[3];
//
//            \Storage::move($doc, $location . '/' . $name);
//        }
//        // license: DL_userId_sequence.pdf / DL_username_YYYY_mm_dd_sequence.pdf
//        elseif (stripos($file_name, $user_id) === 3) {
//            $name_parts = explode('_', $file_name); // 0:DL, 1:userId, 2:sequence
//            $date = date('_Y_m_');
//            $name = 'DL_' . $username . $date . $name_parts[2];
//            \Storage::move($doc, $location . '/' . $name);
//        }
        return $doc;

    }


    /**
     *  Returns the company insurance options. Used in driver registration to display insurance requirements.
     *  'select as' aliases used to disguise db structure on front end
     *
     * @param $company_id
     * @return array
     */

    // TODO: disguise these names so the db structure is unknown
    public function getCompanyInsuranceRequirements($company_id)
    {
        $companyInsuranceRequirements = \App\CompanyInsurance::where('company_id', $company_id)->select(
            'bodily_liability_per_person as blpp',
            'bodily_liability_per_accident as blpa',
            'property_damage as pd',
            'deductible as d',
            'comprehensive_deductible as comd',
            'collision_deductible as cold',
            'medical as m',
            'uninsured_per_person as upp',
            'uninsured_per_accident as upa',
            'public_liability as pl'
        )->first();

        return ['companyInsuranceRequirements' => $companyInsuranceRequirements];
    }

    // TODO: OTHER COMPANY ONE DOESN'T WORK PROPERLY NOTE TO JERZY AND RORY!!!
    public function getCompanyCountryDriver($company_id)
    {
        // test against -1, sometimes this gets called before the company has been rendered
        if ($company_id > 0) {

            $country_id = CompanyProfile::where('company_id', $company_id)->select('country_id')->first();
            return ['country_id' => $country_id];
        }
    }

    public function getCompaniesWithPreDefinedStops()
    {
        $companies = [];


        foreach (SavedStop::distinct()->select('company_id')->whereNotNull('company_id')->get() as $ss) {
            $company = Company::find($ss->company_id);
            $name = $company->name;
            $arr = ['id' => $ss->company_id, 'name' => $name];
            if (!in_array($arr, $companies) && $company->mileage->mileage_entry_method == 'Mi-Route') {
                $companies[] = $arr;
            }
        }

        return ['companies' => $companies];
    }

    /**
     * Returns an array consisting of all contracts for
     * the given saved stop id
     *
     * @param $saved_stop_id
     * @return array
     */
    public function getContractsForSavedStop($saved_stop_id)
    {
        $contracts = \DB::table('saved_stop_company_contract')
            ->where('saved_stop_id', $saved_stop_id)
            ->get()
            ->pluck('contract_id')
            ->all();
        return ['contracts' => $contracts];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getCompanyPredefinedStops(Request $request)
    {
        $offset = ($request->pagination_page - 1) * $request->page_size;

        $message = "";
        $stops = [];
        $data_size = 0;

        if (empty($request->company_id) || $request->company_id == -1):
            $message = "You must supply a company id";
            \Debugbar::info($message);
        else:

            // define conditions
            $where_clause = " AND ss.company_id = " . $request->company_id;
            if (!empty($request->search_param)):
                $where_clause .= " AND (ss.name LIKE '%{$request->search_param}%' OR ss.stop_number LIKE '%{$request->search_param}%' OR ss.street LIKE '%{$request->search_param}%')";
            else:

                if (!empty($request->division_id) && $request->division_id > 0):
                    $where_clause .= " and ds.division_id = {$request->division_id}";
                endif;
                if (!empty($request->state_province_id) && $request->state_province_id > 0):
                    $where_clause .= " and sp.id = {$request->state_province_id}";
                endif;
            endif;

            // build query
            $query = <<<EOQ
SELECT ss.id, ss.name, ss.stop_number, ss.street, ss.city, ss.zip_postal, sp.short_name AS state, ss.deleted_at, 
  COUNT(DISTINCT ds.division_id) AS divisions, 
  COUNT(DISTINCT dt.id) AS `trips`,
  COUNT(DISTINCT dm.user_id) AS driver_count
  FROM saved_stops ss
  JOIN state_provinces sp ON ss.state_province_id = sp.id
  LEFT JOIN division_stop ds ON ds.saved_stop_id = ss.id  
  LEFT JOIN daily_trips dt ON dt.saved_stop_id = ss.id
  LEFT JOIN daily_mileages dm ON dm.id = dt.daily_mileage_id
  WHERE ss.company_id > 0
  {$where_clause}
  GROUP BY ss.id, ss.name, ss.stop_number, ss.street, ss.city, ss.zip_postal, sp.short_name
  ORDER BY ss.stop_number
EOQ;
            $predefinedStops = \DB::select(\DB::raw($query));

            if ($request->use_stop_contract == 1) {
                foreach ($predefinedStops as $stop) {
                    $stop->contracts = \DB::table('saved_stop_company_contract')
                        ->where('saved_stop_id', $stop->id)
                        ->get();
                }
            }

            // get final count of stops
            $data_size = sizeof($predefinedStops);
            $message = 'success';
        endif;

        return ['message' => $message, 'predefinedStops' => $predefinedStops, 'data_size' => $data_size];
    }

    public function getCompanySavedStopDivisions($company_id)
    {
        $divisions = [];

//        foreach (\DB::table('companies as c')->join('divisions as d', 'd.company_id', 'c.id')
//                     ->join('division_stop as ds', 'ds.division_id', 'd.id')
//                     ->where('c.id', $company_id)
//                     ->select('ds.division_id')
//                     ->groupBy('ds.saved_stop_id', 'ds.division_id')->distinct()->get() as $d) {
//            $arr = ['id' => $d->division_id, 'name' => Division::find($d->division_id)->name];
//            $divisions[] = $arr;
//        }
        foreach (\DB::table('companies as c')->join('divisions as d', 'd.company_id', 'c.id')
                     ->join('division_stop as ds', 'ds.division_id', 'd.id')
                     ->where('c.id', $company_id)
                     ->select('d.id', 'd.name')
                     ->distinct()->get() as $d) {
            $arr = ['id' => $d->id, 'name' => $d->name];
            $divisions[] = $arr;
        }

        return ['divisions' => $divisions];
    }

    public function getCompanyPredefinedStop($id, $use_stop_contract = null)
    {
        $stop = SavedStop::find($id);
        $stop->driver_count = $stop->drivers->count();
        $stop->divisions = $stop->divisions;
        $stop->trip_count = DailyTrip::where('saved_stop_id', $id)->count();

        if ($use_stop_contract == 1) {
            $stop->contracts = \DB::table('saved_stop_company_contract')
                ->where('saved_stop_id', $stop->id)
                ->get();
        }

        return ['stop' => $stop];
    }

    public function updatePredefinedStop(Request $request)
    {

        if ($request->use_stop_contract) {
            return $this->updatePredefinedStopWithContract($request);
        }
        //dd($request->all());
        $error_fields = [];
        $stop = SavedStop::find($request->id);

        if (empty($request['stop_number'])) { // davery
            $error_fields[] = 'stop_number';
        }
        if (empty($request['name'])) { // davery
            $error_fields[] = 'name';
        }
        if (empty($request['divisions'])) { // davery
            //dd('empty');
            $error_fields[] = 'divisions';
        }
        if (count($error_fields)) { // davery
            $ret['message'] = 'fail';
        } else {
            $success = $stop->update($request->all());
            $ret['success'] = $success;

            if ($success) {
                // collect $request->divisions ids
                $request_division_ids = array_column($request->divisions, 'id');

                $preexisting_division_ids = []; // init
                foreach ($stop->divisions as $division) { // loop through current stop divisions
                    // if existing stop division id is NOT found in the $request_division_ids array
                    if (!in_array($division->id, $request_division_ids)) {
                        \DB::table('division_stop')->where('division_id', $division->id)
                            ->where('saved_stop_id', $stop->id)
                            ->delete(); // delete it
                    } else { // collect preexisting division ids for adding new divisions below
                        $preexisting_division_ids[] = $division->id;
                    }
                }

                foreach ($request['divisions'] as $division) { // loop through divisions & save to pivot table
                    // if request stop division id is NOT found in the $preexisting_division_ids array
                    if (!in_array($division['id'], $preexisting_division_ids)) {
                        \DB::insert(\DB::raw("insert into division_stop (division_id, saved_stop_id) values ({$division['id']}, {$stop->id})"));
                    }
                }
            }
        }
        //return ['message' => $stop ? 'success' : 'fail'];
        return [$ret, $error_fields];
    }

    public function updatePredefinedStopWithContract(Request $request)
    {
        if (!$request->use_stop_contract) {
            return response('use_stop_contract not present, aborting', 400);
        }

        $error_fields = [];
        $stop = SavedStop::find($request->id);


        if (empty($request['name'])) { // davery
            $error_fields[] = 'name';
        }

        if (isset($request->contract_numbers)) {
            // validate the contract numbers
            $contract_numbers = array_map(function ($x) {
                return trim((string)($x));
            }, explode(',', $request->contract_numbers));
            if (sizeof($contract_numbers) > 5) {
                $error_fields[] = 'contract_numbers';
            } else {
                foreach ($contract_numbers as $contract_number) {
                    if (!preg_match('/^\d{4}$/', $contract_number)) {
                        $error_fields[] = 'contract_numbers';
                    }
                }
            }
        }
        if (count($error_fields)) { // davery
            $ret['message'] = 'fail';
        } else {
            $success = $stop->update($request->all());
            $ret['success'] = $success;

            if ($success) {

                $company = Company::find($request->company_id);
                $now = date('Y-m-d H:i:s');
                $division_ids = $company->divisions->pluck('id')->all();

                // inactivate any contracts that were removed from the list
                foreach ($request->contracts_to_inactivate as $contract_id) {
                    if ($contract_id == 9400) {
                        abort(500, 'Error - cannot inactivate 9400');
                    }

                    \DB::table('saved_stop_company_contract')
                        ->where('saved_stop_id', $stop->id)
                        ->where('contract_id', $contract_id)
                        ->update([
                            'deleted_at' => $now
                        ]);
                }

                /**
                 * Loop through all of the contracts to 'activate'
                 * These may already have been created, and active, in which case they are skipped
                 * If they were created and deleted, they are reactivated for this stop
                 * If they are new, addContractToStop creates a new contract, and returns 1.
                 * We call update division stop as a precaution? TODO see REW-1144
                 */
                foreach ($request->contracts_to_activate as $contract_id) {
                    if ($this->addContractToStop($company->id, $stop->id, $contract_id) > 0) {
                        $this->updateDivisionStop($stop->id, $division_ids);
                    }
                }

            }
        }
        //return ['message' => $stop ? 'success' : 'fail'];
        return [$ret, $error_fields];

    }

    public function deletePredefinedStop(Request $request)
    {
        $stop = SavedStop::find($request->id); // find stop
        $stop->drivers()->detach(); // remove drivers
        $stop->divisions()->detach(); // remove divisions
        $stop->forceDelete(); // delete stop
    }

    /**
     * Return selected options for this company, used to restrict menu items
     *
     * @param Company $company
     * @return array
     */
    public function getCompanySelections(Company $company)
    {
        return [
            'payment_responsibility' => $company->payments->payment_responsibility,
            'use_stop_contract' => $company->options->use_stop_contract,
            'use_company_mileage_module' => $company->options->use_company_mileage_module,
            'use_company_financials_module' => $company->options->use_company_financials_module,
            'use_company_insurance_module' => $company->options->use_company_insurance_module,
            'use_company_license_module' => $company->options->use_company_license_module,
            'can_view_reimbursement_profile' => $company->options->can_view_reimbursement_profile,
            'can_view_reimbursement_details' => $company->options->can_view_reimbursement_details,
            'enforce_car_policy_acceptance' => $company->vehicles->enforce_car_policy_acceptance,
            'reports_display_monthly_tax_limit' => $company->taxes->reports_display_monthly_tax_limit,
            'use_quarterly_tax_adjustment' => $company->taxes->use_quarterly_tax_adjustment
        ];
    }

    /**
     * Returns all company report labels for the company with the given company id
     *
     * @param $company_id
     * @return mixed
     */
    public function getCompanyReportLabels($company_id)
    {
        return CompanyReportLabel::where('company_id', $company_id)->get()->all();
    }

    /**
     * Returns the bank associated with a given company
     *
     * By default, restricted is true, returning only bank_name, country_id, status and created at
     *  created_at is the date the bank was assigned to the company
     */
    public function getCompanyBank($company_id, $restricted = true)
    {
        return response()->json([
            'bank' => Company::find($company_id)->bank($restricted)
        ]);
    }

    public function getCompanyCreatedAndDeletedAt($company_id)
    {
        return Company::withTrashed()->select('created_at', 'deleted_at')->where('id', $company_id)->orderBy('deleted_at')->first();
    }


    /**
     * Function to gather all data for the fixed and variable discrepancies report
     *
     * @param $company_id
     * @param $division_id
     * @param $year
     * @param $month
     * @return array
     */
    public function getFixedAndVariableDiscrepancies($company_id, $division_id, $year, $month)
    {
        $discrepancies = new \stdClass();
        $discrepancies->fixed = [];
        $discrepancies->variable = [];
        $report_date = $year . '-' . str_pad($month, '2', '0', STR_PAD_LEFT) . '-01';

        $previous_month = $month == 1 ? 12 : $month - 1;

        $year_of_previous_month = $previous_month == 12 ? $year - 1 : $year;

        $division_where_clause = $division_id > 0 ? ' and d.division_id = ' . $division_id : '';

        // Speed is needed so the following query is run
        $query = <<<EOQ
select distinct(u.id) as user_id, u.first_name, u.last_name, d.division_id, di.name as division_name, d.employee_number, 
IFNULL(cm.fixed_reimbursement - cm.adjustment_amount, 0) as current_fixed, IFNULL(pm.fixed_reimbursement - pm.adjustment_amount,0) AS previous_fixed,
IFNULL(cm.variable_reimbursement, 0) as current_variable, IFNULL(pm.variable_reimbursement, 0) AS previous_variable,
d.street_light_id, cm.driver_profile_id as current_driver_profile_id, pm.driver_profile_id as previous_driver_profile_id
from users u
left join monthly_reimbursements cm on u.id = cm.user_id and cm.year = {$year} and cm.month = {$month}
left join monthly_reimbursements pm on u.id = pm.user_id and pm.year = {$year_of_previous_month} and pm.month = {$previous_month}
join driver_profiles d on u.id = d.user_id
join divisions di on di.id = d.division_id
where d.company_id = {$company_id} {$division_where_clause}
and ( IFNULL(cm.fixed_reimbursement - cm.adjustment_amount, 0) > 0 or IFNULL(pm.fixed_reimbursement - pm.adjustment_amount,0) > 0 )
order by 3, 2;
EOQ;

        $discrepancies = \DB::select($query);
        foreach ($discrepancies as $d) {
            $d->difference_fixed = $d->current_fixed - $d->previous_fixed;
            $d->difference_variable = $d->current_variable - $d->previous_variable;

            if ($d->previous_fixed == 0) {
                $d->fixed_discrepancy_percentage = $d->current_fixed > 0 ? 100 : 0;
            } else {
                $d->fixed_discrepancy_percentage = 100 * $d->difference_fixed / $d->previous_fixed;
            }

            if ($d->previous_variable == 0) {
                $d->variable_discrepancy_percentage = $d->current_variable > 0 ? 100 : 0;
            } else {
                $d->variable_discrepancy_percentage = 100 * $d->difference_variable / $d->previous_variable;
            }
        }

        // return all the data
        return ['discrepancies' => $discrepancies];
    }


    /**
     * Function to gather information about a specific discrepancy on the fixed and variable discrepancies report
     *
     * @param $new_driver_profile_id
     * @param $old_driver_profile_id
     */
    public function getReimbursementDiscrepancyDetails($new_driver_profile_id, $old_driver_profile_id, $year, $month, $service_plan)
    {

        $previous_month = $month == 1 ? 12 : $month - 1;
        $year_of_previous_month = $previous_month == 12 ? $year - 1 : $year;

        $rc = new ReimbursementController();

        if (!empty($old_driver_profile_id) && $old_driver_profile_id > 0) {
            $oldReimbursementData = $rc->getDataForReimbursementFromDriver($year_of_previous_month, $previous_month, $service_plan, $old_driver_profile_id);
            $oldFixedData = $rc->calculateFixedReimbursementProxy($oldReimbursementData);
            $oldVariableData = $rc->calculateVariableReimbursementProxy($oldReimbursementData);
            $rc->getReimbursementLabels($oldFixedData, $oldVariableData, $oldReimbursementData);
        } else {
            $oldFixedData = null;
            $oldVariableData = null;
        }

        if (!empty($new_driver_profile_id) && $new_driver_profile_id > 0) {
            $newReimbursementData = $rc->getDataForReimbursementFromDriver($year, $month, $service_plan, $new_driver_profile_id);
            $newFixedData = $rc->calculateFixedReimbursementProxy($newReimbursementData);
            $newVariableData = $rc->calculateVariableReimbursementProxy($newReimbursementData);
            $rc->getReimbursementLabels($newFixedData, $newVariableData, $newReimbursementData);
        } else {
            $newFixedData = null;
            $newVariableData = null;
        }

        return [
            'oldDriverProfile' => DriverProfile::withTrashed()->find($old_driver_profile_id),
            'newDriverProfile' => DriverProfile::withTrashed()->find($new_driver_profile_id),
            'newFixedData' => $newFixedData,
            'newVariableData' => $newVariableData,
            'oldFixedData' => $oldFixedData,
            'oldVariableData' => $oldVariableData
        ];

    }

    public function generateFixedAndVariableDiscrepanciesExcel(Request $request)
    {
        $parameters = json_decode(json_encode($request->all()));

        $spreadsheet = PhpSpreadsheet\IOFactory::load(base_path() . '/resources/assets/xls/fixed_and_variable_discrepancies_template.xls');
        $worksheet = $spreadsheet->getActiveSheet();

        $reimbursement_type = ucfirst($parameters->reimbursement_type);

        // title
        $title = $reimbursement_type . ' Rate ' . $parameters->discrepancy_percentage . '% Discrepancy Report';
        $worksheet->getCell('A1')->setValue($title);

        // reimbursement type
        $worksheet->getCell('F2')->setValue($reimbursement_type . ' Reimbursement');

        // months
        $month = date('F', mktime(0, 0, 0, $parameters->month, 10));
        $previous_month = $parameters->month == 1 ? date('F', mktime(0, 0, 0, 12, 10)) : date('F', mktime(0, 0, 0, $parameters->month - 1, 10));
        $year_of_previous_month = $parameters->month == 1 ? $parameters->year - 1 : $parameters->year;

        $worksheet->getCell('E3')->setValue($previous_month . ', ' . $year_of_previous_month . ' Rate');
        $worksheet->getCell('F3')->setValue($month . ', ' . $parameters->year . ' Rate');

        /** report info (ex. name, creation date, company, division selection...) */
        //name
        $worksheet->getCell('K1')->setValue($title);

        $creation_date = date('F jS, Y', strtotime('now'));
        $worksheet->getCell('K2')->setValue($creation_date);

        // company
        $worksheet->getCell('K3')->setValue(Company::find($parameters->company_id)->name);

        // division
        $division_name = $parameters->division_id == -1 ? 'All' : Division::find($parameters->division_id)->name;
        $worksheet->getCell('K4')->setValue($division_name);

        // year
        $worksheet->getCell('K5')->setValue($parameters->year);

        // month
        $worksheet->getCell('K6')->setValue($month);

        // data
        $row = 4;
        foreach ($parameters->discrepancies as $d) {
            // last name
            $worksheet->getCell('A' . $row)->setValue($d->last_name);

            // first name
            $worksheet->getCell('B' . $row)->setValue($d->first_name);

            // employee number
            $worksheet->getCell('C' . $row)->setValue($d->employee_number);

            // division
            $worksheet->getCell('D' . $row)->setValue($d->division_name);

            // rates
            if ($parameters->reimbursement_type == 'fixed') {
                $worksheet->getCell('E' . $row)->setValue($d->previous_fixed);
                $worksheet->getCell('F' . $row)->setValue($d->current_fixed);
                $worksheet->getCell('G' . $row)->setValue(number_format((float)$d->fixed_discrepancy_percentage, 2, '.', ''));
            } else {
                //variable
                $worksheet->getCell('E' . $row)->setValue($d->previous_variable);
                $worksheet->getCell('F' . $row)->setValue($d->current_variable);
                $worksheet->getCell('G' . $row)->setValue(number_format((float)$d->variable_discrepancy_percentage, 2, '.', ''));
            }

            $row++;
        }


        $tmp_dir = storage_path() . '/app/public/tmp/';
        $file_name = $parameters->discrepancy_percentage . '_percent_'
            . $parameters->reimbursement_type
            . '_discrepancies_report_' . date("Y-m-d") . '.xls';

        $writer = PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save($tmp_dir . $file_name);

        return ['file_name' => $file_name];

    }

    public function inactivateDivision($division_id)
    {
        $division = Division::find($division_id);
        if (!empty($division)) {
            $division->delete();
            $message = "Division deleted";
        } else {
            $message = "No division found";
        }
        return ['message' => $message];
    }

    public function getReimbursementYears(Request $request)
    {
        $company_id = $request->company_id;

        $query = "SELECT DISTINCT year FROM monthly_reimbursements WHERE user_id IN 
        ( SELECT user_id FROM driver_profiles WHERE company_id = $company_id ORDER BY year )";

        $years = $records = \DB::select(\DB::raw("$query"));

        if (count($years) > 0) {
            $startYear = $years[0]->year;
            $endYear = $years[count($years) - 1]->year;
        } else {
            $startYear = null;
            $endYear = null;
        }

        return response()->json([
            'startYear' => $startYear,
            'endYear' => $endYear
        ]);

    }

    public function notifyApproverOfNewInsuranceDocument($company_id = -1)
    {
        if ($company_id > 0) {
            $coordinatorId = CompanyCoordinators::where('company_id', $company_id)->first()->insurance_coordinator_id;

            if (empty($coordinatorId)) {
                $coordinatorId = 16341; // this is Doug Little
            }

//            $coordinatorId = 630;
//            \DB::select(\DB::raw(
//                <<<EOQ
//DELETE FROM notification_user WHERE notification_id = 3 AND user_id = $coordinatorId
//EOQ
//            ));
//            Notification::find(3)->fusionNotify($coordinatorId);

            $user = User::find($coordinatorId);
            $user->fusionNotify('new-driver-insurance');
        }
    }

    public function getCompanyLogoExists($company_slug = '')
    {
        $logo_exists = file_exists(public_path() . '/storage/company_logos/' . $company_slug . '.jpg');
        return ['logo_exists' => $logo_exists];
    }

    /**
     * Returns coordinators keyed by their role.
     *
     * @param $company_id
     *
     *
     * $coordinators = [
     *      'miroute_coordinator' => [
     *          'name' => aasdf,
     *          'phone_number' => 9012301324
     *      ],
     *      .
     *      .
     *      .
     * ]
     *
     */
    public function getCompanyCoordinators($company_id)
    {
        $coordinatorsRecord = CompanyCoordinators::where('company_id', $company_id)->first()->toArray();
        $coordinatorsRecordKeys = array_keys($coordinatorsRecord);

        $coordinators = [];
        foreach ($coordinatorsRecordKeys as $index => $coordKey) {
            //three cases, either a phone number, fax number or an id
            if (strpos($coordKey, 'coordinator_id') !== false || strpos($coordKey, 'associate_id') !== false || strpos($coordKey, 'executive_id') !== false) {
                $user = User::find($coordinatorsRecord[$coordinatorsRecordKeys[$index]]);
                if (!isset($user)) {
                    continue;
                }

                $niceNewKey = explode('_', $coordKey);
                $niceNewKey = $niceNewKey[0] . ucfirst($niceNewKey[1]);

                $coordinators[$niceNewKey]['position'] = $niceNewKey;
                $coordinators[$niceNewKey]['name'] = $user->first_name . ' ' . $user->last_name;
                $coordinators[$niceNewKey]['number'] = $coordinatorsRecord[$coordinatorsRecordKeys[$index + 1]];
                $coordinators[$niceNewKey]['email'] = $user->email;
            }
        }

        return ['coordinators' => $coordinators];
    }


    /**
     * This function is called to upload an excel document on add many drivers
     * @param Request $request
     * @param $company_id
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|void
     */
    public function uploadAddManyDriversExcelDocument(Request $request, $company_id)
    {

        // for safety
        if (!Auth::user()->isSuper()) {
            return response(json_encode(['Unauthorized' => ['Only super users can access this route.']]), 401);
        }

        if (!$request->file) {
            return;
        }

        $storage_dir = '/public/docs/' . $company_id . '/driver_upload';

        Storage::makeDirectory($storage_dir, 0775, true, true);

        $count = 0;
        do {
            $filename = 'add_many_drivers_' . date('Y_m_d') . ($count == 0 ? '' : '(' . $count . ')') . '.xls';
            $existingFile = \DB::table('add_many_drivers_files')
                ->where('filename', $filename)
                ->where('company_id', $company_id)
                ->first();

            if ($existingFile && $existingFile->applied == false) {
                \DB::table('add_many_drivers_files')
                    ->where('filename', $filename)
                    ->where('company_id', $company_id)
                    ->delete();
                unset($existingFile);
            }
            $count++;
        } while (!empty($existingFile));

        $request->file->storeAs($storage_dir . '/', $filename);

        // insert the new
        \DB::table('add_many_drivers_files')->insert([
            'company_id' => $company_id,
            'filename' => $filename,
            'applied' => false,
        ]);

        return ['filename' => $filename];
    }

    public function getAddManyDriversFiles($company_id)
    {
        // for safety
        if (!Auth::user()->isSuper()) {
            return response(json_encode(['Unauthorized' => ['Only super users can access this route.']]), 401);
        }

        $existingFiles = \DB::table('add_many_drivers_files')->where('company_id', $company_id)->orderBy('filename', 'desc')->get();

        $path_to_files = base_path() . '/storage/app/public/docs/' . $company_id . '/driver_upload/';

        try {
            foreach ($existingFiles as $file) {
                $this->parseAddManyDriversDocument($path_to_files, $file);
                $this->validateAddManyDriversDocument($company_id, $file);
                if ($file->applied == true) {
                    $file->results = new \stdClass();
                    $file->results->errors = \DB::table('add_many_drivers_errors')
                        ->where('file_id', $file->id)
                        ->get();
                    $file->results->insert_count = $file->insert_count;
                    $file->results->error_count = $file->error_count;
                    foreach ($file->results->errors as $error) {
                        $error->row_content = json_decode($error->row_content);
                    }
                }
            }
        } catch (\Exception $e) {
            return response(['error_parsing_file' => [$e->getMessage()]]);
        }

        return [
            'existingFiles' => $existingFiles
        ];

    }

    private function parseAddManyDriversDocument($path_to_file, &$file)
    {
        $spreadsheet = PhpSpreadsheet\IOFactory::load($path_to_file . $file->filename);
        $worksheet = $spreadsheet->getActiveSheet();
        $file->data = $worksheet->toArray();

        /**
         * I don't want numbered arrays. I want key value pairs!
         * Gimme them key value pairs yo!
         */
        for ($i = 1; $i < sizeof($file->data); $i++) {
            $row_copy = $file->data[$i];
            $file->data[$i] = [];
            for ($j = 0; $j < sizeof($row_copy); $j++) {
                $new_key = str_replace(' ', '_', strtolower($file->data[0][$j]));
                $file->data[$i][$new_key] = $row_copy[$j];
                if ($new_key == 'start_date') {
                    $file->data[$i][$new_key] = date('Y-m-d', strtotime($row_copy[$j]));
                }
            }
        }
    }

    /**
     * This is not in reimbursement traits because it is specific to add many drivers.
     *
     * File is passed by reference, and errors are added to the file object
     * File, when originally passed, should match an add_many_drivers_files record.
     *
     * @param $company_id
     * @param $file
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    private function validateAddManyDriversDocument($company_id, &$file)
    {
        $company = Company::find($company_id);
        if (empty($company)) {
            return response(json_encode(['invalid_company_id' => [$company_id . ' is an invalid company id.']]), 400);
        }

        // initialize the errors array
        $file->errors = [];
        $file->warnings = [];

        // get an array of all emails, (first row is column titles remember)
        $allEmails = array_column(array_slice($file->data, 1), 'email');
        \Debugbar::info($allEmails);

        /**
         * loop through all of the rows and validate each field. start at one as the first row is headers
         *
         */
        for ($i = 1; $i < sizeof($file->data); $i++) {
            $row = $file->data[$i];

            /** REQUIRED FIELDS */
            // only territory_list, payroll, cost_center and job_title can be empty
            $allowed_empty = ['territory_list', 'employee_number', 'payroll', 'cost_center', 'job_title', 'manager_first_name', 'manager_last_name', 'n/a'];
            foreach ($row as $key => $value) {

                if (in_array($key, $allowed_empty)) {
                    continue;
                }

                /**
                 * A required value is not present.
                 * Populate the errors array.
                 * The errors array will be keyed by row number for the front end, and contain an array of errors
                 */
                if (empty($value)) {
                    if (!isset($file->errors[$i])) {
                        $file->errors[$i] = [];
                    }
                    $file->errors[$i][] = $key . ' is a required field';
                }
            }

            /** FIRST NAME */
            if (sizeof($row['first_name']) < 1) {
                $file->errors[$i][] = 'Invalid first name';
            }

            /** LAST NAME */
            if (sizeof($row['last_name']) < 1) {
                $file->errors[$i][] = 'Invalid last name';
            }

            /** EMAIL TODO */
            /**
             * check if this email is duplicated
             * first get an array of driver emails not containing the present email
             * then check if this email is in that array
             */
            $duplicates = array_keys($allEmails, $row['email']);
            \Debugbar::info($duplicates);
            if (sizeof($duplicates) > 1) {
                $duplicates = array_map(function ($val) {
                    return $val + 1;
                }, $duplicates);
                $file->errors[$i][] = 'Duplicate driver email rows ' . json_encode($duplicates);
            }

            // see if an existing user has the email. and if this user has the same first and last name
            $existingUserWithSameEmail = User::where('email', $row['email'])->first();
            if (!empty($existingUserWithSameEmail)) {
                if ($existingUserWithSameEmail->first_name != $row['first_name']
                    || $existingUserWithSameEmail->last_name != $row['last_name']) {
                    $file->errors[$i][] = 'A user with this email already exists: '
                        . 'FIRST NAME - ' . $existingUserWithSameEmail->first_name
                        . ' LAST NAME - ' . $existingUserWithSameEmail->last_name
                        . ' ID - ' . $existingUserWithSameEmail->id
                        . ' If this is the same person, update the spreadsheet to contain matching first and last names '
                        . 'otherwise, use a different email';
                }
                if ($existingUserWithSameEmail->isDriver()) {
                    $file->errors[$i][] = 'A driver with this email already exists: '
                        . 'FIRST NAME - ' . $existingUserWithSameEmail->first_name
                        . ' LAST NAME - ' . $existingUserWithSameEmail->last_name
                        . ' of ' . Company::find($existingUserWithSameEmail->driverProfile->company_id)->name;
                }
                $file->warnings[$i][] = 'A user with this email already exists: '
                    . 'FIRST NAME - ' . $existingUserWithSameEmail->first_name
                    . ' LAST NAME - ' . $existingUserWithSameEmail->last_name
                    . ' ID - ' . $existingUserWithSameEmail->id
                    . ' Proceeding will create a driver profile for this user';
            }

            /** STREET & CITY */
            // validate street
            if (!preg_match('/^[1-90-9]+ +[a-zA-Z0-9#\-\'\.,_()# ]*$/', $row['street'])) {
                $file->errors[$i][] = 'Invalid street';
            }

            // validate city
            if (!preg_match('/^[a-zA-Z0-9] ?[a-zA-Z0-9 ,\-\'.*]*$/', $row['city'])) {
                $file->errors[$i][] = 'Invalid city';
            }

            /** COUNTRY */
            if ($row['country'] != 'US' && $row['country'] != 'CA') {
                $file->errors[$i][] = 'Country must be either US or CA, cannot validate state/province and zip/postal';
            } else if ($row['country'] == 'US') {
                $state_or_province = 'state';
                $zip_or_postal = 'zip_code';
            } else {
                $state_or_province = 'province';
                $zip_or_postal = 'postal_code';
            }

            /** STATE/PROVINCE */
            if ($state_or_province) {
                $stateProvince = StateProvince::where('short_name', $row[$state_or_province])->first();
                if (empty($stateProvince)) {
                    $file->errors[$i][] = 'Invalid ' . $state_or_province;
                }
            }

            /**
             * ZIP/POSTAL
             * it only makes sense to do so if a state or province is present
             */
            if (!empty($stateProvince) && $zip_or_postal) {
                try {
                    $this->validateZipPostal($stateProvince->country_id, $stateProvince->id, $row[$zip_or_postal]);
                } catch (\Exception $e) {
                    $file->errors[$i][] = json_decode($e->getMessage())->zip_postal[0];
                }
            } else {
                $file->errors[$i][] = 'Invalid ' . $state_or_province . ' could not validate ' . $zip_or_postal;
            }

            /** EMPLOYEE NUMBER */
            if (!preg_match('/^[a-zA-Z0-9. \-,]*$/', $row['employee_number'])) {
                $file->errors[$i][] = 'Invalid employee number';
            }

            /** DIVISION */
            // get all divisions for the company, filter them based on the division name equal to this rows divison name, grab all results, if all results are empty its invalid
            if (empty($company->divisions->filter(function ($value, $key) use ($row) {
                return $value->name == $row['division'];
            })->all())) {
                $file->errors[$i][] = 'Invalid division';
            }

            /** VEHICLE PROFILE */
            // get all vehicleprofiles for the company, filter them based on the name equal to this rows vehicle_profile, grab all results, if all results are empty its invalid
            //TODO THIS CURRENTLY DOESN'T CHECK TRIM LEVEL (VEHICLE->RANK). IN THE FUTURE WE MAY WANT TO DO THAT. 2018-07-11
            if (empty($company->vehicleProfiles->filter(function ($value, $key) use ($row) {
                return $value->name() == $row['vehicle_profile'];
            })->all())) {
                $file->errors[$i][] = 'Invalid vehicle profile';
            }


            if (empty(MileageBand::all()->filter(function ($value, $key) use ($row) {
                return $value->name() == $row['annual_mileage'];
            })->all())) {
                $file->errors[$i][] = 'Invalid annual mileage';
            }

            /** TERRITORY */
            $territory_type_list = ['Home City', 'Home State', 'Multi State', 'Home Province', 'Multi Province'];
            if (!in_array($row['territory'], $territory_type_list)) {
                $file->errors[$i][] = 'Invalid territory, territory must be one of the following: ' . implode(', ', $territory_type_list);
            }

            /** TERRITORY LIST */
            if (in_array('Territory List', ($file->data[0]))) {
                if (!in_array($row['territory'], ['Multi Province', 'Multi State']) && sizeof($row['territory_list']) > 0) {
                    $file->errors[$i][] = 'Invalid territory list, if territory is not either Multi Province or Multi State then territory list must be empty';
                } else if (in_array($row['territory'], ['Multi Province', 'Multi State'])) {

                    // we have selected multi city or multi state
                    if (sizeof($row['territory_list']) < 1) {
                        $file->errors[$i][] = 'Invalid territory list, territory list cannot be empty when Multi Province or Multi State is selected';
                    } else if ($row['territory'] == 'Multi City') {

//                        $fuel_city_name_array = array_map(function($val) { return trim($val); }, explode(',', $row['territory_list']));
//
//                        $fuelCities = FuelCity::where('country_id', $company->profile->country_id)->get();
//
//                        // loop through each present entry and see if its valid
//                        foreach($fuel_city_name_array as $fuel_city) {
//                            if ( empty( $fuelCities->filter( function($value, $key) use ($fuel_city) { return $value->name == $fuel_city; } )->all() ) ) {
//                                $file->errors[$i][] = 'Invalid fuel city: ' . $fuel_city;
//                            }
//                        }


                    } else if ($row['territory'] == 'Multi State' || $row['territory'] == 'Multi Province') {

                        $state_province_name_array = array_map(function ($val) {
                            return trim($val);
                        }, explode(',', $row['territory_list']));

                        $stateProvinces = StateProvince::where('country_id', $company->profile->country_id)->get();

                        foreach ($state_province_name_array as $state_province) {
                            if (empty($stateProvinces->filter(function ($value, $key) use ($state_province) {
                                return $value->short_name == $state_province;
                            })->all())) {
                                $file->errors[$i][] = 'Invalid ' . $state_or_province . ' in territory list: ' . $state_province;
                            }
                        }

                    }

                }
            }

            /** MANAGER */
            if ((sizeof($row['manager_first_name']) > 0 || sizeof($row['manager_last_name']) > 0)
                && (sizeof($row['manager_first_name']) < 1) || sizeof($row['manager_last_name']) < 1) {
                $file->errors[$i][] = 'Invalid manager, both first and last names are required when one is present';

                //todo unique manager emails?? need this
                //todo and for first and last names to match!
            }

            /** START DATE */
            if (isset($row['start_date'])) {
                if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $row['start_date'])) {
                    $file->errors[$i][] = 'Invalid start date: ' . $row['start_date'];
                }
            }

        }

        // if there are errors don't bother with the warnings
        if (sizeof($file->errors) > 0) {
            $file->warnings = [];
        }
    }

    /**
     * Request should contain two fields:
     *  - company_id
     *  - file_id
     * @param Request $request
     */
    public function addManyDrivers(Request $request)
    {
        // for safety
        if (!Auth::user()->isSuper()) {
            return response(json_encode(['Unauthorized' => ['Only super users can access this route.']]), 401);
        }

        // get the filename
        $file = \DB::table('add_many_drivers_files')
            ->where('id', $request->file_id)
            ->first();

        // get the path
        $path_to_file = base_path() . '/storage/app/public/docs/' . $request->company_id . '/driver_upload/';

        try {
            $this->parseAddManyDriversDocument($path_to_file, $file);
            $this->validateAddManyDriversDocument($request->company_id, $file);
        } catch (\Exception $e) {
            return response(['error_parsing_file' => [$e->getMessage()]]);
        }

        if (sizeof($file->errors) > 0) {
            return response(json_encode(['Errors' => [$file->filename . ' contains errors no drivers have been inserted. Refresh the page and contact CarData IT Support']]), 400);
        }

        // file is good, initialize some varrys
        $insert_count = 0;
        $error_count = 0;
        $row_number = 0;

        $company = Company::find($request->company_id);
        $country_id = $company->profile->country_id;

        foreach ($file->data as $row_number => $row) {
            if ($row_number == 0) {
                // skip the title row
                continue;
            }
            try {

                $existingUserWithSameEmail = User::where('email', $row['email'])->first();

                /**
                 * Create the user.
                 * Next create the driver.
                 */
                if (empty($existingUserWithSameEmail)) {
                    try {
                        $user = User::create([
                            'username' => app('\App\Http\Controllers\UserController')->generateUsername($row['first_name'], $row['last_name']),
                            'first_name' => $row['first_name'],
                            'last_name' => $row['last_name'],
                            'email' => $row['email'],
                            'password' => \Hash::make('secret'),
                        ]);
                    } catch (\Exception $e) {
                        // if the user was not created successfully log it and continue to the next row
                        $this->logAddManyDriversError($request->file_id, $e->getMessage(), $row_number, $row);
                        $error_count++;
                        continue;
                    }
                }

                // create the address
                try {
                    $zipPostal = $country_id == 1 ? ZipCode::where('zip_code', $row['zip_code'])->first() : PostalCode::where('postal_code', $row['postal_code'])->first();

                    if (empty($zipPostal)) {
                        throw new \Exception('Could not find zip or postal code');
                    }

                    $address = Address::create([
                        'user_id' => (isset($user) ? $user->id : $existingUserWithSameEmail->id),
                        'street' => $row['street'],
                        'street2' => null,
                        'city' => $row['city'],
                        'zip_postal' => ($country_id == 1 ? $zipPostal->zip_code : $zipPostal->postal_code),
                        'state_province_id' => ($country_id == 1 ? $zipPostal->state_id : $zipPostal->province_id),
                        'country_id' => $country_id,
                        'latitude' => $zipPostal->latitude,
                        'longitude' => $zipPostal->longitude,
                        'map_latitude' => $zipPostal->latitude,
                        'map_longitude' => $zipPostal->longitude
                    ]);
                } catch (\Exception $e) {
                    // if the address was not created successfully, delete the user, log it, and continue
                    $user->forceDelete();
                    $this->logAddManyDriversError($request->file_id, $e->getMessage(), $row_number, $row);
                    $error_count++;
                    continue;
                }


                // gather data for dp and create it
                $divisions = Division::where('company_id', $request->company_id)->get();


                try {

                    // get the proper territory and territory list
                    $territory_name = $row['territory'];
                    if ($country_id == 2) {
                        // canada
                        if ($territory_name == 'Home Province') {
                            $territory_name = 'Home State';
                        } else if ($territory_name = 'Multi Province') {
                            $territory_name = 'Multi State';
                        }
                    }

                    $territory_id = TerritoryType::all()->first(function ($value, $key) use ($territory_name) {
                        return $value->name == $territory_name;
                    })->id;

                    // if multi-state is the name, get the states
                    if ($territory_name == 'Multi State') {

                        $state_province_name_array = array_map(function ($val) {
                            return trim($val);
                        }, explode(',', $row['territory_list']));
                        $stateProvinces = StateProvince::where('country_id', $country_id)->get();
                        $territory_list = '';
                        foreach ($state_province_name_array as $state_province) {
                            $territory_list .= ',' . $stateProvinces->first(function ($value, $key) use ($state_province) {
                                    return $value->short_name == $state_province;
                                })->id;
                        }
                    }

                    //todo: note that since trim level isn't checked this just grabs the first vehicle profile present with the same name, see other todos
                    $driver = DriverProfile::create([
                        'user_id' => (isset($user) ? $user->id : $existingUserWithSameEmail->id),
                        'active' => 1,
                        'company_id' => $request->company_id,
                        'division_id' => $divisions->first(function ($value, $key) use ($row) {
                            return $value->name == $row['division'];
                        })->id,
                        'street_light_id' => 1,
                        'employee_number' => (isset($row['employee_number']) ? $row['employee_number'] : null),
                        'start_date' => $row['start_date'],
                        'stop_date' => null,
                        'job_title' => (isset($row['job_title']) ? $row['job_title'] : null),
                        'address_id' => $address->id,
                        'vehicle_profile_id' => $company->vehicleProfiles->first(function ($value, $key) use ($row) {
                            return $value->name() == $row['vehicle_profile'];
                        })->id,
                        'personal_vehicle_id' => null,
                        'cost_center' => (isset($row['cost_center']) ? $row['cost_center'] : null),
                        'mileage_band_id' => MileageBand::all()->first(function ($value, $key) use ($row) {
                            return $value->name() == $row['annual_mileage'];
                        })->id,
                        'territory_type_id' => $territory_id,
                        'territory_list' => isset($territory_list) ? $territory_list : null,
                        'editing_user_id' => null,
                        'fuel_city_id' => app('\App\Http\Controllers\ReimbursementController')->findClosestFuelCity($address)->id,
                        'car_policy_accepted' => 0,
                        'reimbursement_detail_id' => null,
                        'insurance_review_id' => null,
                        'license_review_id' => null,
                        'next_insurance_review_date' => $this->getNextDefaultInsuranceReviewDate($request->company_id),
                        'next_license_review_date' => $this->getNextDefaultLicenseReviewDate($request->company_id),
                    ]);

                    // attach the driver role
                    if (isset($user)) {
                        $user->attachRole(1);
                    } else {
                        $existingUserWithSameEmail->attachRole(1);
                    }

                } catch (\Exception $e) {
                    \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
                    $this->logAddManyDriversError($request->file_id, $e->getMessage(), $row_number, $row);
                    $error_count++;
                    $address->forceDelete();
                    if (!empty($driver)) {
                        $user->syncRoles([]);
                        $driver->forceDelete();
                    }
                    $user->forceDelete();
                    \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
                    continue;
                }


                /**
                 * determine if the manager exists
                 * if it does exist, make the assignment here and now
                 * if it does not exist, create it, and make the assignment.
                 * on subsequent rows, if a driver is assigned to this manager
                 * the manager will be found and the driver assigned
                 */
                if (isset($row['manager_email'])) {
                    $existingManagerUser = \DB::table('users')
                        ->join('manager_profiles', 'users.id', '=', 'manager_profiles.user_id')
                        ->where('users.email', $row['manager_email'])
                        ->where('users.first_name', $row['manager_first_name'])
                        ->where('users.last_name', $row['manager_last_name'])
                        ->select()
                        ->first();

                    if (empty($existingManagerUser)) {
                        // manager doesn't exist, create him
                        try {
                            $managerUser = User::create([
                                'username' => app('\App\Http\Controllers\UserController')->generateUsername($row['manager_first_name'], $row['manager_last_name']),
                                'first_name' => $row['manager_first_name'],
                                'last_name' => $row['manager_last_name'],
                                'email' => $row['manager_email'],
                                'password' => \Hash::make('secret'),
                            ]);

                            $managerUser->attachRole(2);

                            $existingManagerUser = ManagerProfile::create([
                                'user_id' => $managerUser->id,
                                'manager_id' => ManagerProfile::max('manager_id') + 1,
                                'active' => 1,
                                'company_id' => $request->company_id,
                                'manager_number' => null,
                                'approval_description' => null,
                                'address_id' => null,
                                'assigned_drivers' => 'some'
                            ]);

                        } catch (\Exception $e) {
                            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
                            $error_count++;
                            $this->logAddManyDriversError($request->file_id, 'Manager creation failed, driver creation successful', $row_number, $row);
                            if (!empty($managerUser)) {
                                $managerUser->syncRoles([]);
                                $managerUser->forceDelete();
                            }
                            // this existing manager user is actually the manager profile just created
                            if (!empty($existingManagerUser)) {
                                $existingManagerUser->forceDelete();
                            }

                            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
                            continue;
                        }

                    }

                    //if here we have a manager and are just assigning at this point
                    try {
                        \DB::table('manager_driver')->insert([
                            'manager_id' => $existingManagerUser->manager_id,
                            'driver_id' => $driver->user_id,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                    } catch (\Exception $e) {
                        $error_count++;
                        $this->logAddManyDriversError($request->file_id, 'Failed assigning manager to driver', $row_number, $row);
                        continue;
                    }

                }

            } catch (\Exception $e) {
                $error_count++;
                $this->logAddManyDriversError($request->file_id, $e->getMessage(), $row_number, $row);
                continue;
            }

            $insert_count++;
        }


        // generate a report in driver upload for the completed action


        // when insertion is complete update the status of the file
        \DB::table('add_many_drivers_files')
            ->where('id', $request->file_id)
            ->update([
                'applied' => 1,
                'insert_count' => $insert_count,
                'error_count' => $error_count,
            ]);

        return ['success' => 'Insertion complete'];

    }


    /**
     * This function is called to upload a csv file for adding predefined stops
     * It does NOT insert the predefined stops to the database.
     * It parses the file, validates the data, and returns errors, fileData, and a filename
     * to the front end
     *
     * It will make a folder in /public/docs/company_id/pre_defined_stops_upload if it does not exist
     * and add a file titled pre_defined_stops_Y_m_d.csv
     *
     * @param $company_id
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function uploadPreDefinedStops(Request $request, $company_id, $use_stop_contract)
    {
        /**
         * If there is no file simply return
         */
        if (!$request->file) {
            return;
        }

        /**
         * Get the path to storage and store the file
         */
        $storage_dir = '/public/docs/' . $company_id . '/pre_defined_stops_upload';

        /**
         * Make the appropriate directory if it doesn't exist
         */
        Storage::makeDirectory($storage_dir, 0775, true, true);

        /**
         * Upload the file, if we are doing more than one on the same day, replace the old
         */
        $filename = 'pre_defined_stops_' . date('Y_m_d') . '.csv';
        $request->file->storeAs($storage_dir . '/', $filename);
        $path_to_file = base_path() . '/storage/app' . $storage_dir . '/' . $filename;
        $data = $this->parsePreDefinedStopsFile($path_to_file, $use_stop_contract);

        try {
            $errors = null;
            $this->validatePreDefinedStops($data, $use_stop_contract, $company_id);
        } catch (\Exception $e) {
            $errors = json_decode($e->getMessage());

            /**
             * If there are errors, delete the file we don't want bad files cluttering the bunch
             */
            if (!empty($errors)) {
                unlink($path_to_file);
            }
        }

        /**
         * This is kind of ugly, bug it is to avoid vue dropzone's error catcher
         */
        return [
            'errors' => $errors,
            'fileData' => $data,
            'filename' => $filename
        ];
    }

    /**
     * Parses the data from a predefined stops file
     *
     * @param $path_to_file
     * @param $use_stop_contract
     * @return array
     */
    private function parsePreDefinedStopsFile($path_to_file, $use_stop_contract)
    {
        /**
         * Note as the fields are in the same position, and jerzy hates to duplicate code
         * we use the same headers (incorrectly) for us and canada. state represents province
         * and zip represents postal
         */
        if ($use_stop_contract == 1) {
            $headerArray = array("client_name", "address", "city", "state", "zip", "contract_1", "contract_2", "contract_3", "contract_4", "contract_5");
        } else {
            $headerArray = array("stop_num", "name", "division", "last_name", "first_name", "employee_num", "address", "city", "state", "zip", "mi_start_date");
        }

        $array = $this->readCsvToMapArray($path_to_file, $headerArray);

        return $array;
    }

    /**
     * Used in parsing the Pre-Defined Stops File (parsePreDefinedStopsFile())
     * @param $path_to_file
     * @param array $headerArray
     * @return array
     */
    private function readCsvToMapArray($path_to_file, array $headerArray)
    {
        $mapArray = array();
        $handle = fopen($path_to_file, 'r');
        while (!feof($handle)) {
            $rowMap = array();
            $rowArray = fgetcsv($handle);
            if (empty($rowArray)) continue; // skip the blank lines dude
            for ($index = 0; isset($headerArray[$index]); $index++) {
                // for some reason, not all the columns are present in all rows
                // we cannot expect them to be ,, in the csv file due to MSExcel, so do this
                if (isset($rowArray[$index])) {
                    $rowMap[$headerArray[$index]] = $rowArray[$index];
                } else {
                    $rowMap[$headerArray[$index]] = "";
                }
            }
            $mapArray[] = $rowMap;
        }
        fclose($handle);
        return $mapArray;
    }


    /**
     * Validate the predefined stops data.
     * NOTE! This data must have been parsed first by parsePredefinedStopsFile
     *
     * If errors are found, an exception is thrown with errors json_encoded in the message
     *
     * @param array $stops
     * @param $use_stop_contract
     * @param $company_id
     * @throws \Exception
     */
    private function validatePreDefinedStops($stops, $use_stop_contract, $company_id)
    {
        $errors = [];

        for ($i = 0; $i < sizeof($stops); $i++) {
            // skips the header if found
            if ($this->skipStop($stops[$i], $use_stop_contract)) {
                continue;
            }

            /**
             * Initialize all variables common to both contract stop uploads
             * and regular uploads
             */
            $row_errors = [];
            $stateProvince = StateProvince::where('short_name', $stops[$i]['state'])->first();

            // validate common fields
            try {
                $this->validateZipPostal($stateProvince->country_id, $stateProvince->id, $stops[$i]['zip']);
            } catch (\Exception $e) {
                $row_errors = array_merge($row_errors, (array)json_decode($e->getMessage(), true));
            }


            // below is the validation specific to contract use and non contract use
            if ($use_stop_contract == 1) {

                // check if contracts have 4 numbers
                for ($j = 1; $j <= 5; $j++) {
                    $contract_num = 'contract_' . $j;
                    if (!empty($stops[$i][$contract_num]) && !preg_match('/^\d{4}$/', $stops[$i][$contract_num])) {
                        $row_errors['contract_number'] = "Wrong number of digits in $contract_num";
                    }
                }

            } else { // non-contract

                // validate all mandatory fields
                $mandatory_fields = ['name', 'division', 'address', 'city', 'state', 'zip'];
                foreach ($mandatory_fields as $key) {
                    if (empty($stops[$i][$key])) {
                        $row_errors['mandatory_fields'] = title_case($key) . " is mandatory";
                    }
                }

                // validate divisions
                if (trim(strtolower($stops[$i]["division"])) != 'all') {
                    $divisions = explode(',', $stops[$i]['division']);

                    foreach ($divisions as $division_name) {
                        if (empty(Company::find($company_id)->divisions->filter(function ($value, $key) use ($division_name) {
                            return $value->name == $division_name;
                        })->all())) {
                            $row_errors['division'] = 'Invalid division';
                        }
                    }
                }

                /**
                 * If any criteria pointing to a user is present, we must be assigning the stop to a specific user
                 * else it is for divisions only
                 */
                if (!empty($stops[$i]["employee_num"]) || !empty($stops[$i]['first_name']) || !empty($stops[$i]['last_name'])) {

                    // first try to get the user from employee number
                    $user_id = $this->getUserIdFromEmployeeNumber($stops[$i]["employee_num"], $stops[$i]['first_name'], $stops[$i]['last_name'], $company_id);

                    // if that fails then division
                    if (empty($user_id)) {
                        $user_id = $this->getUserIdFromDivision($divisions[0], $stops[$i]['first_name'], $stops[$i]['last_name'], $company_id);
                    }

                    // if that fails, kill it.
                    if (empty($user_id)) {
                        $row_errors['unknown_driver'] = "Unknown Driver";
                        $errors[$i] = $row_errors;
                        continue;
                    }

                    // check if its a duplicate stop
                    if ($this->isDuplicateDriverStop($company_id, $user_id, $stops[$i])) {
                        $row_errors['duplicate'] = "Duplicated";
                    }

                    // check the date's validity
                    if (!empty($stops[$i]["mi_start_date"])) {
                        list($yyyy, $mm, $dd) = explode('-', $stops[$i]["mi_start_date"]);
                        if (checkdate($mm, $dd, $yyyy) === false) {
                            $row_errors['date'] = "Invalide Date";
                        }
                    }
                }

            }

            /**
             * if we have errors for the given row,
             * add them to the overall errors so we can return them to the front end keyed by
             * row number
             */
            if (!empty($row_errors)) {
                $errors[$i] = $row_errors;
            }
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }

    }

    /**
     * Returns a boolean.
     * If true, stop is a duplicate for the given user_id
     * Else it is not.
     *
     *
     * @param $company_id
     * @param $user_id
     * @param $stop
     */
    private function isDuplicateDriverStop($company_id, $user_id, $stop)
    {
        $state_province_id = StateProvince::where('short_name', $stop['state'])->first()->id;

        $savedStop = SavedStop::where('company_id', $company_id)
            ->where('name', $stop['name'])
            ->where('street', $stop['address'])
            ->where('city', $stop['city'])
            ->where('state_province_id', $state_province_id)
            ->where('zip_postal', $stop['zip'])
            ->first();

        if (!empty($savedStop)) {
            $driverStop = DriverStop::where('saved_stop_id', $savedStop->id)
                ->where('user_id', $user_id)
                ->first();
            if (!empty($driverStop)) {
                return true;
            }
        }

        return false;
    }


    /**
     * Given a filename, (of a file already uploaded and validated)
     * will execute all processes related to adding predefined stops
     *
     * Request must contain:
     *  use_stop_contract (bool)
     *  company_id (int)
     *  filename (string)
     *
     *
     * @param $data
     */
    public function addPreDefinedStopsFromFile(Request $request)
    {

        /**
         * If there is no filename simply return
         */
        if (!$request->filename) {
            return response('Filename missing', 400);
        }

        /**
         * Get the path to the file
         */
        $storage_dir = '/public/docs/' . $request->company_id . '/pre_defined_stops_upload';
        $path_to_file = base_path() . '/storage/app' . $storage_dir . '/' . $request->filename;

        try {
            // parse the file, it has already been validated
            $stops = $this->parsePreDefinedStopsFile($path_to_file, $request->use_stop_contract);
            $this->validatePreDefinedStops($stops, $request->use_stop_contract, $request->company_id);
        } catch (\Exception $e) {
            return [
                'errors' => json_decode($e->getMessage()),
            ];
        }

        /**
         * Variables to track our progress
         */
        $lastAddress = null;
        $badRecords = [];
        $added = 0;
        $updated = 0;
        $skipped = 0;
        $contracts_added = 0;
        $index = 0;

        // begin looping through the file
        for ($i = 0; $i < sizeof($stops); $i++) {

            // skips the header if found
            if ($this->skipStop($stops[$i], $request->use_stop_contract)) {
                continue;
            }

            /**
             * Initialize common variables
             */
            $stateProvince = StateProvince::where('short_name', $stops[$i]['state'])->first();
            $state_province_id = $stateProvince->id;
            $country_id = $stateProvince->country_id;
            $company_id = $request->company_id;

            // contract use
            if ($request->use_stop_contract == 1) {

                $existingStop = SavedStop::where('company_id', $request->company_id)
                    ->where('name', $stops[$i]['client_name'])
                    ->where('street', $stops[$i]['address'])
                    ->where('city', $stops[$i]['city'])
                    ->where('state_province_id', $state_province_id)
                    ->where('zip_postal', $stops[$i]['zip'])
                    ->first();

                if (empty($existingStop)) {

                    // get lng/lat from google
                    $fullAddress = $stops[$i]["address"] . ' ' . $stops[$i]["city"] . ', ' . $stops[$i]["state"];

                    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($fullAddress) . "&key=" . config('app.google_key');
                    $geocode = file_get_contents($url);
                    $output = json_decode($geocode);
                    $latitude = $output->results[0]->geometry->location->lat;
                    $longitude = $output->results[0]->geometry->location->lng;

                    if (!empty($latitude) && !empty($longitude)) {

                        $newStop = SavedStop::create([
                            'company_id' => $request->company_id,
                            'name' => $stops[$i]['client_name'],
                            'street' => $stops[$i]['address'],
                            'city' => $stops[$i]['city'],
                            'state_province_id' => $state_province_id,
                            'zip_postal' => $stops[$i]['zip'],
                            'country_id' => $country_id,
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                        ]);

                        $newStop->company_stop_id = $newStop->id;
                        $newStop->save();
                        $current_stop_id = $newStop->id;

                        $company = Company::find($request->company_id);

                        /**
                         * For now assign to first division
                         * TODO UPDATE THIS WHEN WE HAVE MORE DIVISIONS, COPY PRODUCTION FOR NOW
                         */
                        $division_ids[] = $company->divisions->first()->id;
                        $this->addDivisionToStop($current_stop_id, $division_ids);

                        // add all drivers to this stop
                        $user_ids_to_assign = $company->driverProfiles->pluck('user_id')->all();
                        $driverStops = [];
                        $today = date('Y-m-d');
                        foreach ($user_ids_to_assign as $id) {
                            $driverStops[] = [
                                'user_id' => $id,
                                'saved_stop_id' => $current_stop_id,
                                'effective_date' => $today
                            ];
                        }
                        DriverStop::insert($driverStops);

                        // add contracts
                        $contracts_added += $this->addContractToStop($company->id, $current_stop_id, 9400);
                        for ($j = 1; $j <= 5; $j++) {
                            $contract_num = 'contract_' . $j;
                            if (!empty($stops[$j][$contract_num])) {
                                $contracts_added += $this->addContractToStop($company->id, $current_stop_id, $stops[$j][$contract_num]);
                            }
                        }

                        $added++;
                    } else {
                        $current_stop_id = $existingStop->id;
                        $skipped++;
                        $badRecords[$index] = $stops[$i];
                        continue;
                    }

                } else {
                    $this->updateStopContract($existingStop->id, $request->company_id, $stops[$i]);
                    $updated++;
                }


                // non-contract stop client
            } else {

                // create a default start date if not present
                $start_date = empty($stops[$i]["mi_start_date"]) ? date('Y-m-d') : $stops[$i]["mi_start_date"];
                $division_ids = [];

                // get all of the divisions
                if (trim(strtolower($stops[$i]['division'])) != 'all') {
                    $divisions = explode(',', $stops[$i]['division']);

                    foreach ($divisions as $division_name) {
                        $division_ids[] = Company::find($request->company_id)->divisions->first(
                            function ($value, $key) use ($division_name) {
                                return $value->name == $division_name;
                            })->id;
                    }
                } else if (trim(strtolower($stops[$i]['division'])) == 'all') {
                    $division_ids = Company::find($request->company_id)->divisions->pluck('id')->all();
                }

                /**
                 * If any criteria pointing to a user is present, we must be assigning the stop to a specific user
                 * else it is for divisions only
                 */
                $user_id = null;
                if (!empty($stops[$i]["employee_num"]) || !empty($stops[$i]['first_name']) || !empty($stops[$i]['last_name'])) {

                    // first try to get the user from employee number
                    $user_id = $this->getUserIdFromEmployeeNumber($stops[$i]["employee_num"], $stops[$i]['first_name'], $stops[$i]['last_name'], $company_id);

                    // if that fails then division
                    if (empty($user_id)) {
                        $user_id = $this->getUserIdFromDivision($divisions[0], $stops[$i]['first_name'], $stops[$i]['last_name'], $company_id);
                    }
                    // if that fails, skip it.
                    if (empty($user_id)) {
                        $skipped++;
                        $badRecords[$index] = $stops[$i];
                        continue;
                    }

                }

                $existingStop = SavedStop::where('company_id', $request->company_id)
                    ->where('stop_number', $stops[$i]['stop_num'])
                    ->where('name', $stops[$i]['name'])
                    ->where('street', $stops[$i]['address'])
                    ->where('city', $stops[$i]['city'])
                    ->where('state_province_id', $state_province_id)
                    ->where('zip_postal', $stops[$i]['zip'])
                    ->first();

                /**
                 * If this is a new stop
                 */
                if (empty($existingStop)) {

                    // get lng/lat from google
                    $fullAddress = $stops[$i]["address"] . ' ' . $stops[$i]["city"] . ', ' . $stops[$i]["state"];

                    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($fullAddress) . "&key=" . config('app.google_key');
                    $geocode = file_get_contents($url);
                    $output = json_decode($geocode);
                    $latitude = $output->results[0]->geometry->location->lat;
                    $longitude = $output->results[0]->geometry->location->lng;

                    if (!empty($latitude) && !empty($longitude)) {

                        $newStop = SavedStop::create([
                            'company_id' => $request->company_id,
                            'name' => $stops[$i]['name'],
                            'stop_number' => $stops[$i]['stop_num'],
                            'street' => $stops[$i]['address'],
                            'city' => $stops[$i]['city'],
                            'state_province_id' => $state_province_id,
                            'zip_postal' => $stops[$i]['zip'],
                            'country_id' => $country_id,
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                        ]);

                        $newStop->company_stop_id = $newStop->id;
                        $newStop->save();
                        $current_stop_id = $newStop->id;
                        $this->addDivisionToStop($current_stop_id, $division_ids);
                        $added++;
                    } else {
                        $current_stop_id = $existingStop->id;
                        $skipped++;
                        $badRecords[$index] = $stops[$i];
                        continue;
                    }

                    // else this is an existing stop so we update it
                } else {
                    $this->updateDivisionStop($existingStop->id, $division_ids);
                    $updated++;
                }

                // if we had user information, add driver to the stop (we know its not a duplicate)
                if (!empty($user_id) && $current_stop_id > 0) {
                    $this->addDriverToStop($current_stop_id, $user_id, $start_date);
                }

            }
        }

        return [
            'badRecords' => $badRecords,
            'added' => $added,
            'updated' => $updated,
            'skipped' => $skipped,
            'contracts_added' => $contracts_added
        ];

    }


    /**
     * This function is called to update the saved_stop_company_contract table
     * when an existing contract has been put into the csv file
     *
     * @param $saved_stop_id
     * @param $company_id
     * @param $stop
     */
    public function updateStopContract($saved_stop_id, $company_id, $stop)
    {
        $today = date('Y-m-d');

        // stop exists, for each contract check if we have them in this stop
        $existing_contracts = \DB::table('saved_stop_company_contract')
            ->select('contract_id')
            ->where('saved_stop_id')
            ->get()
            ->all();

        for ($i = 1; $i <= 5; $i++) {
            $contract_num = 'contract_' . $i;
            if (!empty($stop[$contract_num]) && array_search($stop[$contract_num], $existing_contracts) === false) {
                $this->addContractToStop($company_id, $saved_stop_id, $stop[$contract_num]);
            }
        }

        $company = Company::find($company_id);

        // TODO currently assigning contract to all divisions like on production
        $all_division_ids = $company->divisions->pluck('id')->all();
        $this->updateDivisionStop($saved_stop_id, $all_division_ids);

        /**
         * Get all existing drivers for the company
         * assign them to the stop
         * Note that add driver stop won't add it if it exists
         */
        $driver_user_ids = $company->driverProfiles->pluck('user_id')->all();
        foreach ($driver_user_ids as $user_id) {
            $this->addDriverToStop($saved_stop_id, $user_id, $today);
        }
    }

    /**
     * This function will try to add records to saved_stop_company_contract
     * This is the pivot table linking contracts to stops
     * It will return the number of contracts added (1 or 0)
     *
     * @param $company_id
     * @param $saved_stop_id
     * @param $contract_id
     * @return int
     */
    public function addContractToStop($company_id, $saved_stop_id, $contract_id)
    {
        $contracts_added = 0;
        // check if we have such contract
        $contract = CompanyContract::where('contract_id', $contract_id)->first();

        if (empty($contract)) {
            CompanyContract::create([
                'contract_id' => $contract_id,
                'company_id' => $company_id,
                'note' => 'Uploaded from csv file',
            ]);
            $contracts_added++;
        }

        $stopContract = \DB::table('saved_stop_company_contract')
            ->where('saved_stop_id', $saved_stop_id)
            ->where('contract_id', $contract_id)
            ->first();

        if (empty($stopContract)) {
            \DB::table('saved_stop_company_contract')->insert([
                'saved_stop_id' => $saved_stop_id,
                'contract_id' => $contract_id
            ]);
        } else if (!empty($stopContract->deleted_at)) {
            \DB::table('saved_stop_company_contract')
                ->where('saved_stop_id', $saved_stop_id)
                ->where('contract_id', $contract_id)
                ->update([
                    'deleted_at' => null,
                ]);
        }

        return $contracts_added;
    }


    /**
     * Inserts records into the division_stop table
     * This is a new stop, so we can just create all division stop records
     *
     * TODO: SEE REW-1168
     * @param $saved_stop_id
     * @param $division_id
     */
    public function addDivisionToStop($saved_stop_id, $division_ids)
    {
        foreach ($division_ids as $id) {
            DivisionStop::create([
                'division_id' => $id,
                'saved_stop_id' => $saved_stop_id
            ]);
        }
    }

    /**
     * This is an existing stop.
     * Update it with any newly assigned division stop records
     *
     * @param $saved_stop_id
     * @param $division_ids
     */
    public function updateDivisionStop($saved_stop_id, $division_ids)
    {
        $old_division_ids = DivisionStop::where('saved_stop_id', $saved_stop_id)->get()->pluck('division_id')->all();

        $division_ids_to_insert = array_diff($division_ids, $old_division_ids);

        foreach ($division_ids_to_insert as $id) {
            DivisionStop::create([
                'division_id' => $id,
                'saved_stop_id' => $saved_stop_id
            ]);
        }

    }

    /**
     * Creates a driver_stop record for the given stop and user.
     * If the stop already exists, update the effective date (if this was empty it will be updated
     * to today and still be active)
     *
     * @param $saved_stop_id
     * @param $user_id
     * @param $start_date
     */
    public function addDriverToStop($saved_stop_id, $user_id, $start_date)
    {
        $existingDriverStop = DriverStop::where('user_id', $user_id)
            ->where('saved_stop_id', $saved_stop_id)
            ->first();

        if (!empty($existingDriverStop)) {
            $existingDriverStop->effective_date = $start_date;
            $existingDriverStop->save();
        } else {
            DriverStop::create([
                'user_id' => $user_id,
                'saved_stop_id' => $saved_stop_id,
                'effective_date' => $start_date,
            ]);
        }
    }


    /**
     * Given a stop, and a stop contract, determine if it is the header row of the csv
     * and skip it if it is.
     *
     * @param $stop
     * @param $use_stop_contract
     * @return bool
     */
    public function skipStop($stop, $use_stop_contract)
    {

        if ($use_stop_contract == 1) {
            // skip header if found
            if ($stop['client_name'] == 'Client Name' && $stop['address'] == 'Address' && $stop['city'] == 'City') {
                return true;
            }

        } else {
            // skip header if found
            if ($stop['stop_num'] == 'Stop Number (optional)' || $stop['name'] == 'Stop Name' && $stop['division'] == 'Division' && $stop['last_name'] == 'Driver Last Name') {
                return true;
            }
        }

        return false;

    }


    /**
     * All parameters required. Will return the user_id or null
     * @param $division_id
     * @param $first_name
     * @param $last_name
     * @param $company_id
     * @return mixed
     */
    public function getUserIdFromDivision($division_id, $first_name, $last_name, $company_id)
    {
        $user = \DB::table('users')
            ->select('users.id')
            ->join('driver_profiles', 'users.id', '=', 'driver_profiles.user_id')
            ->where('driver_profiles.company_id', $company_id)
            ->where('driver_profiles.division_id', $division_id)
            ->where('users.last_name', $last_name)
            ->where('users.first_name', $first_name)
            ->first();

        if (!empty($user)) {
            return $user->id;
        } else {
            return null;
        }
    }

    /**
     * All parameters required. Will return the user_id, or null
     * @param $employee_number
     * @param $first_name
     * @param $last_name
     * @param $company_id
     * @return mixed
     */
    public function getUserIdFromEmployeeNumber($employee_number, $first_name, $last_name, $company_id)
    {
        $user = \DB::table('users')
            ->select('users.id')
            ->join('driver_profiles', 'users.id', '=', 'driver_profiles.user_id')
            ->where('driver_profiles.company_id', $company_id)
            ->where('driver_profiles.employee_number', $employee_number)
            ->where('users.last_name', $last_name)
            ->where('users.first_name', $first_name)
            ->first();

        if (!empty($user)) {
            return $user->id;
        } else {
            return null;
        }

    }


    /**
     * For a given company, this will return the default insurance review date
     */
    public function getNextDefaultInsuranceReviewDate($company_id)
    {
        $company = Company::find($company_id);
        if (empty($company)) {
            throw new \Exception('Could not find company');
        }

        $next_insurance_review_date = null;
        if ($company->options->use_company_insurance_module == true) {
            if ($company->insurance->insurance_review_interval == 'Annually') {
                $next_insurance_review_date = $this->getNextOccurenceOfMonthAndDay(
                    $company->insurance->insurance_review_month,
                    $company->insurance->insurance_review_day
                );
            }
        }

        return $next_insurance_review_date;
    }

    private function getNextDefaultLicenseReviewDate($company_id)
    {
        $company = Company::find($company_id);
        if (empty($company)) {
            throw new \Exception('Could not find company');
        }

        $next_license_review_date = null;
        if ($company->options->use_company_license_module == true) {
            if ($company->license->license_review_interval == 'Annually') {
                $next_license_review_date = $this->getNextOccurenceOfMonthAndDay(
                    $company->license->license_review_month,
                    $company->license->license_review_day
                );
            }
        }

        return $next_license_review_date;

    }

    private function logAddManyDriversError($file_id, $error_message, $row_number, $row)
    {
        \DB::table('add_many_drivers_errors')
            ->insert([
                'file_id' => $file_id,
                'error' => $error_message,
                'row_number' => $row_number,
                'row_content' => json_encode($row)
            ]);

    }

    /**
     * This function will take all annual reviews for a given company,
     * move them to the tmp directory and return filenames
     *
     * @param $company_id
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getAnnualReviews($company_id)
    {
        try {
            $company = Company::find($company_id);
        } catch (\Exception $e) {
            return response(json_encode([$e->getLine() => $e->getMessage()]), 400);
        }

        $path = public_path('storage/docs/' . $company_id . '/annual_reviews/');
        if (!Storage::disk('upload_docs')->exists($company_id)) {
            Storage::disk('upload_docs')->makeDirectory($company_id, 0777, true, false);
        }
        if (!Storage::disk('upload_docs')->exists($company_id . '/annual_reviews')) {
            Storage::disk('upload_docs')->makeDirectory($company_id . '/annual_reviews', 0777, true, false);
        }
        $tmp_dir = public_path('storage/tmp/');

        $files = array_diff(scandir($path), array('..', '.'));

        $tmp_files = [];

        foreach ($files as $filename) {

            if (preg_match('/gitkeep/', $filename)) {
                continue;
            }

            $new_file_name = strtolower(str_replace(' ', '_', $company->name)) . '_' . $filename;
            copy($path . $filename, $tmp_dir . $new_file_name);
            $tmp_files[] = $new_file_name;
        }

        rsort($tmp_files);

        return ['files' => $tmp_files];
    }

    /**
     * Function to upload an annual review for a company
     *
     * @param Request $request
     * @param $company_id
     * @param $year
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|void
     */
    public function uploadAnnualReview(Request $request, $company_id, $year)
    {
        // for safety
        if (!Auth::user()->isSuper()) {
            return response(json_encode(['Unauthorized' => ['Only super users can access this route.']]), 401);
        }

        if (!$request->file || $company_id < 1 || $year < 1) {
            return;
        }

        if (!Storage::disk('upload_docs')->exists($company_id)) {
            Storage::disk('upload_docs')->makeDirectory($company_id, 0777, true, false);
        }
        if (!Storage::disk('upload_docs')->exists($company_id . '/annual_reviews')) {
            Storage::disk('upload_docs')->makeDirectory($company_id . '/annual_reviews', 0777, true, false);
        }
        //Storage::makeDirectory($storage_dir, 0775, true, true);

        $count = 0;
        do {

            $filename = 'annual_review_' . $year;

            if ($count > 0) {
                $filename = $filename . '(' . $count . ')';
            }

            $filename .= '.pdf';

            if (Storage::disk('upload_docs')->exists($company_id . '/annual_reviews/' . $filename)) {
                unset($filename);
            }
            $count++;
        } while (empty($filename));

        \Log::info('Upload Review', ['saving as' => $filename, 'company_id' => $company_id]);
        Storage::disk('upload_docs')->putFileAs($company_id . '/annual_reviews/', $request->file, $filename);
        //$request->file->storeAs($storage_dir, $filename);

        return ['filename' => $filename];
    }

    /**
     * Deletes an annual review
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAnnualReview(Request $request)
    {
        $filename = $request->filename;
        $company_id = $request->company_id;

        // for safety
        if (!Auth::user()->isSuper()) {
            return response(json_encode(['Unauthorized' => ['Only super users can access this route.']]), 401);
        }

        $year = '';
        $matches = [];
        // extract the year from the filename
        if (preg_match('/\d{4}/', $filename, $matches)) {
            $year = $matches[0];
        }

        $copy_number = '';
        $matches = [];
        // extract the copy number
        if (preg_match('/\([0-9]+\)/', $filename, $matches)) {
            $copy_number = $matches[0];
        }

        $path = base_path() . '/storage/app/public/docs/' . $company_id . '/annual_reviews/';

        $target_filename = 'annual_review_' . $year . $copy_number . '.pdf';

        if (file_exists($path . $target_filename)) {
            unlink($path . $target_filename);
        } else {
            abort(400, 'File not found');
        }

        return ['message' => 'successfully deleted ' . $filename];

    }

    public function getProcessingBank($company_id)
    {
        return ['processingBank' => Company::find($company_id)->bank(false)];
    }

    public function getCompanyFeeHistory($company_id, $fee = null)
    {
        $fees = Company::find($company_id)->allFees();
        if (empty($fee)) {
            return $fees;
        } else {
            return array_filter($fees, function ($f) use ($fee) {
                return $f->fee == $fee;
            });
        }
    }

    public function getDemoDrivers(Request $request)
    {
        $company_id = $request->company_id;
        $division_id = $request->division_id;

        if ($company_id > 0) {
            $company_filter = " AND c.id = $company_id ";
            $division_filter = $division_id > 0 ? " AND di.id = $division_id " : null;
        } else {
            $company_filter = null;
            $division_filter = null;
        }

        // drivers from Mi-Route companies before they start date
        // or
        // drivers from not Mi-Route companies before transition to Mi-Route and selected as demo drivers
        $query = <<<EOQ
SELECT c.name AS company, di.name AS division, u.last_name, u.first_name, dp.start_date AS end_of_demo, cm.mileage_entry_method AS entry_type
  FROM driver_profiles dp 
  JOIN companies c ON dp.company_id = c.id
  JOIN company_mileages cm ON cm.company_id = c.id AND cm.mileage_entry_method = 'Mi-Route'
  JOIN divisions di ON dp.division_id = di.id
  JOIN users u ON dp.user_id = u.id
  WHERE dp.deleted_at IS NULL AND dp.start_date > CURRENT_DATE() $company_filter $division_filter
UNION 
SELECT c.name AS company, di.name AS division, u.last_name, u.first_name, cm.miroute_transition_date AS end_of_demo, cm.mileage_entry_method AS entry_type
  FROM driver_profiles dp 
  JOIN companies c ON dp.company_id = c.id
  JOIN company_mileages cm ON cm.company_id = c.id AND cm.mileage_entry_method != 'Mi-Route' 
  JOIN divisions di ON dp.division_id = di.id
  JOIN users u ON dp.user_id = u.id   
  JOIN miroute_driver_options do ON do.user_id = dp.user_id AND do.access = 1 AND do.created_at <  CURRENT_DATE()
  WHERE dp.deleted_at IS NULL AND  cm.miroute_transition_date > CURRENT_DATE() $company_filter $division_filter
  ORDER BY 1, 3, 4
EOQ;
        $drivers = \DB::select(\DB::raw($query));
        $found = $drivers ? count($drivers) : 0;

        return ['rows' => $drivers, 'found' => $found];

    }

    public function getCompanyStartStopYears(Request $request)
    {
        $query = <<<EOQ
SELECT MIN(year) AS start_year, MAX(year) AS stop_year FROM monthly_reimbursements 
WHERE user_id IN ( SELECT user_Id FROM driver_profiles WHERE company_id = $request->company_id )
  UNION
SELECT MIN(year) AS start_year, MAX(year) AS stop_year FROM monthly_mileages 
WHERE user_id IN ( SELECT user_Id FROM driver_profiles WHERE company_id = $request->company_id )
 ORDER BY 1, 2 DESC
EOQ;
        $years = \DB::select(\DB::raw($query));
        \Debugbar::info($years);
        return ['company_start_year' => $years[0]->start_year ? $years[0]->start_year : date("Y-m-d"),
            'company_stop_year' => $years[0]->stop_year ? $years[0]->stop_year : date("Y-m-d")];

    }

    public function generateLoginHistoryExcel(Request $request)
    {

        $parameters = json_decode(json_encode($request->all()));

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        $row = 1;
        $column = ord('A');

        // header
        $worksheet->getCell(chr($column++) . $row)->setValue('Stop #');
        $worksheet->getCell(chr($column++) . $row)->setValue('Name');
        $worksheet->getCell(chr($column++) . $row)->setValue('Address');
        $worksheet->getCell(chr($column++) . $row)->setValue('City');
        $worksheet->getCell(chr($column++) . $row)->setValue('State');
        $worksheet->getCell(chr($column++) . $row)->setValue('Divisions');
        $worksheet->getCell(chr($column++) . $row)->setValue('Drivers');
        $worksheet->getCell(chr($column++) . $row)->setValue('Trips');
        $worksheet->getCell(chr($column++) . $row)->setValue('Active');

        // body
        foreach ($parameters->rows as $r) {
            $column = ord('A');
            $row++;
            $worksheet->getCell(chr($column++) . $row)->setValue($r->stop_number);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->name);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->street);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->city);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->state);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->divisions);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->driver_count);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->trips);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->deleted_at > 0 ? 'No' : 'Yes');
        }

        $column = ord('A');
        $row += 2;
        $worksheet->getCell("A$row")->setValue('Report Name:');
        $worksheet->getCell("B$row")->setValue('Predefined Stops');
        if ($parameters->company_id > 0) {
            $row++;
            $worksheet->getCell("A$row")->setValue('Company:');
            $worksheet->getCell("B$row")->setValue(Company::find($parameters->company_id)->name);
        }
        if ($parameters->division_id > 0) {
            $row++;
            $worksheet->getCell("A$row")->setValue('Division:');
            $division_name = $parameters->division_id > 0 ? Division::find($parameters->division_id)->name : "All";
            $worksheet->getCell("B$row")->setValue($division_name);
        }
        $row++;
        $worksheet->getCell("A$row")->setValue('Report Date:');
        $worksheet->getCell("B$row")->setValue(date("Y-m-d"));

        $tmp_dir = storage_path() . '/app/public/tmp/';
        $rand = rand(1000, 9999);
        $file_name = 'predefined_stops_' . date("Y-m-d_$rand") . '.xls';

        $writer = PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save($tmp_dir . $file_name);

        return ['file_name' => $file_name];

    }


    /**
     * This function is called from the super or administrator (pulte only) and generates a pulte
     * pay file based on the parameters listed in request.
     *
     * It will return the filename so that the user can download the file if they choose
     *
     * @param Request $request {
     *      year, month
     * }
     * @return array
     */
    public function generatePayFile(Request $request)
    {

        /**
         * Note in the query the company id is 57 PULTE!!
         */
        $reimbursements = \DB::select(\DB::raw(<<<EOQ
select 
(mr.fixed_reimbursement - mr.adjustment_amount) as fixed_reimbursement,
(mm.business_mileage * (mr.variable_reimbursement/100)) as variable_reimbursement,
mm.business_mileage,
dp.employee_number
from monthly_reimbursements mr
join driver_profiles dp on dp.user_id = mr.user_id
join monthly_mileages mm on mm.user_id = dp.user_id and mr.`year` = mm.`year` and mr.`month` = mm.`month` 
where dp.deleted_at is null and dp.company_id = 57 and mr.`year` = $request->year and mr.`month` = $request->month
order by dp.employee_number asc
EOQ
        ));

        /**
         * Generate the pay file
         * Open it up and put in the column headings
         */
        $filename = 'pay_file_' . $request->month . '_' . $request->year . '.csv';

        $file = fopen(storage_path() . '/app/public/tmp/' . $filename, 'w+');

        // column headings
        fputcsv($file, ['Employee ID', 'Non Taxable', 'Taxable', 'Total Reimbursement Amount']);


        /**
         * Initialize our totals, since we are looping, may as well calculate them!
         * We also initialze a tax controller instance.
         */
        $count = 0;
        $reimbursement_total = 0;
        $non_taxable_total = 0;
        $taxable_total = 0;

        $taxController = new TaxController();

        foreach ($reimbursements as $r) {
            $reimbursement = $r->fixed_reimbursement + round($r->variable_reimbursement, 2);

            /**
             * Calculate the amounts taxable and non taxable from the reimbursement
             */
            $amountsAfterTax = $taxController->calculateTaxableAmount463(
                $r->business_mileage,
                (int)$request->year,
                (int)$request->month,
                $reimbursement
            );
            $r->non_taxable = round($amountsAfterTax['non_taxable'], 2);
            $r->taxable = round($amountsAfterTax['taxable'], 2);

            /**
             * Write a new line to the file
             */
            fputcsv($file, [
                $r->employee_number,
                $r->non_taxable,
                $r->taxable,
                $reimbursement
            ]);

            /**
             * Adjust the totals accordingly
             */
            $reimbursement_total += $reimbursement;
            $non_taxable_total += $r->non_taxable;
            $taxable_total += $r->taxable;
            $count++;
        }

        /**
         * Construct the file data object
         */
        $fileData = new \stdClass();
        $fileData->reimbursements = $reimbursements;
        $fileData->totals = new \stdClass();
        $fileData->totals->reimbursement = round($reimbursement_total, 2);
        $fileData->totals->non_taxable = round($non_taxable_total, 2);
        $fileData->totals->taxable = round($taxable_total, 2);
        $fileData->reimbursement_count = $count;

        /**
         * Add the file trailer
         */
        fputcsv($file, [
            'TRAILER',
            $fileData->reimbursement_count,
            $fileData->totals->non_taxable,
            $fileData->totals->taxable,
            $fileData->totals->reimbursement
        ]);

        return [
            'filename' => $filename,
            'fileData' => $fileData
        ];
    }

    public function updateCompanyInvoicing(Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $invoicingData = (array)$request->all();

        try {
            $this->validateCompanyInvoicing($invoicingData['company_id'], $invoicingData);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        $fee = CompanyFee::where('company_id', $invoicingData['company_id'])
            ->where('fee', 'invoice')->
            where('to_date', null)->first();

        if ((empty($fee) && $invoicingData['per_driver_fee'] > 0)
            || (!empty($fee) && $fee->amount != $invoicingData['per_driver_fee'])) {
            $newFee = CompanyFee::create([
                'company_id' => $invoicingData['company_id'],
                'from_date' => $invoicingData['from_date'],
                'to_date' => null,
                'fee' => 'invoice',
                'amount' => $invoicingData['per_driver_fee']
            ]);
            if (!empty($fee)) {
                $fee->update(['to_date' => $invoicingData['from_date']]);
            }
        }

        CompanyInvoicing::where('company_id', $invoicingData['company_id'])
            ->update([
                'invoicing_interval' => $invoicingData['invoicing_interval'],
                'per_driver_fee' => $invoicingData['per_driver_fee']
            ]);
        return;
    }

    public function updateCompanyVehicles(Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $vehiclesData = (array)$request->all();

        // Nothing to validate (has to be correct, checkboxes and selects)
//        try {
//            $this->validateCompanyVehicles($vehiclesData['company_id'], $vehiclesData);
//        } catch (\Exception $e) {
//            return response($e->getMessage(), 400);
//        }

        // TODO: pdf file
        CompanyVehicle::where('company_id', $request->company_id)->first()
            ->update([
                'vehicle_responsibility' => $vehiclesData['vehicle_responsibility'],
                'enforce_vehicle_age' => $vehiclesData['enforce_vehicle_age'],
                'enforce_car_policy_acceptance' => $vehiclesData['enforce_car_policy_acceptance']
            ]);

        return;
    }

    /**
     * Function to update the company_taxes table
     */
    public function updateCompanyAdministration($company_id, Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $administrationData = (array)$request->administration;

        try {
            $this->validateCompanyAdministration($company_id, $administrationData);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        CompanyAdministration::where('company_id', $company_id)->first()
            ->update([
                'management' => $administrationData['management'],
                'driver_calls_and_emails' => $administrationData['driver_calls_and_emails'],
                'driver_changes' => $administrationData['driver_changes'],
                'account_maintenance' => $administrationData['account_maintenance'],
                'monthly_report_review' => $administrationData['monthly_report_review'],
                'traffic_light_status' => $administrationData['traffic_light_status'],
            ]);
        return;
    }

    public function updateCompanyPayments(Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        $paymentsData = (array)$request->all();

        try {
            $this->validateCompanyPayments($paymentsData['company_id'], $paymentsData);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        /*if ($paymentsData['payment_responsibility'] == 'CarData') {
            $bank = Bank::find($paymentsData['processingBank']['id']);
            $oldBank = CompanyBank::where('company_id', $company_id)
                ->first();

            if (empty($oldBank)) {
                $newBank = CompanyBank::create([
                    'company_id' => $company_id,
                    'bank_id' => $bank->id
                ]);
            } else if ($oldBank->bank_id != $bank->id) {
                $oldBank->delete();
                $newBank = CompanyBank::create([
                    'company_id' => $company_id,
                    'bank_id' => $bank->id
                ]);
            }
        }*/

        $driver_fee = CompanyFee::where('company_id', $paymentsData['company_id'])
            ->where('fee', 'driver')
            ->where('to_date', null)->first();
        if ((empty($driver_fee) && $paymentsData['direct_pay_driver_fee'] > 0)
            || (!empty($driver_fee) && $driver_fee->amount != $paymentsData['direct_pay_driver_fee'])) {
            $newDriverFee = CompanyFee::create([
                'company_id' => $paymentsData['company_id'],
                'from_date' => $paymentsData['driver_from_date'],
                'to_date' => null,
                'fee' => 'driver',
                'amount' => $paymentsData['direct_pay_driver_fee']
            ]);
            if (!empty($driver_fee)) {
                $driver_fee->update(['to_date' => $paymentsData['driver_from_date']]);
            }
        }

        $file_fee = CompanyFee::where('company_id', $paymentsData['company_id'])
            ->where('fee', 'file')
            ->where('to_date', null)->first();
        if ((empty($file_fee) && $paymentsData['direct_pay_file_fee'] > 0)
            || (!empty($file_fee) && $file_fee->amount != $paymentsData['direct_pay_file_fee'])) {
            $newFileFee = CompanyFee::create([
                'company_id' => $paymentsData['company_id'],
                'from_date' => $paymentsData['file_from_date'],
                'to_date' => null,
                'fee' => 'file',
                'amount' => $paymentsData['direct_pay_file_fee']
            ]);
            if (!empty($file_fee)) {
                $file_fee->update(['to_date' => $paymentsData['driver_from_date']]);
            }
        }

        CompanyPayments::where('company_id', $paymentsData['company_id'])
            ->update([
                'payment_responsibility' => $paymentsData['payment_responsibility'],
                'direct_pay_driver_fee' => $paymentsData['direct_pay_driver_fee'],
                'direct_pay_file_fee' => $paymentsData['direct_pay_file_fee']
            ]);
        return;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|void
     */
    public function updateCompanyDates(Request $request)
    {
        if (!auth()->user()->isSuper()) {
            return response('Unauthorized', 401);
        }

        // validation is not necessary since selects are in the range allowed for all dates

        CompanyDates::where('company_id', $request->company_id)->first()->update([
            'mileage_reminder_day' => $request->dates['mileage_reminder_day'],
            'second_mileage_reminder_day' => $request->dates['second_mileage_reminder_day'],
            'mileage_lock_driver_day' => $request->dates['mileage_lock_driver_day'],
            'mileage_lock_all_day' => $request->dates['mileage_lock_all_day'],
            'mileage_approval_end_day' => $request->dates['mileage_approval_end_day'],
            'pay_file_day' => $request->dates['pay_file_day'],
            'direct_pay_day' => $request->dates['direct_pay_day'],
            'no_reimbursement_notification_day' => $request->dates['no_reimbursement_notification_day']
        ]);

        return;
    }

    public function getCompanyNotes($company_id, $user_id = -1)
    {
        \Debugbar::info("Getting notes for company " . $company_id);
        //$company = Company::find($company_id);
        $notes = Note::where('company_id', $company_id)->with('user')->get();
        return ['notes'=>$notes];
    }

    public function getCompanyDates($company_id)
    {
        $dates = CompanyDates::where('company_id', $company_id)->first();
        return ['dates'=>$dates];
    }
}
