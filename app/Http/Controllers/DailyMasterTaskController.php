<?php

namespace App\Http\Controllers;

use App\Calculators\StreetLightCalculator;
// use http\Env\Request;
use App\Company;
use App\PersonalVehicle;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

use App\Mail\AdministratorApprovalReminderEmail;
use App\Mail\InsuranceExpiresIn20DaysEmail;
use App\Mail\InsuranceExpiresTodayEmail;
use App\Mail\ManagerApprovalReminderEmail;
use App\Mail\MileageReminderEmail;
use App\Mail\NoReimbursementReminderEmail;
use App\Mail\PaymentReminderEmail;
use App\Mail\GracePeriodExpiresEmail;

use Illuminate\Support\Facades\Mail;
use App\Http\Traits\CommonTraits;   // to log scheduled task run
use App\Http\Traits\CompanyTraits;
use App\Http\Traits\DriverTraits;
use App\Http\Traits\PulteTraits;

use App\CompanyMileage;
use App\DailyMileage;
use App\DailyTrip;
use App\Division;
use App\DriverInsuranceReview;
use App\DriverProfile;
use App\FuelCity;
use App\MonthlyMileage;
use Illuminate\Support\Facades\Storage;

// just for testing Pulte from Test page:
use Illuminate\Http\Request;
use App\DriverStreetLightStatus;

class DailyMasterTaskController extends Controller
{
    use CommonTraits;
    use CompanyTraits;
    use PulteTraits;
    use DriverTraits;
    private $street_light;
    private $companyController;
    protected $streetLightCalculator;

    public function __construct()
    {
        $this->middleware('auth');
        $this->street_light = new StreetLightCalculator();
        $this->companyController = new CompanyController();
        $this->streetLightCalculator = new StreetLightCalculator(); // for Pulte
    }

    public function stub($task_id, $task_name)
    {
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
    }

    /**
     * master_tasks id: 84
     *
     * @param $task_id
     * @param $task_name
     */
    public function adminApprovalReminder($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/adminApprovalReminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        $long_description = '';
        $emails_sent = 0;

        try {

            // dates to use
            $date = date('F d, Y');
            $day = date('d');
            $year = date('Y');
            $month = date('n');
            $previous_day = $day - 1;
            $previous_month = date("n") == 1 ? 12 : date("n") - 1;
            $previous_year = date("n") == 1 ? date("Y") - 1 : date("Y");
            $previous_month_name = date("F", mktime(0, 0, 0, $previous_month, 1, $previous_year));

            $company_query = <<<EOQ
SELECT c.id, c.name, cm.mileage_approval_end_day, cp.direct_pay_day, cp.received_date
FROM companies AS c
JOIN company_options AS co ON c.id = co.company_id
JOIN company_dates AS cm ON c.id = cm.company_id
JOIN company_payments AS cp ON c.id = cp.company_id
LEFT JOIN company_payment_dates AS cp ON c.id = cp.company_id AND year = $year AND month = $month
WHERE cm.mileage_approval_end_day = $previous_day AND c.deleted_at IS NULL
ORDER BY c.name
EOQ;

            $companies = \DB::select(\DB::raw($company_query));

            // quit if no companies
            if (count($companies) == 0) {
                $logger->addInfo($task_name, ['result' => 'No companies with admin reminders on ' . $date]);
                $this->scheduledTaskEnded($task_id, $log_id, 'Emails sent to 0 drivers', 'No companies with admin reminders on ' . $date, 'Completed');
                return;
            }

            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }

                $logger->addInfo($task_name, ['company' => $company->name]);

                $long_description .= "<b>$company->name</b> ";
                $coordinator = $this->getCoordinatorData($company->id);
                $from = $coordinator->email;

                if ($company->direct_pay_day) {
                    $logger->addInfo($task_name, ['direct_pay_day' => $company->direct_pay_day, 'received_date' => $company->received_date]);
                    list($y, $m, $d) = explode('-', $company->received_date);
                    $formatted_received_date = date("l, F jS, Y", mktime(0, 0, 0, $m, $d, $y));
                } else {
                    $formatted_received_date = null;
                }

                $dates = (object)[
                    'previous_month_name' => $previous_month_name,
                    'previous_year' => $previous_year,
                    'direct_pay_day' => $company->direct_pay_day,
                    'received_date' => $formatted_received_date
                ];


                // get active company approvers
                $admin_query = <<<EOQ
SELECT u.id, u.last_name, u.first_name, u.email
FROM users u 
JOIN administrator_profiles a ON u.id = a.user_id
WHERE a.company_id = $company->id AND a.deleted_at IS NULL AND ( a.company_approver = 1 OR a.is_limited = 1 )
ORDER BY u.last_name, u.first_name
EOQ;

                $administrators = \DB::select(\DB::raw($admin_query));

                foreach ($administrators as $administrator) {

                    $logger->addInfo($task_name, ['name' => $administrator->last_name, 'email' => $administrator->email]);

                    $data = (object)[
                        'from' => $from,
                        'user' => $administrator,
                        'company' => $company,
                        'coordinator' => $coordinator,
                        'dates' => $dates
                    ];

                    @Mail::to($administrator->email)->send(new AdministratorApprovalReminderEmail($data));
                    $emails_sent++;

                    sleep(2);
                    $long_description .= "$administrator->last_name $administrator->first_name, ";
                    // TODO remove sleep(2);

                }   // administrators

            }   // companies

            $short_description = "Emails sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent managers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    /**
     * master_tasks id: 110
     *
     * @param $task_id
     * @param $task_name
     */
    public function confirmNightlyJobs($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/confirmNightlyJobs.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $message = null;

        try {

            $from_date = date("Y-m-d 23:25:00", strtotime("-1 day"));
            $query = <<<EOQ
SELECT * FROM scheduled_tasks WHERE start_date > '{$from_date}' AND status != 'Completed' ORDER BY name
EOQ;
            $failed_tasks = \DB::select(\DB::raw($query));
            if (count($failed_tasks) > 0) {
                $message = "Following tasks failed: ";
                $sep = '';
                foreach ($failed_tasks as $task) {
                    $message .= "$sep{$task->name}";
                    $sep = ', ';
                }
            }

            $query = <<<EOQ
SELECT * FROM scheduled_tasks WHERE start_date > '{$from_date}' AND status = 'Completed' AND id IN ( 77, 104);
EOQ;
            $tasks = \DB::select(\DB::raw($query));
            foreach ($tasks as $task) {
                if ($task->id == 104) {    // fuel prices from OPIS, compare inserted records vs city counts

                    $cities_with_fuel = [1 => 0, 2 => 0];
                    $fuel_query = <<<EOQ
SELECT c.country_id, count(f.fuel_city_id) AS found FROM fuel_prices f, fuel_cities c 
WHERE f.fuel_city_id = c.id AND f.updated_at = CURRENT_DATE()
GROUP BY c.country
EOQ;
                    $fuel_found = false;
                    $fuels = \DB::select(\DB::raw($fuel_query));
                    foreach ($fuels as $fuel) {
                        $cities_with_fuel[$fuel->country_id] = $fuel->found;
                        $fuel_found = true;
                    }

                    if (!$fuel_found) {
                        $message .= ' No fuel prices found.';
                    } else {

                        $counts_query = <<<EOQ
SELECT country_id, COUNT(id) AS found FROM fuel_cities WHERE deleted_at IS NULL AND id != 423 GROUP BY country_id
EOQ;
                        $counts = \DB::select(\DB::raw($counts_query));
                        $cities = [1 => 0, 2 => 0];
                        foreach ($counts as $total) {
                            $cities[$total->contry_id] = $total->found;
                        }

                        if ($cities_with_fuel[1] < $cities[1]) {
                            $message .= "{$cities[1]} entries expected in US but only $cities_with_fuel[1] inserted. ";
                        }

                        if ($cities_with_fuel[2] < $cities[2]) {
                            $message .= "{$cities[1]} entries expected in Canada but only $cities_with_fuel[1] inserted. ";
                        }
                    }
                }
            }


            $logger->addInfo('Completed', ['task_id' => $task_id, 'task_name' => $task_name]);
            $this->scheduledTaskEnded($task_id, $log_id, 'Completed', $message);

        } catch (\Exception $e) {

            $message = "File: " . $e->getFile() . " Line: " . $e->getLine() . " Error: " . $e->getMessage();
            $logger->addInfo('Failed', ['task_id' => $task_id, 'task_name' => $task_name, 'error' => $message]);
            $this->scheduledTaskEnded($task_id, $log_id, "Failed", $message );

        }
    }

    /**
     * master_tasks id: 124
     *
     * @param $task_id
     * @param $task_name
     */
    public function dailyMileageCheck($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/dailyMileageCheck.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 139
     *
     * @param $task_id
     * @param $task_name
     * @throws \Exception
     */
    public function deleteDemoTrips($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/deleteDemoTrips.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        $long_description = '';
        $short_description = '';

        $yesterday = date('Y-m-d', strtotime('yesterday'));
        $last_company = '';
        $total = 0;
        $company_total = 0;
        $company_trips = 0;

        try {
            $company_drivers = $this->driversWithDemoTrips($yesterday);
            $logger->addInfo('In try block', ['company_drivers'=>$company_drivers]);

            if (!count($company_drivers)) {
                $logger->addInfo($task_name, ['result' => 'No drivers with demo trips']);
                $this->scheduledTaskEnded($task_id, $log_id, 'No demo trips', '', 'Completed');
                return;
            }
            foreach ($company_drivers as $row) {

                if ($last_company != $row->company_name) {

                    if (!empty($last_company)) {
                        $long_description .= " <b>($company_total/$company_trips)";
                    }

                    $last_company = $row->company_name;
                    if (!empty($long_description)) {
                        $long_description .= "<br>";
                    }
                    $long_description .= "<b>$row->company_name</b>";
                    $company_trips = 0;
                    $separator = ' ';

                    $trips_query = <<<EOQ
SELECT dt.id, COUNT(*) AS trips FROM daily_trips dt
JOIN daily_mileages dm ON dm.id = dt.daily_mileage_id
WHERE dm.user_id = $row->driver_id AND trip_date = '$yesterday'
GROUP BY 1
EOQ;
                    $trip_count = \DB::select(\DB::raw($trips_query))[0];
                    $deleted = DailyTrip::where('daily_mileage_id', $trip_count->id)->delete();
                    $total += $deleted;
                    $company_total++;
                    $long_description .= $separator . $row->last_name . ' ' . $row->first_name . " ($deleted)";
                    $separator = ', ';
                }
                $short_description = "$total DEMO trips were deleted";
                $logger->addInfo('Completed', ['short_description' => $short_description, 'long_description' => $long_description]);
                $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');

            }   // companies

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }

    }

    protected function driversWithDemoTrips($yesterday)
    {
        $query = <<<EOQ
SELECT c.id, c.name AS company_name, u.id AS user_id, u.last_name, u.first_name, dp.start_date
  FROM companies c
  JOIN company_mileages cm ON cm.company_id = c.id AND cm.deleted_at IS NULL AND cm.mileage_entry_method = 'Mi-Route'
  JOIN driver_profiles dp ON dp.company_id = c.id AND dp.deleted_at IS NULL AND dp.start_date > '$yesterday'
  JOIN users u ON u.id = dp.user_id
  WHERE c.deleted_at IS NULL AND EXISTS ( SELECT 1 FROM daily_trips dt, daily_mileages dm WHERE dt.daily_mileage_id = dm.id AND 
    dm.user_id = dp.user_id AND dm.trip_date = '$yesterday' )
  ORDER BY 2, 3, 4
EOQ;
        return \DB::select(\DB::raw($query));
    }

    /**
     * master_tasks id: 109
     *
     * This is the 20 day notice
     *
     * @param $task_id
     * @param $task_name
     */
    public function favrGraceReminder($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/favrGraceReminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, "Not Implemented", "");
    }

    /**
     * master_tasks id: 104
     *
     * @param $task_id
     * @param $task_name
     */
    public function getFuelPriceFromOpis($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        \Debugbar::info('getFuelPriceFromOpis started ....');
        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/getFuelPriceFromOpis.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        \Debugbar::info('logger created');

        $warnings = '';
        $us_total = 0;
        $ca_total = 0;
        $to = "lrundle@cardataconsultants.com; jdomsy@cardataconsultants.com; jkalat@cardataconsultants.com";
        $from = "support@cardataconsultants.com";
        $count = 0; // limit to 2 uploads only


        try {
            $host_opis = "ftp.opisnet.com";
            $un_opis = "rek151";
            $pw_opis = "cardata";

            $connection_opis = ftp_connect($host_opis);
            if (!$connection_opis) {
                \Debugbar::info('Cannot connect to OPIS FTP server');
                $logger->addInfo('Failed', ['reason' => 'Cannot connect to OPIS FTP server']);
                $short_description = "Cannot connect to OPIS FTP server'";
                $this->scheduledTaskEnded($task_id, $log_id, $short_description, null, 'Failed');
            }
            if (!ftp_login($connection_opis, $un_opis, $pw_opis)) {
                $short_description = "Cannot login to OPIS FTP server'";
                \Debugbar::info('Cannot login to OPIS FTP server');
                $logger->addInfo('Failed', ['reason' => $short_description]);
                $this->scheduledTaskEnded($task_id, $log_id, $short_description, null, 'Failed');
            }


            // get average prices from last month to check if we do not have any spikes
            $month = date("n");
            $year = date("Y");
            $last_month = $month == 1 ? 12 : $month - 1;
            $last_month_year = $month == 1 ? $year - 1 : $year;
            $beginning_of_last_month = sprintf("%04d-%02d-01", $last_month_year, $last_month);
            $beginning_of_this_month = sprintf("%04d-%02d-01", $year, $month);

            $prices_query = <<< EOQ
SELECT fuel_city_id, AVG(price) as last_price FROM fuel_prices WHERE updated_at >= '$beginning_of_last_month' AND updated_at < '$beginning_of_this_month'
GROUP BY fuel_city_id 
EOQ;
            $last_price = [];
            $prices = \DB::select(\DB::raw($prices_query));
            foreach ($prices as $price) {
                $last_price[$price->fuel_city_id] = $price->last_price;
            }

            // get total of fuel cities per country to check if we get fuel for all of them
            $cities_query = <<<EOQ
SELECT country_id, COUNT(id) AS totals FROM fuel_cities WHERE deleted_at IS NULL AND id != 423 GROUP BY country_id
EOQ;
            $cities = \DB::select(\DB::raw($cities_query));
            foreach ($cities as $city) {
                if ($city->country_id == USA) {
                    $us_city_total = $city->totals;
                } else {
                    $ca_city_total = $city->totals;
                }
            }

            // get fuel city id array based on lookup key
            $fuel_city_arr = [];
            $city_ids = FuelCity::select('id', 'lookup_key')->get();
            foreach ($city_ids as $city_id) {
                $fuel_city_arr[trim($city_id->lookup_key)] = $city_id->id;
            }

            ///
            ///
            //Storage::disk('fuel_prices')->put($new_filename, $output);
            ///

            $opis_files = ftp_nlist($connection_opis, ".");

            foreach ($opis_files as $file) {

                // $file = '20180607_margins.csv';  // for testing only
                // match date in front of filename
                preg_match('/^(\d{8})_(Canada|margins)/', $file, $match);

                if (!count($match)) {
                    continue;
                }

                $date = date_create_from_format('Ymd', $match[1]);
                $date_formatted = $date->format('Y-m-d');
                $date_yesterday_formatted = $date->modify('-1 day')->format('Y-m-d');
                $country_id = $match[2] == 'Canada' ? 2 : 1;


                $exist_query = <<<EOQ
SELECT fp.fuel_city_id FROM fuel_prices fp
JOIN fuel_cities fc ON fc.id = fp.fuel_city_id
WHERE fp.updated_at = '$date_formatted' AND fc.country_id = $country_id
LIMIT 1
EOQ;
                $exists = \DB::select(\DB::raw($exist_query));
                if (count($exists) > 0) {
                    $logger->addInfo('Skip this file as we have data', ['file' => $file]);
                    continue;   // we have this data already for this day and file
                }

                $save_file = public_path('storage/fuel_prices/'.$file);

                $result = ftp_get($connection_opis, $save_file, $file, FTP_ASCII);

                if (!$result) {
                    $logger->addInfo($task_name, ['Cannot save as a file: ' => $save_file]);
                    die();
                }
                //Storage::disk('fuel_prices')->put($file, $result);

                $logger->addInfo($task_name, ['File saved as : ' => $save_file]);

                $file_handle = fopen($save_file, "r");
                if ($file_handle) {

                    if ($country_id == USA) {
                        $num_lines = 0;
                        $late_count = 0;
                        while (!feof($file_handle)) {

                            $file_line = fgets($file_handle, 4096);
                            $file_city = trim(substr($file_line, 0, 32));
                            $file_date = substr($file_line, 32, 8);
                            $file_price = substr($file_line, 40, 1) . "." . substr($file_line, 41, 4);
                            $fuel_price_date = substr($file_date, 0, 4) . '-' . substr($file_date, 4, 2) . '-' . substr($file_date, 6, 2);

                            if (!empty($file_city)) {
                                $num_lines += 1;

                                $fuel_city_id = $fuel_city_arr[$file_city];

                                if ($fuel_city_id > 0) {

                                    \DB::table('fuel_prices')->insert(
                                        ['fuel_city_id' => $fuel_city_id, 'price' => $file_price, 'updated_at' => $fuel_price_date]);
                                    $us_total++;

                                    if (array_key_exists($fuel_city_id, $last_price) && $last_price[$fuel_city_id] > 0 && $file_price > 0 &&
                                        100 * abs($file_price - $last_price[$fuel_city_id]) / $last_price[$fuel_city_id] > 25) {
                                        $percentage = round(100 * ($file_price - $last_price[$fuel_city_id]) / $last_price[$fuel_city_id]);
                                        $warnings .= "Spike in US price in $file_city by {$percentage}%\n";
                                    }
                                    if ($fuel_price_date != $date_formatted) {
                                        $late_count++;
                                    }

                                } else {
                                    $warnings .= "Could not find id for US city $file_city";
                                }
                            }
                        }   // lines in a file

                        // check if late prices were added
                        if ($late_count) {
                            $warnings .= "$late_count prices were added from previous days\n";
                        }


                        // check if all cities got their fuel price
                        $missing_query = <<<EOQ
SELECT sp.name AS state,  fc.name AS city FROM fuel_cities fc
  JOIN state_provinces sp ON sp.id = fc.state_province_id
  LEFT JOIN fuel_prices fp ON fc.id = fp.fuel_city_id AND fp.updated_at = '$date_formatted'
  WHERE fc.country_id = 1 AND fc.deleted_at IS NULL AND fc.id != 423
  AND fp.price IS NULL
  ORDER BY fc.name
EOQ;
                        $missing_cities = \DB::select(\DB::raw($missing_query));
                        $first_time = true;
                        foreach ($missing_cities as $missing_city) {
                            if ($first_time) {
                                $warnings .= "Fuel price is missing in: ";
                                $first_time = false;
                            }
                            $warnings .= " $missing_city->city, ";
                        }
                        if (!$first_time) {
                            $warnings .= "\n";
                        }


                    } else {    // Canada
                        $num_lines = 0;
                        $late_count = 0;
                        while (!feof($file_handle)) {
                            $file_line = fgets($file_handle, 4096);
                            $tokens = explode(",", $file_line);
                            if (count($tokens) == 6) {
                                $file_city = trim($tokens[1]);
                                $file_date = $tokens[2];
                                $file_city = str_replace('"', '', $file_city);
                                $file_city = addslashes($file_city);
                                $file_province = trim($tokens[0]);
                                $file_province = str_replace('City - ', '', $file_province);
                                $file_province = str_replace('"', '', $file_province);
                                $file_price = trim($tokens[4]) * 100; // back compatible to Canadian cost provided in cents

                                list($mm, $dd, $yy) = explode('/', $file_date);
                                $fuel_price_date = sprintf("%04d-%02d-%02d", $yy, $mm, $dd);

                                $city_query = <<<EOQ
SELECT fc.id FROM fuel_cities fc 
  JOIN state_provinces sp ON fc.state_province_id = sp.id AND sp.short_name = '$file_province'
  WHERE fc.country_id = 2 AND fc.deleted_at IS NULL AND fc.lookup_key = '$file_city'
EOQ;
                                $cities = \DB::select(\DB::raw($city_query));
                                if ($cities) {
                                    $fuel_city_id = $cities[0]->id;
                                    \DB::table('fuel_prices')->insert(
                                        ['fuel_city_id' => $fuel_city_id, 'price' => $file_price, 'updated_at' => $fuel_price_date]);
                                    $ca_total++;

                                    if (array_key_exists($fuel_city_id, $last_price) && $last_price[$fuel_city_id] > 0 && $file_price > 0 &&
                                        100 * abs($file_price - $last_price[$fuel_city_id]) / $last_price[$fuel_city_id] > 25) {
                                        $percentage = round(100 * ($file_price - $last_price[$fuel_city_id]) / $last_price[$fuel_city_id]);
                                        $warnings .= "Spike in CA price in $file_city by {$percentage}%\n";
                                    }
                                    if ($fuel_price_date != $date_formatted) {
                                        $late_count++;
                                    }

                                }
                            }   // proper csv line

                            // check if late prices were added
                            if ($late_count) {
                                $warnings .= "$late_count prices were added from previous days\n";
                            }
                        }   // file handler

                        // check if all cities got their fuel price
                        $missing_query = <<<EOQ
SELECT sp.name AS state,  fc.name AS city FROM fuel_cities fc
  JOIN state_provinces sp ON sp.id = fc.state_province_id
  LEFT JOIN fuel_prices fp ON fc.id = fp.fuel_city_id AND fp.updated_at = '$date_formatted'
  WHERE fc.country_id = 2 AND fc.deleted_at IS NULL
  AND fp.price IS NULL
  ORDER BY fc.name
EOQ;
                        $missing_cities = \DB::select(\DB::raw($missing_query));
                        $first_time = true;
                        foreach ($missing_cities as $missing_city) {
                            if ($first_time) {
                                $warnings .= "Fuel price is missing in: ";
                                $first_time = false;
                            }
                            $warnings .= " $missing_city->city, ";
                        }
                        if (!$first_time) {
                            $warnings .= "\n";
                        }

                    }   // Canada
                }   // $file_handle

                $logger->addInfo($task_name, ['warnings' => $warnings, 'us_total' => $us_total, 'ca_total' => $ca_total]);

                fclose($file_handle);
                unlink($save_file);


                $logger->addInfo($task_name, ['process file' => $file]);
                if ($count++ > 4) {
                    die();
                }


            }   // for each OPIS file from directory
            $this->scheduledTaskEnded($task_id, $log_id, "Fuel Prices Successful", "");

        } catch (\Exception $e) {
            \Debugbar::error($e->getMessage());
            $logger->addInfo('Failed', ['jk' => '', 'error' => $e->getMessage()]);
            $this->scheduledTaskEnded($task_id, $log_id, null, $e->getMessage(), 'Failed');
        }

    }

    /**
     * master_tasks id: 80
     *
     * @param $task_id
     * @param $task_name
     */
    public function gracePeriodExpires($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/gracePeriodExpires.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        $long_description = '';
        $emails_sent = 0;

        try {

            $new_driver = YELLOW_NEW_DRIVER;
            $terminated_driver = RED_TERMINATED;
            $no_reimbursement_for_driver = RED_NO_REIMBURSEMENT;
            $green_reimbursement = GREEN_REIMBURSEMENT;

            // dates to use
            $date = date('Y-m-d');
            $day = date('d');
            $year = date('Y');
            $month = date('n');
            $bom = sprintf("%04d-%02d-01", $year, $month);
            $previous_month = date("n") == 1 ? 12 : date("n") - 1;
            $previous_year = date("n") == 1 ? date("Y") - 1 : date("Y");
            $previous_month_name = date("F", mktime(0, 0, 0, $previous_month, 1, $previous_year));
            $circle = ['1.  Your Name', '2.  Make, Model and Year of Vehicle', '3.  Policy Term, ' . "'" . 'From' . "'" . ' - ' . "'" . 'To' . "'" . ' Dates'];


            // select companies with grace periods
            $company_query = <<<EOQ
SELECT c.id, c.name, ci.insurance_fax_number, ci.initial_insurance_grace_end_date, ci.new_driver_insurance_grace_days,
  cd.mileage_lock_all_day, co.service_plan, ci.remove_reimbursement_without_insurance, cp.country_id
FROM companies c
JOIN company_insurances AS ci ON ci.company_id = c.id AND ci.deleted_at IS NULL
JOIN company_dates AS cd ON cd.company_id = c.id AND cd.deleted_at IS NULL
JOIN company_options AS co ON co.company_id = c.id
JOIN company_profiles AS cp ON cp.company_id = c.id
WHERE c.deleted_at IS NULL AND ci.insurance_responsibility IS NOT NULL 
AND ( ci.initial_insurance_grace_end_date IS NOT NULL || ci.new_driver_insurance_grace_days > 0 )
ORDER BY c.name           
EOQ;

            $companies = \DB::select(\DB::raw($company_query));

            // quit if no companies
            if (count($companies) == 0) {
                $logger->addInfo($task_name, ['result' => 'No companies with insurance grace ' . $date]);
                $short_description = 'No companies with insurance grace ' . $date;
                $this->scheduledTaskEnded($task_id, $log_id, $short_description, null, 'Completed');
                $logger->addInfo($task_name, ['short_description' => $short_description]);
                return;
            }

            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }

                $logger->addInfo($task_name, ['company' => $company->name]);

                $long_description .= "<b>$company->name</b> ";
                $coordinator = $this->getCoordinatorData($company->id);
                $from = $coordinator->email;

                $driver_query = <<<EOQ
SELECT u.id AS user_id, u.last_name, u.first_name, u.email, dp.street_light_id, dp.start_date,
    pv.created_at AS vehicle_date, vp.max_vehicle_age, pv.year
FROM users AS u 
JOIN driver_profiles AS dp ON dp.user_id = u.id AND dp.start_date <= '$bom' AND ( dp.stop_date IS NULL OR dp.stop_date > $date )
JOIN personal_vehicles pv ON pv.user_id = u.id AND pv.deleted_at IS NULL
JOIN vehicle_profiles vp ON dp.vehicle_profile_id = vp.id
WHERE dp.company_id = {$company->id}
AND dp.street_light_id & $new_driver = 0
AND dp.street_light_id & $terminated_driver = 0
AND dp.street_light_id & $no_reimbursement_for_driver = 0
AND dp.street_light_id > $green_reimbursement
ORDER BY u.last_name, u.first_name
EOQ;
                $drivers = \DB::select(\DB::raw($driver_query));

                foreach ($drivers as $driver) {

                    $send_insurance_warning = $driver->street_light_id & GREEN_NO_INSURANCE_COMPLIANCE ||
                        $driver->street_light_id & RED_AWAITING_INSURANCE ||
                        $driver->street_light_id & RED_INSUFFICIENT_INSURANCE ||
                        $driver->street_light_id & RED_INSURANCE_EXPIRED ? true : false;
                    $send_vehicle_age_warning = ((int) ($driver->street_light_id & GREEN_NO_VEHICLE_AGE_COMPLIANCE) > 0 ? true : false );
                    $send_vehicle_cost_warning = ((int) ($driver->street_light_id & GREEN_NO_VEHICLE_COST_COMPLIANCE) > 0 ? true : false );

                    $grace_data = $this->ifGraceExpires( $company, $driver, $logger);

                    if( $grace_data->in_grace) {
                        \Debugbar::info("$driver->user_id $company->name $driver->last_name $driver->first_name $grace_data->grace_end_date $driver->street_light_id I=$send_insurance_warning A=$send_vehicle_age_warning C:$send_vehicle_cost_warning");
                    }

                    // special case for January. We cleared FAVR insurance but insurance was provided in last December
                    // for new vehicles
                    if( $company->service_plan == PLAN_FAVR && $send_insurance_warning && date('n') == 1 ) {
                        // find it there is December not rejected insurance file. If found, ignore this warning
                        $last_year = date("Y")-1;
                        $file_name = $driver->user_id .'-' . $last_year . '-12';
                        $insurance_query = <<<EOQ
SELECT approved FROM driver_insurance_documents WHERE filename like '$file_name%' ORDER BY created_at DESC LIMIT 1
EOQ;
                        $insurance = \DB::select(\DB::raw($insurance_query));
                        if( $insurance && $insurance->approved == 1) {  // we got insurance already in December
                            $send_insurance_warning = 0;
                        }
                    }

                    if( true || $grace_data->grace_end_date == $date && // TODO remove true or
                        ( $send_insurance_warning || $send_vehicle_age_warning || $send_vehicle_cost_warning ) ) {

                        $street_light_names = $this->streetLightTitles($driver->street_light_id);
                        \Debugbar::info('street_light_names',$street_light_names);

                        if( $send_insurance_warning ) {
                            $insurance_amounts = $this->getCompanyInsuranceAmount($company->id, $company->country_id);
                            $color = $company->remove_reimbursement_without_insurance ? 'Red' : 'Green';
                            $business_use_required = $this->getCompanyBusinessUse($company->id);
                        } else {
                            $insurance_amounts = null;
                            $color = null;
                            $business_use_required = null;
                        }

                        if( $send_vehicle_age_warning || $send_vehicle_cost_warning ) {
                            $driverProfile = DriverProfile::where('user_id',$driver->user_id)->first();
                            $vehicleFavrParameters = $driverProfile->vehicleFavrParameters();
                        } else {
                            $min_vehicle_age = null;
                            $vehicleFavrParameters = null;
                        }

                        $data = (object)[
                            'from' => $from,
                            'grace_data' => $grace_data,
                            'service_plan' => $company->service_plan,
                            'company' => $company,
                            'driver' => $driver,
                            'coordinator' => $coordinator,
                            'insurance_amounts' => $insurance_amounts,
                            'insurance_warning' => $send_insurance_warning,
                            'vehicle_age_warning' => $send_vehicle_age_warning,
                            'vehicle_cost_warning' => $send_vehicle_cost_warning,
                            'color' => $color,
                            'street_light_names' => $street_light_names,
                            'circle' => $circle,
                            'favr' => $vehicleFavrParameters
                        ];

                        \Debugbar::info('Calling Mail with data:');
                        \Debugbar::info($data);
                        @Mail::to($driver->email)->send(new GracePeriodExpiresEmail($data));
                        $emails_sent++;

                        sleep(2); // TODO remove sleep(2);
                        $long_description .= "$driver->last_name $driver->first_name, ";
                    }
                }   // drivers

            }   // companies

            $short_description = "Emails sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => "Line " . $e->getLine(), 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    private function ifGraceExpires( $company, $driver, $logger, $today=null){

        if (!$today) {
            $today = date("Y-m-d");
        }
        $logger->addInfo('ifGraceExpires XX', ["testing grace for driver $driver->last_name $driver->first_name for date $today"]);
        // \Debugbar::info("ifGraceExpires: testing grace for driver $driver->last_name $driver->first_name for date $today");

        // default values
        $in_grace = false;
        $grace_end_date = null;
        $vehicle_expire_on = null;
        $driver_expire_on = null;

        try {

            if ($company->new_driver_insurance_grace_days && $today <= $company->new_driver_insurance_grace_days) {
                \Debugbar::info("In grace, before new_driver_insurance_grace_days");
                return (object)['in_grace' => true, 'grace_end_date' => $company->new_driver_insurance_grace_days];
            }

            if ($company->new_driver_insurance_grace_days > 0) {

                // new vehicle expire date
                // \Debugbar::info("Driver vehicle date: $driver->vehicle_date");
                if ($driver->vehicle_date) {

                    $vehicle_date = substr($driver->vehicle_date,0,10);

                    $date = date_create($vehicle_date);
                    date_add($date,date_interval_create_from_date_string("$company->new_driver_insurance_grace_days days"));
                    $vehicle_expire_on = date_format($date,"Y-m-d");

                    // \Debugbar::info("Vehicle expire on $vehicle_expire_on");


                    // \Debugbar::info("Driver Start date: $driver->start_date");
                    $date = date_create($driver->start_date);
                    $driver_expire_on = date_format($date,"Y-m-t");
                    // \Debugbar::info("Driver Expire on: $driver_expire_on");


                    $logger->addInfo('ifGraceExpires', ['driver_expire' => $driver_expire_on, 'vehicle_expire' => $vehicle_expire_on]);
                    // \Debugbar::info("Drivers expire on $driver_expire_on vehicle expire on $vehicle_expire_on");


                    list($v_yyyy, $v_mm, $v_dd) = explode('-', $driver->vehicle_date);
                    if ($v_dd <= $company->mileage_lock_all_day && $company->id == 54) {
                        $vehicle_expire_on = date("Y-m-t"); // last day of current month, because they are paying in advance
                    } else {    // next month at lock date
                        if ($v_mm == 12) {
                            $v_yyyy++;
                            $v_mm = 1;
                        } else {
                            $v_mm++;
                        }
                        $vehicle_expire_on = sprintf("%04d-%02d-%02d", $v_yyyy, $v_mm, $company->mileage_lock_all_day);
                    }

                    // \Debugbar::info("Compare T: $today with D: $driver_expire_on V: $vehicle_expire_on");
                    // \Debugbar::info("Comparing results " . strcmp($today, $driver_expire_on) . ' ' . strcmp($today, $vehicle_expire_on));

                    if (strcmp($today, $driver_expire_on) <= 0 || strcmp($today, $vehicle_expire_on) <= 0 ) {
                        // how to recognize new driver? On production we have count of vehicles, but is it true?
                        $in_grace = true;
                        // \Debugbar::info("Compare T: $today with D: $driver_expire_on V: $vehicle_expire_on");
                        // \Debugbar::info("We are in grace?");
                        if (count(PersonalVehicle::where('user_id', $driver->user_id)->withTrashed()->get()) > 1 &&
                            strcmp($today, $vehicle_expire_on)) {
                            $logger->addInfo('ifGraceExpires', ['old_driver' => $vehicle_expire_on]);
                            $grace_end_date = $vehicle_expire_on;
                        } else {
                            $grace_end_date = $driver_expire_on;
                            $logger->addInfo('ifGraceExpires', ['new_driver' => $driver_expire_on]);
                        }
                    } else {
                        $logger->addInfo('ifGraceExpires', ['out of grace period']);
                    }
                }
            }

        } catch ( Exception $e) {
            \Debugbar::error("Error: f=" . $e->getFile() . " l=" . $e->getLine() . " e=" . $e->getMessage());
        }

        return (object)['in_grace' => $in_grace, 'grace_end_date' => $grace_end_date];

    }

    /**
     * master_tasks id: 78
     *
     * @param $task_id
     * @param $task_name
     */
    public function insuranceExpireIn20Days($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/insuranceExpireIn20Days.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        $long_description = '';
        $emails_sent = 0;
        $date = date("Y-m-d");
        $circle = ['1.  Your Name', '2.  Make, Model and Year of Vehicle', '3.  Policy Term, ' . "'" . 'From' . "'" . ' - ' . "'" . 'To' . "'" . ' Dates'];

        try {

            $terminated_driver = RED_TERMINATED;
            $green_no_insurance_compliance = GREEN_NO_INSURANCE_COMPLIANCE;

            // select companies with insurance
            $company_query = <<<EOQ
SELECT c.id, c.name, ci.insurance_fax_number, ci.remove_reimbursement_without_insurance, co.service_plan, cp.country_id
FROM companies c
JOIN company_insurances AS ci ON ci.company_id = c.id AND ci.deleted_at IS NULL
JOIN company_options AS co ON co.company_id = c.id AND co.deleted_at IS NULL
JOIN company_profiles AS cp ON cp.company_id = c.id AND cp.deleted_at IS NULL
WHERE c.deleted_at IS NULL AND ci.insurance_responsibility IS NOT NULL 
AND ci.notify_insurance_expiry = 1
AND co.use_insurance_dates = 1
ORDER BY c.name       
EOQ;

            $companies = \DB::select(\DB::raw($company_query));

            // quit if no companies
            if (count($companies) == 0) {
                $logger->addInfo($task_name, ['result' => 'No companies with insurance grace ' . $date]);
                $this->scheduledTaskEnded($log_id, 'Emails sent to 0 drivers', null, 'Completed');
                return;
            }

            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $logger->addInfo($task_name, ['company' => $company->name, 'country_id' => $company->country_id]);

                $long_description .= "<b>$company->name</b> ";
                $coordinator = $this->getCoordinatorData($company->id);
                $from = $coordinator->email;
                $insurance_amounts = $this->getCompanyInsuranceAmount($company->id, $company->country_id);
                //$logger->addInfo($task_name, ['insurance_amounts' => var_dump($insurance_amounts)]);

                $driver_query = <<<EOQ
SELECT u.id, u.last_name, u.first_name, u.email, dp.street_light_id
FROM users AS u 
JOIN driver_profiles AS dp ON dp.user_id = u.id AND dp.deleted_at IS NULL
JOIN driver_insurance_reviews i ON u.id = i.user_id
WHERE dp.company_id = {$company->id}
AND i.expiration_date = DATE_ADD(current_date(), INTERVAL 20 DAY)
AND dp.street_light_id & $terminated_driver = 0
AND dp.street_light_id & $green_no_insurance_compliance = 0
ORDER BY u.last_name, u.first_name
EOQ;
                $drivers = \DB::select(\DB::raw($driver_query));

                foreach ($drivers as $driver) {

                    $logger->addInfo($task_name, ['driver' => $driver->last_name . ' ' . $driver->first_name]);

                    if ($company->service_plan == 'FAVR' && $company->remove_reimbursement_without_insurance == 0) {
                        $new_street_light = $this->addToStreetLight($driver->street_light_id, GREEN_NO_INSURANCE_COMPLIANCE);
                    } else {
                        $new_street_light = $this->addToStreetLight($driver->street_light_id, RED_INSURANCE_EXPIRED);
                    }

                    $data = (object)[
                        'from' => $from,
                        'company' => $company,
                        'driver' => $driver,
                        'circle' => $circle,
                        'insurance_amounts' => $insurance_amounts,
                        'coordinator' => $coordinator,
                        'new_street_light' => $new_street_light
                    ];

                    @Mail::to($driver->email)->send(new InsuranceExpiresIn20DaysEmail($data));
                    $emails_sent++;
                    sleep(2);

                    $long_description .= "$driver->last_name $driver->first_name, ";

                }

            }

            $short_description = "Emails sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }

    }

    /**
     * master_tasks id: 88
     *
     * This scheduled task checks to see which drivers have expired insurance today.
     * It does this based on the driver profile's next_insurance_review_date
     * There are two scenarios to be aware of:
     *  1. Driver has not uploaded new documentation, since the reminder was set out.
     *          In this scenario we simply set the drivers street light to red insurance expired,
     *          or green no insurance compliance,
     *          and we set the insurance_review_id on the driver profile to null
     *  2. Driver has uploaded new documentation
     *          In this scenario, if we hit the scheduled task, it means the new documentation
     *          the driver has uploaded either has not been processed or was rejected. (If a new package is approved
     *          it will immediately take the spot of the old package).
     *          If the new package has not been processed, the driver will go to RED INSURANCE AWAITING PROCESSING (FAVR - GREEN INSURANCE AWAITING PROCESSING).
     *          If the new package has been processed, the driver will go to red insufficient insurance (FAVR - green no insurance compliance).
     *          We will also update the driver profile with the new insurance review id, and the next insurance
     *          review date to the company default
     *
     * Once the driver profile and active package has been taken care of,
     *
     * @param $task_id
     * @param $task_name
     * @throws \Exception
     */
    public function insuranceExpiryNotice($task_id, $task_name)
    {

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/insuranceExpiryNotice.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        $long_description = '';
        $emails_sent = 0;
        $date = date("Y-m-d");

        $circle = ['1.  Your Name', '2.  Make, Model and Year of Vehicle', '3.  Policy Term, ' . "'" . 'From' . "'" . ' - ' . "'" . 'To' . "'" . ' Dates'];

        try {

            $terminated_driver = RED_TERMINATED;
            $green_no_insurance_compliance = GREEN_NO_INSURANCE_COMPLIANCE;

            // select companies with insurance
            $company_query = <<<EOQ
SELECT c.id, c.name, ci.insurance_fax_number, ci.remove_reimbursement_without_insurance, co.service_plan, cp.country_id
FROM companies c
JOIN company_insurances AS ci ON ci.company_id = c.id
JOIN company_options AS co ON co.company_id = c.id
JOIN company_profiles AS cp ON cp.company_id = c.id
WHERE c.deleted_at IS NULL AND ci.insurance_responsibility IS NOT NULL 
ORDER BY c.name           
EOQ;

            $companies = \DB::select(\DB::raw($company_query));

            // quit if no companies
            if (count($companies) == 0) {
                $logger->addInfo($task_name, ['result' => 'No expired insurance found on ' . $date]);
                $this->scheduledTaskEnded($task_id, $log_id, 'Email sent to 0 drivers', 'No companies with insurance expiry on ' . $date, 'Completed');
                return;
            }

            foreach ($companies as $company) {

                $next_company_default_review_date = $this->companyController->getNextDefaultInsuranceReviewDate($company->id);
                $driver_found = false;
                $coordinator = $this->getCoordinatorData($company->id, 'insurance');
                $from = $coordinator->email;
                $insurance_amounts = $this->getCompanyInsuranceAmount($company->id, $company->country_id);
                $logger->addInfo($task_name, ['company' => $company->name]);

                $driver_query = <<<EOQ
SELECT u.id, u.last_name, u.first_name, u.email, dp.street_light_id, dp.insurance_review_id
FROM users AS u 
JOIN driver_profiles AS dp ON dp.user_id = u.id AND dp.deleted_at IS NULL AND dp.next_insurance_review_date = '$date'
JOIN driver_insurance_reviews dir on dp.insurance_review_id = dir.id
WHERE dp.company_id = {$company->id}
AND dp.street_light_id & $terminated_driver = 0
AND dp.street_light_id & $green_no_insurance_compliance = 0
ORDER BY u.last_name, u.first_name
EOQ;
                $drivers = \DB::select(\DB::raw($driver_query));

                foreach ($drivers as $driver) {

                    $currentInsuranceReivew = DriverInsuranceReview::find($driver->insurance_review_id);

                    /**
                     * Check to see if the driver has an insurance review which was created after the present review
                     * and then follow the cases as per this functions comment
                     */
                    $reviewInQueue = DriverInsuranceReview::where('user_id', $driver->id)
                        ->where('created_at', '>', $currentInsuranceReivew->created_at)
                        ->first();

                    /**
                     * If the driver has an insurance in queue, it is either awaiting processing or rejected and we
                     * update the status appropriately
                     * If the driver has no insurance in queue, they go into red insurance expired or green no insurance
                     * compliance
                     */
                    if (!empty($reviewInQueue)) {

                        // rejected
                        if ($reviewInQueue->approved == 0) {
                            if ($company->service_plan == 'FAVR' && $company->remove_reimbursement_without_insurance == 0) {
                                $new_street_light = $this->addToStreetLight($driver->street_light_id, GREEN_NO_INSURANCE_COMPLIANCE);
                            } else {
                                $new_street_light = $this->addToStreetLight($driver->street_light_id, RED_INSUFFICIENT_INSURANCE);
                            }
                        } else {
                            // awaiting processing
                            if ($company->service_plan == 'FAVR' && $company->remove_reimbursement_without_insurance == 0) {
                                $new_street_light = $this->addToStreetLight($driver->street_light_id, GREEN_INSURANCE_AWAITING_PROCESSING);
                            } else {
                                $new_street_light = $this->addToStreetLight($driver->street_light_id, RED_INSURANCE_AWAITING_PROCESSING);
                            }
                        }
                    } else {
                        if ($company->service_plan == 'FAVR' && $company->remove_reimbursement_without_insurance == 0) {
                            $new_street_light = $this->addToStreetLight($driver->street_light_id, GREEN_NO_INSURANCE_COMPLIANCE);
                        } else {
                            $new_street_light = $this->addToStreetLight($driver->street_light_id, RED_INSURANCE_EXPIRED);
                        }
                    }


                    $data = (object)[
                        'from' => $from,
                        'company' => $company,
                        'driver' => $driver,
                        'circle' => $circle,
                        'insurance_amounts' => $insurance_amounts,
                        'coordinator' => $coordinator,
                        'new_street_light' => $new_street_light
                    ];

                    $data->streetLight = new \stdClass();
                    $data->streetLight->data = $this->street_light->streetLightArray($new_street_light);
                    $data->streetLight->color = $this->street_light->colorOfStreetLight($new_street_light);

                    @Mail::to($driver->email)->send(new InsuranceExpiresTodayEmail($data));
                    $emails_sent++;

                    sleep(3); // TODO remove this line

                    if (!$driver_found) {
                        $driver_found = true;
                        if (!empty($long_description)) {
                            $long_description .= "<br>";
                        }
                        $long_description .= "<b>$company->name</b> ";
                    }
                    $long_description .= "$driver->last_name $driver->first_name, ";

                    // create new driver profile with new street light
                    $driverProfile = DriverProfile::where('user_id', $driver->id)->first();
                    $newDriverProfile = $driverProfile->replicate();
                    $driverProfile->delete();

                    /**
                     * If we have a review in queue, make it the active review package on the driver profile
                     * Otherwise set the review id to null
                     */
                    if (!empty($reviewInQueue)) {
                        $newDriverProfile->insurance_review_id = $reviewInQueue->id;
                    } else {
                        $newDriverProfile->insurance_review_id = null;
                    }

                    /**
                     * Finally apply the new street light, and set the date of next review to the company default
                     */
                    $newDriverProfile->next_insurance_review_date = $next_company_default_review_date;

                    $newDriverProfile->street_light_id = $new_street_light;
                    $newDriverProfile->save();


                }   // drivers

            }   // companies

            $short_description = "Emails sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo($task_name, ['short_description' => $short_description]);

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }

    }

    /**
     * master_tasks id: 91
     *
     * @param $task_id
     * @param $task_name
     */
    public function licenseExpireIn20days($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/licenceExpireIn20days.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 89
     *
     * @param $task_id
     * @param string $task_name
     */
    public function licenseExpiryNotice($task_id, $task_name = '')
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/licenseExpiryNotice.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        // this isn't complete, short-circuit here for now
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented. We do not have tables', '');
        return;

        $emails_sent = 0;
        $long_description = '';
        $date = date("Y-m-d");
        $last_company = null;

        $company_query = <<<EOQ
SELECT c.id, c.name, cl.license_responsibility, 
FROM companies c
JOIN company_licenses cl ON cl.company_id = c.id AND cl.deleted_at IS NULL
JOIN company_options co ON co.company_id = c.id AND deleted_at IS NULL
WHERE c.deleted_at IS NULL AND cl.license_responsibility IS NOT NULL
ORDER BY c.name
EOQ;

        $companies = \DB::select(\DB::raw($company_query));
        foreach ($companies as $c) {

            if( $last_company != $c->name) {
                $last_company = $c->name;
            }

            $drivers_with_emails = '';

            $driver_query = <<<EOQ
SELECT u.last_name, u.first_name, u.email
FROM users u 
JOIN driver_profiles dp ON u.id = dp.user_id AND dp.delated_at IS NULL AND dp.company_id = $c->id 
EOQ;

        }
    }

    /**
     * master_tasks id: 86
     * @param $task_id
     * @param string $task_name
     */
    public function managerApprovalReminder($task_id, $task_name = '')
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/managerApprovalReminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        try {
            $emails_sent = 0;
            $long_description = '';


            // dates to use
            // we generate these record FOR PREVIOUS month
            $year = date('n') == 1 ? date("Y") - 1 : date("Y");
            $month = date('n') == 1 ? 12 : date('n') - 1;
            $previous_day = date("j", strtotime("yesterday"));

            $company_query = <<<EOQ
SELECT c.id, c.name, cd.mileage_lock_driver_day, cd.mileage_lock_all_day, 
  cd.mileage_approval_end_day, cd.direct_pay_day, f.direct_pay_file_fee, f.direct_pay_driver_fee
FROM companies AS c
JOIN company_mileages AS m ON m.company_id = c.id AND m.deleted_at IS NULL
JOIN company_dates AS cd ON cd.company_id = c.id AND cd.deleted_at IS NULL
JOIN company_payments f ON f.company_id = c.id AND f.deleted_at IS NULL
  WHERE c.deleted_at IS NULL
  AND cd.mileage_lock_driver_day = {$previous_day}
  AND cd.mileage_approval_end_day >= 0
  ORDER BY c.name
EOQ;
            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $c) {

                echo "$c->name<br>";

                $logger->addInfo('Company', [$c->name]);

                // create approval record per company
                // check if was already inserted. It can happen if they move driver lock day forward.
                $already_query = <<<EOQ
SELECT * FROM monthly_approvals WHERE company_id = {$c->id} AND year = {$year} AND month = {$month}
EOQ;
                $already = \DB::select(\DB::raw($already_query));
                if ($already != null) {
                    continue;
                }

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$c->name</b> ";

                // new approval record
                $approval_id = \DB::table('monthly_approvals')->insertGetId(
                    ['company_id' => $c->id,
                        'year' => $year,
                        'month' => $month,
                        'file_fee' => $c->direct_pay_file_fee,
                        'driver_fee' => $c->direct_pay_driver_fee
                    ]);

                $coordinator = $this->getCoordinatorData($c);
                $from = $coordinator->email;

                // find managers or company approvers and assign approval_id for each of them
                $manager_query = <<<EOQ
SELECT mp.manager_id AS user_id, u.last_name, u.first_name, u.email, 'manager' AS type, COUNT(md.driver_id) AS drivers
  FROM manager_profiles mp 
  JOIN users u ON mp.user_id = u.id
  LEFT JOIN manager_driver md ON md.manager_id = u.id
  WHERE mp.company_id = {$c->id} AND mp.deleted_at IS NULL
  GROUP BY 1,2,3,4
UNION
  SELECT a.administrator_id AS user_id, u.last_name, u.first_name, u.email, 'administrator' AS type, 0 AS drivers
  FROM administrator_profiles a
  JOIN users u ON a.user_id = u.id
  WHERE a.company_id = {$c->id} AND a.company_approver = 1
ORDER BY 2,3
EOQ;

                $managers = \DB::select(\DB::raw($manager_query));
                foreach ($managers as $m) {

                    // new approver record
                    if ($m->type == 'manager') {
                        \DB::table('monthly_approvers')->insert(
                            ['approval_id' => $approval_id, 'manager_id' => $m->user_id]
                        );
                    } else {
                        \DB::table('monthly_approvers')->insert(
                            ['approval_id' => $approval_id, 'administrator_id' => $m->user_id]
                        );
                    }

                    // send email to managers with drivers
                    if ($m->type == 'manager' && $m->drivers > 0) {
                        $current_year = date("Y");
                        $current_month = date("n");
                        $formatted_lock_date = date("l, F jS, Y", mktime(0, 0, 0, $current_month, $c->mileage_lock_all_day, $current_year));
                        $formatted_approval_date = date("l, F jS, Y", mktime(0, 0, 0, $current_month, $c->mileage_approval_end_day, $current_year));
                        $formatted_month = date("F", mktime(0, 0, 0, $month, 1, $year));
                        $data = (object)[
                            'from' => $from,
                            'coordinator' => $coordinator,
                            'year' => $year,
                            'month' => $formatted_month,
                            'lock_date' => $formatted_lock_date,
                            'approval_date' => $formatted_approval_date];
                        //////////////////////////////// JUST FOR TESTING WITH MAIL TRAP !!!!!
                        if ($emails_sent == 0) {
                            $logger->addInfo('WARNING', ['Email sent to mailtrap.io are limited to one']);
                            Mail::to($m->email)->send(new ManagerApprovalReminderEmail($data));
                        }
                        sleep(2);
                        $emails_sent++;
                        $long_description .= " $m->last_name $m->first_name,";
                    }
                }   // manager email
            }   // companies
            $short_description = "Email sent to $emails_sent managers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description);
            $logger->addInfo('Completed', [$short_description]);
        } catch (\Exception $e) {
            $short_description = "Email sent to $emails_sent managers";
            $error_info = 'File: ' . $e->getFile() . ' Line: ' . $e->getLine() . ' Error: ' . $e->getMessage();
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $error_info, 'Failed');
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => 'File: ' . $e->getFile() .
                ' Line: ' . $e->getLine() . ' Error: ' . $e->getMessage()]);
        }
    }   // managerApprovalReminder()

    /**
     * master_tasks id: 90
     *
     * @param $task_id
     * @param string $task_name
     */
    public function mileageReminder($task_id, $task_name = '')
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/mileageReminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        $long_description = '';

        try {
            // dates to use
            $date = date('F d, Y');
            $day = date('d');
            $emails_sent = 0;

            // get last month
            // we generate these record FOR PREVIOUS month
            $year = date('n') == 1 ? date("Y") - 1 : date("Y");
            $month = date('n') == 1 ? 12 : date('n') - 1;
            $last_month_name = date("F", mktime(0, 0, 0, $month, 1, $year));

            // Find companies that have a mileage reminder of today
            $company_query = <<<EOQ
SELECT
c.id, c.name, d.mileage_reminder_day, d.second_mileage_reminder_day, d.mileage_lock_driver_day, m.mileage_entry_method
FROM companies AS c
JOIN company_mileages AS m ON m.company_id = c.id AND m.deleted_at IS NULL
JOIN company_dates AS d on d.company_id = c.id AND d.deleted_at IS NULL
WHERE c.deleted_at IS NULL AND (d.mileage_reminder_day = {$day} OR d.second_mileage_reminder_day = {$day} OR 1 = 1 ) LIMIT 5
EOQ;
            // TODO remove  OR 1 = 1 ) LIMIT 5

            $companies = \DB::select(\DB::raw($company_query));

            // quit if no companies
            if (count($companies) == 0) {
                $logger->addInfo($task_name, ['result' => 'No companies with reminders on ' . $date]);
                $this->scheduledTaskEnded($task_id, $log_id, 'Emails sent to 0 drivers', 'No companies with mileage reminders on ' . $date, 'Completed');
                return;
            }

            $yellow_new_driver = YELLOW_NEW_DRIVER;
            $red_no_reimbursement = RED_NO_REIMBURSEMENT;
            $red_terminated = RED_TERMINATED;

            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$company->name</b> ";

                $coordinator = $this->getCoordinatorData($company->id);
                $from = $coordinator->email;
                $formatted_lock_date = date("l, F jS, Y", mktime(0, 0, 0, date("n"), $company->mileage_lock_driver_day, date("Y")));

                $emails_per_company = 0;

                $driver_query = <<<EOQ
SELECT
dp.user_id, u.first_name, u.last_name, u.email, dp.company_id, mm.business_mileage
FROM driver_profiles dp
JOIN users u ON u.id = dp.user_id
JOIN monthly_mileages mm ON mm.user_id = dp.user_id AND mm.year = 2017 AND mm.month = 12
WHERE dp.company_id = {$company->id} AND dp.deleted_at IS NULL 
  AND (dp.street_light_id & {$yellow_new_driver}) = 0 AND (street_light_id & {$red_terminated}) = 0 AND (street_light_id & {$red_no_reimbursement}) = 0
ORDER BY u.last_name, u.first_name LIMIT 3
EOQ;
                // TODO remove LIMIT 3 from driver query

                $drivers = \DB::select(\DB::raw($driver_query));

                foreach ($drivers as $driver) {

                    $logger->addInfo('Driver:', [$driver->user_id]);

                    $mileage_discrepancies = $company->mileage_entry_method == 'Mi-Route' ? $this->getMileageDifferences($driver->user_id, $year, $month) : null;
                    // between daily mileage and sum of daily trips for Mi-Route drivers

                    $data = (object)[
                        'from' => $from,
                        'lock_date' => $formatted_lock_date,
                        'year' => $year,
                        'month_name' => $last_month_name,
                        'driver' => $driver,
                        'company' => $company,
                        'coordinator' => $coordinator,
                        'time_zone' => $this->getDriverTimeZone($driver->user_id),
                        'mileage_discrepancies' => $mileage_discrepancies];
                    $logger->addInfo($task_name, ['data' => $data]);

                    Mail::to($driver->email)->send(new MileageReminderEmail($data));

                    $long_description .= " $driver->last_name $driver->first_name,";
                    $emails_per_company++;
                    $emails_sent++;
                    sleep(2);
                    // TODO remove sleep(2)

                }   // drivers
            }   // companies

            $short_description = "Emails sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');

        } catch (\Exception $e) {
            $logger->addInfo($task_name, ['status' => 'Failed', 'sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    protected function getMileageDifferences($driver_id, $year, $month)
    {
        $month_1 = $month == 12 ? 1 : $month + 1;
        $year_1 = $month == 12 ? $year + 1 : $year;
        $from_date = sprintf('%04d-%02d-01', $year, $month);
        $to_date = date("Y-m-d", mktime(0, 0, 0, $month_1, 1, $year_1));
        $return = [];

        $mileage_query = <<<EOQ
SELECT dm.business_mileage, dm.trip_date, user_id, SUM(CAST(dt.mileage_original AS decimal(12,1)) + CAST(IFNULL(dt.mileage_adjusted,0) AS decimal(12,1))) AS trip_mileage
  FROM daily_mileages dm
  LEFT JOIN daily_trips dt ON dm.id = dt.daily_mileage_id
  WHERE dm.user_id = $driver_id AND dm.trip_date >= '$from_date' AND dm.trip_date < '$to_date'
  GROUP BY 2
  HAVING ROUND(SUM(CAST(dt.mileage_original AS decimal(12,1)) + CAST(IFNULL(dt.mileage_adjusted,0) AS decimal(12,1)))) != IFNULL(dm.business_mileage,0)
  ORDER BY dm.trip_date
EOQ;
        $mileages = \DB::select(\DB::raw($mileage_query));
        foreach ($mileages as $m) {
            if ($m->trip_mileage > $m->business_mileage) {
                list($last_month_year, $last_month, $dd) = explode('-', $m->trip_date);
                $diff = round($m->trip_mileage - $m->business_mileage);
                $formatted_trip_date = date('F jS', mktime(0, 0, 0, $last_month, $dd, $last_month_year));
                $return[] = $formatted_trip_date . ' mileage ' . $diff;
            }
        }
        return empty($return) ? null : $return;
    }

    /**
     * master_tasks id: 128
     * @param $task_id
     * @param $task_name
     */
    public function noReimbursementReminder($task_id, $task_name)
    {

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/noReimbursementReminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        try {

            $short_description = '';
            $long_description = '';
            $emails_sent = 0;
            list($year, $month, $day) = explode('-', date("Y-n-d"));

            list($year, $month, $day) = explode('-', '2017-03-01');   // TODO remove this line

            if ($month == 1) {
                $year--;
                $month = 12;
            } else {
                $month--;
            }

            // companies with reimbursement values from last month
            $company_query = <<<EOQ
SELECT c.id, c.name, cc.company_coordinator_phone, cc.cardata_coordinator_phone, cp.use_advance_payment,
  u.last_name as company_coordinator_last_name, u.first_name as company_coordinator_first_name, u.email as company_coordinator_email,  
  u1.last_name as cardata_coordinator_last_name, u1.first_name as cardata_coordinator_first_name, u1.email as cardata_coordinator_email
FROM companies c
JOIN company_payments cp ON cp.company_id = c.id AND cp.deleted_at IS NULL
JOIN company_coordinators cc ON cc.company_id = c.id AND cc.deleted_at IS NULL
JOIN company_dates cd ON cd.company_id = c.id AND cc.deleted_at IS NULL
JOIN users u ON cc.company_coordinator_id = u.id
JOIN users u1 ON cc.cardata_coordinator_id = u1.id
WHERE c.deleted_at IS NULL AND ( cd.no_reimbursement_notification_day = $day )
ORDER BY c.name
EOQ;

            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $company) {

                $drivers_found = 0;

                // skip if nobody got reimbursement
                $check_query = <<<EOQ
SELECT COUNT(mr.user_id) AS found FROM monthly_reimbursements mr
JOIN driver_profiles dp ON dp.id = mr.driver_profile_id AND dp.company_id = $company->id
WHERE mr.year = $year and mr.month = $month 
EOQ;
                $check = \DB::select(\DB::raw($check_query));
                if ($check[0]->found == 0) {
                    continue;
                }

                $logger->addInfo($task_name, ['company' => $company->name]);

                // get drivers without reimbursement
                $drivers = $this->driversWithoutReimbursement(null, $company->id, $year, $month, null);
                if (count($drivers) == 0) {
                    continue;
                }

                $logger->addInfo($task_name, ['drivers' => $drivers]);

                $coordinator = (object)[
                    'name' => $company->cardata_coordinator_first_name . ' ' . $company->cardata_coordinator_last_name,
                    'email' => $company->cardata_coordinator_email,
                    'phone' => $company->cardata_coordinator_phone];


                // skip drivers with GREEN_REIMBURSEMENT
                $drivers = array_filter($drivers, function ($driver) {
                    return $driver->street_light_id != GREEN_REIMBURSEMENT;
                });

                // skip drivers with not past start date, copied from production code

                if ($company->use_advance_payment != 1) {
                    $drivers = array_filter($drivers, function ($driver) use ($year, $month) {
                        list($d_year, $d_month, $d_day) = explode('-', $driver->start_date);
                        return $d_year > $year || $d_year == $year && $d_month > $month ? false : true;
                    });
                }


                foreach ($drivers as $driver) {
                    $street_light = $this->street_light->formattedStreetLight($driver->street_light_id);
                    $driver->description = $street_light['color'] . ' Traffic Light, ' . $street_light['names'];
                    $drivers_found++;
                }

                $data = (object)[
                    'from' => $company->cardata_coordinator_email,
                    'admin_name' => $company->company_coordinator_first_name . ' ' . $company->company_coordinator_last_name,
                    'drivers' => $drivers,
                    'coordinator' => $coordinator
                ];

                if ($drivers_found) {
                    Mail::to($company->company_coordinator_email)->send(new NoReimbursementReminderEmail($data));
                    sleep(2);   // TODO remove sleep(2);

                    if (!empty($long_description)) {
                        $long_description .= "<br>";
                    }
                    $long_description .= "<b>$company->name</b> ";
                }

            }   // companies

            $logger->addInfo('Completed', ['short_description' => $short_description, 'long_description' => $long_description]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');

        } catch (\Exception $e) {
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $short_description = "Email sent to $emails_sent drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    /**
     * master_tasks id: 87
     * @param $task_id
     * @param $task_name
     */
    public function paymentReminder($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/paymentReminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);

        $year = date("Y");
        $month = date("n");
        $day = date("j");
        $long_description = '';
        $email_sent = 0;

        $last_month = $month == 1 ? 12 : $month - 1;
        $last_month_year = $month == 1 ? $year - 1 : $year;
        $last_month_name = $this->getMonthNames()[$last_month];

        try {

            $company_query = <<<EOQ
SELECT c.id, c.name, co.company_coordinator_phone, u.last_name, u.first_name, u.email, 
  cpd.received_date
FROM companies c 
JOIN company_dates cd ON cd.company_id = c.id AND cd.deleted_at IS NULL AND cd.direct_pay_day = $day
JOIN company_coordinators co ON co.company_id = c.id AND co.deleted_at IS NULL
JOIN users u ON co.company_coordinator_id = u.id
JOIN company_payment_dates cpd ON cpd.company_id = c.id AND cpd.year = $year AND cpd.month = $month
WHERE c.deleted_at IS NULL 
ORDER BY c.name
EOQ;
            $companies = \DB::select(\DB::raw($company_query));
            foreach ($companies as $company) {

                if (!empty($long_description)) {
                    $long_description .= "<br>";
                }
                $long_description .= "<b>$company->name</b> ";
                $coordinator = (object)['name' => "$company->first_name $company->last_name",
                    'phone' => $company->company_coordinator_phone,
                    'email' => $company->email];

                $data = (object)[
                    'from' => $company->email,
                    'cc' => 'lrundle@cardataconsultants.com',
                    'previous_month_name' => $last_month_name,
                    'previous_month_year' => $last_month_year,
                    'company_name' => $company->name,
                    'coordinator' => $coordinator];

                // $logger->addInfo($task_name, ['data' => $data]);

                Mail::to('Directpay@cardataconsultants.com')->send(new PaymentReminderEmail($data));
                $email_sent++;
                sleep(2);   // TODO remove this line: sleep(2);
            }

            $logger->addInfo($task_name, ['status' => 'Done', 'long' => $long_description]);

            $short_description = "$email_sent emails sent Direct Pay of CarData";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description, 'Completed');
            $logger->addInfo('Completed', ['sent' => $short_description, 'description' => $long_description]);

        } catch (\Exception $e) {
            $short_description = "$email_sent emails sent Direct Pay of CarData";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
        }

    }

    /**
     * master_tasks id: 77
     *
     * @param $task_id
     * @param $task_name
     */
    public function populateDEMOMileage($task_id, $task_name)
    {

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/populateDEMOMileage.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Started', ['task_id' => $task_id, 'task_name' => $task_name]);


        $driver_count = 0;
        $created_at = date("Y-m-d H:i:s");
        $total_business_last_month = 0;
        $total_business_this_month = 0;
        $odometer_last_month = 0;
        $odometer_this_month = 0;
        $inserted_total = 0;
        $updated_total = 0;

        list($this_year, $this_month, $this_day) = explode('-', date("Y-m-d"));

        // just for testing, as we do not have current data
        //$this_year = 2017;
        //$this_month = 10;
        //$this_day = 1;

        try {

            // get company lock all day as only before this day we are involved in copying from previous month
            $mileage_lock_all_day = CompanyMileage::find(51)->mileage_lock_all_day;

            if ($this_day <= $mileage_lock_all_day) {
                $last_month_year = $this_month == 1 ? $this_year - 1 : $this_year;
                $last_month = $this_month == 1 ? 12 : $this_month - 1;
                $from_date = sprintf("%04d-%02d-01", $last_month_year, $last_month);
            } else {
                $last_month_year = $this_year;
                $last_month = $this_month;
                $from_date = sprintf("%04d-%02d-01", $last_month_year, $last_month);
            }
            $to_date = sprintf("%04d-%02d-%02d", $this_year, $this_month, $this_day);

            //$logger->addInfo($task_name, ['from_date'=>$from_date, 'to_date'=>$to_date]);

            $driver_query = <<<EOQ
SELECT u.id AS demo_id, u1.id AS quaker_id, u.last_name, u.first_name 
  FROM driver_profiles dp, driver_profiles dp1, users u, users u1
  WHERE dp.company_id = 51 AND dp1.company_id = 36
  AND dp.user_id = u.id AND dp1.user_id = u1.id 
  AND u.last_name = u1.last_name AND u.first_name = u1.first_name
EOQ;


            $drivers = \DB::select(\DB::raw($driver_query));

            foreach ($drivers as $d) {

                //$logger->addInfo($task_name, ['from id'=>$d->quaker_id, 'to id'=>$d->demo_id, 'driver'=>$d->last_name . ' ' . $d->first_name]);

                $driver_count++;

                // mileage query
                $mileages = \DB::select('SELECT * FROM daily_mileages WHERE user_id=? AND trip_date>=? AND trip_date < ?', [$d->quaker_id, $from_date, $to_date]);

                foreach ($mileages as $m) {

                    $curr_month = explode('-', $m->trip_date)[1];

                    if (!empty($m->ending_odometer)) {
                        if ($curr_month == $last_month) {
                            $odometer_last_month = $m->ending_odometer;
                        } elseif ($curr_month == $this_month) {
                            $odometer_this_month = $m->ending_odometer;
                        }
                    }
                    if (!empty($m->business_mileage)) {
                        if ($curr_month == $last_month) {
                            $total_business_last_month += $m->business_mileage;
                        } elseif ($curr_month == $this_month) {
                            $total_business_this_month += $m->business_mileage;
                        }
                    }

                    $current_mileage = DailyMileage::where('user_id', $d->demo_id)->where('trip_date', $m->trip_date)->first();

                    if (empty($current_mileage)) {
                        $current_mileage = new DailyMileage;
                        $current_mileage->user_id = $d->demo_id;
                        $current_mileage->trip_date = $m->trip_date;
                        $inserted_total++;
                    } else {
                        $updated_total++;
                    }

                    $current_mileage->destination = $m->destination;
                    $current_mileage->starting_odometer = $m->starting_odometer;
                    $current_mileage->ending_odometer = $m->ending_odometer;
                    $current_mileage->business_mileage = $m->business_mileage;
                    $current_mileage->personal_mileage = $m->personal_mileage;
                    $current_mileage->gap_mileage = $m->gap_mileage;
                    $current_mileage->updated_at = $created_at;
                    $current_mileage->save();

                }   // mileages

                // $logger->addInfo('Find Last Monthly for', ['user_id'=>$d->demo_id, 'year'=>$last_month_year, 'month'=>$last_month]);

                $last_monthly_mileage = MonthlyMileage::where('user_id', $d->demo_id)->where('year', $last_month_year)->where('month', $last_month)->first();
                if (empty($last_monthly_mileage)) {
                    $last_monthly_mileage = new MonthlyMileage;
                    $last_monthly_mileage->user_id = $d->demo_id;
                    $last_monthly_mileage->month = $last_month;
                    $last_monthly_mileage->year = $last_month_year;
                    $last_monthly_mileage->created_at = $created_at;
                }
                $last_monthly_mileage->business_mileage = $total_business_last_month;
                $last_monthly_mileage->ending_odometer = $odometer_last_month;
                $last_monthly_mileage->updated_at = $created_at;
                $last_monthly_mileage->save();

                if ($this_day > $mileage_lock_all_day) {

                    $this_monthly_mileage = MonthlyMileage::where('user_id', $d->demo_id)->where('year', $this_year)->where('month', $this_month)->first();
                    if (empty($this_monthly_mileage)) {
                        $this_monthly_mileage = new MonthlyMileage;
                        $this_monthly_mileage->user_id = $d->demo_id;
                        $this_monthly_mileage->month = $this_month;
                        $this_monthly_mileage->year = $this_year;
                        $this_monthly_mileage->created_at = $created_at;
                    }
                    $this_monthly_mileage->business_mileage = $total_business_this_month;
                    $this_monthly_mileage->ending_odometer = $odometer_this_month;
                    $this_monthly_mileage->updated_at = $created_at;
                    $this_monthly_mileage->save();

                }


            }   // drivers
            $short_description = "Mileage was populated for $driver_count DEMO drivers";
            $long_description = "$updated_total records were updated and $inserted_total were inserted";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description);
            $logger->addInfo('Completed', ['summary' => $short_description, 'details' => $long_description]);
        } catch (\Exception $e) {
            $short_description = "Mileage was populated for $driver_count DEMO drivers";
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
        }
    }

    /**
     * master_tasks id: 92
     *
     * @param $task_id
     * @param $task_name
     */
    public function quarterlyAdjustmentReminder($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/quarterlyAdjustmentReminder.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 118
     *
     * @param $task_id
     * @param $task_name
     */
    public function vRPPayFileEmail($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/vRPPayFileEmail.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id' => $task_id, 'task_name' => $task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    public function testRun() {
        $task_id = 104;
        $script = 'getFuelPriceFromOpis';
        $this->getFuelPriceFromOpis( $task_id, $script);
    }

    /**
     * master_tasks id: 130
     *
     * @param $task_id
     * @param $task_name
     */
    public function miRouteTransitionDate($task_id, $task_name)
    {

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/miRouteTransitionDate.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $total = 0;
        $long_description = '';
        $short_description = '';
        $separator = '';

        try {

        $company_query = <<<EOQ
SELECT c.id, c.name 
FROM companies c 
JOIN company_mileages cm ON c.id = cm.company_id  
WHERE cm.miroute_transition_date = CURRENT_DATE() 
AND cm.mileage_entry_method != 'Mi-Route'  
AND cm.deleted_at IS NULL 
ORDER BY c.name
EOQ;

            $logger->addInfo($task_name, ['$company_query' => $company_query]);
            $companies = \DB::select(\DB::raw($company_query));

            foreach ( $companies as $company) {

                $logger->addInfo($task_name, ['status' => 'get company', 'name' => $company->name]);

                $updated = 0;
                $new_cm = CompanyMileage::where('company_id',$company->id)->first()->replicate();
                CompanyMileage::where('company_id',$company->id)->delete();
                $new_cm->mileage_entry_method = 'Mi-Route';
                $new_cm->save();

                $drivers = \DB::table('driver_profiles')->where('company_id',$company->id)
                    ->where(function ($query) {
                        $query->where('miroute_status',0)
                              ->orWhere('miroute_status',2);

                    })
                    ->select('id','user_id')
                    ->get();
                foreach($drivers as $profile) {

                    $new_profile = DriverProfile::find($profile->id)->replicate();
                    DriverProfile::find($profile->id)->delete();
                    $new_profile->miroute_status = 1;
                    $new_profile->save();

                    $updated++;

                }
                $total++;
                $long_description .= "$separator<strong>{$company->name}</strong> $updated driver profiles updated";
                $separator = ', ';
                $company_label = $total == 1 ? 'company' : 'companies';
                $short_description = "$total $company_label converted to Mi-Route";

            }

            $logger->addInfo($task_name, ['status' => 'completed', 'short' => $short_description, 'long' => $long_description]);
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description);

        } catch (\Exception $e) {
            \Debugbar::error($e->getMessage());
            $logger->addInfo('Failed', ['sent' => $short_description, 'error' => $e->getMessage()]);
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
        }
    }

    public function callingPulteFileFromTestPage(Request $request){
        $task_id = $request->task_id;
        $task_name = $request->task_name;
        return $this->gracePeriodExpires($task_id, $task_name);
    }

    /////////////////// PULTE ///////////////////////
    /**
     * master_tasks id: 93
     *
     * @param $task_id
     * @param $task_name
     */
    public function pulteDriverFile($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/pulteDriverFile.log', 0, Logger::INFO);
        $logger->pushHandler($handler);

        $short_description = '';
        $long_description = '';

        $logger->addInfo("Pulte Started");

        set_time_limit(0);

        try {

            $return_notes = [];
            $total_added = 0;
            $total_terminated = 0;
            $total_be_terminated = 0;
            $total_deactivated = 0; // managers
            $total_reinstated = 0;  // managers

            $header = true;
            $err_array = [];
            $alerts = [];

            $company_id = 57; // Pulte Homes
            $path_to_files = $company_id . '/import_file';
            $files = Storage::disk('upload_docs')->allFiles($path_to_files);

            $logger->addInfo($task_name, ['status' => 'running', 'short' => $path_to_files, 'long' => 'After Storage']);


            $columns = $this->pulteRecordColumns();

            $data = [];
            $i = 1;

            $vehicleProfilesNames = $this->getVehicleProfileNames($company_id);
            $mileageBandNames = $this->getMileageBandsNames();
            $divisionNames = $this->getDivisionNames($company_id);
            $stateProvincesNames = $this->getStateProvinces();

            // get all active drivers from this company
            // if driver is missing from this file, we have to delete it

            $missing_drivers = [];
            $drivers = \DB::table('driver_profiles as d')
                ->join('users as u', 'u.id', '=', 'd.user_id')
                ->whereNull('d.deleted_at')
                ->where('d.active', 1)
                ->where('d.company_id', $company_id)
                ->select('u.id', 'u.last_name', 'u.first_name')->get();
            foreach ($drivers as $driver) {
                $missing_drivers[$driver->id] = $driver->last_name . ' ' . $driver->first_name;
            }

            /* this sucks! 4000+ queries were generated. with above code only 23
            $all_drivers = DriverProfile::where('company_id',$company_id)->get();
            $missing_drivers = [];
            foreach( $all_drivers as $driver) {
                $missing_drivers[$driver->id] = $driver->user->last_name . ' ' . $driver->user->first_name;
            }
            */

            $logger->addInfo($task_name, ['status' => 'before looping through files']);

            foreach ($files as $file) {

                $full_path = public_path() . '/storage/docs/' . $file;

                if (($handle = fopen($full_path, 'r')) !== false) {
                    while (($row = fgetcsv($handle, 1000, ',')) !== false) {

                        if ($header) {
                            $header = false;
                            continue;
                        }
                        if (count($row) != 23) {
                            $err_array[] = "Invalid number of fields " . count($row) . " in row $i";
                        }
                        $data[] = (object)$this->generatePulteRecord($row, $columns);
                        // $logger->addInfo($task_name, ['row' => $row]);
                        $i++;
                    }
                }

                $err_row = [];
                foreach ($data as $record_id => $record) {

                    $row_num = $record_id + 2;

                    \Debugbar::info('record', $record);
                    $logger->addInfo($task_name, ['status' => "---- Row $row_num Process driver $record->last_name $record->first_name ----"]);

                    $action_taken = null;

                    $err = $this->pulteValidateRecord($record, $company_id, $vehicleProfilesNames, $mileageBandNames, $divisionNames, $logger);

                    if (!empty($err)) {

                        $logger->addInfo($task_name, ['status' => "Validation Failed: " .  implode(', ', $err)]);
                        \Debugbar::error("Row $row_num $record->last_name $record->first_name ERROR: " . implode(',', $err));
                        $return_notes[] = "Row $row_num $record->last_name $record->first_name ERROR: " . implode(',', $err);
                        continue;
                    }
                    // \Debugbar::info('Validation passed');

                    // check if we have a new division
                    ///////////////////////////////////////////////////////////////
                    if (!in_array($record->market, $divisionNames)) {
                        $new_division = Division::create([
                            'company_id' => $company_id,
                            'name' => $record->market
                        ]);
                        $division_id = $new_division->id;
                        $divisionNames[$division_id] = $record->market;
                        $alerts[] = "New division: $record->market was created for driver $record->last_name $record->first_name";
                        $logger->addInfo($task_name, ['row' => $row_num, 'status' => "New division created: $record->market was created for driver $record->last_name $record->first_name"]);
                    } else {
                        $division_id = Division::where('company_id', $company_id)
                            ->where('name', $record->market)->pluck('id');
                    }


                    // check if we have a new manager
                    ///////////////////////////////////////////////////////////////
                    \Debugbar::info("Check if we have this manager $record->manager_last_name $record->manager_first_name $record->manager_email ");

                    $r = $this->find_manager($company_id, $record);
                    \Debugbar::info('Returned from find_manager: ', $r);
                    $activated = $r->activated;

                    $manager = $r->manager;
                    if ($activated) {
                        $action_taken = 'REINSTATED';
                        $logger->addInfo($task_name, ['status' => "Row $row_num Manager was reinstates $record->manager_last_name $record->manager_first_name"]);
                    }

                    if (!$manager) {
                        \Debugbar::info("Create a new manager");
                        $manager = $this->add_new_manager($company_id, $record);
                        $action_taken = 'REINSTATED';
                        $logger->addInfo($task_name, ['status' => "Row $row_num New manager created $manager->last_name $manager->first_name"]);
                    }

                    // find driver by employee number
                    //////////////////////////////////////////////////////////////////////////
                    $driver = DriverProfile::where('company_id', $company_id)
                        ->where('employee_number', $record->employee_number)->first();

                    if (!$driver) { // add driver

                        \Debugbar::info('Calling Insert Driver');

                        $results = $this->add_new_driver($company_id, $divisionNames,
                            $stateProvincesNames, $vehicleProfilesNames, $mileageBandNames, $record);

                        if ($results->errors) {     // problems found
                            $alerts[] = array_merge($alerts, $results->errors);
                            $logger->addInfo($task_name, ['row' => $row_num, 'status' => 'adding driver failed', 'err' => $results->errors]);
                            continue;
                        }

                        $driver = $results->driver;
                        \Debugbar::info("Driver Profile after adding:", $driver);
                        $action_taken = 'ADDED';


                    } else { // update driver

                        \Debugbar::info("Calling Update Driver $record->last_name $record->first_name ");
                        $action_taken = $this->updatePulteDriver($driver->user_id, $manager->manager_id, $divisionNames, $stateProvincesNames,
                            $vehicleProfilesNames, $mileageBandNames, $record);

                        unset($missing_drivers[$driver->user_id]);  // not missing
                    }

                    \Debugbar::info("Row $row_num $record->last_name $record->first_name $action_taken");

                    if ($action_taken) {
                        if ($action_taken == 'REINSTATED' || $action_taken == 'DEACTIVATED') {
                            $logger->addInfo($task_name, ['status' => "Row $row_num Manager $record->manager_last_name $record->manager_first_name $action_taken"]);
                            $return_notes[] = "Row $row_num $record->manager_last_name $record->manager_first_name $action_taken";
                        } elseif( $action_taken != 'No Changes') {
                            $return_notes[] = "Row $row_num $record->last_name $record->first_name $action_taken";
                        }

                        if ($action_taken == 'ADDED') {
                            $total_added++;
                            $logger->addInfo($task_name, ['status' => "Row $row_num Driver: $record->last_name $record->first_name $action_taken"]);
                        }
                        elseif ($action_taken == 'TERMINATED') {
                            $total_terminated++;
                            $logger->addInfo($task_name, ['status' => "Row $row_num Driver: $record->last_name $record->first_name $action_taken"]);
                        }
                        elseif ($action_taken == 'TO_BE_TERMINATED') {
                            $total_be_terminated++;
                            $logger->addInfo($task_name, ['status' => "Row $row_num Driver: $record->last_name $record->first_name $action_taken"]);
                        }
                        elseif ($action_taken == 'REINSTATED') $total_reinstated++;
                    }


                    \Debugbar::info("assignDriverToManager($driver->user_id, $manager->manager_id)");
                    $this->assignDriverToManager($driver->user_id, $manager->manager_id);
                    if ($manager->assigned_drivers == 'none') {
                        $manager->assigned_drivers == 'some';
                        $manager->save();
                    }
                    // $logger->addInfo($task_name, ['status' => "---- Driver $record->last_name $record->first_name processed ----"]);
                }   // for each record in file


                \Debugbar::info("process missing drivers");
                // TODO limit deletion of missing drivers as we are testing on small sample of Pulte file
                $limit = 2;
                $i = 0;
                foreach ($missing_drivers as $user_id => $driver_name) { // we could not find it so terminate it
                    if ($i++ > $limit) {
                        break;
                    }
                    \Debugbar::info("driver missing: $driver_name $user_id $total_be_terminated");
                    if( $this->scheduleToDeleteMissingDriver($user_id, $manager->manager_id, $driver_name, $logger)){
                        $total_be_terminated++;
                        $return_notes[] = "$driver_name TO BE TERMINATED";
                    }
                    \Debugbar::info("total_be_terminated=$total_be_terminated");
                }

                $logger->addInfo($task_name, ['status' => 'After deleteMissingDrivers']);


            }   // for each file

            \Debugbar::info("End of file");

            $logger->addInfo($task_name, ['status' => '----- END OF FILE -----']);

            // delete drivers already marked as to be terminated

            $res = $this->deletedScheduledDrivers($company_id);

            // update manager assign drivers to 'none' if there are no drivers assigned to them.
            // deactivate managers without drivers
            $res = $this->updateManagerAssignDrivers($company_id);

            if ($res->reinstated > 0) {
                $total_reinstated += $res->reinstated;
                $return_notes = array_merge($return_notes, $res->reinstated_arr);
            }
            if ($res->deactivated > 0) {
                $total_deactivated += $res->deactivated;
                $return_notes = array_merge($return_notes, $res->deactivated_arr);
            }

            $logger->addInfo($task_name, ['status' => 'After updateManagerAssignDrivers',
                'reinstated' => $res->reinstated, 'deactivated' => $res->deactivated]);

            $short = [];
            if ($total_added > 0) $short[] = "Drivers Added $total_added";
            if ($total_terminated > 0) $short[] = "Drivers Terminated $total_terminated";
            if ($total_be_terminated > 0) $short[] = "Drivers to be terminated $total_be_terminated";
            if ($total_reinstated > 0) $short[] = "Managers Reinstated $total_reinstated";
            if ($total_deactivated > 0) $short[] = "Managers Deactivated $total_deactivated";

            $short_description = implode(', ', $short);
            $long_description = implode('<br/>',$return_notes);


            $logger->addInfo($task_name, ['status' => 'completed', 'short' => $short_description, 'long' => $long_description]);
            \Debugbar::info('Before scheduledTaskEnded');
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $long_description);
            \Debugbar::info('After scheduledTaskEnded');

            \Debugbar::info('Short:',$short_description);
            \Debugbar::info('Long:',$long_description);
            \Debugbar::info("End of Pulte File");
            return ['short_description' => $short_description, 'long_description' => $return_notes];

        } catch (\Exception $e) {
            $error_msg = 'File: ' . $e->getFile() . ' Line: ' . $e->getLine() . ' ' . $e->getMessage();
            $logger->addInfo('FAILED', ['short' => $short_description, 'ERROR' => $error_msg]);
            $this->scheduledTaskEnded($task_id, $log_id, $short_description, $e->getMessage(), 'Failed');
            \Debugbar::info("FAILED: " . $error_msg);

            return ['short_description' => $short_description, 'long_description' => $error_msg];
        }
    }
}



