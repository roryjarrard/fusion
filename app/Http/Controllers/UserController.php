<?php

namespace App\Http\Controllers;

use App\Address;
use App\AdministratorProfile;
use App\Calculators\StreetLightCalculator;
use App\Company;
use App\Division;
use App\DriverProfile;
use App\Http\Traits\CommonTraits;
use App\Http\Traits\ValidationTraits;
use App\Mail\Test;
use App\Mail\UsernameChangedEmail;
use App\ManagerProfile;
use App\Note;
use App\Notifications\CarDataWelcomeEmail;
use App\SuperProfile;
use App\TimeZone;
use App\User;
use App\ZipCode;
use Auth;
use HttpOz\Roles\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator as Validator;
use PhpOffice\PhpSpreadsheet;


class UserController extends Controller
{
    use CommonTraits;
    use ValidationTraits;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Drivers will be dependant upon company, division, manager and active status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @internal param bool|int $inactive
     */
    public function getDrivers(Request $request)
    {
        $company_id = $request->company_id ?: -1;
        $division_id = $request->division_id ?: -1;
        $manager_id = $request->manager_id ?: -1;
        $from_session = $request->from_session;

        /*\Log::info('getDrivers', ['company_id' => $company_id, 'division_id' => $division_id, 'manager_id' => $manager_id]);*/
        \Debugbar::info($company_id . ' ' . $division_id . ' ' . $manager_id);
        $active_drivers_count = 0;
        $inactive_drivers_count = 0;

        $join = "";
        $where = "";

        if ($from_session && session()->has('impersonator_id') && session()->get('active_role') == 'driver') {
            $user = User::find(session()->get('impersonator_id'));
            $role = session()->get('impersonator_role');
        } else {
            $user = User::find(session()->get('viewed_user_id'));
            $role = session()->get('active_role');
        }

        /*\Log::info('Got user and role', ['user' => $user, $role => $role]);*/
        $filters = $this->divisionAndUserFilters($user->id, $division_id, $role, 'dp.division_id', 'u.id');
        /*\Log::info('Filters', ['filters' => $filters]);*/
        $division_filter = $filters->division;
        $driver_filter = $filters->driver;


        $query = <<<EOQ
SELECT u.id, u.first_name, u.last_name, dp.active, u.deleted_at
FROM users u
JOIN driver_profiles dp on dp.user_id = u.id
{$join}
WHERE dp.company_id = {$company_id} {$division_filter} {$driver_filter} 
AND dp.deleted_at IS NULL
ORDER BY u.last_name, u.first_name
EOQ;
        /*\Log::info('Driver Query', ['query' => $query]);*/
        $drivers = \DB::select(\DB::raw($query));

        // get counts
        foreach ($drivers as $d) {
            if (empty($d->deleted_at)) {
                $active_drivers_count += 1;
            } else {
                $inactive_drivers_count += 1;
            }
        }

        return response()->json([
            'drivers' => $drivers,
            'active_drivers_count' => $active_drivers_count,
            'inactive_drivers_count' => $inactive_drivers_count
        ]);
    }

    /**
     * This function gets drivers who have banking information, and also returns
     * their active account type, first and last names, username, deleted_at value,
     * as well as inactive and active counts.
     *
     * I use a raw query to
     *  a) leave the sensitive banking information on the server
     *  b) only execute a single query
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @internal param bool|int $inactive
     */
    public function getDriversWhoHaveBanking(Request $request)
    {
        $company_id = $request->company_id ?: -1;

        $active_drivers_count = 0;
        $inactive_drivers_count = 0;

        $query = (<<<EOQ
SELECT u.id, u.first_name, u.last_name, u.username, u.deleted_at, uba.account_type, dp.active
FROM users u
JOIN driver_profiles dp on dp.user_id = u.id
JOIN user_bank_accounts uba on uba.user_id = u.id
WHERE dp.company_id = {$company_id} and uba.deleted_at is null
AND dp.deleted_at IS NULL
ORDER BY u.last_name, u.first_name
EOQ
        );
        $drivers = \DB::select(\DB::raw($query));

        // get counts
        foreach ($drivers as $d) {
            if (empty($d->deleted_at)) {
                $active_drivers_count += 1;
            } else {
                $inactive_drivers_count += 1;
            }
        }

        return response()->json([
            'drivers' => $drivers,
            'active_drivers_count' => $active_drivers_count,
            'inactive_drivers_count' => $inactive_drivers_count
        ]);
    }


    /**
     * Managers are dependant upon company
     *
     * @param int $company_id
     * @return array
     */
    public function getManagers($company_id = -1, $from_session = false)
    {
        if ($company_id == -1):
            // no company selected, we will not retrieve all drivers
            // for all companies
            $managers = [];
        else:
            // return all managers, hide and show on the front end for inactive
            $managers = \DB::table('users')->join('manager_profiles', 'manager_profiles.user_id', 'users.id')
                ->select(
                    'manager_profiles.user_id',
                    'manager_profiles.manager_id as id', // this is lazy. how about manager_id as manager_id?
                    'manager_profiles.manager_id as manager_id',
                    'manager_profiles.assigned_drivers',
                    'users.first_name',
                    'users.last_name',
                    'users.deleted_at'
                )
                ->where('manager_profiles.company_id', $company_id)
                ->whereNull('manager_profiles.deleted_at')
                ->orderBy('users.last_name')->get();
        endif;

        return ['managers' => $managers];
    }

    /**
     * Return super users to super user select partial
     * @return array of super users
     */
    public function getSuperUsers()
    {
        $superUsers = Role::find(4)->users;
        return ['superUsers' => $superUsers];
    }


    /**
     * Administrators are dependant upon company
     *
     * @param int $company_id
     * @return array
     */
    public function getAdministrators($company_id = -1, $from_session = false)
    {
        if ($company_id == -1):
            // no company selected, we will not retrieve all drivers
            // for all companies
            $administrators = [];
        else:
            // return all administrators for company, show or hide inactive on front end
            $administrators = \DB::table('users')->join('administrator_profiles', 'administrator_profiles.user_id', 'users.id')
                ->select('administrator_profiles.user_id as user_id', 'administrator_profiles.administrator_id as id', 'users.first_name', 'users.last_name', 'users.deleted_at')
                ->where('administrator_profiles.company_id', $company_id)
                ->whereNull('administrator_profiles.deleted_at')
                ->orderBy('users.last_name')->get();
        endif;

        return ['administrators' => $administrators];
    }

    /**
     * Format user data for createUser so that User::create( return val ) can be called.
     * @param Request $request
     * @return array
     */
    private function formatUserData(Request $request)
    {
        //create the user
        $userData = [];
        $userData['username'] = $this->generateUsername($request->first_name, $request->last_name);
        $userData['email'] = strtolower($request->email);
        $userData['personal_email'] = sizeof($request->personal_email) > 0 ? strtolower($request->personal_email) : null;
        $userData['first_name'] = $this->titleCase($request->first_name);
        $userData['last_name'] = $this->titleCase($request->last_name);
        //$input['password'] = \Hash::make(str_random(8)); //TODO:  <-- should do this
        $userData['password'] = \Hash::make('secret'); //TODO: <-- instead of this
        return $userData;
    }

    /* Update the user status */
    public function updateProfileStatus(Request $request)
    {
        $user = User::find($request->id);

        \Debugbar::info(json_encode($request->id));
        return $request->id;
        if ($request->is_driver && $user->is_driver) {
            $user->diver->driverProfile->active = $request->diver->driverProfile->active;
        }
        if ($request->is_manager && $user->is_manager) {
            $user->manager->managerProfile->active = $request->manager->managerProfile->active;
        }
        if ($request->is_administrator && $user->is_administrator) {
            $user->administrator->administratorProfile->active = $request->administrator->administratorProfile->active;
        }
        try {
            $user->save();
        } catch (\Exception $e) {
            \Log::error('Error updating users table', ['error_message' => $e->getMessage()]);
            abort(400,
                json_encode(['update_user_failed' => ['Failed to update user profile. User update aborted, please contact CarData IT Support.']])
            );
        }

        return ['message' => 'successful status change'];
    }

    /**
     * Format address data for createUser so that Address:create( return val ) can be called.
     * @param Request $request
     * @param $user
     */
    private function formatAddressData(Request $request, $user)
    {
        $addressData = $request->driver['address'];
        $addressData['user_id'] = $user->id;
        $addressData['zip_postal'] = strtoupper(preg_replace('/\s*/', '', $addressData['zip_postal']));
        return $addressData;
    }

    /**
     * Formats the driver data during createUser or updateUser so that DriverProfile::create( return val )
     * can be called.
     *
     * @param Request $request
     * @param $user
     * @param $address
     * @return mixed
     */
    private function formatDriverData(Request $request, $user, $address)
    {
        // format the driverProfileData
        $driverProfileData = $request->driver['driverProfile'];
        \Log::info('$driverProfileData', [$driverProfileData]);
        // grab the user id and the address id recently created
        $driverProfileData['user_id'] = $user->id;
        $driverProfileData['address_id'] = $address->id;

        // format the territory list ( what was passed was an array keyed by territory id )
        $territory_list = "";
        foreach (array_keys($driverProfileData['territory_list']) as $territory_id) {
            $territory_list .= $territory_id . ',';
        }

        // trim last comma
        $driverProfileData['territory_list'] = substr($territory_list, 0, -1);

        if (empty($driverProfileData['next_insurance_review_date'])) {
            $driverProfileData['next_insurance_review_date'] = Company::find($request->company_id)->insurance->defaultDateOfNextReview();
        }

        if (empty($driverProfileData['next_license_review_date'])) {
            $driverProfileData['next_license_review_date'] = Company::find($request->company_id)->license->defaultDateOfNextReview();
        }


        return $driverProfileData;
    }

    /**
     * Function to assign managers to a given driver during createUser or updateUser
     *
     * @param Request $request
     * @param $user
     */
    private function assignManagersToDriver(Request $request, $user)
    {
        foreach (array_keys($request->driver['driverManager']) as $manager_id) {
            \DB::table('manager_driver')->insert([
                'manager_id' => $manager_id,
                'driver_id' => $user->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

    /**
     * Formats manager data for createUser or updateUser so that ManagerProfile::create( return val )
     * can be called
     *
     * @param Request $request
     * @param $user
     * @param null $driverProfile
     * @param bool $existing_manager
     * @return mixed
     */
    private function formatManagerData(Request $request, $user, $driverProfile = null, $existing_manager = false)
    {
        $managerProfileData = $request->manager['managerProfile'];

        // we just grab the next manager id and use it as our manager's manager id
        $managerProfileData['user_id'] = $user->id;
        $managerProfileData['manager_id'] = $existing_manager ? $managerProfileData['manager_id'] : ManagerProfile::max('manager_id') + 1;
//        $managerProfileData['address_id'] = !empty($driverProfile) ? $driverProfile->address()->id : null;
        $managerProfileData['address_id'] = null;

        // driver assignment
        $driver_ids = array_keys($request->manager['managerDriver']);

        //if no drivers will be assigned
        if (empty($driver_ids)) {
            $managerProfileData['assigned_drivers'] = 'none';
            //else if all
        } elseif ($driver_ids[0] == -2) {
            $managerProfileData['assigned_drivers'] = 'all';
            //else some
        } else {
            $managerProfileData['assigned_drivers'] = 'some';
        }

        return $managerProfileData;
    }

    /**
     * Called during createUser or updateUser to assign drivers to the provided manager profile.
     *
     * @param Request $request
     * @param $managerProfile
     */
    private function assignDriversToManager(Request $request, $managerProfile)
    {
        $driver_ids = array_keys($request->manager['managerDriver']);
        // only if some drivers are assigned do we insert any records
        if ($managerProfile->assigned_drivers == 'some') {
            foreach ($driver_ids as $driver_id) {
                \DB::table('manager_driver')->insert([
                    'manager_id' => $managerProfile->manager_id,
                    'driver_id' => $driver_id,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
    }

    /**
     * Returns an associative array of data (matching the administrator_profiles table) so that
     * AdministratorProfile::create( return_val ) can be called.
     *
     * @param Request $request
     * @param $user
     * @param null $driverProfile
     * @param bool $existing_administrator
     * @return mixed
     */
    private function formatAdministratorData(Request $request, $user, $driverProfile = null, $existing_administrator = false)
    {
        $administratorProfileData = $request->administrator['administratorProfile'];
        $administratorProfileData['user_id'] = $user->id;
        $administratorProfileData['administrator_id'] = $existing_administrator ? $administratorProfileData['administrator_id'] : AdministratorProfile::max('administrator_id') + 1;
        $administratorProfileData['address_id'] = !empty($driverProfile) ? $driverProfile->address_id : null;
        $division_ids = array_keys($request->administrator['administratorDivision']);

        //if no divisions will be assigned
        if (empty($division_ids)) {
            $administratorProfileData['assigned_divisions'] = 'none';
            //else if all
        } elseif ($division_ids[0] == -2) {
            $administratorProfileData['assigned_divisions'] = 'all';
            //else some
        } else {
            $administratorProfileData['assigned_divisions'] = 'some';
        }

        return $administratorProfileData;
    }

    /**
     * Called during createUser or updateUser.
     * Assigns divisions to the provided administratorProfile.
     *
     * @param Request $request
     * @param $administratorProfile
     */
    private function assignDivisonsToAdministrator(Request $request, $administratorProfile)
    {
        $division_ids = array_keys($request->administrator['administratorDivision']);
        // only if some drivers are assigned do we insert any records
        if ($administratorProfile->assigned_divisions == 'some') {
            foreach ($division_ids as $division_id) {
                \DB::table('administrator_division')->insert([
                    'administrator_id' => $administratorProfile->administrator_id,
                    'division_id' => $division_id,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
    }

    /**
     * Returns an associative array of data (matching the super_profiles table) so that
     * SuperProfile::create( return_val ) can be called.
     *
     * @param Request $request
     * @param $user
     * @return mixed
     */
    private function formatSuperData(Request $request, $user)
    {
        $superProfileData = $request->super['superProfile'];
        $superProfileData['user_id'] = $user->id;
        return $superProfileData;
    }

    /**
     * Creates a new user and any associated profiles.
     *
     * NOTES:
     *  Todo: documentation once complete on exact formatting of the request
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function createUser(Request $request)
    {
        // authorize
        try {
            $this->authorizeCreateOrUpdateUser($request);
        } catch (\Exception $e) {
            return response(
                $e->getMessage(),
                403
            );
        }

        // validate
        try {
            $this->validateCreateOrUpdateUser($request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        // create the user
        try {
            $user = User::create($this->formatUserData($request));
        } catch (\Exception $e) {
            \Log::error('Error creating user', ['error_message' => $e->getMessage()]);
            return response(
                json_encode(['user_generation_failed' => ['Failed to generate user, please contact CarData IT Support']]),
                400
            );
        }

        // get the assigned roles ready, 1 for driver, 2 for manager, 3 for administrator, 4 for super
        $assignedRoles = [];


        /**
         * Each of the following methods will take care of clean up if they fail,
         * and each of the following methods SHOULD throw errors, hopefully none are missed
         */
        if ($request->is_driver) {

            $driverProfile = $this->createNewDriver($request, $user, false);
            $address = $driverProfile->address();

            $assignedRoles[] = 1;

        }

        if ($request->is_manager) {

            $managerProfile = $this->createNewManager(
                $request,
                $user,
                isset($driverProfile) ? $driverProfile : null,
                false
            );

            $assignedRoles[] = 2;
        }

        if ($request->is_administrator) {

            $administratorProfile = $this->createNewAdministrator(
                $request,
                $user,
                isset($driverprofile) ? $driverProfile : null,
                isset($managerProfile) ? $managerProfile : null,
                false
            );

            $assignedRoles[] = 3;
        }

        if ($request->is_super) {


            try {
                $superProfile = SuperProfile::create($this->formatSuperData($request, $user));
            } catch (\Exception $e) {

                // if a driver profile was created, delete all the driver specific records
                if (isset($driverProfile)) {
                    $driverProfile->forceDelete();
                    $address->forceDelete();
                }

                // if a manager profile was created, delete the profile and any assigned drivers
                if (isset($managerProfile)) {
                    // delete any manager_driver records, and delete the manager, as well as the user
                    \DB::table('manager_driver')->where('manager_id', $managerProfile->manager_id)->delete();
                    $managerProfile->forceDelete();
                }

                // if an administrator profile was created, delete the profile any assigned divisions
                if (isset($administratorProfile)) {
                    // delete any administrator_division records, and delete the administrator, as well as the user
                    \DB::table('administrator_division')->where('administrator_id', $administratorProfile->administrator_id)->delete();
                    $administratorProfile->forceDelete();
                }

                $user->forceDelete();

                \Log::error('Error creating super profile', ['error_message' => $e->getMessage()]);
                return response(
                    json_encode(['super_profile_creation_failed' => ['Failed to create super profile. User creation aborted, please contact CarData IT Support.']]),
                    400
                );
            }


            $assignedRoles[] = 4;
        }

        $user->syncRoles($assignedRoles);


        if ($request->send_welcome_email) {
            $this->sendWelcomeEmail($user->id);
        }


        return [
            'message' => 'User ' . $user->username . ' (' . $user->id . ') created successfully!',
            'user_id' => $user->id,
        ];
    }

    /**
     * Called during updateUser to update the portion of the user stored in the users table
     *
     * @param Request $request
     * @return User|User[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    private function updateExistingUser(Request $request)
    {
        $user = User::find($request->id);
        $user->first_name = $this->titleCase($request->first_name);
        $user->last_name = $this->titleCase($request->last_name);
        $user->email = strtolower($request->email);
        $user->personal_email = sizeof($request->personal_email) > 0 ? strtolower($request->email) : null;

        // update the user
        try {
            $user->save();
        } catch (\Exception $e) {
            \Log::error('Error updating users table', ['error_message' => $e->getMessage()]);
            abort(400,
                json_encode(['update_user_failed' => ['Failed to update user profile. User update aborted, please contact CarData IT Support.']])
            );
        }

        return $user;
    }

    /**
     * Checks if a user exists, if not, it throws an exception.
     *
     * @param $user_id
     * @throws \Exception
     */
    private function checkUserExists($user_id)
    {
        if (empty(User::find($user_id))) {
            throw new \Exception();
        }
    }

    /**
     * Called during updateUser to update an existing driver
     * @param Request $request
     * @param $userCopy
     * @param $user
     * @param $oldDriverProfile
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    private function updateExistingDriver(Request $request, $user, $oldDriverProfile)
    {
        \Log::info('update driver object', [$request->all()]);
        // create the driver profile
        try {
            $old_driver_profile_id = $oldDriverProfile->id;
            $address = $oldDriverProfile->address();
            $oldDriverProfile->delete();
            $driverProfile = DriverProfile::create($this->formatDriverData($request, $user, $address));
        } catch (\Exception $e) {
            DriverProfile::withTrashed()->find($old_driver_profile_id)->restore();
            \Log::error('Error creating driver profile', ['error_message' => $e->getMessage()]);
            return response(
                json_encode(['driver_profile_creation_failed' => ['Failed to create driver profile. User creation aborted, please contact CarData IT Support.']]),
                400
            );
        }

        /**
         * Gather the current manager driver records. If new ones are being assigned, assign them.
         * If old ones have been unassigned, unassign them.
         *
         * I store the entire old history of manager driver records, because if this fails I will delete
         * everything, and restore from the old
         *
         */
        $oldManagerDriverRecordsForThisDriver = \DB::table('manager_driver')->where('driver_id', $user->id)->get();

        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // as this is with the query builder it is a HARD DELETE
        \DB::table('manager_driver')->where('driver_id', $user->id)->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        // hold onto the new manager profile ids and the old manager profile ids
        $oldManagerProfileIds = [];
        $newManagerProfileIds = [];

        /**
         * Loop through assigned managers.
         * If the assigned manager is not assigned to all drivers, insert a manager_driver
         * record.
         *
         * Update the manager profile to assigned_drivers = some.
         */

        try {
            foreach (array_keys($request->driver['driverManager']) as $manager_id) {
                // only insert records for those managers who do not have 'all' for assigned drivers
                \Log::info('managerDriver key', ['key' => $manager_id]);
                $managerProfile = ManagerProfile::where('manager_id', $manager_id)->first();

                if ($managerProfile->assigned_drivers != 'all') {
                    \DB::table('manager_driver')->insert([
                        'manager_id' => $manager_id,
                        'driver_id' => $user->id,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                    $oldManagerProfileIds[] = $managerProfile->id;
                    $newManagerProfile = $managerProfile->replicate();
                    unset($newManagerProfile->id);
                    $newManagerProfile->assigned_drivers = 'some';
                    $managerProfile->delete();
                    $newManagerProfile->save();
                    $newManagerProfileIds[] = $newManagerProfile->id;
                }
            }

        } catch (\Exception $e) {

            // don't bother me MySQL!!!
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            /**
             * Delete ALL manager_driver records involving this driver, and restore the old records
             */
            \DB::table('manager_driver')->where('driver_id', $user->id)->delete();
            foreach ($oldManagerDriverRecordsForThisDriver as $oldManagerDriverRecord) {
                \DB::table('manager_driver')->insert(json_decode(json_encode($oldManagerDriverRecord), true));
            }

            /**
             * Force delete any of the newly created manager profile records
             */
            foreach ($newManagerProfileIds as $id) {
                ManagerProfile::find($id)->forceDelete();
            }

            /**
             * Restore all of the previously present manager profile records
             */
            foreach ($oldManagerProfileIds as $id) {
                ManagerProfile::withTrashed()->find($id)->restore();
            }

            // force delete the newly created driver profile and restore the old record
            $driverProfile->forceDelete();

            if (!empty($oldDriverProfile)) {
                DriverProfile::withTrashed()->find($old_driver_profile_id)->restore();
            }

            // okay MySQL, you can bother me again
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            \Log::error('Error updating manager driver records', ['error_message' => $e->getTraceAsString()]);
            return response(
                json_encode(['manager_driver_update_failed' => ['Failed to assign managers to driver. User update aborted, please contact CarData IT Support.']]),
                400
            );
        }

        return $driverProfile;
    }

    /**
     * This function is called during createUser and updateUser to create a new driver.
     *
     * @param Request $request
     * @param $user
     * @param bool $called_during_update
     * @return DriverProfile|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Database\Eloquent\Model|\Symfony\Component\HttpFoundation\Response
     */
    private function createNewDriver(Request $request, $user, $called_during_update = false)
    {
        try {
            $address = Address::create($this->formatAddressData($request, $user));
        } catch (\Exception $e) {
            if (!$called_during_update) {
                $user->forceDelete();
            }
            \Log::error('Error creating address', ['error_message' => $e->getMessage()]);
            return response(
                json_encode(['address_creation_failed' => ['Failed to create address. User creation aborted, please contact CarData IT Support.']]),
                400
            );
        }

        // create the driver profile
        try {
            $driverProfile = DriverProfile::create($this->formatDriverData($request, $user, $address));
        } catch (\Exception $e) {
            // delete the recently created address
            $address->forceDelete();
            if (!$called_during_update) {
                $user->forceDelete();
            }
            \Log::error('Error creating driver profile', ['error_message' => $e->getMessage()]);
            return response(
                json_encode(['driver_profile_creation_failed' => ['Failed to create driver profile. User creation aborted, please contact CarData IT Support.']]),
                400
            );
        }

        try {
            $this->assignManagersToDriver($request, $user);
        } catch (\Exception $e) {
            // delete the recently created address
            $address->forceDelete();
            $driverProfile->forceDelete();
            if (!$called_during_update) {
                $user->forceDelete();
            }

            return response(
                json_encode(['driver_profile_creation_failed' => ['Failed to assign managers to driver. User creation aborted, please contact CarData IT Support.']]),
                400
            );
        }

        return $driverProfile;

    }

    /**
     * This function is called during updateUser to update an existing manager.
     *
     * @param Request $request
     * @param $user
     * @param $driverProfile
     * @return ManagerProfile|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Database\Eloquent\Model|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    private function updateExistingManager(Request $request, $user, $driverProfile)
    {
        /**
         * This is very important. We cannot assume the manager profile data has the correct id.
         * If this is a manager driver, and the manager previously had no assigned drivers and now has some then
         * a new manager profile was created. Thus manager profile data has an id pointing to an OLD profile.
         */
        $old_manager_profile_id = ManagerProfile::where('manager_id', $request->manager['managerProfile']['manager_id'])->first()->id;

        $managerProfileData = $this->formatManagerData($request, $user, $driverProfile, true);

        // unset the id
        unset($managerProfileData['id']);


        /**
         * Soft delete the old manager profile
         * get a copy of all old manager driver records for this manager
         * hard delete all of the old manager driver records
         * then try to create the new manager profile and all associated manager driver records
         * if it fails,
         * first undo the driver profile update if it occurred...
         * restore the old manager profile,
         * hard delete all manager driver records for this manager we inserted
         * then restore all of the old manager driver records of which we took copies
         */
        ManagerProfile::find($old_manager_profile_id)->delete();

        $oldManagerDriverRecordsForThisManager = \DB::table('manager_driver')->where('manager_id', $managerProfileData['manager_id'])->get();


        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // as this is with the query builder it is a HARD DELETE
        \DB::table('manager_driver')->where('manager_id', $managerProfileData['manager_id'])->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            $managerProfile = ManagerProfile::create($managerProfileData);

            $this->assignDriversToManager($request, $managerProfile);

        } catch (\Exception $e) {

            // don't bother me MySQL!!!
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            if ($managerProfile) {
                $managerProfile->forceDelete();
            }

            // restore the recently soft deleted manager profile record
            ManagerProfile::withTrashed()->find($old_manager_profile_id)->restore();

            // delete the recently inserted manager driver records
            \DB::table('manager_driver')->where('manager_id', $managerProfileData['manager_id'])->delete();

            // insert the old records
            foreach ($oldManagerDriverRecordsForThisManager as $oldManagerDriverRecord) {
                \DB::table('manager_driver')->insert(json_decode(json_encode($oldManagerDriverRecord), true));
            }

            // okay MySQL, you can bother me again
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            \Log::error('Error updating manager profile', ['error_message' => $e->getMessage()]);

            return response(
                json_encode(['manager_profile_update_failed' => ['Failed to update manager profile. User update aborted, please contact CarData IT Support.']]),
                400
            );
        }

        return $managerProfile;

    }

    /**
     * This function is called in the createUser and updateUser functions.
     * This function creates a new manager.
     *
     *
     * @param Request $request
     * @param $user
     * @param null $driverProfile
     * @param bool $called_during_update
     * @return ManagerProfile|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Database\Eloquent\Model|\Symfony\Component\HttpFoundation\Response
     */
    private function createNewManager(Request $request, $user, $driverProfile = null, $called_during_update = false)
    {
        try {
            $managerProfile = ManagerProfile::create($this->formatManagerData($request, $user, $driverProfile, false));
            $this->assignDriversToManager($request, $managerProfile);
        } catch (\Exception $e) {
            if ($called_during_update == false) {
                // if a driver profile was created, delete all the driver specific records
                if (isset($driverProfile)) {
                    $address = $driverProfile->address();
                    $driverProfile->forceDelete();
                    $address->forceDelete();
                }

                // delete any manager_driver records, and delete the manager, as well as the user
                \DB::table('manager_driver')->where('manager_id', $managerProfile->manager_id)->delete();
                if (!empty($managerProfile)) {
                    $managerProfile->forceDelete();
                }
                $user->forceDelete();
            } else {
                // this was called during update, so just remove the manager profile and manager_driver records
                if (!empty($managerProfile)) {
                    // delete any manager_driver records, and delete the manager, as well as the user
                    \DB::table('manager_driver')->where('manager_id', $managerProfile->manager_id)->delete();
                    $managerProfile->forceDelete();
                }
            }

            \Log::error('Error creating manager profile', ['error_message' => $e->getMessage()]);
            return response(
                json_encode(['manager_profile_creation_failed' => ['Failed to create manager profile. User creation aborted, please contact CarData IT Support.']]),
                400
            );
        }

        return $managerProfile;

    }

    /**
     * This private function is used in the createUser and updateUser functions.
     * It creates a new administrator
     *
     * @param $request
     * @param $user
     * @param null $driverProfile
     * @param null $managerProfile
     * @param bool $called_during_update
     * @return AdministratorProfile|\Illuminate\Database\Eloquent\Model|void\
     */
    private function createNewAdministrator($request, $user, $driverProfile = null, $managerProfile = null, $called_during_update = false)
    {
        try {
            $administratorProfile = AdministratorProfile::create($this->formatAdministratorData($request, $user, $driverProfile));
            $this->assignDivisonsToAdministrator($request, $administratorProfile);
        } catch (\Exception $e) {
            if ($called_during_update == false) {
                // if a driver profile was created, delete all the driver specific records
                if (isset($driverProfile)) {
                    $address = $driverProfile->address();
                    $driverProfile->forceDelete();
                    $address->forceDelete();
                }

                // if a manager profile was created, delete the profile and any assigned drivers
                if (isset($managerProfile)) {
                    // delete any manager_driver records, and delete the manager, as well as the user
                    \DB::table('manager_driver')->where('manager_id', $managerProfile->manager_id)->delete();
                    $managerProfile->forceDelete();
                }

                // delete any administrator_division records, and delete the administrator, as well as the user
                \DB::table('administrator_division')->where('administrator_id', $administratorProfile->administrator_id)->delete();
                $administratorProfile->forceDelete();
                $user->forceDelete();
            } else {
                if (!empty($administratorProfile)) {
                    \DB::table('administrator_division')->where('administrator_id')->delete();
                }
            }

            \Log::error('Error creating administrator profile', ['error_message' => $e->getMessage()]);
            return abort(
                400,
                json_encode(['administrator_profile_creation_failed' => ['Failed to create administrator profile. User creation aborted, please contact CarData IT Support.']])
            );
        }

        return $administratorProfile;
    }

    /**
     * This function is used internally to update an existing administrator.
     * It is called by updateUser.
     *
     * Probably don't just call this all willy nilly.
     *
     * @param Request $request
     * @param $user
     * @param null $driverProfile
     * @return AdministratorProfile|\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    private function updateExistingAdministrator(Request $request, $user, $driverProfile = null)
    {
        // format the data correctly
        $administratorProfileData = $this->formatAdministratorData($request, $user, $driverProfile, true);

        //old id for restoration
        $oldAdministratorProfile = AdministratorProfile::where('administrator_id', $administratorProfileData['administrator_id'])->first();
        $old_administrator_profile_id = $oldAdministratorProfile->id;

        // unset the id
        unset($administratorProfileData['id']);

        /**
         * Soft delete the old administrator profile
         * get a copy of all old administrator division records for this administrator
         * hard delete all of the old administrator_division records
         * then try to create the new administrator profile and all associated administrator_division records
         */
        $oldAdministratorProfile->delete();

        $oldAdministratorDivisionRecords = \DB::table('administrator_division')->where('administrator_id', $administratorProfileData['administrator_id'])->get();

        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // as this is with the query builder it is a HARD DELETE
        \DB::table('administrator_division')->where('administrator_id', $administratorProfileData['administrator_id'])->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            $administratorProfile = AdministratorProfile::create($administratorProfileData);
            $this->assignDivisonsToAdministrator($request, $administratorProfile);
        } catch (\Exception $e) {
            if (!empty($administratorProfile)) {
                // delete any manager_driver records, and delete the manager, as well as the user
                \DB::table('administrator_division')->where('administrator_id', $administratorProfile->administrator_id)->delete();
                $administratorProfile->forceDelete();

                AdministratorProfile::withTrashed()->find($old_administrator_profile_id)->restore();
                // insert the old records
                foreach ($oldAdministratorDivisionRecords as $oldAdministratorDivisionRecord) {
                    \DB::table('administrator_division')->insert(json_decode(json_encode($oldAdministratorDivisionRecord), true));
                }
            }

            // don't bother me MySQL!!!
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            \Log::error('Error updating administrator profile', ['error_message' => $e->getMessage()]);
            abort(400, json_encode(['update_administrator_failed' => ['Failed to update administrator. Please contact CarData IT Support.']]));
        }

        return $administratorProfile;
    }

    /**
     * This function is called on the AddEditUserPage
     * It is used to update an existing user.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function updateUser(Request $request)
    {
        // authorize
        try {
            $this->authorizeCreateOrUpdateUser($request);
        } catch (\Exception $e) {
            return response(
                $e->getMessage(),
//                json_encode(['Forbidden' => ['You are not authorized to perform this action']]),
                403
            );
        }

        // validate
        try {
            $this->validateCreateOrUpdateUser($request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        // update user
        try {
            $this->checkUserExists($request->id);
            $userCopy = json_decode(json_encode(User::find($request->id)));
            $user = $this->updateExistingUser($request);
        } catch (\Exception $e) {
            return response(json_encode(['update_user_failed' => ['Failed to update user. User not found, please contact CarData IT Support.']]), 400);
        }


        // get the assigned roles ready, 1 for driver, 2 for manager, 3 for administrator, 4 for super
        $assignedRoles = [];

        if ($request->is_driver) {

            // old driver profile
            $oldDriverProfile = DriverProfile::where('user_id', $user->id)->first();

            // update or create
            if (!empty($oldDriverProfile)) {
                $driverProfile = $this->updateExistingDriver($request, $user, $oldDriverProfile);
            } else {
                $driverProfile = $this->createNewDriver($request, $user, true);
            }

            $assignedRoles[] = 1;

        } // end driver

        if ($request->is_manager) {

            $oldManagerProfile = ManagerProfile::where('user_id', $user->id)
                ->where('company_id', $request->company_id)
                ->first();

            // update or create
            if (!empty($oldManagerProfile)) {
                $managerProfile = $this->updateExistingManager($request, $user, isset($driverProfile) ? $driverProfile : null);
            } else {
                $managerProfile = $this->createNewManager($request, $user, isset($driverProfile) ? $driverProfile : null, true);
            }

            $assignedRoles[] = 2;
        } // end manager


        if ($request->is_administrator) {

            $oldAdministratorProfile = AdministratorProfile::where('user_id', $user->id)
                ->where('company_id', $request->company_id)
                ->first();

            // update or create
            if (!empty($oldAdministratorProfile)) {
                $administratorProfile = $this->updateExistingAdministrator($request, $user, isset($driverProfile) ? $driverProfile : null);
            } else {
                $administratorProfile = $this->createNewAdministrator(
                    $request,
                    $user,
                    isset($driverprofile) ? $driverProfile : null,
                    isset($managerProfile) ? $managerProfile : null,
                    false
                );
            }

            $assignedRoles[] = 3;
        } // end administrator

        if ($request->is_super) {
            $superProfileData = $request->super['superProfile'];
            $superProfileData['user_id'] = $user->id;
            unset($superProfileData['id']);


            $oldSuperProfile = SuperProfile::where('user_id', $user->id)->first();
            $old_super_profile_id = $oldSuperProfile->id;

            $oldSuperProfile->delete();

            try {
                $superProfile = SuperProfile::create($superProfileData);
            } catch (\Exception $e) {

                // No other profiles should be created. If we fail, restore the old super profile and user.

                // don't bother me MySQL!!!
                \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

                // restore the users previous settings
                $user->update([
                    'first_name' => $userCopy->first_name,
                    'last_name' => $userCopy->last_name,
                    'email' => $userCopy->email,
                    'personal_email' => $userCopy->personal_email,
                ]);

                SuperProfile::withTrashed()->find($old_super_profile_id)->restore();

                \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

                \Log::error('Error updating super profile', ['error_message' => $e->getMessage()]);
                return response(
                    json_encode(['super_profile_update_failed' => ['Failed to update super profile. User update aborted, please contact CarData IT Support.']]),
                    400
                );
            }


            $assignedRoles[] = 4;
        }


        $user->syncRoles($assignedRoles);

        // send a welcome email with a link to create a new password
        if ($request->send_welcome_email) {
            $this->sendWelcomeEmail($user->id);
        }

        // generate a new username
        if ($request->generate_new_username) {
            $user->username = $this->generateUsername($user->first_name, $user->last_name);
            $user->save();

            $data = new \stdClass();
            $data->user = $user;
            $data->coordinator = $this->getCoordinatorData($request->company_id);
            $data->from = ALERTS_EMAIL_ADDRESS;

            @Mail::to($user->email)->send(new UsernameChangedEmail($data));
        }


        return ['message' => 'User ' . $user->username . ' (' . $user->id . ') updated successfully!'];

    }


    /**
     * This function returns all the profiles, with their id (driver_profile_id, manager_id, admin_id)
     * company name.
     *
     * @param $user_id
     * @return array
     */
    public function getUserDataForDeactivation($user_id)
    {
        //dd($user_id);
        $user = User::find($user_id);

        // only one driver profile is possible
        $driver = null;

        // it is possible to have more than one manager, and more than one administrator profile
        $managers = [];
        $administrators = [];

        // only one super profile is possible
        $super = null;


        // if user is a driver, get the driver data
        if ($user->hasRole(1) && $user->driverProfile->active) {
            $driver = [
                'id' => $user->driverProfile->id,
                'company_name' => $user->driverProfile->company->name
            ];
        }

        // if the user is a manager, get the manager data
        if ($user->hasRole(2)) {
            foreach ($user->managerProfiles as $managerProfile) {
                if (!$managerProfile->active) {
                    continue;
                }
                $managers[] = [
                    'id' => $managerProfile->manager_id,
                    'company_name' => $managerProfile->company->name
                ];
            }
        }

        // if the user is an administrator, get the administrator data
        if ($user->hasRole(3)) {
            foreach ($user->administratorProfiles as $administratorProfile) {
                if (!$administratorProfile->active) {
                    continue;
                }
                //$administratorProfile[] = [ // incorrect var name - davery
                $administrators[] = [
                    'id' => $administratorProfile->administrator_id,
                    'company_name' => $administratorProfile->company->name
                ];
            }
        }

        // if the user is an super, get the super data
        if ($user->hasRole(4)) {
            $super = [
                'id' => $user->superProfile->id,
                'company_name' => 'CarData'
            ];
        }

        return [
            'driver' => $driver,
            'managers' => $managers,
            'administrators' => $administrators,
            'super' => $super
        ];

    }

    /**
     * Returns all data required for the user reactivation modal
     *
     * @param $user_id
     * @return array
     */
    public function getUserDataForReactivation($user_id)
    {

        $user = User::find($user_id);

        // only one driver profile is possible
        $driver = null;

        // it is possible to have more than one manager, and more than one administrator profile
        $managers = [];
        $administrators = [];

        // only one super profile is possible
        $super = null;


        // if user is a driver, get the driver data
        if ($user->hasRole(1) && !$user->driverProfile->active) {
            $driver = [
                'id' => $user->driverProfile->id,
                'company_name' => $user->driverProfile->company->name
            ];
        }

        // if the user is a manager, get the manager data
        if ($user->hasRole(2)) {
            foreach ($user->managerProfiles as $managerProfile) {
                if ($managerProfile->active) {
                    continue;
                }
                $managers[] = [
                    'id' => $managerProfile->manager_id,
                    'company_name' => $managerProfile->company->name
                ];
            }
        }

        // if the user is an administrator, get the administrator data
        if ($user->hasRole(3)) {
            foreach ($user->administratorProfiles as $administratorProfile) {
                if ($administratorProfile->active) {
                    continue;
                }
                $administratorProfile[] = [
                    'id' => $administratorProfile->administrator_id,
                    'company_name' => $administratorProfile->company->name
                ];
            }
        }

        // if the user is an super, get the super data
        if ($user->hasRole(4)) {
            $super = [
                'id' => $user->superProfile->id,
                'company_name' => 'CarData'
            ];
        }

        return [
            'driver' => $driver,
            'managers' => $managers,
            'administrators' => $administrators,
            'super' => $super
        ];

    }


    /**
     * To deactivate a user, a user_id and  two objects are required.
     *
     *  toDeactivate = {
     *      driver_ids: [a, b, ... ],
     *      manager_ids: [c, d, ... ],              toDeactivate contains ids of all profiles to be soft deleted
     *      administrator_ids: [e, f, ... ],
     *      super_ids: [g, h, ... ],
     *  }
     *
     *  and
     *
     *  status: [x, y, ... ]                        status contains the reason for deactivation of driver profiles
     *                                                 by the id's in driver_street_light_statuses
     *
     * You may pass status as simply an empty array ( [] ),
     *  as long as you are not deactivating driver profiles.
     *
     *
     * @param Request $request
     * @return string message
     */
    public function deactivateUser(Request $request)
    {
        $user = User::find($request->user_id);

        // deactivate driver profile if selected
        if (sizeof($request->toDeactivate['driver_ids']) == 1) {

            $streetLightCalculator = new StreetLightCalculator();

            $driverProfile = $user->driverProfile;
            $newDriverProfile = $driverProfile->replicate();
            $driverProfile->delete();
            $newDriverProfile->street_light_id = $streetLightCalculator->calculateDeactivationStatus($request->status);

            $newDriverProfile->active = 0;
            $newDriverProfile->save();
        }

        // deactivate the selected manager profiles
        foreach ($request->toDeactivate['manager_ids'] as $manager_id) {
            $managerProfile = ManagerProfile::where('user_id', $request->user_id)
                ->where('manager_id', $manager_id)
                ->first();

            $newManagerProfile = $managerProfile->replicate();
            $managerProfile->delete();
            $newManagerProfile->active = 0;
            $newManagerProfile->save();
        }

        // deactivate selected administrator profiles
        foreach ($request->toDeactivate['administrator_ids'] as $administrator_id) {
            $administratorProfile = AdministratorProfile::where('user_id', $request->user_id)
                ->where('administrator_id', $administrator_id)
                ->first();

            $newAdministratorProfile = $administratorProfile->replicate();
            $administratorProfile->delete();
            $newAdministratorProfile->active = 0;
            $newAdministratorProfile->save();
        }

        // deactivate super profile if selected
        if (sizeof($request->toDeactivate['super_ids']) == 1) {
            $superProfile = $user->superProfile;
            $newSuperProfile = $user->superProfile->replicate();
            $superProfile->delete();
            $newSuperProfile->active = 0;
            $newSuperProfile->save();
        }


        return ['message' => 'successful deactivation'];

    }

    /**
     * Reactivate them users yo
     * toReactivate = {
     *      driver_ids: [a, b, ... ],
     *      manager_ids: [c, d, ... ],              toReactivate contains ids of all profiles to be soft deleted
     *      administrator_ids: [e, f, ... ],
     *      super_ids: [g, h, ... ],
     *  }
     *
     * NOTE! Reactivated driver profiles are assigned YELLOW NEW DRIVER STATUS (street_light_id = 1)
     *
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function reactivateUser(Request $request)
    {
        $user = User::find($request->user_id);

        // deactivate driver profile if selected
        if (sizeof($request->toReactivate['driver_ids']) == 1) {


            $driverProfile = $user->driverProfile;
            $newDriverProfile = $driverProfile->replicate();
            $driverProfile->delete();

            // send em back to yellow
            $newDriverProfile->street_light_id = 1;

            $newDriverProfile->active = 1;
            $newDriverProfile->save();
        }

        // deactivate the selected manager profiles
        foreach ($request->toReactivate['manager_ids'] as $manager_id) {
            $managerProfile = ManagerProfile::where('user_id', $request->user_id)
                ->where('manager_id', $manager_id)
                ->first();

            $newManagerProfile = $managerProfile->replicate();
            $managerProfile->delete();
            $newManagerProfile->active = 1;
            $newManagerProfile->save();
        }

        // deactivate selected administrator profiles
        foreach ($request->toReactivate['administrator_ids'] as $administrator_id) {
            $administratorProfile = AdministratorProfile::where('user_id', $request->user_id)
                ->where('administrator_id', $administrator_id)
                ->first();

            $newAdministratorProfile = $administratorProfile->replicate();
            $administratorProfile->delete();
            $newAdministratorProfile->active = 1;
            $newAdministratorProfile->save();
        }

        // deactivate super profile if selected
        if (sizeof($request->toReactivate['super_ids']) == 1) {
            $superProfile = $user->superProfile;
            $newSuperProfile = $user->superProfile->replicate();
            $superProfile->delete();
            $newSuperProfile->active = 1;
            $newSuperProfile->save();
        }


        return ['message' => 'successful reactivation'];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        //
    }

    public function getUser($user_id = -1, $user_type_id = -1)
    {
        $user = null;

        \Debugbar::info('about to test on user_id ' . $user_id);
        if ($user_id > 0) {
            switch ($user_type_id) {
                case 2:
                    $user_id = ManagerProfile::where('manager_id', $user_id)->pluck('user_id')->first();
                    break;
                case 3:
                    $user_id = AdministratorProfile::where('administrator_id', $user_id)->pluck('user_id')->first();
                    break;
                case 4:
                    $user_id = SuperProfile::where('super_id', $user_id)->pluck('user_id')->first();
                    break;
            }
            \Debugbar::info("user_id is " . $user_id);
            $user = User::where('id', $user_id)
                ->with('driverProfile')
                ->with('address')
                ->with('managerProfiles')
                ->with('administratorProfiles')
                ->first();
        }

        $user->is_driver = $user->hasRole(1);
        $user->is_manager = $user->hasRole(2) || $user->isManager();
        $user->is_administrator = $user->hasRole(3) || $user->isAdmin();
        $user->is_super = $user->hasRole(4);

        $user->company = $user->company();

        return ['user' => $user];
    }

    public function getMe($user_id = null, $user_type_id = null)
    {
        $impersonator_id = session()->get('authenticated_user_id');

        // either both user_id and user_type_id are null or both have a value
        if (empty($user_id)) {
            \Debugbar::info('in if');
            if (session()->has('impersonate')) {
                $user_id = User::find(session()->get('impersonate'))->id;
            } else {
                $user_id = \Auth::user()->id;
            }
        } else {
            \Debugbar::info('in else');
            switch ($user_type_id) {
                case 1:
                    $user_id = $user_id; // = DriverProfile::where('user_id', $user_id)->first()->user;
                    break;
                case 2:
                    $user_id = ManagerProfile::where('manager_id', $user_id)->pluck('user_id')[0];
                    break;
                case 3:
                    $user_id = AdministratorProfile::where('administrator_id', $user_id)->pluck('user_id')[0];
                    break;
                case 4:
                    $user_id = SuperProfile::where('super_id', $user_id)->pluck('user_id')[0];
                    break;
                case null:
                    $user_id = $user_id; // redundant
                    break;
            }
        }

        \Debugbar::info('stuff');

        $user = User::find($user_id);
        if ($impersonator_id != $user_id) {
            $user->impersonator_id = $impersonator_id;
        }
        $user->driverProfile = $user->driverProfile;
        $user->managerProfiles = $user->managerProfiles;
        $user->administratorProfiles = $user->administratorProfiles;
        $user->superProfile = $user->superProfile;


        // default user types, these set to true in role test below
        $user->is_driver = false;
        $user->is_manager = false;
        $user->is_administrator = false;
        $user->is_super = false;

        $user->company = $user->company();

        // TODO add financial part to get direct_pay_day,
        if ($user->hasRole(1)) {
            $user->is_driver = true;
            $user->address = $user->address;
            $user->driverProfile->mileageBand = $user->driverProfile->mileageBand;
            $user->driverProfile->company_name = $user->driverProfile->company->name;
            $user->mileage_entry_method = $user->driverProfile->company->mileage->mileage_entry_method;
            $user->company_plan = $user->driverProfile->company->options->service_plan;
            $user->mileage_lock_day = session()->has('impersonate') // different for driver and admin or super
                ? $user->driverProfile->company->dates->mileage_lock_all_day
                : $user->driverProfile->company->dates->mileage_lock_driver_day;
            $user->driver_lock_day = $user->driverProfile->company->dates->mileage_lock_driver_day;        // to display when mileage is locked

            $user->address->stateProvince = $user->address->stateProvince;
            $user->address->time_zone = $user->address->country_id == 2
                ? TimeZone::find($user->address->stateProvince->time_zone_id)
                : TimeZone::find(ZipCode::find($user->address->zip_postal)->time_zone_id);
        }

        if ($user->hasRole(2)) {
            $user->is_manager = true;
            foreach ($user->managerProfiles as $profile) {
                $profile->company_name = $profile->company->name;
            }
        }

        if ($user->hasRole(3)) {
            $user->is_administrator = true;
            foreach ($user->administratorProfiles as $profile) {
                $profile->company_name = $profile->company->name;
            }
        }

        if ($user->hasRole(4)) {
            $user->is_super = true;
            $user->session_company_id = session()->get('session_company_id', -1);
        }

        $user->userType = session()->get('active_role');
        $user->view = session()->get('view', 'users');
        $user->subView = session()->get('subView', '');

        return json_encode(['user' => $user], JSON_NUMERIC_CHECK);
    }

    public function getUserByType(Request $request)
    {
        $user = null;
        $type = $request->userType;
        $id = $request->id;

        \Debugbar::info(['type' => $type, 'id' => $id]);

        switch ($type) {
            case 'driver':
                $user = User::find($id);
                break;
            case 'manager':
                $manager = ManagerProfile::where('manager_id', $id)->first();
                $user = $manager->user;
                break;
            case 'administrator':
                $administrator = AdministratorProfile::where('administrator_id', $id)->first();
                $user = $administrator->user;
                break;
        }

        \Debugbar::info($user);
        return response()->json(['user' => $user]);
    }

    /**
     * Get details on individual driver.
     * We allow inactive.
     *
     * @param $driver_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDriver($driver_id)
    {
        $driver = User::where('id', $driver_id)->with('driverProfile')->first();
        return response()->json(['driver' => $driver]);
    }

    /**
     * Get details on individual manager.
     * We allow inactive.
     *
     * @param $manager_id
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function getManager($manager_id)
    {
        $manager = User::join('manager_profiles', 'manager_profiles.user_id', 'users.id')->select('users.first_name', 'users.last_name'
            , 'manager_profiles.manager_id as id', 'manager_profiles.user_id as user_id', 'users.deleted_at')->where('manager_profiles.manager_id', $manager_id)->first();

        return response()->json([
            'manager' => $manager
        ]);
    }

    /**
     * Get details on individual administrator.
     * We allow inactive.
     *
     * @param $administrator_id
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function getAdministrator($administrator_id)
    {
        $administrator = User::join('administrator_profiles', 'administrator_profiles.user_id', 'users.id')->select('users.first_name', 'users.last_name'
            , 'administrator_profiles.administrator_id as id', 'users.deleted_at', 'administrator_profiles.user_id as user_id')->where('administrator_profiles.administrator_id', $administrator_id)->first();

        \Debugbar::info($administrator);
        return response()->json([
            'administrator' => $administrator
        ]);
    }

    /**
     * Returns the role names and id's to be used in the UserTypeSelect
     *
     * @return array
     */
    public function getUserTypes()
    {
        $roles = Role::all();
        return ['userTypes' => $roles];
    }

    protected function titleCase($string)
    {
        #region
        $word_splitters = array(' ', '-', "O'", "L'", "D'", 'St.', 'Mc');
        $lowercase_exceptions = array('the', 'van', 'den', 'von', 'und', 'der', 'de', 'da', 'of', 'and', "l'", "d'");
        $uppercase_exceptions = array('III', 'IV', 'VI', 'VII', 'VIII', 'IX');

        $string = strtolower($string);
        foreach ($word_splitters as $delimiter) {
            $words = explode($delimiter, $string);
            $newwords = array();
            foreach ($words as $word) {
                if (in_array(strtoupper($word), $uppercase_exceptions))
                    $word = strtoupper($word);
                else
                    if (!in_array($word, $lowercase_exceptions))
                        $word = ucfirst($word);

                $newwords[] = $word;
            }

            if (in_array(strtolower($delimiter), $lowercase_exceptions))
                $delimiter = strtolower($delimiter);

            $string = join($delimiter, $newwords);
        }
        #endregion
        // do stuff to the original input and return modified version
        return $string;
    }

    public
    function validateStoreUpdateRequest(Request $request, $id = null)
    {
        $data = (array)$request->all();
        $validating_driver = $request['is_driver'];
        $validating_manager = $request['is_manager'];
        $validating_administrator = $request['is_administrator'];
        $validating_super = $request['is_super'];

//        //ensure a user profile of at least one kind is being created
//        $userProfileValidator = Validator::make($data, [
//            'is_driver' => 'required_without_all:is_manager,is_administrator,is_super',
//            'is_manager' => 'required_without_all:is_driver,is_administrator,is_super',
//            'is_administrator' => 'required_without_all:is_manager,is_driver,is_super',
//            'is_super' => 'required_without_all:is_manager,is_administrator,is_driver',
//            'company_id' => 'required|min:0',
//            'username' => 'required|unique:users,' . $id ,
//            'email' => 'required|email',
//            'first_name' => 'required',
//            'last_name' => 'required',
//        ]);
//        $userProfileValidator->validate();

        //first, validate driver if available
        if ($validating_driver) {

            //todo remove the pushing and use . notation for array validation
            //$data['job_title'] = $request['driver']['driverProfile']['job_title'];
            //$data['cost_center'] = $request['driver']['driverProfile']['cost_center'];
            $data['company_id'] = $request['company_id'];
            $data['division_id'] = $request['driver']['driverProfile']['division_id'];
            $data['street'] = $request['driver']['address']['street'];
            $data['city'] = $request['driver']['address']['city'];
            $data['zip_postal'] = $request['driver']['address']['zip_postal'];
            $data['state_province_id'] = $request['driver']['address']['state_province_id'];
            $data['country_id'] = $request['driver']['address']['country_id'];
            $data['vehicle_profile_id'] = $request['driver']['driverProfile']['vehicle_profile_id'];
            $data['mileage_band_id'] = $request['driver']['driverProfile']['mileage_band_id'];
            $data['territory_type_id'] = $request['driver']['driverProfile']['territory_type_id'];
            $data['territory_list'] = $request['driver']['driverProfile']['territory_list'];
            $data['start_date'] = $request['driver']['driverProfile']['start_date'];


            $driverValidator = Validator::make($data, [
                'is_driver' => 'required_without_all:is_manager,is_administrator,is_super',
                'is_manager' => 'required_without_all:is_driver,is_administrator,is_super',
                'is_administrator' => 'required_without_all:is_manager,is_driver,is_super',
                'is_super' => 'required_without_all:is_manager,is_administrator,is_driver',
                'company_id' => 'required|min:0',
                'username' => 'required|unique:users,username,' . $id,
                'email' => 'required|email',
                'first_name' => 'required',
                'last_name' => 'required',

                // 'division_id' => 'required|min:0',

                'street' => 'required_with_all:is_driver',
                'city' => 'required_with_all:is_driver',
                'state_province_id' => 'required_with:is_driver|min:0',
                'zip_postal' => 'required_with:is_driver',
                'country_id' => 'required_with:is_driver|min:0',
                //'employee_number' => 'required_with:is_driver',
                //'job_title' => 'required_with:is_driver',
                //'cost_center' => 'required_with:is_driver',
                'vehicle_profile_id' => 'required_with:is_driver|min:0',
                'mileage_band_id' => 'required_with:is_driver|min:0',
                'territory_type_id' => 'required_with:is_driver|min:0',
                'start_date' => 'required'
            ]);
            $driverValidator->sometimes(['division_id', 'country_id', 'vehicle_profile_id', 'mileage_band_id', 'territory_type_id', 'company_id'], 'required|numeric|min:1', function ($request) {
                return $request['is_driver'] === true;
            });
            $driverValidator->validate();
        }

        //then if manager is being created, validate manager
        if ($validating_manager) {
            //TODO: determine what to validate
            $managerValidator = Validator::make($data['manager']['managerProfile'], [

                //VALIDATION HERE

            ]);

            $managerValidator->validate();
        }

        //then if administrator is being created, validate administrator
        if ($validating_administrator === true) {
            \Debugbar::info($data);

            //Todo: check if all values are boolean, divisions required validate phone
            $administratorValidator = Validator::make($data, [
                'is_driver' => 'required_without_all:is_manager,is_administrator,is_super',
                'is_manager' => 'required_without_all:is_driver,is_administrator,is_super',
                'is_administrator' => 'required_without_all:is_manager,is_driver,is_super',
                'is_super' => 'required_without_all:is_manager,is_administrator,is_driver',
                'company_id' => 'required|min:0',
                'username' => 'required|unique:users,username,' . $id,
                'email' => 'required|email',
                'first_name' => 'required',
                'last_name' => 'required',

                //validates array
                'administrator.administratorDivision.divisions' => 'filled'

            ]);

            $administratorValidator->validate();
        }

        //then if super is being created, validate super
        if ($validating_super) {
            //TODO: determine what makes a user super
            $superValidator = Validator::make($data['super']['superProfile'], [

                //VALIDATION HERE
                //ensure company is a cardata company

            ]);

            $superValidator->validate();
        }
    }

    public function findUser(Request $request)
    {
        $company_id = $request->company_id;
        $search_parameter = $request->search_parameter;

        if (!empty($search_parameter)) {
            $select = <<<EOQ
SELECT u.id, u.first_name, u.last_name, u.email, u.username, u.deleted_at,
dp.user_id as `driver_id`, mp.manager_id as `manager_id`, ap.administrator_id as `administrator_id`
, case when count(dp.id) > 0 then true else false end as `is_driver`
, case when count(dp.id) > 0 then dp.company_id else -1 end as `driver_company_id`
, case when count(mp.id) > 0 then true else false end as `is_manager`
, case when count(mp.id) > 0 then mp.company_id else -1 end as `manager_company_id`
, case when count(ap.id) > 0 then true else false end as `is_administrator`
, case when count(ap.id) > 0 then ap.company_id else -1 end as `administrator_company_id`
, dp.active as `active_driver`
, mp.active as `active_manager`
, ap.active as `active_administrator`
, dp.street_light_id as `street_light_id`
, dp.division_id
from users u
left join driver_profiles dp on dp.user_id = u.id and dp.deleted_at is null
left join manager_profiles mp on mp.user_id = u.id and mp.deleted_at is null
left join administrator_profiles ap on ap.user_id = u.id and ap.deleted_at is null
where (dp.deleted_at is null || mp.deleted_at is null || ap.deleted_at is null) and (u.first_name LIKE '%{$search_parameter}%' OR u.last_name LIKE '%{$search_parameter}%' OR u.username LIKE '%{$search_parameter}%' OR u.email LIKE '%{$search_parameter}%')
group by u.id, dp.active, u.first_name, u.last_name, u.email, u.username, mp.active, ap.active, u.deleted_at, dp.user_id, mp.manager_id, ap.administrator_id, dp.company_id, mp.company_id, ap.company_id, dp.division_id, dp.street_light_id 
order by u.last_name, u.first_name
EOQ;


        } elseif (!empty($company_id)) {
            $select = <<<EOQ
SELECT u.id, u.first_name, u.last_name, u.email, u.username, u.deleted_at,
dp.user_id as `driver_id`, mp.manager_id as `manager_id`, ap.administrator_id as `administrator_id`
, case when count(dp.id) > 0 then true else false end as `is_driver`
, case when count(dp.id) > 0 then dp.company_id else -1 end as `driver_company_id`
, case when count(mp.id) > 0 then true else false end as `is_manager`
, case when count(mp.id) > 0 then mp.company_id else -1 end as `manager_company_id`
, case when count(ap.id) > 0 then true else false end as `is_administrator`
, case when count(ap.id) > 0 then ap.company_id else -1 end as `administrator_company_id`
, dp.active as `active_driver`
, mp.active as `active_manager`
, ap.active as `active_administrator`
, dp.street_light_id as `street_light_id`
, dp.division_id
from users u
left join driver_profiles dp on dp.user_id = u.id and dp.deleted_at is null
left join manager_profiles mp on mp.user_id = u.id and mp.deleted_at is null
left join administrator_profiles ap on ap.user_id = u.id and ap.deleted_at is null
where (dp.deleted_at is null || mp.deleted_at is null || ap.deleted_at is null) and {$company_id} in (dp.company_id, mp.company_id, ap.company_id)
group by u.id, dp.active, u.first_name, u.last_name, u.email, u.username, mp.active, ap.active, u.deleted_at, dp.user_id, mp.manager_id, ap.administrator_id, dp.company_id, mp.company_id, ap.company_id, dp.division_id, dp.street_light_id 
order by u.last_name, u.first_name
EOQ;
        } else {
            return;
        }

        \Debugbar::info($select);
        $users = \DB::select(\DB::raw($select));

        return $users;
    }

    public function resolveUserId($id, $type)
    {
        $return_id = -1;

        switch ($type) {
            case 1:
            case 'driver':
                $return_id = DriverProfile::find($id)->user_id;
                break;
            case 2:
            case 'manager':
                $return_id = ManagerProfile::find($id)->user_id;
                break;
            case 3:
            case 'administrator':
                $return_id = AdministratorProfile::find($id)->user_id;
                break;
        }

        return ['user_id' => $return_id];
    }

    /**
     * Get me the highest user type of the guy who logged in
     */
    public function getHighestAuthenticatedUserType()
    {
        $user = Auth::user();
        if ($user->hasRole(4)) {
            return 'super';
        } else if ($user->hasRole(3)) {
            return 'administrator';
        } else if ($user->hasRole(2)) {
            return 'manager';
        } else if ($user->hasRole(1)) {
            return 'driver';
        } else {
            return ['error' => 'user has no type'];
        }

    }

    public function getEmail()
    {
        if (session()->has('impersonate')) {
            $user = User::find(session()->get('impersonate'));
        } else {
            $user = \Auth::user();
        }
        return ['email' => $user->email];
    }

    public function updateEmail(Request $request)
    {
        try {
            if (session()->has('impersonate')) {
                $user = User::find(session()->get('impersonate'));
            } else {
                $user = \Auth::user();
            }
            $user->email = $request->email;
            $user->save();
            return ['message' => 'success'];
        } catch (Exception $e) {
            return ['message' => 'failure', 'error' => $e->getMessage()];
        }
    }

    public function updatePersonalEmail(Request $request)
    {
        try {
            if (session()->has('impersonate')) {
                $user = User::find(session()->get('impersonate'));
            } else {
                $user = \Auth::user();
            }
            $user->personal_email = $request->email;
            $user->save();
            return ['message' => 'success'];
        } catch (Exception $e) {
            return ['message' => 'failure', 'error' => $e->getMessage()];
        }
    }

    public function updatePassword(Request $request)
    {
        if (session()->has('impersonate')) {
            return response()->json(['invalid_user' => 'You do not have permission to change this password']);
        } else {
            $user = \Auth::user();

            //check old password
            if (!\Hash::check($request->old_password, $user->password)) {
                return response()->json(['old_password' => ['Incorrect password']], 401);
            } else {
                \Debugbar::info('success');
            }

            $this->validate($request, [
                'new_password' =>
                    array(
                        'required',
                        'min:6',
                        'regex:/^((?=.*\d)|(?=.*[$@$!%*#?&]))[a-zA-Z0-9$@$!%*#?&]*$/'
                    )
            ]);

            $user->password = \Hash::make($request->new_password);
            $user->save();

            return ['message' => 'success'];
        }

    }


    public function checkUserPermission($user_id, $user_type, $permission)
    {
        $bool = false;
        switch ($user_type) {
            case('driver'):
                break;
            case('manager'):
                break;
            case('administrator'):
                break;
            case('super'):
                $bool = SuperProfile::where('user_id', $user_id)->pluck($permission)->first();
                break;
        }

        return $bool;
    }

    /**
     * We have to assume this user is a driver
     * @return array
     */
    public function getTimeZone()
    {
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        $country_id = $user->address->stateProvince->country_id;
        $timeZone = null;

        if ($country_id == 2) {
            $timeZone = TimeZone::find($user->address->stateProvince->time_zone_id);
        } elseif ($country_id == 1) {
            $timeZone = TimeZone::find(ZipCode::find($user->address->zip_postal)->time_zone_id);
        }

        return ['timeZone' => $timeZone];
    }

    public function getNotesForDriver($driver_id = null)
    {
        $driverNotes = [];
        if (!empty($driver_id)) {
            $driverNotes = Note::where('category_id', 3)->where('driver_id', $driver_id)->get();
        }
        return ['driverNotes' => $driverNotes];
    }

    /**
     * Return current address of user.
     *
     * Although not implemented except for drivers, we will be able
     * to leverage this function if other user types have an address
     * in the future.
     *
     * @param $user_id
     * @param $user_type
     * @return array
     */
    public function getUserAddress($user_id, $user_type)
    {
        return ['address' => $this->UserAddress($user_id, $user_type)];
    }

    public function findUserByEmail($email)
    {
        $user = User::where('email', $email)
            ->with('driverProfile')
            ->with('address')
            ->with('managerProfiles')
            ->with('administratorProfiles')
            ->first();
        $user->company = $user->company();
        return ['user' => $user];
    }

    /**
     * Return a username that does not exist in the users table in the format,
     *  f##lllllllll (1 char from first name, two digit num - if needed, 9 chars from last name).
     */
    public function generateUsername($first_name, $last_name)
    {
        $first_name_one = strtolower(substr(str_replace(array(' ', "'", "\\"), '', trim($first_name)), 0, 1));
        $last_name_nine = strtolower(substr(str_replace(array(' ', "'", "\\"), '', trim($last_name)), 0, 9));

        $new_username = $first_name_one . $last_name_nine;

        if ($this->usernameExists($new_username) == false) {
            return $new_username;
        } else {
            // Add numbers to username starting with 2.
            $user_num = 1;
            do {
                $user_num += 1;
                $new_username = $first_name_one . $user_num . $last_name_nine;
            } while ($this->usernameExists($new_username) == true);
            return $new_username;
        }
    }

    /**
     * Check if a given username exists in the database
     * @param $username
     * @return bool
     */
    public function usernameExists($username)
    {
        return User::where('username', $username)->get()->count() > 0;
    }


    public function getUsers($company_id, $from_session = false)
    {

        if (!is_numeric($company_id)) {
            return response('Invalid Company Id', 400);
        }

        $query = <<<EOQ
select distinct(u.id), u.first_name, u.last_name, u.email, u.username, u.deleted_at
, case when count(dp.id) > 0 then true else false end as `is_driver`
, case when count(dp.id) > 0 then dp.company_id else dp.company_id end as `driver_company_id`
, case when count(mp.id) > 0 then true else false end as `is_manager`
, case when count(mp.id) > 0 then mp.company_id else mp.company_id  end as `manager_company_id`
, case when count(ap.id) > 0 then true else false end as `is_administrator`
, case when count(ap.id) > 0 then ap.company_id else ap.company_id end as `administrator_company_id`
, dp.active as `active_driver`
, mp.active as `active_manager`
, ap.active as `active_administrator`, dp.user_id as `driver_id`
, mp.manager_id as `manager_id`
, ap.administrator_id as `administrator_id`
from users u
left join driver_profiles dp on dp.user_id = u.id  and dp.company_id = {$company_id}
left join manager_profiles mp on mp.user_id = u.id  and mp.company_id = {$company_id}
left join administrator_profiles ap on ap.user_id = u.id  and ap.company_id = {$company_id}
where (dp.company_id = {$company_id} or mp.company_id = {$company_id} or ap.company_id = {$company_id}) and dp.deleted_at is null and mp.deleted_at is null and ap.deleted_at is null
group by u.id, u.username, u.email, u.last_name, u.first_name, dp.active, mp.active, ap.active, u.deleted_at, dp.user_id, mp.manager_id, ap.administrator_id, dp.company_id, mp.company_id, ap.company_id
order by u.last_name asc, u.first_name asc
EOQ;
        $users = \DB::select(\DB::raw($query));
        return $users;
    }

    private function authorizeCreateOrUpdateUser(Request $request)
    {
        $currentUser = User::find(\Auth::user()->id);
        if ($currentUser->isSuper() == false && $currentUser->isAdmin() == false) {
            throw new \Exception();
        }

        if ($currentUser->isAdmin() == true) {
            if ($currentUser->administratorProfile($request->company_id)->is_limited == true) {
                throw new \Exception();
            } else if (Company::find($request->company_id)->options->account_management_responsibility != 'Client') {
                throw new \Exception();
            }
        }
    }

    public function generateLoginHistoryExcel(Request $request)
    {
        $parameters = json_decode(json_encode($request->all()));
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        $row = 1;
        $column = ord('A');

        // header
        $worksheet->getCell(chr($column++) . $row)->setValue('Date Time');
        if ($parameters->company_id == -1) {
            $worksheet->getCell(chr($column++) . $row)->setValue('Company');
        }
        $worksheet->getCell(chr($column++) . $row)->setValue('Division');
        $worksheet->getCell(chr($column++) . $row)->setValue('Driver Name');
        $worksheet->getCell(chr($column++) . $row)->setValue('Login Type');
        $worksheet->getCell(chr($column++) . $row)->setValue('Access');

        foreach ($parameters->rows as $r) {
            $column = ord('A');
            $row++;
            $worksheet->getCell(chr($column++) . $row)->setValue($r->date_time);
            if ($parameters->company_id == -1) {
                $worksheet->getCell(chr($column++) . $row)->setValue($r->company);
            }
            $worksheet->getCell(chr($column++) . $row)->setValue($r->division);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->last_name . ' ' . $r->first_name);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->login_type);
            $worksheet->getCell(chr($column++) . $row)->setValue($r->access);
        }

        $column = ord('A');
        $row += 2;
        $worksheet->getCell("A$row")->setValue('Report Name:');
        $worksheet->getCell("B$row")->setValue('History Report');
        if ($parameters->company_id > 0) {
            $row++;
            $worksheet->getCell("A$row")->setValue('Company:');
            $worksheet->getCell("B$row")->setValue(Company::find($parameters->company_id)->name);
        }
        if ($parameters->division_id > 0) {
            $row++;
            $worksheet->getCell("A$row")->setValue('Division:');
            $worksheet->getCell("B$row")->setValue(Division::find($parameters->division_id)->name);
        }
        $row++;
        $worksheet->getCell("A$row")->setValue('From Date:');
        $worksheet->getCell("B$row")->setValue($parameters->from_date);
        $row++;
        $worksheet->getCell("A$row")->setValue('To Date:');
        $worksheet->getCell("B$row")->setValue($parameters->to_date);

        /*
        // variable units
        $variable_units = $parameters->country_id == 1 ? 'Cents/mile' : 'Cents/km';
        $worksheet->getCell('C1')->setValue($variable_units);

        // mileage band
        $formatted_mileage_band = $this->formattedMileageBand($parameters->mileage_band_id);
        $worksheet->getCell('A3')->setValue($formatted_mileage_band);

        // fixed & variable rate
        $worksheet->getCell('B3')->setValue($request->fixed_rate);
        $worksheet->getCell('C3')->setValue($request->variable_rate);

        // date
        $date = date('F jS, Y', strtotime('now'));
        $worksheet->getCell('B7')->setValue($date);

        // company
        $worksheet->getCell('B8')->setValue($request->company_name);

        // driver name
        $worksheet->getCell('B9')->setValue($parameters->driver_name);

        // Zip Postal
        $zip_postal_label = $parameters->country_id == 1 ? 'Zip Code:' : 'Postal Code:';
        $worksheet->getCell('A10')->setValue($zip_postal_label);
        $worksheet->getCell('B10')->setValue($parameters->zip_postal);

        // State Province
        $state_province_label = $parameters->country_id == 1 ? 'State:' : 'Province:';
        $worksheet->getCell('A11')->setValue($state_province_label);
        $stateProvince = StateProvince::find($parameters->state_province_id);
        $worksheet->getCell('B11')->setValue($stateProvince->name);

        // vehicle profile
        $vehicleProfile = VehicleProfile::find($parameters->vehicle_profile_id);
        $worksheet->getCell('B12')->setValue($vehicleProfile->name());

        // territory
        $formatted_territory_type = $this->formattedTerritoryType($parameters->territory_type_id);
        $worksheet->getCell('B13')->setValue($formatted_territory_type);

        // territory list
        $formatted_territory_list = 'n/a';
        if ($parameters->territory_type_id > 2) {
            $formatted_territory_list = $this->formattedTerritoryList($parameters->territory_type_id, implode(',',$parameters->territory_list));
            $worksheet->getCell('B14')->setValue($formatted_territory_list);
            $worksheet->getCell('A14')->setValue('Territory List:');
            $worksheet->getCell('B15')->setValue($request->fuel_string);
            $worksheet->getCell('A15')->setValue('Fuel:');
        } else {
            $worksheet->getCell('B14')->setValue($request->fuel_string);
            $worksheet->getCell('A14')->setValue('Fuel:');
        }
        */

        $tmp_dir = storage_path() . '/app/public/tmp/';
        $file_name = 'login_history_' . date("Y-m-d") . '.xls';

        $writer = PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save($tmp_dir . $file_name);

        return ['file_name' => $file_name];

    }


    public function sendWelcomeEmail($user_id)
    {
        $user = User::find($user_id);

        $token = app('auth.password.broker')->createToken($user);

        $user->notify(new CarDataWelcomeEmail($user, $token));
    }

    //todo delete this test function
    public function sendCredentialsTest()
    {
        $user = Auth::user();
        $token = app('auth.password.broker')->createToken($user);

        $user->notify(new CarDataWelcomeEmail($user, $token));
    }

    public function switchRole(Request $request)
    {
        $role = $request->role;
        session()->put('target_active_role', $role);
        return redirect()->to(url()->previous());
    }

    public function test()
    {
        \Debugbar::info('here');
        return view('super.test')->with(['user' => auth()->user()]);
    }
}

