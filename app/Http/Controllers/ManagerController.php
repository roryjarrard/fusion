<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyDates;
use App\StateProvince;
use Auth;

use App\Address;
use App\AdministratorProfile;
use App\CompanyMileage;
use App\User;
use App\ManagerProfile;
use App\DriverProfile;
use App\Http\Traits\CompanyTraits;
use App\Http\Traits\CommonTraits;
use Illuminate\Http\Request;
use Carbon\Carbon;

// for generating approval records
use App\MonthlyApproval;
use App\MonthlyApprover;
use Illuminate\Support\Facades\DB;
use \Datetime;
set_time_limit(0);

class ManagerController extends Controller
{
    use CompanyTraits;
    use CommonTraits;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAllDataForMileageApproval(Request $request){

        //\Debugbar::error('Request for getAllDataForMileageApproval:');
        //\Debugbar::info($request);

        $company_id = $request->company_id;
        $manager_id = $request->manager_id;
        $year = $request->year;
        $month = $request->month;

        $dates = $this->getCompanyMileageDates($company_id);

        $first_approval = $this->getFirstApprovalYearMonth($manager_id);

        if( $first_approval->from_year == $year && $first_approval->from_month > $month ) {

            return ['drivers' => null, "dates"=> null, 'approval' => null,
                'before_first_approval' => 1,
                'first_approval_month' => $first_approval->from_month,
                'first_approval_year' => $first_approval->from_year];
        }

        $drivers = $this->getMyDrivers($manager_id);
        $company_dates = CompanyDates::where('company_id', $company_id)->first()->toArray();
        $mileage_entry_method = CompanyMileage::where('company_id', $company_id)->first()->mileage_entry_method;

        $approval = $this->getApprovedStatusPrivate($company_id, $manager_id, $year, $month);


        return ['drivers' => $drivers, "dates"=> $dates, 'approval' => $approval, 'before_first_approval' => null,
            'company_dates' => $company_dates, 'mileage_entry_method'=>$mileage_entry_method,
            'first_approval_month' => $first_approval->from_month,
            'first_approval_year' => $first_approval->from_year];
    }

    private function getFirstApprovalYearMonth($manager_id){
        $first_record = \DB::table('monthly_approvals')
            ->join('monthly_approvers','monthly_approvals.id','=','monthly_approvers.approval_id')
            ->where('monthly_approvers.manager_id','=',32109)
            ->orderBy('monthly_approvals.year')->select('year','month')->orderBy('monthly_approvals.month')->first();
        if( empty($first_record)) {
            return ['from_year'=>moment().year(), 'from_month'=>moment().month()+1];
        } else {
            return (object)['from_year'=>$first_record->year, 'from_month'=>$first_record->month];
        }
    }

    /**
     * Returns a manager's drivers keyed by
     *
     * @param $manager_id
     * @return array
     */
    public function getMyDrivers($manager_id)
    {
        $manager = ManagerProfile::where('manager_id',$manager_id)->first();

        if( $manager->assigned_drivers == 'some') {

            $drivers = $manager->drivers();
            foreach ($drivers as $driver) {
                $driver->driverProfile = DriverProfile::where('user_id', $driver->id)->first();
            }

        } else if ($manager->assigned_drivers == 'all') {
            $drivers = DriverProfile::where('company_id',$manager->company_id)->get();
            foreach($drivers as $driver) {
                $driver->last_name = $driver->user->last_name;
                $driver->first_name = $driver->user->first_name;
            }
        } else {
            $drivers = null;
        }
        \Debugbar::info($drivers);
        return ['drivers'=>$drivers];
    }

    public function getReimbursementForTimePeriod(Request $request) {
        \Debugbar::info($request);
        $start_month = $request->start_month;
        $end_month = $request->end_month;

        $start_date = $request->start_year . '-' . str_pad($start_month, 2, '0', STR_PAD_LEFT) . '-' . '01';
        $end_date = $request->end_year . '-' . str_pad($end_month, 2, '0', STR_PAD_LEFT) . '-' . '01';

        return response()->json(['start_date'=>$start_date, 'end_date'=>$end_date, 'ids'=>$request->driver_ids]);
    }

    public function getDriverInfo($id) {
        $user = User::find($id);
        $driver_info = [
            'first_name'=> $user->first_name,
            'last_name'=>$user->last_name,
            'email' => $user->email,
            'status' => $user->deleted_at == null ? 'active' : 'inactive',
            'company' => $user->driverProfile->company->name,
            'division' => $user->driverProfile->division->name,
            'job_title' => $user->driverProfile->job_title,
            'cost_center' => ucwords(strtolower($user->driverProfile->cost_center))
        ];
        \Debugbar::info($driver_info);
        return response()->json(['driver_info'=>$driver_info]);
    }

    public function mileageApproval () {
        return view('manager.home')->with('message', 'This is the Manager Mileage Approval page');
    }

    public function linkToDriver () {
        return view('manager.home')->with('message', 'This is the Manager Link to a Driver page');
    }

    public function viewedTutorials () {
        return view('manager.home')->with('message', 'This is the Manager Viewed Tutorials page');
    }

    public function carPolicy () {
        return view('manager.home')->with('message', 'This is the Car Policy page');
    }

    public function reimbursementTotals () {
        return view('manager.home')->with('message', 'This is the Reimbursement Totals page');
    }

    public function fuelPrices () {
        return view('manager.home')->with('message', 'This is the Fuel Price page');
    }

    public function vehicleReport () {
        return view('manager.home')->with('message', 'This is the Vehicle Report page');
    }

    public function trafficLightReport () {
        return view('manager.home')->with('message', 'This is the Traffic Light Report page');
    }

    //

    public function getApprovalRecords() {
        $yesterday = new DateTime();
        $yesterday->modify("-1 day");
        $yesterday_day = $yesterday->format('d');

        $companies = \DB::table('companies as c')
            ->join('company_dates as cm', 'c.id', '=', 'cm.company_id')
            ->join('company_payments as cf', 'c.id', '=', 'cf.company_id')
            ->select('c.id', 'c.name', 'cf.direct_pay_file_fee', 'cf.direct_pay_driver_fee')
            ->where('cm.mileage_approval_end_day', '>', 0)
            ->orderBy('c.name')
            ->get();

        //             ->where('cm.mileage_lock_driver', $yesterday_day)


        $year = date("Y");
        $month = date("n");

        foreach ($companies as $company) {

            $approval_data = array('company_id' => $company->id, 'year' => $year, 'month' => $month,
                'file_fee' => $company->direct_pay_file_fee, 'driver_fee' => $company->direct_pay_driver_fee );

            $approval_id = \DB::table('monthly_approvals')->insertGetId($approval_data); // INSERT

            \Debugbar::info('Approval ID ' . $approval_id . ' for ' . $company->name);

            $managers = ManagerProfile::where('company_id', $company->id)
                ->where('assigned_drivers', '!=', 'none')->select('manager_id')->get();

            foreach ($managers as $manager) {
                $approver_data = array('approval_id' => $approval_id, 'manager_id' => $manager->manager_id);
                \DB::table('monthly_approvers')->insert($approver_data);
            }


            $administrators = AdministratorProfile::where('company_id',$company->id)->where('company_approver',1)
                ->select('administrator_id')->get();

            foreach ($administrators as $administrator) {
                $approver_data = array('approval_id' => $approval_id, 'administrator_id' => $administrator->administrator_id);
                \DB::table('monthly_approvers')->insert($approver_data);
            }

        }
    }

    public function getApprovedStatus(Request $request) {
         return ['approval' => $this->getApprovedStatusPrivate($request->company_id, $request->manager_id,
             $request->year, $request->month) ];
    }

    public function postManagerApproval(Request $request){
        $manager_id = $request['manager_id'];
        $approval_id = $request['approval_id'];
        $today = date("Y-m-d H:i:s");
        DB::table('monthly_approvers')
            ->where('manager_id', $request['manager_id'])
            ->where('approval_id', $request['approval_id'])
            ->update(['approval_date' => $today]);
        return ['approved' => 1];
    }

    private function getApprovedStatusPrivate($company_id, $manager_id, $year, $month){
        $status = false;
        $user_type = 'manager';
        $manager_name = null;
        $admin_name = null;
        $approval_date = null;
        $processed_date = null;
        \Debugbar::info('getApprovedStatusPrivate y=' . $year . ' m=' . $month);
        $approval = MonthlyApproval::where('company_id',$company_id)->where('year',$year)
            ->where('month',$month)->first();
        \Debugbar::info($approval);

        if( $approval && $approval->id > 0 ) {
            $processed_date = $approval->approval_date;
            $approval_id = $approval->id;
            $approved = MonthlyApprover::where('approval_id', $approval->id)->where('manager_id',$manager_id)->first();
            if( $approved) {
                if( $approved->approval_date != null ) {
                    $status = true;
                    $approval_date = $approved->approval_date;
                    if( $approved->override_user_id > 0 ) {
                        $user_type = 'admin';
                        $ap = AdministratorProfile::where('administrator_id', $approved->override_user_id)->first();
                        $u = $ap->user;
                        $admin_name = $u->first_name . ' ' . $u->last_name;
                    }
                }
            }
        } else {
            $approval_id = null;
        }
        $mp = ManagerProfile::where('manager_id',$manager_id)->first();
        $u = $mp->user;
        $manager_name = $u->first_name . ' ' . $u->last_name;

        \Debugbar::info('getApprovedStatusPrivate returns ');
        \Debugbar::info($status,$user_type,$manager_name);
        return ['status'=>$status, 'user_type' => $user_type, 'manager_name' => $manager_name,
            'admin_name' => $admin_name, 'approval_date' => $approval_date,
            'processed_date' => $processed_date, 'approval_id' => $approval_id];
    }

    /**
     * Generating data for Manager Approval Status page
     * @param Request $request [ company_id, year, month, check_year ]
     * @return array|null
     */
    public function getManagerApprovalStatus(Request $request){
        $company_id = $request['company_id'];
        $year = $request['year'];
        $month = $request['month'];
        $admin_id = $request['admin_id'];

        $admin_approved_id = 0;
        $admin_approval_date = null;

        $first_year = \DB::table('monthly_approvals')->where('company_id',$company_id)->min('year');

        if( $year < $first_year ) {
            $year = $first_year;
        }

        if( $admin_id > 0 ) {
            $company_mileage = CompanyMileage::where('company_id',$company_id)->first();
            $company_dates = CompanyDates::where('company_id', $company_id)->first();
        } else {
            $company_mileage = null;
        }

        // get approval record
        $approval = MonthlyApproval::where([
            ['company_id',$company_id],
            ['year',$year],
            ['month',$month]
            ])->first();

        if( $approval != null ) {
            $approval_id = $approval->id;
            $approval_date = $approval->approval_date;

            // get approvers
            $admins = \DB::table('monthly_approvers as ma')
                ->join('administrator_profiles as ap', 'ma.administrator_id', '=', 'ap.administrator_id')
                ->join('users as u', 'ap.user_id', '=', 'u.id')
                ->select('ma.administrator_id', 'u.last_name', 'u.id as user_id', 'u.first_name', 'ma.approval_date',
                    'ma.override_user_id')
                ->where('ma.approval_id', $approval_id)
                ->orderBy('u.last_name')
                ->orderBy('u.first_name')
                ->get();

            \Debugbar::info('Admins');
            \Debugbar::info($admins);

            // get managers involved in this app
            // if we logged as admin, limit managers to divisions assigned to this admin, unless manager is assigned to all drivers,
            // unless admin is assigned to all divisions ( not records in administrator_division table

            // TODO: if admin has all divisions or logged as super
            // TODO: else selected managers with drivers from admin divisions

            if( $admin_id > 0 ) {

                // get admin divisions
                $ap = AdministratorProfile::where('administrator_id',$admin_id)->first();

                $manager_arr=[];
                if( $ap->assigned_divisions == 'some') {
                    $divisions = $ap->divisions();
                    $div_arr = [];
                    foreach($divisions as $row) {
                        $div_arr[] = $row->id;
                    }
                    $selected_managers = \DB::table('administrator_division as ad')
                        ->join('driver_profiles as dp', 'ad.division_id','=','dp.division_id')
                        ->join('monthly_reimbursements as mr', function ($join) use($year, $month) {
                            $join->on('mr.user_id','=','dp.user_id')
                                ->where('mr.year','=',$year)
                                ->where('mr.month','=',$month);
                        })
                        ->join('manager_driver as md', 'dp.user_id','=','md.driver_id')
                        ->where('ad.administrator_id',$admin_id)
                        ->select('md.manager_id')->distinct()->get();

                    foreach($selected_managers as $row) {
                        $manager_arr[] = $row->manager_id;
                    }

                } else if( $ap->assigned_divisions == 'all') {

                    $selected_managers = \DB::table('driver_profiles as dp')
                        ->join('monthly_reimbursements as mr', function ($join) use ($year, $month) {
                            $join->on('mr.user_id','=','dp.user_id')
                                ->where('mr.year','=',$year)
                                ->where('mr.month','=',$month);
                        })
                        ->join('manager_driver as md', 'dp.user_id','=','md.driver_id')
                        ->select('md.manager_id')->distinct()->get();

                    foreach($selected_managers as $row) {
                        $manager_arr[] = $row->manager_id;
                    }
                }

            } else {    // super user
                $selected_managers = \DB::table('driver_profiles as dp')
                    ->join('monthly_reimbursements as mr', function ($join) use ($year, $month) {
                        $join->on('mr.user_id','=','dp.user_id')
                            ->where('mr.year','=',$year)
                            ->where('mr.month','=',$month);
                    })
                    ->join('manager_driver as md', 'dp.user_id','=','md.driver_id')
                    ->select('md.manager_id')->distinct()->get();

                foreach($selected_managers as $row) {
                    $manager_arr[] = $row->manager_id;
                }

            }

            $managers = \DB::table('monthly_approvers as ma')
                ->join('manager_profiles as m', 'ma.manager_id', '=', 'm.manager_id')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->leftJoin('administrator_profiles as ap', 'ma.override_user_id', '=', 'ap.administrator_id')
                ->leftJoin('users as ov', 'ap.user_id', '=', 'ov.id')
                ->select('m.manager_id', 'u.id as user_id', 'u.last_name', 'u.first_name', 'ma.approval_date',
                    'ma.override_user_id', 'm.approval_description', 'ov.last_name as ov_last_name',
                    'ov.first_name as ov_first_name')
                ->where('ma.approval_id', $approval_id)
                ->whereIn('m.manager_id',$manager_arr)
                ->orderBy('u.last_name')
                ->orderBy('u.first_name')
                ->get();

            // $stops = \DB::select(\DB::raw(""));

            // build table records
            $arr = [];
            $found_missing_approvals = false; // default

            foreach ($admins as $admin) {

                if ($admin_id > 0 && $admin_id = $admin->administrator_id && $admin->approval_date != null) {
                    $admin_approved_id = $admin->override_user_id > 0 ? $admin->override_user_id : $admin->administrator_id;
                    $admin_approval_date = $admin->approval_date;
                }

                if( !$found_missing_approvals && empty($admin->approval_date)) {
                    $found_missing_approvals = true;
                }

                $arr[] = ['last_name' => $admin->last_name, 'first_name' => $admin->first_name,
                    'description' => null, 'type' => 'Admin Approver', 'user_id' => $admin->user_id,
                    'admin_or_manager_id' => $admin->administrator_id, 'over_last_name' => null, 'over_first_name' => null,
                    'approved' => $admin->approval_date];
            }
            $all_managers_approved = true;
            foreach ($managers as $manager) {
                if ($manager->approval_date == null) {
                    $all_managers_approved = false;
                }
                $arr[] = ['last_name' => $manager->last_name, 'first_name' => $manager->first_name,
                    'description' => $manager->approval_description, 'type' => 'Manager', 'user_id' => $manager->user_id,
                    'admin_or_manager_id' => $manager->user_id, 'over_last_name' => $manager->ov_last_name,
                    'over_first_name' => $manager->ov_first_name,
                    'approved' => $manager->approval_date];
            }
            \Debugbar::info($arr);
            \Debugbar::info($all_managers_approved);

            return ['approval_id' => $approval_id, 'admin_approved_id' => $admin_approved_id,
                'admin_approval_date' => $admin_approval_date, 'data' => $arr, 'company_mileage' => $company_mileage,
                'company_dates'=>$company_dates, 'all_managers_approved' => $all_managers_approved, 'first_year' => $first_year,
                'found_missing_approvals' => $found_missing_approvals];
        } else {
            return ['company_mileage' => $company_mileage, 'company_dates'=>$company_dates]; // still need it for messages
        }
    }

    public function getManagerAddress($manager_id)
    {
        $address = ManagerProfile::select()
            ->where('manager_id', $manager_id)
            ->whereNull('deleted_at')
            ->whereNull('deleted_at')
            ->first()
            ->address();

        if (!empty($address)) {
            $address->stateProvince = StateProvince::find($address->state_province_id);
        }

        return response()->json([
            'address' => $address
        ]);
    }

    /**
     * @param Request $request
     *      request will be an object of the following form:
     *          { address:  address obj, manager_id: int }
     *
     *      This is necessary, because it is possible for a user to have multiple active manager profiles
     *      one in canada and one in america for example. Thus the manager_id allows us to determine which
     *      address we are in fact editing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addManagerAddress(Request $request)
    {
        //TODO: get lat long
        $this->validate($request, [
            'address.street' => 'required',
            'address.city' => 'required',
            'address.state_province_id' => 'required|integer|min:1',
            'address.zip_postal' => 'required',
            'address.country_id' => 'required|min:1'
        ]);

        $user_id = Auth::user()->id;
        $data = $request->address;
        $data['user_id'] = $user_id;

        $address_id = Address::create($data)->id;

        $managerProfile = ManagerProfile::where('manager_id', $request->manager_id)->first();

        // duplicate manager profile, set new address id and user id who made change, delete previous manager profile
        $newManagerProfile = $managerProfile->replicate();
        $newManagerProfile->address_id = $address_id;
        $newManagerProfile->id = null;
        $managerProfile->delete();
        $newManagerProfile->save();
    }

    /**
     * @param Request $request
     *      request will be an object of the following form:
     *          { address:  address obj, manager_id: int }
     *
     *      This is necessary, because it is possible for a user to have multiple active manager profiles
     *      one in canada and one in america for example. Thus the manager_id allows us to determine which
     *      address we are in fact editing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateManagerAddress(Request $request)
    {
        //dd($request->all());
        //TODO: get latlong
        $this->validate($request, [
            'address.street' => 'required',
            'address.city' => 'required',
            'address.state_province_id' => 'required|integer|min:1',
            'address.zip_postal' => 'required',
            'address.country_id' => 'required|min:1'
        ]);

        //$managerProfile = ManagerProfile::where('manager_id', $request->manager_id)->first();
        $managerProfile = ManagerProfile::where('user_id', $request->user_id)->first();

        //dd($managerProfile);

        $currentAddress = $managerProfile->address();
dd($currentAddress);
        $currentAddress->delete();
        $user_id = Auth::user()->id;
        $data = $request->address;
        $data['user_id'] = $user_id;
dd($data);
        $updatedAddress = Address::create($data);

        // get new address id and current user_id
        $new_address_id = $updatedAddress->id;

        // duplicate manager profile, set new address id and user id who made change, delete previous manager profile
        $newManagerProfile = $managerProfile->replicate();
        $newManagerProfile->address_id = $new_address_id;
        $newManagerProfile->id = null;
        $managerProfile->delete();
        $newManagerProfile->save();

        return response()->json(['message' => 'success']);
    }
}
