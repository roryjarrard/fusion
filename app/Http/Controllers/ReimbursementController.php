<?php

namespace App\Http\Controllers;

use App\Address;
use App\AdministratorProfile;
use App\Calculators\StreetLightCalculator;
use App\Company;
use App\CompanyOptions;
use App\County;
use App\CountyTax;
use App\DriverProfile;
use App\FairPurchasePrice;
use App\FavrParameter;
use App\FuelCity;
use App\Http\Traits\CommonTraits;
use App\Http\Traits\DriverTraits;
use App\Http\Traits\UtilityTraits;
use App\Http\Traits\ValidationTraits;
use App\InsuranceRate;
use App\InterestRate;
use App\MaintenanceRepair2009;
use App\MaintenanceRepair2010;
use App\ManagerProfile;
use App\MileageBand;
use App\MonthlyApproval;
use App\MonthlyApprover;
use App\MonthlyMileage;
use App\MonthlyReimbursement;
use App\PersonalVehicle;
use App\PostalCode;
use App\ReimbursementCalculatorSession;
use App\ReimbursementDetail;
use App\ResaleCondition;
use App\StateProvince;
use App\TaxesFeesCA;
use App\TaxesFeesUS;
use App\TrackingChange;
use App\User;
use App\Vehicle;
use App\VehicleCategory;
use App\VehicleCategoryInsuranceAdjustment;
use App\VehicleMileageAdjustment2009;
use App\VehicleMileageAdjustment2011;
use App\VehicleProfile;
use App\VehicleResaleValueCA;
use App\VehicleResaleValueUS;
use App\ZipCode;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as Validator;
use PhpOffice\PhpSpreadsheet;
use Storage;


set_time_limit(0);

class ReimbursementController extends Controller
{
    use CommonTraits;
    use DriverTraits;
    use UtilityTraits;
    use ValidationTraits;

    protected $streetLightCalculator;

    public function __construct()
    {
        $this->streetLightCalculator = new StreetLightCalculator();
        $this->middleware('auth');
    }


    /**
     * Generating reimbursement. Populating records in monthly_reimbursements and reimbursement_details tables
     *
     * @param int $year , optional default: year of the last month
     * @param int $month , optional default: last month
     * @param int $company_id , optional default: calculate reimbursement for all active clients
     * @param int $driver_profile_id , optional default: calculate reimbursement for all active drivers
     * @return array - only for testing, we have to remove it later
     */
    public function generateReimbursement($year = 0, $month = 0, $company_id = 0, $user_id = 0, $rates_only = 0, $logger = null)
    {

        \Debugbar::info("Data to generate reimbursement: y= $year  m= $month c= $company_id  u= $user_id");

        $count = ['inserted' => 0, 'updated' => 0];

        /////////////////////// DETERMINE DEFAULT VALUES
        if ($year <= 0) {
            $year = date("n") == 1 ? date("Y") - 1 : date("Y");
        }
        if ($month <= 0) {
            $month = date("n") == 1 ? 12 : date("n") - 1;
        }
        $company_where = $company_id <= 0 ? '' : " AND c.id = $company_id ";
        $driver_where = $user_id <= 0 ? '' : " AND u.id = $user_id"; // $driver_profile_id ";

        $q_companies = "SELECT c.id, c.name, cf.direct_pay_day, cp.country_id, co.service_plan, 
          ci.remove_reimbursement_without_insurance, use_advance_payment, mileage_lock_all_day
  FROM companies c
  JOIN company_payments cf ON c.id = cf.company_id
  JOIN company_profiles cp ON c.id = cp.company_id
  JOIN company_options co ON c.id = co.company_id
  JOIN company_insurances ci ON c.id = ci.company_id
  JOIN company_mileages cm ON c.id = cm.company_id
  WHERE c.deleted_at IS NULL {$company_where}
  ORDER BY c.name";
        $companies = $rec = \DB::select(\DB::raw($q_companies));

        $results = [];

        foreach ($companies as $company) {
            if ($logger) {
                $logger->addInfo('reimbursement', ['processing company' => "$company->name $year $month"]);
            } else {
                \Log::info("Processing company $company->name Year=$year Month=$month");
            }
            // do not generate reimbursement if bank account is needed but is not set.
            $join_with_bank = $company->direct_pay_day > 0 ? " JOIN user_bank_accounts b ON b.user_id = u.id " : '';
            $d_drivers = "SELECT d.id AS driver_id, u.id AS user_id, u.last_name, u.first_name, d.street_light_id, 
      YEAR(d.start_date) AS ds_year, MONTH(d.start_date) AS ds_month, vp.business_use_percent, d.start_date 
  FROM users u 
  JOIN driver_profiles d ON u.id = d.user_id AND d.deleted_at IS NULL
  JOIN vehicle_profiles vp ON d.vehicle_profile_id = vp.id AND vp.deleted_at IS NULL
  {$join_with_bank}
  WHERE u.deleted_at IS NULL AND d.company_id = $company->id 
  {$driver_where} 
  ORDER BY u.last_name, u.first_name ";

            $drivers = $rec = \DB::select(\DB::raw($d_drivers));

            foreach ($drivers as $driver) {

                \Debugbar::info("Driver: $driver->last_name $driver->first_name");

                // only payment in advance can get reimbursement in the same month driver starts
                if ($company->use_advance_payment != 1 && ($driver->ds_year > $year || $driver->ds_year == $year && $driver->ds_month > $month)) {
                    continue;
                }

                // filter drivers where they should not have reimbursement and record the reason
                $accept_red_insurance = ($company->service_plan == PLAN_FAVR && $company->remove_reimbursement_without_insurance || $company->id == 56) ? true : false;
                if ($this->noReimbursementForThisDriver($driver, $accept_red_insurance)) {
                    continue;
                }

                // prepare data for reimbursement rd: geo_data + vehicle_profile
                $rd = $this->getDataForReimbursementFromDriver($year, $month, $company->service_plan, $driver->driver_id);
                if ($rd->status == false) {
                    \Log::info("Skipping driver as data is missing: " . implode(', ', $rd->missing_data));
                    continue;
                }

                if ($logger) {
                    $logger->addInfo('reimbursement', ['driver' => "$driver->last_name $driver->first_name $driver->driver_id"]);
                } else {
                    \Log::info("$driver->last_name $driver->first_name user id: $driver->user_id profile: $driver->driver_id details: " . $rd->geo_data->reimbursement_detail_id);
                }

                ////////////////////////////////////////////////////////////////
                $fixed_reimb = $this->calculateFixedReimbursementProxy($rd);
                \Log::info("Fixed Completed\n");
                ////////////////////////////////////////////////////////////////
                $variable_reimb = $this->calculateVariableReimbursementProxy($rd);
                \Log::info("Variable Completed\n");
                ////////////////////////////////////////////////////////////////
                // \Debugbar::info($fixed_reimb);

                // populate reimbursement detail record
                $detail_id = $rd->geo_data->reimbursement_detail_id;
                if (empty($detail_id)) {
                    $detail_id = $this->insertOrUpdateReimbursementDetails($driver->driver_id,
                        $rd->vehicle_profile->business_use_percent, $fixed_reimb, $variable_reimb);
                }
                if ($rates_only) {
                    return ['fixed_reimb' => $fixed_reimb, 'variable_reimb' => $variable_reimb];
                }


                // when calculating adjustment we need 'real' depreciation value, with adjustments and with BUP
                if ($rd->vehicle_profile->calculate_fixed) {
                    $depreciation = $fixed_reimb['fixedReimbursementDetails']['depreciation']['amount'] *
                        (1 + $fixed_reimb['fixedReimbursementDetails']['depreciation']['adjustment'] / 100.0) *
                        $rd->vehicle_profile->business_use_percent / 100;
                } else {
                    $depreciation = 0;
                }

                // \Debugbar::error("Depreciation: ");
                // \Debugbar::info( $fixed_reimb['fixedReimbursementDetails']['depreciation']['amount'] . " * ( 1 + " .
                //    $fixed_reimb['fixedReimbursementDetails']['depreciation']['adjustment'] . " / 100 ) * " .
                //   $rd->vehicle_profile->business_use_percent . " / 100 => $depreciation");

                $mileage = MonthlyMileage::where('user_id', $driver->user_id)->where('year', $year)
                    ->where('month', $month)->first()->business_mileage;

                // prepare data needed for adjustment and to create monthly reimbursement
                $data = (object)['driver' => $driver, 'year' => $year, 'month' => $month,
                    'company_id' => $company->id,
                    'lock_all_day' => $company->mileage_lock_all_day,
                    'street_light_id' => $driver->street_light_id,
                    'remove_reimbursement_without_insurance' => $company->remove_reimbursement_without_insurance,
                    'fixed_amount' => $fixed_reimb['fixedReimbursement'],
                    'depreciation' => $depreciation,
                    'adjustment_amount' => 0,
                    'fixed_paid' => $fixed_reimb['fixedReimbursement'],
                    'cents_per_mile' => $variable_reimb['variableReimbursement'],
                    'business_use_percent' => $rd->vehicle_profile->business_use_percent,
                    'taxable_amount' => 0,
                    'within_grace_period' => 0,
                    'detail_id' => $detail_id,
                    'mileage' => $mileage,
                    'profile_year' => $rd->vehicle_profile->vehicle->year
                ];

                // calculate adjustments for company specific rules.
                if (($company->service_plan == PLAN_FAVR || $company->id == WASTE_MANAGEMENT_CANADA)
                    && $driver->street_light_id > GREEN_REIMBURSEMENT) {
                    $data = $this->getFAVRAdjustment($data);
                }   // adjustments


                // \Debugbar::info($data);

                $counts = $this->insertOrUpdateMonthlyReimbursement($data);

                $count['inserted'] += $counts->inserted;
                $count['updated'] += $counts->updated;

                // populating labels with details describing on which data values were populated.
                $this->getReimbursementLabels($fixed_reimb, $variable_reimb, $rd);

                // this is for testing only to display reimbursement values as we do not have yet reimbursement report
                /*
                $new_row = ['last_name' => $driver->last_name, 'first_name' => $driver->first_name,
                    'fixed' => $data->fixed_amount, 'adjustment' => $data->adjustment_amount,
                    'cents_per_mile' => $data->cents_per_mile, 'within_grace_period' => $data->within_grace_period,
                    'fixed_paid' => $data->fixed_paid, 'taxable_amount' => $data->taxable_amount];
                */
                //\Debugbar::warning('new_row');
                //\Debugbar::warning($new_row);

                //$results[] = $new_row;
                $results[] = null;

            }   // drivers
            \Log::info("All drivers processed");

        }   // companies

        return ['results' => $results, 'count' => $count];
        // return response()->json(['reimbursement' => $reimbursement, 'WM_note' => $WM_note, 'show_red_ball' => $show_red_ball]);

    }

    /**
     * @param $data
     * @return mixed
     */
    public function getFAVRAdjustment($data)
    {
        // find you if we are in grace period. One payment after changing vehicles or for new or reimstatede drivers
        if ($data->company_id == WASTE_MANAGEMENT_USA || $data->company_id == WASTE_MANAGEMENT_CANADA) {
            $graceData = $this->getWMGraceData($data->driver, $data->lock_all_day);
        } else {
            $graceData = $this->getFAVRGraceData($data->driver, $data->lock_all_day);
        }

        // \Debugbar::info($graceData);

        if ($graceData->in_grace) {
            $data->within_grace_period = 1;
            return $data;
        }

        $data->within_grace_period = 0;
        if ($data->remove_reimbursement_without_insurance && (
                $data->street_light_id & RED_AWAITING_INSURANCE ||
                $data->street_light_id & RED_INSURANCE_EXPIRED ||
                $data->street_light_id & RED_INSUFFICIENT_INSURANCE ||
                $data->street_light_id & GREEN_NO_INSURANCE_COMPLIANCE
            )) {
            $data->adjustment_amount = $data->fixed_amount;
            $data->depreciation = 0;
            if ($graceData->keep_variable == false) {
                $data->cents_per_mile = 0;
            }
        } elseif ($data->company_id == WASTE_MANAGEMENT_USA &&
            $data->street_light_id & GREEN_NO_VEHICLE_AGE_COMPLIANCE) {
            $data->adjustment_amount = $data->depreciation;
            $data->depreciation = 0;
        }
        $data->fixed_paid = $data->fixed_amount - $data->adjustment_amount;

        $variable_reimbursement = round($data->cents_per_mile * $data->mileage / 100.0, 2);
        // calculate taxable amount
        if ($data->street_light_id & GREEN_NO_DEPRECIATION_COMPLIANCE) {
            $data->taxable_amount = $data->fixed_paid + $variable_reimbursement;
        } else {
            $standard_rate = FavrParameter::find($data->profile_year)->standard_rate;
            $non_taxable = $standard_rate * $data->mileage;
            $data->taxable_amount = max(0, round($data->fixed_paid + $variable_reimbursement - $non_taxable, 2));
        }

        return $data;

    }


    /**
     *
     * WM specific calculation of grace. WM pays fixed reimbursement in advance.
     *
     * @param Driver $driver
     * @param int $year
     * @param int $month
     */
    private function getWMGraceData($driver, $mileage_lock_all_day)
    {

        $driver_date = substr($driver->start_date, 0, 10);
        $personal_vehicle = PersonalVehicle::where('user_id', $driver->user_id)->first();
        $vehicle_date = substr($personal_vehicle->created_at, 0, 10);
        $date1 = strcmp($vehicle_date, $driver_date) < 0 ? $driver_date : $vehicle_date; // most recent from driver
        list($yy1, $mm1, $dd1) = explode('-', $date1);

        // user current month and year as $year $month are for previous month
        if ($yy1 < date("Y")) {
            $mm2 = date("n") + 12;
        } else {
            $mm2 = date("n");
        }
        $months_diff = $mm2 - $mm1;

        //\Debugbar::info("GRACE: dr=$driver_date v=$vehicle_date => $date1 ");
        //\Debugbar::info(" $dd1 <= $mileage_lock_all_day ? months diff $months_diff = $mm2 - $mm1; ");

        if ($months_diff == 0) {                    // the same month so we have grace
            $grace = true;
            $keep_variable = true;
        } elseif ($months_diff == 1) {              // keep variable
            if ($dd1 <= $mileage_lock_all_day) {
                $grace = false;
                $keep_variable = true;
            } else {
                $grace = true;
                $keep_variable = true;
            }
        } else {
            $grace = false;
            $keep_variable = false;
        }
        return (object)['in_grace' => $grace, 'keep_variable' => $keep_variable];


        // \Debugbar::info("LOCK: $last_all_lock_date DR: $driver_date VI: $vehicle_date -> $grace");
        return (object)['in_grace' => $grace, 'keep_variable' => $keep_variable];
    }

    private function getFAVRGraceData($driver, $mileage_lock_all_day)
    {
        $driver_date = substr($driver->start_date, 0, 10);
        $personal_vehicle = PersonalVehicle::where('user_id', $driver->user_id)->first();
        $vehicle_date = substr($personal_vehicle->created_at, 0, 10);

        // check is new driver, ( starting previous month )
        list($yy1, $mm1, $dd1) = explode('-', $driver_date);
        $mm2 = $yy1 < date("Y") ? date("n") + 12 : date("n");
        $month_diff = $mm2 - $mm1;
        if ($month_diff == 1 || $month_diff == 0) {
            // \Debugbar::info("New Driver Grace");
            return (object)['in_grace' => true, 'keep_variable' => true];
        }

        // check if vehicle was introduced last month
        list($yy1, $mm1, $dd1) = explode('-', $vehicle_date);
        $mm2 = $yy1 < date("Y") ? date("n") + 12 : date("n");
        $month_diff = $mm2 - $mm1;
        if ($month_diff == 1) {
            // \Debugbar::info("New Vehicle Grace");
            return (object)['in_grace' => true, 'keep_variable' => true];
        }

        // \Debugbar::info('No grace');
        return (object)['in_grace' => false, 'keep_variable' => false];
    }

    public function addReimbursementDetails($driver_id)
    {
        $year = date("n") == 1 ? date("Y") - 1 : date("Y");
        $month = date("n") == 1 ? 12 : date("n") - 1;
        $service_plan = DriverProfile::find($driver_id)->company()->first()->options()->first()->service_plan;
        $rd = $this->getDataForReimbursementFromDriver($year, $month, $service_plan, $driver_id);
        // \Debugbar::info($rd);
        $fixed_reimb = $this->calculateFixedReimbursementProxy($rd);
        // \Debugbar::info($fixed_reimb);
        $variable_reimb = $this->calculateVariableReimbursementProxy($rd);
        // \Debugbar::info($variable_reimb);
        return $fixed_reimb;
    }

    private function noReimbursementForThisDriver($driver, $accept_red_insurance)
    {

//        if ($driver->street_light_id & RED_INACTIVE) {
//            // TODO: store addNoReimbursementReason('inactive');
//            //\Debugbar::warning("Skipping inactive $driver->last_name $driver->first_name");
//            return true;
//        }

        if ($driver->street_light_id & YELLOW_NEW_DRIVER) {
            // TODO: store addNoReimbursementReason('new driver')
            //\Debugbar::warning("Skipping New Driver $driver->last_name $driver->first_name");
            return true;
        }


        if (!($this->colorOfStreetLight($driver->street_light_id) == 'Green' ||
            $accept_red_insurance &&
            ($driver->street_light_id & RED_AWAITING_INSURANCE ||
                $driver->street_light_id & RED_INSUFFICIENT_INSURANCE ||
                $driver->street_light_id & RED_INSURANCE_EXPIRED
            ))) {
            // TODO: store addNoReimbursementReason('insurance')
            //\Debugbar::warning("Skipping no insurance $driver->last_name $driver->first_name");
            return true;
        }


        if ($driver->street_light_id & RED_DISABILITY || $driver->street_light_id & RED_NO_REIMBURSEMENT ||
            $driver->street_light_id & RED_ON_LEAVE || $driver->street_light_id & RED_TERMINATED) {
            // TODO: store addNoReimbursementReason('terminated')
            //\Debugbar::warning("Skipping terminated $driver->last_name $driver->first_name");
            return true;
        }

        return false;

    }


    public function getDataForReimbursementFromDriver($year, $month, $service_plan, $driver_id)
    {
        /*
         let geo_data = {'year': this.year, 'month': this.month, 'plan': this.plan,
                    'mileage_band_id': this.mileage_band_id, 'address': this.address,
                    'territory_type_id': this.territory_type_id, 'territory_list': this.territory_list };
                let reimb_data = { 'geo_data': geo_data, 'vehicle_profile': this.vehicle_profile};
         */

        // the user/driver_id
        $driver = DriverProfile::withTrashed()->find($driver_id);
        $details = $driver->reimbursement_detail_id > 0 ? ReimbursementDetail::find($driver->reimbursement_detail_id) : null;

        // we found problem when creating driver profile with replication. We are caring previous driver profile reimbursement details
        // so changes of address, mileage band, vehicle profile will be ignored as we already pointing to older reimbursement details.
        // in this case if driver profile created at is newer than reimbursement details created at, IGNORE reimbursement details
        if ($details && $driver->created_at > $details->created_at) {
            \Debugbar::info("IGNORE REIMBURSEMENT DETAILS");
            $details = null;
        }


        $status = true;
        $missing_data = [];

        $geo_data = (object)[
            'year' => $year,
            'month' => $month,
            'plan' => $service_plan,
            'mileage_band_id' => $driver->mileage_band_id,
            'address' => $driver->address(),
            'territory_type_id' => $driver->territory_type_id,
            'territory_list' => $driver->territory_list,
            'street_light_id' => $driver->street_light_id,
            'reimbursement_detail_id' => $driver->reimbursement_detail_id,
            'reimbursement_details' => $details,
            'driver_id' => $driver_id];     // JUST FOR A TEST

        $vehicle_profile = $driver->vehicleProfile;

        if ($vehicle_profile == null) {
            $status = false;
            $missing_data[] = 'Vehicle Profile';
        }
        if ($driver->address() == null) {
            $status = false;
            $missing_data[] = 'Driver Address';
        }

        $rd = (object)[
            'geo_data' => $geo_data,
            'vehicle_profile' => $vehicle_profile,
            'status' => $status,
            'missing_data' => $missing_data
        ];

        //\Debugbar::info($rd);

        return $rd;
    }

    public function insertOrUpdateReimbursementDetails($driver_id,
                                                       $business_use_percent, $fixed_reimb, $variable_reimb)
    {
        $fixed = $fixed_reimb['fixedReimbursementDetails'];
        $variable = $variable_reimb['variableReimbursementDetails'];

        //$monthly = MonthlyReimbursement::where('user_id', $user_id)->where('year', $year)->where('month', $month)->first();
        $driver_profile = DriverProfile::find($driver_id);

        if ($driver_profile == null || $driver_profile->reimbursement_detail_id == null) {
            $detail = new ReimbursementDetail;
        } else {
            $detail = ReimbursementDetail::find($driver_profile->reimbursement_detail_id);
        }

        // for FAVE we do not have some details: finance_cost and monthly_payment

        if (count($fixed) > 1) {  // calculated fixed

            if (!array_key_exists('monthly_payment', $fixed)) {
                $fixed['monthly_payment']['amount'] = null;
                $fixed['monthly_payment']['adjustment'] = null;
            }
            if (!array_key_exists('finance_cost', $fixed)) {
                $fixed['finance_cost']['amount'] = null;
                $fixed['finance_cost']['adjustment'] = null;
            }

            $detail->capital_cost = $fixed['capitalCost']['amount'];
            $detail->resale_value = $fixed['resaleValue']['amount'];
            $detail->tax = $fixed['tax']['amount'];
            $detail->monthly_payment = isset($fixed['monthlyPayment']['amount']) ? $fixed['monthlyPayment']['amount'] : null;
            $detail->depreciation = $fixed['depreciation']['amount'];
            $detail->finance_cost = isset($fixed['financeCost']['amount']) ? $fixed['financeCost']['amount'] : null;
            $detail->insurance = $fixed['insurance']['amount'];
            $detail->fee_renewal = $fixed['feeRenewal']['amount'];
            $detail->capital_cost_adj = $fixed['capitalCost']['adjustment'];
            $detail->resale_value_adj = $fixed['resaleValue']['adjustment'];
            $detail->tax_adj = $fixed['tax']['adjustment'];
            $detail->monthly_payment_adj = isset($fixed['monthlyPayment']['adjustment']) ?
                $fixed['monthlyPayment']['adjustment'] : 0;
            $detail->depreciation_adj = $fixed['depreciation']['adjustment'];
            $detail->finance_cost_adj = isset($fixed['financeCost']['adjustment']) ? $fixed['financeCost']['adjustment'] : 0;
            $detail->insurance_adj = $fixed['insurance']['adjustment'];
            $detail->fee_renewal_adj = $fixed['feeRenewal']['adjustment'];

            $detail->fixed_reimbursement = $fixed['fixedReimbursementWithBup']['amount'];
            $detail->fixed_adj = $fixed['fixedReimbursementWithBup']['adjustment'];

        } else {    // predefined fix
            $detail = $this->fillDetailWithPresetFixed($detail, $fixed);
        }

        if (count($variable) > 1) {

            $detail->fuel_economy = $variable['fuelEconomy']['amount'];

            //TODO IS THIS CORRECT TO COMMENT OUT, SEE JERZY
//            $detail->fuel_price = $variable['fuelAverage']['amount'];
//            $detail->fuel_price_adj = $variable['fuelAverage']['adjustment'];

            $detail->maintenance = $variable['maintenance']['amount'];
            $detail->repair = $variable['repair']['amount'];

            $detail->cents_per_mile = $variable['centsPerMile']['amount'];

            $detail->fuel_economy_adj = $variable['fuelEconomy']['adjustment'];

            $detail->maintenance_adj = $variable['maintenance']['adjustment'];
            $detail->repair_adj = $variable['repair']['adjustment'];

            $detail->cents_per_mile_adj = $variable['centsPerMile']['adjustment'];

            $detail->business_use_percent = $business_use_percent;
        } else {
            $detail = $this->fillDetailWithPresetVariable($detail, $variable);
        }


        $detail->save();
        $detail_id = $detail->id;


        $driver_profile = DriverProfile::find($driver_id);
        if ($driver_profile && $driver_profile->reimbursement_detail_id == null) {
            $driver_profile->reimbursement_detail_id = $detail_id;
            $driver_profile->save();
        }
        return $detail_id;
    }

    private function fillDetailWithPresetFixed($detail, $fixed)
    {
        $detail->capital_cost = 0;
        $detail->resale_value = 0;
        $detail->tax = 0;
        $detail->monthly_payment = 0;
        $detail->depreciation = 0;
        $detail->finance_cost = 0;
        $detail->insurance = 0;
        $detail->fee_renewal = 0;

        $detail->capital_cost_adj = 0;
        $detail->resale_value_adj = 0;
        $detail->tax_adj = 0;
        $detail->monthly_payment_adj = 0;
        $detail->depreciation_adj = 0;
        $detail->finance_cost_adj = 0;
        $detail->insurance_adj = 0;
        $detail->fee_renewal_adj = 0;

        $detail->fixed_adj = 0;
        $detail->fixed_reimbursement = $fixed[0]['amount'] + 0;

        $detail->business_use_percent = 100;

        return $detail;
    }

    private function fillDetailWithPresetVariable($detail, $variable)
    {
        $detail->fuel_economy = 0;
        $detail->maintenance = 0;
        $detail->repair = 0;

        $detail->fuel_economy_adj = 0;
        $detail->fuel_price_adj = 0;
        $detail->maintenance_adj = 0;
        $detail->repair_adj = 0;

        $detail->cents_per_mile = $variable['variableReimbursement']['amount'] + 0;
        $detail->cents_per_mile_adj = 0;

        return $detail;
    }

    private function insertOrUpdateMonthlyReimbursement($data)
    {
        // $year, $month, $user_id, $driver_id, $detail_id, $fixed, $variable)
        $inserted = 0;
        $updated = 0;

        $monthly = MonthlyReimbursement::where('user_id', $data->driver->user_id)->where('year', $data->year)
            ->where('month', $data->month)->first();
        if ($monthly == null) {
            \DB::table('monthly_reimbursements')->insert(
                ['year' => $data->year, 'month' => $data->month,
                    'user_id' => $data->driver->user_id,
                    'driver_profile_id' => $data->driver->driver_id,
                    'fixed_reimbursement' => $data->fixed_amount,
                    'fixed_status' => $data->street_light_id,
                    'variable_reimbursement' => $data->cents_per_mile,
                    'adjustment_amount' => $data->adjustment_amount,
                    'vehicle_depreciation' => $data->depreciation,
                    'taxable_amount' => $data->taxable_amount,
                    'within_grace_period' => $data->within_grace_period,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]
            );
            $inserted = 1;
        } else {
            \DB::table('monthly_reimbursements')
                ->where('year', $data->year)->where('month', $data->month)->where('user_id', $data->driver->user_id)
                ->update([
                    'driver_profile_id' => $data->driver->driver_id,
                    'fixed_reimbursement' => $data->fixed_amount,
                    'fixed_status' => $data->street_light_id,
                    'variable_reimbursement' => $data->cents_per_mile,
                    'adjustment_amount' => $data->adjustment_amount,
                    'vehicle_depreciation' => $data->depreciation,
                    'taxable_amount' => $data->taxable_amount,
                    'within_grace_period' => $data->within_grace_period,
                    'updated_at' => Carbon::now()
                ]);
            $updated = 1;
        }
        return (object)['inserted' => $inserted, 'updated' => $updated];
    }

    public function calculateFixedReimbursementForDriver($user_id)
    {

        $driver_profile = DriverProfile::where('user_id', $user_id)->first();
        $vehicle_profile = VehicleProfile::find($driver_profile->vehicle_profile_id);
        $address = $driver_profile->address();
        $company_options = CompanyOptions::where('company_id', $driver_profile->company_id)->first();
        $geo_data = (object)['year' => 2017, 'month' => 1, 'plan' => $company_options->service_plan,
            'mileage_band_id' => $driver_profile->mileage_band_id,
            'territory_type_id' => $driver_profile->territory_type_id,
            'territory_list' => $driver_profile->territory_list,
            'address' => $address];
        $rd = (object)[
            'geo_data' => $geo_data, 'vehicle_profile' => $vehicle_profile
        ];

        // TODO: what is this error? -frank
//        $driver =

        $results = $this->calculateFixedReimbursementProxy($rd);
        return $results;

    }

    public function getDriversWithYearProfile($year)
    {
        $rec = \DB::select(\DB::raw("SELECT c.name AS company, d.id, u.last_name, u.first_name 
  FROM vehicle_profiles vp, vehicles v, vehicle_categories vc, driver_profiles d, users u, companies c, company_profiles cp
  WHERE vp.vehicle_id = v.id AND v.category_id = vc.id AND v.year = $year
  AND d.vehicle_profile_id = vp.id AND d.deleted_at IS NULL
  AND d.user_id = u.id AND u.deleted_at IS NULL  
  AND d.company_id = c.id and c.id = cp.company_id and cp.country_id = 1
  ORDER BY c.name, u.last_name, u.first_name LIMIT 20"));

        $res = [];

        foreach ($rec as $r) { // prepare reimbursement data
            $driver_profile = DriverProfile::find($r->id);
            $vehicle_profile = VehicleProfile::find($driver_profile->vehicle_profile_id);
            $address = $driver_profile->address();
            $company_options = CompanyOptions::where('company_id', $driver_profile->company_id)->first();
            $geo_data = (object)['year' => 2017, 'month' => 1, 'plan' => $company_options->service_plan,
                'mileage_band_id' => $driver_profile->mileage_band_id,
                'territory_type_id' => $driver_profile->territory_type_id,
                'territory_list' => $driver_profile->territory_list,
                'address' => $address];
            $rd = (object)[
                'geo_data' => $geo_data, 'vehicle_profile' => $vehicle_profile
            ];

            $results = $this->calculateFixedReimbursementProxy($rd);

            $res[] = ['company' => $r->company, 'last_name' => $r->last_name, 'first_name' => $r->first_name, 'fix_reimbursement' => $results['fixed_reimbursement']];
        }

        return ['drivers' => $res];
    }

    public function getMileageRate($country_id, $year, $month)
    {
        if ($country_id == 1) { // US
            $row = \DB::table('irs_standard_rates')
                ->select('rate')
                ->where('year', '<', $year)
                ->orderBy('year', 'desc')
                ->orderBy('month', 'desc')
                ->orWhere(function ($query) use ($year, $month) {
                    $query->where('year', '=', $year)
                        ->where('month', '<=', $month);
                })
                ->first();
            return $row->rate / 100;
        }
    }

    /**
     * Populating 12 months of standard rate for selected year.
     *
     * @param $country_id
     * @param $year
     *
     * @return array $rateArr - rates for all 12 months
     */
    public function getMileageRateArr($country_id, $year)
    {
        $rateArr = [];

        if ($year == -1) {
            return $rateArr;
        }

        if ($country_id == 1) {
            $last_entry = \DB::table('irs_standard_rates')
                ->select('rate')
                ->where('year', '<', $year)
                ->orderBy('year', 'desc')
                ->orderBy('month', 'desc')
                ->first();

            // pre-populate with last rate in case we do not have entries
            for ($mm = 1; $mm <= 12; $mm++) {
                $rateArr[$mm] = $last_entry->rate / 100;
            }

            //  any changes in current year?
            $rows = \DB::table('irs_standard_rates')
                ->select('rate', 'month')
                ->where('year', '=', $year)
                ->orderBy('month', 'desc')
                ->get();

            $end_month = 12;
            foreach ($rows as $row) {
                for ($mm = $end_month; $mm >= $row->month; $mm--) {
                    $rateArr[$mm] = $row->rate / 100;
                }
                $end_month = $row->month - 1;
            }
        }
        return $rateArr;
    }

    public function getMileageRateYears($country_id)
    {

    }

    public function setMileageRate($country_id, $year, $month, $rate)
    {

    }

    /**
     * @param Request $request : company_id, year, month, array with user_ids
     */
    public function getReimbursementForDrivers(Request $request)
    {

        // \Debugbar::info('DEB: getReimbursementForDrivers Request: ');
        // \Debugbar::info($request);

        $reimbursement = [];
        $company = Company::find($request->company_id);

        $show_tax_limit = $company->show_tax_limit > 0 ? true : false;
        $mileage_entry_method = $company->mileage->mileage_entry_method;
        $commuter = $mileage_entry_method == 'Daily Commuter' ? true : false;


        $total_fixed = 0;
        $total_adjustment = 0;
        $total_fixed_paid = 0;
        $total_variable = 0;
        $total_mileage = 0;
        $total_personal = 0;
        $total_total = 0;
        $total_tax_limit = 0;
        $total_depreciation = 0;
        $total_commuter = 0;
        $total_taxable_reimbursement = 0;

        if ($show_tax_limit) {
            $mileage_rate_arr = $this->reimbursementController->getMileageRateArr(1, $request->year);
        }

        foreach ($request->users_arr as $user_id) {

            $data = \DB::table('monthly_reimbursements AS mr')->leftJoin('monthly_mileages AS mm', function ($join) {
                $join->on('mr.user_id', '=', 'mm.user_id');
                $join->on('mr.year', '=', 'mm.year');
                $join->on('mr.month', '=', 'mm.month');
            })
                ->join('users as u', 'mr.user_id', '=', 'u.id')
                ->leftJoin('driver_profiles as dp', 'mr.driver_profile_id', '=', 'dp.id')
                ->leftJoin('divisions as di', 'dp.division_id', '=', 'di.id')
                ->leftJoin('monthly_approvals as ma', function ($join) {
                    $join->on('mr.year', '=', 'ma.year');
                    $join->on('mr.month', '=', 'ma.month');
                    $join->on('dp.company_id', '=', 'ma.company_id');
                })
                ->select('mr.*', 'mm.business_mileage', 'mm.personal_mileage', 'ma.approval_date', 'u.last_name', 'u.first_name',
                    'dp.employee_number', 'dp.cost_center', 'di.name as division', 'dp.start_date', 'dp.deleted_at')
                ->where('mr.user_id', $user_id)
                ->where('mr.year', $request->year)
                ->where('mr.month', $request->month)->first();

            if ($data) {

                // \Debugbar::info("REIMB for " . $user_id . " F=" . $data->fixed_reimbursement . " C/M=" . $data->variable_reimbursement);

                $variable_reimburs = $this->getVariableReimbursement($request->company_id, $request->year,
                    $request->month, $data->variable_reimbursement, $data->business_mileage);

                if ($commuter) {
                    $from_date = sprintf("%04d-%02d-01", $request->year, $request->month);
                    $n_year = $request->month == 12 ? $request->year + 1 : $request->year;
                    $n_month = $request->month == 12 ? 1 : $request->month + 1;
                    $to_date = sprintf("%04d-%02d-01", $n_year, $n_month);
                    $query = "SELECT IFNULL(SUM(dm.commuter_mileage),0) as commuter " .
                        "FROM daily_mileages dm WHERE dm.user_id = $user_id AND dm.trip_date >= '$from_date' " .
                        "AND trip_date < '$to_date'";
                    $comm_totals = \DB::select($query);
                    $commuter_mileage = $comm_totals[0]->commuter;
                    $total_commuter += $commuter_mileage;
                } else {
                    $commuter_mileage = 0;
                }

                $reimbursement[] = [
                    'user_id' => $user_id,
                    'last_name' => $data->last_name,
                    'first_name' => $data->first_name,
                    'division' => $data->division,
                    'employee_number' => $data->employee_number,
                    'cost_center' => $data->cost_center,
                    'fixed' => number_format($data->fixed_reimbursement, 2),
                    'adjustment' => number_format($data->adjustment_amount, 2),
                    'fixed_paid' => number_format($data->fixed_reimbursement - $data->adjustment_amount, 2),
                    'mileage' => number_format($data->business_mileage),
                    'personal_mileage' => number_format($data->personal_mileage),
                    'commuter_mileage' => number_format($commuter_mileage),
                    'centPerMile' => number_format($data->variable_reimbursement, 2),
                    'variable' => number_format($variable_reimburs, 2),
                    'total' => number_format($data->fixed_reimbursement - $data->adjustment_amount + $variable_reimburs, 2),
                    'direct_pay_date' => !empty($data->approval_date) ? date("D, M jS, Y", strtotime($data->approval_date)) : null,
                    'status' => null,
                    'depreciation' => 0,
                    'tax_limit' => 0,
                    'taxable_reimbursement' => 0,
                    'show_link' => 1,
                    'created_at' => $data->start_date,
                    'deleted_at' => substr($data->deleted_at, 0, 10)];

                $total_fixed += round($data->fixed_reimbursement, 2);
                $total_adjustment += round($data->adjustment_amount, 2);
                $total_fixed_paid += round($data->fixed_reimbursement, 2) - round($data->adjustment_amount, 2);
                $total_mileage += $data->business_mileage;
                $total_personal += $data->personal_mileage;
                $total_variable += $variable_reimburs;
                $total_total += round($data->fixed_reimbursement, 2) - round($data->adjustment_amount, 2) + $variable_reimburs;
            }
        }


        $reimbursement[] = [
            'last_name' => "TOTALS",
            'first_name' => null,
            'fixed' => number_format($total_fixed, 2),
            'adjustment' => number_format($total_adjustment, 2),
            'fixed_paid' => number_format($total_fixed_paid, 2),
            'mileage' => number_format($total_mileage),
            'personal_mileage' => number_format($total_personal),
            'commuter_mileage' => number_format($total_commuter),
            'centPerMile' => '',
            'variable' => number_format($total_variable, 2),
            'total' => number_format($total_total, 2),
            'direct_pay_date' => null,
            'status' => null,
            'depreciation' => number_format($total_depreciation, 2),
            'tax_limit' => $total_tax_limit,
            'taxable_reimbursement' => $total_taxable_reimbursement,
            'show_link => 0'

        ];
        return ['reimbursement' => $reimbursement];
    }

    //TODO this may not grab the correct vehicle profile

    /**
     * BELIEVED TO BE JUST FOR TESTING
     * @param $user_id
     * @param $year
     * @param $month
     * @return object
     */
    public function reimbursementDataFromDriver($user_id, $year, $month)
    {

        $rd = [];

        // driver profile data
        $dp = DriverProfile::where('user_id', $user_id)->first();
        $rd['company_id'] = $dp->company_id;
        $rd['mileage_band_id'] = $dp->mileage_band_id;
        $rd['territory_type'] = $dp->territory_type;
        $rd['territory_list'] = $dp->territory_list;

        // company data
        $company = Company::find($dp->company_id)->first();
        $companyOptions = $company->options()->first();
        $rd['service_plan'] = $companyOptions->service_plan;

        // vehicle profile
        $vp = $dp->vehicleProfile()->first();
        $rd['retention'] = $vp->retention;
        $rd['capital_cost'] = $vp->capital_cost;
        $rd['resale_condition'] = $vp->resale_condition;
        $rd['business_use_percent'] = $vp->business_use_percent;
        $rd['calculate_fixed'] = $vp->calculate_fixed;
        $rd['preset_fixed'] = $vp->preset_fixed;
        $rd['calculate_variable'] = $vp->calculate_variable;
        $rd['preset_variable'] = $vp->preset_variable;
        $adj = [];
        $adj['capital_cost_adj'] = $vp->capital_cost_adj;
        $adj['capital_cost_tax_adj'] = $vp->capital_cost_tax_adj;
        $adj['depreciation_adj'] = $vp->depreciation_adj;
        $adj['fee_renewal_adj'] = $vp->fee_renewal_adj;
        $adj['finance_cost_adj'] = $vp->finance_cost_adj;
        $adj['fixed_adj'] = $vp->fixed_adj;
        $adj['fixed_adj_text'] = $vp->fixed_adj_text;
        $adj['fuel_economy_adj'] = $vp->fuel_economy_adj;
        $adj['fuel_price_adj'] = $vp->fuel_price_adj;
        $adj['insurance_adj'] = $vp->insurance_adj;
        $adj['maintenance_adj'] = $vp->maintenance_adj;
        $adj['monthly_payment_adj'] = $vp->monthly_payment_adj;
        $adj['repair_adj'] = $vp->repair_adj;
        $adj['resale_value_adj'] = $vp->resale_value_adj;
        $adj['variable_adj'] = $vp->variable_adj;
        $adj['variable_adj_text'] = $vp->variable_adj_text;
        $rd['adj'] = $adj;

        // vehicle
        $ve = $vp->vehicle()->first();
        $vehicle = [];
        $vehicle['id'] = $ve->id;
        $vehicle['vehicle_name'] = $ve->name;
        $vehicle['category_id'] = $ve->category_id;
        $vehicle['year'] = $ve->year;
        $vehicle['resale_year'] = $ve->year - $rd['retention'];
        $vehicle['favr'] = $ve->favr;
        $vehicle['msrp'] = $ve->msrp;
        $vehicle['msrp_ca'] = $ve->msrp_ca;
        $vehicle['invoice'] = $ve->invoice;
        $vehicle['invoice_ca'] = $ve->invoice_ca;
        $vehicle['blue_book'] = $ve->blue_book;
        $vehicle['epa_city'] = $ve->epa_city;
        $vehicle['epa_highway'] = $ve->epa_highway;
        $vehicle['epa_city_ca'] = $ve->epa_city_ca;
        $vehicle['epa_highway_ca'] = $ve->epa_highway_ca;
        $vehicle['blended_vehicle'] = $ve->blended_vehicle;
        $vehicle['mileage_group_id'] = $ve->mileage_group_id;
        $vehicle['rank'] = $ve->rank;
        $rd['vehicle'] = $vehicle;

        // FAVR
        $favrParameters = FavrParameter::find($ve->year)->first();
        if ($favrParameters) {
            $favr['vehicle_cost_percentage'] = $favrParameters->vehicle_cost_percentage;
            $favr['profile_vehicle_percentage'] = $favrParameters->profile_vehicle_percentage;
        } else {
            $favr = null;
        }
        $rd['favr'] = $favr;

        // geo data
        $address = [];
        $addr = $dp->address()->first();
        $address['zip_postal'] = $addr->zip_postal;
        $address['state_province_id'] = $addr->state_province;
        $address['country_id'] = $addr->country_id;
        if ($addr->country_id == 1) { // USA
            $zip = ZipCode::find($addr->zip_postal);
            $address['county_id'] = $zip->county_id;
        }
        // mapping Canadian provinces to states. It was not used in resale calculation. Bug was discovered in December 2017.
        if ($addr->country == 2) {
            $address['mapped_to_state_id'] = $addr->stateProvince->use_state_id;
        } else {
            $address['mapped_to_state_id'] = $address['state_province_id'];
        }
        $address['geo_id'] = $vehicle->year >= 2013 ?
            $rd['address'] = $address->state_province_id :
            $rd['address'] = $address->stateProvince->region_id; // TODO get it from state province table, column: region_id

        // taxes
        $taxes = [];
        $state = TaxesFeesUS::where('year', $vehicle->year)->where('state_province_id', $address->state_province_id)->select('sales_tax')->first();
        $county = CountyTax::where('year', $vehicle->year)->where('county_id', $address->county_id)->select('sales_tax', 'personal_property_tax')->first();
        $taxes['state'] = $state;
        $taxes['county'] = $county;
        $rd['taxes'] = $taxes;


        return (object)['reimbursementData' => $rd];

    }

    /**
     * @param $rd :  { geo_data { year, month, plan, mileage_band_id, Address address, territory_type_id, territory_list },
     *                vehicle_profile { vehicle_id, capital_cost, resale_condition, retention, year }
     *              }
     * @return mixed array with reimbursement elements: title, label, amount
     */
    public function calculateReimbursement(Request $request)
    {
        // convert associate nested arrays into nested objects
        $rd = json_decode(json_encode($request->data), FALSE);

        $fixed_reimb = $this->calculateFixedReimbursementProxy($rd);
        // \Debugbar::info($fixed_reimb);
        $variable_reimb = $this->calculateVariableReimbursementProxy($rd);
        // \Debugbar::info($variable_reimb);

        $res = array_merge($fixed_reimb, $variable_reimb);
        return $res;

    }

    public function calculateFixedReimbursementProxy($rd)
    {
        $reimbursement = [];

        try {
            if ($rd->vehicle_profile->calculate_fixed == 0) {
                $reimbursement[] = ['key' => 'fixedReimbursement', 'title' => 'Fixed Reimbursement', 'label' => 'Preset Value', 'adjustment' => 0,
                    'amount' => $rd->vehicle_profile->preset_fixed + 0, 'amount_adj' => $rd->vehicle_profile->preset_fixed + 0];
                return ['fixedReimbursement' => round($rd->vehicle_profile->preset_fixed, 2), 'fixedReimbursementDetails' => $reimbursement];
            }

            $reimbursement = [];
            $fixed_reimbursement = 0;
            // if we have details we do not have to calculate amounts
            $details = $rd->geo_data->reimbursement_detail_id > 0 ?
                ReimbursementDetail::find($rd->geo_data->reimbursement_detail_id) : null;

            if ($details) {
                $capital_cost_tax_adj = $details->tax * (1 + $details->tax_adj / 100.0);
                $depreciation_adj = $details->depreciation * (1 + $details->depreciation_adj / 100.0);
                $finance_cost_adj = $details->finance_cost * (1 + $details->finance_cost_adj / 100.0);
                $insurance_adj = $details->insurance * (1 + $details->insurance_adj / 100.0);
                $fee_renewal_adj = $details->fee_renewal * (1 + $details->fee_renewal_adj / 100);

                //\Debugbar::info("FROM details: CC=$capital_cost_tax_adj DE=$depreciation_adj FC=$finance_cost_adj IN=$insurance_adj FEE=$fee_renewal_adj");

                $reimbursement['capital_cost_tax'] = ['title' => 'Capital Cost Tax'];
                $reimbursement['finance_cost'] = ['title' => 'Finance Cost'];
                $reimbursement['insurance'] = ['title' => 'Insurance', 'insurance_city' => $details->insurance_city];
                $reimbursement['fee_renewal'] = ['title' => 'Insurance'];
                $reimbursement['depreciation'] = ['title' => 'Depreciation', 'label' => null, 'amount' => $details->depreciation,
                    'adjustment' => $details->depreciation_adj, 'amount_adj' => $depreciation_adj];

            } else {

                if ($rd->geo_data->plan == 'FAVR') {
                    $rd->favr = FavrParameter::find($rd->vehicle_profile->vehicle->year);
                    $rd->geo_data->taxes = new \stdClass();
                    $rd->geo_data->taxes = (object)$this->getTaxData($rd->vehicle_profile->vehicle->year, $rd->geo_data->address);
                }

                ///////////////////// CAPITAL COST /////////////////////////
                $results = $rd->geo_data->plan == 'FAVR' ?
                    $this->invoiceCostWithTaxes($rd)
                    :
                    $this->capitalCost463($rd);
                $capital_cost = $results['amount'];
                //\Debugbar::info("CAPITAL COST : $capital_cost");
                // \Debugbar::info($results['details']);
                $reimbursement['capitalCost'] = $results['details'];

                ///////////////////// RESALE VALUE /////////////////////////
                $results = $this->resaleValue($rd);
                $resale_value = $results['amount'];
                $reimbursement['resaleValue'] = $results['details'];

                ///////////////////// CAPITAL COST TAX OR PROPERTY TAX /////////////////////////
                $results = $this->capitalCostTax($capital_cost, $rd);
                $capital_cost_tax_adj = $results['amount'];
                $reimbursement['tax'] = $results['details'];

                ///////////////////// MONTHLY PAYMENT ///////////////////1/////////////////
                if ($rd->geo_data->plan != 'FAVR') {
                    $results = $this->monthlyPayment($capital_cost, $resale_value, $rd);
                    $monthly_payment = $results['amount'];
                    $reimbursement['monthlyPayment'] = $results['details'];
                }

                ///////////////////////// DEPRECIATION ///////////////////////////////
                $depreciation = ($capital_cost - $resale_value) / (12 * $rd->vehicle_profile->retention);
                // \Debugbar::info("DEPRECIATION $depreciation = ($capital_cost - $resale_value) / (12 * $rd->vehicle_profile->retention);");
                $depreciation_adj = $depreciation * (1 + $rd->vehicle_profile->depreciation_adj / 100.0);
                $reimbursement['depreciation'] = ['title' => 'Depreciation', 'label' => null, 'amount' => $depreciation,
                    'adjustment' => $rd->vehicle_profile->depreciation_adj, 'amount_adj' => $depreciation_adj];

                /////////////////////// FINANCE COST //////////////////////////////
                if ($rd->geo_data->plan != 'FAVR') {
                    $finance_cost = $monthly_payment - $depreciation;
                    $finance_cost_adj = $finance_cost * (1 + $rd->vehicle_profile->finance_cost_adj / 100.0);
                    $reimbursement['financeCost'] = ['title' => 'Finance Cost', 'label' => null, 'amount' => $finance_cost,
                        'adjustment' => $rd->vehicle_profile->finance_cost_adj, 'amount_adj' => $finance_cost_adj];
                } else {
                    $finance_cost_adj = 0;
                }

                ////////////////////// INSURANCE ////////////////////////////////
                $results = $this->reimbursementInsurance($rd);
                $insurance_adj = $results['amount'];
                $reimbursement['insurance'] = $results['details'];

                /////////////////////// FEES / RENEWAL ///////////////////////////////
                $results = $this->reimbursementFees($rd);
                $fee_renewal_adj = $results['amount'];
                $reimbursement['feeRenewal'] = $results['details'];

            }

            ////////////////////// PURE REIMBURSEMENT /////////////////////////////

            $fixed_reimbursement = $capital_cost_tax_adj + $depreciation_adj + $finance_cost_adj + $insurance_adj + $fee_renewal_adj;
            // \Debugbar::info("FIXED = $fixed_reimbursement = $capital_cost_tax_adj + $depreciation_adj + $finance_cost_adj + $insurance_adj + $fee_renewal_adj;");
            $reimbursement['fixedReimbursement'] = ['title' => 'Fixed Reimbursement', 'label' => null,
                'amount' => $fixed_reimbursement, 'adjustment' => 0, 'amount_adj' => round($fixed_reimbursement, 2)];

            ////////////////////// FIXED REIMBURSEMENT WITH BPU ///////////////////////
            $title = $rd->vehicle_profile->fixed_adj == 0 ? '' : 'Fixed Adjustment';
            $amount = $fixed_reimbursement * $rd->vehicle_profile->business_use_percent / 100;
            // \Debugbar::info("FIX * bpu $amount = $fixed_reimbursement * $rd->vehicle_profile->business_use_percent / 100;");

            $fixed_reimbursement_adj = $amount + $rd->vehicle_profile->fixed_adj;
            $reimbursement['fixedReimbursementWithBup'] = ['title' => $title, 'label' => null,
                'amount' => $amount, 'adjustment' => $rd->vehicle_profile->fixed_adj,
                'amount_adj' => round($fixed_reimbursement_adj, 2)];

            return ['fixedReimbursement' => round($fixed_reimbursement_adj, 2),
                'fixedReimbursementDetails' => $reimbursement];
        } catch (Exception $e) {
            \Log::info("Failed in Fix Reimbursement " . $e->getMessage());
        }
    }


    public function calculateVariableReimbursementProxy($rd)
    {

        \Log::info('INFO FOR DRIVER IN calculateVariableReimbursementProxy');
        if ($rd->vehicle_profile->calculate_variable == 0) {
            $reimbursement['variableReimbursement'] = ['title' => 'Variable Reimbursement', 'label' => 'Preset Value',
                'amount' => $rd->vehicle_profile->preset_variable];
            return ['variableReimbursement' => $rd->vehicle_profile->preset_variable,
                'variableReimbursementDetails' => $reimbursement];
        }

        $reimbursement = [];
        $state_province_name = StateProvince::find($rd->geo_data->address->state_province_id)->name;

        $details = $rd->geo_data->reimbursement_detail_id > 0 ?
            ReimbursementDetail::find($rd->geo_data->reimbursement_detail_id) : null;

        //////////////////////// FUEL ECONOMY //////////////////////////////
        if ($details) {
            $epa = $details->fuel_economy * (1 + $details->fuel_economy_adj / 100.0);
        } else {
            $results = $this->getEpaPerformance($rd);
            $epa = $results['amount'];
            $reimbursement['fuelEconomy'] = $results['details'];
        }

        //////////////////////// AVERAGE FUEL COST IN THIS MONTH //////////////////////////////
        $fuel_avg_data = (object)$this->getFuelAverageCost($rd->geo_data);
        $avg_fuel = $fuel_avg_data->avg_fuel;
        $geo_list = is_array($fuel_avg_data->name_list) ?
            implode(', ', $fuel_avg_data->name_list) : $fuel_avg_data->name_list;
        $avg_fuel_adj = $avg_fuel * (1 + $rd->vehicle_profile->fuel_price_adj / 100.0);
        // \Debugbar::info("FUEL $avg_fuel_adj = $avg_fuel * (1 + " . $rd->vehicle_profile->fuel_price_adj . " / 100.0);");
        $reimbursement['fuelAverage'] = ['title' => 'Fuel Average', 'label' => null, 'amount' => $avg_fuel,
            'count' => $fuel_avg_data->count, 'geo_list' => $geo_list,
            'adjustment' => $rd->vehicle_profile->fuel_price_adj, 'amount_adj' => round($avg_fuel_adj, 2)];

        ///////////////////////////// FUEL Price ///////////////////////////////////////
        $fuel = $this->getFuelCost($rd->geo_data->address->country_id, $rd->vehicle_profile->vehicle->year, $epa, $avg_fuel_adj);
        $reimbursement['fuelPrice'] = ['title' => 'Fuel', 'label' => null, 'amount' => round($fuel, 2)];

        ///////////////////////////// MAINTENANCE AND REPAIR ///////////////////////////////////////
        if ($details) {
            $maintenance = $details->maintenance * (1 + $details->maintenance_adj / 100.0);
            $repair = $details->repair * (1 + $details->repair_adj / 100.0);
        } else {
            $results = $this->getMaintenanceAndRepair($rd);
            $maintenance = $results['maintenance_amount'];
            $reimbursement['maintenance'] = $results['maintenance_details'];
            $repair = $results['repair_amount'];
            $reimbursement['repair'] = $results['repair_details'];
        }

        ///////////////////////////// CENTS PER MILE ///////////////////////////////////////
        $cents_per_mile = $fuel + $maintenance + $repair;
        // \Debugbar::info("C/M $cents_per_mile = $fuel + $maintenance + $repair;");
        $label = $this->ReimbursementLabel('cents_per_mile', $rd);
        $cents_per_mile_adj = $cents_per_mile + $rd->vehicle_profile->variable_adj;
        $reimbursement['centsPerMile'] = ['title' => 'Variable Reimbursement', 'label' => $label, 'amount' => $cents_per_mile,
            'adjustment' => $rd->vehicle_profile->variable_adj, 'amount_adj' => round($cents_per_mile_adj, 2)];

        return ['variableReimbursement' => round($cents_per_mile_adj, 2), 'variableReimbursementDetails' => $reimbursement];

    }

    private function getMaintenanceAndRepair($rd)
    {

        if ($rd->geo_data->reimbursement_detail_id > 0) {
            $maintenance = $rd->geo_data->reimbursement_details->maintenance;
            $repair = $rd->geo_data->reimbursement_details->repair;
        } else {
            $state_arr = $rd->geo_data->territory_type_id == MULTI_STATES ?
                explode(',', $rd->geo_data->territory_list) : [$rd->geo_data->address->state_province_id];
            $state_count = 0;
            $maintenance = 0;
            $repair = 0;
            foreach ($state_arr as $state_id) {
                $mr_data = $this->getMaintenanceAndRepairPerState($rd, $state_id);
                $maintenance += $mr_data->maintenance;
                $repair += $mr_data->repair;
                $state_count++;
            }
            $maintenance /= $state_count;
            $repair /= $state_count;
        }
        $state_province_name = StateProvince::find($rd->geo_data->address->state_province_id)->name;

        $maintenance_adj = $maintenance * (1 + $rd->vehicle_profile->maintenance_adj / 100.0);
        $maintenance_details = ['title' => 'Maintenance', 'label' => null, 'amount' => $maintenance,
            'state_province_name' => $state_province_name, 'adjustment' => $rd->vehicle_profile->maintenance_adj,
            'amount_adj' => round($maintenance_adj, 2)];

        $repair_adj = $repair * (1 + $rd->vehicle_profile->repair_adj / 100.0);
        $repair_details = ['title' => 'Repair', 'label' => null, 'amount' => $repair,
            'state_province_name' => $state_province_name, 'adjustment' => $rd->vehicle_profile->repair_adj,
            'amount_adj' => round($repair_adj, 2)];

        return ['maintenance_amount' => $maintenance_adj, 'repair_amount' => $repair_adj,
            'maintenance_details' => $maintenance_details,
            'repair_details' => $repair_details];
    }

    private function getMaintenanceAndRepairPerState($rd, $state_province_id)
    {
        if ($rd->vehicle_profile->vehicle->year == 2009) {
            return $this->getMaintenanceAndRepair_2009($rd, $state_province_id);
        } else if ($rd->vehicle_profile->vehicle->year >= 2010) {
            return $this->getMaintenanceAndRepair_2010($rd, $state_province_id);  // Centrilogic changed data structure in 2010
        } else {
            // \Debugbar::error("Vehicle Profile older than 2009");
            return (object)['maintenance' => -100, 'repair' => -100];
        }
    }

    private function getMaintenanceAndRepair_2009($rd, $state_province_id)
    {

        $state_id = $this->convertMaintenanceProvinceToStates($rd->geo_data->address,
            $rd->vehicle_profile->vehicle->year, $state_province_id);
        $retention = $rd->vehicle_profile->retention;
        $mr_mileage_band_id = $this->convertToMR($rd->geo_data->mileage_band_id);

        // \Debugbar::info("2009 Maintenance: v=" . $rd->vehicle_profile->vehicle_id . " st=$state_id mb=$mr_mileage_band_id");
        $res = MaintenanceRepair2009::where('vehicle_id', $rd->vehicle_profile->vehicle_id)
            ->where('state_province_id', $state_id)
            ->where('mr_mileage_band_id', $mr_mileage_band_id)->first();

        if (!$res) {
            // \Debugbar::erorr("M&R not found in MaintenanceRepair2009: v=" . $rd->vehicle_profile->vehicle_id . " st=$state_id mb=$mr_mileage_band_id");
            return (object)['maintenance' => -100, 'repair' => -100];
        }

        if ($retention >= 3 && $retention <= 5) {
            $maintenance = $res->maintenance_y1 + $res->maintenance_y2 + $res->maintenance_y3;
            $repair = $res->repair_y1 + $res->repair_y2 + $res->repair_y3;
            $mileage = 20000;
            if ($retention >= 4) {
                $maintenance += $res->maintenance_y4;
                $repair += $res->repair_y4;
                $mileage = 25000;
            }
            if ($retention == 5) {
                $maintenance += $res->maintenance_y5;
                $repair += $res->repair_y5;
                $mileage = 30000;
            }
        } else {
            return (object)['maintenance' => 0, 'repair' => 0];
        }

        // \Debugbar::info("MR after retention checks m=$maintenance r=$repair m=$mileage");

        // added to overcompensate on maintenance for high mileage
        $factor = $rd->geo_data->mileage_band_id >= 9 ? 0.9 : 1.0;

        $jkm = 100 * $factor * $maintenance / ($retention * $mileage);
        $jkr = 100 * $repair / ($retention * $mileage);
        // \Debugbar::info("MR Final calculation: m=$jkm=100 * $factor * $maintenance / ($retention * $mileage)");
        // \Debugbar::info("MR Final calculation: m=$jkr=100 * $repair / ($retention * $mileage)");

        return (object)['maintenance' => 100 * $factor * $maintenance / ($retention * $mileage),
            'repair' => 100 * $repair / ($retention * $mileage)];

    }

    private function convertToMR($mileage_band_id)
    {
        if ($mileage_band_id <= 3) {
            return 3;
        } elseif ($mileage_band_id <= 5) {
            return 4;
        } else {
            return 5;
        }
    }

    private function getMaintenanceAndRepair_2010($rd, $state_province_id)
    {

        // map Canadian Provinces to States ( two different ways
        $state_id = $this->convertMaintenanceProvinceToStates($rd->geo_data->address,
            $rd->vehicle_profile->vehicle->year, $state_province_id);

        //\Debugbar::info("getMaintenanceAndRepair_2010 convert $state_province_id into $state_id");

        // calculate total mileage, based on midlde point of mileage bands
        $mileage_band = MileageBand::find($rd->geo_data->mileage_band_id);
        $start_mileage = $mileage_band->mileage_start / 1000;
        $end_mileage = $mileage_band->mileage_end > 0 ? $mileage_band->mileage_end / 1000 : 60;
        $total_mileage = $rd->vehicle_profile->retention * ($start_mileage + $end_mileage) / 2;

        //\Debugbar::info("total_mileage=$total_mileage");

        // extrapolation after 200K
        $preserve_total_mileage = $total_mileage;
        if ($total_mileage > 300) {
            $total_mileage -= 200;

            $mr200 = MaintenanceRepair2010::where('vehicle_id', $rd->vehicle_profile->vehicle->id)
                ->where('state_province_id', $state_id)
                ->where('mileage', 200)->select('maintenance', 'repair')->first();
            $mr100 = (object)['maintenance' => 0, 'repair' => 0];
        } elseif ($total_mileage > 200) {
            $total_mileage -= 100;
            $mr200 = MaintenanceRepair2010::where('vehicle_id', $rd->vehicle_profile->vehicle->id)
                ->where('state_province_id', $state_id)
                ->where('mileage', 200)->select('maintenance', 'repair')->first();
            $mr100 = MaintenanceRepair2010::where('vehicle_id', $rd->vehicle_profile->vehicle->id)
                ->where('state_province_id', $state_id)
                ->where('mileage', 100)->select('maintenance', 'repair')->first();
        } else {
            $mr100 = (object)['maintenance' => 0, 'repair' => 0];
            $mr200 = (object)['maintenance' => 0, 'repair' => 0];
        }

        //\Debugbar::info('mr100');
        //\Debugbar::info($mr100);
        //\Debugbar::info('mr200');
        //\Debugbar::info($mr200);

        // find mileage bracket
        $mr = $this->getMileageBracket($total_mileage, $rd->vehicle_profile->vehicle->id, $state_id);

        // in case we do not have data.
        if ($mr == null) {
            $bracket_maintenance = 0;
            $bracket_repair = 0;
        } else {
            $bracket_maintenance = $mr->maintenance;
            $bracket_repair = $mr->repair;
        }

        $maintenance = $mr200->maintenance - $mr100->maintenance + $bracket_maintenance;
        $repair = $mr200->repair - $mr100->repair + $bracket_repair;

        $jk = $maintenance / ($preserve_total_mileage * 1000) * 100;
        // \Debugbar::info("MAINTENANCE $jk = $maintenance/( $preserve_total_mileage * 1000 )* 100;");

        $maintenance = $maintenance / ($preserve_total_mileage * 1000) * 100; // to kilometer, to cents


        $repair = $repair / ($preserve_total_mileage * 1000) * 100; // to kilometer, to cents

        if ($preserve_total_mileage > 300) {
            $maintenance *= 1.2;
            $repair *= 1.2;
        }

        if ($rd->geo_data->address->country_id == CANADA) {
            if ($rd->vehicle_profile->vehicle->year >= 2012) {
                $winter_tire_adjustment = $preserve_total_mileage < 30 ? 1.9 : 1.0;
            } else {
                $winter_tire_adjustment = 0;
            }

            // \Debugbar::info("Conversion to Canada: $maintenance * 0.8 + $winter_tire_adjustment");
            $maintenance = $maintenance * 0.8 + $winter_tire_adjustment;
            $repair = $repair * 0.8;
        }

        return (object)['maintenance' => $maintenance, 'repair' => $repair];
    }

    private function getMileageBracket($mileage, $vehicle_id, $state_id)
    {

        if ($mileage == 0) {
            return (object)['maintenance' => 0, 'repair' => 0];
        } elseif ($mileage % 5 == 0) {

            return MaintenanceRepair2010::where('vehicle_id', $vehicle_id)
                ->where('state_province_id', $state_id)
                ->where('mileage', $mileage)->select('maintenance', 'repair')->first();
        } else {
            $low_mileage = (int)($mileage / 5) * 5;
            $high_mileage = $low_mileage + 5;
            // \Debugbar::info("Brackets found: $low_mileage - $high_mileage");
            if ($low_mileage == 0) {
                $low_mr = ['maintenance' => 0, 'repair' => 0];
            } else {
                $low_mr = MaintenanceRepair2010::where('vehicle_id', $vehicle_id)
                    ->where('state_province_id', $state_id)
                    ->where('mileage', $low_mileage)->select('maintenance', 'repair')->first()->toArray();
            }
            $high_mr = MaintenanceRepair2010::where('vehicle_id', $vehicle_id)
                ->where('state_province_id', $state_id)
                ->where('mileage', $high_mileage)->select('maintenance', 'repair')->first()->toArray();

            // \Debugbar::info("low_mr");
            // \Debugbar::info($low_mr);
            // \Debugbar::info("high_mr");
            // \Debugbar::info($high_mr);
            return (object)['maintenance' => ($low_mr['maintenance'] + $high_mr['maintenance']) / 2,
                'repair' => ($low_mr['repair'] + $high_mr['repair']) / 2];
        }
    }

    private function convertMaintenanceProvinceToStates($address, $year, $state_province_id)
    {
        if ($address->country_id == CANADA) {
            if ($year >= 2012) {
                $state_id = StateProvince::find($state_province_id)->use_state_id;
            } else {
                $province_id = $state_province_id;
                if ($province_id == 109) {         // Ontario to Illinois
                    $state_id = 15;
                } elseif ($province_id == 104 || $province_id == 105 || $province_id == 107 || $province_id == 110) {
                    $state_id = 39;                 // East Provinces to Pensylvania
                } else {
                    $state_id = 48;                 // West Provinces to Washington
                }
            }
        } elseif ($state_province_id == 52) {
            $state_id = 15;                         // Puerto Rico to Illinois
        } else {
            $state_id = $state_province_id;
        }

        return $state_id;
    }

    private function getFuelCost($country_id, $year, $epa, $avg_fuel)
    {
        return $country_id == CANADA && $year >= 2012 ? $avg_fuel * $epa : $avg_fuel / $epa * 100;
    }

    private function getEpaPerformance($rd)
    {

        if ($rd->geo_data->reimbursement_detail_id > 0) {
            $fuel_economy = $rd->geo_data->reimbursement_details->fuel_economy;
        } else {
            $vehicle = $rd->vehicle_profile->vehicle;
            $territory_type_id = $rd->geo_data->territory_type_id;
            $country_id = $rd->geo_data->address->country_id;
            $year = $vehicle->year;

            // we start recording Canadian EPA in 2012
            $epa_city = $country_id == CANADA && $year >= 2012 ? $vehicle->epa_city_ca : $vehicle->epa_city;
            $epa_highway = $country_id == CANADA && $year >= 2012 ? $vehicle->epa_highway_ca : $vehicle->epa_highway;


            // before 2015 EPA was not based on real road performance
            if ($year < 2015) {
                if ($country_id == CANADA && $year >= 2012) {
                    $epa_city /= 0.88;
                    $epa_highway /= 0.88;
                } else {
                    $epa_city *= 0.88;
                    $epa_highway *= 0.88;
                }
            }

            if ($country_id == CANADA && $year < 2012) {
                // convert mpg to kml (miles per gallon to kilometres per litre).
                $epa_city = $epa_city * 0.425143707;
                $epa_highway = $epa_highway * 0.425143707;
            }

            if ($year <= 2012) {
                if ($territory_type_id == HOME_CITY) {
                    $epa = $year == 2012 ? $epa_city * .75 + $epa_highway * 0.25 : $epa_city * .5 + $epa_highway * 0.5;
                } elseif ($territory_type_id == MULTI_CITIES || $territory_type_id == HOME_STATE) {
                    $epa = $epa_city * .5 + $epa_highway * 0.5;
                } elseif ($territory_type_id == MULTI_STATES) {
                    $epa = $epa_city * .25 + $epa_highway * 0.75;
                } else {
                    $epa = 0;
                }
            } else {
                if ($country_id == USA) {
                    if ($territory_type_id == HOME_CITY || $territory_type_id == MULTI_CITIES) {
                        $epa = 1 / (.75 / $epa_city + 0.25 / $epa_highway);
                    } elseif ($territory_type_id == HOME_STATE || $territory_type_id == MULTI_STATES) {
                        $epa = 1 / (.5 / $epa_city + 0.5 / $epa_highway);
                    } else {
                        $epa = 0;
                    }
                } elseif ($country_id == CANADA) {
                    if ($territory_type_id == HOME_CITY || $territory_type_id == MULTI_CITIES) {
                        $epa = .75 * $epa_city + 0.25 * $epa_highway;
                    } elseif ($territory_type_id == HOME_STATE || $territory_type_id == MULTI_STATES) {
                        $epa = .5 * $epa_city + 0.5 * $epa_highway;
                    } else {
                        $epa = 0;
                    }
                } else {
                    $epa = 0;
                }
            }
        }
        $epa_adj = $epa * (1 + $rd->vehicle_profile->fuel_economy_adj / 100.0);
        $details = ['title' => 'Fuel Economy', 'label' => null, 'amount' => $epa,
            'adjustment' => $rd->vehicle_profile->fuel_economy_adj, 'amount_adj' => round($epa_adj, 2)];

        return ['amount' => $epa_adj, 'details' => $details];
    }

    private function getFuelAverageCost($geo_data)
    {
        $territory_type_id = $geo_data->territory_type_id;
        $territory_list = $geo_data->territory_list;
        $city_list = '';
        $city_name = '';

        switch ($territory_type_id) {
            case HOME_CITY:
                $fuel_data = $this->findClosestFuelCity($geo_data->address);
                $city_list = $fuel_data->id;
                $name_list = $fuel_data->city_name;
                break;
            case HOME_STATE:
                $fuel_data = $this->getFuelCitiesFromStates($geo_data->address->state_province_id);
                $city_list = $fuel_data['city_ids'];
                $name_list = $fuel_data['city_names'];
                break;
            case MULTI_CITIES:
                $city_list = $territory_list;
                $name_list = $this->getCityNamesFromList($city_list);
                break;
            case MULTI_STATES:
                $fuel_data = $this->getFuelCitiesFromStates($territory_list);
                $city_list = $fuel_data['city_ids'];
                $name_list = $fuel_data['city_names'];
                break;
        }

        $cities = is_array($city_list) ? implode(',', $city_list) : $city_list;

        if (!empty($city_list)) {
            $greaterThanZero = $geo_data->year > 2015 ? " AND price > 0 " : "";
            $query = "SELECT COUNT(*) AS found, SUM(price) as total FROM fuel_prices " .
                "WHERE fuel_city_id IN ($cities) AND MONTH(updated_at) = $geo_data->month " .
                "AND YEAR(updated_at) = $geo_data->year $greaterThanZero";
            $average_cost = \DB::select(\DB::raw($query))[0];
            $total = $average_cost->found;
            $sum = $average_cost->total;

            if ($geo_data->address->country_id == CANADA) {
                $sum /= 100;
            }
            $avg_fuel = $total > 0 ? $sum / $total : 0;
        } else {
            $avg_fuel = 0;
        }

        return ['avg_fuel' => $avg_fuel, 'count' => $total, 'name_list' => $name_list];
    }

    private function getCityNamesFromList($cities)
    {
        $list = is_array($cities) ? implode(',', $cities) : $cities;
        $city_names = [];
        $query = "SELECT name FROM fuel_cities WHERE id IN ($list) ORDER BY name";
        $rows = \DB::select(\DB::raw($query));
        foreach ($rows as $row) {
            $city_names[] = $row->name;
        }
        return $city_names;
    }

    private function getFuelCitiesFromStates($state_list)
    {

        if (is_array($state_list)) {
            $state_list = implode(',', $state_list);
        }

        $query = "SELECT id, name FROM fuel_cities WHERE state_province_id IN ($state_list) " .
            " AND deleted_at IS NULL ORDER BY name";
        $rows = \DB::select(\DB::raw($query));
        foreach ($rows as $row) {
            $city_ids[] = $row->id;
        }

        $query = "SELECT name from state_provinces WHERE id IN ($state_list) ORDER BY name";
        $rows = \DB::select(\DB::raw($query));
        foreach ($rows as $row) {
            $city_names[] = $row->name;
        }

        //$cities = FuelCity::whereIn('state_province_id',[$state_list])->select('id')->get()->toArray();
        // this does not work as list of states are embeded in quotes: ['101,102'] instead of [101,102]
        return ['city_ids' => $city_ids, 'city_names' => $city_names];
    }

    private function ReimbursementLabel($key, $rd, $extra_data = null)
    {

        $vehicle = $rd->vehicle_profile->vehicle;
        $vehicle_profile = $rd->vehicle_profile;

        switch ($key) {
            case 'capitalCost':
                return $rd->geo_data->plan != 'FAVR' ?
                    $vehicle->year . ' ' . $vehicle->name . ', ' . $this->formattedCapitalCost($vehicle_profile->capital_cost)
                    :
                    "$vehicle->year $vehicle->name, (Invoice + Taxes) * 95%";
                break;

            case 'resaleValue':
                $label = $vehicle_profile->vehicle->year . ' ' .
                    $vehicle_profile->vehicle->name . ', ' . $vehicle_profile->retention . ' Years, ';
                $label .= ResaleCondition::find($vehicle_profile->resale_condition_id)->name;
                $label .= ', ' . $this->getGeoName($vehicle_profile->vehicle->year, $rd->geo_data->address->state_province_id);
                $label .= ', ' . $this->formattedMileageBand($rd->geo_data->mileage_band_id) . ' Miles';
                return $label;
                break;

            case 'tax':
                $state_name = StateProvince::find($rd->geo_data->address->state_province_id)->name;
                if ($rd->geo_data->address->country_id == USA) {
                    $county_id = ZipCode::where('zip_code', $rd->geo_data->address->zip_postal)->pluck('county_id')->first();
                    $county_name = County::find($county_id)->name;
                }
                $label = $rd->geo_data->plan != 'FAVR' ? 'Capital Cost, ' : '';
                if ($rd->geo_data->address->country_id == USA) {
                    return $label . "$state_name, $county_name, " . $vehicle_profile->vehicle->year . ", " .
                        $vehicle_profile->retention . ' Years';
                } else {
                    return $label . "$state_name, " . $vehicle_profile->vehicle->year . ", " .
                        $vehicle_profile->retention . ' Years';
                }
                break;

            case 'monthlyPayment':
                return "Capital Cost, Resale Value, " . $rd->vehicle_profile->vehicle->year . ", " . $rd->vehicle_profile->retention . " Years";
                break;

            case 'depreciation':
                return '(Capital Cost - Resale Value)/(Retention * 12)';
                break;

            case 'financeCost':
                return 'Monthly Payment - Depreciation';
                break;

            case 'insurance':
                return $extra_data . ", " . $rd->vehicle_profile->vehicle->year;
                break;

            case 'feeRenewal':
                return $extra_data . ", " . $rd->vehicle_profile->retention . " Years";
                break;

            case 'fixedReimbursement':
                return $rd->geo_data->plan == 'FAVR' ?
                    'Property Tax + Depreciation + Insurance + Fee/Renewal'
                    :
                    'Capital Cost Tax + Depreciation + Finance Cost + Insurance + Fee/Renewal';
                break;

            case 'fixedReimbursementWithBup':
                return "Business Use Percentage " . $rd->vehicle_profile->business_use_percent . "%";
                break;

            case 'fuelEconomy':
                $label = $rd->vehicle_profile->vehicle->year . ' ' . $rd->vehicle_profile->vehicle->name . ' ';
                $label .= $rd->geo_data->address->country_id == USA ? 'm/g' : 'l/100 km';
                return $label;
                break;

            case 'fuelAverage':
                $label = $extra_data->geo_list . " - based on " . $extra_data->count . " entries from " .
                    date('F Y', mktime(0, 0, 0, $rd->geo_data->month, 1, $rd->geo_data->year));
                break;

            case 'fuelPrice':
                return $rd->geo_data->address->country_id == USA ?
                    'Fuel Average / EPA Performance * 100'
                    :
                    'Fuel Average * EPA Performance';
                break;

            case 'maintenance':
                return $extra_data . ", " . $rd->vehicle_profile->retention . " Years";
                break;

            case 'repair':
                return $extra_data . ", " . $rd->vehicle_profile->retention . " Years";
                break;

            case 'centsPerMile':
                return 'Fuel + Maintenance + Repair';
                break;

            default:
                return "UNKNOWN LABEL for $key";
        }
    }


    private function monthlyPayment($capital_cost, $resale_value, $rd)
    {
        if ($rd->geo_data->reimbursement_detail_id > 0) {
            $monthly_payment = $rd->reimbursement_details->monthly_payment;
        }

        $retention = $rd->vehicle_profile->retention;
        $country_id = $rd->geo_data->address->country_id;
        $year = $rd->vehicle_profile->vehicle->year;
        $interest_rate = InterestRate::where('country_id', $country_id)->where('year', $year)->pluck('interest_rate')->first();
        $monthly_interest = $interest_rate / (100 * 12);
        $n = $retention * 12;
        $top = -$capital_cost * pow(1 + $monthly_interest, $n) + $resale_value;
        $bottom_left = 1 + $monthly_interest;
        $bottom_right = (pow(1 + $monthly_interest, $n) - 1) / $monthly_interest;

        // \Debugbar::info("capital_cost=$capital_cost resale_value=$resale_value interest_rate=$interest_rate monthly_interest=$monthly_interest");
        // \Debugbar::info("top=$top bottom_left=$bottom_left bottom_right=$bottom_right");

        $monthly_payment = (-($top / ($bottom_left * $bottom_right)));

        $monthly_payment_adj = $monthly_payment * (1 + $rd->vehicle_profile->monthly_payment_adj / 100.0);
        $details = ['title' => 'Monthly Payment', 'label' => null,
            'amount' => $monthly_payment, 'adjustment' => $rd->vehicle_profile->monthly_payment_adj,
            'amount_adj' => round($monthly_payment_adj, 2)];

        return ['amount' => $monthly_payment, 'details' => $details];
    }

    private function capitalCost463($rd)
    {
        try {
            //\Debugbar::info('capitalCost463');
            if ($rd->geo_data->reimbursement_detail_id > 0) {
                $value = $rd->reimbursement_details->capital_cost;
                $capital_cost_adj = $value * (1 + $rd->reimbursement_details->capital_cost / 100.0);
            } else {
                $capital_cost = $rd->vehicle_profile->capital_cost;
                $vehicle = $rd->vehicle_profile->vehicle;
                $address = $rd->geo_data->address;
                $value = 0;
                if ($address->country_id == USA) { // USA
                    if ($capital_cost == 'msrp') $value = $vehicle->msrp;
                    else if ($capital_cost == 'invoice') $value = $vehicle->invoice;
                    else if ($capital_cost == 'blue_book') {
                        //\Debugbar::info("BB = $vehicle->blue_book");
                        $jk = $this->fairPurchasePriceZip($vehicle->year, $vehicle->id, $vehicle->blended_vehicle, $address);
                        //\Debugbar::info("adjustment = $jk");
                        $value = round($vehicle->blue_book * (1 +
                                $this->fairPurchasePriceZip($vehicle->year, $vehicle->id, $vehicle->blended_vehicle, $address)
                                / 100), 2);
                        //\Debugbar::info("$value = $vehicle->blue_book * ( 1 + $jk / 100 ");
                    }
                } else if ($address->country_id == CANADA) { // Canada
                    if ($capital_cost == 'msrp') $value = $vehicle->msrp_ca;
                    else if ($capital_cost == 'invoice') $value = $vehicle->invoice_ca;
                    else if ($capital_cost == 'blue_book') {
                        $value = $vehicle->invoice_ca + 0.63 * ($vehicle->msrp_ca - $vehicle->invoice_ca);
                        //\Debugbar::info("CC 463 = $value = $vehicle->invoice_ca + 0.63 * ($vehicle->msrp_ca - $vehicle->invoice_ca);");
                    }
                }
                $capital_cost_adj = $value * (1 + $rd->vehicle_profile->capital_cost_adj / 100.0);
            }
            $capital_cost_details = ['title' => 'Capital Cost', 'label' => null, 'amount' => $value,
                'adjustment' => $rd->vehicle_profile->capital_cost_adj, 'amount_adj' => $capital_cost_adj];
            return ['amount' => $capital_cost_adj, 'details' => $capital_cost_details];
        } catch (\Exception $e) {
            \Log::info("Failed in capitalCost463: at line " . $e->getLine() . ' ' . $e->getMessage());
        }
    }

    private function invoiceCostWithTaxes($rd)
    {
        if ($rd->geo_data->reimbursement_detail_id > 0) {
            $amount = $rd->geo_data->reimbursement_details->capital_cost;
            $capital_cost_adj = $amount * (1 + $rd->geo_data->reimbursement_details->capital_cost_adj / 100.0);
        } else {
            $vehicle = $rd->vehicle_profile->vehicle;
            $taxes = $rd->geo_data->taxes;
            $favr = $rd->favr;
            $amount = $vehicle->invoice * $favr->vehicle_cost_percentage * (1 + ($taxes->state_tax + $taxes->county_tax) / 100);
            //\Debugbar::info("CC = $amount = $vehicle->invoice * $favr->vehicle_cost_percentage * (1 + ($taxes->state_tax + $taxes->county_tax) / 100);");
            $capital_cost_adj = $amount * (1 + $rd->vehicle_profile->capital_cost_adj / 100.0);
            //\Debugbar::info("CCADJ = $capital_cost_adj = $amount * (1 + $rd->vehicle_profile->capital_cost_adj / 100.0);");
        }
        $capital_cost_details = ['title' => 'Capital Cost', 'label' => null, 'amount' => $amount,
            'adjustment' => $rd->vehicle_profile->capital_cost_adj, 'amount_adj' => $capital_cost_adj];
        return ['amount' => $capital_cost_adj, 'details' => $capital_cost_details];
    }

    /**
     * get adjustment from fair_purchase_price table. If record does not exists, populate it from KBB tables.
     *
     * @param int $year
     * @param Vehicle $vehicle
     * @param Address $address
     * @return float
     */
    private function fairPurchasePriceZip($year, $vehicle_id, $blended_vehicle, $address)
    {

        if ($address->country_id == CANADA || $year < 2017 || empty($address->zip_postal)) {
            return 0;
        } else {
            $fpp = FairPurchasePrice::where('vehicle_id', $vehicle_id)->where('zip_postal', $address->zip_postal)->first();
            if ($fpp) {
                return $fpp->adjustment_amount;
            } else {
                $fpp = $this->CreateAndGetFairPurchasePrice($year, $vehicle_id, $blended_vehicle, $address);
                return $fpp;

            }
        }
    }

    /**
     * @param int $year
     * @param int $vehicle_id
     * @param int $blended_vehicle
     * @param Address $address
     * @return double
     */
    private function CreateAndGetFairPurchasePrice($year, $vehicle_id, $blended_vehicle, $address)
    {

        // \Debugbar:info("CreateAndGetFairPurchasePrice for $vehicle_id");

        if ($blended_vehicle == 0) {
            $rec = \DB::select(\DB::raw("SELECT rga.Adjustment AS adjustment_amount FROM vehicles avc,  
      KellyBlueBook.Vehicle v, KellyBlueBook.VehicleGroup vg, KellyBlueBook.RegionGroupAdjustment rga, KellyBlueBook.Region r, 
      KellyBlueBook.RegionZipCode rzc, KellyBlueBook.ZipCode zc
      WHERE avc.id = $vehicle_id 
      AND v.VehicleId = avc.kbb_vehicle_id
      AND v.VehicleId = vg.VehicleId 
      AND vg.GroupId = rga.GroupId 
      AND rga.RegionAdjustmentTypeId = 2
      AND rga.RegionId = r.RegionId
      AND r.RegionId = rzc.RegionId
      AND rzc.ZipCodeId = zc.ZipCodeId
      AND zc.ZipCode = $address->zip_postal"));

            if (!empty($rec)) {

                $new = new FairPurchasePrice;
                $new->vehicle_id = $vehicle_id;
                $new->zip_postal = $address->zip_postal;
                $new->adjustment_amount = $rec[0]->adjustment_amount;
                $new->created_at = date('Y-m-d H:i:s');
                $new->updated_at = date('Y-m-d H:i:s');
                $new->save();
                return $rec[0]->adjustment_amount;
            } else {
                return 0;
            }
        } else { // get vehicles for this blended one

            $vehicle = Vehicle::find($vehicle_id);

            $vehicles = Vehicle::where('year', $vehicle->year)->where('category_id', $vehicle->category_id)
                ->where('rank', $vehicle->rank)->where('blended_vehicle', 0)->get();
            $count = 0;
            $s = 0;

            // \Debugbar:info($vehicles);
            foreach ($vehicles as $v) {
                $adj = $this->fairPurchasePriceZip($year, $v->id, 0, $address);
                $s += $adj;
                //\Debugbar::info("ADJ: V=" . $v->id . " A=$adj S=$s ");

                $count++;
            }

            $adjustment = $count > 0 ? $s / $count : 1;
            $new = new FairPurchasePrice;
            $new->vehicle_id = $vehicle_id;
            $new->zip_postal = $address->zip_postal;
            $new->adjustment_amount = $adjustment;
            $new->created_at = date('Y-m-d H:i:s');
            $new->updated_at = date('Y-m-d H:i:s');
            $new->save();

        }
    }

    public function capitalCostTax($capital_cost, $rd)
    {
        if ($rd->geo_data->reimbursement_detail_id > 0) {
            $capital_cost_tax = $rd->geo_data->reimbursement_details->tax;
        } else {

            $taxes = $this->getReimbursementTaxes($rd->geo_data->address, $rd->vehicle_profile->vehicle->year);
            //\Debugbar::info("Taxes:");
            //\Debugbar::info($taxes);
            $capital_cost_tax = $rd->geo_data->plan == 'FAVR' ?
                $this->calculatePropertyTax($rd->vehicle_profile->vehicle, $taxes)
                :
                $this->calculateCapitalCostTax($capital_cost, $rd->vehicle_profile->retention, $rd->geo_data->address->country_id, $taxes);

        }
        $title = $rd->geo_data->plan == 'FAVR' ? 'Property Tax' : 'Capital Cost Tax';
        $tax_adj = $capital_cost_tax * (1 + $rd->vehicle_profile->capital_cost_tax_adj / 100.0);
        $details = ['title' => $title, 'label' => null,
            'amount' => $capital_cost_tax, 'adjustment' => $rd->vehicle_profile->capital_cost_tax_adj,
            'amount_adj' => $tax_adj];

        return ['amount' => $tax_adj, 'details' => $details];
    }

    private function getReimbursementTaxes($address, $year)
    {
        if ($address->country_id == USA) {
            $state = TaxesFeesUS::where('year', $year)->where('state_province_id', $address->state_province_id)
                ->select('sales_tax', 'personal_property_tax')->first();
            $county_id = ZipCode::where('zip_code', $address->zip_postal)->pluck('county_id')->first();

            if ($county_id == 3211) {   // San Juan => Ohio
                $county_id = 608;
            }

            //\Debugbar::error("TAXES: State=" . $address->state_province_id . ' ' . $address->zip_postal . ' County=' . $county_id);

            $county = CountyTax::where('year', $year)->where('county_id', $county_id)->select('county_sales_tax')->first();

            return (object)['state_sale' => $state->sales_tax, 'county_sale' => $county->county_sales_tax, 'personal_property' => $state->personal_property_tax];

            /*
            \Debugbar::error('state zip county county id');
            \Debugbar::error($address->state_province_id . ' ' . $address->zip_postal . ' ' . $county_id);
            \Debugbar::error('state taxes:');
            \Debugbar::error($state);
            \Debugbar::error('county taxes:');
            \Debugbar::error($county);
            */
        } else {
            $sales_tax = TaxesFeesCA::where('year', $year)->where('state_province_id', $address->state_province_id)
                ->pluck('sales_tax')->first();
            // \Debugbar:error('CA state taxes:');
            // \Debugbar:error($sales_tax);
            return (object)['state_sale' => $sales_tax];
        }

    }


    private function calculateCapitalCostTax($capital_cost, $retention, $country_id, $taxes)
    {

        // \Debugbar::info('calculateCapitalCostTax .... taxes=');
        // \Debugbar::info($taxes);

        $months = $retention * 12;
        if ($country_id == USA) {
            $one_time_tax = ($taxes->county_sale + $taxes->state_sale) / 100.0;
            $annual_tax = $taxes->personal_property / 100.0;

            //\Debugbar::info("One Time Tax: $one_time_tax = ( $taxes->county_sale + $taxes->state_sale )/100.0");
            //\Debugbar::info("Annual Tax: $annual_tax = $taxes->personal_property / 100.0");

            $jk = $capital_cost * ($one_time_tax / $months + $annual_tax / 12.0);
            //\Debugbar::info("TAX: $jk = $capital_cost * (  $one_time_tax / $months + $annual_tax / 12.0 )");
            return $capital_cost * ($one_time_tax / $months + $annual_tax / 12.0);
        } else {    // Canada
            // $jk = $capital_cost * $taxes->state_sale / (100 * $months);
            // \Debugbar::info("returns $jk = $capital_cost * $taxes->state_sale / ( 100 * $months )");
            return $capital_cost * $taxes->state_sale / (100 * $months);
        }
    }

    private function calculatePropertyTax($vehicle, $taxes)
    {
        return $vehicle->invoice * $taxes->personal_property / (12 * 100);
    }

    public function resaleValue($rd)
    {
        try {
            if ($rd->geo_data->reimbursement_detail_id > 0) {
                $resale_value = $rd->geo_data->reimbursement_details->resale_value;
                $resale_value_adj = $resale_value * (1 + $rd->geo_data->reimbursement_details->resale_value_adj / 100.0);
            } else {
                $geo_data = $rd->geo_data;
                $vehicle_profile = $rd->vehicle_profile;
                $retention = $vehicle_profile->retention;
                $resale_condition_id = $vehicle_profile->resale_condition_id;
                $vehicle = $vehicle_profile->vehicle;
                if ($vehicle_profile->vehicle->blended_vehicle && $vehicle_profile->vehicle->year >= 2016) {
                    $resale_value = $this->resaleValueForBlended($geo_data, $vehicle, $retention, $resale_condition_id);
                } else {
                    $resale_value = $this->resaleValueForVehicle($geo_data, $vehicle, $retention, $resale_condition_id);
                }
                $resale_value = $resale_value;
                $resale_value_adj = $resale_value * (1 + $rd->vehicle_profile->resale_value_adj / 100.0);
            }
            $details = ['title' => 'Resale Value', 'label' => null,
                'amount' => $resale_value, 'adjustment' => $rd->vehicle_profile->resale_value_adj,
                'amount_adj' => $resale_value_adj];
            return ['amount' => $resale_value_adj, 'details' => $details];
        } catch (Exception $e) {
            \Log::info("Failed in resaleValue: " . $e->getMessage());
        }
    }

    /**
     * Resale value for single vehicle or blended before 2016
     *
     * @param Address + year, month, plan, mileage_band_id, territory_type_id, territory_list: $geo_data
     * @param Vehicle $vehicle
     * @param int $retention
     * @param int $resale_condition_id
     * @return double resale_value
     */
    private function resaleValueForVehicle($geo_data, $vehicle, $retention, $resale_condition_id)
    {

        $country_id = $geo_data->address->country_id;
        $model_year = $vehicle->year;
        $mileage_band_id = $geo_data->mileage_band_id;

        // to convert US values to Canadian
        $canadian_ratio = $this->CanadianRatio($country_id, $vehicle);

        $geo_id = $this->getGeoId($model_year, $geo_data->address); // state id or region id

        // map Puerto Rico to Illinois
        if ($geo_id == 52) $geo_id = 15;

        $resale_year = $model_year - $retention;

        if ($model_year >= 2013) {

            $rec_resale = VehicleResaleValueUS::where('state_id', $geo_id)->where('vehicle_id', $vehicle->id)
                ->where('resale_year', $resale_year)->where('resale_condition_id', $resale_condition_id)
                ->where('model_year', $vehicle->year)->select('base_cost', 'equipment_adj')->first();

            $rec_wholesale = VehicleResaleValueUS::where('state_id', $geo_id)->where('vehicle_id', $vehicle->id)
                ->where('resale_year', $resale_year)->where('resale_condition_id', WHOLESALE)
                ->where('model_year', $vehicle->year)->select('base_cost')->first();
            $wholesale_cost = $rec_wholesale->base_cost;

        } else if ($country_id == USA || $model_year >= 2012) {

            $rec_resale = VehicleResaleValueUS::where('region_id', $geo_id)->where('vehicle_id', $vehicle->id)
                ->where('resale_year', $resale_year)->where('resale_condition_id', $resale_condition_id)
                ->where('model_year', $vehicle->year)->select('base_cost', 'equipment_adj')->first();

            $rec_wholesale = VehicleResaleValueUS::where('region_id', $geo_id)->where('vehicle_id', $vehicle->id)
                ->where('resale_year', $resale_year)->where('resale_condition_id', WHOLESALE)
                ->where('model_year', $vehicle->year)->select('base_cost')->first();
            $wholesale_cost = $rec_wholesale->base_cost;

        } else {

            if ($model_year > 2009) {

                $rec_resale = VehicleResaleValueCA::where('region_id', $geo_id)
                    ->where('vehicle_id', $vehicle->id)
                    ->where('resale_year', $resale_year)
                    ->where('resale_condition_id', $resale_condition_id)
                    ->where('model_year', $vehicle->year)
                    ->where('mileage_band_id', $mileage_band_id)
                    ->select('base_cost')->first();
                $rec_resale->equipment_adj = new \stdClass();;
                $rec_resale->equipment_adj = 0;

                $wholesale_cost = $rec_resale->base_cost;

            } else {    // model_year <= 2009

                $rec_resale = VehicleResaleValueCA::where('vehicle_id', $vehicle->id)
                    ->where('resale_year', $resale_year)
                    ->where('model_year', $vehicle->year)
                    ->where('mileage_band_id', $mileage_band_id)
                    ->select('base_cost')->first();
                $rec_resale->equipment_adj = new \stdClass();;
                $rec_resale->equipment_adj = 0;

                $wholesale_cost = $rec_resale->base_cost;

            }
        }

        $resale_value_base_cost = $rec_resale->base_cost;
        $resale_value_equipment_adj = $rec_resale->equipment_adj;

        // \Debugbar::info("DEB: resale_value_base_cost = $resale_value_base_cost  wholesale_cost = $wholesale_cost \n");
        // \Debugbar::info("DEB: resale_value_equipment_adj = $resale_value_equipment_adj\n");


        /////////////////////// Mileage Adjustment ///////////////////

        $mileage_adjustment = $this->VehicleMileageAdjustment($geo_data, $vehicle, $resale_value_base_cost, $wholesale_cost,
            $resale_year, $retention);

        $high_mileage_ratio = $this->HighMileageRatio($vehicle->year, $retention, $geo_data->mileage_band_id);

        $resale_value = $canadian_ratio * ($resale_value_base_cost + $resale_value_equipment_adj + $high_mileage_ratio * $mileage_adjustment);

        // \Debugbar:info("Resale calculation: $resale_value = $canadian_ratio * ( $resale_value_base_cost + $resale_value_equipment_adj + $high_mileage_ratio * $mileage_adjustment )");

        return $resale_value;

    }

    /**
     * Since 2016 we calculate resale value as it is not a pure average
     * On top of it we have to split trucks into 4x4 vs 4x2 and Diesel vs Gasoline
     *
     * @param Address + year, month, plan, mileage_band_id, territory_type_id, territory_list: $geo_data
     * @param Vehicle $vehicle
     * @param int $retention
     * @param int $resale_condition_id
     * @return float
     */
    private function resaleValueForBlended($geo_data, $vehicle, $retention, $resale_condition_id)
    {
        // fixing truck issue
        $category_id = $vehicle->category_id;
        $vehicle_name = $vehicle->name;
        $vehicle_model_year = $vehicle->year;
        $sub_filter = "";

        if ($category_id == 94) { // super duty, split by Diesel and Gasoline
            if (strpos($vehicle_name, 'Diesel')) {
                $sub_filter = " AND name LIKE '%Diesel%' ";
            } else if (strpos($vehicle_name, 'Gasoline')) {
                $sub_filter = " AND name LIKE '%Gasoline%' ";
            }
        } else if ($category_id == 93 || $category_id == 91 || $category_id == 92) {
            if (strpos($vehicle_name, '4x2')) {
                $sub_filter = " AND name LIKE '%4x2%' ";
            } else if (strpos($vehicle_name, '4x4')) {
                $sub_filter = " AND name LIKE '%4x4%' ";
            }
        }

        $vehicles = \DB::select(\DB::raw("SELECT *  FROM vehicles " .
            "WHERE `year` = $vehicle_model_year " .
            "AND category_id = $category_id " .
            "AND `rank` = $vehicle->rank " .
            "AND blended_vehicle = 0 $sub_filter ORDER BY id"));
        $s = 0;
        $i = 0;
        // \Debugbar:info("LOOP");
        foreach ($vehicles as $v) {
            $resale = $this->resaleValueForVehicle($geo_data, $v, $retention, $resale_condition_id);
            $s += $resale;
            $i++;
            // \Debugbar:info("DEB: Resale for $v->id = $resale");
        }
        if ($i > 1) $s /= $i;

        return $s;

    }

    public function updateProrateReimbursement(Request $request)
    {

        try {
            $this->validateUpdateProrateReimbursementRequest($request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        \Debugbar::info($request);

        try {
            $year = $request->year;
            $month = $request->month;
            $prorates = $request->prorate;
            $logged_user_id = $request->user_id;
            $service_plan = $request->service_plan;
            $count = 0;
            $changes = [];
            $date = date("Y-m-d H:i:s");

            \Debugbar::info('Prorates:');
            \Debugbar::info($prorates);

            foreach ($prorates as $prorate) {

                $update = [];
                $notes = isset($prorate['notes']) ? $prorate['notes'] : '';
                $user_id = $prorate['user_id'];

                if ($prorate['fix_amount'] !== null && !empty($prorate['fix_amount'])) {
                    $count++;

                    \Debugbar::info("Process Fixed Prorate:", $user_id, $year, $month, $prorate);

                    $update['fixed_reimbursement'] = $prorate['fix_amount'];

                    $changes[] = ['change_type' => 'Prorate',
                        'change_description' => 'Fixed Reimbursement',
                        'old_value' => $prorate['old_fix'],
                        'new_value' => $prorate['fix_amount'],
                        'changed_id' => $user_id,
                        'by_whom' => $logged_user_id,
                        'notes' => $notes,
                        'change_date' => $date];

                    if ($service_plan == 'FAVR' && $prorate['old_fix'] > 0) {  // update depreciation

                        $new_depreciation = round($prorate['vehicle_depreciation'] * $prorate['fix_amount'] / $prorate['old_fix'], 2);

                        // \Debugbar::info('new dept.', "{$prorate['vehicle_depreciation']} * {$prorate['fix_amount']} / {$prorate['old_fix']}");

                        $update['vehicle_depreciation'] = $new_depreciation;

                        $changes[] = ['change_type' => 'Prorate',
                            'change_description' => 'Vehicle Depreciation',
                            'old_value' => $prorate['vehicle_depreciation'], // WHY !!!!!!, because depreciation was changed and we are using it in a report
                            'new_value' => $new_depreciation,
                            'changed_id' => $user_id,
                            'by_whom' => $logged_user_id,
                            'notes' => $notes,
                            'change_date' => $date];
                    }
                }

                if ($prorate['var_amount'] !== null && !empty($prorate['var_amount'])) {
                    $count++;

                    \Debugbar::info("Process Variable Prorate:", $user_id, $year, $month, $prorate);

                    $update['variable_reimbursement'] = round(100 * $prorate['var_amount'] / $prorate['business_mileage'], 4);

                    $changes[] = ['change_type' => 'Prorate', 'change_description' => 'Variable Reimbursement',
                        'old_value' => $prorate['old_var'], 'new_value' => $prorate['var_amount'],
                        'changed_id' => $user_id, 'by_whom' => $logged_user_id, 'notes' => $notes, 'change_date' => $date];
                    $changes[] = ['change_type' => 'Prorate', 'change_description' => 'Cents per Mile',
                        'old_value' => $prorate['old_variable_reimbursement'], 'new_value' => $update['variable_reimbursement'],
                        'changed_id' => $user_id, 'by_whom' => $logged_user_id, 'notes' => $notes, 'change_date' => $date];
                }
            }

            if (count($update) > 0) {
                \Debugbar::info('update:', $update);
                \Debugbar::info('changes:', $changes);
                \DB::table('monthly_reimbursements')->where('user_id', $user_id)->where('year', $year)->where('month', $month)->update($update);
                \DB::table('tracking_changes')->insert($changes);
            }

            return ['success' => "Reimbursement was updated with $count prorates", 'failure' => null];

        } catch (\Exception $e) {
            return ['success' => null, 'failure' => "Prorate failed at line " . $e->getLine() . " :" . $e->getMessage()];
        }
    }

    /**
     * Resale value adjustment based on mileage. KBB was changing how we are getting this
     * @param Address + year, month, plan, mileage_band_id, territory_type_id, territory_list: $geo_data
     * @param Vehicle $vehicle
     * @param double $base_value
     * @param double $wholesale_value
     * @param int $resale_year
     * @param int $retention
     * @return float
     */
    private function VehicleMileageAdjustment($geo_data, $vehicle, $base_value, $wholesale_value, $resale_year, $retention)
    {
        $kbb_mileage = MileageBand::find($geo_data->mileage_band_id)->kbb_mileage;
        $total_mileage = $retention * $kbb_mileage;
        // \Debugbar::info("TOTAL MILEAGE $total_mileage = $retention * $kbb_mileage;");


        // \Debugbar:info("DEB: Total Mileage = $total_mileage, Base Value = $base_value\n");

        // KBB changed how they store mileage adjustments in 2009 and 2011
        if ($vehicle->year >= 2009) {
            $total_mileage *= 1000;
            if ($vehicle->year >= 2011) {
                $adj = VehicleMileageAdjustment2011::where('model_year', $vehicle->year)
                    ->where('resale_year', $resale_year)
                    ->where('mileage_range_min', '<=', $total_mileage)
                    ->where('mileage_range_max', '>=', $total_mileage)
                    ->where('mileage_group_id', $vehicle->mileage_group_id)
                    ->select('adjustment_type', 'adjustment')->first();
            } else {
                $adj = VehicleMileageAdjustment2009::where('model_year', $vehicle->year)
                    ->where('resale_year', $resale_year)->where('mileage_range_min', '<=', $total_mileage)
                    ->where('mileage_range_max', '>=', $total_mileage)->where('value_range_min', '<=', $wholesale_value)
                    ->where('value_range_max', '>=', $wholesale_value)->select('adjustment_type', 'adjustment')->first();
            }

            $mileage_adjustment = ($adj->adjustment_type == 'value') ?
                $adj->adjustment
                :
                $base_value * $adj->adjustment; // adjustment is stored as percentage value

            // \Debugbar::info("DEB: mileage_adjustment=$mileage_adjustment\n");

            return $mileage_adjustment;
        }
    }

    /**
     * Craziness from 2012 and 2012. John's rules for adjustment to level reimbursemewnt
     *
     * @param $year
     * @param $retention
     * @param $mileage_band_id
     * @return float
     */
    private function HighMileageRatio($year, $retention, $mileage_band_id)
    {
        $high_mileage_ratio = 1.0;
        if ($year >= 2012 && $year <= 2013) {
            if ($retention == 4 && $mileage_band_id >= 8) {
                if ($mileage_band_id == 8)
                    $high_mileage_ratio = 1.1;
                if ($mileage_band_id == 9)
                    $high_mileage_ratio = 1.2;
                if ($mileage_band_id == 10)
                    $high_mileage_ratio = 1.29;
                if ($mileage_band_id == 11)
                    $high_mileage_ratio = 1.36;
            }
            if ($retention == 5 && $mileage_band_id >= 7) {
                if ($mileage_band_id == 7)
                    $high_mileage_ratio = 1.1;
                if ($mileage_band_id == 8)
                    $high_mileage_ratio = 1.2;
                if ($mileage_band_id == 9)
                    $high_mileage_ratio = 1.29;
                if ($mileage_band_id == 10)
                    $high_mileage_ratio = 1.38;
                if ($mileage_band_id == 11)
                    $high_mileage_ratio = 1.50;
            }
            if ($retention == 6 && $mileage_band_id >= 6) {
                if ($mileage_band_id == 6)
                    $high_mileage_ratio = 1.2;
                if ($mileage_band_id == 7)
                    $high_mileage_ratio = 1.34;
                if ($mileage_band_id == 8)
                    $high_mileage_ratio = 1.48;
                if ($mileage_band_id == 9)
                    $high_mileage_ratio = 1.60;
                if ($mileage_band_id == 10)
                    $high_mileage_ratio = 1.67;
                if ($mileage_band_id == 11)
                    $high_mileage_ratio = 1.79;
            }
        }
        return $high_mileage_ratio;
    }

    private function reimbursementInsurance($rd)
    {
        if ($rd->geo_data->reimbursement_detail_id > 0) {
            $insurance = $rd->geo_data->reimbursement_details->insurance;
            $insurance_city = $rd->geo_data->reimbursement_details->insurance_city;
        } else {
            $vehicle_profile = $rd->vehicle_profile;
            $address = $rd->geo_data->address;
            $insurance_data = $this->findClosestInsuranceCity($vehicle_profile->vehicle->year, $address);
            $insurance_city = $insurance_data->insurance_city;
            $insurance = $insurance_data->monthly_cost;

            // we have insurance values for mid-size vehicles only. Other vehicle categories have to be adjusted.
            $adjustment = VehicleCategoryInsuranceAdjustment::where('year', $vehicle_profile->vehicle->year)
                ->where('vehicle_category_id', $vehicle_profile->vehicle->category_id)
                ->pluck('insurance_adjustment')->first();
            $insurance = $insurance_data->monthly_cost * (1 + $adjustment / 100.0);
        }

        $insurance_adj = $insurance * (1 + $rd->vehicle_profile->insurance_adj / 100.0);
        //\Debugbar::info("Insurance from $insurance_city $insurance = $insurance_data->monthly_cost * (1 + $adjustment / 100.0);");
        $details = ['title' => 'Insurance', 'label' => null, 'amount' => $insurance, 'insurance_city' => $insurance_city,
            'adjustment' => $rd->vehicle_profile->insurance_adj, 'amount_adj' => $insurance_adj];

        return ['amount' => $insurance_adj, 'details' => $details];
    }

    private function reimbursementFees($rd)
    {

        $address = $rd->geo_data->address;

        if ($rd->geo_data->reimbursement_detail_id > 0) {
            $fee_renewal = $rd->geo_data->reimbursement_details->fee_renewal;
        } else {

            $vehicle_profile = $rd->vehicle_profile;

            // PuertoRico case
            $state_province_id = ($address->state_province_id == 52) ? 15 : $address->state_province_id;

            if ($address->country_id == USA) {

                $county_id = ZipCode::find($address->zip_postal)->county_id;

                // PuertoRico case
                if ($address->state_province_id == 52) $county_id = 608;

                $fee_data = CountyTax::where('year', $vehicle_profile->vehicle->year)->where('county_id', $county_id)->first();
                if ($vehicle_profile->vehicle->year >= 2018) {
                    $fee_renewal =
                        ($fee_data->title_fee
                            + $fee_data->registration_fee
                            + $fee_data->emission_fee) * $vehicle_profile->retention;
                } elseif ($vehicle_profile->vehicle->year >= 2009) {
                    $fee_renewal =
                        $fee_data->title_fee
                        + $fee_data->registration_fee
                        + $vehicle_profile->retention * ($fee_data->license_fee + $fee_data->other_fees);
                    //     \Debugbar::info("FEES $fee_renewal = $fee_data->title_fee + $fee_data->registration_fee + $vehicle_profile->retention * ($fee_data->license_fee + $fee_data->other_fees);");
                } else {
                    $fee_renewal =
                        $fee_data->license_fee
                        + $fee_data->title_fee
                        + $fee_data->registration_fee + ($vehicle_profile->retention - 1) * $fee_data->renewal;
                }
            } else {

                $fee_data = TaxesFeesCA::where('year', $vehicle_profile->vehicle->year)->where('state_province_id', $state_province_id)->first();

                // rules for Toronto and northern Ontario
                if ($state_province_id == 109) {
                    if (substr($address->zip_postal, 0, 1) == 'M' && $vehicle_profile->vehicle->year < 2017) {
                        $fee_data->annual_renewal += 50;
                    } elseif ($address->latitude >= 44.42) {
                        $fee_data->annual_renewal -= 37;
                    }
                }
                $fee_renewal = $fee_data->initial_registration + $fee_data->annual_renewal * $vehicle_profile->retention +
                    $fee_data->annual_safety * ($vehicle_profile->retention - 1);
                // \Debugbar::info("$fee_renewal = $fee_data->initial_registration + $fee_data->annual_renewal * $vehicle_profile->retention +
                //    $fee_data->annual_safety * ($vehicle_profile->retention - 1);");

            }
            $fee_renewal = $fee_renewal / (12 * $vehicle_profile->retention);

        }
        $fee_renewal_adj = $fee_renewal * (1 + $rd->vehicle_profile->fee_renewal_adj / 100.0);
        $details = ['title' => 'Fee/Renewal', 'label' => null, 'amount' => $fee_renewal,
            'adjustment' => $rd->vehicle_profile->insurance_adj, 'amount_adj' => round($fee_renewal_adj, 2)];

        return ['amount' => $fee_renewal_adj, 'details' => $details];

    }

    public function findClosestFuelCity($address)
    {

        $radius = $address->country_id == 1 ? 3963.0 : 6378.7;
        $fuel_city = 'unknown?';

        // fix for Puerto Rico
        //if ($address->state_province_id == 52) {
        //    $state_province_id = 15;
        //    $zip_postal = 60601;
        //} else {
        $state_province_id = $address->state_province_id;
        $zip_postal = str_replace(' ', '', trim($address->zip_postal));
        //}

        // check if we have lat/lng in address record
        if (!empty($address->latitude) && !empty($address->longitude)) {
            $lat1 = $address->latitude;
            $lng1 = $address->longitude;
        } elseif ($address->country_id == 1) {
            $latlng = ZipCode::where('zip_code', $zip_postal)->select('latitude', 'longitude')->first();
            $lat1 = $latlng->latitude;
            $lng1 = $latlng->longitude;
        } else {
            $latlng = PostalCode::where('postal_code', $zip_postal)->select('latitude', 'longitude')->first();
            $lat1 = $latlng->latitude;
            $lng1 = $latlng->longitude;
        }

        $state_province_arr = [$state_province_id];
        if ($address->country_id == CANADA) {
            // we do not have fuel in PEI, Yukon, NWT and Nunavut, so we have to take it from closest provinces
            if ($state_province_id == 110)
                $state_province_arr = [104, 107, 110]; // PEI -> Nova Scotia + New Brunswick
            else if ($state_province_id == 106)
                $state_province_arr = [106, 101];   // Northwest Territories -> Alberta
            else if ($state_province_id == 113)
                $state_province_arr = [102, 113];   // Yukon Territory -> British Columbia
            else if ($state_province_id == 108)
                $state_province_arr = [103, 108];   // Nunavut -> Manitoba
        }

        $records = FuelCity::where('country_id', $address->country_id)
            ->whereIn('state_province_id', $state_province_arr)
            ->select('latitude', 'longitude', 'name', 'id')->get();
        $min_distance = 999999;

        foreach ($records as $r) {
            $lat2 = $r->latitude;
            $lng2 = $r->longitude;
            $distance = $radius * acos(sin($lat1 / 57.2958) *
                    sin($lat2 / 57.2958) + cos($lat1 / 57.2958) *
                    cos($lat2 / 57.2958) * cos($lng2 / 57.2958 - $lng1 / 57.2958));
            if ($distance < $min_distance) {
                $min_distance = $distance;
                $fuel_city = $r->name;
                $city_id = $r->id;
                // \Debugbar::info("Found closer city $fuel_city d=$distance ");
            }
        }

        return (object)['id' => $city_id, 'city_name' => $fuel_city];

    }

    private function findClosestInsuranceCity($year, $address)
    {

        $radius = $address->country_id == 1 ? 3963.0 : 6378.7;

        // fix for Puerto Rico
        if ($address->state_province_id == 52) {
            $state_province_id = 15;
            $zip_postal = 60601;
        } else {
            $state_province_id = $address->state_province_id;
            $zip_postal = str_replace(' ', '', trim($address->zip_postal)); // remoe middle stase from postal_code
        }

        // we do not have insurance in NWT so change province to Alberta
        if ($address->country_id == CANADA && $state_province_id == 106) {
            $state_province_id = 101;
        }


        // check if we have lat/lng in address record WRONG !!!!! In production we are taking it from Postal Code table

        //if ($address->state_province_id != 52 && !empty($address->latitude) && !empty($address->longitude)) {
        //    $lat1 = $address->latitude;
        //    $lng1 = $address->longitude;
        //} elseif ($address->country_id == USA) {
        if ($address->country_id == USA) {
            $latlng = ZipCode::where('zip_code', $zip_postal)->select('latitude', 'longitude')->first();
            $lat1 = $latlng->latitude;
            $lng1 = $latlng->longitude;
        } else {
            $latlng = PostalCode::where('postal_code', $zip_postal)->select('latitude', 'longitude')->first();
            $lat1 = $latlng->latitude;
            $lng1 = $latlng->longitude;
        }

        $records = InsuranceRate::where('country_id', $address->country_id)
            ->where('state_province_id', $state_province_id)
            ->where('year', $year)
            ->select('latitude', 'longitude', 'city_name', 'monthly_cost')->get();
        $min_distance = 999999;

        //\Debugbar::info("FIND lat/lng for $address->zip_postal / $state_province_id: $lat1 / $lng1");

        $insurance_city = '';
        $monthly_cost = 0;
        foreach ($records as $r) {
            $lat2 = $r->latitude;
            $lng2 = $r->longitude;
            $distance = $radius * acos(sin($lat1 / 57.2958) *
                    sin($lat2 / 57.2958) + cos($lat1 / 57.2958) *
                    cos($lat2 / 57.2958) * cos($lng2 / 57.2958 - $lng1 / 57.2958));
            if ($distance < $min_distance) {
                $min_distance = $distance;
                $monthly_cost = $r->monthly_cost;
                $insurance_city = $r->city_name;
                //\Debugbar::info("Found closer city $insurance_city d=$distance rate=$monthly_cost distance=$distance");
            }
        }


        return (object)['insurance_city' => $insurance_city, 'monthly_cost' => $monthly_cost];
    }

    public function getVehiclesFromYearAndPlan($year, $plan)
    {
        if ($plan == 'FAVR' || $plan == 2) {
            $vehicles = Vehicle::where('year', $year)->where('favr', 1)->orderBy('display_order')
                ->orderBy('category_id')->orderBy('name')->get();
        } else {
            $vehicles = Vehicle::where('year', $year)->orderBy('display_order')->orderBy('category_id')
                ->orderBy('name')->get();
        }

        $categories = VehicleCategory::select('id', 'name')->get();
        $category_arr = [];
        foreach ($categories as $category) {
            $category_arr[$category->id] = $category->name;
        }

        $vehicle_arr = [];
        $last_category_id = -1;

        foreach ($vehicles as $vehicle) {
            if ($last_category_id != $vehicle->category_id) {
                $vehicle_arr[] = (object)['id' => 0, 'name' => '----  ' . strtoupper($category_arr[$vehicle->category_id]) . ' ----'];
                $last_category_id = $vehicle->category_id;
            }
            $vehicle_arr[] = $vehicle;
        }


        return ['vehicles' => $vehicle_arr];
    }

    private function CanadianRatio($country_id, $vehicle)
    {
        if ($country_id == 2 && $vehicle->year >= 2012) {
            if ($vehicle->msrp > 0) {
                return $vehicle->msrp_ca / $vehicle->msrp;
            } elseif ($vehicle->invoice > 0) {
                return $vehicle->invoice_ca / $vehicle->invoice;
            } else {
                return 0; // why?
            }
        } else {
            return 1;
        }
    }

    /**
     *
     *
     *
     * @param $year
     * @param $address
     * @return int
     */
    // Mileage adjustment after 2012 are based on states. Before on regions.
    // For Canada we had bug all this time, assigning state id to province id :-(
    // Instead of looking into AP_Province.state_id. It was corrected for 2018

    private function getGeoId($year, $address)
    {
        if ($year >= 2013) {
            if ($address->country_id == 2) {
                if ($year > 2017) {
                    return StateProvince::find($address->state_province_id)->use_state_id;
                } else {
                    return $address->state_province_id - 100; // we never caught this error, until 2018
                }
            } else return $address->state_province_id;
        } else {
            return StateProvince::find($address->state_province_id)->region_id;
        }
    }

    private function getGeoName($year, $state_province_id)
    {
        // \Debugbar:info("DEB: Get geoName for $year and $state_province_id");
        if ($year >= 2013) {
            // \Debugbar:info("DEB: Return Province");
            return StateProvince::find($state_province_id)->name;
        } else {
            // \Debugbar:info("DEB: Return Region");
            return \DB::table('regions as r')
                ->join('state_provinces as s', 's.region_id', '=', 'r.id')
                ->where('s.id', $state_province_id)->pluck('r.name')->first();
        }
    }

    private function getOlderCanadianResaleValues($address, $vehicle)
    {

        $model_year = $vehicle->year;

        // blended vehicles were not stored in database before 2014
        $vehicle_list = $vehicle->blended_vehicle && $model_year < 2014 ?
            $this->getBlendedVehicles($model_year, $vehicle->category_id)
            :
            $vehicle->id;

        // TODO are we going to support Canadian 2010 2009 and older models? Ad for this we need convert AP_KBB_Vehicle_CA


    }

    private function getBlendedVehicles($year, $category_id)
    {
        $vehicles_arr = Vehicle::where('year', $year)->where('category_id', $category_id)->where('favr', 0)->pluck('id');
        return implode(',', $vehicles_arr);
    }


    private function TaxLabel($geo_data, $vehicle_profile)
    {
    }

    private function getTaxData($year, $address)
    {
        $arr = [];
        $arr['state_tax'] = TaxesFeesUS::where('year', $year)
            ->where('state_province_id', $address->state_province_id)->pluck('sales_tax')->first();
        $county_id = ZipCode::where('zip_code', $address->zip_postal)->pluck('county_id')->first();
        $arr['county_tax'] = CountyTax::where('county_id', $county_id)->where('year', $year)
            ->pluck('county_sales_tax')->first();
        $arr['county_name'] = County::find($county_id)->name;

        if (empty($arr['state_tax']) || empty($arr['county_tax'])) {
            //\Debugbar::info("Could not get tax values for year $year and address:");
            //\Debugbar::info($address);
            //\Debugbar::info($arr);
        }

        return $arr;
    }


    public function getMyFixedReimbursementData($month = null, $year = null) // davery - added 7-26-18
    {

        //TODO: CHANGE THIS TO GET DATA FROM REIMBURSEMENT DETAILS, IF NULL, CALCULATE AND INSERT IT

        $today = Carbon::today();
        $month = $month ? $month : $today->month; // davery - added 7-26-18
        $year = $year ? $year : $today->year; // davery - added 7-26-18

        // get the impersonated user, or the current user
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();

        if (!$user->hasRole(1)) {
            return response()->json(['errors' => ['You do not have permission to access this data']], 401);
        }

        \Log::info('Reimbursement Data For ' . $user->username);
        \Log::info(json_encode([
            'year' => $year,
            'month' => $month,
            'service_plan' => $user->driverProfile->company->options->service_plan,
            'driver_profile_id' => $user->driverProfile->id
        ]));

        $reimbursementData = $this->getDataForReimbursementFromDriver($year, $month, $user->driverProfile->company->options->service_plan, $user->driverProfile->id);

        $fixedReimbursementData = $this->calculateFixedReimbursementProxy($reimbursementData);
        $fixedReimbursementData['fixedReimbursementDetails']['business_use_percent'] = $user->driverProfile->vehicleProfile->business_use_percent;

        return [
            'fixedReimbursementData' => $fixedReimbursementData
        ];
    }


    public function getMyVariableReimbursementData($month = null, $year = null)  // davery - added 7-26-18
    {
        $today = Carbon::today();
        $month = $month ? $month : $today->month; // davery - added 7-26-18
        $year = $year ? $year : $today->year; // davery - added 7-26-18

        // get the impersonated user, or the current user
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();

        if (!$user->hasRole(1)) {
            return response()->json(['errors' => ['You do not have permission to access this data']], 401);
        }

        $reimbursementData = $this->getDataForReimbursementFromDriver($year, $month, $user->driverProfile->company->options->service_plan, $user->driverProfile->id);
        $variableReimbursementData = $this->calculateVariableReimbursementProxy($reimbursementData);
        $variableReimbursementData['variableReimbursementDetails']['business_use_percent'] = $user->driverProfile->vehicleProfile->business_use_percent;

        return [
            'variableReimbursementData' => $variableReimbursementData
        ];
    }

    /**
     * Parameters are passed by REFERENCE. RETURN VOID
     *
     * @param $fixed_reimb
     * @param $variable_reimb
     * @param $rd
     */
    public function getReimbursementLabels($fixed_reimb, $variable_reimb, $rd)
    {
        $fixed_details = $fixed_reimb['fixedReimbursementDetails'];
        $cents_per_mile = $variable_reimb['variableReimbursementDetails'];

        if ($rd->vehicle_profile->calculate_fixed) {
            foreach ($fixed_details as $key => $value) {
                if ($key == 'insurance') {
                    $extra_data = $fixed_details['insurance']['insurance_city'];
                } else {
                    $extra_data = null;
                }
                $fixed_details[$key]['label'] = $this->ReimbursementLabel($key, $rd, $extra_data);
            }
            // \Debugbar::info($fixed_details);
        }

        if ($rd->vehicle_profile->calculate_variable) {
            foreach ($cents_per_mile as $key => $value) {
                if ($key == 'fuelAverage') {
                    $extra_data = (object)['geo_list' => $cents_per_mile['fuelAverage']['geo_list'],
                        'count' => $cents_per_mile['fuelAverage']['count']];
                } elseif ($key == 'maintenance' || $key == 'repair') {
                    $extra_data = $cents_per_mile[$key]['state_province_name'];
                } else {
                    $extra_data = null;
                }
                $cents_per_mile[$key]['label'] = $this->ReimbursementLabel($key, $rd, $extra_data);
            }
            // \Debugbar::info($cents_per_mile);
        }
    }


    /**
     * The following function will take the result of calculateFixedReimbursementProxy and fill in any missing values.
     * Currently if some values are 0, rather than including the object with an amount set to 0, they are omitted.
     * This means on the front end we may expect fuelPrice for example, but it will not be present.
     * This function fixes that problem.
     * It will also convert scalar values to snake case where necessary.
     * Tax is an example of this.
     *
     * $data is passed by reference, and is an object of the following format:
     *  [
     *      'fixedReimbursement' => scalar value of reimbursement,
     *      'fixedReimbursementDetails => dictionary keyed by category (ex. depreciation, capital cost, etc...)
     *  ]
     *
     * $data is the output format of the calculateFixedReimbursementProxy function
     *
     */
    public function formatFixedReimbursement(&$data)
    {
        // convert scalars to snake case
        if (isset($data['fixedReimbursement'])) {
            $data['fixed_reimbursememnt'] = $data['fixedReimbursement'];
            unset($data['fixedReimbursement']);
        }

        // set up all categories
        $categories = [
            ['key' => 'depreciation', 'title' => 'Depreciation'],
            ['key' => 'tax', 'title' => 'Depreciation'],
            ['key' => 'financeCost', 'title' => 'Depreciation'],
            ['key' => 'insurance', 'title' => 'Depreciation'],
            ['key' => 'feeRenewal', 'title' => 'Depreciation'],
        ];

        // fill in missing values
        foreach ($categories as $category) {
            if (!isset($data['fixedReimbursementDetails'][$category['key']])) {
                $data['fixedReimbursementDetails'][$category['key']] = [
                    'amount_adj' => 0,
                    'amount' => 0,
                    'adjustment' => 0,
                    'title' => $category['title'],
                    'label' => $category['label']
                ];
            }
        }


    }

    /**
     * The following function will take the result of calculateVariableReimbursementProxy and fill in any missing values.
     * Currently if some values are 0, rather than including the object with an amount set to 0, they are omitted.
     * This means on the front end we may expect fuelPrice for example, but it will not be present.
     * This function fixes that problem.
     * It will also convert scalar values to snake case where necessary.
     *
     * $data is passed by reference, and is an object of the following format:
     *  [
     *      'variableReimbursement' => scalar value of reimbursement,
     *      'variableReimbursementDetails => dictionary keyed by category (ex. depreciation, capital cost, etc...)
     *  ]
     *
     * $data is the output format of the calculateVariableReimbursementProxy function
     *
     */
    public function formatVariableReimbursement(&$data)
    {
        if (isset($data['variableReimbursement'])) {
            $data['variable_reimbursememnt'] = $data['variableReimbursement'];
            unset($data['variableReimbursement']);
        }

    }


    /** ACH & EFT FUNCTIONS ----------------------------------------------------------------------------------------- */
    public function wipeFile()
    {
        Storage::disk('upload_docs')->put('test.txt', "");
    }

    /**
     * Generates an ACH file TODO: temp file with cleanup
     *
     * Request must contain
     *  drivers array [
     *      driver {
     *          payment: (double),
     *          id: (int) note this is also the user_id
     *      }
     *      ...
     *  ],
     *
     *  paymentProperties: {
     *      effective_entry_date: (YYYY-MM-DD)
     *      file_id_modifier: (str)
     *      type: (str)
     *  },
     *
     *  company_id: (int)
     *
     *
     * @param Request $request
     * @return array
     */
    public function generateAch(Request $request)
    {

        $user = \Auth::user();
        if (!$user->superProfile->is_payment_processor) {
            abort(401, 'You do not have permission to view this page');
        }

        $allData = $request->all();

        try {
            $this->validateAch($allData);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        $validator = Validator::make($allData, [
            'drivers.*.id' => 'distinct',
            'drivers.*.payment' => 'required|numeric',
            'paymentProperties.effective_entry_date' => 'required',
            'paymentProperties.file_id_modifier' => 'required',
            'paymentProperties.type' => 'required',
        ]);

        $validator->validate();

        $data = json_decode(json_encode($allData));


        try {

            // get company and country info
            $company = Company::find($data->company_id);
            $country_id = $company->profile->country_id;

            // get the bank's information
            $bank = $company->bank(false);

            if (!$bank || $bank->status != 1) {
                abort(410, 'Bank could not be found');
            }

            // pad the banking information, simply store originating_dfi
            $padded_immediate_destination = str_pad($bank->immediate_destination, 10, ' ', STR_PAD_LEFT);  //  b021313103
            $padded_immediate_origin = str_pad($bank->immediate_origin, 10, ' ', STR_PAD_LEFT);            //  980518853
            $padded_immediate_origin_name = str_pad($bank->immediate_origin_name, 23, ' ', STR_PAD_RIGHT);
            $padded_company_id = str_pad($bank->company_id, 10, '0', STR_PAD_LEFT);                        //  0980518853
            $padded_company_name = str_pad($bank->company_name, 23, ' ', STR_PAD_RIGHT);
            $padded_shorter_company_name = str_pad(substr($padded_company_name, 0, 16), 16, ' ', STR_PAD_RIGHT);
            $originating_dfi = $bank->originating_dfi;

            // variables specific to payment
            $total_reimb = 0;  // used for the total credit entry dollar amount in file
            $nonzero_drivers = 0; // used for the entry/addenda count

            // initialize ACH structure variables
            $block_count = 0;
            $entry_hash = 0;
            $extra_blocks = '';
            $driver_ach_entries = '';
            $driver_ach_entries_prenote = '';


            // loop through the drivers
            foreach ($data->drivers as $driver) {

                // gather driver information
                $user = \App\User::find($driver->id);

                $bankingInformation = $user->banking($company->country_id, false); // masked =

                $temp_reimb = number_format((double)$driver->payment, 2, '.', '');

                // only do the work for drivers who are receiving non-zero payment
                if ($temp_reimb > 0) {
                    $total_reimb += $temp_reimb;
                    $nonzero_drivers += 1;

                    // if US we add routing number to entry hash, else we add branch (transit) //TODO: this was in old code but is unnecessary
                    if ($country_id == 1) {
                        $entry_hash += substr($bankingInformation->routing_number, 0, 8);
                        $bankingInformation->routing_transit = $bankingInformation->routing_number;
                    } else {
                        $entry_hash += substr($bankingInformation->transit_number, 0, 8);
                        $bankingInformation->routing_transit = $bankingInformation->branch_number;
                    }

                    // Format to $$$$$$$$cc with left padded zeroes, two decimal places, and no seperators.
                    $formatted_variable = str_pad(number_format($temp_reimb, 2, "", ""), 10, "0", STR_PAD_LEFT);

                    // Format to last_name,first_name to 22 spaces, right padded with spaces, truncates long names.
                    $formatted_name = str_pad(substr($user->last_name . "," . $user->first_name, 0, 22), 22, " ", STR_PAD_RIGHT);
                    $formatted_number = str_pad($nonzero_drivers, 7, "0", STR_PAD_LEFT);
                    $formatted_account_number = str_pad(substr($bankingInformation->account_number, 0, 17), 17, " ", STR_PAD_RIGHT);
                    $formatted_username = str_pad(substr($user->username, 0, 15), 15, " ", STR_PAD_RIGHT);


                    $achEntries = $this->getAchEntries(
                        $bankingInformation->account_type,
                        $bankingInformation->routing_transit,
                        $formatted_account_number,
                        $formatted_variable,
                        $formatted_username,
                        $formatted_name,
                        $formatted_number,
                        $originating_dfi,
                        $driver_ach_entries,
                        $driver_ach_entries_prenote
                    );

                    $driver_ach_entries = $achEntries->driver_ach_entries;
                    $driver_ach_entries_prenote = $achEntries->driver_ach_entries_prenote;

                } // end if temp_reimb > 0

            } // end foreach driver

            $block_count = 4 + $nonzero_drivers;
            if ($block_count % 10 > 0) {
                // 10 - [1 , 9] = [1, 9]
                $blocks_needed = 10 - ($block_count % 10);
                $block_count += 10 - ($block_count % 10);
                for ($x = 1; $x <= $blocks_needed; $x++) {
                    $extra_blocks .= '9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999' . "\r\n";
                }
            }
            $block_count = $block_count / 10;

            $header = $this->getAchHeader($padded_immediate_destination, $padded_immediate_origin, $padded_immediate_origin_name, $padded_company_name);

            $batchHeader = $this->getAchBatchHeader($padded_shorter_company_name, $padded_company_id, $originating_dfi);

            $fileControl = $this->getAchFileControl($block_count, $nonzero_drivers, $entry_hash, $total_reimb);

            $batchControl = $this->getAchBatchControl($padded_company_id, $originating_dfi, $nonzero_drivers, $entry_hash, $total_reimb);


            // get date information and pad
            $effective_entry_date = Carbon::parse($data->paymentProperties->effective_entry_date);

            $year_ach = $effective_entry_date->format('y');
            $padded_year = str_pad($year_ach, 2, "0", STR_PAD_LEFT);

            $month_ach = $effective_entry_date->format('m');
            $padded_month = str_pad($month_ach, 2, "0", STR_PAD_LEFT);

            $day_ach = $effective_entry_date->format('d');
            $padded_day = str_pad($day_ach, 2, "0", STR_PAD_LEFT);

            $month_short_name = strtolower(date("M"));

            $filename = "ach_" . $month_short_name . "_" . $year_ach . "_" . date('d') . "_" . ".txt";


            if ($data->paymentProperties->type == 'prenote') {
                $ach_file = $header->file_header_record_start
                    . $data->paymentProperties->file_id_modifier
                    . $header->file_header_record_end
                    . $batchHeader->batch_header_record_start
                    . $padded_year
                    . $padded_month
                    . $padded_day
                    . $batchHeader->batch_header_record_end
                    . $driver_ach_entries_prenote
                    . $batchControl->batch_control_record_prenote
                    . $fileControl->file_control_record_prenote
                    . $extra_blocks;
            } else {
                $ach_file = $header->file_header_record_start
                    . $data->paymentProperties->file_id_modifier
                    . $header->file_header_record_end
                    . $batchHeader->batch_header_record_start
                    . $padded_year
                    . $padded_month
                    . $padded_day
                    . $batchHeader->batch_header_record_end
                    . $driver_ach_entries
                    . $batchControl->batch_control_record
                    . $fileControl->file_control_record
                    . $extra_blocks;
            }

            $filename = substr(md5(rand()), 0, 7) . '.txt';

            Storage::disk('upload_tmp')->put($filename, $ach_file);

            return response()->download(public_path('/storage/tmp/' . $filename));

//            return ['filename' => $filename];

        } catch (\Exception $e) {
            return response($e->getTraceAsString(), 500);
        }
    }

    /**
     * ACH RECORD 1
     *
     * @param $padded_immediate_destination
     * @param $padded_immediate_origin
     * @param $padded_immediate_origin_name
     * @param $padded_company_name
     * @return \stdClass
     */
    function getAchHeader($padded_immediate_destination, $padded_immediate_origin, $padded_immediate_origin_name, $padded_company_name)
    {

        $file_header_record_start = '1' . '01' . "$padded_immediate_destination" . "$padded_immediate_origin" . date("ymd") . date("Hi");

        /**
         * NOTE!
         * The padded immediate origin name is the bank's padded immediate origin name. Really this is the
         * immediate destination name in the structure of the file. This is because CarData sends
         * moneys to the bank (the destination) identified by the bank's immediate origin name. The bank
         * then distributes payments at a later time. The company name here is CarData, being the immediate
         * origin name. Confusing stuff!
         */
        $file_header_record_end = '094' . '10' . '1' . "$padded_immediate_origin_name" . "$padded_company_name" . '        ' . "\r\n";

        $header = new \stdClass();
        $header->file_header_record_start = "$file_header_record_start";
        $header->file_header_record_end = "$file_header_record_end";

        return $header;
    }

    /**
     * ACH RECORD 5
     *
     * @param $padded_shorter_company_name
     * @param $padded_company_id
     * @param $originating_dfi
     * @return \stdClass
     */
    function getAchBatchHeader($padded_shorter_company_name, $padded_company_id, $originating_dfi)
    {

        $batch_header_record_start = '5' . '220' . $padded_shorter_company_name . '                    ' . $padded_company_id . 'PPD' . 'FV REIMB  ' . '      ';
        $batch_header_record_end = '   ' . '1' . $originating_dfi . '0000001' . "\r\n";

        $batchHeader = new \stdClass();
        $batchHeader->batch_header_record_start = "$batch_header_record_start";
        $batchHeader->batch_header_record_end = "$batch_header_record_end";

        return $batchHeader;
    }

    /**
     * ACH RECORD 6
     *
     * @param $account_type
     * @param $routing_transit
     * @param $formatted_account_number
     * @param $formatted_variable
     * @param $formatted_username
     * @param $formatted_name
     * @param $formatted_number
     * @param $originating_dfi
     * @param $driver_ach_entries
     * @param $driver_ach_entries_prenote
     * @return \stdClass
     */
    public function getAchEntries($account_type, $routing_transit, $formatted_account_number, $formatted_variable, $formatted_username, $formatted_name, $formatted_number, $originating_dfi, $driver_ach_entries, $driver_ach_entries_prenote)
    {

        if ($account_type == "checking") {
            $ppd_entry = '6' . '22' . $routing_transit . $formatted_account_number . $formatted_variable . $formatted_username . $formatted_name . '  ' . '0' . $originating_dfi . $formatted_number;
            $ppd_entry_prenote = '6' . '23' . $routing_transit . $formatted_account_number . '0000000000' . $formatted_username . $formatted_name . '  ' . '0' . $originating_dfi . $formatted_number;
        } else {
            $ppd_entry = '6' . '32' . $routing_transit . $formatted_account_number . $formatted_variable . $formatted_username . $formatted_name . '  ' . '0' . $originating_dfi . $formatted_number;
            $ppd_entry_prenote = '6' . '33' . $routing_transit . $formatted_account_number . '0000000000' . $formatted_username . $formatted_name . '  ' . '0' . $originating_dfi . $formatted_number;
        }
        $driver_ach_entries .= $ppd_entry . "\r\n";
        $driver_ach_entries_prenote .= $ppd_entry_prenote . "\r\n";

        $achEntries = new \stdClass();
        $achEntries->driver_ach_entries = "$driver_ach_entries";
        $achEntries->driver_ach_entries_prenote = "$driver_ach_entries_prenote";

        return $achEntries;
    }


    /**
     * ACH RECORD 8
     *
     * @param $padded_company_id
     * @param $originating_dfi
     * @param $nonzero_drivers
     * @param $entry_hash
     * @param $total_reimb
     * @return \stdClass
     */
    function getAchBatchControl($padded_company_id, $originating_dfi, $nonzero_drivers, $entry_hash, $total_reimb)
    {
        $batch_control_record = '8' . '220' . str_pad($nonzero_drivers, 6, "0", STR_PAD_LEFT) . str_pad(substr($entry_hash, 0, 10), 10, "0", STR_PAD_LEFT) . '000000000000' . str_pad(number_format($total_reimb, 2, "", ""), 12, "0", STR_PAD_LEFT) . $padded_company_id . '                   ' . '      ' . $originating_dfi . '0000001' . "\r\n";
        $batch_control_record_prenote = '8' . '220' . str_pad($nonzero_drivers, 6, "0", STR_PAD_LEFT) . str_pad(substr($entry_hash, 0, 10), 10, "0", STR_PAD_LEFT) . '000000000000' . '000000000000' . $originating_dfi . '                   ' . '      ' . $originating_dfi . '0000001' . "\r\n";

        $batchControl = new \stdClass();
        $batchControl->batch_control_record = "$batch_control_record";
        $batchControl->batch_control_record_prenote = "$batch_control_record_prenote";

        return $batchControl;
    }

    /**
     * ACH RECORD 9
     *
     * @param $block_count
     * @param $nonzero_drivers
     * @param $entry_hash
     * @param $total_reimb
     * @return \stdClass
     */
    function getAchFileControl($block_count, $nonzero_drivers, $entry_hash, $total_reimb)
    {

        $file_control_record = '9' . '000001' . str_pad($block_count, 6, "0", STR_PAD_LEFT) . str_pad($nonzero_drivers, 8, "0", STR_PAD_LEFT) . str_pad(substr($entry_hash, 0, 10), 10, "0", STR_PAD_LEFT) . '000000000000' . str_pad(number_format($total_reimb, 2, "", ""), 12, "0", STR_PAD_LEFT) . '                                       ' . "\r\n";
        $file_control_record_prenote = '9' . '000001' . str_pad($block_count, 6, "0", STR_PAD_LEFT) . str_pad($nonzero_drivers, 8, "0", STR_PAD_LEFT) . str_pad(substr($entry_hash, 0, 10), 10, "0", STR_PAD_LEFT) . '000000000000' . '000000000000' . '                                       ' . "\r\n";

        $fileControl = new \stdClass();
        $fileControl->file_control_record = "$file_control_record";
        $fileControl->file_control_record_prenote = "$file_control_record_prenote";

        return $fileControl;
    }


    /**
     * Generates an EFT file TODO: temp file with cleanup
     *
     * Request must contain
     *  drivers array [
     *      driver {
     *          payment: (double),
     *          id: (int) note this is also the user_id
     *      }
     *      ...
     *  ],
     *
     *  paymentProperties: {
     *      effective_entry_date: (YYYY-MM-DD)
     *      file_creation_number: (str) or (int)
     *  },
     *
     *  company_id: (int)
     *
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function generateEft(Request $request)
    {
        $user = \Auth::user();
        if (!$user->superProfile->is_payment_processor) {
            abort(401, 'You do not have permission to view this page');
        }

        $allData = $request->all();

        try {
            $this->validateEft($allData);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        $errorMessages = [
            'drivers.*.id.distinct' => 'Duplicate driver!',
            'drivers.*.first_name.required' => 'First name is required',
            'drivers.*.last_name.required' => 'Last name is required',
            'drivers.*.username.required' => 'Username is required',
            'drivers.*.payment.min' => 'Invalid amount',
            'paymentProperties.effective_entry_date.required' => 'The effective entry date is required',
            'paymentProperties.file_creation_number.required' => 'The file creation number is required',

        ];

        $validationRules = [
            'drivers.*.id' => 'distinct',
            'drivers.*.first_name' => 'required',
            'drivers.*.last_name' => 'required',
            'drivers.*.username' => 'required',
            'drivers.*.payment' => 'required|numeric|min:1',
            'paymentProperties.effective_entry_date' => 'required',
            'paymentProperties.file_creation_number' => 'required'
        ];

        $validator = Validator::make($allData, $validationRules, $errorMessages);

        $validator->validate();

        $data = json_decode(json_encode($allData));

        // get company and country info
        $company = Company::find($data->company_id);

        $country_id = $company->profile->country_id;

        // get the bank's information
        $bank = $company->bank(false);

        if (!$bank || $bank->status != 1) {
            abort(410, 'Bank could not be found');
        }

        $eft_header = $this->getEftHeader($bank, $data->paymentProperties);

        // number_of_payments and total_value_of_payments are passed by reference
        $number_of_payments = 0;
        $total_value_of_payments = 0;
        $eft_detail = $this->getEftDetail($data->drivers, $data->paymentProperties, $number_of_payments, $total_value_of_payments);

        $eft_trailer = $this->getEftTrailer($number_of_payments, $total_value_of_payments);

        $eft_file = $eft_header . $eft_detail . $eft_trailer;

        $filename = substr(md5(rand()), 0, 7) . '.txt';

        Storage::disk('upload_tmp')->put($filename, $eft_file);

        return response()->download(public_path('/storage/tmp/' . $filename));


//        return ['filename' => $filename];
    }

    /**
     * Returns EFT 80-byte header as a properly formatted string
     *  note the newline is already appended.
     *
     * @param $bank
     *  an object containing the originator_id, originator_name, transit_number, and account_number
     *
     * @param $paymentProperties
     *  object containing effective_entry_date and file_creation_number
     *
     * @return string
     */
    public function getEftHeader($bank, $paymentProperties)
    {

        // trim the file creation number just in case
        $file_creation_number = trim($paymentProperties->file_creation_number);

        // pad the dates
        $effective_entry_date = Carbon::parse($paymentProperties->effective_entry_date);

        $year = $effective_entry_date->format('y');
        $padded_year = str_pad($year, 2, "0", STR_PAD_LEFT);

        $month = $effective_entry_date->format('m');
        $padded_month = str_pad($month, 2, "0", STR_PAD_LEFT);

        $day = $effective_entry_date->format('d');
        $padded_day = str_pad($day, 2, "0", STR_PAD_LEFT);

        $padded_effective_entry_date = $padded_day . $padded_month . $padded_year;

        // pad the applicable bank information
        $originator_id = $bank->originator_id;

        $padded_originator_name = preg_replace("/[^A-Za-z0-9]/", "", $bank->originator_name);
        $padded_originator_name = str_pad($padded_originator_name, 15, " ", STR_PAD_RIGHT);

        $transit_number = $bank->transit_number;

        $padded_account_number = str_pad($bank->account_number, 11, "0", STR_PAD_LEFT) . " ";


        // pad the file number
        $padded_file_creation_number = str_pad($file_creation_number, 4, "0", STR_PAD_LEFT);

        // return the header
        return "H" . $originator_id . "C452" . $padded_effective_entry_date . $padded_originator_name . "0004" . $transit_number . $padded_account_number . $padded_file_creation_number . "                   " . "\r\n";

    }

    /**
     * Returns EFT 80-byte trailer as a properly formatted string
     *
     *  NOTE: This function mutates number of payments and total value of payments
     *          these two variables should be passed in at 0
     *
     * @param $drivers
     * @param $paymentProperties
     *  an object containing effective_entry_date and file_creation_number
     * @param $number_of_payments
     * @param $total_value_of_payments
     * @return string
     */
    public function getEftDetail($drivers, $paymentProperties, &$number_of_payments, &$total_value_of_payments)
    {
        $eft_detail = "";

        //\Debugbar::info($paymentProperties->effective_entry_date);

        // pad the dates
        $effective_entry_date = Carbon::parse($paymentProperties->effective_entry_date);

        //\Debugbar::info($effective_entry_date);

        $year = $effective_entry_date->format('y');
        $padded_year = str_pad($year, 2, "0", STR_PAD_LEFT);

        $month = $effective_entry_date->format('m');
        $padded_month = str_pad($month, 2, "0", STR_PAD_LEFT);

        $day = $effective_entry_date->format('d');
        $padded_day = str_pad($day, 2, "0", STR_PAD_LEFT);

        $padded_effective_entry_date = $padded_day . $padded_month . $padded_year;


        foreach ($drivers as $driver) {
            if (!$driver->payment || $driver->payment < 0) {
                continue;
            }

            $user = \App\User::find($driver->id);

            // only generate details for canadian drivers
            if ($user->driverProfile->address()->country_id != 2) {
                continue;
            }

            // pad payee name
            $padded_payee_name = preg_replace("/[^A-Za-z]/", "", $user->last_name) . " " . preg_replace("/[^A-Za-z]/", "", $user->first_name);
            $padded_payee_name = str_pad($padded_payee_name, 23, " ", STR_PAD_RIGHT);

            // pad user id to use as ref number
            $padded_reference_number = str_pad($user->id, 19, " ", STR_PAD_RIGHT);


            $bankingInformation = $user->banking(2, false); // force canadian, masked = false

            $padded_institution_number = str_pad($bankingInformation->institution_number, 3, "0", STR_PAD_LEFT);
            $padded_branch_number = str_pad($bankingInformation->branch_number, 5, "0", STR_PAD_LEFT); // remember branch is transit
            // sometimes in documentation this is referred to as the drawee account number
            $padded_account_number = str_pad($bankingInformation->account_number, 12, " ", STR_PAD_RIGHT);

            $padded_payment_amount = number_format($driver->payment, 2, "", "");
            $padded_payment_amount = str_pad($padded_payment_amount, 10, "0", STR_PAD_LEFT);

            // increment the number of payments
            $number_of_payments += 1;

            // add payment to the total value of payments
            $total_value_of_payments += number_format($driver->payment, 2, ".", "");

            $eft_detail .= "D" . $padded_payee_name . $padded_effective_entry_date . $padded_reference_number . "0" . $padded_institution_number . $padded_branch_number . $padded_account_number . $padded_payment_amount . "\r\n";
            //\Debugbar::info($padded_institution_number);

        }

        return $eft_detail;
    }

    /**
     * Returns EFT 80-byte trailer as a properly formatted string
     *
     * @param $number_of_payments
     * @param $total :_value_of_payments
     * @return string
     */
    public function getEftTrailer($number_of_payments, $total_value_of_payments)
    {
        $padded_number_of_payments = str_pad($number_of_payments, 8, "0", STR_PAD_LEFT);

        $padded_total_value_of_payments = number_format($total_value_of_payments, 2, "", "");
        $padded_total_value_of_payments = str_pad($padded_total_value_of_payments, 14, "0", STR_PAD_LEFT);

        return "T" . $padded_number_of_payments . $padded_total_value_of_payments . "                                                         " . "\r\n";
    }


    /** END EFT & ACH FUNCTIONS ------------------------------------------------------------------------------------- */


    /** RATE COMPARISON TOOL ---------------------------------------------------------------------------------------- */
    /**
     * This function is called on the Rate Comparison tool
     * It will calculate the impact of proposed vehicle profiles on reimbursement.
     *
     * Return @array
     *  An array of 'rows' are returned. Each row contain a user, driverProfile, currentReimbursement and
     *  a proposedReimbursement object. These objects contain the data required for the rate comparison spreadsheet.
     *
     * Request must contain
     *  - mappings: an array of objects containing old_vehicle_profile_id and proposed_vehicle_profile_id
     *
     * TODO: reimbursement_details records are unavailable for past reimbursements. We need to either convert 1 month's
     * TODO: worth of records, or we have to calculate on the fly. calculation on the fly requires vehicle_profile_ids
     *
     */
    public function calculateImpactOfProposedVehicleProfiles(Request $request)
    {
        // set an empty map table (this will speed up calculation later)
        $mapTable = [];

        foreach ($request->mappings as $key => $value) {
            // skip the unmapped records TODO: check with linda on this one
            if ($value['proposed_vehicle_profile_id'] == -1) {
                continue;
            }

            // create the map table
            $mapTable[$value['old_vehicle_profile_id']] = $value['proposed_vehicle_profile_id'];

        }

        // gather all drivers for each old vehicle profile
        $drivers = DriverProfile::whereIn('vehicle_profile_id', array_keys($mapTable))
            ->whereNull('deleted_at')
            ->get();
        // set time params for reimbursement
        $dates = $this->getPreviousMonthAndYear();
        $year = $dates['year'];
        $month = $dates['month'];

        $proposedVehicleProfile = null;

        $rows = [];
        // loop through these drivers
        foreach ($drivers as $driver) {

            // if not green light, skip TODO: check with linda
            if ($this->streetLightCalculator->colorOfStreetLight($driver->street_light_id) != 'Green') {
                continue;
            }

            if (!$driver->active) {
                continue;
            }

            // if soft deleted, skip TODO: check with linda
            $driver->user;
            if (isset($driver->user->deleted_at)) {
                continue;
            }

            // get the proposed vehicle profile object
            $proposedVehicleProfile = \App\VehicleProfile::find($mapTable[$driver->vehicle_profile_id]);

            // fetch all required data used in the calculation of reimbursement
            $currentReimbursementData = $this->getDataForReimbursementFromDriver(
                $year,
                $month,
                $driver->company->options->service_plan,
                $driver->id
            );

            $proposedReimbursementData = json_decode(json_encode($currentReimbursementData));

            // todo: go untangle the reimbursement code and remove non-geo data from geo_data
            $proposedReimbursementData->geo_data->reimbursement_detail_id = null;
            $proposedReimbursementData->geo_data->reimbursement_details = null;

            // todo go untangle the code and rename vehicleProfile appropriately
            $proposedReimbursementData->vehicle_profile = $proposedVehicleProfile;
            $proposedReimbursementData->vehicle_profile->vehicle;


            /**
             * calculate the fixed and reimbursement based upon reimbursementData, and DO NOT alter the database
             * Jerzy's "Proxy" methods ensure no inserts or updates take place
             */
            $proposedReimbursement = array_merge(
                $this->calculateFixedReimbursementProxy($proposedReimbursementData),
                $this->calculateVariableReimbursementProxy($proposedReimbursementData)
            );

            $currentReimbursement = array_merge(
                $this->calculateFixedReimbursementProxy($currentReimbursementData),
                $this->calculateVariableReimbursementProxy($currentReimbursementData)
            );

            $rows[] = [
                'user' => $driver->user,
                'driverProfile' => $driver,
                'proposedReimbursement' => $proposedReimbursement,
                'currentReimbursement' => $currentReimbursement
            ];
        }
        return $rows;
    }

    public function getProrateAdjustments(Request $request)
    {

        \Debugbar::info($request);

        //$input = (array)json_decode(json_encode($request->all()), true);

        //\Debugbar::info($input);

        $filters = $this->divisionAndUserFilters($request->user_id, $request->division_id, $request->user_type,
            'dp.division_id', 'u.id');
        $division_filter = $filters->division;

        // selected date refers to adjusted reimbursement which was adjusted NEXT month, do
        $m1 = $request->month == 12 ? 1 : $request->month + 1;
        $y1 = $request->month == 12 ? $request->year + 1 : $request->year;
        $m2 = $m1 == 12 ? 1 : $m1 + 1;
        $y2 = $m1 == 12 ? $y1 + 1 : $y1;

        $from_date = sprintf("%04d-%02d-01", $y1, $m1);
        $to_date = sprintf("%04d-%02d-01", $y2, $m2);

        $query = <<<EOQ
SELECT u.last_name, u.first_name, u1.last_name AS driver_last, u1.first_name AS driver_first, dp.company_id, 
    dp.employee_number, d.name as division, dp.cost_center, tc.* 
  FROM tracking_changes tc
  JOIN users u ON tc.by_whom = u.id
  JOIN users u1 ON tc.changed_id = u1.id
  JOIN driver_profiles dp ON tc.changed_id = dp.user_id AND dp.deleted_at IS NULL
  JOIN divisions d ON dp.division_id = d.id
  WHERE tc.change_type = 'Prorate'
  AND tc.change_date >= '$from_date' AND tc.change_date < '$to_date'
  AND dp.company_id = $request->company_id $division_filter
  ORDER BY u1.last_name, u1.first_name, tc.id
EOQ;

        $rows = [];
        $records_found = 0;
        $records = \DB::select(\DB::raw($query));
        foreach ($records as $record) {
            $row = [
                'last_name' => $record->driver_last,
                'first_name' => $record->driver_first,
                'employee_number' => $record->employee_number,
                'division' => $record->division,
                'business_unit' => $record->cost_center,
                'adjusted_name' => $record->change_description,
                'old_value' => $record->old_value,
                'new_value' => $record->new_value,
                'updated_date' => $record->change_date,
                'notes' => $record->notes,
                'updated_by' => "$record->last_name $record->first_name"];
            $rows[] = $row;
            $records_found++;
        }

        return ['rows' => $rows];
    }

    public function getProrateReimbursementData(Request $request)
    {
        $input = (array)json_decode(json_encode($request->all()), true);


        $totals = (object)['mileage_total' => 0, 'fixed_total' => 0, 'variable_total' => 0, 'total_total' => 0];
        $amounts = [];
        $drivers = 0;
        $data = [];

        // Prorate is allowed ONLY for last month not after approval end date

        list($yy, $mm, $dd) = explode('-', date("Y-m-d"));
        $approval_end_day = Company::find($request->company_id)->dates->mileage_approval_end_day;

        if ($dd <= $approval_end_day &&
            ($request->user_type == 'super' ||
                $request->user_type == 'administrator' &&
                AdministratorProfile::where('user_id', $request->user_id)->first()->company_coordinator)) {

            // previous month
            $last_month_yy = $mm == 1 ? $yy - 1 : $yy;
            $last_month_mm = $mm == 1 ? 12 : $mm - 1;

            $input['year'] = $last_month_yy;
            $input['month'] = $last_month_mm;

            if ($dd <= $approval_end_day) {

                $allowed = true;
                $query = $this->reimbursementQuery($input);

                $records = \DB::select(\DB::raw($query));
                foreach ($records as $record) {
                    $record->variable = $this->getVariableReimbursement($request->company_id, $last_month_yy,
                        $last_month_mm, $record->variable_reimbursement, $record->business_mileage);
                    $record->total = $record->fixed_reimbursement + $record->variable;
                    $data[] = $record;

                    $totals->fixed_total += $record->fixed_reimbursement;
                    $drivers++;
                }
            }
        } else {
            $allowed = false;
        }

        return ['allowed' => $allowed, 'rows' => $data, 'totals' => $totals, 'records_found' => $drivers];
    }


    /** REIMBURSEMENT CALCULATOR ------------------------------------------------------------------------------------ */
    public function getReimbursementCalculatorSession($user_id)
    {
        $previousSession = ReimbursementCalculatorSession::where('user_id', $user_id)->first();
        if (!isset($previousSession)) {
            return ReimbursementCalculatorSession::find(1);
        }
        return $previousSession;
    }

    /**
     * ????????????????????????????????????????????????????????????
     * ????????????????????????????????????????????????????????????
     *
     * @param $input array: company_id, division_id (-1), year, month, user_type
     */
    public function reimbursementQuery($input)
    {
        if (!empty($input['manager_id']) && $input['manager_id'] > 0) { // davery
            $managerJoin = "JOIN manager_driver md ON md.driver_id = dp.user_id";
            $managerWhere = "AND md.manager_id = {$input['manager_id']}";
        } else {
            $managerJoin = "";
            $managerWhere = "";
        }
        $company_id = $input['company_id'];
        $division_id = isset($input['division_id']) ? $input['division_id'] : -1;
        $year = $input['year'];
        $month = $input['month'];
        $user_type = $input['user_type'];
        $user_id = $input['user_id'];

        $no_banking_data = GREEN_NO_BANKING_DATA;

        \Debugbar::info("reimbursementQuery user=$user_id, type=$user_type");

        /* from Common Traits */
        $filters = $this->divisionAndUserFilters($user_id, $division_id, $user_type, 'dp.division_id', 'u.id');
        $division_filter = $filters->division;
        $driver_filter = $filters->driver;

        \Debugbar::info("division filter: " . $division_filter);

        $reimbursement_query = <<<EOQ
SELECT u.id, u.last_name, u.first_name, dp.employee_number, di.name AS division, dp.cost_center,
  mr.fixed_reimbursement - mr.adjustment_amount AS fixed_reimbursement, mr.vehicle_depreciation,
  dp.start_date, dp.stop_date, mr.fixed_status,
  mm.business_mileage, mr.variable_reimbursement, v.year as vehicle_year, v.name AS vehicle_name, vp.name_postfix
  FROM users u
  JOIN monthly_reimbursements mr ON mr.user_id = u.id AND mr.year = $year AND mr.month = $month AND( mr.fixed_status & $no_banking_data ) = 0
  JOIN driver_profiles dp ON mr.driver_profile_id = dp.id 
  JOIN monthly_mileages mm ON mm.user_id = u.id AND mm.year = $year AND mm.month = $month
  JOIN divisions di ON di.id = dp.division_id
  JOIN vehicle_profiles vp ON vp.id = dp.vehicle_profile_id
  JOIN vehicles v ON v.id = vp.vehicle_id
  {$managerJoin}
  WHERE dp.company_id = $company_id 
    {$managerWhere}
    {$division_filter} {$driver_filter}
  AND (( mr.fixed_reimbursement - mr.adjustment_amount ) > 0 OR mr.variable_reimbursement * mm.business_mileage  > 0 ) 
  ORDER BY u.last_name, u.first_name
EOQ;

        return $reimbursement_query;

    }

    private function reimbursementTotalsQuery($input)
    {
        $company_id = $input['company_id'];
        $division_id = isset($input['division_id']) ? $input['division_id'] : -1;
        $from_date = $input['from_date'];
        $to_date = $input['to_date'];
        $user_type = $input['user_type'];
        $user_id = isset($input['user_id']) ? $input['user_id'] : 'super';

        $from_yy = substr($from_date, 0, 4);
        $from_mm = substr($from_date, 5, 2);
        $to_yy = substr($to_date, 0, 4);
        $to_mm = substr($to_date, 5, 2);

        $totals = (object)['mileage_total' => 0, 'fixed_total' => 0, 'variable_total' => 0, 'total_total' => 0,
            'personal_total' => 0];
        $records_found = 0;

        $admin_clause = '';   // default for $administrator_profile->assigned_divisions == 'all'
        $manager_clause = '';
        $div_arr = [];

        $filters = $this->divisionAndUserFilters($user_id, $division_id, $user_type, 'dp.division_id', 'u.id');
        $division_filter = $filters->division;
        $driver_filter = $filters->driver;


        // get drivers
        $query = <<<EOQ
SELECT DISTINCT u.id, u.last_name, u.first_name, v.year as vehicle_year, v.name AS vehicle_name, vp.name_postfix,
  dp.employee_number, dp.start_date, dp.stop_date, dp.cost_center, di.name AS division
  FROM users u 
  JOIN driver_profiles dp ON u.id = dp.user_id AND dp.company_id = $company_id AND dp.deleted_at IS NULL $division_filter $driver_filter
  JOIN divisions di ON dp.division_id = di.id
  JOIN vehicle_profiles vp ON vp.id = dp.vehicle_profile_id AND vp.deleted_at IS NULL
  JOIN vehicles v ON v.id = vp.vehicle_id
  WHERE EXISTS ( 
    SELECT 1 FROM monthly_reimbursements mr WHERE mr.user_id = u.id 
      AND ( mr.year = $from_yy AND mr.month >= $from_mm OR mr.year > $from_yy ) 
      AND ( mr.year = $to_yy AND mr.month <= $to_mm OR mr.year < $to_yy ))  
  ORDER BY u.last_name, u.first_name
EOQ;

        $rows = [];

        $drivers = \DB::select(\DB::raw($query));
        foreach ($drivers as $d) {

            $fixed_query = <<<EOQ
SELECT SUM(fixed_reimbursement - adjustment_amount) AS fixed_reimbursement 
FROM monthly_reimbursements WHERE user_id = $d->id 
AND ( year = $from_yy AND month >= $from_mm OR year > $from_yy )
AND ( year = $to_yy   AND month <= $to_mm   OR year < $to_yy ) 
EOQ;
            $fix = \DB::select(\DB::raw($fixed_query))[0];

            $vehicle_profile = $d->vehicle_year . ' ' . $d->vehicle_name . ' ' . $d->name_postfix;

            $row = $d;
            $row->vehicle_profile = $vehicle_profile;
            $row->fixed_reimbursement = round($fix->fixed_reimbursement, 2);

            $from_date = sprintf("%04d-%02d-01", $from_yy, $from_mm);
            $to_date = sprintf("%04d-%02d-01", $to_yy, $from_mm);
            $end_of_month = date("Y-m-t", strtotime($to_date));

            // \Debugbar::info('row=',$row);

            if ($company_id != 45) { // Mitel USA

                $variable_query = <<<EOQ
SELECT SUM(ROUND(mr.variable_reimbursement*mm.business_mileage/100,2)) AS variable_reimbursement, 
    SUM(mm.business_mileage) AS business_mileage, SUM(mm.personal_mileage) as personal_mileage 
  FROM monthly_reimbursements mr, monthly_mileages mm
  WHERE mr.user_id = $d->id  
  AND ( mr.year = $from_yy AND mr.month >= $from_mm OR mr.year > $from_yy )
  AND ( mr.year = $to_yy   AND mr.month <= $to_mm   OR mr.year < $to_yy ) 
  AND mm.user_id = mr.user_id AND mm.year = mr.year AND mm.month = mr.month 
EOQ;
                $var = \DB::select(\DB::raw($variable_query))[0];

            } else {

                $var_total = 0;
                $bus_total = 0;
                for ($yy = $from_yy; $yy <= $to_yy; $yy++) {
                    $from = $yy == $from_yy ? $from_mm : 1;
                    $to = $yy == $to_yy ? $to_mm : 12;
                    $var_45_query = <<<EOQ
SELECT mr.variable_reimbursement, mm.business_mileage, mr.month, mr.year 
FROM monthly_reimbursements mr, monthly_mileages mm
  WHERE mr.user_id = $d->id  AND mr.year = $yy AND mr.month => $from AND mr.month <= $to
  AND mm.year = mr.year AND mm.month = mr.month 
EOQ;
                    $vars = \DB::select(\DB::raw($var_45_query));
                    foreach ($vars as $v) {

                        if ($v->year == 2010 && $v->month > 1 || $v->year > 2010) {
                            if ($v->business_mileage > 1100) {
                                $var_total += round(($v->business_mileage - 1100) * 0.26, 2);
                            }
                        } else {
                            $var_total += round($v->variable_reimbursemment * $v->business_mileage / 100, 2);
                        }
                        $bus_total += $v->business_mileage;
                    }
                }
                $var = (object)['business_mileage' => $bus_total, 'variable_reimbursement' => $var_total];
            }

            $row->business_mileage = $var->business_mileage;
            $row->personal_mileage = $var->personal_mileage;
            $row->variable_reimbursement = round($var->variable_reimbursement, 2);
            $row->total = round($row->fixed_reimbursement + $var->variable_reimbursement, 2);

            $rows[] = $row;

            // \Debugbar::info('var=',$var);

            $totals->mileage_total += $var->business_mileage;
            $totals->fixed_total += $row->fixed_reimbursement;
            $totals->variable_total += $var->variable_reimbursement;
            $totals->total_total += $row->fixed_reimbursement + $var->variable_reimbursement;
            $totals->personal_total += $row->personal_mileage;
            $records_found++;

        }   // drivers loop

        $totals->fixed_total = round($totals->fixed_total, 2);
        $totals->variable_total = round($totals->variable_total, 2);
        $totals->total_total = round($totals->total_total, 2);

        \Debugbar::info('rows', $rows);
        \Debugbar::info('totals', $totals);

        return ['rows' => $rows, 'totals' => $totals, 'records_found' => $records_found];

    }

    /**
     * This function is called to gather all data for the master payment notice page.
     *
     * @param Request $request
     * @return array
     */
    public function getMasterPaymentData(Request $request)
    {
        $input = (array)json_decode(json_encode($request->all()), true);

        $totals = new \stdClass();
        $totals->business_mileage = 0;
        $totals->fixed_reimbursement = 0;
        $totals->variable_reimbursement = 0;
        $totals->total_reimbursement = 0;
        $totals->non_taxable_limit = 0;
        $totals->taxable_reimbursement = 0;
        $totals->reimbursement_with_per_driver_fee = 0;
        $totals->direct_pay_driver_fee = 0;
        $messages = (object)['bank_info' => null, 'payment_info' => null];
        $amounts = [];
        $drivers = 0;
        $amount_summary = ['item' => 'Amount Due to CarData', 'details' => 'TOTAL', 'amount' => 0];
        $data = [];

        $query = $this->masterPaymentQuery($input);

        $company = Company::find($input['company_id']);


        /**
         * Get any relevant fee data as this effects which columns are displayed!
         */

        // get fees
        $beginning_of_month = sprintf("%04d-%02s-01", $input['year'], $input['month']);
        $fee = \DB::table('company_fees')
            ->where('company_id', $input['company_id'])
            ->where('fee', 'driver')
            ->where('from_date', '<=', $beginning_of_month)
            ->orderBy('from_date', 'desc')
            ->pluck('amount')
            ->first();

        $direct_pay_driver_fee = !empty($fee) ? $fee : 0;

        $fee = \DB::table('company_fees')->
        where('company_id', $input['company_id'])->
        where('fee', 'file')->
        where('from_date', '<=', $beginning_of_month)->
        orderBy('from_date', 'desc')->pluck('amount')->first();
        $direct_pay_file_fee = !empty($fee) ? $fee : 0;

        /**
         * Table Columns Titles
         */
        $columnTitles = [];
        $columnTitles['last_name'] = 'Last';
        $columnTitles['first_name'] = 'First';
        $columnTitles['employee_number'] = 'Employee #';

        /**
         * If the company has display_tax_limit is Pulte or 5ME
         * Note that for these two we call Division Market and Cost Center area
         * Otherwise we give them defaults
         */
        if ($company->taxes->reports_display_monthly_tax_limit) {
            $columnTitles['division'] = 'Market';
            $columnTitles['cost_center'] = 'Area';
        } else {
            $columnTitles['division'] = 'Division';
            $columnTitles['cost_center'] = 'Business Unit';
        }

        if ($company->options->reports_display_job_title) {
            $columnTitles['job_title'] = ($company->taxes->reports_display_monthly_tax_limit) ? 'Job Code/Title' : 'Job Title';
        } else {
            $columnTitles['job_title'] = null;
        }

        /**
         * Again this is pulte and 5ME only, we show tier (otherwise known as vehicle_profiles.name_postfix)
         */
        if ($company->taxes->reports_display_monthly_tax_limit) {
            $columnTitles['vehicle_profile_postfix'] = 'Tier';
        } else {
            $columnTitles['vehicle_profile_postfix'] = null;
        }

        $columnTitles['fixed_reimbursement'] = 'Fixed';
        $columnTitles['business_mileage'] = date('F', strtotime($input['month'])) . ' Business Mileage';
        $columnTitles['variable_reimbursement'] = 'Variable';
        $columnTitles['total_reimbursement'] = 'Total';

        /**
         * If the company has a direct pay per driver fee, show it!
         */
        if ($direct_pay_driver_fee > 0) {
            $columnTitles['direct_pay_driver_fee'] = 'Direct Deposit Fee';
            $columnTitles['reimbursement_with_per_driver_fee'] = 'Total Per Driver';
        } else {
            $columnTitles['direct_pay_driver_fee'] = null;
            $columnTitles['reimbursement_with_per_driver_fee'] = null;
        }

        $columnTitles['start_date'] = 'Start Date';
        $columnTitles['stop_date'] = 'Stop Date';

        /**
         * If reports display tax limit, show these too!
         */
        if ($company->taxes->reports_display_monthly_tax_limit) {
            $columnTitles['non_taxable_limit'] = 'Non-Taxable Limit';
            $columnTitles['taxable_reimbursement'] = 'Taxable Reimbursement';
            $taxController = new TaxController();
            $totals->non_taxable_limit_total = 0;
            $totals->taxable_reimbursement_total = 0;
        } else {
            $columnTitles['non_taxable_limit'] = null;
            $columnTitles['taxable_reimbursement'] = null;
        }


        $records = \DB::select(\DB::raw($query));
        foreach ($records as $record) {
            $record->variable_reimbursement = $this->getVariableReimbursement($request->company_id, $request->year,
                $request->month, $record->variable_reimbursement, $record->business_mileage);
            $record->total_reimbursement = $record->fixed_reimbursement + $record->variable_reimbursement;

            /**
             * Get the tax limits if appropriate
             */
            if ($company->taxes->reports_display_monthly_tax_limit) {

                $record->non_taxable_limit = $taxController->calculateW2NonTaxableLimit(
                    $record->business_mileage,
                    $input['year'],
                    $input['month']
                );

                $record->taxable_reimbursement = $taxController->calculateTaxableAmount463(
                    $record->business_mileage,
                    $input['year'],
                    $input['month'],
                    $record->total_reimbursement
                )['taxable'];

                $totals->non_taxable_limit += $record->non_taxable_limit;
                $totals->taxable_reimbursement += $record->taxable_reimbursement;
            }

            /**
             * Get the per driver fee if appropriate
             */
            if ($company->payments->direct_pay_driver_fee > 0) {
                $record->direct_pay_driver_fee = $direct_pay_driver_fee;
                $totals->direct_pay_driver_fee += $direct_pay_driver_fee;
                $record->reimbursement_with_per_driver_fee = $record->total_reimbursement + $record->direct_pay_driver_fee;
                $totals->reimbursement_with_per_driver_fee += $record->reimbursement_with_per_driver_fee;
            }

            /**
             * Calculate the remaining totals
             */
            $totals->business_mileage += $record->business_mileage;
            $totals->fixed_reimbursement += $record->fixed_reimbursement;
            $totals->variable_reimbursement += $record->variable_reimbursement;
            $totals->total_reimbursement += $record->total_reimbursement;

            /**
             * Unset the bad values,
             * why bring back data we aren't showing
             */
            foreach ($record as $key => $value) {

                if (!array_key_exists($key, $columnTitles) && $key != 'id') {
                    unset($record->{$key});
                }
            }

            $data[] = $record;
            $drivers++;
        }


        /**
         * Jerzy's direct pay section
         */
        if ($company->dates->direct_pay_day > 0
            && ($request->user_type == 'super'
                || $request->user_type == 'administrator'
                && AdministratorProfile::where('user_id', $request->user_id)->first()->company_approver == 1
            )
        ) { // populate payment array only for direct payment

            $messages->payment_info = 'The payment will be processed 3 business days following the payment to CarData Consultants.';

            $amounts[] = ['item' => 'Fixed Reimbursement', 'details' => null, 'amount' => $totals->fixed_reimbursement];
            $amounts[] = ['item' => 'Variable Reimbursement', 'details' => null, 'amount' => $totals->variable_reimbursement];
            $amounts[] = ['item' => 'Fee per Driver', 'details' => "$drivers drivers * " . number_format($direct_pay_driver_fee, 2), 'amount' => round($drivers * $direct_pay_driver_fee, 2)];
            $amounts[] = ['item' => 'Bank File Fee', 'details' => number_format($direct_pay_file_fee, 2), 'amount' => round($direct_pay_file_fee, 2)];

            $amount_summary = ['item' => 'Amount Due to CarData', 'details' => 'TOTAL',
                'amount' => round($totals->fixed_reimbursement + $totals->variable_reimbursement + $drivers * $direct_pay_driver_fee + $direct_pay_file_fee, 2)];

            $bank = Company::find($request->company_id)->bank();
            $messages->bank_info = "$bank->bank_name since " . substr($bank->active_since, 0, 10);
            $messages->payment_info = 'The payment will be processed 3 business days following the payment to CarData Consultants.';
        }

        /**
         * Frank 2018-08-30
         * The following code is to determine whether or not to display the section to generate ach or eft,
         * and is also here to get the information about whether or not payment was processed, and approved!
         */

        /**
         * Get the month's approval record
         */
        $approvalRecord = MonthlyApproval::where('year', $input['year'])
            ->where('month', $input['month'])
            ->where('company_id', $input['company_id'])
            ->first();

        /**
         * Get the list of approvers who have not approved the payment
         * if it is empty, great
         * if not get each user's name and role
         */
        $approvers = empty($approvalRecord)
            ? null
            : MonthlyApprover::where('approval_id', $approvalRecord->id)
                ->whereNull('approval_date')
                ->get();

        $approversWhoHaventApproved = [];
        if (!empty($approvers)) {
            foreach ($approvers as $approver) {

                if (!empty($approver->manager_id)) {
                    $user_who_didnt_approve = ManagerProfile::where('manager_id', $approver->manager_id)
                        ->first()
                        ->user_id;
                    $role = 'Manager';
                } else {
                    $user_who_didnt_approve = AdministratorProfile::where('administrator_id', $approver->administrator_id)
                        ->first()
                        ->user_id;
                    $role = 'Administrator';
                }

                $user = User::find($user_who_didnt_approve);

                $approversWhoHaventApproved[] = [
                    'role' => $role,
                    'username' => $user->username,
                    'name' => $user->first_name . ', ' . $user->last_name
                ];
            }
        }

        return ['columnTitles' => $columnTitles, 'rows' => $data, 'totals' => $totals, 'amounts' => $amounts, 'amount_summary' => $amount_summary,
            'records_found' => $drivers, 'messages' => $messages, 'approvalRecord' => $approvalRecord,
            'approversWhoHaventApproved' => $approversWhoHaventApproved
        ];
    }


    /**
     * This is a modified version of the reimbursement query below.
     * It will get the required data for master payment
     *
     * @param $input
     * @return string
     */
    private function masterPaymentQuery($input)
    {
        if (!empty($input['manager_id']) && $input['manager_id'] > 0) { // davery
            $managerJoin = "JOIN manager_driver md ON md.driver_id = dp.user_id";
            $managerWhere = "AND md.manager_id = {$input['manager_id']}";
        } else {
            $managerJoin = "";
            $managerWhere = "";
        }
        $company_id = $input['company_id'];
        $division_id = isset($input['division_id']) ? $input['division_id'] : -1;
        $year = $input['year'];
        $month = $input['month'];
        $user_type = $input['user_type'];
        $user_id = $input['user_id'];

        $no_banking_data = GREEN_NO_BANKING_DATA;

        \Debugbar::info("reimbursementQuery user=$user_id, type=$user_type");

        /* from Common Traits */
        $filters = $this->divisionAndUserFilters($user_id, $division_id, $user_type, 'dp.division_id', 'u.id');
        $division_filter = $filters->division;
        $driver_filter = $filters->driver;

        \Debugbar::info("division filter: " . $division_filter);

        $reimbursement_query = <<<EOQ
SELECT u.id, u.last_name, u.first_name, dp.employee_number, di.name AS division, dp.cost_center,
  mr.fixed_reimbursement - mr.adjustment_amount AS fixed_reimbursement, mr.vehicle_depreciation,
  dp.start_date, dp.stop_date, mr.fixed_status, dp.job_title,
  mm.business_mileage, mr.variable_reimbursement, v.year as vehicle_year, v.name AS vehicle_name, vp.name_postfix as vehicle_profile_postfix
  FROM users u
  JOIN monthly_reimbursements mr ON mr.user_id = u.id AND mr.year = $year AND mr.month = $month AND( mr.fixed_status & $no_banking_data ) = 0
  JOIN driver_profiles dp ON mr.driver_profile_id = dp.id 
  JOIN monthly_mileages mm ON mm.user_id = u.id AND mm.year = $year AND mm.month = $month
  JOIN divisions di ON di.id = dp.division_id
  JOIN vehicle_profiles vp ON vp.id = dp.vehicle_profile_id
  JOIN vehicles v ON v.id = vp.vehicle_id
  {$managerJoin}
  WHERE dp.company_id = $company_id 
    {$managerWhere}
    {$division_filter} {$driver_filter}
  AND (( mr.fixed_reimbursement - mr.adjustment_amount ) > 0 OR mr.variable_reimbursement * mm.business_mileage  > 0 ) 
  ORDER BY u.last_name, u.first_name
EOQ;

        return $reimbursement_query;
    }


    /**
     * This function generates the ach or eft file from the master payment notice!
     *
     * Request must contain:
     *  drivers (array) [
     *      (driver obj) {
     *          payment: (double),
     *          id: (int) note this is also the user_id
     *      }
     *      ...
     *  ],
     *
     *  paymentProperties: {
     *      effective_entry_date: (YYYY-MM-DD)
     *      file_id_modifier: (str)
     *      type: (str)
     *  },
     *
     *  company_id: (int)
     *
     * @param Request $request
     * @return array
     */
    public function generateAchEftFromMasterPaymentNotice(Request $request)
    {
        /**
         * The functions called will take care of their own permission checks.
         */
        try {
            /**
             * Both functions return ['filename' => the_files_name]
             */
            \Log::info('Calling generateAch', ['file' => 'ReimbursementController']);
            if ($request->file_type == 'ach') {
                return $this->generateAch($request);
            } else if ($request->file_type == 'eft') {
                return $this->generateEft($request);
            }
        } catch (\Exception $e) {
            return response(400, $e->getMessage());
        }

    }

    /**
     * This function is called after generateAchFromMasterPaymentNotice to update the company's monthly
     * approval record.
     * Once that record is updated, the generate ach/eft box will no longer show on the master payment notice
     * for the give company, year and month
     */
    public function updateMonthlyApproval(Request $request)
    {
        try {
            MonthlyApproval::where('company_id', $request->company_id)
                ->where('year', $request->year)
                ->where('month', $request->month)
                ->update([
                    'approval_date' => date('Y-m-d H:i:s')
                ]);
        } catch (\Exception $e) {
            return response(400, $e->getMessage());
        }
    }


    /**
     * This function generates a an excel doc for the master payment notice page.
     * It takes most of the data present on that page, and turns it into an excel sheet.
     *
     * request must contain:
     *  columnTitles
     *  tableHeaderColumnSpans
     *  company_id
     *  rows
     *  year
     *  month
     *  totals
     *  divison_id
     *  manager_id
     *  amounts
     *  amount_summary
     *
     * @param Request $request
     * @return array
     * @throws PhpSpreadsheet\Exception
     * @throws PhpSpreadsheet\Reader\Exception
     * @throws PhpSpreadsheet\Writer\Exception
     */
    public function generateMasterPaymentNoticeExcel(Request $request)
    {
        $spreadsheet = PhpSpreadsheet\IOFactory::load(base_path() . '/resources/assets/xls/master_payment_notice_template.xls');
        $worksheet = $spreadsheet->getActiveSheet();

        $request = json_decode(json_encode($request->all()));

        $company = Company::find($request->company_id);
        $reportOptions = $company->reportOptions();


        /**
         * Insert the table header
         * Start by construction the style dictionary
         * Then merge the appropriate cells, finally insert the data and apply the styles
         */
        $styleArray = [
            'fill' => [
                'fillType' => PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => [
                    'argb' => 'FF99CCFF'
                ],
            ],
            'font' => [
                'bold' => true
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ];

        $start_driver_column = 'A';
        $end_driver_column = chr(ord($start_driver_column) + $request->tableHeaderColumnSpans->driver - 1);
        $driver_cell_range = $start_driver_column . '1:' . $end_driver_column . '1';
        $worksheet->mergeCells($driver_cell_range);
        $worksheet->getCell('A1')->setValue('Driver');
        $worksheet->getStyle($driver_cell_range)->applyFromArray($styleArray);

        $start_reimbursement_column = chr(ord($end_driver_column) + 1);
        $end_reimbursement_column = chr(ord($start_reimbursement_column) + $request->tableHeaderColumnSpans->reimbursement - 1);
        $reimbursement_cell_range = $start_reimbursement_column . '1:' . $end_reimbursement_column . '1';
        $worksheet->mergeCells($reimbursement_cell_range);
        $worksheet->getCell($start_reimbursement_column . '1')->setValue('Reimbursement');
        $worksheet->getStyle($reimbursement_cell_range)->applyFromArray($styleArray);

        $start_dates_column = chr(ord($end_reimbursement_column) + 1);
        $end_dates_column = chr(ord($start_dates_column) + 1);
        $dates_cell_range = $start_dates_column . '1:' . $end_dates_column . '1';
        $worksheet->mergeCells($dates_cell_range);
        $worksheet->getCell($start_dates_column . '1')->setValue('Driver Dates');
        $worksheet->getStyle($dates_cell_range)->applyFromArray($styleArray);

        if ($request->tableHeaderColumnSpans->tax > 0) {
            $start_tax_column = chr(ord($end_dates_column) + 1);
            $end_tax_column = chr(ord($start_tax_column) + 1);
            $tax_cell_range = $start_tax_column . '1:' . $end_tax_column . '1';
            $worksheet->mergeCells($tax_cell_range);
            $worksheet->getCell($start_tax_column . '1')->setValue('Tax');
            $worksheet->getStyle($tax_cell_range)->applyFromArray($styleArray);
        }


        /**
         * Next insert the sub-titles
         * As we loop through the column titles, we generate a lookup table of the following format
         * for easy insertion of data later on:
         *  [ $key => chr(col + index) , ... ]
         *
         * We also update the style array to apply a different colour
         *
         */
        $styleArray = [
            'fill' => [
                'fillType' => PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => [
                    'argb' => 'FF808080'
                ]
            ],
            'font' => [
                'bold' => true
            ]
        ];

        $lookupTable = [];
        $col_num = ord('A');
        $index = 0;
        foreach ($request->columnTitles as $key => $title) {
            $column = chr($col_num + $index);
            $lookupTable[$key] = $column;
            $worksheet->getCell($column . '2')->setValue($title);
            $worksheet->getStyle($column . '2')->applyFromArray($styleArray);
            $index++;
        }


        /**
         * Next loop through the rows, and insert values!
         * Note we start at index 3 as title and subtitles are rows 1 and 2 respectively on our worksheet
         *      and also note that row is an object of type std class hence the foreach loop with
         *      indices baby please
         */
        $i = 3;
        foreach ($request->rows as $row) {
            foreach ($row as $key => $value) {
                if (array_key_exists($key, $lookupTable)) {
                    $worksheet->getCell($lookupTable[$key] . $i)->setValue($value);
                }
            }
            $i++;
        }


        /**
         * Display the totals
         * These always start at fixed reimbursement, so we now in the cell to the left of that,
         * we can place the word TOTALS
         */
        $totals_starting_cell = chr(ord($lookupTable['fixed_reimbursement']) - 1);
        $worksheet->getCell($totals_starting_cell . $i)->setValue('TOTALS');

        /**
         * Next loop through the other totals, and get em in!
         * We keep of count so that we can bold the range of cells from
         * totals_starting_cell to totals_starting_cell + count
         */
        $count = 0;
        foreach ($request->totals as $key => $value) {
            if (array_key_exists($key, $lookupTable)) {
                $worksheet->getCell($lookupTable[$key] . $i)->setValue($value);
                if ($key != 'business_mileage') {
                    $worksheet->getStyle($lookupTable[$key] . $i)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
                $count++;
            }
        }

        /**
         * Bold the totals
         */
        $range_to_bold = $totals_starting_cell . $i . ':' . chr(ord($totals_starting_cell) + $count + 2) . $i;
        $worksheet->getStyle($range_to_bold)->getFont()->setBold(true);


        /**
         * Next give 3 spaces to the last record
         * We then insert the report info
         */
        $i += 3;
        $reportInfo = [];
        $reportInfo[] = 'Master Payment Notice';
        $reportInfo[] = date('F d, Y');
        $reportInfo[] = $company->name;
        $reportInfo[] = $request->division_id == -1 ? 'All' : Division::find($request->division_id)->name;
        $reportInfo[] = $request->year;
        $reportInfo[] = date('F', mktime(0, 0, 0, $request->month, 10));

        if ($request->manager_id < 1) {
            $reportInfo[] = 'All';
        } else {
            $managerUser = ManagerProfile::where('manager_id', $request->manager_id)->first()->user;
            $reportInfo[] = $managerUser->last_name . ', ' . $managerUser->first_name;
        }

        for ($j = 0; $j < sizeof($reportInfo); $j++) {
            $title = '';
            switch ($j) {
                case 0:
                    $title = 'Report Name';
                    break;
                case 1:
                    $title = 'Creation Date';
                    break;
                case 2:
                    $title = 'Company';
                    break;
                case 3:
                    $title = 'Division';
                    break;
                case 4:
                    $title = 'Year';
                    break;
                case 5:
                    $title = 'Month';
                    break;
                case 6:
                    $title = 'Manager';
                    break;
            }

            $worksheet->getCell('A' . ($i + $j))->setValue($title);
            $worksheet->getStyle('A' . ($i + $j))->getFont()->setBold(true);
            $worksheet->getCell('B' . ($i + $j))->setValue($reportInfo[$j]);
            $worksheet->getStyle('B' . ($i + $j))->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        }

        // reconcile current row number
        $i += sizeof($reportInfo);


        /**
         * If amounts were included in the report add them
         * These are the payment details oddly enough... thanks jerzy
         */
        if (sizeof($request->amounts) > 0) {
            // add 3 spaces again
            $i += 3;

            // bold the titles
            $cellsToBold = ['A', 'B', 'C'];
            $cellsToBold = array_map(function ($c) use ($i) {
                return $c . $i;
            }, $cellsToBold);
            foreach ($cellsToBold as $cell) {
                $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                ];
                $spreadsheet->getActiveSheet()->getStyle($cell)->applyFromArray($styleArray);
            }

            $worksheet->getCell('A' . $i)->setValue('Item');
            $worksheet->getCell('B' . $i)->setValue('Details');
            $worksheet->getCell('C' . $i)->setValue('Total');

            $i++;

            foreach ($request->amounts as $a) {
                $worksheet->getCell('A' . $i)->setValue($a->item);
                $worksheet->getCell('B' . $i)->setValue($a->details);
                $worksheet->getCell('C' . $i)->setValue($a->amount);
                $worksheet->getStyle('C' . $i)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $i++;
            }

            /**
             * Bold the final line
             */
            $cellsToBold = ['A', 'B', 'C'];
            $cellsToBold = array_map(function ($c) use ($i) {
                return $c . $i;
            }, $cellsToBold);
            foreach ($cellsToBold as $cell) {
                $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                ];
                $spreadsheet->getActiveSheet()->getStyle($cell)->applyFromArray($styleArray);
            }

            $worksheet->getCell('A' . $i)->setValue($request->amount_summary->item);
            $worksheet->getCell('B' . $i)->setValue($request->amount_summary->details);
            $worksheet->getCell('C' . $i)->setValue($request->amount_summary->amount);
            $worksheet->getStyle('C' . $i)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);


        }

        /**
         * Finally set the column widths appropriately
         */
        for ($i = ord('A'); $i < ord('A') + 20; $i++) {
            $worksheet->getColumnDimension(chr($i))->setAutoSize(true);
        }

        $filename = 'master_payment_notice_' . implode('_', explode(' ', strtolower($company->name))) . '_' . date("Y-m-d") . '.xls';

        $writer = PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save(public_path('/storage/tmp') . '/' . $filename);

        return response()->download(public_path('/storage/tmp/' . $filename));
        
    }


    /**
     * @param Request $request
     * @return array
     */
    public function getReimbursementData(Request $request)
    {
        $input = (array)json_decode(json_encode($request->all()), true);

//        return response(json_encode($request->all()));

        $totals = (object)['mileage_total' => 0, 'fixed_total' => 0, 'variable_total' => 0, 'total_total' => 0];
        $drivers = 0;
        $data = [];

        $all_lock_day = Company::find($request->company_id)->mileage->mileage_lock_all_day;
        $next_month = $request->month == 12 ? 1 : $request->month + 1;
        $next_month_year = $request->month == 12 ? $request->year + 1 : $request->year;
        $formated_lock_all = date("F jS", mktime(0, 0, 0, $next_month, $all_lock_day, $next_month_year));

        $query = $this->reimbursementQuery($input);

        $records = \DB::select(\DB::raw($query));
        foreach ($records as $record) {
            $record->variable = $this->getVariableReimbursement($request->company_id, $request->year,
                $request->month, $record->variable_reimbursement, $record->business_mileage);
            $vehicle_profile_name = $record->vehicle_year . ' ' . $record->vehicle_name;
            if (!empty($record->name_postfix)) $vehicle_profile_name .= ' - ' . $record->name_postfix;
            $record->vehicle_profile = $vehicle_profile_name;
            $record->total = $record->fixed_reimbursement + $record->variable;
            $data[] = $record;

            $totals->mileage_total += $record->business_mileage;
            $totals->fixed_total += $record->fixed_reimbursement;
            $totals->variable_total += $record->variable;
            $totals->total_total += $record->total;
            $drivers++;
        }

        return ['rows' => $data, 'totals' => $totals, 'records_found' => $drivers, 'formatted_lock_all' => $formated_lock_all];
    }

    public function getReimbursementTotals(Request $request)
    {
        $input = (array)json_decode(json_encode($request->all()), true);
        $totals = (object)['mileage_total' => 0, 'fixed_total' => 0, 'variable_total' => 0, 'total_total' => 0, 'personal_mileage' => 0];
        $drivers = 0;
        $data = [];

        return $this->reimbursementTotalsQuery($input);
    }


    /**
     * This function will return the appropriately formatted data for the power to the people component
     * It requires the user id of the driver in question
     */
    public function getPowerToThePeopleData($user_id)
    {

        /**
         * For ease of use:
         *
         * oneMonthPrior = [ 'year' => xxxx, 'month' => xx ] of previous month
         * twoMonthsPrior = [ 'year' => xxxx, 'month' => xx ] of two months previous
         */
        $oneMonthPrior = $this->getPreviousMonthAndYear();
        $twoMonthsPrior = $this->getPreviousMonthAndYear($oneMonthPrior['year'], $oneMonthPrior['month']);

        $user = User::find($user_id);
        $oneMonthPriorReimbursement = $user->driverProfile->reimbursement($oneMonthPrior['year'], $oneMonthPrior['month']);
        $twoMonthsPriorReimbursement = $user->driverProfile->reimbursement($twoMonthsPrior['year'], $twoMonthsPrior['month']);


        /**
         * Get the mileage data from one month prior
         */
        $oneMonthPriorMileage = MonthlyMileage::where('user_id', $user->id)
            ->where('year', $oneMonthPrior['year'])
            ->where('month', $oneMonthPrior['month'])
            ->first();

        /**
         * Get the mileage data from two months prior
         */
        $twoMonthsPriorMileage = MonthlyMileage::where('user_id', $user->id)
            ->where('year', $twoMonthsPrior['year'])
            ->where('month', $twoMonthsPrior['month'])
            ->first();


        /**
         * Todo discus with jerzy how to get the notes
         */

        /**
         * Construct the data array for the front end
         * array pos 0 will be the most recent line (1 month ago)
         * array pos 1 will be the other (2 months ago)
         */
        $data = [];
        $data[] = new \stdClass();
        $data[] = new \stdClass();

        /**
         * Previous month
         */
        $data[0]->year = $oneMonthPrior['year'];
        $data[0]->month = $oneMonthPrior['month'];
        $data[0]->ending_odometer = $oneMonthPriorMileage->ending_odometer;
        $data[0]->business_mileage = $oneMonthPriorMileage->business_mileage;
        $data[0]->correct_mileage = false;
        $data[0]->cents_per_mile = !empty($oneMonthPriorReimbursement['variable_reimbursement']) ? $oneMonthPriorReimbursement['variable_reimbursement'] : 0;
        $data[0]->fixed_reimbursement = !empty($oneMonthPriorReimbursement['fixed_reimbursement']) ? $oneMonthPriorReimbursement['fixed_reimbursement'] : 0;
        $data[0]->correct_reimbursement = false;
        $data[0]->notes = null;

        /**
         * Previous to the previous
         */
        $data[1]->year = $oneMonthPrior['year'];
        $data[1]->month = $twoMonthsPrior['month'];
        $data[1]->ending_odometer = $twoMonthsPriorMileage->ending_odometer;
        $data[1]->business_mileage = $twoMonthsPriorMileage->business_mileage;
        $data[1]->correct_mileage = false;
        $data[1]->cents_per_mile = !empty($twoMonthsPriorReimbursement['variable_reimbursement']) ? $twoMonthsPriorReimbursement['variable_reimbursement'] : 0;
        $data[1]->fixed_reimbursement = !empty($twoMonthsPriorReimbursement['fixed_reimbursement']) ? $twoMonthsPriorReimbursement['fixed_reimbursement'] : 0;
        $data[1]->correct_reimbursement = false;
        $data[1]->notes = null;


        return $data;
    }

    public function thePeopleExerciseTheirPower(Request $request)
    {
        $data = json_decode(json_encode($request->all()));

        $now = date('Y-m-d H:i:s');
        /**
         * Force a check of highest authenticated user type just to be safe
         * Store the editingUser so we can apply change tracker records
         */
        $editingUser = auth()->user();
        if (!$editingUser->isSuper()) {
            return response('Unauthorized', 401);
        }

        /**
         * This is the user being corrected.
         * He is the marxist proletariat being forced into submission by a corrupt oligarchy
         */
        $user = User::find($request->user_id);

        foreach ($data->rows as $row) {
            /**
             * First check if we are correcting mileage
             */
            if ($row->correct_mileage) {

                /**
                 * Get the old monthly mileage record
                 * Store the old val, and the new val, and create the appropriate
                 * change tracker entries
                 * Then update the monthly mileage record
                 */
                $oldMonthlyMileageRecord = MonthlyMileage::where('user_id', $user->id)
                    ->where('year', $row->year)
                    ->where('month', $row->month)
                    ->first();

                /**
                 * Ending Odometer
                 */
                if ($oldMonthlyMileageRecord->ending_odometer != $row->ending_odometer) {
                    TrackingChange::create([
                        'changed_id' => $user->id,
                        'by_whom' => $editingUser->id,
                        'change_type' => 'Power To The People',
                        'change_description' => 'ending_odometer',
                        'old_value' => $oldMonthlyMileageRecord->ending_odometer,
                        'new_value' => $row->ending_odometer,
                        'notes' => $row->notes,
                        'change_date' => $now
                    ]);
                }

                /**
                 * Business Mileage
                 */
                if ($oldMonthlyMileageRecord->business_mileage != $row->business_mileage) {
                    TrackingChange::create([
                        'changed_id' => $user->id,
                        'by_whom' => $editingUser->id,
                        'change_type' => 'Power To The People',
                        'change_description' => 'business_mileage',
                        'old_value' => $oldMonthlyMileageRecord->business_mileage,
                        'new_value' => $row->business_mileage,
                        'notes' => $row->notes,
                        'change_date' => $now
                    ]);
                }

                $oldMonthlyMileageRecord->business_mileage = $row->business_mileage;
                $oldMonthlyMileageRecord->ending_odometer = $row->ending_odometer;
                $oldMonthlyMileageRecord->save();
            }

            /**
             * Next check if we are correcting reimbursement
             */
            if ($row->correct_reimbursement) {

                /**
                 * Gather the old reimbursement from the database
                 * Make a call to recalculate reimbursement
                 * If the values differ, create a change tracker record
                 * (potentially 1 for fixed and 1 for variable)
                 */
                $oldReimbursement = $user->driverProfile->reimbursement($row->year, $row->month);
                $this->generateReimbursement($row->year, $row->month, $user->driverProfile->company_id, $user->id);
                $newReimbursement = $user->driverProfile->reimbursement($row->year, $row->month);

                /**
                 * Old and New reimbursement may contain null values.
                 * Ensure that if they are null we supply 0
                 */
                $oldReimbursement['fixed_reimbursement'] = empty($oldReimbursement['fixed_reimbursement']) ? 0 : $oldReimbursement['fixed_reimbursement'];
                $oldReimbursement['variable_reimbursement'] = empty($oldReimbursement['variable_reimbursement']) ? 0 : $oldReimbursement['variable_reimbursement'];
                $newReimbursement['fixed_reimbursement'] = empty($newReimbursement['fixed_reimbursement']) ? 0 : $newReimbursement['fixed_reimbursement'];
                $newReimbursement['variable_reimbursement'] = empty($newReimbursement['variable_reimbursement']) ? 0 : $newReimbursement['variable_reimbursement'];

                /**
                 * Fixed reimbursement
                 */
                if ($oldReimbursement['fixed_reimbursement'] != $newReimbursement['fixed_reimbursement']) {
                    TrackingChange::create([
                        'changed_id' => $user->id,
                        'by_whom' => $editingUser->id,
                        'change_type' => 'Power To The People',
                        'change_description' => 'fixed_reimbursement',
                        'old_value' => $oldReimbursement['fixed_reimbursement'],
                        'new_value' => $newReimbursement['fixed_reimbursement'],
                        'notes' => $row->notes,
                        'change_date' => $now
                    ]);
                }

                /**
                 * Variable reimbursement
                 */
                if ($oldReimbursement['variable_reimbursement'] != $newReimbursement['variable_reimbursement']) {
                    TrackingChange::create([
                        'changed_id' => $user->id,
                        'by_whom' => $editingUser->id,
                        'change_type' => 'Power To The People',
                        'change_description' => 'variable_reimbursement',
                        'old_value' => $oldReimbursement['variable_reimbursement'],
                        'new_value' => $newReimbursement['variable_reimbursement'],
                        'notes' => $row->notes,
                        'change_date' => $now
                    ]);
                }

            }
        }
        return ['success_message' => 'Successfully corrected records for ' . $user->last_name . ', ' . $user->first_name];
    }

    public function getStandardMileageRate($country_id = -1)
    {
        //If false value
        if($country_id == -1){
           return;
        }
        // Oh Canada
        if ($country_id == 2) {
            $select = <<< EOQ
             SELECT  * FROM cra_rates;
EOQ;
            // If United States
        } else {
            $select = <<<EOQ
            SELECT * FROM irs_standard_rates;


EOQ;
        }
        $data = \DB::select(\DB::raw($select));
        return $data;
    }
}
