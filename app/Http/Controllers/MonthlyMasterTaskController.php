<?php
/**
 * Created by PhpStorm.
 * User: Jerzy 15
 * Date: 7/4/2018
 * Time: 4:11 PM
 */

namespace App\Http\Controllers;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

use App\Calculators\StreetLightCalculator;
use App\Http\Traits\CommonTraits;   // to log scheduled task run


class MonthlyMasterTaskController extends Controller
{
    use CommonTraits;
    private $street_light;
    private $reimbursementController;

    public function __construct()
    {
        $this->middleware('auth');
        $this->street_light = new StreetLightCalculator();
        $this->reimbursementController = new ReimbursementController();
    }

    public function stub($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/stub.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id'=>$task_id, 'task_name'=>$task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 94
     *
     * @param $task_id
     * @param $task_name
     */
    public function generateReimbursementValues($task_id, $task_name){

        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits

        // set up a new logger
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/generateReimbursementValues.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Startedd', ['task_id'=>$task_id, 'task_name'=>$task_name]);

        $long_description = '';

        $year = 0; $month = 0; $company_id = 0; $user_id = 0; $rates_only=0;

        $count = $this->reimbursementController->generateReimbursement( $year, $month, $company_id, $user_id, $rates_only, $logger );

        $logger->addInfo($task_name, ['finished' => var_dump($count)]);

        $this->scheduledTaskEnded($task_id, $log_id, 'Not Fully Implemented', '');
    }

    /**
     * master_tasks id: 126
     *
     * @param $task_id
     * @param $task_name
     */
    public function vehicleReimbursementAtRisk($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/vehicleReimbursementAtRisk.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id'=>$task_id, 'task_name'=>$task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }

    /**
     * master_tasks id: 83
     *
     * Although this task is "Weekly", it runs in the monthly controller with 6,13,20,27 as pre-defined dates
     *
     * @param $task_id
     * @param $task_name
     */
    public function weeklyBulletinBoardNotices($task_id, $task_name)
    {
        $log_id = $this->scheduledTaskStarted($task_id, $task_name);    // from CommonTraits
        $logger = new Logger('tasks');
        $handler = new RotatingFileHandler(config('app.vhost_root') . '/logs/tasks/weeklyBulletinBoardNotices.log', 0, Logger::INFO);
        $logger->pushHandler($handler);
        $logger->addInfo('Task Not Implemented', ['task_id'=>$task_id, 'task_name'=>$task_name]);
        $this->scheduledTaskEnded($task_id, $log_id, 'Not Implemented', '');
    }
}