<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 2018-09-19
 * Time: 10:16 AM
 */

namespace App\Http\Controllers;

use App\FileGenerators\ExcelGenerator;
use Illuminate\Http\Request;
use App\FileGenerators\DirectDepositPaymentScheduleExcelGenerator;
use App\Http\Traits\ValidationTraits;

class FileController extends Controller
{
    use ValidationTraits;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Returns the direct deposit payment schedule for a given company in
     * a given year
     *
     * @param $company_id
     * @param $year
     */
    public function getDirectDepositPaymentScheduleExcel($company_id, $year)
    {
        if (empty(\DB::table('company_payment_days')
            ->where('company_id', $company_id)
            ->where('year', $year))) {
            return response('Invalid year and company', 400);
        }

        $excelGen = new DirectDepositPaymentScheduleExcelGenerator($company_id, $year);
        $excelGen->create();
        $excelGen->store();
        return response()->download(public_path('/storage/tmp/' . $excelGen->filename));
    }

    /**
     * This function is used to generate our general excel spreadsheets.
     * See the class definition in ExcelGenerator.php for details on what your request must contain.
     *
     * If you require functionality beyond what is present in ExcelGenerator.php, or need an
     * spreadsheet that does not fit the standard CarData pattern, implement a new class
     * inside of the app/FileGenerators/ and extend ExcelGeneratorAbstract.
     *
     * For further detail see:
     * @link https://cardata.atlassian.net/wiki/spaces/REW/pages/515670017/Files+Generation+downloads+etc...
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function generateExcel(Request $request)
    {
        try {
            $this->validateExcelGenerationData($request->all());
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        $excelGen = new ExcelGenerator($request->all());
        $excelGen->create();
        $excelGen->store();
        return response()->download(public_path('/storage/tmp/' . $excelGen->filename));

    }





}