<?php

namespace App\Http\Controllers;

use App\Company;
use App\PostalCode;
use App\ZipCode;
use App\MileageBand;
use App\User;
use App\StateProvince;
use App\VehicleProfile;
use App\AdministratorProfile;
use Barryvdh\Debugbar\DataCollector\IlluminateRouteCollector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Traits\ValidationTraits;
use App\Http\Traits\CommonTraits;
use PhpOffice\PhpSpreadsheet;


class AdministratorController extends Controller
{
    use CommonTraits;
    use ValidationTraits;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getMyDivisions($administrator_id) {
        $administrator = AdministratorProfile::where('administrator_id', $administrator_id)
                                             ->where('deleted_at', null)
                                             ->first();

        $divisions = $administrator->divisions();

        return response()->json([
            'divisions' => $divisions
        ]);
    }

    public function postAdministratorApproval(Request $request){

        $now = date("Y-m-d H:i:s");
        \DB::table('monthly_approvers')
            ->where('approval_id',$request->approval_id)
            ->where('administrator_id', $request->administrator_id)->update(array('approval_date' => $now ));
    }


    public function postManagerApprovalStatus(Request $request) {
        $manager_id_arr = $request->managers_arr;
        $now = date("Y-m-d H:i:s");

        if( !empty($manager_id_arr)){
            \DB::table('monthly_approvers')
                ->where('approval_id',$request->approval_id)
                ->whereIn('manager_id', $manager_id_arr)->update(array('override_user_id' => $request->administrator_id, 'approval_date' => $now ));
        }
    }

    public function getAdministratorProfile($active_administrator_id = -1)
    {
        $administatorProfile = null;
        if ($active_administrator_id > 0) {
            $administatorProfile = AdministratorProfile::where('administrator_id', $active_administrator_id)->first();
        }
        return ['administratorProfile'=>$administatorProfile];
    }

    public function calculateReimbursementForAdministrator(Request $request)
    {
        $parameters = json_decode(json_encode($request->parameters));

        // validate
        $errors = [];

        if ($parameters->state_province_id < 1) {
            $errors['state_province_id'] = ['Invalid ' . ($parameters->country_id == 1 ? 'state' : 'province')];
        }

        // check that zip postal is valid one
        try {
            $this->validateZipPostal($parameters->country_id, $parameters->state_province_id, $parameters->zip_postal);
        } catch (\Exception $e) {
            $errors = array_merge($errors, (array)json_decode($e->getMessage(), true));
        }

        if ($parameters->territory_type_id > 2) {
            if ( !isset($parameters->territory_list) || sizeof($parameters->territory_list) < 1 ) {
                $errors['territory_list'] = ['A territory selection is required'];
            }
        }

        if ($parameters->mileage_band_id < 1 || 12 < $parameters->mileage_band_id) {
            $errors['mileage_band_id'] = ['Invalid mileage band'];
        }

        if ($parameters->vehicle_profile_id < 1) {
            $errors['vehicle_profile_id'] = ['A vehicle profile is required'];
        }

        if ( !empty($errors) ) {
            return response($errors, 400);
        }

        $data = new \stdClass();
        $data->geo_data = new \stdClass();
        $data->geo_data->address = new \stdClass();

        $data->vehicle_profile = VehicleProfile::find($parameters->vehicle_profile_id);
        $data->vehicle_profile->vehicle;

        $data->geo_data->year = date('Y');
        $data->geo_data->month = date('m') - 1; // always last month
        $zipPostal = $parameters->country_id == 1 ? ZipCode::find($parameters->zip_postal) : PostalCode::find($parameters->zip_postal);
        $data->geo_data->address->latitude = $zipPostal->latitude;
        $data->geo_data->address->longitude = $zipPostal->longitude;
        $data->geo_data->address->zip_postal = $parameters->zip_postal;
        $data->geo_data->address->state_province_id = $parameters->state_province_id;
        $data->geo_data->address->country_id = $parameters->country_id;
        $data->geo_data->mileage_band_id = $parameters->mileage_band_id;
        $data->geo_data->territory_type_id = $parameters->territory_type_id;
        $data->geo_data->territory_list = is_array($parameters->territory_list) ? implode(",", $parameters->territory_list) : $parameters->territory_list;
        $data->geo_data->plan = $parameters->service_plan;

        // force green
        $data->geo_data->street_light_id = GREEN_REIMBURSEMENT;

        // since we are calculating from scratch, we do not want to look in the reimbursement_details table
        $data->geo_data->reimbursement_detail_id = null;
        $data->geo_data->reimbursement_details = null;

        // calculate rates
        $fixedReimbursementData = app('\App\Http\Controllers\ReimbursementController')->calculateFixedReimbursementProxy($data);
        $variableReimbursementData = app('\App\Http\Controllers\ReimbursementController')->calculateVariableReimbursementProxy($data);

        return [
            'fixed_rate' => $fixedReimbursementData['fixedReimbursement'],
            'variable_rate' => $variableReimbursementData['variableReimbursement'],
            'fuel_average' => $variableReimbursementData['variableReimbursementDetails']['fuelAverage']['amount_adj'],
        ];

    }


    public function generateExcelForReimbursementCalculatorAdministrator(Request $request)
    {
        $parameters = json_decode(json_encode($request->parameters));

        $spreadsheet = PhpSpreadsheet\IOFactory::load(base_path() . '/resources/assets/xls/reimbursement_calculator_administrator_template.xls');
        $worksheet = $spreadsheet->getActiveSheet();

        // variable units
        $variable_units = $parameters->country_id == 1 ? 'Cents/mile' : 'Cents/km';
        $worksheet->getCell('C1')->setValue($variable_units);

        // mileage band
        $formatted_mileage_band = $this->formattedMileageBand($parameters->mileage_band_id);
        $worksheet->getCell('A3')->setValue($formatted_mileage_band);

        // fixed & variable rate
        $worksheet->getCell('B3')->setValue($request->fixed_rate);
        $worksheet->getCell('C3')->setValue($request->variable_rate);

        // date
        $date = date('F jS, Y', strtotime('now'));
        $worksheet->getCell('B7')->setValue($date);

        // company
        $worksheet->getCell('B8')->setValue($request->company_name);

        // driver name
        $worksheet->getCell('B9')->setValue($parameters->driver_name);

        // Zip Postal
        $zip_postal_label = $parameters->country_id == 1 ? 'Zip Code:' : 'Postal Code:';
        $worksheet->getCell('A10')->setValue($zip_postal_label);
        $worksheet->getCell('B10')->setValue($parameters->zip_postal);

        // State Province
        $state_province_label = $parameters->country_id == 1 ? 'State:' : 'Province:';
        $worksheet->getCell('A11')->setValue($state_province_label);
        $stateProvince = StateProvince::find($parameters->state_province_id);
        $worksheet->getCell('B11')->setValue($stateProvince->name);

        // vehicle profile
        $vehicleProfile = VehicleProfile::find($parameters->vehicle_profile_id);
        $worksheet->getCell('B12')->setValue($vehicleProfile->name());

        // territory
        $formatted_territory_type = $this->formattedTerritoryType($parameters->territory_type_id);
        $worksheet->getCell('B13')->setValue($formatted_territory_type);

        // territory list
        $formatted_territory_list = 'n/a';
        if ($parameters->territory_type_id > 2) {
            $formatted_territory_list = $this->formattedTerritoryList($parameters->territory_type_id, implode(',',$parameters->territory_list));
            $worksheet->getCell('B14')->setValue($formatted_territory_list);
            $worksheet->getCell('A14')->setValue('Territory List:');
            $worksheet->getCell('B15')->setValue($request->fuel_string);
            $worksheet->getCell('A15')->setValue('Fuel:');
        } else {
            $worksheet->getCell('B14')->setValue($request->fuel_string);
            $worksheet->getCell('A14')->setValue('Fuel:');
        }

        $tmp_dir = storage_path() . '/app/public/tmp/';
        $file_name = 'reimbursement_administrator_' . date("Y-m-d") . '.xls';

        $writer = PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save($tmp_dir . $file_name);

        return ['file_name' => $file_name];

    }


    public function getUsersAdministrator($company_id)
    {
        return app('App\Http\Controllers\UserController')->getUsers($company_id);
    }

    public function getDataForAdministratorAddManyDrivers($company_id)
    {
        $company = Company::find($company_id);
        if (empty($company)) { abort(400, 'Company not found'); }

        return [
            'divisions' => $company->divisions,
            'vehicleProfiles' => $company->vehicleProfiles->map(function($vp, $key) { return $vp->name(); }),
            'mileageBands' => MileageBand::all()->map(function($mb, $key) { return $mb->name(); } )
        ];
    }
}
