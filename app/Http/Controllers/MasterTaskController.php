<?php

namespace App\Http\Controllers;

use App\MasterTask;
use App\ScheduledTask;
use Illuminate\Http\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use App\Http\Traits\CommonTraits;

class MasterTaskController extends Controller
{
    protected $hourlyMasterTaskController;
    protected $dailyMasterTaskController;
    protected $monthlyMasterTaskController;
    protected $annualMasterTaskController;

    private $today;
    private $log;

    public function __construct()
    {
        $this->hourlyMasterTaskController = new HourlyMasterTaskController();
        $this->dailyMasterTaskController = new DailyMasterTaskController();
        $this->monthlyMasterTaskController = new MonthlyMasterTaskController();
        $this->annualMasterTaskController = new AnnualMasterTaskController();

        $this->middleware('auth');

        // create a log for today
        $this->today = date('Y-m-d');
        CommonTraits::delete_older_than(storage_path('logs/tasks'), 3600 * 24 * 7);
        $this->log = new Logger('tasks');
        $this->log->pushHandler(new StreamHandler(storage_path('logs/tasks/tasks.' . $this->today . '.log')), Logger::INFO);
    }

    public function runManualTask(MasterTask $task)
    {
        $this->log->info('Running Task', ['task' => $task]);

        switch ($task->interval) {
            case 'hourly':
                $this->hourlyMasterTaskController->{$task->script}($task->id, $task->name);
                break;
            case 'daily':
                $this->dailyMasterTaskController->{$task->script}($task->id, $task->name);
                break;
            case 'monthly':
                $this->monthlyMasterTaskController->{$task->script}($task->id, $task->name);
                break;
            case 'annually':
                $this->annualMasterTaskController->{$task->script}($task->id, $task->name);
                break;
        }
    }

    public function getMasterTask(MasterTask $task)
    {
        return ['task'=>$task];
    }

    public function getMasterTaskStatus($task_id)
    {
        $mt_run = MasterTask::where('id', $task_id)->select(['last_run', 'last_status'])->first();
        return ['run_result' => $mt_run];
    }

    public function updateMasterTaskStatus($task_id, $status = '')
    {
        $date = date('Y-m-d H:i:s');
        MasterTask::find($task_id)->update(['last_status' => $status, 'last_run' => $date]);
        $scheduledTask = ScheduledTask::where('task_id', $task_id)->first();
        if (!empty($scheduledTask)) {
            $scheduledTask->update(['status'=>'Failed']);
        }
        return ['last_status' => $status, 'last_run' => $date];
    }
}
