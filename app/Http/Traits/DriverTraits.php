<?php namespace App\Http\Traits;



use App\User;
use App\Company;
use App\DriverProfile;
use App\CompanyInsurance;
use App\CompanyLicense;
use App\DriverStreetLightStatus;
use App\Http\Traits\ReimbursementTraits;
use App\Http\Traits\CommonTraits;
use App\Calculators\StreetLightCalculator;


use Auth;
use Carbon\Carbon;


trait DriverTraits {

    use ReimbursementTraits;


    /**
     * Function to be called any time a driver profile field is updated.
     * This function will take care of:
     *  - soft deleting the old driver profile
     *  - soft deleting old relationships (ex. address)
     *  - calculating reimbursement details if necessary
     *  - calculating street_light_id if necessary
     *  - calculating reimbursement if necessary
     *
     *
     * @param $user_id
     * @param $data
     *      A dictionary of key value pairs:
     *          ex. if you are updating address, pass in the newly created address' id
     *              data = ['address_id' => $the_address_id]
     * @return driverProfile
     *      The newly updated driver profile is returned, so that it can be commited to the store on the frontend
     */
    public function updateDriverProfile($user_id, $data)
    {

        $user = User::find($user_id);

        $oldDriverProfile = $user->driverProfile;
        $newDriverProfile = $oldDriverProfile->replicate();

        $toDoList = [
            'calculate_driver_street_light' => false,
            'calculate_reimbursement_details' => false,
            'check_if_necessary_to_calculate_reimbursement' => false,
        ];


        /**
         * Loop over all data fields to be updated.
         * Based on what driver_profiles columns are being changed, perform the appropriate actions:
         *  - do nothing
         *  - simple update
         *  - calculate reimbursement details
         *  - calculate street light id
         *  - check if necessary to calculate monthly reimbursement
         *
         */
        foreach ($data as $key => $value) {

            switch($key) {

                // no action cases
                case 'company_id':
                case 'editing_user_id':
                    // company_id should not occur, do nothing
                    // editing user id will be not be passed, it will be determined with AUTH
                    break;

                // update profile value cases
                case 'division_id':
                case 'cost_center' :
                case 'employee_number':
                case 'start_date':
                case 'stop_date':
                case 'job_title':
                case 'car_policy_accepted':
                    $newDriverProfile[$key] = $value;
                    break;

                // calculate reimbursement details cases
                case 'vehicle_profile_id' :
                case 'mileage_band_id':
                case 'territory_type_id':
                case 'territory_list':
                    $newDriverProfile[$key] = $value;
                    if ($oldDriverProfile[$key] != $newDriverProfile[$key]) {
                        $toDoList['calculate_reimbursement_details'] = true;
                    }
                    break;

                // check if necessary to calculate reimbursement cases
                case 'street_light_id':
                    // put the new street_light_id onto driver profile
                    $newDriverProfile->street_light_id = $data['street_light_id'];
                    // update the to do list
                    $toDoList['check_if_necessary_to_calculate_reimbursement'] = true;
                    break;

                case 'address_id':
                    // put the new address onto the new driver profile
                    $newDriverProfile->address_id = $data['address_id'];
                    // soft delete the old address record
                    $oldDriverProfile->address()->delete();
                    // update our to do list
                    $toDoList['calculate_reimbursement_details'] = true;
                    break;

                case 'personal_vehicle_id':
                    $newDriverProfile->personal_vehicle_id = $data['personal_vehicle_id'];
                    $oldDriverProfile->personalVehicle->delete();
                    // check if necessary to update street light
                    $toDoList['check_if_necessary_to_calculate_reimbursement'] = true;
                    break;

            }

        }

        $newDriverProfile->editing_user_id = Auth::user()->id;

        /**
         * we do this here, because the calculate reimbursement details function for example,
         * requires an already existing record on the driver_profiles table
         */
        $oldDriverProfile->delete();
        $newDriverProfile->save();

        // get the date information
        $today = Carbon::today();

        if ( $toDoList['calculate_driver_street_light'] ) {
            $c = new StreetLightCalculator();
            // calculate the driver street light
            $newDriverProfile->street_light_id = $c->calculateStreetLightId($user_id);
            $newDriverProfile->save();
        }

        // if street_light_id is 1 (YELLOW_NEW_DRIVER) do not calculate TODO: VERIFY
        if ( $toDoList['calculate_reimbursement_details'] && $newDriverProfile->street_light_id != YELLOW_NEW_DRIVER) {

            /** not pretty but this is a forced requirement by code in the reimbursement controller
             *  if we do not set the current reimbursement_details_id to null, it will simply overwrite the old
             *  record, and thus result in a loss of historical data ie. old driver profile will point to new reimbursement details...
             *  if calculateReimbursementData sees reimbursement_detail_id, it will, to save time, grab old records instead
             *  of recalculating
             */
            $newDriverProfile->reimbursement_detail_id = null;
            $newDriverProfile->save();

            // calculate the reimbursement data
            $reimbursementData = $this->calculateReimbursementData($user_id, $today->year, $today->month);

            // insert a new reimbursement details record, and put the id on the driver profile
            $newDriverProfile->reimbursement_detail_id = $this->insertReimbursementDetails($user_id, $reimbursementData);
            $newDriverProfile->save();
        }

        if ( $toDoList['check_if_necessary_to_calculate_reimbursement'] ) {

            if (empty($c)) { $c = new StreetLightCalculator(); }

            /**
             * It is necessary to recalculate reimbursement in the following scenarios:
             *
             *  street_light_id has changed:
             *      If the street_light_id has changed from red to green up until the mileage lock driver date.
             *          or
             *      For Waste Management US and Canada, if the start date is the prior month or current month.
             *
             *  personal_vehicle_id has changed:
             *      If the company is Waste Management
             *      green to red, red to red, up until mileage lock all date
             */
            if ($newDriverProfile->street_light_id != $oldDriverProfile->street_light_id) {

                $red_to_green = $c->fromRedToGreen($newDriverProfile->street_light_id, $oldDriverProfile->street_light_id);

                $before_mileage_lock_driver_day = $today->format('d') <= Company::find($newDriverProfile->company_id)->dates->mileage_lock_driver_day;

                if ( $red_to_green && $before_mileage_lock_driver_day ) {
                    $this->generateReimbursementForDriver($user_id, $today->year, $today->month);
                } else if ( $newDriverProfile->company_id == WASTE_MANAGEMENT_USA || $newDriverProfile->company_id == WASTE_MANAGEMENT_CANADA ) {

                    $firstDayOfPreviousMonth = Carbon::now()->startOfMonth()->subMonth()->toDateString();
                    $lastDayOfCurrentMonth = Carbon::now()->endOfMonth()->toDateString();

                    // if the start_date is in the last month or the current month
                    if ( $firstDayOfPreviousMonth <= $newDriverProfile->start_date && $newDriverProfile->start_date <= $lastDayOfCurrentMonth ) {
                        $this->generateReimbursementForDriver($user_id, $today->year, $today->month);
                    }
                }

            } else if ( $newDriverProfile->personal_vehicle_id != $oldDriverProfile->personal_vehicle_id ) {

                $red_to_red = $c->fromRedToRed($newDriverProfile->street_light_id, $oldDriverProfile->street_light_id);
                $green_to_red = $c->fromGreenToRed($newDriverProfile->street_light_id, $oldDriverProfile->street_light_id);
                $before_mileage_lock_all_day = $today->format('d') <= Company::find($newDriverProfile->company_id)->mileage->mileage_lock_all_day;

                if ( ($green_to_red || $red_to_red) && $before_mileage_lock_all_day ) {
                    $this->generateReimbursementForDriver($user_id, $today->year, $today->month);
                }
            }
        }

        return $newDriverProfile;

    }



    public function updateDriverAddress(Request $request)
    {
    }

    /**
     * Drivers without reimbursement records or with zero payable reimbursement amount.
     * Exclude drivers with red statuses ( not insurance related )
     *
     * @param $user_type ['admin','super'] with admin add filter on administrator divisions
     * @param $company_id
     * @param $year
     * @param $month
     * @param null $division_id
     */
    public function driversWithoutReimbursement( $user_type, $company_id, $year, $month, $division_id = null) {

        // year and month is for reimbursement, we have to find first day of current month
        $yyyy = $month == 12 ? $year + 1 : $year;
        $mm = $month == 12 ? 1 : 1 + $month ;
        $first_of_month = sprintf("%04d-%02d-01", $yyyy, $mm);
        $yyyy1 = $mm == 12 ? $yyyy + 1 : $yyyy;
        $mm1 = $mm == 12 ? 1 : 1 + $mm;
        $first_of_next_month = sprintf("%04d-%02d-01", $yyyy1, $mm1);
        $no_reimbursement = RED_NO_REIMBURSEMENT;
        $terminated = RED_TERMINATED;
        $on_leave = RED_ON_LEAVE;
        $disability = RED_DISABILITY;

        // description = place holder to get description of street light id

        $drivers_query = <<<EOQ
        SELECT dp.user_id, u.last_name, u.first_name, COALESCE(nrr.street_light_id,dp.street_light_id) AS street_light_id, 
          dp.stop_date, '' AS description, 'A' AS type, dp.start_date
        FROM driver_profiles dp
        JOIN users u ON dp.user_id = u.id
        LEFT JOIN monthly_reimbursements mr ON mr.driver_profile_id = dp.id AND mr.year = $year AND mr.month = $month
        LEFT JOIN no_reimbursement_reasons nrr ON nrr.user_id = dp.user_id AND nrr.year = $year AND nrr.month = $month
        WHERE dp.company_id = $company_id AND dp.start_date < '$first_of_month' AND dp.active = 1
        AND dp.street_light_id & $no_reimbursement = 0 AND dp.street_light_id & $terminated = 0
        AND dp.street_light_id & $on_leave = 0 AND dp.street_light_id & $disability = 0
        AND mr.driver_profile_id IS NULL
        UNION
        SELECT dp.user_id, u.last_name, u.first_name, COALESCE(mr.fixed_status,dp.street_light_id) AS street_light_id, 
          dp.stop_date, '' AS description, 'B' AS type, dp.start_date
        FROM driver_profiles dp
        JOIN users u ON u.id = dp.user_id
        JOIN monthly_reimbursements mr ON mr.driver_profile_id = dp.id AND mr.year = $year AND mr.month = $month
        WHERE dp.company_id = $company_id AND dp.start_date < '$first_of_month' and dp.active = 1
        AND ( dp.stop_date IS NULL OR dp.stop_date >= '$first_of_next_month')
        AND mr.variable_reimbursement = 0 AND ( mr.fixed_reimbursement = 0 OR mr.fixed_reimbursement = mr.adjustment_amount )
        ORDER BY 2,3
        
EOQ;

        $drivers = \DB::select(\DB::raw($drivers_query));
        return $drivers;

    }

    public function getGraceData($company_id, $user_id, $date=null, $testing=false){


        if(empty(date)){
            $date = date("Y-m-d");
        }
        list($year,$month,$day) = explode('-',$date);

        // previous month: year and month
        $p_year = $month == 1 ? $year - 1 : $year;
        $p_month = $month == 1 ? 12 : $month - 1;
        $lm = mktime(0,0,0, $p_month, 1, $p_year);
        $last_day_of_month = date("j");

        // get payment in advance and driver lock date
        $company = Company::find($company_id);
        $mileage_lock_day = $company->dates->mileage_lock_driver_day;
        $advance_payment = $company->payments->use_advance_payment;

        // dates involved in calculations
        $beginning_of_last_month = sprintf("%04d-%02d-01", $p_year, $p_month);
        $last_month_lock_date = sprintf("%04d-%02d-%02d", $p_year, $p_month, $mileage_lock_day);
        $beginning_of_this_month = sprintf("%04d-%02d-01", $year, $month);
        $this_month_lock_date = sprintf("%04d-%02d-%02d", $year, $month, $mileage_lock_day);
        $end_of_this_month = sprintf("%04d-%02d-%02d", $year, $month, $last_day_of_month);

        if( $advance_payment) {

            // 1. new vehicle was entered after last month lock date, so reimbursement was not run yet for this driver
            $vehicles_query = <<<EOQ
            SELECT created_at FROM personal_vehicles WHERE user_id = $user_id AND 
              created_at > '$last_month_lock_date' AND created_on <= '$this_month_lock_date'
              ORDER BY 1 LIMIT 1;
EOQ;
            $vehicles = \DB::select(\DB::raw($vehicles_query));
            if ($vehicles) {
                return (object)['inGrace'=>true, 'graceEndDate' => $this_month_lock_date ];
            }

            // 2. new driver and registration was done this month before current lock day
            $driver_query = <<<EOQ
            SELECT d.start_date FROM driver_profiles AS d, personal_vehicles AS v WHERE d.user_id = $user_id 
              AND d.user_id = u.user_id AND start_date >= '$beginning_of_this_month' 
              AND start_date <= '$this_month_lock_date' AND v.created_on <= '$this_month_lock_date' 
              ORDER BY v.created_on LIMIT 1
EOQ;
            $driver = \DB::select(\DB::raw($driver_query));
            if ($vehicles) {
                return (object)['inGrace'=>true, 'graceEndDate' => $this_month_lock_date ];
            }

            // 3. insurance was submitted last month after
            $insurance_query = <<<EOQ
            SELECT approval_date FROM driver_insurance_reviews WHERE user_id = $user_id 
            AND approval_date >= '$beginning_of_last_month' AND approval_date < '$this_month_lock_date'
EOQ;
            $insurances = \DB::select(\DB::raw($insurance_query));
            if( $insurances) {
                return (object)['inGrace'=>true, 'graceEndDate' => $this_month_lock_date ];
            }


        } else {

            // 1. Driver started last month
            $driver_query = <<<EOQ
            SELECT d.start_date FROM driver_profiles d WHERE user_id = $user_id
            AND d.start_date >= '$beginning_of_last_month' AND d.start_date < '$beginning_of_this_month' 
EOQ;
            $drivers = \DB::select(\DB::raw($driver_query));
            if( $drivers) {
                return (object)['inGrace'=>true, 'graceEndDate' => $beginning_of_this_month ];
            }

            // 2. new vehicle was added last month
            $vehicle_query = <<<EOQ
            SELECT created_at FROM personal_vehicles WHERE created_at >= '$beginning_of_last_month' 
            AND created_at < '$beginning_of_this_month'           
EOQ;
            $vehicles = \DB::select(\DB::raw($vehicle_query));
            if( $vehicles) {
                return (object)['inGrace'=>true, 'graceEndDate' => $beginning_of_this_month ];
            }
        }

        return (object)['inGrace'=>false, 'graceEndDate' => null ];

    }

}