<?php namespace App\Http\Traits;

use App\CompanyInvoicing;
use App\CompanyPayments;
use Illuminate\Support\Facades\Validator as Validator;
use Illuminate\Validation\Rule as Rule;
use Illuminate\Http\Request;
use App\User;
use App\Company;
use App\Bank;
use App\CompanyFee;


trait ValidationTraits
{

    public function validateUser($user)
    {
        $errors = [];

        // validate the email
        try {
            $this->validateEmail($user['email'], 'both', isset($user['id']) ? $user['id'] : -1);
        } catch (\Exception $e) {
            $errors['email'] = json_decode($e->getMessage())->email;
        }

        // validate personal email if applicable
        if (isset($user['personal_email']) && sizeof($user['personal_email']) > 0) {
            try {
                $this->validateEmail($user['personal_email'], 'both', isset($user['id']) ? $user['id'] : -1);
            } catch (\Exception $e) {
                $errors['personal_email'] = json_decode($e->getMessage())->email;
            }
        }

        // first name
        if (!preg_match('/^[a-zA-Z][a-zA-Z,.\- \']*$/', $user['first_name'])) {
            $errors['first_name'] = ['Invalid first name, first name must contain only letters, spaces, and apostrophes'];
        }

        // last name
        if (!preg_match('/^[a-zA-Z][a-zA-Z,.\- \']*$/', $user['last_name'])) {
            $errors['last_name'] = ['Invalid last name, last name must contain only letters, spaces, and apostrophes'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    public function validateDriverProfile($driverProfile)
    {
        $errors = [];

        if (!isset($driverProfile['company_id']) || $driverProfile['company_id'] < 1) {
            $errors['company_id'] = ['A company is required'];
        }

        if (!isset($driverProfile['division_id']) || $driverProfile['division_id'] < 1) {
            $errors['division_id'] = ['A division is required'];
        }

        if (!isset($driverProfile['street_light_id']) || $driverProfile['street_light_id'] < 1) {
            $errors['street_light_id'] = ['A street_light_id is required'];
        }

        // nothing for employee number (since we use weird codes)

        if (isset($driverProfile['job_title']) && strlen($driverProfile['job_title']) > 250) {
            $errors['job_title'] = ['Maximum length for job title is 250 characters'];
        }

        if (isset($driverProfile['cost_center']) && strlen($driverProfile['cost_center']) > 250) {
            $errors['cost_center'] = ['Maximum length for cost center is 250 characters'];
        }


        if (!isset($driverProfile['vehicle_profile_id']) || $driverProfile['vehicle_profile_id'] < 1) {
            $errors['vehicle_profile_id'] = ['A vehicle profile is required'];
        }

        if (!isset($driverProfile['mileage_band_id']) || $driverProfile['mileage_band_id'] < 1) {
            $errors['mileage_band_id'] = ['A mileage band is required'];
        }

        if (!isset($driverProfile['territory_type_id']) || $driverProfile['territory_type_id'] < 1) {
            $errors['territory_type_id'] = ['A territory type is required'];
        } else {
            // if territory type is multi city or multi state we must have a territory list
            if ($driverProfile['territory_type_id'] == 3 || $driverProfile['territory_type_id'] == 4) {
                //todo check if the territory list is empty or not
                if (sizeof($driverProfile['territory_list']) < 1) {
                    $errors['territory_list'] = ['A territory is required'];
                }

            }
        }


        // somehow validate the managers


        if (!isset($driverProfile['start_date']) || !(bool)strtotime($driverProfile['start_date'])) {
            $errors['start_date'] = ['A start date is required'];
        }

        if (isset($driverProfile['stop_date']) && !(bool)strtotime($driverProfile['start_date'])) {
            $errors['stop_date'] = ['Invalid stop date'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    /**
     * This validates managers assigned to a driver profile (ie. the data to be inserted to manager_driver)
     *
     * @param $managers
     * @throws \Exception
     */
    public function validateAssignedManagers($managers)
    {
//        $errors = [];
//
//        foreach($managers as $id => $manager) {
//            $m = \App\ManagerProfile::where('manager_id', $id)->first();
////            throw new \Exception( json_encode($manager) );
//            if ( !isset($m) ) {
//                $errors['assigned_managers'] = $manager['last_name'] . ', ' . $manager['first_name'] . ' is not found in our records';
//            } else if ( $m->active == 0 ) {
//                $errors['assigned_managers'] = $manager['last_name'] . ', ' . $manager['first_name'] . ' is inactive';
//            }
//        }
//
//        if ( !empty($errors) ) {
//            throw new \Exception( json_encode($errors) );
//        }
    }

    public function validateManagerProfile($managerProfile)
    {
        $errors = [];

        //todo

        if (!empty($errors)) {
            throw new  \Exception(json_encode($errors));
        }
    }

    public function validateAdministratorProfile($administratorProfile)
    {
        $errors = [];

//        // check to see if assigned divisions are valid
//        if ( isset($administrator['managerDriver']) && sizeof($administrator['managerDriver'] > 0) ) {
//            foreach ($administrator['managerDriver'] as $user_id => $driver) {
//                if (\App\DriverProfile::where('user_id', $user_id)->get()->count() < 1) {
//                    $errors['managerDriver'] = ['Invalid driver assignment. Driver could not be found, or managerDriver object incorrectly formatted. Please contact CarData IT Support'];
//                }
//            }
//        }

        if (isset($administratorProfile['company_coordinator']) && !empty($administratorProfile['company_coordinator_phone'])) {
            try {
                $this->validatePhoneNumber($administratorProfile['company_coordinator_phone']);
            } catch (\Exception $e) {
                $errors['company_coordinator_phone'] = ['Invalid coordinator phone number'];
            }
        }

        if (isset($administratorProfile['insurance_coordinator']) && !empty($administratorProfile['insurance_coordinator_phone'])) {
            try {
                $this->validatePhoneNumber($administratorProfile['insurance_coordinator_phone']);
            } catch (\Exception $e) {
                $errors['insurance_coordinator_phone'] = ['Invalid insurance coordinator phone number'];

            }
        }

        if (isset($administratorProfile['license_coordinator']) && !empty($administratorProfile['license_coordinator_phone'])) {
            try {
                $this->validatePhoneNumber($administratorProfile['license_coordinator_phone']);
            } catch (\Exception $e) {
                $errors['license_coordinator_phone'] = ['Invalid license coordinator phone number'];

            }
        }

        if (isset($administratorProfile['miroute_coordinator']) && !empty($administratorProfile['miroute_coordinator_phone'])) {
            try {
                $this->validatePhoneNumber($administratorProfile['miroute_coordinator_phone']);
            } catch (\Exception $e) {
                $errors['miroute_coordinator_phone'] = ['Invalid Mi-ROUTE coordinator phone number'];
            }
        }

        if (!empty($errors)) {
            throw new  \Exception(json_encode($errors));
        }
    }

    public function validateSuperProfile($superProfile)
    {
        $errors = [];

        // todo ?

        if (!empty($errors)) {
            throw new  \Exception(json_encode($errors));
        }
    }


    public function validateAddress($address)
    {
        $errors = [];

        // validate street
        if (!preg_match('/^[1-90-9]+ +[a-zA-Z0-9#\-\'\.,_() ]*$/', $address['street'])) {
            $errors['street'] = ['Invalid street'];
        }

        // validate street 2
        if (!empty($address['street2'])) {
            if (!preg_match('/^[a-zA-Z0-9#\-\'\.,_() ]*$/', $address['street2'])) {
                $errors['street2'] = ['Invalid street 2'];
            }
        }

        // validate city
        if (!preg_match('/^[a-zA-Z0-9] ?[a-zA-Z0-9 ,\-\'.*]*$/', $address['city'])) {
            $errors['city'] = ['Invalid city'];
        }

        if (!isset($address['country_id']) || $address['country_id'] < 1) {
            $errors['country_id'] = ['A country is required'];
        }

        // validate state/province
        if (!isset($address['state_province_id']) || $address['state_province_id'] < 1) {
            $errors['state_province_id'] = ['A ' . ($address['country_id'] == 2 ? 'province' : 'state') . ' is required'];
        }

        try {
            if (isset($address['state_province_id']) && isset($address['country_id'])) {
                $this->validateZipPostal($address['country_id'], $address['state_province_id'], $address['zip_postal']);
            }
        } catch (\Exception $e) {
            $errors = array_merge($errors, (array)json_decode($e->getMessage(), true));
        }

        // validate zip

        //else if can

        // validate province (if can)
        //validate postal code

        // if the errors object is non empty, throw an exception
        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    /**
     * In order to properly validate a zip/postal code we need to know the country and state/province
     *
     * @param $country_id
     * @param $state_province_id
     * @param $zip_postal_code
     * @return \App\PostalCode|\App\PostalCode[]|\App\ZipCode|\App\ZipCode[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function validateZipPostal($country_id, $state_province_id, $zip_postal)
    {
        $errors = [];

        if ($country_id == 1) {
            // USA
            if (!preg_match('/^\d{5}$/', $zip_postal)) {
                $errors['zip_postal'] = ['Invalid zip code'];
            } else {
                $zipCode = \App\ZipCode::find($zip_postal);
                if (empty($zipCode)) {
                    $errors['zip_postal'] = ['Zip Code not found'];
                } else if ($state_province_id != $zipCode->state->id) {
                    $errors['zip_postal'] = ['Invalid state/zip code combination'];
                }
            }

        } else {
            // CANADA
            if (!preg_match('/^([a-zA-Z]\d[a-zA-Z])\ {0,1}(\d[a-zA-Z]\d)$/', $zip_postal)) {
                $errors['zip_postal'] = ['Invalid postal code'];
            } else {
                $postalCode = \App\PostalCode::find(
                    strtoupper(preg_replace('/\s*/', '', $zip_postal))
                );
                if (empty($postalCode)) {
                    $errors['zip_postal'] = ['Postal Code not found'];
                } else if ($state_province_id != $postalCode->province->id) {
                    $errors['zip_postal'] = ['Invalid province/postal code combination'];
                }
            }
        }

        // if the errors object is non empty, throw an exception
        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }


    public function validateVehicleProfile($vehicleProfile)
    {
        $errors = [];

        // validate name_postfix
        if (empty($vehicleProfile['name_postfix'])) {
            $errors['vehicleProfile.name_postfix'] = ['Vehicle Postfix is a required field'];
        } else {
            // check for duplicate vehicle + post fix (this will result in the same name)
            $duplicate_name = \App\VehicleProfile::where('company_id', $vehicleProfile['company_id'])
                ->where('name_postfix', $vehicleProfile['name_postfix'])
                ->where('vehicle_id', $vehicleProfile['vehicle_id'])
                ->get()
                ->count() > 0 ? true : false;

            if ($duplicate_name) {

                $errors['vehicleProfile.name_postfix'] = ['Duplicate name postfix for the selected vehicle'];
            }
        }

        // validate vehicle_id
        if (empty($vehicleProfile['vehicle_id']) || !is_numeric($vehicleProfile['vehicle_id']) || $vehicleProfile['vehicle_id'] < 1) {
            $errors['vehicleProfile.vehicle_id'] = ['Please select a vehicle'];
        }

        // validate company_id
        if (empty($vehicleProfile['company_id']) || !is_numeric($vehicleProfile['company_id']) || $vehicleProfile['company_id'] < 1) {
            $errors['vehicleProfile.company_id'] = ['Please select a company'];
        }

        // validate vehicle year, it must be greater than 2006, and less than or equal to the current year
        $current_year = (int)date('Y');
        if (empty($vehicleProfile['vehicle_year']) || !is_numeric($vehicleProfile['vehicle_year']) || $vehicleProfile['vehicle_year'] < 2007 || $current_year < $vehicleProfile['vehicle_year']) {
            $errors['vehicleProfile.vehicle_year'] = ['Please select a vehicle year'];
        }

        // validate capital cost
        if (empty($vehicleProfile['capital_cost']) || !in_array($vehicleProfile['capital_cost'], ['msrp', 'invoice', 'blue_book'])) {
            $errors['vehicleProfile.capital_cost'] = ['Please select a capital cost'];
        }

        //TODO ASK JERZY ABOUT THIS MESS!!! WHERE THE HECK DO THE YEARS COME FROM
        // validate retention
        if (empty($vehicleProfile['retention']) || !is_numeric($vehicleProfile['retention']) || $vehicleProfile['retention'] < 2 || 6 < $vehicleProfile['retention']) {
            $errors['vehicleProfile.retention'] = ['Retention must be between 2 and 6'];
        }

        //TODO ASK JERZY ABOUT THE RESALE CONDITIONS, DO I NEED TO GO KBB TABLES??
        // need a function to check valid resale condition id?
        // validate resale_condition_id
        if (empty($vehicleProfile['resale_condition_id']) || !is_numeric($vehicleProfile['resale_condition_id']) || $vehicleProfile['resale_condition_id'] < 1 || 8 < $vehicleProfile['resale_condition_id']) {
            $errors['vehicleProfile.resale_condition_id'] = ['Please select a resale condition'];
        }

        //validate max_vehicle_age
        if (empty($vehicleProfile['max_vehicle_age']) || !is_numeric($vehicleProfile['max_vehicle_age']) || $vehicleProfile['max_vehicle_age'] < 0) {
            $errors['vehicleProfile.max_vehicle_age'] = ['Max vehicle age must be greater than or equal to 0'];
        }

        //validate business_use_percent
        if (empty($vehicleProfile['business_use_percent']) || !is_numeric($vehicleProfile['business_use_percent']) || $vehicleProfile['business_use_percent'] < 0) {
            $errors['vehicleProfile.business_use_percent'] = ['Business use percent must be greater than or equal to 0'];
        }

        //validate ALL relative adjustments
        foreach ($vehicleProfile as $key => $value) {

            // skip the absolute and the adj_text fields
            if (!preg_match('/\badj\b/', $key)
                || preg_match('/\bfixed_adj\b/', $key)
                || preg_match('/\bfixed_adj_text\b/', $key)
                || preg_match('/\bvariable_adj\b/', $key)
                || preg_match('/\bvariable_adj_text\b/', $key)) {
                continue;
            }

            if (empty($value) || !is_numeric($value) || $value < -100 || 100 < $value) {

                $error_key = 'vehicleProfile.' . $key;

                $errors[$error_key] = ['Relative adjustments must be between -100 and 100'];
            }

        }

        //validate ALL absolute adjustments
        //validate fixed_adj
        if (!is_numeric($vehicleProfile['fixed_adj']) || $vehicleProfile['fixed_adj'] < -10000 || 10000 < $vehicleProfile['fixed_adj']) {
            $errors['vehicleProfile.fixed_adj'] = ['Fixed adjustment must be between -10000 and 10000'];
        }

        //validate fixed_adj_text
        if (!empty($vehicleProfile['fixed_adj']) && $vehicleProfile['fixed_adj'] != 0) {
            if (!preg_match('/^[a-zA-Z ]?,*.*$/', $vehicleProfile['fixed_adj_text'])) {
                $errors['vehicleProfile.fixed_adj_text'] = ['A reason for the fixed adjustment is required'];
            }
        }

        //validate variable_adj
        if (!is_numeric($vehicleProfile['variable_adj']) || $vehicleProfile['variable_adj'] < -1000 || 1000 < $vehicleProfile['fixed_adj']) {
            $errors['vehicleProfile.variable_adj'] = ['Variable adjustment must be between -1000 and 1000'];
        }

        //validate variable_adj_text
        if (!empty($vehicleProfile['variable_adj']) && $vehicleProfile['variable_adj'] != 0) {
            if (!preg_match('/^[a-zA-Z ]?,*.*$/', $vehicleProfile['variable_adj_text'])) {
                $errors['vehicleProfile.variable_adj_text'] = ['A reason for the variable adjustment is required'];
            }
        }

        // If calculate_fixed or calculate_variable is false/unselected, we require preset_fixed or preset_variable
        // validate preset_fixed
        if (empty($vehicleProfile['calculate_fixed'])) {
            if (!is_numeric($vehicleProfile['preset_fixed']) || $vehicleProfile['preset_fixed'] < 0) {
                $errors['vehicleProfile.preset_fixed'] = ['Preset fixed must be greater than or equal to 0 when not using a calculated fixed reimbursement amount'];
            }
        }

        // validate preset_variable
        if (empty($vehicleProfile['calculate_variable'])) {
            if (!is_numeric($vehicleProfile['preset_variable']) || $vehicleProfile['preset_variable'] < 0) {
                $errors['vehicleProfile.preset_variable'] = ['Preset variable must be greater than or equal to 0 when not using a calculated variable reimbursement amount'];
            }
        }

        // if the errors object is non empty, throw an exception
        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }

    }


    public function validateBank($bank)
    {
        $errors = [];

        // validation for both countries
        if ($bank['country_id'] != 1 && $bank['country_id'] != 2) {
            $errors['country_id'] = ['Select a country'];
        }

        if (!preg_match('/^[a-zA-Z0-9 ]{1,23}$/', $bank['bank_name'])) {
            $errors['bank_name'] = ['The bank name must be 1-23 alphanumeric characters'];
        }

        if ($bank['status'] != 0 && $bank['status'] != 1) {
            $errors['status'] = ['Please select a status'];
        }

        // US Only Validation
        if ($bank['country_id'] == 1) {

            // validate immediate destination
            // should be 9 digits. In the ach it is a blank space followed by 9 numbers
            if (!preg_match('/^\d{9}$/', $bank['immediate_destination'])) {
                $errors['immediate_destination'] = ['The immediate destination must be a 9 digit number'];
            }

            // validate immediate destination
            // should be 9 digits. In the ach it is a blank space followed by 9 numbers
            if (!preg_match('/^\d{9,10}$/', $bank['immediate_origin'])) {
                $errors['immediate_origin'] = ['The immediate origin must be a 9-10 digit number'];
            }

            // validate immediate_origin_name
            // should be 9 digits. In the ach it is a blank space followed by 9 numbers
            if (!preg_match('/^[a-zA-Z0-9 ]{1,23}$/', $bank['immediate_origin_name'])) {
                $errors['immediate_origin_name'] = ['The immediate origin name must be 1-23 alphanumeric characters'];
            }

            // validate company_id
            // should be 10 digits.
            if (!preg_match('/^[a-zA-Z0-9]{1,23}$/', $bank['company_id'])) {
                $errors['company_id'] = ['The company identification must be a 10 digit number'];
            }

            // should be 1-16 alphanumeric characters.
            if (!preg_match('/^[a-zA-Z0-9 ]{1,23}$/', $bank['company_name'])) {
                $errors['company_name'] = ['The company name must be 1-23 alphanumeric characters'];
            }

            // validate originating_dfi
            // should be 8 digits
            if (!preg_match('/^\d{8}$/', $bank['originating_dfi'])) {
                $errors['originating_dfi'] = ['The originating DFI must be an 8 digit number'];
            }

            // Canada Specific Validation
        } else if ($bank['country_id'] == 2) {

            // validate originator_id
            // should be 10 alphanumeric characters.
            if (!preg_match('/^[a-zA-Z0-9]{10}$/', $bank['originator_id'])) {
                $errors['originator_id'] = ['The originator ID must be 10 alphanumeric characters'];
            }

            // validate originator_name
            // NOTE on some docs this is referred to as the originator's short name
            if (!preg_match('/^[a-zA-Z0-9]{1,15}$/', $bank['originator_name'])) {
                $errors['originator_name'] = ['The originator name must be 1-15 alphanumeric characters'];
            }

            // validate transit_number
            // 3 to 5 digits
            if (!preg_match('/^\d{3,5}$/', $bank['transit_number'])) {
                $errors['transit_number'] = ['The branch/transit number must be 3-5 digits'];
            }

            // validate account_number
            // NOTE on some docs this is referred to as the Drawee Account #
            if (!preg_match('/^\d{1,12}$/', $bank['account_number'])) {
                $errors['account_number'] = ['The account number must be 1-12 digits'];
            }
        }


        // if the errors object is non empty, throw an exception
        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    public function validateNotification($notification)
    {
        $errors = [];

        if (empty($notification['area'])) {
            $errors['area'] = ['Area is required'];
        }

        if (!in_array($notification['severity'], ['low', 'medium', 'high'])) {
            $errors['severity'] = ['Severity is required'];
        }

        if (empty($notification['title'])) {
            $errors['title'] = ['Title is required'];
        } else if (strlen($notification['title']) > 255) {
            $errors['title'] = ['Title can be at most 255 characters'];
        }

        if (empty($notification['content'])) {
            $errors['content'] = ['Content is required'];
        } else if (strlen($notification['content']) > 255) {
            $errors['content'] = ['Content can be at most 255 characters'];
        }

        if (array_key_exists('note', $notification)) {
            if (strlen($notification['note']) > 50) {
                $errors['note'] = ['Note can be at most 50 characters'];
            }
        }

        // if the errors object is non empty, throw an exception
        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    public function validateNote($note)
    {
        $errors = [];

        if (empty($note['category_id'])) {
            $errors['category_id'] = ['A category id is required'];
        }

        if (empty($note['title'])) {
            $errors['title'] = ['Title is required'];
        } else if (strlen($note['title']) > 255) {
            $errors['title'] = ['Title can be at most 255 characters'];
        }

        if (empty($note['content'])) {
            $errors['content'] = ['Content is required'];
        } else if (strlen($note['content']) > 255) {
            $errors['content'] = ['Content can be at most 255 characters'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    /**
     * Validates email against what is in the users table.
     *
     * Defaults to validate against business not personal.
     *
     * If you want to validate both, supply type as 'both'
     *
     * @param $email
     * @param $type - defualt is 'business'. Options are 'business', 'personal', 'both'
     * @throws \Exception
     */
    public function validateEmail($email, $type = 'business', $user_id = -1)
    {
        $errors = [];

        // check against php email standard
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $errors['email'] = ['Invalid email'];
            throw new \Exception(json_encode($errors));
        }

        // check if the email is in our database
        switch ($type) {
            case 'business':
                if (\App\User::where('email', $email)
                        ->where('id', '!=', $user_id)
                        ->get()
                        ->count() > 0) {
                    $errors['email'] = ['There is already a user with this email in the system'];
                }
                break;
            case 'personal':
                if (\App\User::where('personal_email', $email)
                        ->where('id', '!=', $user_id)
                        ->get()
                        ->count() > 0) {
                    $errors['email'] = ['There is already a user with this personal email in the system'];
                }
                break;
            case 'both':
                if (\App\User::where('personal_email', $email)
                        ->where('id', '!=', $user_id)
                        ->get()
                        ->count() > 0) {
                    $errors['email'] = ['There is already a user with this personal email in the system'];
                }
                if (\App\User::where('email', $email)
                        ->where('id', '!=', $user_id)
                        ->get()
                        ->count() > 0) {
                    $errors['email'] = ['There is already a user with this email in the system'];
                }
                break;
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }


    /**
     * This one function calls many other of the fucntions in this file to validate a user object when it is being
     * created or updated.
     *
     * It will try each validator, catch any exceptions, and merge errors into an overall errors array.
     * If any errors are present, it will throw an exception with those errors json encoded.
     *
     * @param $user
     */
    public function validateCreateOrUpdateUser(Request $request)
    {
        // first validate the user, and then the other profiles based on if they are present
        $errors = [];

        // check a user type is being assigned
        if (!$request->is_driver
            && !$request->is_manager
            && !$request->is_administrator
            && !$request->is_super
        ) {
            $errors['profile_types_errors'] = ['At least one profile type must be selected'];
        }

        // format the data appropriately for validation

        $u = [
            'id' => isset($request->id) ? $request->id : null,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'personal_email' => $request->personal_email,
        ];

        // todo I think this is unnecessary?
//        $driverProfile = $request->driver['driverProfile'];
//        $driverProfile['territory_list'] = array_keys($driverProfile['territory_list']);

        try {
            $this->validateUser($u);
        } catch (\Exception $e) {

            //NOTE json decode won't produce a true php array, hence the forced type conversion

            $errors = array_merge($errors, (array)json_decode($e->getMessage(), true));
        }

        if ($request->is_driver) {

            try {
                $this->validateAddress($request->driver['address']);
            } catch (\Exception $e) {
                $errors = array_merge($errors, (array)json_decode($e->getMessage(), true));
            }

            try {
                $this->validateDriverProfile($request->driver['driverProfile']);
            } catch (\Exception $e) {
                $errors = array_merge($errors, (array)json_decode($e->getMessage(), true));
            }

        }

        if ($request->is_manager) {
            $this->validateManagerProfile($request->manager['managerProfile']);
        }

        if ($request->is_administrator) {
            $this->validateAdministratorProfile($request->administrator['administratorProfile']);
        }

        if ($request->is_super) {
            $this->validateSuperProfile($request->super['superProfile']);
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }

    }


    public function validatePhoneNumber($phone_number)
    {
        $errors = [];

        if (!preg_match('/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/', $phone_number)) {
            $errors['phone_number'] = ['Invalid phone number'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    public function validateUpdateProrateReimbursementRequest(Request $request)
    {

        $errors = [];
        $prorates = $request->prorate;

        foreach ($prorates as $prorate) {

            $user_id = $prorate['user_id'];
            $user = User::find($user_id);

            if ($prorate['fix_amount'] !== null) {
                $val = trim($prorate['fix_amount']);
                if (!empty($val) && !preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $val)) {
                    $errors[$user_id . '_f'] = ["Invalid fixed override [" . $val . "] for " . $user->last_name . ' ' . $user->first_name];
                } elseif ($prorate['fix_amount'] > 9999.99) {
                    $errors[$user_id . '_f'] = ["Fixed override [" . $val . "] for " . $user->last_name . ' ' . $user->first_name] . " cannot be greater than 9999.99";
                }
            }
            if ($prorate['var_amount'] !== null) {
                $val = trim($prorate['var_amount']);
                if (!empty($val) && !preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $val)) {
                    $errors[$user_id . '_v'] = ["Invalid variable override [" . $val . "] for " . $user->last_name . ' ' . $user->first_name];
                } else {
                    if ($prorate['business_mileage'] == 0) {
                        $errors[$user_id . '_z'] = ["Cannot calculate new c/m for " . $user->last_name . ' ' . $user->first_name . ' (no mileage)'];
                    }
                }
            }
        }
        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }


    public function validateCreateCompany($company)
    {
        $errors = [];

        if (empty($company['name'])) {
            $errors['name'] = ['A name is required'];
        } else if (strlen($company['name']) < 1 || 254 < strlen($company['name'])) {
            $errors['name'] = ['Company name must be between 1 and 255 characters'];
        }

        if (empty($company['country_id']) || ($company['country_id'] != 1 && $company['country_id'] != 2)) {
            $errors['country_id'] = ['A country is required'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }

    }


    public function validateAch($achData)
    {
        $errors = [];

        $known = [];
        foreach ($achData['drivers'] as $driver) {
            // check distinct
            if (in_array($driver['id'], $known)) {
                $errors['duplicate'] = ['Driver id ' . $driver['id'] . ' is duplicated'];
            }
            $known[] = $driver['id'];

            // validate payment
            if ($driver['payment'] <= 0 || 9999.99 < $driver['payment']) {
                $errors[$driver['id']] = ['Payment must be greater than 0 and less than 10,000'];
            }
        }


        if (!isset($achData['paymentProperties']['effective_entry_date']) || !(bool)strtotime($achData['paymentProperties']['effective_entry_date'])) {
            $errors['effective_entry_date'] = ['Invalid effective entry date'];
        }

        if (!isset($achData['paymentProperties']['file_id_modifier']) || !preg_match('/[A-Z]/', $achData['paymentProperties']['file_id_modifier'])) {
            $errors['file_id_modifier'] = ['Invalid file id modifier'];
        }

        if (!isset($achData['paymentProperties']['type'])
            || !in_array($achData['paymentProperties']['type'], ['payment', 'prenote'])) {
            $errors['type'] = ['Invalid type'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }

    }

    public function validateEft($eftData)
    {

        $errors = [];

        $known = [];
        foreach ($eftData['drivers'] as $driver) {
            // check distinct
            if (in_array($driver['id'], $known)) {
                $errors['duplicate'] = ['Driver id ' . $driver['id'] . ' is duplicated'];
            }
            $known[] = $driver['id'];

            // validate payment
            if ($driver['payment'] <= 0 || 9999.99 < $driver['payment']) {
                $errors[$driver['id']] = ['Payment must be greater than 0 and less than 10,000'];
            }

            if (empty($driver['first_name'])) {
                $errors[$driver['id']] = ['First name is required'];
            }
            if (empty($driver['last_name'])) {
                $errors[$driver['id']] = ['Last name is required'];
            }
            if (empty($driver['username'])) {
                $errors[$driver['id']] = ['Username is required'];
            }
        }


        if (!isset($eftData['paymentProperties']['effective_entry_date']) || !(bool)strtotime($eftData['paymentProperties']['effective_entry_date'])) {
            $errors['effective_entry_date'] = ['Invalid effective entry date'];
        }

        if (!isset($eftData['paymentProperties']['file_creation_number']) || !preg_match('/[1-9]*/', $eftData['paymentProperties']['file_id_modifier'])) {
            $errors['file_creation_number'] = ['Invalid file id modifier'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }

    }

    public function validateCompanyVehicles($company_id, $vehiclesData)
    {
        // TODO: really nothing to validate, deprecated
    }

    /**
     * Validates company invoicing.
     *
     *
     * @param $company_id
     * @param $invoicingData :
     *  - company_id
     *  - invoicing_interval
     *  - per_driver_fee
     *  - from_date: date current fee was applied
     *
     * @throws \Exception
     */
    public function validateCompanyInvoicing($company_id, $invoicingData)
    {
        $errors = [];

        $currentInvoicing = CompanyInvoicing::where('company_id', $company_id)->first();

        if (!in_array($invoicingData['invoicing_interval'], ['Monthly', 'Annually'])) {
            $errors['invoicing_interval'] = ['Invalid invoicing interval'];
        }

        if ($invoicingData['per_driver_fee'] != $currentInvoicing->per_driver_fee
            && (empty($invoicingData['per_driver_fee']) || $invoicingData['per_driver_fee'] <= 0)) {
            $errors['per_driving_fee'] = ['Fee must be a positive number'];
        }

        if (empty($invoicingData['from_date']) && $invoicingData['per_driver_fee'] > 0) {
            $errors['invoice_fee_starting'][] = 'Starting date must be supplied';
        }

        $currentFee = CompanyFee::where('company_id', $company_id)
            ->where('fee', 'invoice')
            ->where('to_date', null)
            ->first();
        if ((!empty($currentFee) && $invoicingData['from_date'] == $currentFee->from_date && $invoicingData['per_driver_fee'] != $currentFee->amount)
            || (empty($currentFee) && $invoicingData['per_driver_fee'] > 0 && $invoicingData['from_date'] == '')) {
            $errors['invoice_fee_starting'] = ['You must supply a new valid starting date'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    public function validateCompanyAdministration($company_id, $administrationData) {
        $errors = [];

        foreach ($administrationData as $k=>$v) {
            if (in_array($k, ['management', 'driver_calls_and_emails', 'driver_changes', 'account_maintenance', 'monthly_report_review', 'traffic_light_status']) && !in_array($v, ['CarData', 'Client'])) {
                $errors[$k] = ['No administration selected'];
            }
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    public function validateCompanyPayments($company_id, $paymentsData)
    {
        $errors = [];

        // get current settings
        $currentPayments = CompanyPayments::where('company_id', $company_id)->first();


        if (!in_array($paymentsData['payment_responsibility'], ['CarData', 'Client'])) {
            $errors['payment_responsibility'] = ['Invalid payment responsibility'];
        }

        // when the responsibility is cardata validate the bank
        if ($paymentsData['payment_responsibility'] == 'CarData') {

            // a bank is required
            $bank = Bank::find($paymentsData['processingBank']['id']);
            if (empty($bank)) {
                $errors['processing_bank'] = ['Invalid Processing Bank'];
            }

        }

        if ($paymentsData['direct_pay_driver_fee'] != $currentPayments->direct_pay_driver_fee
            && (empty($paymentsData['direct_pay_driver_fee']) || $paymentsData['direct_pay_driver_fee'] <= 0)) {
            $errors['direct_pay_driver_fee'] = ['Fee must be a positive number'];
        }

        if ($paymentsData['direct_pay_file_fee'] != $currentPayments->direct_pay_file_fee
            && (empty($paymentsData['direct_pay_file_fee']) || $paymentsData['direct_pay_file_fee'] <= 0)) {
            $errors['direct_pay_file_fee'] = ['Fee must be a positive number'];
        }

        if (empty($paymentsData['driver_from_date']) && $paymentsData['direct_pay_driver_fee'] > 0) {
            $errors['driver_fee_starting'][] = 'Starting date must be supplied';
        }

        if (empty($paymentsData['file_from_date']) && $paymentsData['direct_pay_file_fee'] > 0) {
            $errors['file_fee_starting'][] = 'Starting date must be supplied';
        }

        $currentDriverFee = CompanyFee::where('company_id', $company_id)
            ->where('to_date', null)
            ->where('fee', 'driver')
            ->first();
        if ((!empty($currentDriverFee) && $paymentsData['driver_from_date'] == $currentDriverFee->from_date && $paymentsData['direct_pay_driver_fee'] != $currentDriverFee->amount)
            || (empty($currentDriverFee) && $paymentsData['direct_pay_driver_fee'] > 0 && $paymentsData['driver_from_date'] == '')) {
            $errors['driver_fee_starting'] = ['You must supply a new valid starting date'];
        }

        $currentFileFee = CompanyFee::where('company_id', $company_id)
            ->where('to_date', null)
            ->where('fee', 'file')
            ->first();
        if ((!empty($currentFileFee) && $paymentsData['file_from_date'] == $currentFileFee->from_date && $paymentsData['direct_pay_file_fee'] != $currentFileFee->amount)
            || (empty($currentFileFee) && $paymentsData['direct_pay_file_fee'] > 0 && $paymentsData['file_from_date'] == '')) {
            $errors['file_fee_starting'] = ['You must supply a new valid starting date'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    /**
     * Validate a new tax quarter definition for a given company.
     * The tax quarters passed in should be an array of the following format:
     *  [
     *      'q1' => month_id,
     *      'q2' => month_id,
     *      .
     *      .
     *      .
     *  ]
     */
    public function validateQuarters($quarters, $company_id)
    {
        $errors = [];

        /**
         * companies can define their quarters however they please.
         * All we check is that each month is between 1 and 12 inclusive, there are no duplicates,
         * and that each value is numeric
         */
        foreach ($quarters as $key => $value) {
            if (!is_numeric($value) || $value < 1 || 12 < $value) {
                $errors[$key] = ['Invalid month selection'];
            }

            // duplicate check
            foreach ($quarters as $key2 => $value2) {
                // skip the same entry
                if ($key2 == $key || !empty($errors[$key2]) || !empty($errors[$key])) {
                    continue;
                } else if ($value2 == $value) {
                    // we found a duplicate value
                    $errors[$key2] = ['Q' . $key2 . ' duplicates Q' . $key];
                }
            }
        }

        /**
         * Next we must check that the quarter definition does not duplicate the currently active definition
         */
        $previousQuarterDefinition = \DB::table('company_tax_quarters')
            ->join('tax_quarters', 'company_tax_quarters.tax_quarter_id', '=', 'tax_quarters.id')
            ->where('company_id', $company_id)
            ->whereRaw("(company_tax_quarters.valid_until_year > '" . date('Y') . "' or company_tax_quarters.valid_until_year is NULL)")
            ->first();

        if (!empty($previousQuarterDefinition)
            && $previousQuarterDefinition->q1 == $quarters[1]
            && $previousQuarterDefinition->q2 == $quarters[2]
            && $previousQuarterDefinition->q3 == $quarters[3]
            && $previousQuarterDefinition->q4 == $quarters[4]) {
            // we have a duplicate definition
            $errors['duplicate'] = ['Duplicate definition. We cannot add the same active definition twice.'];
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }


    public function validateExcelGenerationData($data)
    {
        $errors = [];

        // validate category headings if applicable
        if (isset($data['categoryHeadings'])) {

        }

        // validate column headings
        if (isset($data['columnHeadings'])) {

        } else {
            $errors['columnHeadings'] = ['The property columnHeadings must be supplied in the request'];
        }


        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

}
