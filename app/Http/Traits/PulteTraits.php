<?php namespace App\Http\Traits;

use App\Address;
use App\Calculators\StreetLightCalculator;
use App\Division;
use App\DriverProfile;
use App\ManagerProfile;
use App\MileageBand;
use App\MonthlyMileage;
use App\MonthlyReimbursement;
use App\StateProvince;
use App\User;
use App\VehicleProfile;
use App\ZipCode;


trait PulteTraits
{
    public function scheduleToDeleteMissingDriver($user_id, $manager_id, $driver_name, $logger)
    {
        $deleted = 0;
        $driver_profile = DriverProfile::where('user_id',$user_id)->first();
        if ($driver_profile) {      // in case it was already soft deleted
            $c = new StreetLightCalculator();
            $color = $c->colorOfStreetLight($driver_profile->street_light_id);
            if( $color == 'Green' || $color == 'Yellow' || $color == 'Red' && ($driver_profile->street_light_id & RED_TERMINATED) == 0 ) {

                list($yyyy,$mm) = explode('-',date("Y-m"));
                if( $mm == 12) { $mm = 1; $yyyy++; }
                else { $mm++; }

                $scheduled_date = sprintf("%04d-%02d-10", $yyyy, $mm);
                \DB::table('scheduled_terminations')->insert([
                    'user_id' => $user_id,
                    'scheduled_date' => $scheduled_date,
                    'stop_date' => date("Y-m-d")
                ]);
                $deleted = 1;
            }
        }
        return $deleted;
    }


    public function pulteRecordColumns() {
        return ['last_name','first_name','email','employee_number','job_title','street','city','state_province',
            'zip_postal','country', 'market','area','vehicle_profile','annual_mileage', 'territory', 'manager_id',
            'manager_last_name', 'manager_first_name', 'manager_email', 'stop_date', 'status', 'start_date',
            'rehire_date' ];
    }

    public function generatePulteRecord($row, $columns){
        $arr = [];
        foreach( $row as $i => $token) {
            $arr[$columns[$i]] = trim($token);
        }
        return $arr;
    }

    public function pulteValidateRecord($record, $company_id, $vehicleProfiles, $mileageBandNames, $divisionNames, $logger) {

        $err_arr = [];
        $alert_arr = [];

        \Debugbar::info('Validation started ...');
        // $logger->addInfo('Validation', ['status' => "Started"]);

        try {

            if (empty($record->last_name)) {
                $err_arr[] = "Driver Last Name is missing";
            }
            if (empty($record->last_name)) {
                $err_arr[] = "Driver First Name is missing";
            }
            if (empty($record->email)) {
                $err_arr[] = "Driver Email is missing for driver $record->last_name $record->first_name";
            } else {
                if (!preg_match("/^.+@.+\..+$/", $record->email)) {
                    $err_arr[] = "Driver Email $record->email is invalid";
                }
            }

            \Debugbar::info('After last name ...');

            if (empty($record->street)) {
                $err_arr[] = "Driver Street Address is missing";
            }
            if (empty($record->city)) {
                $err_arr[] = "Driver City Address is missing";
            }
            if (empty($record->zip_postal)) {
                $err_arr[] = "Driver Zip Address is missing";
            }


            if (empty($record->state_province)) {
                $err_arr[] = "Driver State Address is missing";
            } else if (!StateProvince::where('short_name', $record->state_province)) {
                $err_arr[] = "Invalid State: $record->state_province abbreviation";
            }

            \Debugbar::info('After address mandatory fields ...');

            if (empty($record->country)) {
                $err_arr[] = "Driver Country Address is missing";
            } else if ($record->country == 'US' && !empty($record->state_province) && !empty($record->zip_postal)) {
                $padded_zip = str_pad($record->zip_postal, 5, '0', STR_PAD_LEFT);
                // \Debugbar::info('$padded_zip', $padded_zip);
                $zip = ZipCode::find($padded_zip);
                if (!$zip) {
                    $err_arr[] = "Unknown Driver Zip Adddress: $record->zip_postal";
                } else if ($zip->state->short_name != $record->state_province) {
                    $err_arr[] = "Driver Zip Address $record->zip_postal was assigned to state $record->state_province instead of " . $zip->state->short_name;
                }
            } else if ($record->country == 'CA' && !empty($record->state_province) && !empty($record->zip_postal)) {
                $zip = PostalCode::find($record->zip_province);
                if (!$zip) {
                    $err_arr[] = "Unknown Driver Postal Adddress: $record->zip_postal";
                } else if ($zip->state->short_name != $record->state_province) {
                    $err_arr[] = "Driver Postal Address $record->zip_postal was assigned to province $record->state_province instead of " . $zip->state->short_name;
                }
            }
            if ($record->country != 'US' && $record->country != 'CA') {
                $err_arr[] = "Invalid Driver Country Address: $record->country";
            }

            if (empty($record->market)) {
                $err_arr[] = "Market Name is missing";
            }

            \Debugbar::info('After market ...');

            if (empty($record->vehicle_profile)) {
                $err_arr[] = "Vehicle Profile is missing";
            } else if (in_array($record->vehicle_profile, $vehicleProfiles) === false) {
                $err_arr[] = "Invalid Vehicle Profile Postfix: $record->vehicle_profile";
            }

            if (empty($record->annual_mileage)) {
                $err_arr[] = "Mileage Band is missing";
            } else if (!in_array($record->annual_mileage, $mileageBandNames)) {
                $err_arr[] = "Invalid Mileage Band: $record->annual_mileage";
            }

            if (empty($record->territory)) {
                $err_arr[] = "Territory is missing";
            } else if ($record->territory != 'Home Address City') {
                $err_arr[] = "Invalid Territory type: $record->territory, Only Home Address City is implemented";
            }

            if (empty($record->manager_id)) {
                $err_arr[] = "Manager ID is missing";
            }
            if (empty($record->manager_last_name)) {
                $err_arr[] = "Manager Last Name is missing";
            }
            if (empty($record->manager_first_name)) {
                $err_arr[] = "Manager First Name is missing";
            }
            if (empty($record->manager_email)) {
                $err_arr[] = "Manager Email is missing";
            } else if (!preg_match("/^.+@.+\..+$/", $record->manager_email)) {
                $err_arr[] = "Manager Email: $record->email is invalid";
            }

            \Debugbar::info('After manager fields ...');

            // check if we have such user related to driver and if names match
            if (!empty($record->email) && !empty($record->last_name) && !empty($record->first_name)) {
                $data = $this->get_user_from_email($company_id, $record->email);
                \Debugbar::info('After manager get_user_from_email data=', $data);
                if ($data && ($data->first_name != $record->first_name || $data->last_name != $record->last_name)) {
                    $err_arr[] = "Record found with driver email, but first or last name do not match";
                }
                if (empty($data->driver_id) && $record->status == 'T') {
                    $err_arr[] = "Cannot terminate driver that doesn't exist in the database: Employee ID: $record->employee_number";
                }
            }

            // check if we have such user related to manager
            if (!empty($record->manager_email) && !empty($record->manager_last_name) && !empty($record->manger_first_name)) {
                $data = $this->get_user_from_email($company_id, $record->manager_email);
                if ($data && ($data->manager_first_name != $record->manager_first_name || $data->manager_last_name != $record->manager_last_name)) {
                    $err_arr[] = "Record found with manager email, but first or last name do not match";
                }
            }

            if ($record->status == 'T') {
                if (empty($record->stop_date)) {
                    $err_arr[] = "Term Date cannot be empty for terminated drivers.";
                } else {
                    $termDate = strtotime($record->stop_date);
                    if ($termDate > time()) {
                        $err_arr[] = "Term Date cannot be in the future: $record->stop_date";
                    }
                }
            }
            \Debugbar::info('Verification Ended ...',$err_arr);

        } catch ( Exception $e ) {
            \Debugbar::error( "Error:  File:" . $e->getFile() . " Line: " . $e->getLine() .' : '. $e->getMessage() );
        }

        return $err_arr;

    }

    public function get_user_from_email($company_id, $email){
        $sql_email = str_replace("'","\'",$email);
        $sql = <<<EOQ
select u.id AS user_id, u.first_name, u.last_name, u.active, dp.id, mp.manager_id, ap.administrator_id 
from users as u
  left join driver_profiles dp ON u.id = dp.id AND dp.company_id = $company_id and dp.deleted_at is null
  left join manager_profiles mp ON u.id = mp.user_id AND mp.company_id = $company_id and mp.deleted_at is null
  left join administrator_profiles ap ON u.id = ap.user_id AND ap.company_id = $company_id and ap.deleted_at is null
  where u.email = '$sql_email'
EOQ;
        \Debugbar::info($sql);
        $data = \DB::select(\DB::raw($sql));
        \Debugbar::info('get_user_from_email:',$sql,$data);
        return $data ? $data[0] : null ;

    }

    public function getVehicleProfileNames($company_id){

        $vp_arr = [];
        $vps = VehicleProfile::where('company_id', $company_id)->get();
        foreach( $vps as $v) {
            $vp_arr[$v->id] = trim($v->name_postfix);
        }
        return $vp_arr;
    }

    public function getMileageBandsNames(){
        $mbn_arr = [];
        $mbn = MileageBand::all();
        foreach( $mbn as $n) {
            if( $n->mileage_start != 50000) {
                $mbn_arr[$n->id] = $n->mileage_start . ' - ' . $n->mileage_end;
            } else {
                $mbn_arr[$n->id] = '> ' . $n->mileage_start;
            }
        }
        return $mbn_arr;
    }

    public function getDivisionNames($company_id){
        $div_arr = [];
        $div = Division::where('company_id', $company_id)->get();
        foreach( $div as $d){
            $div_arr[$d->id] = trim($d->name);
        }
        return $div_arr;
    }

    public function getStateProvinces(){
        $state_arr = [];
        $states = StateProvince::all();
        foreach( $states as $s){
            $state_arr[$s->id] = $s->short_name;
        }
        return $state_arr;
    }

    public function add_new_driver($company_id, $divisionNames, $stateProvincesNames,
                                    $vehicleProfileNames, $mileageBandNames, $record) {

        $alerts = null;
        $driverProfile = null;

        // checking if we have user record from manager or administrator
        $user_data = $this->get_user_from_email($company_id, $record->email);
        \Debugbar::info('$user_data',$user_data);

        $if_fail_delete = 0; // store what should be deleted if fail for any reason

        //////////////////////////////////////////////////////////////////////
        try {
            if (!$user_data) {
                \Debugbar::info("We have to create a new user record");
                $driverUser = User::create([
                    'username' => app('\App\Http\Controllers\UserController')->generateUsername($record->first_name, $record->last_name),
                    'first_name' => $record->first_name,
                    'last_name' => $record->last_name,
                    'email' => $record->email,
                    'password' => \Hash::make('secret'), // TODO replace this make('secret')
                ]);
                $user_id = $driverUser->id;

                $if_fail_delete = 1;
                \Debugbar::info("New user record was created with id=$user_id");
            } else {
                \Debugbar::info("User id $user_data->id was found based on his email");
                $user_id = $user_data->user_id;
            }

            $fullAddress = $record->street . ' ' . $record->city . ', ' . $record->state_province;
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($fullAddress) . "&key=" . config('app.google_key');
            $geocode = file_get_contents($url);
            $output = json_decode($geocode);
            $latitude = $output->results[0]->geometry->location->lat;
            $longitude = $output->results[0]->geometry->location->lng;

            \Debugbar::info("lat lng for $fullAddress", $latitude,$longitude);

            $state_province_id = array_search(trim($record->state_province),$stateProvincesNames);
            $driverAddress = Address::create([
                'user_id' => $user_id,
                'street' => $record->street,
                'city' => $record->city,
                'zip_postal' => $record->zip_postal,
                'state_province_id' => $state_province_id,
                'map_latitude' => $latitude,
                'map_longitude' => $longitude,
                'country_id' => $record->country == 'US' ? 1 : 2
            ]);
            $if_fail_delete = +2;
            $address_id = $driverAddress->id;
            \Debugbar::info('Address Created:', $driverAddress);

            $division_id = array_search(trim($record->market), $divisionNames);
            $vehicle_profile_id = array_search($record->vehicle_profile, $vehicleProfileNames);
            $mileage_band_id = array_search($record->annual_mileage, $mileageBandNames);

            $step = 4;
            $driverProfile = DriverProfile::create([
                'user_id' => $user_id,
                'active' => 1,
                'company_id' => $company_id,
                'division_id' => $division_id,
                'street_light_id' => 1,
                'employee_number' => $record->employee_number,
                'start_time' => date("Y-m-01"),
                'job_title' => $record->job_title,
                'address_id' => $address_id,
                'vehicle_profile_id' => $vehicle_profile_id,
                'cost_center' => $record->area,
                'mileage_band_id' => $mileage_band_id,
                'territory_type_id' => 1,
                'car_policy_accepted' => 0,
                'reimbursement_detail_id' => null
            ]);
            \Debugbar::info('Driver Profile created:',$driverProfile);
            $driver_id = $driverProfile->id;
            $if_fail_delete = +4;

            // Add monthly records in reimbursement and mileage for this year
            $year = date("Y");
            for( $m=1; $m<=12; $m++ ) {
                MonthlyMileage::create(['user_id' => $user_id, 'company_id' => $company_id, 'year' => $year, 'month' => $m ]);
                MonthlyReimbursement::create(['user_id' => $user_id, 'driver_profile_id' => $driver_id, 'company_id' => $company_id, 'year' => $year, 'month' => $m ]);
            }



        } catch (\Exception $e) {
            \Debugbar::error('line:' . $e->getLine() . " error: " . $e->getMessage());
            if ($if_fail_delete & 4) {
                \Debugbar::error('Could not create Driver Profile');
                $alerts[] = "Could not create Driver Profile for record->last_name $record-first_name. Skipped";
                if(isset($driver_id)) {
                    DriverProfile::find($driver_id)->delete();
                }
            }
            if ($if_fail_delete & 2) {
                \Debugbar::error('Could not create Address');
                $alerts[] = "Could not create Address for record->last_name $record->first_name. Skipped";
                if( isset($address_id)) {
                    Address::find($address_id)->delete();
                }
            }
            if ($if_fail_delete & 1) {
                \Debugbar::error('Could not create User');
                $alerts[] = "Could not create User for record->last_name $record->first_name. Skipped";
                if( isset($user_id)) {
                    User::find($user_id)->delete();
                }
            }
        }

        \Debugbar::error($alerts);

        return (object)['driver' => $driverProfile, 'errors' => $alerts];
    }

    public function updatePulteDriver($user_id, $manager_id, $divisionNames, $stateProvinceNames, $driverProfileNames,
                                       $mileageBandNames, $record) {
        // find differences
        $change_found = false;
        $action_taken = null;

        // prepare new profile if changes found
        $old_profile = DriverProfile::where('user_id', $user_id)->first();
        $new_profile = $old_profile->replicate();

        //\Debugbar::info("Old profile:", $old_profile);
        //\Debugbar::info("Old profile address", $old_profile->address());

        $new_profile->reimbursement_detail_id = null;   // be sure that we are not passing previous reimbursement details

        if( $divisionNames[$old_profile->division_id] != $record->market ) {
            $change_found = true;
            $new_profile->division_id = array_search($record->market, $divisionNames);
        }

        if( $mileageBandNames[$old_profile->mileage_band_id] != $record->annual_mileage) {
            $change_found = true;
            $new_profile->mileage_band_id = array_search($record->annual_mileage,$mileageBandNames);
        }

        if( $driverProfileNames[$old_profile->vehicle_profile_id] != $record->vehicle_profile) {
            $change_found = true;
            $new_profile->vehicle_profile_id = array_search($record->vehicle_profile,$driverProfileNames);
        }

        if( $old_profile->employee_number != $record->employee_number) {
            $change_found = true;
            $new_profile->employee_number = $record->employee_number;
        }

        if( $old_profile->job_title != $record->job_title) {
            $change_found = true;
            $new_profile->job_title = $record->job_title;
        }

        if( $record->stop_date && $old_profile->stop_date != $record->stop_date) {
            $change_found = true;
            $new_profile->stop_date = $record->stop_date;
        }

        // we have only one territory = 1, so do not change it. Also territory list is null

        $old_address = $old_profile->address();
        $new_address = $old_address->replicate();
        if( $old_address->street != $record->street || $old_address->city != $record->city  ||
            $old_address->zip_postal != $record->zip_postal || $old_address->state) {
            $change_found = true;

            $old_address->delete();

            $fullAddress = $record->street . ' ' . $record->city . ', ' . $record->state_province;
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($fullAddress) . "&key=" . config('app.google_key');
            $geocode = file_get_contents($url);
            $output = json_decode($geocode);
            $latitude = $output->results[0]->geometry->location->lat;
            $longitude = $output->results[0]->geometry->location->lng;

            \Debugbar::info("lat lng for $fullAddress", $latitude,$longitude);

            $new_address->street = $record->street;
            $new_address->city = $record->city;
            $new_address->zip_postal = $record->zip_postal;
            $new_address->state_province_id = array_search($record->state_province, $stateProvinceNames);
            $new_address->map_latitude = $latitude;
            $new_address->map_longitude = $longitude;
            $new_address->created_at = date("Y-m-d H:i:s");
            $new_address->updated_at = date("Y-m-d H:i:s");
            $new_address->save();
            $address_id = $new_address->id;

            $new_profile->address_id = $address_id;

        }


        // the best part - status
        if( $record->status == 'T') {   // terminate driver

            $c = new StreetLightCalculator();
            $color = $c->colorOfStreetLight($old_profile->street_light_id);
            if( $color == 'Green' || $color == 'Yellow' || $color == 'Red' && ($old_profile->street_light_id & RED_TERMINATED) == 0 ) {
                $change_found = true;
                if( $record->stop_date < date("Y-m-01")) {  // last month, so terminate now
                    $new_profile->active = 0;
                    $new_profile->street_light_id = RED_TERMINATED;
                    $action_taken = 'TERMINATED';
                    \DB::table('manger_driver')->where('manager_id',$manager_id)->where('driver_id',$user_id)
                        ->update('deleted_at', date("Y-m-d H:i:s"));
                } else {                                            // this month so terminate on Approval End + 2 next month

                    list($yyyy,$mm) = explode('-',date("Y-m"));
                    if( $mm == 12) { $mm = 1; $yyyy++; }
                    else { $mm++; }

                    $scheduled_date = sprintf("%04d-%02d-10", $yyyy, $mm);
                    \DB::table('scheduled_terminations')->insert([
                        'user_id' => $record->user_id,
                        'scheduled_date' => $scheduled_date,
                        'stop_date' => $record->stop_date
                    ]);
                    $action_taken = 'TO BE TERMINATED';
                }
            }
        } elseif( $record->status == 'A') {
            if( $old_profile->street_light_id & RED_TERMINATED) {
                $new_profile->street_light_id = 1;
                $new_profile->stop_date = null;
                $mm = date("n") == 12 ? 1 : date("n")+1;
                $yy = date("n") == 12 ? date("Y")+1 : date("Y");
                $new_profile->start_date = sprintf("%04d-%02d-01", $yy, $mm );
                $change_found = true;
                $action_taken = 'ADDED';
            }
        }

        if( $change_found ) {
            if( !$action_taken ) {
                $action_taken = 'UPDATED';
            }
            \Debugbar::info("New Driver Profile created");
            $old_profile->delete();
            $new_profile->save();
        } else {
            \Debugbar::info("No changes in Driver Profile");
            $action_taken = "No Changes";
        }

        return $action_taken;

    }

    public function deletedScheduledDrivers($company_id) {
        $date = date("Y-m-d");
        $terminated = 0;
        $query = <<<EOQ
select u.id AS user_id, u.first_name, u.last_name, u.active, dp.id, mp.manager_id, ap.administrator_id, st.scheduled_date
from scheduled_terminations st 
  join users as u on st.user_id = u.id
  left join driver_profiles dp ON u.id = dp.id AND dp.company_id = $company_id and dp.deleted_at is null
  left join manager_profiles mp ON u.id = mp.user_id AND mp.company_id = $company_id and mp.deleted_at is null
  left join administrator_profiles ap ON u.id = ap.user_id AND ap.company_id = $company_id and ap.deleted_at is null
  where st.scheduled_date <= '$date' and executed_at is null
EOQ;
        $users = \DB::select(\DB::raw($query));

        foreach( $users as $u ) {
            // terminate driver
            $driver_profile = DriverProfile::find($u->id);
            $new_driver_profile = $driver_profile->replicate();
            $driver_profile->delete();
            $new_driver_profile->stop_date = $date;
            $new_driver_profile->active = 0;
            $new_driver_profile->street_light_id = RED_TERMINATED;
            $new_driver_profile->save();
            $terminated++;


            // DB::table('scheduled_terminations')->where('user_id',$u->user_id)->where('')

            \DB::table('manager_drivers')->where('user_id',$u->user_id)->update('deleted_at',$date);

            // check if we suppose to inactivate user
            if( $u->manager_id == '' && $u->administrator_id == '') {
                $user = User::find($u->user_id);
                $new_user = $user->replicate();
                $user->delete();
                $new_user->active = 0;
                $new_user->save();
            }

        }
    }

    public function find_manager($company_id, $record) {

        // try by manager number
        $manager = ManagerProfile::where('company_id', $company_id)
            ->where('manager_number', $record->manager_id)->first();

        if( !$manager ) {
            // try by last name, first name and email

            \Debugbar::info("Manager not found by his number");

            $manager_query = <<<EOQ
SELECT m.* from manager_profiles m, users u 
  WHERE m.user_id = u.id AND u.last_name = '$record->manager_last_name' 
  AND u.first_name = '$record->manager_first_name'
  AND u.email = '$record->manager_email' 
  AND m.company_id = $company_id AND m.deleted_at IS NULL
EOQ;

            $managers = \DB::select(\DB::raw($manager_query));

            \Debugbar::info("Manager Profile found by email and name: ", $managers);

            if( $managers ) {

                \Debugbar::info("Update manager profile with number: $record->manager_id");
                $m = $managers[0];
                $manager = ManagerProfile::find($m->id);    // we need it to use $manager->save()
                $manager->manager_number = $record->manager_id;
                $manager->save();

            } else {
                \Debugbar::info("No such manager");
                $manager = null;
            }
        }

        if( $record->status == 'A' && $manager && $manager->active == 0 ) {   // create new active manager profile
            $new_manager = $manager->replicate();
            $new_manager->active = 1;
            $manager->delete();
            $new_manager->save();
            return (object)['activated' => 1, 'manager' => $new_manager];
        }
        return (object)['activated' => 0, 'manager' => $manager];
    }

    public function add_new_manager($company_id, $record) {

        $user = $this->get_user_from_email($company_id, $record->manager_email);
        \Debugbar::info("User from get_user_from_email: ", $user);

        if( $user ) {
            $user_id = $user->user_id;
            \Debugbar::info("We have user id: $user_id");
            // TODO update if inactive

        } else {

            $user = User::create([
                'username' => app('\App\Http\Controllers\UserController')
                    ->generateUsername($record->manager_first_name, $record->manager_last_name),
                'first_name' => $record->first_name,
                'last_name' => $record->last_name,
                'email' => $record->manager_email,
                'password' => \Hash::make('silver'),
            ]);
            $user_id = $user->id;
            \Debugbar::info("We create a new user: id= $user_id");
        }


        $manager = ManagerProfile::create([
            'user_id' => $user_id,
            'manager_id' => $user_id,
            'company_id' => $company_id,
            'manager_number' => $record->manager_id,
            'assigned_drivers' => 'some',
            'active' => 1
        ]);
        return $manager;

    }

    public function assignDriverToManager( $driver_id, $manager_id){

        $md = \DB::table('manager_driver')->where('manager_id',$manager_id)
            ->where('driver_id',$driver_id)->first();

        \Debugbar::info('Manager Driver record: ', $md);

        if( !$md || $md && $md->deleted_at != null ) {
            \Debugbar::info('Add New manager driver record');
            \DB::table('manager_driver')->insert([
                'manager_id' => $manager_id,
                'driver_id' => $driver_id,
                'created_at' => date('Y-m-d H:i:d'),
                'updated_at' => date('Y-m-d H:i:d')
            ]);
        } else {
            \Debugbar::info('Driver already assigned');
        }
    }

    public function updateManagerAssignDrivers($company_id) {

        $reinstated_total = 0;
        $deactivated_total = 0;
        $reinstated_names = [];
        $deactivated_names = [];

        // activate managers

        $sql = <<<EOQ
SELECT mp.id, u.last_name, u.first_name FROM manager_profiles mp, users u 
WHERE mp.company_id = $company_id AND mp.deleted_at IS NULL AND mp.assigned_drivers = 'none'
AND mp.user_id = u.id
        AND EXISTS 
        ( SELECT driver_id FROM manager_driver 
            WHERE manager_id = mp.manager_id AND deleted_at IS NULL LIMIT 1 )
EOQ;
        $managers = \DB::select(\DB::raw($sql));
        foreach($managers as $m){
            $manager = ManagerProfile::find($m->id);
            $new_manager = $manager->replicate();
            $manager->delete();
            $new_manager->active = 1;
            $new_manager->assigned_drivers = 'some';
            $new_manager->save();

            $reinstated_total++;
            $reinstated_names[] = $m->last_name . ' ' . $m->first_name . ' REINSTATED';
        }

        // deactivate managers without drivers

        $sql = <<<EOQ
SELECT mp.id, mp.active, u.last_name, u.first_name FROM manager_profiles mp
    JOIN users u ON mp.user_id = u.id
    LEFT JOIN manager_driver md ON mp.manager_id = md.manager_id AND md.deleted_at IS NULL
    WHERE mp.company_id = $company_id AND mp.assigned_drivers != 'none' AND mp.deleted_at IS NULL
    AND md.driver_id IS null
    ORDER BY u.last_name, first_name
EOQ;

        $managers = \DB::select(\DB::raw($sql));
        foreach($managers as $m){
            $manager = ManagerProfile::find($m->id);
            $new_manager = $manager->replicate();
            $manager->delete();
            $new_manager->active = 0;
            $new_manager->assigned_drivers = 'none';
            $new_manager->save();

            $deactivated_total++;
            $deactivated_names[] = $m->last_name . ' ' . $m->first_name . ' DEACTIVATED';
        }

        return (object)['reinstated' => $reinstated_total, 'reinstated_arr' => $reinstated_names,
            'deactivated' => $deactivated_total, 'deactivated_arr' => $deactivated_names];
    }


}