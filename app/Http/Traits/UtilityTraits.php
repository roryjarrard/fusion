<?php namespace App\Http\Traits;

trait UtilityTraits
{
    /**
     * Returns holiday objects for a specific date
     *  If a country and a state province are supplied the result will be null or a SINGLE holiday object
     *  If neither a state_province_id nor a country_id is supplied, it is possible that MANY holidays objects
     *  (for the specific day) will be returned.
     *
     *  This behaviour is desired as not all countries, provinces, states, celebrate holidays (such as family day)
     *  at the same time.
     *
     *  If no holidays are found in our records, null is returned
     *
     * @param $date Y-m-d
     * @param null $country_id
     * @param null $state_province_id
     * @return null
     */
    public function getHoliday($date, $country_id = null, $state_province_id = null)
    {
        $arr = explode('-', $date);
        $year = (int)($arr[0]);
        $month = (int)($arr[1]);
        $day = (int)($arr[2]);

        if ($country_id != null && $state_province_id != null) {
            $holiday = \DB::table('holidays')
                ->select()
                ->where('country_id', $country_id)
                ->where('state_province_id', $state_province_id)
                ->where('year', $year)
                ->where('month', $month)
                ->where('day', $day)
                ->get();
        } else if ($country_id) {
            $holiday = \DB::table('holidays')
                ->select()
                ->where('country_id', $country_id)
                ->where('year', $year)
                ->where('month', $month)
                ->where('day', $day)
                ->get();
        } else if ($state_province_id) {
            $holiday = \DB::table('holidays')
                ->select()
                ->where('state_province_id', $state_province_id)
                ->where('year', $year)
                ->where('month', $month)
                ->where('day', $day)
                ->get();
        } else {
            $holiday = \DB::table('holidays')
                ->select()
                ->where('year', $year)
                ->where('month', $month)
                ->where('day', $day)
                ->get();
        }

        $count = $holiday->count();

        if ($count == 0) {
            return null;
        } else if ($count == 1) {
            return $holiday[0];
        } else {
            // there are more than 1 holidays grabbed for this particular day
            return $holiday;
        }
    }

    //TODO: implement get many holidays (if needed)
    public function getManyHolidays($dates, $country_id = null, $state_province_id = null)
    {

    }

    /**
     * Returns an associative array with the previous year and month (based upon today's date, or
     * the year and month parameters).
     *
     * You may specify the return type, by default it is integer.
     *
     * If an invalid type is specified, the function will die().
     *
     * @param null $year
     * @param null $month
     * @param string $type must be either 'string' or 'int'
     * @return array
     */
    public function getPreviousMonthAndYear($year = null, $month = null, $type = 'int')
    {
        $dates = [];

        // if pararms passed in
        if ( !( isset($year) && isset($month) )) {
            $dates['year'] = date("n") == 1 ? date("Y") - 1 : date("Y");
            $dates['month'] = date("n") == 1 ? 12 : date("n") - 1;
        } else {
            $dates['year'] = $month == 1 ? $year - 1 : $year;
            $dates['month'] = $month == 12 ? 1 : $month - 1;
        }

        if ($type == 'int') {
            $dates['year'] = (int)$dates['year'];
            $dates['month'] = (int)$dates['month'];
        } else if ($type == 'string') {
            $dates['year'] = (string)$dates['year'];
            $dates['month'] = (string)$dates['month'];
        } else {
            die('Invalid type parameter');
        }

        return $dates;
    }

    public function getNextOccurenceOfMonthAndDay($month, $day)
    {
        $year = date("Y");
        $today = date("Y-m-d");
        $target_date = $year . '-' . sprintf("%'02d", $month) . '-' . sprintf("%'02d", $day);

        if ( $today > $target_date ) {
            $year = $year + 1;
            $target_date = $year . '-' . sprintf("%'02d", $month) . '-' . sprintf("%'02d", $day);
        }

        return $target_date;
    }

}
