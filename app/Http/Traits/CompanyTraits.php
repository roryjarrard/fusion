<?php namespace App\Http\Traits;

use App\CompanyInsurance;

trait CompanyTraits
{
    public function getCompanyMileageDates($company_id = null)
    {

        $local_company_id = $company_id;

        if ($company_id == null) {
            $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
            $local_company_id = $user->driverProfile->company_id;
        }

        $companyMileageDates = \DB::table('company_dates')
            ->join('company_mileages', 'company_mileages.company_id', 'company_dates.company_id')
            ->select('company_dates.mileage_lock_driver_day',
                'company_dates.mileage_lock_all_day',
                'company_dates.mileage_approval_end_day',
                'company_dates.mileage_reminder_day',
                'company_dates.second_mileage_reminder_day',
                'company_mileages.mileage_entry_method')
            ->where('company_id', '=', $local_company_id)
            ->first();

        return ['companyMileageDates' => $companyMileageDates];
    }

    public function getCompanyInsuranceAmount($company_id, $country_id) {
        $insurance = CompanyInsurance::where('company_id', $company_id)->first();
        $data = [];

        $max_space = 30;
        if( $country_id == USA ) {
            if ($insurance->bodily_liability_per_person > 0) {
                $database_column_name = 'bodily_liability_per_person';
                $name = 'Bodily Liability';
                $spacing = $max_space - strlen(number_format($insurance->bodily_liability_per_person));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->bodily_liability_per_person), 'affix' => 'per person', 'database_column_name' => $database_column_name];
            }
            if ($insurance->bodily_liability_per_accident > 0) {
                $database_column_name = 'bodily_liability_per_accident';
                $name = 'Bodily Liability';
                $spacing = $max_space - strlen(number_format($insurance->bodily_liability_per_accident));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->bodily_liability_per_accident), 'affix' => 'per accident', 'database_column_name' => $database_column_name];
            }
            if ($insurance->property_damage > 0) {
                $database_column_name = 'property_damage';
                $name = 'Property Damage';
                $spacing = $max_space - strlen(number_format($insurance->property_damage));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->property_damage), 'affix' => 'each accident', 'database_column_name' => $database_column_name];
            }
            if ($insurance->deductible > 0) {
                $database_column_name = 'deductible';
                $name = 'Deductible';
                $spacing = $max_space - strlen(number_format($insurance->deductible));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->deductible), 'affix' => 'maximum', 'database_column_name' => $database_column_name];
            }
            if ($insurance->collision_deductible > 0) {
                $database_column_name = 'collision_deductible';
                $name = 'Collision Deductible';
                $len = strlen(number_format($insurance->collision_deductible));
                $spacing = $max_space - $len;
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->collision_deductible), 'affix' => 'maximum', 'database_column_name' => $database_column_name];
            }
            if ($insurance->medical > 0) {
                $database_column_name = 'medical';
                $name = 'Medical';
                $spacing = $max_space - strlen(number_format($insurance->medical));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->medical), 'affix' => 'minimum', 'database_column_name' => $database_column_name];
            }
            if ($insurance->uninsured_per_person > 0) {
                $database_column_name = 'uninsured_per_person';
                $name = 'Uninsured/Underinsured';
                $spacing = $max_space - strlen(number_format($insurance->uninsured_per_person));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->uninsured_per_person), 'affix' => 'per person', 'database_column_name' => $database_column_name];
            }
            if ($insurance->uninsured_per_accident > 0) {
                $database_column_name = 'uninsured_per_accident';
                $name = 'Uninsured/Underinsured';
                $spacing = $max_space - strlen(number_format($insurance->uninsured_per_accident));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->uninsured_per_accident), 'affix' => 'per accident', 'database_column_name' => $database_column_name];
            }
        }

        /**
         * 2018-09-03 switched company id to country id,
         * I think this was what was meant!
         * - Frank
         */
        if( $country_id == CANADA ) {
            if ($insurance->public_liability > 0) {
                $database_column_name = 'public_liability';
                $name = 'Public Liability, Public Damage';
                $spacing = $max_space - strlen(number_format($insurance->public_liability));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->public_liability), 'affix' => null, 'database_column_name' => $database_column_name];
            }
            if ($insurance->collision_deductible > 0) {
                $database_column_name = 'collision_deductible';
                $name = 'Collision Deductible, Public Damage';
                $spacing = $max_space - strlen(number_format($insurance->collision_deductible));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->collision_deductible), 'affix' => null, 'database_column_name' => $database_column_name];
            }
            if ($insurance->comprehensive_deductible > 0) {
                $database_column_name = 'comprehensive_deductible';
                $name = 'Comprehensive Deductible';
                $spacing = $max_space - strlen(number_format($insurance->comprehensive_deductible));
                $data[] = (object)['name' => str_pad($name,$spacing,' '), 'amount' => number_format($insurance->comprehensive_deductible), 'affix' => null, 'database_column_name' => $database_column_name];
            }
        }

        return $data;
    }

    public function getCompanyBusinessUse($company_id)
    {
        $insurance = CompanyInsurance::where('company_id', $company_id)->first();
        return $insurance->business_use > 0 ? true : false;
    }
}
