<?php namespace App\Http\Traits;

use App\Address;
use App\AdministratorProfile;
use App\DriverStreetLightStatus;
use App\FuelCity;
use App\ManagerProfile;
use App\MasterTask;
use App\MileageBand;
use App\ScheduledTask;
use App\StateProvince;
use App\TerritoryType;
use App\TimeZone;
use App\VehicleProfile;
use App\ZipCode;

trait CommonTraits
{
    public function formattedAddress($address_id)
    {
        $rec = Address::withTrashed()->find($address_id);
        if ($rec) {
            $address = $rec->street;
            if (!empty($rec->street_2)) $address .= ' ' . $rec->street_2;
            if (!empty($rec->city)) $address .= ' ' . $rec->city;
            if (!empty($rec->zip_postal)) $address .= ', ' . $rec->zip_postal;
            if (!empty($rec->stateProvince->short_name)) $address .= ' ' . $rec->stateProvince->short_name;
            return $address;
        } else {
            return null;
        }
    }

    /**
     * Returns the formatted mileage band for a given id
     *
     * ex. func(1) returns
     *      '2,500 - 5,000'
     *
     * @param $id
     * @return null|string
     */
    public function formattedMileageBand($id)
    {
        $rec = MileageBand::find($id);
        if ($rec) {
            return ($id < 11) ? number_format($rec->mileage_start) . ' - ' . number_format($rec->mileage_end)
                :
                '> ' . number_format($rec->mileage_start);
        } else {
            return null;
        }
    }

    /**
     * Returns formatted trade in mileage for a given mileage_band_id and retention.
     *
     * Ex. func(1, 4) returns
     *      '10,000 - 20,000'
     *
     * @param $mileage_band_id
     * @param $retention
     * @param $thousands - optional, display 20,000 as 20K
     * @return null|string
     */
    public function formattedTradeInMileage($mileage_band_id, $retention, $thousands = false)
    {
        $mileageBand = MileageBand::find($mileage_band_id);
        if ($mileageBand) {

            if ($thousands) {
                return ($mileage_band_id < 11) ? (($mileageBand->mileage_start * $retention) / 1000)
                    . 'K - ' . (($mileageBand->mileage_end * $retention) / 1000) . 'K'
                    :
                    '> ' . (($mileageBand->mileage_start * $retention) / 1000) . 'K';
            }

            return ($mileage_band_id < 11) ? number_format($mileageBand->mileage_start * $retention)
                . ' - ' . number_format($mileageBand->mileage_end * $retention)
                :
                '> ' . number_format($mileageBand->mileage_start * $retention);
        } else {
            return null;
        }
    }

    public function formattedCapitalCost($capital_cost)
    {
        if ($capital_cost == 'msrp') return 'MSRP';
        if ($capital_cost == 'invoice') return 'Invoice';
        if ($capital_cost == 'blue_book') return 'Blue Book';
    }


    public function formattedVehicleProfile($id)
    {
        $rec = VehicleProfile::withTrashed()->find($id);
        if ($rec) {
            $vehicle = $rec->vehicle;
            $profile = $vehicle->year . ' ' . trim($vehicle->name);
            if (!empty($vehicle->rank)) $profile .= " [$vehicle->rank] ";
            $profile .= $rec->name_postfix;
            return trim($profile);
        } else {
            return null;
        }
    }

    public function formattedTerritoryType($id)
    {
        $rec = TerritoryType::find($id);
        if ($rec) {
            return $rec->name;
        } else {
            return null;
        }
    }

    public function formattedTerritoryList($type_id, $list)
    {

        $names = null;
        if ($type_id == 1) {
            return 'Get Fuel City from Profile';
        } elseif ($type_id == 2) {
            return 'Get Driver State/Province';
        } elseif ($type_id == 3) {
            if (!empty($list)) {
                $arr = explode(',', $list);
                $names = implode(', ', FuelCity::whereIn('id', $arr)->orderBy('name')->pluck('name')->flatten()->all());
            }
        } elseif ($type_id == 4) {
            if (!empty($list)) {
                $arr = explode(',', $list);
                $names = implode(', ', StateProvince::whereIn('id', $arr)->orderBy('name')->pluck('name')->flatten()->all());
            }
        } else {
            $names = 'Invalid Territory Type:' . $type_id;
        }

        return $names;
    }

    public function formattedDate($date, $format)
    {
        if ($date) {
            @list($y, $m, $d) = explode('-', $date);
            @$timestamp = mktime(0, 0, 0, $m, $d, $y);
            if ($timestamp) {
                return date($format, $timestamp);
            }
        }
        return null;
    }


    public function getMonthNames()
    {

        return [1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July',
            8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'];
    }

    /**
     * Return current address of user.
     *
     * Although not implemented except for drivers, we will be able
     * to leverage this function if other user types have an address
     * in the future.
     *
     * @param $user_id
     * @param $user_type
     * @return object
     */
    public function UserAddress($user_id, $user_type)
    {
        if ($user_id < 0) {
            return;
        }

        $address = null;

        switch ($user_type) {
            case 'driver':
                $address = Address::where('user_id', $user_id)->with('stateProvince')->first();
                break;
            case 'manager':
                $address = Address::where('user_id', $user_id)->with('stateProvince')->first(); // davery
                break;
            case 'administrator':
                break;
            case 'super':
        }

        if (!empty($address) && (empty($address->latitude) || empty($address->longitude))
            && (empty($address->map_latitude) || empty($address->map_longitude))) {
            $key = config('app.google_key');
            $postal_code = $address->zip_postal;
            $city_name = $address->city;
            $province = $address->stateProvince->short_name;
            $address_encoded = urlencode("{$city_name},{$postal_code},{$province}");
            $url = "https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address={$address_encoded}&key={$key}";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // Disable SSL verification
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            $result = curl_exec($ch);
            curl_close($ch);

            //dd(json_decode($result));

            $json = json_decode($result);

            //echo print_r($json->results[0]->geometry->location->lat);

            $address->map_latitude = $json->results[0]->geometry->location->lat;
            $address->map_longitude = $json->results[0]->geometry->location->lng;
            $address->save(); // don't save address for now
        }
        //echo print_r($json);
        return $address;
    }


    /**
     * Given initial values (from portal controller), as well as a user type,
     * an object containing booleans keyed by page id's is returned.
     * We can then filter our menu items by key value pair
     *
     * params $user_type String - (driver,manager,administrator,super)
     *        $initial_values - dictionary
     */
    private function getMenuPermissions($user_type, $initial_values)
    {
        $permissions = [];
        switch ($user_type) {
            case 'driver':
                $permissions['bankingInformation'] = $initial_values['company_selections']['payment_responsibility'] == 'CarData';
                break;
            case 'manager':
                break;
            case 'administrator':
                break;
            case 'super':
                break;
        }

        return $permissions;
    }

    /**
     * Returns the fusion menu object so that it can be injected into the vue instance on page load
     *
     * @param $user_type
     * @param null $company_name
     * @param null $mileage_entry_type
     * @param $permissions
     * @return array|null
     */
    public function Menu($user_type, $company_name = null, $mileage_entry_type = null, $permissions)
    {
        $menu = null;
        // allow for string or numeric,
        switch ($user_type) {
            case 1:
            case 'driver':
                $menu = [
                    [
                        'name' => 'Mileage',
                        'icon' => 'Road',
                        'menuIcon' => 'road',
                        'defaultPage' => 'driverDashboard',
                        'data' => [
                            ['label' => 'Entry'],
                            ['name' => 'Mileage Entry', 'id' => 'mileageEntry'],
                            [
                                'divider' => true
                            ],
                            [
                                'label' => 'Testing'
                            ],
                            [
                                'name' => 'Store State',
                                'id' => 'storeState'
                            ]/*,
                            [
                                'name' => 'Chart Demo',
                                'id' => 'chartDemo'
                            ]*/
                        ]
                    ],
                    [
                        'name' => 'Reimbursement',
                        'icon' => 'usd',
                        'menuIcon' => 'usd-circle',
                        'data' => [
                            ['label' => 'General'],
                            ['name' => 'Monthly Reimbursement Report', 'id' => 'monthlyReimbursement'],
                            ['name' => 'Reimbursement Profile', 'id' => 'reimbursementProfile'],
                            ['name' => $company_name . ' Car Policy', 'id' => 'carPolicy'],
                            ['name' => 'Depreciation Report', 'id' => 'depreciationReport'],
                            ['name' => 'Banking Information', 'id' => 'bankingInformation', 'display' => $permissions['bankingInformation']]
                        ]
                    ],
                    [
                        'name' => 'Vehicles',
                        'icon' => 'car',
                        'menuIcon' => 'car',
                        'data' => [
                            ['label' => 'General'],
                            ['name' => 'Personal Vehicles', 'id' => 'personalVehicles']
                        ]
                    ],
                    [
                        'data' => [
                            ['label' => 'General'],
                            ['id' => 'driverDashboard', 'name' => 'Dashboard', 'subpage_of' => 'driverDashboard'],
                            ['id' => 'driverProfile', 'name' => 'Dashboard', 'subpage_of' => 'driverProfile'],
                            ['id' => 'personalVehicle', 'name' => 'Dashboard', 'subpage_of' => 'personalVehicles'],
                            ['id' => 'personalVehicleFAVR', 'name' => 'Dashboard', 'subpage_of' => 'personalVehiclesFAVR']
                        ]
                    ]
                ];
                if ($mileage_entry_type === 'Mi-Route') {
                    $menu[0]['data'] = array_merge(array_slice($menu[0]['data'], 0, 1), [['name' => 'Destination Addresses', 'id' => 'destinationAddresses']], array_slice($menu[0]['data'], 1));
                }
                break;
            case 2:
            case 'manager':
                $menu = [
                    [
                        'name' => 'Tools',
                        'icon' => 'wrench',
                        'menuIcon' => 'wrench',
                        'defaultPage' => 'mileageApproval',
                        'data' => [
                            ['label' => 'General'],
                            ['name' => 'Mileage Approval', 'id' => 'mileageApproval'],
                            ['name' => 'Link to Driver', 'id' => 'linkToDriver'],
                            ['name' => 'View Tutorials', 'id' => 'viewTutorials'],
                            ['name' => $company_name . ' Car Policy', 'id' => 'carPolicy'],
                            [
                                'name' => 'Store State',
                                'id' => 'storeState'
                            ]
                        ]
                    ],
                    [
                        'name' => 'Reports',
                        'icon' => 'list-alt',
                        'menuIcon' => 'list-alt',
                        'data' => [
                            ['label' => 'Reports'],
                            ['name' => 'Reimbursement Totals', 'id' => 'reimbursementTotals'],
                            ['name' => 'Fuel Prices', 'id' => 'fuelPrices'],
                            ['name' => 'Vehicle Report', 'id' => 'vehicleReport'],
                            ['name' => 'Traffic Light', 'id' => 'trafficLight']
                        ]
                    ],
                    [
                        'name' => 'Miscellaneous',
                        'icon' => 'pencil',
                        'menuIcon' => 'pencil',
                        'data' => [
                            ['label' => 'Miscellaneous'],
                            [
                                'name' => 'Password',
                                'id' => 'changePassword'
                            ]
                        ]
                    ],
                    [
                        'data' => [['id' => 'managerProfile']]
                    ]
                ];
                break;
            case 3:
            case 'administrator':
                $menu = [
                    [
                        'name' => 'Tools',
                        'icon' => 'wrench',
                        'menuIcon' => 'wrench',
                        'defaultPage' => 'companyProfile',
                        'data' => [
                            [
                                'label' => 'Users'
                            ],
                            [
                                'name' => 'Show Users',
                                'id' => 'showUserPage'
                            ],
                            [
                                'name' => 'Add / Edit User',
                                'id' => 'administratorAddEditUser'
                            ],
                            [
                                'name' => 'Link To Driver',
                                'id' => 'linkToDriver'
                            ],
                            [
                                'name' => 'Add Many Drivers',
                                'id' => 'administratorAddManyDrivers'
                            ],
                            [
                                'label' => $company_name
                            ],
                            [
                                'name' => 'Company Profile',
                                'id' => 'companyProfile'
                            ],
                            [
                                'name' => 'Divisions',
                                'id' => 'divisionsView'
                            ],
                            [
                                'name' => 'Vehicle Profiles',
                                'id' => 'administratorVehicleProfiles'
                            ],
                            [
                                'label' => 'Business Intelligence'
                            ],
                            [
                                'name' => 'Reimbursement Calculator',
                                'id' => 'administratorReimbursementCalculator'
                            ],
                            [
                                'name' => 'Annual Reviews',
                                'id' => 'annualReviews'
                            ],
                            [
                                'name' => 'Store State',
                                'id' => 'storeState'
                            ]
                        ]
                    ],
                    [
                        'name' => 'Reports',
                        'icon' => 'th-list',
                        'menuIcon' => 'th-list',
                        'data' => [
                            ['label' => 'Reports'],
                            [
                                'name' => 'Fixed and Variable',
                                'id' => 'fixedVariable'
                            ],
                            [
                                'name' => 'Reimbursement Totals',
                                'id' => 'reimbursementTotals'
                            ],
                            [
                                'name' => 'Fuel Prices', 'id' => 'fuelPrices'
                            ]
                        ]
                    ],
                    [
                        //TODO=> ADD DIVIDERS AND LABELS
                        'name' => 'Payments',
                        'icon' => 'usd',
                        'menuIcon' => 'dollar-sign',
                        'data' => [
                            ['label' => 'Payments'],
                            [
                                'name' => 'Master Payment Notice',
                                'id' => 'masterPaymentNotice'
                            ],
                            [
                                'name' => 'Manager Approval Status',
                                'id' => 'managerApprovalStatus'
                            ],
                            [
                                'name' => 'Prorate Reimbursement',
                                'id' => 'prorateReimbursement'
                            ],
                            [
                                'name' => 'Direct Deposit Payment Schedule',
                                'id' => 'directDepositPaymentSchedule'
                            ],
                            [
                                'name' => 'Pay File',
                                'id' => 'payFile'
                            ]
                        ]
                    ],
                    [
                        'name' => 'Miscellaneous',
                        'icon' => 'pencil',
                        'menuIcon' => 'pencil',
                        'data' => [
                            ['label' => 'General'],
                            [
                                'name' => 'Password',
                                'id' => 'changePassword'
                            ]
                        ]
                    ],
                    [
                        'data' => [['id' => 'administratorProfile']]
                    ]
                ];
                break;
            case 4:
            case 'super':
                $menu = [
                    [
                        'name' => 'Tools',
                        'icon' => 'wrench',
                        'menuIcon' => 'wrench',
                        'defaultPage' => 'toolsMenu',
                        'data' => [
                            [
                                'label' => 'Users'
                            ],
                            [
                                'name' => 'Show Individual User',
                                'id' => 'showUsers'
                            ],
                            [
                                'name' => 'Company Users List',
                                'id' => 'users'
                            ],
                            [
                                'name' => 'Insurance Approval',
                                'id' => 'insuranceApproval'
                            ],
                            [
                                'name' => 'License Approval',
                                'id' => 'licenseApproval'
                            ],
                            [
                                'divider' => true
                            ],
                            [
                                'label' => 'Companies'
                            ],
                            [
                                'name' => 'Company Information',
                                'id' => 'companies'
                            ],
                            [
                                'name' => 'Divisions',
                                'id' => 'divisions'
                            ],
                            [
                                'name' => 'Rate Comparison',
                                'id' => 'rateComparison'
                            ],
                            [
                                'name' => 'Add Many Drivers',
                                'id' => 'addManyDrivers'
                            ],
                            [
                                'divider' => true
                            ],
                            [
                                'label' => 'Vehicle Profiles'
                            ],
                            [
                                'name' => 'Vehicle Profile Information',
                                'id' => 'vehicleProfiles'
                            ],
                            [
                                'name' => 'Vehicle Approvals',
                                'id' => 'vehicleProfileApprovals'
                            ],
                            [
                                'divider' => true
                            ],
                            [
                                'label' => 'Utilities'
                            ],
                            [
                                'name' => 'Change Tracker',
                                'id' => 'changeTracker'
                            ],
                            [
                                'name' => 'ScheduledTasks',
                                'id' => 'scheduledTasks'
                            ],
                            [
                                'name' => 'Note Categories',
                                'id' => 'noteCategories'
                            ],
                            [
                                'name' => 'Notifications',
                                'id' => 'notifications'
                            ],
                            [
                                'name' => 'Holidays',
                                'id' => 'holidays',
                            ],
                            [
                                'name' => 'Add Zip / Postal Code',
                                'id' => 'addPostalCode',
                            ],
                            [
                                'name' => 'Master Task Control',
                                'id' => 'masterTask'
                            ],
                            [
                                'name' => 'Rate Selector',
                                'id' => 'rateSelector'
                            ],
                            [
                                'name' => 'Reimbursement Calculator',
                                'id' => 'reimbursementCalculator'
                            ],
                            [
                                'name' => 'Annual Reviews',
                                'id' => 'annualReviews'
                            ],
                            [
                                'divider' => true
                            ],
                            [
                                'label' => 'Business Intelligence'
                            ],
                            [
                                'name' => 'Api Management',
                                'id' => 'apiManagement'
                            ],
                            [
                                'name' => 'Store State',
                                'id' => 'storeState'
                            ],
                            [
                                'name' => 'Testing Page',
                                'id' => 'testingPage'
                            ],
                            [
                                'name' => 'Testing Reimbursement - Calculator',
                                'id' => 'testingReimbursement'
                            ],
                            [
                                'name' => 'Reimbursement Details for Driver',
                                'id' => 'reimbursementDetails'
                            ],
                            [
                                'name' => 'Generate Reimbursement',
                                'id' => 'generateReimbursement'
                            ],
                            [
                                'name' => 'Fixed and Variable Discrepancies',
                                'id' => 'fixedAndVariableDiscrepancies'
                            ]
                            /*
                            [
                              'name'=> 'Testing Tax Quarter Months',
                              'id'=> 'testTaxQuarterMonths'
                            ],
                            */

                        ]
                    ],
                    [
                        'name' => 'Reports',
                        'icon' => 'th-list',
                        'menuIcon' => 'th-list',
                        'data' => [
                            [
                                'label' => 'Reimbursement'
                            ],
                            [
                                'name' => 'Dynamic Reports',
                                'id' => 'dynamicReports'
                            ],
                            [
                                'name' => 'Fixed & Variable',
                                'id' => 'fixedVariable'
                            ],
                            [
                                'name' => 'Reimbursement and Mileage Chart',
                                'id' => 'reimbursementAndMileageChart'
                            ],
                            [
                                'name' => 'Reimbursement Totals - Time Period',
                                'id' => 'reimbursementTotals'
                            ],
                            [
                                'name' => 'All Fields Report',
                                'id' => 'allFieldsReport'
                            ],
                            [
                                'name' => 'Drivers Without Reimbursement',
                                'id' => 'driversWithoutReimbursement'
                            ],
                            [
                                'name' => 'Enrollment Audit',
                                'id' => 'enrollmentAudit'
                            ],
                            [
                                'name' => 'Fuel Prices',
                                'id' => 'fuelPrices'
                            ],
                            [
                                'name' => 'IRS Standard Mileage Rates',
                                'id' => 'iRSStandardMileageRates'
                            ],
                            [
                                'name' => 'CRA Standard Mileage Rates',
                                'id' => 'cRAStandardMileageRates'
                            ],
                            [
                                'divider' => true
                            ],
                            [
                                'label' => 'Driver'
                            ],
                            [
                                'name' => 'Login History',
                                'id' => 'loginHistory'
                            ],
                            [
                                'name' => 'Vehicle and Traffic Light',
                                'id' => 'vehicleAndTrafficLight'
                            ],
                            [
                                'name' => 'Vehicle',
                                'id' => 'vehicle'
                            ],
                            [
                                'name' => 'Activity',
                                'id' => 'activity'
                            ],
                            [
                                'name' => 'Recent Mileage Logs',
                                'id' => 'recentMileageLogs'
                            ],
                            [
                                'name' => 'Mileage',
                                'id' => 'mileage'
                            ],
                            [
                                'name' => 'Daily Mileage Details',
                                'id' => 'dailyMileageDetails'
                            ],
                            [
                                'name' => 'Bank Account',
                                'id' => 'bankAccount'
                            ],
                            [
                                'name' => 'Manager',
                                'id' => 'manager'
                            ],
                            [
                                'name' => 'Address and Email',
                                'id' => 'addressAndEmail'
                            ],
                            [
                                'name' => 'Driver Dates',
                                'id' => 'driverDates'
                            ],
                            [
                                'name' => 'Insurance Expired',
                                'id' => 'insuranceExpired'
                            ],
                            [
                                'name' => 'License Expired',
                                'id' => 'licenseExpired'
                            ],
                            [
                                'name' => 'Vehicle Expired',
                                'id' => 'vehicleExpired'
                            ],
                            [
                                'name' => 'Inactive Drivers',
                                'id' => 'inactiveDrivers'
                            ],
                            [
                                'name' => 'Reimbursement',
                                'id' => 'reimbursement'
                            ],
                            [
                                'label' => 'Tax and Audit'
                            ],
                            [
                                'name' => 'Quarterly Definitions',
                                'id' => 'quarterlyDefinitions'
                            ],
                            [
                                'name' => 'Quarterly Tax Adjustment',
                                'id' => 'quarterlyTaxAdjustment'
                            ],
                            [
                                'name' => 'Summary of Quarterly Tax Adjustments',
                                'id' => 'summaryOfQuarterlyTaxAdjustments'
                            ],
                            [
                                'name' => 'IRS FAVR Annual Tax Adjustment Report',
                                'id' => 'annualTaxAdjustment'
                            ],
                            [
                                'name' => 'Mileage Band Audit',
                                'id' => 'mileageBandAudit'
                            ],
                            [
                                'name' => 'Fuel Card Tax Report (Rockwater only)',
                                'id' => 'fuelCardTaxReport'
                            ],
                            [
                                'name' => 'Unsubmitted Mileage',
                                'id' => 'unsubmittedMileage'
                            ],
                            [
                                'name' => '5000 Limit Report',
                                'id' => 'fiveThousandLimitReport'
                            ]
                        ]
                    ],
                    [
                        //TODO=> update to differ or be the same as administrator
                        'name' => 'Payments',
                        'icon' => 'dollar-sign',
                        'menuIcon' => 'dollar-sign',
                        'data' => [
                            [
                                'label' => 'Payments'
                            ],
                            [
                                'name' => 'Draft Rate Report',
                                'id' => 'draftRateReport'
                            ],
                            [
                                'name' => 'Master Payment Notice',
                                'id' => 'masterPaymentNotice'
                            ],
                            [
                                'name' => 'Direct Deposit Payment Schedule',
                                'id' => 'directDepositPaymentSchedule'
                            ],
                            [
                                'name' => 'Pay File',
                                'id' => 'payFile'
                            ],
                            [
                                'name' => 'Manager Approval Status',
                                'id' => 'managerApprovalStatus'
                            ],
                            [
                                'divider' => true
                            ],
                            [
                                'label' => 'Prorates'
                            ],
                            [
                                'name' => 'Prorate Reimbursement',
                                'id' => 'prorateReimbursement'
                            ],
                            [
                                'name' => 'Prorate Adjustment Report',
                                'id' => 'prorateAdjustmentReport'
                            ],
                            [
                                'divider' => true
                            ],
                            [
                                'label' => 'Direct Pay'
                            ],
                            [
                                'name' => 'Manual ACH & EFT Files',
                                'id' => 'manualAchEftFilesPage'
                            ],
                            [
                                'name' => 'Bank Information',
                                'id' => 'bankInformation'
                            ],
                        ]
                    ],
                    [
                        'name' => 'Miscellaneous',
                        'icon' => 'th-list',
                        'menuIcon' => 'th-list',
                        'data' => [
                            [
                                'label' => 'Miscellaneous'
                            ],
                            [
                                'name' => 'Temp Name',
                                'id' => 'tempName'
                            ]
                        ]

                    ],
                    ['name' => 'MI-Route',
                        'icon' => 'car',
                        'menuIcon' => 'car',
                        'data' => [
                            [
                                'label' => 'Companies'
                            ],
                            [
                                'name' => 'Pre-Defined Stops',
                                'id' => 'preDefinedStops'
                            ],
                            [
                                'divider' => true
                            ],
                            [
                                'name' => 'Demo Drivers Page',
                                'id' => 'demoDriversPage'
                            ],
                            [
                                'name' => 'Mi-Route Testing Drivers Page',
                                'id' => 'mirouteTestingDriversPage'
                            ],
                            [
                                'name' => 'Some example',
                                'id' => 'someExample'
                            ]
                        ]
                    ],
                    [
                        'data' => [['name' => 'Super Profile', 'id' => 'superProfile']]
                    ]
                ];
                break;
        }
        /**
         * Remove the bad eggs
         * Loop through each menu category
         * If an item is not present for all users, it will contain a property called display
         * display is a boolean based on permissions passed as a parameter to this function
         * if it is set, and its value is false, we remove it from the array
         *
         * Note the reference on category
         */
        foreach ($menu as &$category) {
            for ($i = 0; $i < sizeof($category['data']); $i++) {
                if (isset($category['data'][$i]['display']) && $category['data'][$i]['display'] == false) unset($category['data'][$i]);
            }
        }

        return $menu;
    }

    /**
     * @param int $company_id
     * @param int $year
     * @param int $month
     * @param decimal $cents_per_mile
     * @param int $mileage
     * @return float
     */
    public function getVariableReimbursement($company_id, $year, $month, $cents_per_mile, $mileage)
    {
        if ($company_id == 45 && ($year == 2010 && $month >= 2 || $year > 2010)) {    // Mitel US
            $variable_reimbursement = $mileage > 1100 ? ($mileage - 1100) * 0.26 : 0;
        } else {
            $variable_reimbursement = $mileage * $cents_per_mile / 100;
        }
        return round($variable_reimbursement, 2);
    }

    public function addToStreetLight($street_light, $add_this)
    {
        $new_id = $street_light;
        if (!($street_light & $add_this)) {
            $new_id += $add_this;
            if ($new_id > GREEN_REIMBURSEMENT && $street_light & GREEN_REIMBURSEMENT)
                $new_id -= GREEN_REIMBURSEMENT;
        }
        return $new_id;
    }

    public function removeFromStreetLight($street_light, $remove_this)
    {
        $new_id = $street_light;
        if ($street_light & $remove_this) {
            $new_id -= $remove_this;
            if ($new_id == 0)
                $new_id = GREEN_REIMBURSEMENT;
        }
        return $new_id;
    }

    public function colorOfStreetLight($id)
    {
        if ($id & YELLOW_NEW_DRIVER) {
            return 'Yellow';
        } else {
            $status_arr = DriverStreetLightStatus::all();
            foreach ($status_arr as $row) {
                if (($id & $row['id']) && $row['color'] == 'Red') {
                    return 'Red';
                }
            }
        }
        return 'Green';
    }

    public function streetLightTitles($street_light_id)
    {
        $names_arr = [];
        $names = DriverStreetLightStatus::all();
        foreach( $names as $n ) {
            if( $street_light_id & $n['id']) {
                $names_arr[] = $n['title'];
            }
        }
        return implode(', ', $names_arr);
    }

    public function scheduledTaskStarted($task_id, $task_name, $status = 'Running')
    {
        MasterTask::where('id', $task_id)->update(['last_run' => date('Y-m-d H:i:s'), 'last_status' => 'pending']);
        $now = date("Y-m-d H:i:s");
        $new_scheduled_task = new ScheduledTask;
        $new_scheduled_task->task_id = $task_id;
        $new_scheduled_task->name = $task_name;
        $new_scheduled_task->status = 'Started';
        $new_scheduled_task->start_date = $now;
        $new_scheduled_task->status = $status;
        $new_scheduled_task->save();
        return $new_scheduled_task->id;
    }

    public function scheduledTaskEnded($task_id, $log_id, $short_description = '', $long_description = '', $status = 'Completed')
    {
        $now = date("Y-m-d H:i:s");
        $rec = ScheduledTask::find($log_id);
        $rec->short_description = $short_description;
        $rec->long_description = $long_description;
        $rec->status = $status;
        $rec->end_date = $now;
        $rec->save();

        $mt_status = $status = 'Completed' ? 'successful' : 'failed';
        MasterTask::find($task_id)->update(['last_status' => $mt_status]);
    }

    public function getCoordinatorData($company_id, $type = '')
    {

        $coordinator = null;
        $user_column = null;
        $phone_column = null;
        $fax_column = 'insurance_fax_number';

        switch ($type) {
            case 'company':
                $user_column = 'company_coordinator_id';
                $phone_column = 'company_coordinator_phone';
                break;
            case 'cardata':
                $user_column = 'cardata_coordinator_id';
                $phone_column = 'cardata_coordinator_phone';
                break;
            case 'insurance':
                $user_column = 'insurance_coordinator_id';
                $phone_column = 'insurance_coordinator_phone';
                break;
            case 'license':
                $user_column = 'cardata_coordinator_id';
                $phone_column = 'cardata_coordinator_id';
                $fax_column = 'license_fax_number';
                break;
            case 'miroute':
                $user_column = 'miroute_coordinator_id';
                $phone_column = 'miroute_coordinator_phone';
                break;
            default:
                $option_query = <<<EOQ
SELECT account_management_responsibility 
FROM company_options 
WHERE company_id = {$company_id} AND deleted_at IS NULL
EOQ;
                $company_option = \DB::select(\DB::raw($option_query));
                $responsibility = $company_option[0]->account_management_responsibility;
                if ($responsibility == 'CarData') {
                    $user_column = 'cardata_coordinator_id';
                    $phone_column = 'cardata_coordinator_phone';
                } else {
                    $user_column = 'company_coordinator_id';
                    $phone_column = 'company_coordinator_phone';
                }
                break;
        }

        $company_query = <<<EOQ
SELECT $phone_column AS coordinator_phone, $fax_column AS coordinator_fax,  u.email AS coordinator_email, CONCAT(u.first_name,' ',u.last_name) AS coordinator_name 
FROM companies c JOIN company_coordinators AS o ON o.company_id = c.id AND o.deleted_at IS NULL 
LEFT JOIN users AS u ON u.id = {$user_column} 
WHERE c.id = {$company_id}
EOQ;
        $companies = \DB::select(\DB::raw($company_query));
        if (is_array($companies)) {
            $company = $companies[0];
            $coordinator = (object)[
                'name' => $company->coordinator_name,
                'email' => $company->coordinator_email,
                'phone' => $company->coordinator_phone,
                'fax' => $company->coordinator_fax
            ];
        }

        return $coordinator;
    }


    public function getDriverTimeZone($user_id)
    {
        $address = Address::where('user_id', $user_id)->first();
        if ($address->country_id == USA) {
            $zip = ZipCode::find($address->zip_postal);
            return TimeZone::find($zip->time_zone_id);
        } else {
            return TimeZone::find($this->mapProvinceToTimeZone($address->state_province_id)); // TODO: Until we do not know how to resolve it
        }
    }

    // TODO replace this with timezones assigned to postal codes.
    private function mapProvinceToTimeZone($province_id)
    {
        switch ($province_id) {
            case 104:
            case 107:
            case 110:
                return 4;
                break;
            case 108:
            case 109:
            case 111:
                return 5;
                break;
            case 103:
            case 112:
                return 6;
                break;
            case 101:
            case 106:
                return 7;
                break;
            case 102:
            case 113:
                return 8;
                break;
            case 105:
                return 11;
                break;
        }
    }

    public static function delete_older_than($dir, $max_age)
    {
        \Log::info('deleting older logs', ['dir' => $dir, 'max_age' => $max_age]);
        $list = array();

        $limit = time() - $max_age;

        $dir = realpath($dir);

        \Log::info('dir ' . $dir);

        if (!is_dir($dir)) {
            return;
        }

        $dh = opendir($dir);
        if ($dh === false) {
            return;
        }

        while (($file = readdir($dh)) !== false) {
            $file = $dir . '/' . $file;
            if (!is_file($file)) {
                continue;
            }

            if (filemtime($file) < $limit) {
                $list[] = $file;
                unlink($file);
            }

        }
        closedir($dh);
        return $list;
    }

    public function divisionAndUserFilters($user_id, $division_id, $user_type, $division_column, $user_column)
    {
        /*\Log::info('divisionAndUserFilters', compact('user_id', 'division_id', 'user_type', 'division_column', 'user_column'));*/
        $division_clause = '';
        $driver_clause = '';
        $div_arr = [];

        if ($user_type == 'super') {

            $division_clause = $division_id > 0 ? " AND $division_column = $division_id " : '';

        } else if ($user_type == 'administrator') {

            if ($division_id > 0) {
                $division_clause = " AND $division_column = $division_id ";
            } else {
                $administrator_profile = AdministratorProfile::where('user_id', $user_id)->first();
                if ($administrator_profile->assigned_divisions == 'some') {
                    $divisions = $administrator_profile->divisions();
                    foreach ($divisions as $division) {
                        $div_arr[] = $division->id;
                    }
                    $div_arr = array_unique($div_arr);
                    $division_clause = " AND $division_column IN ( 0, " . implode(',', $div_arr) . ') ';
                } else if ($administrator_profile->assigned_divisions == 'none') {
                    $division_clause = " AND 1 = 0 ";
                }
            }

        } else if ($user_type == 'manager') {

            if ($division_id > 0) {
                $division_clause = " AND $division_column = $division_id ";
            }

            $manager_profile = ManagerProfile::where('user_id', $user_id)->first();
            $manager_drivers = $manager_profile->drivers();
            \Log::info('Got Manager Drivers', $manager_drivers->toArray());
            $drivers_id = [0];
            foreach ($manager_drivers as $md) {
                $drivers_id[] = $md->id;
            }
            $driver_clause = " AND $user_column in (" . implode(',', $drivers_id) . ") ";

        } else if ($user_type == 'driver') {
            $driver_clause = " AND $user_column = $user_id ";
        }

        return (object)['division' => $division_clause, 'driver' => $driver_clause];
    }

}
