<?php namespace App\Http\Traits;

use Carbon\Carbon;
use Auth;
use App\User;
use App\DriverProfile;

trait ReimbursementTraits
{
    /**
     * Generates Reimbursement for a single driver and insert or update the appropriate monthly_reimbursements record
     *
     * The reason generateReimbursement doesn't call this, is because generateReimbursement is optimized to use
     * only two queries and the are tightly coupled. In the case of a single driver, using the eloquent relationships
     * will not cause a performance reduction
     *
     * Note that this does not set the month previous itself.
     * When we call generateReimbursementForDriver in April for example, it is really calculating reimbursement based on
     * March fuel prices. The generateReimbursement function takes care of this.
     *
     */
    public function generateReimbursementForDriver($user_id, $year, $month)
    {
        // get the user
        $user = User::find($user_id);

        app('\App\Http\Controllers\ReimbursementController')->generateReimbursement($year, $month, $user->driverProfile->company->id, $user_id);
    }

    public function getYearlyVariableReimbursementTotalForDriver($user_id, $year)
    {
        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $new_months = [];
        foreach ($months as $index => $month) {
            $new_months[] = $month;
            //$this->calculateReimbursementData($user_id, $year, $month);
        }

        \Debugbar::info('new_months: ' . $new_months);
    }


    /**
     * Calculates reimbursement data for a given driver, at a given point in time.
     *
     * Note that this function will roll the date back to the previous month. This is because variable reimbursement
     * is always based on the previous month's fuel prices. Therefore, when we getDataForReimbursementFromDriver
     * this data is in fact in the previous month.
     *
     * @param $user_id
     * @param $year
     * @param $month
     * @return array
     */
    public function calculateReimbursementData($user_id, $year, $month)
    {

        if ($year == 0) {
            $year = date("n") == 1 ? date("Y") - 1 : date("Y");
        } else {
            $year = $month == 1 ? $year - 1 : $year;
        }
        if ($month == 0) {
            $month = date("n") == 1 ? 12 : date("n") - 1;
        } else {
            $month = $month == 12 ? 1 : $month - 1;
        }

        // get the user
        $user = User::find($user_id);

        // fetch all required data used in the calculation of reimbursement
        $reimbursementData = app('\App\Http\Controllers\ReimbursementController')->getDataForReimbursementFromDriver($year, $month, $user->driverProfile->company->options->service_plan, $user->driverProfile->id);

        /**
         * calculate the fixed and reimbursement based upon reimbursementData, and DO NOT alter the database
         * Jerzy's "Proxy" methods ensure no inserts or updates take place
         */
        $fixedReimbursementData = app('\App\Http\Controllers\ReimbursementController')->calculateFixedReimbursementProxy($reimbursementData);
        $variableReimbursementData = app('\App\Http\Controllers\ReimbursementController')->calculateVariableReimbursementProxy($reimbursementData);

        // attach business use percent to both the fixed and variable reimbursement objects
        $variableReimbursementData['variableReimbursementDetails']['business_use_percent'] = $user->driverProfile->vehicleProfile->business_use_percent;
        $fixedReimbursementData['fixedReimbursementDetails']['business_use_percent'] = $user->driverProfile->vehicleProfile->business_use_percent;


        return [
            'fixedReimbursementData' => $fixedReimbursementData,
            'variableReimbursementData' => $variableReimbursementData
        ];
    }

    /**
     * This function will insert the reimbursement details record, and return that id.
     * It is a requirement that the user's active driverProfile does not have a reimbursement_detail_id present.
     * If it does, call updateReimbursementDetails instead.
     *
     * NOTE: This function requires $reimbursementData, which is the result of the function calculateReimbursementData
     *
     * @return reimbursement_details_id
     */
    public function insertReimbursementDetails($user_id, $reimbursementData)
    {

        // get the impersonated user, or the current user
        $user = User::find($user_id);

        if ($user->driverProfile->reimbursement_detail_id) {
            abort(401, 'The driver profile associated with this user already has an attached reimbursement_details record. Function insertReimbursementDetails (ReimbursementTraits.php) aborted.');
        }

        return app('\App\Http\Controllers\ReimbursementController')->insertOrUpdateReimbursementDetails(
            $user->driverProfile->id,
            $user->driverProfile->vehicleProfile->business_use_percent,
            $reimbursementData['fixedReimbursementData'],
            $reimbursementData['variableReimbursementData'] );
    }

    /**
     * This function will update the reimbursement details record, and return that id.
     * It is a requirement that the user's active driverProfile have a reimbursement_detail_id present.
     * If it does not, call insertReimbursementDetails instead.
     *
     *
     * @return reimbursement_details_id
     */
    public function updateReimbursementDetails($user_id, $reimbursementData)
    {

        // get the impersonated user, or the current user
        $user = User::find($user_id);

        if (!$user->driverProfile->reimbursement_detail_id) {
            abort(401, 'The driver profile associated with this user does not have a reimbursement_details record. Function updateReimbursementDetails (ReimbursementTraits.php) aborted.');
        }

        return app('\App\Http\Controllers\ReimbursementController')->insertOrUpdateReimbursementDetails(
            $user->driverProfile->id,
            $user->driverProfile->vehicleProfile->business_use_percent,
            $reimbursementData['fixedReimbursementData'],
            $reimbursementData['variableReimbursementData'] );
    }


    public function isNecessaryToCalculateReimbursement()
    {
        // is it necessary
        $necessary = true;

        // do stuff

        return $necessary;
    }

}
