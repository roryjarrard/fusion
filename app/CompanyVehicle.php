<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyVehicle extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id', 'vehicle_responsibility', 'enforce_vehicle_age', 'enforce_car_policy_acceptance'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($companyVehicle) {
            \Log::info('in companyVehicle model created');
        });

        static::updated(function ($companyVehicle) {
            \Log::info('in companyVehicle model updated');
        });

        static::updating(function ($companyVehicle) {
            \Log::info('in companyVehicle model updating');
        });

        static::deleted(function ($companyVehicle) {
            \Log::info('in companyVehicle model deleted');
        });
    }
}
