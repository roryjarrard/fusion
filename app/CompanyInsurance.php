<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyInsurance extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id',  'insurance_responsibility',
        'notify_insurance_expiry', 'remove_reimbursement_without_insurance', 'insurance_review_interval', 'insurance_review_day', 'insurance_review_month',
        'insurance_fax_number', 'initial_insurance_grace_end_date', 'new_driver_grace_days',
        'bodily_liability_per_person', 'bodily_liability_per_accident', 'property_damage', 'deductible',
        'comprehensive_deductible', 'collision_deductible', 'medical', 'uninsured_per_person', 'uninsured_per_accident',
        'public_liability', 'business_use_required', 'additional_insured_additional_interest'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function defaultDateOfNextReview()
    {
        // if the review interval is not annually return null, there is no default for this company.
        if ( $this->insurance_review_interval != 'Annually') {
            return null;
        }

        $today = date('Y-m-d');

        $year = date('Y');

        $default_date_of_next_review = date('Y-m-d', mktime(0,0,0, $this->insurance_review_month, $this->insurance_review_day, $year));

        if ( $default_date_of_next_review <= $today  ) {
            $year = (int)$year + 1;
            $default_date_of_next_review = date('Y-m-d', mktime(0,0,0, $this->insurance_review_month, $this->insurance_review_day, $year));
        }

        return $default_date_of_next_review;
    }


}
