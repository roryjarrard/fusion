<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class NotificationTemplate extends Model
{
    protected $fillable = [
        'creator', 'area', 'severity', 'title', 'content', 'icon', 'link', 'dismissible'
    ];

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Send notifications to users
     *
     * @param $toBeNotified
     *  Either a single user_id, or an array of user_ids
     *
     * @param $visible_to - optional
     *  'all', 'driver', 'manager', 'admin', 'super',
     *  default is 'all'
     *
     * @param custom note is optional.
     */
    public function fusionNotify($toBeNotified, $note = null, $visible_to = 'all')
    {
        if (is_array($toBeNotified)) {

            foreach ($toBeNotified as $user_id) {
                \DB::table('notifications')
                    ->insert([
                        'template_slug' => $this->slug,
                        'user_id' => $user_id,
                        'area' => $this->area,
                        'severity' => $this->severity,
                        'title' => $this->title,
                        'content' => $this->content,
                        'icon' => $this->icon,
                        'link' => $this->link,
                        'visible_to' => $visible_to,
                        'note' => $note
                    ]);
            }
        } else if (is_numeric($toBeNotified)) {
            \DB::table('notifications')
                ->insert([
                    'template_slug' => $this->slug,
                    'user_id' => $toBeNotified,
                    'area' => $this->area,
                    'severity' => $this->severity,
                    'title' => $this->title,
                    'content' => $this->content,
                    'icon' => $this->icon,
                    'link' => $this->link,
                    'visible_to' => $visible_to,
                    'note' => $note
                ]);
        } else {
            return response(['errors' => ['Notification Failed']], 400);
        }
    }

}
