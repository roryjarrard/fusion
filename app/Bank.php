<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = ['country_id', 'status', 'bank_name', 'immediate_destination', 'immediate_origin',
        'immediate_origin_name', 'company_id', 'company_name', 'originating_dfi', 'originator_id', 'originator_name',
        'transit_number', 'account_number'];

    public function company()
    {
        return $this->belongsToMany('App\Company', 'company_banks', 'bank_id', 'company_id')->withPivot('deleted_at');
    }

}
