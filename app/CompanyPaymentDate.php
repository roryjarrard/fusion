<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPaymentDate extends Model
{
    protected $fillable = ['company_id', 'year', 'month', 'banking_lock_date', 'driver_payment_date',
        'received_date', 'approved_date', 'created_at', 'updated_at'];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
