<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyFee extends Model
{
    protected $fillable = ['company_id', 'from_date', 'to_date', 'fee', 'amount'];

    public $timestamps = false;

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
