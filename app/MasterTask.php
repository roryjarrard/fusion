<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterTask extends Model
{
    protected $fillable = ['name', 'script', 'description', 'interval', 'minute', 'hour', 'day', 'day_of_week',
        'month', 'last_run', 'last_status'];
}