<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleResaleValueCA extends Model
{
    protected $table = 'kbb_vehicles_ca';
    protected $fillable = ['vehicle_id', 'resale_year', 'kbb_model_id', 'kbb_equipment_class_code',
        'kbb_sequence_key', 'value_condition_id', 'base_cost', 'equipment_adj', 'region_id', 'state_id'];

}
