<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyPayments extends Model
{
    use SoftDeletes;

    protected $fillable = ['payment_responsibility', 'direct_pay_driver_fee', 'direct_pay_file_fee', 'additional_fees',
        'use_advance_payment', 'override_fixed_rates', 'override_variable_rates'];
}
