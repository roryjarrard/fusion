<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyContract extends Model
{
    use SoftDeletes;

    protected $fillable = ['contract_id', 'company_id', 'note', 'deleted_at', 'created_at', 'updated_at'];
}
