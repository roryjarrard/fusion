<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyMileage extends Model
{
    protected $fillable = [
        'user_id',
        'trip_date',
        'last_state',
        'destination',
        'business_purpose',
        'starting_odometer',
        'ending_odometer',
        'business_mileage',
        'commuter_mileage',
        'personal_mileage',
        'gap_mileage'
    ];

    public function trips() {
        return $this->hasMany('App\DailyTrip');
    }
}
