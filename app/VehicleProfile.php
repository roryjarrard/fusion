<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleProfile extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'vehicle_id',
        'company_id',
        'name_postfix',
        'retention',
        'max_vehicle_age',
        'capital_cost',
        'resale_condition_id',
        'business_use_percent',
        'calculate_fixed',
        'preset_fixed',
        'calculate_variable',
        'preset_variable',
        'capital_cost_adj',
        'fuel_economy_adj',
        'maintenance_adj',
        'repair_adj',
        'depreciation_adj',
        'insurance_adj',
        'fuel_price_adj',
        'resale_value_adj',
        'capital_cost_tax_adj',
        'monthly_payment_adj',
        'finance_cost_adj',
        'fee_renewal_adj',
        'fixed_adj',
        'variable_adj',
        'fixed_adj_text',
        'variable_adj_text',
        'business_use_percent_back',
        'proposed',
        'created_by'
    ];

    public static function boot() {

        parent::boot();

        static::created(function($vehicleProfile) {
            \Event::fire('vehicleProfile.created', $vehicleProfile);
        });

        static::updated(function($vehicleProfile) {
            \Event::fire('vehicleProfile.updated', $vehicleProfile);
        });

        static::deleted(function($vehicleProfile) {
            \Event::fire('vehicleProfile.deleted', $vehicleProfile);
        });
    }

    public function vehicle() {
        return $this->belongsTo('App\Vehicle', 'vehicle_id', 'id');
    }

    public function company() {
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }

    public function driverProfiles() {
        return $this->hasMany('App\DriverProfile');
    }

    public function driverCount($active = null) {
        // active returns the count of active drivers
        if ($active == true) {
            return $this->driverProfiles()->where('active', 1)->whereNull('deleted_at')->count();
        }

        return $this->driverProfiles()->count();
    }

    public function resaleCondition() {
        return $this->belongsTo('App\ResaleCondition', 'resale_condition_id', 'id');
    }

    /**
     * @param User $user
     *
     * @return array['maximum_vehicle_age','minimum_vehicle_cost'];
     */
    public function FAVR_Limits(User $user){
        $return = ['maximum_vehicle_age' => null, 'minimum_vehicle_cost' => null];

        $this->vehicle();
    }

    public function variableAdjustments() {
        return $this->belongsToMany('App\VariableAdjustment','vehicle_profile_variable_adjustment')
                    ->withPivot('deleted_at', 'created_at', 'updated_at')
                    ->whereNull('deleted_at');
    }

    public function approval() {
        return $this->hasOne('App\VehicleProfileApproval');
    }

    public function creator() {
        return User::where('id', $this->created_by)->first();
    }

    public function note() {
        return $this->hasOne('App\Note', 'vehicle_profile_id', 'id');
    }

    public function name() {
        $vehicle = $this->vehicle;
        $profile = $vehicle->year . ' ' . trim($vehicle->name);
//        if (!empty($vehicle->rank)) $profile .= " [$vehicle->rank] "; TODO commented out for add many drivers, we might need this in the future...
        $profile .= $this->name_postfix;
        return trim($profile);
    }

}
