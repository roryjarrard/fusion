<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyTaxQuarter extends Model
{
    protected $fillable = ['company_id', 'tax_quarter_id', 'valid_until_year', 'created_at', 'updated_at'];
}
