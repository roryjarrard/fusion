<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleCategoryInsuranceAdjustment extends Model
{
    protected $fillable = ['vehicle_category_id', 'year', 'insurance_adjustment'];
}
