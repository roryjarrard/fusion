<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MasterTaskWeekly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MasterTask:weekly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Master Tasks that run weekly';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // grab date first to comply with cron time
        $date = new \DateTime();

        $minute = $date->format('i');
        $hour = $date->format('H');
        $day_of_week = $date->format('l'); // lowercase L

        $commands = \DB::table('master_tasks')->where('interval', 'weekly')->whereNull('deleted_at')->get();

        foreach ($commands as $command) {
            $run = in_array($minute, explode(',', $command->minute));
            $run = $run && in_array($hour, explode(',', $command->hour));
            $run = $run && in_array($day_of_week, explode(',', $command->day_of_week));
            if ($run) {
                // run task
            }
        }
    }
}
