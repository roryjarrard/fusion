<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MasterTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MasterTask:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'MasterTask will check DB for any tasks needed to be run this minute.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // grab date immediately, then test on date components
        $date = new \DateTime();

        $minute = $date->format('i');
        $hour = $date->format('H');
        $day = $date->format('d');
        $day_of_week = $date->format('l'); // lowercase L
        $month = $date->format('m');

        $commands = \DB::table('master_tasks')->get();
        foreach ($commands as $command):
            $run = true;

            switch ($command->interval):
                case 'hourly':
                    $run = $this->testMinute($minute, $command->minute);
                    break;
                case 'daily':
                    $run = $this->testHour($hour, $command->hour);
                    if ($run && !empty($command->minute)) {
                        $run = $this->testMinute($minute, $command->minute);
                    } else { $run = false; }
                    break;
                case 'weekly':
                    $run = $this->testDay($day_of_week, $command->day_of_week);
                    if ($run && !empty($command->hour)) {
                        $run = $this->testHour($hour, $command->hour);
                    } else { $run = false; }
                    if ($run && !empty($command->minute)) {
                        $run = $this->testMinute($minute, $command->minute);
                    } else { $run = false; }
                    break;
                case 'monthly':
                    $run = $this->testDay($day, $command->day);
                    if ($run && !empty($command->hour)) {
                        $run = $this->testHour($hour, $command->hour);
                    } else { $run = false; }
                    if ($run && !empty($command->minute)) {
                        $run = $this->testMinute($minute, $command->minute);
                    } else { $run = false; }
                    break;
                case 'annually':
                    $run = $this->testMonth($month, $command->month);
                    if ($run && !empty($command->day)) {
                        $run = $this->testDay($day, $command->day);
                    } else { $run = false; }
                    if ($run && !empty($command->hour)) {
                        $run = $this->testHour($hour, $command->hour);
                    } else { $run = false; }
                    if ($run && !empty($command->minute)) {
                        $run = $this->testMinute($minute, $command->minute);
                    } else { $run = false; }
                    break;
                default:
                    break;
            endswitch;

            if ($run) {
                exec('/home/cdstaging/master_tasks/' . $command->script . '.php');
            }
        endforeach;


    }

    protected function testMinute($minute, $minuteArray)
    {
        return in_array($minute, explode(',', $minuteArray));
    }

    protected function testHour($hour, $hourArray)
    {
        return in_array($hour, explode(',', $hourArray));
    }

    protected function testDay($day, $dayArray)
    {
        return in_array($day, explode(',', $dayArray));
    }

    protected function testMonth($month, $monthArray)
    {
        return in_array($month, explode(',', $monthArray));
    }
}
