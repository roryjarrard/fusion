<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\DailyMasterTaskController;

class MasterTaskDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MasterTask:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Master Tasks that run daily';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // grab date first to comply with cron time
        $date = new \DateTime();
        $i = date("i");
        //if( $i%10 == 0 ) {
        //    echo "running this now at " . $date->format("H:i:s") . "\n";
        //}
        $minute = $date->format('i');
        $hour = $date->format('H');

        $commands = \DB::table('master_tasks')->where('interval', 'daily')->whereNull('deleted_at')->get();

        foreach ($commands as $command) {
            $run = in_array($minute, explode(',', $command->minute));
            $run = $run && in_array($hour, explode(',', $command->hour));
            if ($run) {
                echo date("Y-m-d H:i:s ") . "Pass $command->id and $command->name to $command->script at " .  $date->format("H:i:s") . "\n";
                $controller = new DailyMasterTaskController();
                $controller->{$command->script}($command->id, $command->name);
//                $this->call('\\App\\Http\\Controllers\\MasterTaskController@' . $command->script);

            }
        }
    }
}
