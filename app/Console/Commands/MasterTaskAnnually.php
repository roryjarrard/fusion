<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\AnnualMasterTaskController;

class MasterTaskAnnually extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MasterTask:annually';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Master Tasks that run annually';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new \DateTime();
        $i = date("i");
        //if( $i%10 == 0 ) {
        //    echo "running this now at " . $date->format("H:i:s") . "\n";
        //}

        $month = $date->format('m');
        $day = $date->format('d');
        $hour = $date->format('H');
        $minute = $date->format('i');

        $commands = \DB::table('master_tasks')->where('interval', 'annually')->whereNull('deleted_at')->get();

        foreach ($commands as $command) {
            $run = in_array($month, explode(',', $command->month));
            $run = $run && in_array($day, explode(',', $command->day));
            $run = $run && in_array($hour, explode(',', $command->hour));
            $run = $run && in_array($minute, explode(',', $command->minute));
            if ($run) {
                echo date("Y-m-d H:i:s ") . "Pass $command->id and $command->name to $command->script at " . $date->format("H:i:s") . "\n";
                $controller = new AnnualMasterTaskController;
                $controller->{$command->script}($command->id, $command->name);
//                $this->call('\\App\\Http\\Controllers\\MasterTaskController@' . $command->script);

            }
        }
    }
}
