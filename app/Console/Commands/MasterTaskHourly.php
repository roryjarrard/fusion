<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\HourlyMasterTaskController;

class MasterTaskHourly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MasterTask:hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Master Tasks that run hourly';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // grab date first to comply with cron time
        $date = new \DateTime();

        $minute = $date->format('i');

        $commands = \DB::table('master_tasks')->where('interval', 'hourly')->whereNull('deleted_at')->get();

        foreach ($commands as $command) {
            if (in_array($minute, strpos(',',$command->minute) === false ? [$command->minute] : explode($command->minute))) {
                echo date("Y-m-d H:i:s ") . "Pass $command->id and $command->name to $command->script at " .  $date->format("H:i:s") . "\n";
                $controller = new HourlyMasterTaskController();
                $controller->{$command->script}($command->id, $command->name);
            }
        }
    }
}
