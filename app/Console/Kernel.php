<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\MasterTaskHourly',
        'App\Console\Commands\MasterTaskDaily',
        'App\Console\Commands\MasterTaskWeekly',
        'App\Console\Commands\MasterTaskMonthly',
        'App\Console\Commands\MasterTaskAnnually'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $date = date('Y-m-d');
        $schedule->command('MasterTask:hourly')->appendOutputTo(config('app.vhost_root') . '/logs/hourly_master_task.' . $date . '.log');
        $schedule->command('MasterTask:daily')->appendOutputTo(config('app.vhost_root') . '/logs/daily_master_task.' . $date . '.log');
        $schedule->command('MasterTask:weekly')->appendOutputTo(config('app.vhost_root') . '/logs/weekly_master_task.' . $date . '.log');
        $schedule->command('MasterTask:monthly')->appendOutputTo(config('app.vhost_root') . '/logs/monthly_master_task.' . $date . '.log');
        $schedule->command('MasterTask:annually')->appendOutputTo(config('app.vhost_root') . '/logs/annually_master_task.' . $date . '.log');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
