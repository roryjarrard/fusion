<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DriverStop extends Model
{
    use SoftDeletes;
    protected $table = 'driver_stop';
    protected $fillable = ['user_id', 'saved_stop_id', 'effective_date', 'deleted_at', 'created_at', 'updated_at'];
}
