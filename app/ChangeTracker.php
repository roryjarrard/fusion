<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeTracker extends Model
{
    /**
     * How does it work? In each model there is a boot() function that extends the method, then looks for
     * created, updated and deleted Events.
     *
     * Each model may have specific implementations. However, generally it will follow these patterns
     *   - created: not tracking created, as this is not a "change", and the created_at date on the record is enough
     *   - updated: get an array of old and new values. loop through, and create a ChangeTracker instance for each difference
     *   - deleted: just report that the entire record was deleted
     *
     * Properties:
     * - target: the id of the record being altered
     * - author: the signed-in user id (may be null if system is creating the action)
     * - company_id: attach if available for easier front-end filtering
     * - model: the name of the model used by the changed record
     * - attribute: the database column name holding the changed value
     * - action: created, updated or deleted
     * - old_value: previous value being changed (may be null for created)
     * - new_value: updated value (may be null for deleted)
     * - notes: legacy column, but could be used for extra information
     */

    protected $fillable = ['target', 'author', 'company_id', 'model', 'attribute', 'action', 'old_value', 'new_value', 'notes'];

    public function company() {
        return $this->hasOne('App\Company');
    }
}
