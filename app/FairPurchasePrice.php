<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FairPurchasePrice extends Model
{
    protected $fillable = ['vehicle_id', 'zip', 'adjustment_amount'];
}
