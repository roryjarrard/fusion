<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverInsuranceReview extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'company_id',
        'next_review_date',
        'expiration_date',
        'approved', // null = not reviewed, 0 = partial rejection, 1 = approved, 2 = rejected/historical
        'approved_by',
        'approval_date',

        'bodily_liability_per_person',
        'bodily_liability_per_accident',
        'property_damage',
        'deductible',
        'comprehensive_deductible',
        'collision_deductible',
        'medical',
        'uninsured_per_person',
        'uninsured_per_accident',
        'public_liability',
        'business_use',

        'no_policy_term_dates',
        'vehicle_match',
        'document_illegible',
        'document_current',
        'name_missing',
        'other',

        'processor_id',
        'comment',
        'processing_date',
        'created_at' //  Do not delete me, needed for annual insertion of created at
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function insurance_documents()
    {
        return $this->hasMany('App\DriverInsuranceDocument', 'insurance_review_id')->orderBy('created_at', 'desc');
    }
}
