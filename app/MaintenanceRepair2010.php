<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintenanceRepair2010 extends Model
{
    protected $table = 'maintenance_repair_2010';
    protected $fillable = ['vehicle_id','state_province_id','mileage','maintenance','repair'];
}
