<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\favrParameter;
use App\Address;
use App\StateSalesTax;
use App\CountyTax;
use App\ZipCode;
use App\TaxesFeesCA;
use Carbon\Carbon;

class DriverProfile extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'active', 'company_id', 'division_id',
        'street_light_id', 'employee_number', 'start_date', 'stop_date',
        'job_title', 'address_id', 'vehicle_profile_id', 'personal_vehicle_id',
        'cost_center', 'mileage_band_id', 'territory_type_id', 'territory_list',
        'editing_user_id', 'fuel_city_id', 'car_policy_accepted', 'reimbursement_detail_id', 'miroute_status',
        'insurance_review_id', 'license_review_id', 'next_insurance_review_date', 'next_license_review_date',
        'deleted_at'
    ];


    public static function boot() {

        parent::boot();

        static::created(function($driverProfile) {
            \Event::fire('driverProfile.created', $driverProfile);
        });

        static::updated(function($driverProfile) {
            \Event::fire('driverProfile.updated', $driverProfile);
        });

        static::deleted(function($driverProfile) {
            \Event::fire('driverProfile.deleted', $driverProfile);
        });
    }



    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function personalVehicle()
    {
        return $this->belongsTo('App\PersonalVehicle');
    }

    public function vehicleProfile()
    {
        return $this->belongsTo('App\VehicleProfile');
    }

    public function address()
    {
        //return $this->hasOne('App\Address');

        return Address::where('id', $this->address_id)->first();
    }

    public function division()
    {
        return $this->belongsTo('App\Division');
    }

    public function market()
    {
        return $this->belongsTo('App\Market');
    }

    public function managers()
    {
        return ManagerProfile::join('manager_driver', 'manager_driver.manager_id', 'manager_profiles.manager_id')
                               ->where('manager_driver.driver_id', $this->user_id)
                               ->get();
    }

    public function manager($manager_id)
    {
        return ManagerProfile::where('manager_id', $manager_id)->first();
    }

    public function mileageBand()
    {
        return $this->belongsTo('App\MileageBand');
    }

    /**
     * Calculates limits imposed by FAVR for minimum vehicle cost and minimum vehicle year.
     * Failing these conditions can exclude driver from FAVR program or reduce fixed reimbursement.
     * Driver status is marked as Green-Vehicle-Too-Old.
     * Maximum vehicle age, if provided is the client's imposed restriction
     *
     * Since 2014,  maximum taxes are calculated based driver address state and county.
     * Before 2014, all drivers had the same maximum tax found for combination state and county.
     *
     * @return mixed['vehicle_cost','minimum_year','max_
     */
    public function vehicleFavrParameters()
    {
        if (empty($this->vehicleProfile)) {
            return;
        }
        $vehicle_profile= $this->vehicleProfile;

        $vehicle = $vehicle_profile->vehicle;

        $favr = favrParameter::find($vehicle->year);

        if( $vehicle->year > 2013 ) {    // instead of taking maximum tax from all states and counties, base it on driver address
            $state_sales_tax = TaxesFeesUS::where(['year'=>$vehicle->year, 'state_province_id'=>$this->address()->state_province_id])->pluck('sales_tax')->first();
            $county_id = ZipCode::where('zip_code',$this->address()->zip_postal)->pluck('county_id')->first();
            $county_sales_tax = CountyTax::where(['county_id'=>$county_id,'year'=>$vehicle->year])->pluck('county_sales_tax')->first();
            $sales_taxes = $state_sales_tax/100 + $county_sales_tax/100 ;

        } else {
            $sales_taxes = $favr->maximum_tax_rate;
        }

        $minimum_vehicle_cost = round($vehicle->invoice * ( 1 + $sales_taxes ) * ( 1 + $vehicle_profile->capital_cost_adj/100) * $favr->vehicle_cost_percentage * $favr->profile_vehicle_percentage);
        $minimum_year = Carbon::today()->year - $vehicle_profile->max_vehicle_age;

        \Debugbar::info([$minimum_vehicle_cost, $minimum_year, $vehicle_profile->max_vehicle_age]);

        return [
            'minimum_vehicle_cost'=> $minimum_vehicle_cost,
            'minimum_year' => $minimum_year,
            'maximum_vehicle_age' => $vehicle_profile->max_vehicle_age,
            'minimum_vehicle_cost_formatted'=> number_format($minimum_vehicle_cost)
        ];

    }

    /**
     * Gets (or checks) if the driver has a declaration in the current year
     *
     * @return null or an AnnualIrsOdometerDeclaration
     */
    public function annualIrsOdometerDeclaration()
    {
        return AnnualIrsOdometerDeclaration::where('user_id', $this->user_id)
            ->whereYear('created_at', '=', date('Y'))
            ->first();
    }

    /**
     * Gets all declarations for the current driver
     *
     * @return array of  AnnualIrsOdometerDeclarations
     */
    public function allAnnualIrsOdometerDeclarations()
    {
        return AnnualIrsOdometerDeclaration::where('user_id', $this->user_id)
            ->orderBy('created_at', 'desc')
            ->get();
    }


    public function notes()
    {
        return Note::where('driver_id', $this->user_id)->get();
    }

    /**
     * Returns a monthly reimbursement record for the given driver on provided year and month.
     * If with details is provided, the return will be merged with details.
     *
     * @param $year - REQUIRED
     * @param $month - REQUIRED
     * @param bool $with_details - OPTIONAL default false
     * @return MonthlyReimbursement|Model|null
     */
    public function reimbursement($year, $month, $with_details = false)
    {
        $reimbursement = MonthlyReimbursement::where('user_id', $this->user_id)
            ->where('year', $year)
            ->where('month', $month)
            ->first();

        if ($with_details) {
            // this will work in the future when we have past reimbursement details records
            // get the driver profile associated with a reimbursement, figure out the reimbursement detail id from that
            // driver profile, and then get the reimbursement detail record
            $details = ReimbursementDetail::find(DriverProfile::find($reimbursement->driver_profile_id)->reimbursement_detail_id);
            $reimbursement = array_merge($details->toArray(), $reimbursement->toArray());
        }

        return $reimbursement;
    }

    public function streetLight() {
        return $this->belongsTo('App\DriverStreetLightStatus');
    }

    public function registrationCompletion() {
        return $this->hasOne('App\DriverRegistrationCompletion', 'user_id', 'user_id');
    }

    public function insuranceReview() {
        return $this->hasOne('App\DriverInsuranceReview', 'id', 'insurance_review_id');
    }

}
