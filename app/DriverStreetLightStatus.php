<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverStreetLightStatus extends Model
{
    /**
     * id, color, title, description, service_plan, insurance_related, license_related, created_at, updated_at
     */
    protected $fillable = [
        'id',
        'color',
        'title',
        'description',
        'service_plan',
        'insurance_related',
        'license_related',
        'active_related'
    ];
}
