<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverTaxColumn extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;
}
