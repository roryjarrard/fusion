<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxColumn extends Model
{
    protected $table = 'driver_tax_columns';

    protected $fillable = ['name'];
}

