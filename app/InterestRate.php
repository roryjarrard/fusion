<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterestRate extends Model
{
    protected $fillable = ['country_id', 'year', 'interest_rate'];
}
