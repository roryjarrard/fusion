<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerritoryType extends Model
{
    protected $fillable = ['name'];
}
