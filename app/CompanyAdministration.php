<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CompanyAdministration extends Model
{
    protected $fillable = ['company_id', 'management', 'driver_calls_and_emails',
        'driver_changes', 'account_maintenance', 'monthly_report_review', 'traffic_light_status'];

    /**
     * These event listeners do not have to be "registered" anywhere. However,
     * the model has to be "pulled" to trigger the event.
     *
     * Example:
     * $model->where('relation_id', 100)->delete() WILL NOT trigger
     * $model->where('relateion_id', 100)->first()->delete() WILL trigger
     *
     * Or use get() and loop if multiple instances.
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Deprecated for now (there is no "change" so no need for a change tracker.
         * Upon creation we would not have 'old' values
         */
        /*static::created(function (CompanyAdministration $model) {

        });*/

        /**
         * When updating, get original values, then turn both new
         * and original values to arrays. Loop through keys, then
         * compare new and original values, and create a ChangeTracker
         * for every change.
         *
         * Attach company_id to all changes.
         *
         * Ignoring timestamps and primary keys.
         */
        static::updated(function (CompanyAdministration $model) {
            $newValues = $model->toArray();
            $oldValues = $model->getOriginal(); // this returns an array

            $user_id = Auth::user() ?: null;

            /**
             * Loop on key=>value pairs
             */
            foreach ($newValues as $k => $v) {
                if (!in_array($k, ['id', 'created_at', 'updated_at'])) {
                    if ($v !== $oldValues[$k]) {
                        ChangeTracker::create([
                            'target' => $newValues['id'],
                            'author' => $user_id,
                            'company_id' => $newValues['company_id'],
                            'model' => 'CompanyAdministration',
                            'attribute' => $k,
                            'action' => 'updated',
                            'old_value' => $oldValues[$k],
                            'new_value' => $newValues[$k],
                            'notes' => ''
                        ]);
                    }
                }
            }
        });

        /**
         * When deleting there is no "new" values.
         */
        static::deleted(function (CompanyAdministration $model) {
            $user_id = Auth::user() ?: null;
            ChangeTracker::create([
                'target' => $model->id,
                'author' => $user_id,
                'company_id' => $model->company_id,
                'model' => 'CompanyAdministration',
                'attribute' => 'Model',
                'action' => 'deleted',
                'old_value' => $model->id,
                'new_value' => null,
                'notes' => ''
            ]);
        });
    }
}
