<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleMileageAdjustment extends Model
{
    protected $fillable = ['miles', 'model_year', 'resale_year', 'price1', 'adjustment1', 'price2', 'adjustment2',
        'price3', 'adjustment3', 'price4', 'adjustment4', 'price5', 'adjustment5', 'price6', 'adjustment6',
        'price7', 'adjustment7', 'price8', 'adjustment8',];
}
