<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyOptions extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id', 'use_company_insurance_module', 'use_company_invoicing_module',
        'use_company_license_module', 'use_company_mileage_module', 'use_company_payments_module',
        'use_company_vehicles_module', 'launch_date', 'driver_passwords_date', 'service_plan',
        'account_management_responsibility', 'driver_can_edit_profile', 'can_view_reimbursement_profile',
        'can_view_reimbursement_details', 'reports_display_manager_id', 'reports_display_job_title',
        'use_cardata_online', 'use_start_stop_dates', 'use_insurance_dates',
        'use_drivers_license_dates', 'use_stop_contract'];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
