<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyMileage extends Model
{
    protected $fillable = [
        'user_id',
        'company_id',
        'year',
        'month',
        'business_mileage',
        'personal_mileage',
        'ending_odometer'
    ];
}
