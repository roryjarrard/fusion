<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'is_demo_company'];

    private $division_id = '';

    /**
     * Returns the bank, man
     *
     * @param bool $restricted
     * @return mixed
     */
    public function bank($restricted = true)
    {
        if (!$restricted) {
            $raw_select_stmt = 'banks.*, company_banks.created_at as active_since';
        } else {
            $raw_select_stmt = 'banks.bank_name, banks.country_id, banks.status, company_banks.created_at as active_since';
        }
        return \DB::table('banks')
            ->select(\DB::raw($raw_select_stmt))
            ->join('company_banks', 'company_banks.bank_id', '=', 'banks.id')
            ->where('company_banks.company_id', $this->id)
            ->whereNull('company_banks.deleted_at')
            ->first();

    }

    public function savedStops()
    {
        return $this->hasMany('App\SavedStop');
    }

    public function options()
    {
        return $this->hasOne('App\CompanyOptions');
    }

    public function vehicles() {
        return $this->hasOne('App\CompanyVehicle');
    }

    public function invoicing()
    {
        return $this->hasOne('App\CompanyInvoicing');
    }

    public function payments()
    {
        return $this->hasOne('App\CompanyPayments');
    }

    public function dates() {
        return $this->hasOne('App\CompanyDates');
    }

    public function contacts()
    {
        return $this->hasMany('App\CompanyContact');
    }

    public function coordinators()
    {
        return $this->hasOne('App\CompanyCoordinators');
    }

    public function mileage()
    {
        return $this->hasOne('App\CompanyMileage');
    }

    public function administration()
    {
        return $this->hasOne('App\CompanyAdministration');
    }

    public function license()
    {
        return $this->hasOne('App\CompanyLicense');
    }

    public function insurance()
    {
        return $this->hasOne('App\CompanyInsurance');
    }

    public function paymentHistory()
    {
        return $this->hasOne('App\CompanyPaymentDate');
    }

    public function profile()
    {
        return $this->hasOne('App\CompanyProfile');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('role_id');
    }

    public function driverProfiles()
    {
        return $this->hasMany('App\DriverProfile');
    }

    public function drivers($division_id = '')
    {
        $this->division_id = $division_id;
        return $this->driverProfiles->map(function ($p) {
            \Debugbar::info($p);
            if ($this->division_id != '' && $p->division_id == $this->division_id) {
                return $p->user;
            } elseif ($this->division_id == '') {
                return $p->user;
            }
        });
    }

    public function driverCount()
    {
        return \DB::table('users')->join('driver_profiles', 'driver_profiles.user_id', 'users.id')->where('users.deleted_at', null)->where('driver_profiles.company_id', $this->id)->count();
    }

    public function managerProfiles()
    {
        return $this->hasMany('App\ManagerProfile');
    }

    public function managers()
    {
        return $this->managerProfiles->map(function ($p) {
            return $p->user;
        });
    }

    public function managerCount()
    {
        return \DB::table('users')->join('manager_profiles', 'manager_profiles.user_id', 'users.id')->where('users.deleted_at', null)->where('manager_profiles.company_id', $this->id)->count();
    }

    public function administratorProfiles()
    {
        return $this->hasMany('App\AdministratorProfile');
    }

    public function administrators()
    {
        return $this->administratorProfiles->map(function ($p) {
            return $p->user;
        });
    }

    public function administratorCount()
    {
        return \DB::table('users')->join('administrator_profiles', 'administrator_profiles.user_id', 'users.id')->where('users.deleted_at', null)->where('administrator_profiles.company_id', $this->id)->count();
    }

    public function divisions()
    {
        return $this->hasMany('App\Division');
    }

    public function markets()
    {
        return $this->hasMany('App\Market');
    }

    public function vehicleProfiles()
    {
        return $this->hasMany('App\VehicleProfile');
    }

    public function taxes()
    {
        return $this->hasOne('App\CompanyTaxes');
    }

    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    public function allFees()
    {
        $query = <<<EOQ
select * from company_fees 
where company_id = $this->id
order by fee desc, from_date desc
EOQ;
        return \DB::select(\DB::raw($query));
    }

    public function currentFees()
    {
        $today = date('Y-m-d');
        $query = <<<EOQ
select * from company_fees 
where company_id = $this->id
and ( (to_date >= '$today' or to_date is null) and from_date <= '$today')
EOQ;
        $unformattedFees = \DB::select(\DB::raw($query));
        $fees = new \stdClass();
        $fees->driver = new \stdClass();
        $fees->file = new \stdClass();
        $fees->invoice = new \stdClass();
        foreach ($fees as $defaultFee) {
            $defaultFee->amount = 0;
        }
        foreach ($unformattedFees as $fee) {
            $fees->{$fee->fee} = $fee;
        }

        return $fees;
    }

    /**
     * Returns the report related options present in the company_options table
     */
    public function reportOptions()
    {
        $options = new \stdClass();
        $options->reports_display_manager_id = $this->options->reports_display_manager_id;
        $options->reports_display_monthly_tax_limit = $this->taxes->reports_display_monthly_tax_limit;
        $options->reports_display_job_title = $this->options->reports_display_job_title;
        return $options;
    }

    public function reportLabels()
    {
        return $this->hasMany('\App\CompanyReportLabel');
    }
}
