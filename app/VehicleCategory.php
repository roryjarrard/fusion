<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleCategory extends Model
{
    protected $fillable = [
        'name', 'type', 'display_order'
    ];

    public function vehicles(){
        return $this->belongsToMany('App\Vehicle');
    }
}
