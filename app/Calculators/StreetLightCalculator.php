<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 17/04/2018
 * Time: 9:03 PM
 */

/** IMPORTANT ---------------------------------------------------------------------------------------------------------
 * If you need to calculate only a small part of street light, for example vehicle age compliance, you can access
 *      calculateStreetLightIdVehicleAgeCompliance(params...)
 *  But you must pass ALL required parameters. An example of how to use this function can be found inside of
 *      calculateStreetLightId($user_id)
 *
 * The function calculateStreetLightId($user_id) will calculate ALL aspects of the given user's street light,
 * EXCEPT insurance and license compliance (these will not be changed) at the
 * present time. It will return what their street_light_id should be.
 *
 * The portions of street light relating to INSURANCE and LICENSE will NOT be calculated by calculateStreetLightId($user_id)
 * When dealing with INSURANCE or LICENSE RELATED STREET LIGHTS, you must explicitly call functions such as:
 *  - calculateStreetLightIdInsuranceAwaiting to get the portion of streetlight related to 'awaiting insurance'.
 -------------------------------------------------------------------------------------------------------------------- */

namespace App\Calculators;

use App\User;
use App\DriverProfile;
use App\CompanyInsurance;
use App\CompanyLicense;
use App\DriverStreetLightStatus;

use Carbon\Carbon;

class StreetLightCalculator
{

    /**
     * Given a street light id, returns the over-arching colour
     *
     * @param $id
     * @return string
     */
    public function colorOfStreetLight($id){
        if( $id & YELLOW_NEW_DRIVER ) {
            return 'Yellow';
        } else {
            $status_arr = DriverStreetLightStatus::all();
            foreach($status_arr as $row) {
                if( ($id & $row['id']) && $row['color'] == 'Red') {
                    return 'Red';
                }
            }
        }
        return 'Green';
    }

    /**
     * decide on color ball on Monthly Reimbursement page
     * if fixed status = 2 green
     * else if color red and no grace red
     * else yellow
     *
     * @param $reimbursement_row from monthly_reimbursements table + street_light_id from driver profiles.
     * @return string
     */
    public function colorOfReimbursementStatus($reimbursement_row) {
        if( $reimbursement_row->fixed_status == GREEN_REIMBURSEMENT ) {
            return 'Green';
        } else {
            $color = $this->colorOfStreetLight($reimbursement_row->fixed_status);
            if( $color == 'Green') {
                return 'Yellow';    //  compliance problems
            } elseif( $color == 'Red' && $reimbursement_row->within_grace_period == 0) {
                return 'Red';
            } else {
                return 'Yellow';
            }
        }
    }


    /**
     * Function will return all street light objects associated with a given street_light_id
     *
     * @param $id
     * @return array
     */
    public function streetLightArray($id) {
        $arr = [];
        $status_arr = \DB::select( \DB::raw("SELECT * FROM driver_street_light_statuses 
        ORDER BY color DESC, description"));
        foreach($status_arr as $row) {
            if($id & $row->id) {
                $arr[] = $row;
            }
        }
        return $arr;
    }

    /**
     * Get color and description of streetlight
     * Get extra data if license or insurance
     * Use user_id not driver_profile_id as it is always available
     *
     * @param $user_id
     * @return array
     */
    public function driverStreetLightData($user_id)
    {
        $faxes = [];
        $insurance_related = false;
        $license_related = false;
        $street_light_id = User::find($user_id)->driverProfile->street_light_id;
        $street_arr = $this->StreetLightArray($street_light_id);
        foreach ($street_arr as $rec) {
            if ($rec->insurance_related) {
                $insurance_related = true;
            }
            if ($rec->license_related) {
                $license_related = true;
            }
        }
        if ($insurance_related || $license_related) {  // get insurance tax
            $company_id = DriverProfile::where('user_id', $user_id)->pluck('company_id')[0];
            if ($insurance_related) {
                $faxes['insurance_tax'] = CompanyInsurance::where('company_id', $company_id)->pluck('insurance_fax_number')[0];
            }
            if ($license_related) {
                $faxes['license_tax'] = CompanyLicense::where('company_id', $company_id)->pluck('license_fax_number')[0];
            }

            foreach ($street_arr as $rec) {
                if ($rec->insurance_related) {
                    $rec->description = str_replace('%fax%', $faxes['insurance_tax'], $rec->description);
                }
                if ($license_related) {
                    $rec->description = str_replace('%fax%', $faxes['license_tax'], $rec->description);
                }
            }
        }
        return $street_arr;
    }


    /**
     * Function to return the name of a given street light
     *
     * @param $id
     * @param string $glue
     * @return string
     */
    public function nameOfStreetLight($id, $glue=', '){
        $arr = [];
        $status_arr = DriverStreetLightStatus::all()->sortBy('title');
        foreach($status_arr as $row) {
            if( $id & $row['id']){
                $arr[] = ucwords($row['title']);
            }
        }
        return implode($glue, $arr);
    }

    /**
     * Returns a dictionary of the form:
     *      [ 'value' => street_light_id, 'color' => 'colour of streetlight', 'names' => name of given streetlight ]
     *
     *
     * @param $id
     * @return array
     */
    public function formattedStreetLight($id){
        // color + description
        $value = $id;
        $color = $this->colorOfStreetLight($id);
        $names = $this->nameOfStreetLight($id);
        return compact('value', 'color', 'names');
    }


    /**
     * Given a street_light_id, this function will add the passed street_light_id and remove
     *      GREEN_REIMBURSEMENT if applicable
     *
     * @param $street_light
     * @param $add_this
     * @return int
     */
    public function addToStreetLight($street_light, $add_this) {
        $new_id = $street_light;
        if (!($street_light & $add_this)) {
            $new_id += $add_this;
            if ( $new_id > GREEN_REIMBURSEMENT && $street_light & GREEN_REIMBURSEMENT)
                $new_id -= GREEN_REIMBURSEMENT;
        }
        return $new_id;
    }

    /**
     * Given a street_light_id, this function will add a red status and remove
     *      GREEN_REIMBURSEMENT if applicable
     *
     * @param int $old_status
     * @param int $add_status
     * @return int
     */
    public function addRedStatus($old_status,$add_status){
        $new_status = $old_status;
        if( ! ($old_status & $add_status)) {
            $new_status += $add_status;
            if( $old_status & GREEN_REIMBURSEMENT ) {
                $new_status -= GREEN_REIMBURSEMENT;
            }
        }
        return $new_status;
    }


    /**
     * Given a street_light_id, this function will remove the provided status, and also ensure
     *      if it was the only status set, that the street_light_id returned is GREEN_REIMBURSEMENT
     *
     * @param $street_light
     * @param $remove_this
     * @return int
     */
    public function removeFromStreetLight($street_light, $remove_this) {
        $new_id = $street_light;
        if ($street_light & $remove_this) {
            $new_id -= $remove_this;
            if ($new_id == 0)
                $new_id = GREEN_REIMBURSEMENT;
        }
        return $new_id;
    }

    /**  INCOMPLETE
     * This function will only calculate and return the appropriate street_light_id for a given user's
     * CURRENT ACTIVE driver profile.
     *
     *      Example use case:
     *          Driver Billy's got a new driver_profile record with personal_vehicle_id = 2.
     *          The new driver_profile with personal_vehicle_id 2 still has the same streetlight id as his old profile
     *          To update Billy's driver_profile.streetlight_id the following call is made:
     *
     *              $billysDriverProfile->street_light_id = calculateStreetLightId($billys_user_id);
     *
     * This function does NOT insert or update any records
     *
     * This function will assume a driver has completed registration, and if YELLOW_NEW_DRIVER was present before
     * calculation, it will not be present in the returned value.
     *
     * This function requires the user have a non-soft-deleted driver profile. If not, it will throw an exception.
     *
     * TODO: verify this: function will not calculate street light for license and insurance as those are taken care
     *          of explicitly when processing, or when performing system tasks
     *
     * @param $user_id
     * @return int
     */
    public function calculateStreetLightId($user_id)
    {

        $user = User::find($user_id);

        if (!$user->driverProfile) {
            throw new \Exception('User does not have an active driver profile.');
        }

        //initialize all required variables
        $today = Carbon::today();
        $current_year = $today->year;
        $company = $user->driverProfile->company;
        $old_street_light_id = $user->driverProfile->street_light_id;
        $new_street_light_id = $old_street_light_id;
        $service_plan = $company->options->service_plan;

        /**
         * if the driver had a yellow street_light_id, we are in the first calculation after
         * registration so we remove it
         */
        $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, YELLOW_NEW_DRIVER);


        // VEHICLE AGE COMPLIANCE
        if ( $company->vehicles->enforce_vehicle_age ) {
            $max_vehicle_age = $user->driverProfile->vehicleProfile->max_vehicle_age;

            // deal with the case where the driver had no personal vehicle, they cannot be put into green reimbursement...
            if ($user->driverProfile->personalVehicle) {
                $current_vehicle_age = $current_year - $user->driverProfile->personalVehicle->year;
            } else {
                $current_vehicle_age = 1000;
            }

            $new_street_light_id = $this->calculateStreetLightIdVehicleAgeCompliance($new_street_light_id, $service_plan, $max_vehicle_age, $current_vehicle_age);
        }

        /* TODO MOST RECENT WORK
        // INSURANCE COMPLIANCE
        if ($company->options->use_company_insurance_module) {
            $remove_reimbursement_without_insurance = $company->insurance->remove_reimbursement_without_insurance;
            $insurance_awaiting_processing = false; // todo
            $insurance_processed = false; // todo

            // todo insurance awaiting processing
            $new_street_light_id = $this->calculateStreetLightIdInsuranceCompliance(
                $new_street_light_id,
                $service_plan,
                $remove_reimbursement_without_insurance,
                $insurance_awaiting_processing,
                $insurance_processed,
                $coverage_sufficient,
                $insurance_expired
            );
        }
*/

/* TODO 2018-07-20
        // INSURANCE COMPLIANCE
        // TODO TALK TO RORY TO MAKE THIS PORTION WORK WITH NEW INSURANCE PROCESSING
        $insurance_processed = !empty($user->driverProfile->insurance_id);

        if ( $insurance_processed ) {
            $coverage_sufficient = $user->driverProfile->insurance->approved;
        } else {
            $coverage_sufficient = false;
        }

        if ( $insurance_processed ) {
            $expiry_date = new Carbon($user->driverProfile->insurance->expiration_date);
            // insurance marked as expired if today is greater than or equal to the expiry date
            $insurance_expired = $today->gte($expiry_date);
        } else {
            $insurance_expired = false;
        }
*/
        /**
         * NOTE! company_insurance_module is the general insurance service provided by CarData
         *       the FAVR_insurance_module of the OLD system, is reflected in remove_reimbursement_without_insurance
         */
        //TODO commented out for now, this is likely not going to be a part of standard street light recalculation
//        if ($company->options->use_company_insurance_module) {
//            $remove_reimbursement_without_insurance = $company->insurance->remove_reimbursement_without_insurance;
//            $new_street_light_id = $this->calculateStreetLightIdInsuranceCompliance($new_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_processed, $coverage_sufficient, $insurance_expired);
//        }


        // FINANCIALS / BANKING COMPLIANCE
        if ($company->options->use_company_financials_module && $company->payments->payment_responsibility == 'CarData') {
            $driver_has_provided_banking_information = !( empty($user->banking()) );
            $new_street_light_id = $this->calculateStreetLightIdBankingCompliance($new_street_light_id, $driver_has_provided_banking_information);
        }

        // LICENSE COMPLIANCE
        //TODO commented out for now, this is likely not going to be a part of standard street light recalculation
//        if ($company->options->use_company_license_module) {
//            $license_processed = !empty($user->driverProfile->license_id);
//
//            if ($license_processed) {
//                $renewal_date = new Carbon($user->driverProfile->license->renewal_date);
//                // insurance marked as expired if today is greater than or equal to the expiry date
//                $new_license_required = $today->gte($renewal_date);
//            } else {
//                $new_license_required = null;
//            }
//
//            $new_street_light_id = $this->calculateStreetLightIdLicenseCompliance($new_street_light_id, $license_processed, $new_license_required);
//        }


        /**
         * FAVR SPECIFIC STATUS CHECKS
         *      - VEHICLE COST COMPLIANCE
         *      - ANNUAL DECLARATION COMPLIANCE
         *      - 5000 MILE LIMIT COMPLIANCE
         *      - DEPRECIATION COMPLIANCE
         */
        if ( $service_plan == 'FAVR' ) {

            // we only hit this case if the driver skipped the vehicle portion of registration
            if ( empty($user->driverProfile->personalVehicle) ) {
                // TODO: figure out a potential alternative, although true, this is not the most accurate description of the user's issue
                $new_street_light_id = $this->calculateStreetLightIdVehicleCostCompliance($new_street_light_id, false);
                return $new_street_light_id;
            }

            // VEHICLE COST COMPLIANCE
            $current_vehicle_cost = $user->driverProfile->personalVehicle->vehicle_cost;
            $minimum_vehicle_cost = $user->driverProfile->vehicleFavrParameters()->minimum_vehicle_cost;
            $vehicle_meets_minimum_cost_requirement = $current_vehicle_cost >= $minimum_vehicle_cost;
            $new_street_light_id = $this->calculateStreetLightIdVehicleCostCompliance($new_street_light_id, $vehicle_meets_minimum_cost_requirement);

            // ANNUAL DECLARATION COMPLIANCE
            $driver_has_annual_irs_odometer_declaration = $user->driverProfile->annualIrsOdometerDeclaration()->count() > 0;
            $new_street_light_id = $this->calculateStreetLightIdAnnualDeclarationCompliance($new_street_light_id, $driver_has_annual_irs_odometer_declaration);

            // DEPRECIATION COMPLIANCE
            $depreciation_is_compliant = $user->driverProfile->personalVehicle->depreciation_method == 1;
            $new_street_light_id = $this->calculateStreetLightIdDepreciationCompliance($new_street_light_id, $depreciation_is_compliant);

        }

        return $new_street_light_id;
    }


    /**
     * This function calculates the portion of street_light_id relating to vehicle age compliance.
     *
     * NOTE! This function should only be called when a given company has 'enforce_vehicle_age' on their company
     * options record
     *
     * @param $old_street_light_id
     * @param $service_plan
     * @param $max_vehicle_age
     * @param $current_vehicle_age
     * @return int
     */
    public function calculateStreetLightIdVehicleAgeCompliance($old_street_light_id, $service_plan, $max_vehicle_age, $current_vehicle_age)
    {
        $current_year = Carbon::today()->year;

        $new_street_light_id = $old_street_light_id;

        // if we have vehicle age compliance
        if ( $current_year - $current_vehicle_age >= $current_year - $max_vehicle_age ) {

            $new_street_light_id = $this->removeFromStreetLight($old_street_light_id, GREEN_NO_VEHICLE_AGE_COMPLIANCE);

            // check if the driver had RED_VEHICLE_TOO_OLD, if so remove it
            $new_street_light_id = $this->removeFromStreetLight($old_street_light_id, RED_VEHICLE_TOO_OLD);

            // else we do not have vehicle age compliance
        } else {

            if ( $service_plan == 'FAVR' ) {

                // if driver did not have GREEN_NO_VEHICLE_AGE_COMPLIANCE, add it in
                $new_street_light_id = $this->addToStreetLight($old_street_light_id, GREEN_NO_VEHICLE_AGE_COMPLIANCE);

            } else {

                // check if the driver had RED_VEHICLE_TOO_OLD, if not, add it it
                $new_street_light_id = $this->addToStreetLight($old_street_light_id, RED_VEHICLE_TOO_OLD);
            }
        }

        return $new_street_light_id;
    }


    /**
     * This is for FAVR only.
     *
     * This function calculates the portion of street light id relating to vehicle cost compliance.
     *
     * @param $old_street_light_id
     * @param $vehicle_meets_minimum_cost_requirement
     * @return int
     */
    public function calculateStreetLightIdVehicleCostCompliance($old_street_light_id, $vehicle_meets_minimum_cost_requirement)
    {
        $new_street_light_id = $old_street_light_id;

        if ( $vehicle_meets_minimum_cost_requirement ) {

            $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, GREEN_NO_VEHICLE_COST_COMPLIANCE);

        } else {

            $new_street_light_id = $this->addToStreetLight($new_street_light_id, GREEN_NO_VEHICLE_COST_COMPLIANCE);
        }

        return $new_street_light_id;
    }

    /**
     * This is for FAVR only.
     *
     * This function calculates the portion of street_light pertaining to a FAVR drivers'
     * annual delcaration.
     *
     * @param $old_street_light_id
     * @param $driver_has_declaration
     * @return int $new_street_light_id
     */
    public function calculateStreetLightIdAnnualDeclarationCompliance($old_street_light_id, $driver_has_declaration)
    {
        $new_street_light_id = $old_street_light_id;

        if ( $driver_has_declaration ) {

            $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, GREEN_NO_ANNUAL_DECLARATION);

        } else {

            $new_street_light_id = $this->addToStreetLight($new_street_light_id, GREEN_NO_ANNUAL_DECLARATION);

        }

        return $new_street_light_id;
    }

    /**
     * Given a street_light_id, and whether or not a FAVR personal vehicle declaration's depreciation is valid,
     * this function will return the appropriate street_light_id
     *
     * @param $old_street_light_id
     * @param $depreciation_is_compliant
     * @return int $new_street_light_id
     */
    public function calculateStreetLightIdDepreciationCompliance($old_street_light_id, $depreciation_is_compliant)
    {
        $new_street_light_id = $old_street_light_id;

        if ( $depreciation_is_compliant ) {

            $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, GREEN_NO_DEPRECIATION_COMPLIANCE);

        } else {

            $new_street_light_id = $this->addToStreetLight($new_street_light_id, GREEN_NO_DEPRECIATION_COMPLIANCE);

        }

        return $new_street_light_id;
    }


    /**
     * Given a street_light_id, service_plan, remove_reimbursement_without_insurance (FAVR Insurance Module) and
     * a user_id, this function will compute the entire street_light_id for a given user relating to insurance.
     *
     * It will calculate:
     *  - whether insurance has been submitted
     *  - if insurance has been provided, whether insurance is sufficient
     *  - if insurance has been provided, whether or not it is expired
     */
    public function calculateStreetLightIdInsuranceCompliance($old_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_processed, $coverage_sufficient, $insurance_expired)
    {
        $today = Carbon::today()->format('Y-m-d');

        $new_street_light_id = $old_street_light_id;

        /**
         * A $insurance_processed is true once a driver's provided insurance has been processed. At this point it is linked to their driverProfile
         * under insurance_id.
         */
        $new_street_light_id = $this->calculateStreetLightIdInsuranceAwaiting($new_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_processed);

        /**
         * if awaiting insurance, or no insurance in grace, we return.
         * There would be no sense in checking if nonexistent coverage is sufficient.
         * Otherwise we remove the awaiting processing statuses
         */
        if ( !$insurance_processed ) {
            return $new_street_light_id;
        } else {
            $new_street_light_id = $this->calculateStreetLightIdInsuranceProcessed($new_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_processed);
        }

        $new_street_light_id = $this->calculateStreetLightIdInsuranceSufficient($new_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_processed, $coverage_sufficient);

        $new_street_light_id = $this->calculateStreetLightIdInsuranceExpired($new_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_expired, $coverage_sufficient);


        return $new_street_light_id;
    }


    /**
     * Given an existing street light and a bunch of parameters, the following function will calculate a new street
     * light id, and add or remove awaiting insurance based statuses
     *
     * @param $old_street_light_id
     * @param $service_plan
     * @param $remove_reimbursement_without_insurance
     * @param null $insurance_processed
     * @return int
     */
    public function calculateStreetLightIdInsuranceAwaiting($old_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_processed = null)
    {
        $new_street_light_id = $old_street_light_id;

        // traditional FAVR plan, reimbursement is NOT removed without insurance, this is currently not used
        if ($service_plan == 'FAVR' && !$remove_reimbursement_without_insurance) {

            if ($insurance_processed) {
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_AWAITING_INSURANCE);
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, GREEN_NO_INSURANCE_IN_GRACE_PERIOD);

                // driver has not provided insurance
            } else {
                $in_grace = false;
                // TODO: calculate grace

                if ($in_grace) {
                    $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_AWAITING_INSURANCE);
                    $new_street_light_id = $this->addToStreetLight($new_street_light_id,GREEN_NO_INSURANCE_IN_GRACE_PERIOD);
                } else {
                    $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, GREEN_NO_INSURANCE_IN_GRACE_PERIOD);
                    $new_street_light_id = $this->addToStreetLight($new_street_light_id,GREEN_NO_INSURANCE_COMPLIANCE);
                }
            }

        // 463 AND NEW FAVR PLAN WITH "INSURANCE MODULE" ie. $remove_reimbursement_without_insurance == true
        } else {
            if ($insurance_processed) {
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_AWAITING_INSURANCE);
            } else {
                $new_street_light_id = $this->addRedStatus($new_street_light_id,RED_AWAITING_INSURANCE);
            }
        }

        return $new_street_light_id;
    }


    /**
     * Given an existing street light and the parameters below, the following function will calculate the portion
     * of street light related to insurance documents awaiting processing.
     *
     * @param $old_street_light_id
     * @param $service_plan
     * @param $remove_reimbursement_without_insurance
     * @param $insurance_processed
     * @return int
     */
    public function calculateStreetLightIdInsuranceProcessed($old_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_processed)
    {
        $new_street_light_id = $old_street_light_id;

        // traditional FAVR plan, reimbursement is NOT removed without insurance, this is currently not used
        if ($service_plan == 'FAVR' && !$remove_reimbursement_without_insurance) {

            if ($insurance_processed) {
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_INSURANCE_AWAITING_PROCESSING);
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, GREEN_INSURANCE_AWAITING_PROCESSING);
            } else {
                $new_street_light_id = $this->addToStreetLight($new_street_light_id,GREEN_INSURANCE_AWAITING_PROCESSING);
            }

            // 463 AND NEW FAVR PLAN WITH "INSURANCE MODULE" ie. $remove_reimbursement_without_insurance == true
        } else {
            if ($insurance_processed) {
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_INSURANCE_AWAITING_PROCESSING);
            } else {
                $new_street_light_id = $this->addRedStatus($new_street_light_id,RED_INSURANCE_AWAITING_PROCESSING);
            }
        }

        return $new_street_light_id;
    }


    /**
     * This function will calculate the portion of street_light_id relating to meeting minimum insurance requirements
     * (ie. is coverage sufficient)
     *
     * This function should be called after calculateStreetLightIdInsuranceAwaiting
     * if the $old_street_light_id involves some form of 'awaiting insurance' of course this will return the same value
     * insurance cannot be sufficient nor insufficient if it does not exist.
     *
     * @param $old_street_light_id
     * @param $service_plan
     * @param $remove_reimbursement_without_insurance
     * @param $driver_has_provided_insurance
     * @param $coverage_sufficient
     * @return int
     */
    public function calculateStreetLightIdInsuranceSufficient($old_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_processed, $coverage_sufficient)
    {
        $new_street_light_id = $old_street_light_id;

        // if no insurance has been processed, simply return the old street light back
        if ( !$insurance_processed ) {
            return $old_street_light_id;
        }

        // traditional FAVR plan, reimbursement is NOT removed without insurance, this is currently not used
        if ($service_plan == 'FAVR' && !$remove_reimbursement_without_insurance) {

            if ($coverage_sufficient) {
                // just in case
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_INSUFFICIENT_INSURANCE);
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, GREEN_NO_INSURANCE_COMPLIANCE);

            } else {
                $new_street_light_id = $this->addToStreetLight($new_street_light_id, GREEN_NO_INSURANCE_COMPLIANCE);
            }

            // 463 and FAVR + remove reimb without insurance (aka insurance module)
        } else {

            if ($coverage_sufficient) {
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_INSUFFICIENT_INSURANCE);
            } else {
                $new_street_light_id = $this->addRedStatus($new_street_light_id, RED_INSUFFICIENT_INSURANCE);
            }
        }

        return $new_street_light_id;
    }


    /**
     * This function should be called after calculateStreetLightIdInsuranceAwaiting.
     * It will calculate the portion of street_light_id relating to insurance expiry.
     * If the $old_street_light_id involves RED_AWAITING_INSURANCE, this function will return $old_street_light_id
     * as insurance cannot be sufficient nor insufficient if it does not exist.
     *
     * @param $old_street_light_id
     * @param $service_plan
     * @param $remove_reimbursement_without_insurance
     * @param $insurance_expired
     * @param $coverage_sufficient is required in the case of $remove_reimbursement_without_insurance = 0
     * @return int
     */
    public function calculateStreetLightIdInsuranceExpired($old_street_light_id, $service_plan, $remove_reimbursement_without_insurance, $insurance_expired, $coverage_sufficient = null)
    {

        $new_street_light_id = $old_street_light_id;

        // if no insurance has been provided, simply return the old street light back
        if ($old_street_light_id & RED_AWAITING_INSURANCE || $old_street_light_id & GREEN_NO_INSURANCE_IN_GRACE_PERIOD) {
            return $new_street_light_id;
        }
        // traditional FAVR plan, reimbursement is NOT removed without insurance, this is currently not used
        if ($service_plan == 'FAVR' && !$remove_reimbursement_without_insurance) {

            if (!$insurance_expired) {
                // just in case
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_INSURANCE_EXPIRED);

                if ($coverage_sufficient) {
                    $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, GREEN_NO_INSURANCE_COMPLIANCE);
                }
            } else {
                $new_street_light_id = $this->addToStreetLight($new_street_light_id, GREEN_NO_INSURANCE_COMPLIANCE);
            }

            // 463 and FAVR + remove reimb without insurance (aka insurance module)
        } else {

            if (!$insurance_expired) {
                $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_INSURANCE_EXPIRED);
            } else {
                $new_street_light_id = $this->addRedStatus($new_street_light_id, RED_INSURANCE_EXPIRED);
            }

        }

        return $new_street_light_id;

    }


    /**
     * This function will calculate the portion of street_light_id relating to license.
     * license_expired is optional if the driver has not provided their license information.
     *
     * In the case where a driver_has_provided_license = true, if license_expired is not provided it will be assumed false,
     * that is to see RED_DRIVER_LICENSE_REQUIRED will be removed if it exists
     *
     * @param $old_street_light_id
     * @param $license_processed
     * @param null $new_license_required
     * @return int
     */
    public function calculateStreetLightIdLicenseCompliance($old_street_light_id, $license_processed, $new_license_required = null)
    {
        $new_street_light_id = $old_street_light_id;

        if ($license_processed) {
            $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_AWAITING_LICENCE);
        } else {
            $new_street_light_id = $this->addRedStatus($new_street_light_id, RED_AWAITING_LICENCE);
            // no license exists, no need to check expiry
            return $new_street_light_id;
        }

        if ($new_license_required) {
            $new_street_light_id = $this->addRedStatus($new_street_light_id, RED_DRIVER_LICENCE_REQUIRED);
        } else {
            $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, RED_DRIVER_LICENCE_REQUIRED);
        }

        return $new_street_light_id;
    }

    /**
     * This function will calculate the portion of street_light_id relating to banking compliance
     * specifically, whether or not the user will have status GREEN_NO_BANKING_DATA
     *
     * @param $old_street_light_id
     * @param $driver_has_provided_banking_information
     * @return int
     */
    public function calculateStreetLightIdBankingCompliance($old_street_light_id, $driver_has_provided_banking_information)
    {
        $new_street_light_id = $old_street_light_id;

        if ($driver_has_provided_banking_information) {
            $new_street_light_id = $this->removeFromStreetLight($new_street_light_id, GREEN_NO_BANKING_DATA);
        } else {
            $new_street_light_id = $this->addToStreetLight($new_street_light_id, GREEN_NO_BANKING_DATA);
            return $new_street_light_id;
        }

        return $new_street_light_id;

    }


    /**
     * This function calculates the appropriate deactivate status given an array of street_light_ids (powers of 2), or
     * a single street light value ( a sum of powers of 2 )
     *
     * In either case, the function will strip the given street light down to some combination of the following statuses:
     *      - RED_ON_LEAVE
     *      - RED_DISABILITY
     *      - RED_TERMINATED
     *      - RED_NO_REIMBURSEMENT
     *
     * If none of the above statuses are present, the function will throw an exception
     *
     * @param $statusArray
     * @return int
     */
    public function calculateDeactivationStatus($status)
    {
        $deactivationStreetLightIds = [RED_ON_LEAVE, RED_DISABILITY, RED_TERMINATED, RED_NO_REIMBURSEMENT];
        $new_street_light_id = 0;

        if ( is_array($status) ) {

            $new_street_light_id = array_reduce($status, function($carry, $val) use ($deactivationStreetLightIds) { return in_array($val, $deactivationStreetLightIds) ? $carry + $val : $carry ; } );

        } else if ( is_int($status) ) {

            $new_street_light_id = array_reduce($deactivationStreetLightIds, function($carry, $val) use ($status) { return $status & $val ? $carry + $val : $carry; } );

        } else {

            throw new \Exception('Invalid type. Argument must be of type array or type integer');

        }

        if ($new_street_light_id == 0) {
            throw new \Exception('Error calculating deactivation street_light_id');
        }

        return (int)$new_street_light_id;
    }


    public function newDriver()
    {
        return YELLOW_NEW_DRIVER;
    }

    /**
     * This function will determine whether a street light transition from Red to Green occurred.
     *
     *      TRUE:  Old street light red, new street light green
     *      FALSE: else
     *
     * @param $new_street_light_id
     * @param $old_street_light_id
     * @return bool
     */
    public function fromRedToGreen($new_street_light_id, $old_street_light_id)
    {
        return $this->colorOfStreetLight($old_street_light_id) == 'Red' && $this->colorOfStreetLight($new_street_light_id) == 'Green';
    }

    /**
     * This function will determine whether a street light transition from Green to Red occurred.
     *
     *      TRUE:  Old street light green, new street light red
     *      FALSE: else
     *
     * @param $new_street_light_id
     * @param $old_street_light_id
     * @return bool
     */
    public function fromGreenToRed($new_street_light_id, $old_street_light_id)
    {
        return $this->colorOfStreetLight($old_street_light_id) == 'Green' && $this->colorOfStreetLight($new_street_light_id) == 'Red';
    }

    /**
     * This function will determine whether a street light transition from Red to Red occurred.
     *
     *      TRUE:  Old street light red, new street light red
     *      FALSE: else
     *
     * @param $new_street_light_id
     * @param $old_street_light_id
     * @return bool
     */
    public function fromRedToRed($new_street_light_id, $old_street_light_id)
    {
        return $this->colorOfStreetLight($old_street_light_id) == 'Red' && $this->colorOfStreetLight($new_street_light_id) == 'Red';
    }

}