<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Division extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id', 'market', 'name'];

    public function savedStops() {
        return $this->belongsToMany('App\SavedStop', 'division_stop');
    }
}
