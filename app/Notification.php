<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'template_slug', 'user_id', 'area', 'severity', 'title', 'content', 'icon', 'link', 'dismissible', 'note',
        'visible_to'
    ];
}
