<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverInsuranceDocument extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'insurance_review_id',
        'filename',
        'bodily_liability_per_person',
        'bodily_liability_per_accident',
        'property_damage',
        'deductible',
        'comprehensive_deductible',
        'collision_deductible',
        'medical',
        'uninsured_per_person',
        'uninsured_per_accident',
        'public_liability',
        'business_use',

        'company_requirements',
        'no_policy_term_dates',
        'vehicle_match',
        'document_illegible',
        'document_current',
        'name_missing',
        'other',

        'approved',
        'rejection_id',
        'processor_id',
        'comment',
        'processing_date',
        'created_at' // Do not delete me, needed for manual insertion of created_at
    ];

    public function insurance_reviews()
    {
        return $this->belongsTo('App\DriverInsuranceReview');
    }
}
