<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model
{
    protected $fillable = ['short_name', 'name', 'country_id', 'utc_offset'];
}
