<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyBank extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id', 'bank_id'];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function bank() {
        return $this->belongsTo('App\Bank');
    }
}
