<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleMileageAdjustment2009 extends Model
{
    protected $table = 'vehicle_mileage_adjustment_2009';

    protected $fillable = ['mileage_range_id', 'value_range_id', 'model_year', 'resale_year',
        'mileage_range_min', 'mileage_range_max', 'value_range_min', 'value_range_max', 'adjustment_type', 'adjustment'];
}
