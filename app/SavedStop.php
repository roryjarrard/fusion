<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Psy\Exception\Exception;

class SavedStop extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'company_id', 'active', 'stop_number', 'latitude', 'longitude', 'street', 'street2',
        'city', 'zip_postal', 'state_province_id', 'country_id', 'company_stop_id', 'driver_stop_id'];

    public function drivers()
    {
        return $this->belongsToMany('App\User', 'driver_stop');
    }

    public function divisions()
    {
        return $this->belongsToMany('App\Division', 'division_stop');
    }

    /**
     * returns an array of all associated contract numbers
     * @return mixed
     */
    public function contracts() {
        return \DB::table('saved_stop_company_contract')
            ->where('saved_stop_id', $this->id)
            ->get()
            ->pluck('contract_id')
            ->all();
    }

    public function createFromGoogle($data)
    {
        //\Log::info('creating new stop',[$data]);
        $error_fields = [];
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        if (empty($data['user_id'])) {
            $data['user_id'] = $user->id;
        }

        $ret = ['success' => false, 'message' => '', 'id' => null];
        if (empty($data['name'])) {
            $error_fields[] = 'name';
        }
        if (empty($data['street_number'])) {
            $error_fields[] = 'street_number';
        }
        if (!empty($data['is_super']) && empty($data['divisions'])) { // davery
            $error_fields[] = 'divisions';
        }

        if($data['country'] != 'United States' && $data['country'] != 'Canada') {
            $error_fields[] = 'country';
        }
        if (count($error_fields)) { // davery
            $ret['message'] = 'fail';
        } else {
            if (!empty($data['is_super'])) { // davery
                $this->company_id = $data['company_id'];
                $this->stop_number = empty($data['stop_number']) ? '' : $data['stop_number'];
                $this->active = $data['active'];
            } // davery
            $this->name = $data['name'];
            $this->latitude = $data['latitude'];
            $this->longitude = $data['longitude'];
            $this->street = $data['street_number'] . ' ' . $data['route'];
            $this->city = $data['locality'];
            $this->state_province_id = StateProvince::where('short_name', $data['administrative_area_level_1'])->first()->id;
            $this->country_id = Country::where('name', $data['country'])->first()->id;
            $this->zip_postal = $data['postal_code'];

            $success = $this->save();

            $ret['success'] = $success;

            if ($success && empty($data['is_super'])) { // prevent if is_super - davery
                $ret['message'] = 'SavedStop created';
                $date = date('Y-m-d'); // davery
                $driverStop = \DB::select(\DB::raw("insert into driver_stop (user_id, saved_stop_id, effective_date) values ({$data['user_id']}, {$this->id}, '{$date}')"));
            } else if ($success && !empty($data['is_super'])) { // if is_super - davery
                foreach($data['divisions'] as $division) { // loop through divisions & save to pivot table
                    \DB::insert(\DB::raw("insert into division_stop (division_id, saved_stop_id) values ({$division['id']}, {$this->id})"));
                }
            } else {
                $ret['message'] = 'Failed to create SavedStop';
            }
            $ret['id'] = $this->id;
        }

        return [$ret, $error_fields]; // davery
    }

    /**
     * Creates a stop for a company which uses contract stops
     * @param $data
     */
    public function createFromGoogleUsingContracts($data)
    {
        $error_fields = [];
        $user = session()->has('impersonate') ? User::find(session()->get('impersonate')) : Auth::user();
        if (empty($data['user_id'])) {
            $data['user_id'] = $user->id;
        }

        $ret = ['success' => false, 'message' => '', 'id' => null];
        if (empty($data['name'])) {
            $error_fields[] = 'name';
        }
        if (empty($data['street_number'])) {
            $error_fields[] = 'street_number';
        }

        if (isset($data['stop_number'])) {
            // validate the contract numbers
            $contract_numbers = array_map(function($x) { return trim((string)($x)); }, explode(',', $data['stop_number']));
            if (sizeof($contract_numbers) > 5) {
                $error_fields[] = 'contract_numbers';
            } else {
                foreach($contract_numbers as $contract_number) {
                    if (!preg_match('/^\d{4}$/', $contract_number)) {
                        $error_fields[] = 'contract_numbers';
                    }
                }
            }
        } else {
            $contract_numbers = [];
        }


        if($data['country'] != 'United States' && $data['country'] != 'Canada') {
            $error_fields[] = 'country';
        }

        if (empty($error_fields)) {
            // check for duplicate
            $existingStop = SavedStop::where('company_id', $data['company_id'])
                ->where('street', $data['street_number'] . ' ' . $data['route'])
                ->where('city', $data['locality'])
                ->where('state_province_id', StateProvince::where('short_name', $data['administrative_area_level_1'])->first()->id)
                ->where('zip_postal', $data['postal_code'])
                ->first();
            if (!empty($existingStop)) {
                $error_fields[] = 'duplicate';
            }
        }

        if (count($error_fields)) {
            // errors
            $ret['message'] = 'fail';
        } else {
            // no errors
            if (!empty($data['is_super'])) {
                $this->company_id = $data['company_id'];
                $this->active = $data['active'];
            }
            $this->name = $data['name'];
            $this->latitude = $data['latitude'];
            $this->longitude = $data['longitude'];
            $this->street = $data['street_number'] . ' ' . $data['route'];
            $this->city = $data['locality'];
            $this->state_province_id = StateProvince::where('short_name', $data['administrative_area_level_1'])->first()->id;
            $this->country_id = Country::where('name', $data['country'])->first()->id;
            $this->zip_postal = $data['postal_code'];

            $success = $this->save();

            $ret['success'] = $success;

            $cc = new \App\Http\Controllers\CompanyController();
            $company = Company::find($data['company_id']);
            $today = date('Y-m-d');


            if ($success && empty($data['is_super'])) { // prevent if is_super
                $ret['message'] = 'SavedStop created';
                $cc->addDriverToStop($this->id, $data['user_id'], $today);

                $cc->addContractToStop($company->id, $this->id, 9400);
                foreach($contract_numbers as $contract_number) {
                    $cc->addContractToStop($company->id, $this->id, $contract_number);
                }

            } else if ($success && !empty($data['is_super'])) { // if is_super

                //TODO for now we assign to ALL divisions regardless of selection
                $division_ids = $company->divisions->pluck('id')->all();
                $cc->addDivisionToStop($this->id, $division_ids);

                // assign stop to all drivers effective today
                $driver_user_ids = $company->driverProfiles->pluck('user_id')->all();
                foreach($driver_user_ids as $user_id) {
                    $cc->addDriverToStop($this->id, $user_id, $today);
                }

                $cc->addContractToStop($company->id, $this->id, 9400);
                foreach($contract_numbers as $contract_number) {
                    $cc->addContractToStop($company->id, $this->id, $contract_number);
                }


            } else {
                $ret['message'] = 'Failed to create SavedStop';
            }
            $ret['id'] = $this->id;
        }

        return [$ret, $error_fields]; // davery


    }

}