<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModelYear extends Model
{
    protected $fillable = ['year', 'model_id'];
}
