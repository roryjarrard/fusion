<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DivisionStop extends Model
{
    protected $table = 'division_stop';

    protected $fillable = ['division_id', 'saved_stop_id'];
}
