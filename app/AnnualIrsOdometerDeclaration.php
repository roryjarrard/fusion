<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnualIrsOdometerDeclaration extends Model
{
    protected $fillable = [
        'user_id',
        'personal_vehicle_id',
        'odometer',
        'minimum_vehicle_year',
        'created_at',   // this is the date declared
    ];

    public function personalVehicle()
    {
        return $this->belongsTo('App\PersonalVehicle', 'personal_vehicle_id', 'id');
    }

}
