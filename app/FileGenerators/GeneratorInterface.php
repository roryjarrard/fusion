<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 2018-09-19
 * Time: 9:53 AM
 */

namespace App\FileGenerators;


/**
 * Interface GeneratorInterface
 *
 * All file generators must be at least able to create and store their files
 *
 * @package App\FileGenerators
 */
interface GeneratorInterface
{
    public function create();

    public function store();
}