<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 2018-09-19
 * Time: 9:55 AM
 */

namespace App\FileGenerators;
use App\CompanyPaymentDate;
use App\Company;

class DirectDepositPaymentScheduleExcelGenerator extends ExcelGeneratorAbstract
{
    private $company_id = null;
    private $year = null;

    /**
     * Data must be an object containing a company id and a year
     * DirectDepositPaymentScheduleExcelGenerator constructor.
     * @param $data
     */
    public function __construct($company_id, $year)
    {
        $this->filename = 'direct_deposit_payment_schedule.xls';
        $this->company_id = $company_id;
        $this->year = $year;
        $this->getTemplate('direct_deposit_payment_schedule_template.xls');
    }


    /**
     * Generates the actual file
     */
    public function create()
    {
        try {
            /**
             * get all the data
             */
            $today = date('F d, Y');
            $company_name = Company::find($this->company_id)->name;
            $paymentDates = CompanyPaymentDate::where('year', $this->year)
                ->where('company_id', $this->company_id)
                ->get()
                ->all();

            /**
             * Fill the template
             */
            $worksheet = $this->spreadsheet->getActiveSheet();
            for ($i = 0; $i < sizeof($paymentDates); $i++) {
                $row_number = $i + 3;
                $worksheet->getCell('A' . $row_number)->setValue($paymentDates[$i]->year);
                $worksheet->getCell('B' . $row_number)->setValue(
                    date('F', mktime(0,0,0,$paymentDates[$i]->month, 1))
                );
                $worksheet->getCell('C' . $row_number)->setValue($paymentDates[$i]->banking_lock_date);
                $worksheet->getCell('D' . $row_number)->setValue($paymentDates[$i]->approved_date);
                $worksheet->getCell('E' . $row_number)->setValue($paymentDates[$i]->received_date);
                $worksheet->getCell('F' . $row_number)->setValue($paymentDates[$i]->driver_payment_date);
            }

            $worksheet->getCell('B16')->setValue('Direct Deposit Payment Schedule');
            $worksheet->getCell('B17')->setValue($today);
            $worksheet->getCell('B18')->setValue($company_name);
            $worksheet->getCell('B19')->setValue($this->year);


        } catch(\Exception $e) {
            return response($e->getMessage(), 500);
        }

    }

}