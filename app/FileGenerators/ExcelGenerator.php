<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 2018-10-04
 * Time: 8:00AM
 */

namespace App\FileGenerators;


/**
 * Use this class in order to generate a 'General' Excel document.
 *
 * To work, $data must be passed to the constructor.
 *
 * Here is an example of how data must be structured
 * data: {
        categoryHeadings: [
            { title: 'Driver', column_span: 2},
            { title: 'Manager', column_span: 4}
        ],
        columnHeadings: [
            { title: 'First Name', column_alignment: 'left', key_on_row: 'first_name'},
            { title: 'Last Name', column_alignment: 'right', key_on_row: 'last_name' },
            { title: 'right', column_alignment: 'right', key_on_row: 'c' },
            { title: 'left', column_alignment: 'left', key_on_row: 'd' },
            { title: 'center', column_alignment: 'center', key_on_row: 'e' },
            // OPTIONAL EXAMPLE FOR NUMBER FORMATTING
            { title: 'Some Num', column_alignment: 'right', key_on_row: 'some_num', precision: 2, comma_separated: true}
        ],
        rows: [
            {first_name: 'john', last_name: 'smith', c: 'align to right', d: 'align to left', e: 'align to center'},
            {first_name: 'jacob', last_name: 'smitt', c: '2align to right', d: '2align to left', e: '2align to center'},
            {first_name: 'jeremy', last_name: 'schmidt', c: '3align to right', d: '3align to left', e: '3align to center'}
        ],
        filename: 'testing.xls',
        reportDescription: {
            'Report Name': 'Testing Report',
            'Creation Date': null,
            'Company': 'Demo',
            'Year': 2018,
            'Month': 9,
        }
    },
 *
 * For more detail:
 * @link https://cardata.atlassian.net/wiki/spaces/REW/pages/515670017/Files+Generation+downloads+etc...
 *
 * Class ExcelGenerator
 * @package App\FileGenerators
 */
class ExcelGenerator extends ExcelGeneratorAbstract
{
    protected $data;

    /**
     * This is the constructor
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;

        // generate a pseudo random temporary filename so we avoid collisions
        $this->loadEmptySpreadsheet(substr(md5(rand()), 0, 7) . '.xls');

        /**
         * If column alignments weren't specified in their own array, but instead as part of columnHeadings
         * do the following
         */
        if (!isset($data['columnAlignments'])) {
            $this->data['columnAlignments'] = [];
            for($col = 'A', $i = 0, $len = sizeof($this->data['columnHeadings']); $i < $len; $col = chr(ord($col) + 1), $i++) {
                $this->data['columnAlignments'][] = [
                    'column' => $col,
                    'alignment' => $this->data['columnHeadings'][$i]['column_alignment']];
            }
        }

        /**
         * If column number formats weren't specified in their own array, but instead as part of columnHeadings
         * do the following
         */
        if (!isset($data['columnNumberFormats'])) {
            $this->data['columnNumberFormats'] = [];
            for($col = 'A', $i = 0, $len = sizeof($this->data['columnHeadings']); $i < $len; $col = chr(ord($col) + 1), $i++) {
                // if precision and comma separated were omitted assume its not a number field and skip the formatting
                if (!array_key_exists('precision', $this->data['columnHeadings'][$i]) && !array_key_exists('comma_separated', $this->data['columnHeadings'][$i])) continue;

                $this->data['columnNumberFormats'][] = [
                    'column' => $col,
                    'precision' => $this->data['columnHeadings'][$i]['precision'],
                    'comma_separated' => array_key_exists('comma_separated', $this->data['columnHeadings'][$i])
                        ? $this->data['columnHeadings'][$i]['comma_separated']
                        : true
                ];
            }
        }
    }

    /**
     * Generates the actual file
     */
    public function create()
    {

        /**
         * First the column alignments are set, this applies to the rows.
         * It is done first because the headers, and the report description have different alignments,
         * and if we update the data alignments last it will overwrite those.
         */
        $this->alignColumns();

        /**
         * Next apply the number formats (if number formatting is not present it will be skipped)
         */
        $this->setColumnNumberFormats();

        /**
         * Next insert the headings
         * This is comprised of two portions (categoryHeadings and columnHeadings)
         */
        $this->insertHeadings();

        /**
         * Dumps all of the data into the rows of the spreadsheet
         */
        $this->insertRows();

        /**
         * Inserts totals
         */
        $this->insertTotals();

        /**
         * Applies a description footer to the report
         */
        $this->insertReportDescription();

        /**
         * Auto sizes column widths to fit the data
         */
        $this->autosizeColumnWidths();


    }

}