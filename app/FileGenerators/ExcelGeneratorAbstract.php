<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 2018-09-19
 * Time: 10:23 AM
 *
 *
 * Note:
 *  The functions within this abstract class can be overloaded if they do not suit your needs.
 *  Overload them on the child class!
 *
 */

namespace App\FileGenerators;

use PhpOffice\PhpSpreadsheet;


abstract class ExcelGeneratorAbstract implements GeneratorInterface
{
    protected $data = null;
    protected $mapTable = null;
    protected $spreadsheet = null;
    protected $worksheet = null;
    public $filename = null;

    /**
     * Gets the spreadsheet template loaded
     *
     * @return PhpSpreadsheet\Spreadsheet
     */
    protected function getTemplate($template_filename)
    {
        try {
            $this->spreadsheet = PhpSpreadSheet\IOFactory::load(
                base_path() . '/resources/assets/xls/' . $template_filename
            );
        } catch (\Exception $e) {
            return response($e->getMessage(),  404);
        }

    }

    /**
     * Creates a new spreadsheet from scratch
     * Whenever a spreadsheet is created a workbook is as well
     *      https://phpspreadsheet.readthedocs.io/en/develop/topics/creating-spreadsheet/
     *
     * A filename is required only to load it into the class's data for the store method.
     * Perhaps this is the wrong place to load a filename, but hey, it is what it is until there is a reason it isnt
     *
     * @param $filename
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function loadEmptySpreadsheet($filename)
    {
        try {
            $this->filename = $filename;
            $this->spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            $this->worksheet = $this->spreadsheet->getActiveSheet();
        } catch (\Exception $e) {
            return response($e->getMessage(),  404);
        }

    }

    /**
     * The following function inserts both the category headings and the column headings
     */
    protected function insertHeadings()
    {
        $this->insertCategoryHeadings();

        $this->insertColumnHeadings();
    }

    /**
     * The following function inserts categorical headings (ex. Driver, or Reimbursement)
     *
     * It requires that the generator object have a field within data called categoryHeadings structured
     * as it is below:
     *
     * data {
     *   ...,
     *    categoryHeadings: [
     *      {title, col_span},
     *      ...
     *    ],
     *   ...,
     * }
     *
     */
    protected function insertCategoryHeadings()
    {
        if (!isset($this->data['categoryHeadings'])) return;

        $current_column = 'A';
        $end_merge_column = null;
        foreach($this->data['categoryHeadings'] as $heading) {

            // merge the number of cells required
            $end_merge_column = chr(ord($current_column) + $heading['column_span'] - 1);
            $this->worksheet->mergeCells($current_column . '1:' . $end_merge_column . '1');

            // insert the value
            $this->worksheet->getCell($current_column . '1')->setValue($heading['title']);

            // move to next column
            $current_column = chr(ord($end_merge_column) + 1);
        }

        /**
         * Apply styles
         *  - background colour
         *  - font weight bold
         *  - center aligned
         */
        $styleArray = [
            'fill' => [
                'fillType' => PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => [
                    'argb' =>  CARDATA_RED_ARGB
                ],
            ],
//            'borders' => [
//                'vertical' => [
//                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
//                    'color' => [
//                        'argb' => 'FFFFFFFF' // WHITE
//                    ]
//                ]
//            ],
            'font' => [
                'bold' => true,
                'color' => [
                    'argb' => 'FFFFFFFF' // WHITE
                ],
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ];

        // note end merge column will be the final column of our category headers here
        $this->worksheet->getStyle('A1:' . $end_merge_column . '1')->applyFromArray($styleArray);


    }

    /**
     * The following function inserts column headings (ex. First Name, Last Name, etc...)
     *
     * It requires that the generator object have a field within data called columnHeadings structured
     * as it is below. Note to insert column headings the only data required is a title
     *
     * data {
     *   ...,
     *    categoryHeadings: [
     *      {title: 'First Name', ...}},
     *      ...
     *    ],
     *   ...
     * }
     *
     */
    protected function insertColumnHeadings()
    {

        $current_column = 'A';
        $number_of_columns = sizeof($this->data['columnHeadings']);
        $ending_column = chr(ord('A') + $number_of_columns - 1);

        foreach($this->data['columnHeadings'] as $heading) {
            // insert the value
            $this->worksheet->getCell($current_column . '2')->setValue($heading['title']);
            $current_column = chr(ord($current_column) + 1);
        }

        /**
         * Apply styles
         *  - background colour
         *  - font weight bold
         *  - center aligned
         */
        $styleArray = [
            'fill' => [
                'fillType' => PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => [
                    'argb' => CARDATA_BLUE_ARGB
                ],
            ],
            'font' => [
                'bold' => true,
                'color' => [
                    'argb' => 'FFFFFFFF' // WHITE
                ],
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ];

        $this->worksheet->getStyle('A2:' . $ending_column . '2')->applyFromArray($styleArray);

    }


    /**
     * The following function inserts all rows of data into the spreadsheet
     * First it gets the table mapping data keys to columns.
     * Looping through all the rows, data is inserted starting from the previous row with data + 1
     *
     * It requires that the generator object have a field within data called rows structured
     * as it is below.
     *
     * data {
     *   ...,
     *    rows: [
     *      {key1: value1, key2: value2, ...},
     *      {key1: value1, key2: value2, ...},
     *      ...
     *    ],
     *   ...
     * }
     *
     * NOTE the keys should match the key_on_row value in columnHeadings that this datapoint corresponds to.
     * columnHeadings: [
            { title: 'First Name', column_alignment: 'left', key_on_row: 'first_name'},
            ...
        ],
        rows: [
            {first_name: 'John', ...},
            {first_name: 'Jane', ...}
        ],
     *
     * Having the key_on_row for each column match the key on the row means the order of the values
     * within the row do not matter (this was a cause of much confusion in the legacy system)
     *
     *
     */
    protected function insertRows()
    {
        if (!isset($this->mapTable)) {
            $this->getKeyToColumnTable(true);
        }

        for($i = 0, $row_count = sizeof($this->data['rows']), $starting_row = $this->worksheet->getHighestDataRow('A') + 1; $i < $row_count; $i++) {
            foreach($this->data['rows'][$i] as $key => $value) {
                if(!array_key_exists($key, $this->mapTable)) continue;
                $this->worksheet->getCell($this->mapTable[$key] . ($starting_row + $i))->setValue($value);
            }
        }
    }


    /**
     * The following function inserts totals beneath the rows if applicable
     */
    protected function insertTotals()
    {
        if (!isset($this->data['totals'])) return;

        // get the row beneath all inserted rows
        $row = $this->worksheet->getHighestDataRow('A') + 1;

        if (!isset($this->mapTable)) {
            $this->getKeyToColumnTable();
        }

        // insert the totals trailer
        $this->worksheet->getCell('A' . $row)->setValue('Totals');

        foreach($this->data['totals'] as $totalObj) {
            // if the total is not present as a column skip it
            if(!array_key_exists($totalObj['key_on_row'], $this->mapTable)) continue;

            // insert the total
            $this->worksheet->getCell($this->mapTable[$totalObj['key_on_row']] . $row)->setValue($totalObj['amount']);
        }


        // make the row bold
        $this->worksheet->getStyle('A' . $row . ':' . $this->worksheet->getHighestDataColumn() . $row)->getFont()->setBold(true);

    }

    /**
     * Creates a table containing data keys, and the associated column letter
     * This requires that data['columnHeadings'] contains the value key_on_row
     *
     * The parameter save_table will add the table to $this->mapTable
     */
    private function getKeyToColumnTable($save_table = false)
    {
        $mapTable = [];
        for($i = 0, $col = 'A', $l = sizeof($this->data['columnHeadings']); $i < $l; $i++, $col = chr(ord($col) + 1) ) {
            $mapTable[$this->data['columnHeadings'][$i]['key_on_row']] = $col;
        }

        if ($save_table) {
            $this->mapTable = $mapTable;
        }


        return $mapTable;
    }


    /**
     * The following function inserts a report description at the bottom of the excel spreadsheet
     * 3 rows below the last inserted data (based on column A).
     *
     * It requires the $data object of the generator class contain:
     * data: {
     *   ...,
     *      reportDescription: {
     *          'Example Title': 'Example Value',
     *          ...
     *      }
     *   ...
     * }
     *
     * reportDescription can contain as few or as many items as you would like.
     * At the bare minimum include:
     *  'Report Name': 'The report's name',
     *  'Creation Date': 'The reports creation date' OR null
     *      ** if the the value of Creation Date is null, it will be generated
     */
    protected function insertReportDescription()
    {
        // if no reportDescription is present simply return and do nothing
        if (!isset($this->data['reportDescription'])) { return; }

        $highest_data_row = $this->worksheet->getHighestDataRow('A');
        $row = $highest_data_row + 3;

        foreach($this->data['reportDescription'] as $key => &$value) {
            /**
             * merge cells AX and BX to ensure the titles have enough room
             * set the key
             * bold the key
             */
            $this->worksheet->mergeCells('A' . $row . ':' . 'B' . $row);
            $this->worksheet->getCell('A' . $row)->setValue($key);
            $this->worksheet->getStyle('A' . $row)->getFont()->setBold(true);

            if ($key == 'Creation Date' && empty($value)) {
                $value = $this->getFormattedCreationDate();
            }


            /**
             * merge cells CX to FX to ensure values (such as company name) have enough room
             * set the value
             * bold the value
             * align left
             */
            $this->worksheet->mergeCells('C' . $row . ':' . 'F' . $row);
            $this->worksheet->getCell('C' . $row)->setValue($value);
            $this->worksheet->getStyle('C' . $row)->getFont()->setBold(true);
            $this->alignCell('C' . $row, 'left');

            $row++;
        }

    }


    /**
     * This function gives all columns their required alignment.
     * It requires data contain an object called columnAlignments structured as follows:
     *  data {
     *    ...,
     *      columnAlignments: [
     *          {column: 'A', alignment: 'left'},
     *          {column: 'B', alignment: 'right'}
     *              ...
     *      ],
     *    ...
     *  }
     *
     * NOTE alignment must be either 'left', 'right', or 'center'
     *
     * The columnAlignments object must either be passed directly into the constructor as a part of data, or it will
     *      attempt to be created if the values are present on the column headings
     *
     */
    protected function alignColumns()
    {
        foreach($this->data['columnAlignments'] as $col) {
            $this->alignCell($col['column'], $col['alignment']);
        }
    }

    /**
     * Sets the alignment of the given cell (ex. A1) or column (ex. B) to
     * left, right, or center
     *
     * It requires that the worksheet property of the ExcelGeneratorObject be set and valid.
     *
     * @param $cell
     * @param $alignment (string) 'left', 'right', 'center'
     */
    public function alignCell($cell, $alignment)
    {
        if (!in_array($alignment, ['left', 'right', 'center'])) {
            abort(400, 'Invalid cell alignment for ' . $cell);
        }

        $a = null;
        switch($alignment) {
            case 'left':
                $a = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT;
                break;
            case 'right':
                $a = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT;
                break;
            case 'center':
                $a = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER;
                break;

        }

        $this->worksheet->getStyle($cell)
            ->getAlignment()
            ->setHorizontal($a);
    }


    /**
     * This function gives all columns their required number formats.
     * It requires data contain an object called numberFormats structured as follows:
     *  data {
     *    ...,
     *      columnNumberFormats: [
     *          {column: 'A', precision: 0, comma_separated: true},
     *          {column: 'B', precision: 2, comma_separated: true}
     *              ...
     *      ],
     *    ...
     *  }
     *
     * the numberFormats object must either be passed directly into the constructor as a part of data, or it will
     *      attempt to be created if the values are present on the column headings
     *
     */
    protected function setColumnNumberFormats()
    {
        if (!isset($this->data['columnNumberFormats'])) return;

        foreach($this->data['columnNumberFormats'] as $col) {
            $this->setCellNumberFormat($col['column'], $col['precision'], $col['comma_separated']);
        }
    }

    /**
     * Sets the number format of the given cell (ex. A1) or column (ex. B) to
     * a provided precision, and applies comma separators (thousands)
     *
     * @param $cell
     * @param $precision - null or an integer between 0 and 10 inclusive
     * @param $comma_separated - boolean, true will comma separate thousands
     */
    public function setCellNumberFormat($cell, $precision, $comma_separated)
    {
        if (!empty($precision) && ($precision < 0 || $precision > 10)) {
            /**
             * 10 was chosen as only 15 sigdigs can be shown by excel.
             * https://en.wikipedia.org/wiki/Numeric_precision_in_Microsoft_Excel
             */
            abort(400, 'Invalid cell precision for ' . $cell . '. Precision must be null, or between 0 and 10');
        }

        $c = $comma_separated ? '#,##0' : '###0';
        if ($precision > 0) $c .= '.' . str_pad('', $precision, '0', STR_PAD_RIGHT);

        $this->worksheet->getStyle($cell)
            ->getNumberFormat()
            ->setFormatCode($c);
    }

    /**
     * Make the column widths correctly sized to fit the data
     */
    protected function autosizeColumnWidths()
    {
        $length = sizeof($this->data['columnHeadings']);
        for($i = ord('A'); $i < ord('A') + $length; $i++) {
            $this->worksheet->getColumnDimension(chr($i))->setAutoSize(true);
        }
    }

    /**
     * Gets a nicely formatted version of today ex. October 9th, 2018
     * @return false|string
     */
    public function getFormattedCreationDate()
    {
        return date('F d, Y');
    }

    /**
     * Stores the file in the temporary directory
     * with the filename specified in the child class
     */
    public function store()
    {
        try {
            $writer = PhpSpreadsheet\IOFactory::createWriter($this->spreadsheet, 'Xls');
            $writer->save(public_path('/storage/tmp') . '/' . $this->filename);
        } catch(\Exception $e) {
            return response($e->getMessage(), 500);
        }

    }

    /**
     * Returns the spreadsheet data
     *
     * @return
     */
    public function getSpreadsheet()
    {
        return $this->spreadsheet;
    }

}