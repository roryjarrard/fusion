<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class favrParameter extends Model
{
    protected $fillable = ['vehicle_cost_limit', 'truck_cost_limit', 'standard_rate', 'vehicle_cost_percentage', 'profile_vehicle_percentage', 'maximum_tax_rate'];

    public $primaryKey = 'year';
}
