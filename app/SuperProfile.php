<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuperProfile extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'active', 'is_account_executive', 'is_insurance_processor', 'is_license_processor', 'is_payment_processor',
        'is_service_associate', 'is_vehicle_profile_approver', 'is_announcement_approver',
        'can_view_eft', 'can_view_login_information', 'can_view_database_tables',
        'is_database_maintainer', 'is_bank_maintainer', 'is_caa_maintainer', 'is_tasks_maintainer', 'has_all_permissions'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function address()
    {
        //return $this->hasOne('App\Address');

        return Address::where('user_id', $this->user_id)->first();
    }
}