<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingChange extends Model
{
    public $timestamps = false;
    protected $fillable = ['changed_id', 'by_whom', 'change_type', 'change_description', 'old_value', 'new_value', 'notes', 'change_date'];
}
