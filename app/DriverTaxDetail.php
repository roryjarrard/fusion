<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverTaxDetail extends Model
{
    protected $table = 'driver_tax_details';

    protected $fillable = ['driver_tax_id', 'driver_tax_column_id', 'driver_tax_row_id', 'amount'];
}
