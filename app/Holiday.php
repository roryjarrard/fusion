<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $fillable = ['id', 'year', 'month', 'day', 'day_of_week', 'country_id', 'state_province_id',
                            'name', 'note'];

    public function state() {
        return $this->belongsTo('App\StateProvince', 'state_province_id', 'id');
    }

    public function province() {
        return $this->belongsTo('App\StateProvince', 'state_province_id', 'id');
    }

    public function country() {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    //TODO: reformat functions, return lists for province, or specific state, etc...
}
