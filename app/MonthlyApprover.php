<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyApprover extends Model
{
    protected $fillable = ['approval_id', 'manager_id', 'administrator_id', 'override_user_id', 'approval_date'];
}
