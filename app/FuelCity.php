<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FuelCity extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'name', 'country_id', 'state_province_id', 'lookup_key', 'latitude', 'longitude'];

    public function state() {
        return $this->belongsTo('App\StateProvince', 'state_province_id', 'id');
    }
}
