<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyLicense extends Model
{
    use SoftDeletes;

    protected $fillable = ['company_id',  'license_responsibility', 'license_review_interval', 'license_review_day', 'license_review_month',
        'notify_license_expiry', 'license_fax_number', 'initial_license_enforcement_date', 'new_driver_license_grace_days'];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }


    public function defaultDateOfNextReview()
    {
        // if the review interval is not annually return null, there is no default for this company.
        if ( $this->license_review_interval != 'Annually') {
            return null;
        }

        $today = date('Y-m-d');

        $year = date('Y');

        $default_date_of_next_review = date('Y-m-d', mktime(0,0,0, $this->license_review_month, $this->license_review_day, $year));

        if ( $default_date_of_next_review <= $today  ) {
            $year = (int)$year + 1;
            $default_date_of_next_review = date('Y-m-d', mktime(0,0,0, $this->license_review_month, $this->license_review_day, $year));
        }

        return $default_date_of_next_review;
    }
}
