<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceRate extends Model
{
    protected $fillable = ['country_id', 'state_province_id', 'year', 'zip_code_id', 'postal_code_id',
        'latitude', 'longitude', 'monthly_cost', 'city_name'];
}
