<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GenerateUsername extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGenerateUsername()
    {
        $users = [
            ['first_name' => 'franklin', 'last_name' => 'domsy', 'expected_result' => 'f3domsy'],
            ['first_name' => 'habibi', 'last_name' => 'habib', 'expected_result' => 'hhabib'],
        ];

        foreach( $users as $user ) {
            $username = app('\App\Http\Controllers\UserController')
                ->generateUsername($user['first_name'], $user['last_name']);

            $this->assertTrue($username == $user['expected_result']);
        }


    }
}
