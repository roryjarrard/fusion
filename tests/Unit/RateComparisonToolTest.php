<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\VehicleProfile;
use App\User;

class RateComparisonToolTest extends TestCase
{
    /**
     * This test will try to 'override' a single user's vehicle profile with the same vehicle profile,
     * calculate impact without altering the database, and then check that the reimbursement values match.
     */
    public function testSingleDriverCalculationWithProposedVehicleProfile()
    {
        $year = 2018;
        $month = 2;

        $user_id = 9616; // Brandi Carr (wm)
        $user = User::find($user_id);
        $proposed_vehicle_profile_id = 988; // proposed profile for testing
        $proposedVehicleProfile = VehicleProfile::find($proposed_vehicle_profile_id);

        // fetch all required data used in the calculation of reimbursement
        $reimbursementData = app('\App\Http\Controllers\ReimbursementController')
            ->getDataForReimbursementFromDriver(
                $year, $month, $user->driverProfile->company->options->service_plan,
                $user->driverProfile->id);

        // todo: go untangle the reimbursement code and remove non-geo data from geo_data
        $reimbursementData->geo_data->reimbursement_detail_id = null;
        $reimbursementData->geo_data->reimbursement_details = null;

        // todo go untangle the code and rename vehicleProfile appropriately
        $reimbursementData->vehicle_profile = $proposedVehicleProfile;
        $reimbursementData->vehicle_profile->vehicle;

        /**
         * calculate the fixed and reimbursement based upon reimbursementData, and DO NOT alter the database
         * Jerzy's "Proxy" methods ensure no inserts or updates take place
         */
        $fixedReimbursementData = app('\App\Http\Controllers\ReimbursementController')
            ->calculateFixedReimbursementProxy($reimbursementData);

        $variableReimbursementData = app('\App\Http\Controllers\ReimbursementController')
            ->calculateVariableReimbursementProxy($reimbursementData);

        $pastReimbursement = $user->driverProfile->reimbursement($year, $month);

        $this->assertEquals(
            $pastReimbursement['fixed_reimbursement'],
            $fixedReimbursementData['fixedReimbursement']
        );

        $this->assertEquals(
            $pastReimbursement['variable_reimbursement'],
            $variableReimbursementData['variableReimbursement']
        );

    }

    /**
     * This test will clone a vehicle profile, insert it into the database, try to calculate impact and compare it
     * to current values.
     */
//    public function testCalculateImpactOnSingleDriverProfile()
//    {
//        $vp = \App\VehicleProfile::find(942);
//        $vpClone = $vp->replicate();
//        $vpClone->proposed = 1;
//        $vpClone->save();
//
//        $requestData = [
//            'mappings' => [
//                [
//                    'proposed_vehicle_profile_id' => $vpClone->id,
//                    'old_vehicle_profile_id' => $vp->id,
//                ]
//            ]
//        ];
//
//
//        $request = new \Illuminate\Http\Request;
//        $request->createFromBase(
//            \Symfony\Component\HttpFoundation\Request::create(
//                $uri,
//                $method,
//                $parameters,
//                $cookies,
//                $files,
//                $server,
//                $content
//            )
//        );
//
//        $reimbursementData = app('\App\Http\Controllers\ReimbursementController')
//            ->calculateImpactOfProposedVehicleProfiles(
//                json_encode($requestData)
//            );
//
//
//        $response = null;
//
//
//    }
}
