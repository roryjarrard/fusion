<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

defined("CARDATA_BLUE_HEX") or define("CARDATA_BLUE_HEX", '#052460');
// TODO: this cant happen, constants must be scalar values
//defined("CARDATA_BLUE_RGB") or define("CARDATA_BLUE_RGB", [5,36,96]);
defined("CARDATA_BLUE_ARGB") or define("CARDATA_BLUE_ARGB", 'FF052460');

defined("CARDATA_RED_HEX") or define("CARDATA_RED_HEX", '#910811');
// TODO: this cant happen, constants must be scalar values
//defined("CARDATA_RED_RGB") or define("CARDATA_RED_RGB", [145,8,17]);
defined("CARDATA_RED_ARGB") or define("CARDATA_RED_ARGB", 'FF910811');

class ExcelGeneratorTest extends TestCase
{
    protected $fileController;
    protected $generator;

    protected function setUp() {
        $this->fileController = new \App\Http\Controllers\FileController();
    }

    /**
     * Generates an excel file and compares it against a known excel file
     *
     * @return void
     */
    public function testExcelGeneration()
    {
        // get some dummy data
        $data = [];
        $data['categoryHeadings'] = [
            ['title' => 'Driver', 'column_span' => 3],
        ];

        $data['columnHeadings'] = [
            ['title' => 'Last Name', 'column_alignment' => 'left', 'key_on_row' => 'last_name'],
            ['title' => 'First Name', 'column_alignment' => 'right', 'key_on_row' => 'first_name'],
            ['title' => 'Fake Reimb', 'column_alignment' => 'right', 'key_on_row' => 'fake_reimb', 'precision' => 2]
        ];

        $data['rows'] = [
            ['first_name' => 'John', 'last_name' => 'Rowone', 'asdfgg' => 3333, 'fake_reimb' => 2.00],
            ['first_name' => 'Jacob', 'last_name' => 'Rowtwo', 'fake_reimb' => 1000.12344]
        ];

        $data['totals'] = [
            'fake_reimb' => 1000.12344 + 2.00
        ];

        // the report description
        $data['reportDescription'] = [
            'Report Name' => 'Test Excel Generator',
            'Creation Date' => 'This is a dummy date',
            'Company' => 'CarData Consultants Inc.',
        ];

        $this->generator = new \App\FileGenerators\ExcelGenerator($data);
        $this->generator->create();
        $this->generator->filename = 'ExcelGeneratorTest-GeneratedFile.xls';
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->generator->getSpreadsheet(), 'Xls');
        $writer->save('tests/test_documents/' . $this->generator->filename);

        /**
         * Compare the files
         */
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        // no need to read styles, we just care about data
        $reader->setReadDataOnly(true);
        // load expected file (stored somewhere in the tests directory)
        $spreadsheetExpected = $reader->load('tests/test_documents/ExcelGeneratorTest-ControlFile.xls');
        // load the generated file
        $spreadsheetActual = $reader->load('tests/test_documents/' . $this->generator->filename);
        // loop through 2 rows
        foreach (range(1, 20) as $row) {
            // loop through first 6 columns
            foreach (['A', 'B', 'C', 'D', 'E', 'F'] as $column) {
                // coord
                $cell = $column . $row;
                // get expected cell value
                $expected = $spreadsheetExpected->getSheet(0)->getCell($cell)->getValue();
                // get actual cell value
                $actual = $spreadsheetActual->getSheet(0)->getCell($cell)->getValue();
                // compare values, show the sheet and coordination in case of failure
                $this->assertEquals($expected, $actual, "Mismatch in sheet {0}, cell {$cell}");
            }
        }

    }
}
