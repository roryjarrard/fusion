<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

use App\Calculators\StreetLightCalculator;

class StreetLightCalculatorTest extends TestCase
{


    public function testCalculateStreetLightId()
    {
        $c = new StreetLightCalculator();

        // 463
        // use company mileage option only.
        // No insurance, license, or banking modules selected.
        $this->assertEquals(User::find(21017)->driverProfile->street_light_id, $c->calculateStreetLightId(21017));

        // TODO: Write tests for users from Data Set for Testing Users.xlsx

    }

    public function testStreetLightVehicleAgeCompliance()
    {
        $expected_street_light_ids = [2, 516, 40960, 4];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // RED_VEHICLE_TOO_OLD to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdVehicleAgeCompliance(4, '463', 2, 1));

        // RED_TERMINATED to RED_TERMINATED + NO_VEHICLE_AGE_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdVehicleAgeCompliance(512, '463', 1, 3));

        // GREEN_NO_INSURANCE_COMPLIANCE to GREEN_NO_INSURANCE_COMPLIANCE + GREEN_NO_VEHICLE_AGE_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdVehicleAgeCompliance(8192, 'FAVR', 3, 4));

        // GREEN_REIMBURSEMENT to RED_VEHICLE_TOO_OLD
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdVehicleAgeCompliance(2, '463', 3, 4));


        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);

    }

    public function testStreetLightVehicleCostCompliance()
    {
        $expected_street_light_ids = [2, 16384, 2, 16400, 16];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN_REIMBURSEMENT to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdVehicleCostCompliance(2, true));

        // GREEN_NO_VEHICLE_COST_COMPLIANCE to GREEN_NO_VEHICLE_COST_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdVehicleCostCompliance(16384, false));

        // GREEN_NO_VEHICLE_COST_COMPLIANCE to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdVehicleCostCompliance(16384, true));

        // RED_AWAITING_INSURANCE to RED_AWAITING_INSURANCE + GREEN_NO_VEHICLE_COST_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdVehicleCostCompliance(16, false));

        // GREEN_NO_VEHICLE_COST_COMPLIANCE + RED_AWAITING_INSURANCE to RED_AWAITING_INSURANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdVehicleCostCompliance(16400, true));

        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);

    }

    public function testStreetLightAnnualDeclarationCompliance()
    {
        $expected_street_light_ids = [2, 4, 524288, 64 + 524288, 2, 64];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN_REIMBURSEMENT to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdAnnualDeclarationCompliance(2, true));

        // RED_VEHICLE_TOO_OLD to RED_VEHICLE_TOO_OLD
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdAnnualDeclarationCompliance(4, true));

        // GREEN_REIMBURSEMENT to GREEN_NO_ANNUAL_DECLARATION
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdAnnualDeclarationCompliance(2, false));

        // RED_INSURANCE_EXPIRED to RED_INSURANCE_EXPIRED + GREEN_NO_ANNUAL_DECLARATION
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdAnnualDeclarationCompliance(64, false));

        // GREEN_NO_ANNUAL_DECLARATION to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdAnnualDeclarationCompliance(524288, true));

        // RED_INSURANCE_EXPIRED + GREEN_NO_ANNUAL_DECLARATION to RED_INSURANCE_EXPIRED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdAnnualDeclarationCompliance(64 + 524288, true));

        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);

    }

    public function testStreetLightDepreciationCompliance()
    {
        $expected_street_light_ids = [2, 4, 131072, 64 + 131072, 2, 64];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN_REIMBURSEMENT to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdDepreciationCompliance(2, true));

        // RED_VEHICLE_TOO_OLD to RED_VEHICLE_TOO_OLD
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdDepreciationCompliance(4, true));

        // GREEN_REIMBURSEMENT to GREEN_NO_DEPRECIATION_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdDepreciationCompliance(2, false));

        // RED_INSURANCE_EXPIRED to RED_INSURANCE_EXPIRED + GREEN_NO_DEPRECIATION_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdDepreciationCompliance(64, false));

        // GREEN_NO_DEPRECIATION_COMPLIANCE to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdDepreciationCompliance(131072, true));

        // RED_INSURANCE_EXPIRED + GREEN_NO_DEPRECIATION_COMPLIANCE to RED_INSURANCE_EXPIRED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdDepreciationCompliance(64 + 131072, true));

        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);

    }

    public function testStreetLightInsuranceAwaiting()
    {

        $expected_street_light_ids = [16, 2, 16, 32784, 2, 16];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN_REIMBURSEMENT to RED_AWAITING_INSURANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceAwaiting(2, '463', 0, false));

        // RED_AWAITING_INSURANCE to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceAwaiting(16, '463', 0, true));

        // GREEN_REIMBURSEMENT to GREEN_NO_INSURANCE_IN_GRACE_PERIOD  note! this is old FAVR only we are assuming in grace
//        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceAwaiting(2, 'FAVR', 0, false));

        // GREEN_REIMBURSEMENT to RED_AWAITING_INSURANCE  note! this is new FAVR with remove_reimbursement_without_insurance
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceAwaiting(2, 'FAVR', 1, false));

        // GREEN_NO_VEHICLE_AGE_COMPLIANCE to RED_AWAITING_INSURANCE + GREEN_NO_VEHICLE_AGE_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceAwaiting(32768, 'FAVR', 1, false));

        // RED_AWAITING_INSURANCE to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceAwaiting(16, 'FAVR', 1, true));

        // RED_AWAITING_INSURANCE to RED_AWAITING_INSURANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceAwaiting(16, 'FAVR', 1, false));

        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);

    }

    public function testStreetLightInsuranceProcessed()
    {
        $expected_street_light_ids = [
            pow(2,25), // RED_INSURANCE_AWAITING_PROCESSING
            pow(2,25), // RED_INSURANCE_AWAITING_PROCESSING
            pow(2,26), // GREEN_INSURANCE_AWAITING_PROCESSING
            2, // GREEN_REIMBURSEMENT
            4, // RED_VEHICLE_TOO_OLD
            4, // RED_VEHICLE_TOO_OLD
            2  // GREEN_REIMBURSEMENT
        ];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN TO RED_INSURANCE_AWAITING_PROCESSING  FAVR
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceProcessed(2, 'FAVR', 1, false));

        // GREEN TO RED_INSURANCE_AWAITING_PROCESSING  463
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceProcessed(2, '463', 1, false));

        // GREEN TO GREEN_INSURANCE_AWAITING_PROCESSING
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceProcessed(2, 'FAVR', 0, false));

        // GREEN_INSURANCE_AWAITING_PROCESSING TO GREEN
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceProcessed(pow(2,26), 'FAVR', 0, true));

        // RED_INSURANCE_AWAITING_PROCESSING + RED_VEHICLE_TOO_OLD TO RED_VEHICLE_TOO_OLD
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceProcessed(pow(2,25) + 4, '463', 0, true));

        // RED_INSURANCE_AWAITING_PROCESSING + RED_VEHICLE_TOO_OLD TO RED_VEHICLE_TOO_OLD
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceProcessed(pow(2,25) + 4, '463', 0, true));

        // RED_INSURANCE_AWAITING_PROCESSING TO GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceProcessed(pow(2,25), '463', 0, true));

        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);
    }

    public function testStreetLightInsuranceSufficient()
    {
        $expected_street_light_ids = [262144, 8192, 2, 2, 8, 8];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();


        /**
         * GREEN_NO_INSURANCE_GRACE_PERIOD to GREEN_NO_INSURANCE_GRACE_PERIOD
         * This function should hit the first if statement in and not bother trying to calculate further as no insurance was provided (returns the original street_light)
         */
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceSufficient(262144, 'FAVR', 0, false, false));

        /**
         * GREEN_NO_INSURANCE_COMPLIANCE to GREEN_NO_INSURANCE_COMPLIANCE
         * This function should return the same status, as an out of compliance insurance remained out of compliance
         */
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceSufficient(8192, 'FAVR', 0, true, false));


        // RED_INSUFFICIENT_INSURANCE to GREEN_REIMBURSEMENT (463 & FAVR modern)
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceSufficient(8, 'FAVR', 1, true, true));
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceSufficient(8, '463', 1, true, true));

        // GREEN_REIMBURSEMENT to RED_INSUFFICIENT_INSURANCE (463 & FAVR modern)
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceSufficient(2, 'FAVR', 1, true, false));
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceSufficient(2, '463', 1, true, false));




        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);

    }

    public function testStreetLightInsuranceExpired()
    {

        $expected_street_light_ids = [64, 64, 8192, 2, 2, 2, 32768];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN_REIMBURSEMENT to RED_INSURANCE_EXPIRED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceExpired(2, '463', 0, true));

        // GREEN_REIMBURSEMENT to RED_INSURANCE_EXPIRED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceExpired(2, 'FAVR', 1, true));

        // GREEN_REIMBURSEMENT to GREEN_NO_INSURANCE_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceExpired(2, 'FAVR', 0, true));

        // RED_INSURANCE_EXPIRED to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceExpired(64, '463', 0, false));

        // RED_INSURANCE_EXPIRED to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceExpired(64, 'FAVR', 1, false));

        // RED_INSURANCE_EXPIRED to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceExpired(64, 'FAVR', 0, false));

        // GREEN_NO_VEHICLE_AGE_COMPLIANCE to GREEN_NO_VEHICLE_AGE_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceExpired(32768, 'FAVR', 1, false));


        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);

    }

    public function testStreetLightInsuranceCompliance()
    {

        $expected_street_light_ids = [16, 16, 8192, 8, 8, 8, 64, 72];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN_REIMBURSEMENT to RED_AWAITING_INSURANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceCompliance(2, '463', 0, false, false, false));

        // GREEN_REIMBURSEMENT to RED_AWAITING_INSURANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceCompliance(2, 'FAVR', 1, false, false, false));

        // GREEN_REIMBURSEMENT to GREEN_NO_INSURANCE_COMPLIANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceCompliance(2, 'FAVR', 0, true, false, false));

        // GREEN_REIMBURSEMENT to RED_INSUFFICIENT_INSURANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceCompliance(64, '463', 0, true, false, false));

        // RED_AWAITING_INSURANCE to RED_INSUFFICIENT_INSURANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceCompliance(16, 'FAVR', 1, true, false, false));

        // RED_AWAITING_INSURANCE to RED_INSUFFICIENT_INSURANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceCompliance(16, '463', 1, true, false, false));

        // GREEN_REIMBURSEMENT to RED_INSURANCE_EXPIRED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceCompliance(2, 'FAVR', 1, true, true, true));

        // RED_INSUFFICIENT_INSURANCE to RED_INSURANCE_EXPIRED + RED_INSUFFICIENT_INSURANCE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdInsuranceCompliance(16, 'FAVR', 1, true, false, true));

        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);


    }

    public function testStreetLightLicenseCompliance()
    {
        $expected_street_light_ids = [1048576, 4096, 2, 4096, 2, 2, 4096];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN_REIMBURSEMENT to RED_AWAITING_LICENSE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdLicenseCompliance(2, false));

        // GREEN_REIMBURSEMENT to RED_LICENSE_EXPIRED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdLicenseCompliance(2, true, true));

        // GREEN_REIMBURSEMENT to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdLicenseCompliance(2, true, false));

        // RED_AWAITING_LICENSE to RED_LICENSE_EXPIRED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdLicenseCompliance(1048576, true, true));

        // RED_AWAITING_LICENSE to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdLicenseCompliance(1048576, true, false));

        // RED_LICENSE_EXPIRED to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdLicenseCompliance(1048576, true, false));

        // RED_LICENSE_EXPIRED to RED_LICENSE_EXPIRED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdLicenseCompliance(1048576, true, true));

        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);
    }

    public function testStreetLightIdBankingCompliance()
    {

        $expected_street_light_ids = [4194304, 2, 1048576, 1048576 + 4194304];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN_REIMBURSEMENT to GREEN_NO_BANKING_DATA
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdBankingCompliance(2, false));

        // GREEN_NO_BANKING_DATA to GREEN_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdBankingCompliance(4194304, true));

        // RED_AWAITING_LICENSE + GREEN_NO_BANKING_DATA to RED_AWAITING_LICENSE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdBankingCompliance(1048576 + 4194304, true));

        // RED_AWAITING_LICENSE to RED_AWAITING_LICENSE + GREEN_NO_BANKING_DATA
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateStreetLightIdBankingCompliance(1048576, false));


        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);

    }

    public function testCalculateDeactivationStatus()
    {

        $expected_street_light_ids = [128, 128, 256, 256, 512, 512, 32, 32, 32 + 128 + 256 + 512, 32 + 128 + 256 + 512, 128 + 256, 128 + 256, 32, 32];
        $calculated_street_light_ids = [];
        $streetLightCalculator = new StreetLightCalculator();

        // GREEN_REIMBURSEMENT + RED_ON_LEAVE to RED_ON_LEAVE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus(130));

        // GREEN_REIMBURSEMENT + RED_ON_LEAVE to RED_ON_LEAVE
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus([128, 2]));

        // GREEN_REIMBURSEMENT + RED_DISABILITY to RED_DISABILITY
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus(258));

        // GREEN_REIMBURSEMENT + RED_DISABILITY to RED_DISABILITY
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus([256, 2]));

        // GREEN_REIMBURSEMENT + RED_TERMINATED to RED_TERMINATED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus(514));

        // GREEN_REIMBURSEMENT + RED_TERMINATED to RED_TERMINATED
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus([512, 2]));

        // GREEN_REIMBURSEMENT + RED_NO_REIMBURSEMENT to RED_NO_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus(34));

        // GREEN_REIMBURSEMENT + RED_NO_REIMBURSEMENT to RED_NO_REIMBURSEMENT
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus([32, 2]));

        // straight calculation when passed combinations of deactivation streetlights
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus(32 + 128 + 256 + 512 ));
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus([32, 128, 256, 512]));
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus(128 + 256));
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus([128, 256]));

        // single deactivation street light passed
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus([32]));
        array_push($calculated_street_light_ids, $streetLightCalculator->calculateDeactivationStatus(32));

        $this->assertEquals($expected_street_light_ids, $calculated_street_light_ids);

    }

    public function testFromRedToGreen()
    {
        $expected_booleans = [ true, true, false, false, false, false];
        $calculated_booleans = [];
        $c = new StreetLightCalculator();

        // RED_VEHICLE_TOO_OLD + RED_INSUFFICIENT_INSURANCE to GREEN_REIMBURSEMENT
        array_push($calculated_booleans, $c->fromRedToGreen(2, 4 + 8));

        // RED_DRIVER_LICENCE_REQUIRED + GREEN_NO_INSURANCE_COMPLIANCE + GREEN_NO_BANKING_DATA  to GREEN_NO_BANKING_DATA
        array_push($calculated_booleans, $c->fromRedToGreen(4194304, 4096 + 8192 + 4194304));

        // GREEN_REIMBURSEMENT to RED_ON_LEAVE
        array_push($calculated_booleans, $c->fromRedToGreen(128, 2));

        // RED_AWAITING_INSURANCE to RED_INSUFFICIENT_INSURANCE
        array_push($calculated_booleans, $c->fromRedToGreen(8, 16));

        // GREEN_REIMBURSEMENT + GREEN_REIMBURSEMENT
        array_push($calculated_booleans, $c->fromRedToGreen(2, 2));

        // GREEN_REIMBURSEMENT to GREEN_NO_INSURANCE_COMPLIANCE
        array_push($calculated_booleans, $c->fromRedToGreen(65536, 2));


        $this->assertEquals($expected_booleans, $calculated_booleans);
    }


}
