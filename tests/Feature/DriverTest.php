<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DriverTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function a_driver_initially_sees_login_page()
    {
        $this->get('/login')->assertSee('Login');
    }

    /** @test */
    public function a_dirvers_enters_wrong_credentials_and_sees_failure_message()
    {
//        $this->visit('/login');
    }
}
