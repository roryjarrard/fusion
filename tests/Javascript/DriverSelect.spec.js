import { mount } from 'vue-test-utils'
import Example from '../../resources/assets/js/components/Example.vue';
import expect from 'expect';

describe('Example', () => {
    // Now mount the component and you have the wrapper
    const wrapper = mount(Example)

    it('renders the correct markup', () => {
        expect(wrapper.html()).toContain('<span class="count">0</span>')
    })

    // it's also easy to check for the existence of elements
    it('has a button', () => {
        expect(wrapper.contains('button')).toBe(true)
    })
})