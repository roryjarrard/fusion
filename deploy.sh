#!/bin/bash

### stage is stage1, stage2, stage3
STAGE=$1
TAR="deploy.$STAGE.tgz"
tar --exclude=node_modules --exclude=vendor --exclude=.git --exclude=storage/app/public/docs --exclude=storage/debugbar/* --exclude=storage/framework/views/* -zcvf ${TAR} .
scp ${TAR} cdstaging:.
rm ${TAR}
ssh -tt cdstaging 'bash -s' < post_${STAGE}.sh


