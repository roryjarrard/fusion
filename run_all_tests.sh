#!/usr/bin/env bash
rm test_results.txt
touch test_results.txt

echo "Running all php tests..."

for file in tests/Feature/*
do
    vendor/bin/phpunit "$file" >> test_results.txt
done

for file in tests/Unit/*
do
    vendor/bin/phpunit "$file" >> test_results.txt
done

echo "Done"

echo "Running all vue tests..."

yarn test >> test_results.txt

echo "Done"
echo "------ LOG ------"

cat test_results.txt